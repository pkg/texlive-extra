%% start of file `old-arrows.tex'.
%% Copyright 2015-2017 Riccardo Dossena (riccardo.dossena@gmail.com).
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.
%%
\documentclass[11pt]{ltxdoc}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[shortlabels]{enumitem}
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage[new]{old-arrows}
\usepackage[hmargin=3.3cm,vmargin=2.5cm,footskip=30pt]{geometry}
\usepackage[colorlinks=true]{hyperref}
\usepackage[all]{hypcap} % needed to help hyperlinks direct correctly;
\usepackage[chicago]{ellipsis}

\def\fileversion{2.0}
\def\filedate{January 5, 2017}

%Logo per MiKTeX e TeXLive
\newcommand{\MiKTeX}{MiK\TeX}
\newcommand{\TeXLive}{\mbox{\TeX\ Live}}
%
%
% Symbol Entry for Math Symbol Tables
%
\newcommand{\X}[1]{$#1$&\texttt{\string#1}\hspace*{1ex}}

\newcommand{\W}[2]{$#1{#2}$&
  \texttt{\string#1}\texttt{\string{\string#2\string}}\hspace*{1ex}}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

% Mathsymbol Table
\newsavebox{\symbbox}
\newenvironment{symbols}[1]%
{\par\vspace*{2ex}
\renewcommand{\arraystretch}{1.1}
\begin{lrbox}{\symbbox}
\hspace*{4ex}\begin{tabular}{@{}#1@{}}}%
{\end{tabular}\end{lrbox}\makebox[\textwidth]{\usebox{\symbbox}}\par\medskip}

%Per l'esempio senza amsmath
\makeatletter
\def\overrightfreccia#1{\vbox{\m@th\ialign{##\crcr
      \rightfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\overleftfreccia#1{\vbox{\m@th\ialign{##\crcr
      \leftfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}%
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\varoverrightfreccia#1{\vbox{\m@th\ialign{##\crcr
      \varrightfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\varoverleftfreccia#1{\vbox{\m@th\ialign{##\crcr
      \varleftfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}%
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\rightfrecciafill{$\m@th\smash\meno\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash\meno\mkern-2mu$}\hfill
  \mkern-7mu\mathord\rightarrow$}
\def\leftfrecciafill{$\m@th\mathord\leftarrow\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash\meno\mkern-2mu$}\hfill
  \mkern-7mu\smash\meno$}
\def\varrightfrecciafill{$\m@th\smash-\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash-\mkern-2mu$}\hfill
  \mkern-7mu\mathord\varrightarrow$}
\def\varleftfrecciafill{$\m@th\mathord\varleftarrow\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash-\mkern-2mu$}\hfill
  \mkern-7mu\smash-$}
\makeatother

\begin{document}

\title{The \textsf{old-arrows} package}
\author{Riccardo Dossena\thanks{%
E-mail: \href{mailto:riccardo.dossena@gmail.com}{\tt riccardo.dossena@gmail.com}}}
\date{Version \fileversion, released on \filedate}

\maketitle

\begin{abstract}
This package provides Computer Modern old-style arrows ($\rightarrow$) with smaller arrowheads, associated
with ordinary \LaTeX\ commands. It can be used in a document that contains other \texttt{amssymb} arrow
characters, like $\twoheadrightarrow$, which also have small arrowheads. It is possible to use the usual
new-style Computer Modern arrows ($\varrightarrow$) together with the old-style ones.
\end{abstract}

\tableofcontents

%\pagebreak

\section{Introduction}

In 1992, Donald E. Knuth made some important corrections to Computer Modern fonts\footnote{See
\url{http://www-cs-faculty.stanford.edu/~uno/cm.html}}. As a consequence, the characters corresponding to
arrows have been modified. Just to make things clearer,
\[
A \to B
\]
became
\[
A \varto B
\]
that is, the character $\to$ was replaced by $\varto$, which has a larger arrowhead. The same happened to
other arrow characters. However, many arrow characters defined by \texttt{amssymb}, like $\nrightarrow$,
$\rightarrowtail$, $\twoheadrightarrow$ and others, maintained a small arrowhead and seem too different from
$\varto$.

The \texttt{old-arrows} package with Old Arrows font family allows the user to use the old-style arrows
($\to$, $\gets$, \ldots) with the traditional commands (\verb|\rightarrow|, \verb|\leftarrow|,~\ldots).
Furthermore, the options \texttt{new} and \texttt{old} allow the user to obtain the new-style arrows
($\varto$, $\vargets$,~\ldots) together with the  old-style ones by putting  \verb|\var| before the
corresponding commands (\verb|\varrightarrow|, \verb|\varleftarrow|,~\ldots)\footnote{See sections
\ref{sec:new} and \ref{sec:old}.}.

Old Arrows font family was derived from an old version of Blue Sky Computer Modern Math Symbols (1991--1992,
released by AMS) by deleting many characters with FontForge.

\section{Licenses}

The \LaTeX\ code in this package is licensed under the \LaTeX\ Project Public License, v1.3.

The fonts in this package are licensed under the SIL Open Font License, v1.1.

\section{Installation}

The \texttt{old-arrows} package is included in the latest \MiKTeX\ and \TeXLive\ distributions. However, if
you want to install it manually, follow the instructions below.

\subsection{Copying the files in the local \texttt{texmf} tree}

The Old Arrows fonts files are:

\begin{center}
\begin{tabular}{p{3cm}p{3cm}p{3cm}l}
\hline
\texttt{oasy5.afm}   & \texttt{oasy5.pfm}   & \texttt{oasy5.tfm}   &  \texttt{oasy5.pfb}   \\
\texttt{oasy6.afm}   & \texttt{oasy6.pfm}   & \texttt{oasy6.tfm}   &  \texttt{oasy6.pfb}   \\
\texttt{oasy7.afm}   & \texttt{oasy7.pfm}   & \texttt{oasy7.tfm}   &  \texttt{oasy7.pfb}   \\
\texttt{oasy8.afm}   & \texttt{oasy8.pfm}   & \texttt{oasy8.tfm}   &  \texttt{oasy8.pfb}   \\
\texttt{oasy9.afm}   & \texttt{oasy9.pfm}   & \texttt{oasy9.tfm}   &  \texttt{oasy9.pfb}   \\
\texttt{oasy10.afm}  & \texttt{oasy10.pfm}  & \texttt{oasy10.tfm}  &  \texttt{oasy10.pfb}  \\
\texttt{oabsy5.afm}  & \texttt{oabsy5.pfm}  & \texttt{oabsy5.tfm}  &  \texttt{oabsy5.pfb}  \\
                     &                      & \texttt{oabsy6.tfm}  &                       \\
\texttt{oabsy7.afm}  & \texttt{oabsy7.pfm}  & \texttt{oabsy7.tfm}  &  \texttt{oabsy7.pfb}  \\
                     &                      & \texttt{oabsy8.tfm}  &                       \\
                     &                      & \texttt{oabsy9.tfm}  &                       \\
\texttt{oabsy10.afm} & \texttt{oabsy10.pfm} & \texttt{oabsy10.tfm} &  \texttt{oabsy10.pfb} \\
\hline
\end{tabular}
\end{center}

These files were derived from Computer Modern fonts \texttt{cmbsy5}, \texttt{cmbsy7}, \texttt{cmbsy10},
\texttt{cmsy5}, \texttt{cmsy7}, \texttt{cmsy8}, \texttt{cmsy9} and \texttt{cmsy10}.

Call \texttt{<localtexmf>} the path of your local \texttt{texmf} tree. For \TeXLive, the local tree is
usually placed in \texttt{/usr/local/texlive/texmf-local}; for \MiKTeX, it can be set up on any directory, by
the \textsf{Roots} tab of ``\MiKTeX\ Options''.

\begin{enumerate}
\item Copy the \texttt{*.afm} and \texttt{*.tfm} font files into the corresponding \texttt{old-arrows}
    directories (you have to create them, as shown below):
\begin{verbatim}
    <localtexmf>/fonts/afm/old-arrows
    <localtexmf>/fonts/tfm/old-arrows
\end{verbatim}

\item Copy the \texttt{*.pfb} and \texttt{*.pfm} font files into the directory
\begin{verbatim}
    <localtexmf>/fonts/type1/old-arrows
\end{verbatim}

\item Copy the \texttt{oasy.enc} and \texttt{oasy.map} files, respectively, into the directories
\begin{verbatim}
    <localtexmf>/fonts/enc/dvips/old-arrows
    <localtexmf>/fonts/map/dvips/old-arrows
\end{verbatim}

\item Copy the \texttt{old-arrows.sty} file into the directory
\begin{verbatim}
    <localtexmf>/tex/latex/old-arrows
\end{verbatim}
\end{enumerate}

\subsection{Updating the filename database}\label{agg-database-file}

\begin{description}[font=\bfseries\sffamily, leftmargin=5em, style=sameline]
\item[\MiKTeX] On the \textsf{General} tab of ``MiKTeX Options (Admin)'' click the \textsf{Refresh~FNDB}
    button. Alternatively, in a DOS command prompt window run
\begin{verbatim}
    initexmf --update-fndb
\end{verbatim}

\item[\TeXLive] Start the ``\TeXLive\ Manager''. From \textsf{Actions} menu, select \textsf{Update
    filename
 database}. Alternatively, run in a terminal command line
\begin{verbatim}
    mktexlsr
\end{verbatim}
\end{description}

\subsection{Updating the font map files}

\begin{description}[font=\bfseries\sffamily, leftmargin=5em, style=sameline]
\item[\MiKTeX] To update the configuration file \texttt{updmap.cfg}, execute in a DOS command prompt
\begin{verbatim}
    initexmf --edit-config-file updmap
\end{verbatim}
add to \texttt{updmap.cfg} (that will be opened) the following line
\begin{verbatim}
    Map oasy.map
\end{verbatim}
save, close and execute (always in the DOS command prompt)
\begin{verbatim}
    initexmf --mkmaps
\end{verbatim}

\item[\TeXLive] Execute in a terminal command line
\begin{verbatim}
    updmap-sys --enable Map=oasy.map
\end{verbatim}
\end{description}
Finally, it is better to make another update of the filename database (see~\ref{agg-database-file}).

\section{Usage}

\subsection{Basic usage}

Simply type in the preamble of your \LaTeX\ document
\begin{verbatim}
    \usepackage{old-arrows}
\end{verbatim}
and every arrow command will be associated to the ``old-style'', as indicated in
table~\ref{tab-frecce-vecchie}.

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \X{\leftarrow}or \verb|\gets|& \X{\longleftarrow}     & \X{\uparrow}          \\
 \X{\rightarrow}or \verb|\to|& \X{\longrightarrow}    & \X{\downarrow}        \\
 \X{\leftrightarrow}    & \X{\longleftrightarrow}& \X{\updownarrow}      \\
 \X{\mapsto}            & \X{\longmapsto}        & \X{\nearrow}          \\
 \X{\hookleftarrow}     & \X{\hookrightarrow}    & \X{\searrow}          \\
 \X{\leftharpoonup}     & \X{\rightharpoonup}    & \X{\swarrow}          \\
 \X{\leftharpoondown}   & \X{\rightharpoondown}  & \X{\nwarrow}          \\
\end{symbols}
\caption{Old-style arrows provided by \texttt{old-arrows}.}\label{tab-frecce-vecchie}
\end{table}

The commands \verb|\rightarrowfill| and \verb|\leftarrowfill| allow to fill empty spaces with extensible
arrows. For example, the first command written at the end of this paragraph gives the following result:
\rightarrowfill

\subsection{Usage together with other packages}

\subsubsection{\texttt{amsmath}}

The \texttt{old-arrows} package does not require \texttt{amsmath}. However, if you want to use the
\texttt{amsmath} package, you must load it \emph{before} \texttt{old-arrows}:
\begin{verbatim}
    \usepackage{amsmath}
    \usepackage{old-arrows}
\end{verbatim}

The \texttt{amsmath} package provides over, under (table~\ref{tab-frecce-vecchie-amsmath}), extensible
(table~\ref{tab-frecce-vecchie-ext}) arrows and operator names (table~\ref{tab-operatori-vecchi}). Note that
\texttt{amsmath} adds more space between the arrow above and the characters below, with a better
typographical result. The commands \verb|\overrightarrow{AB}| and \verb|\overleftarrow{AB}|, without
\texttt{amsmath}, produce respectively
\[
\overrightfreccia{AB} \,\text{ rather than }\, \overrightarrow{AB} \qquad \text{and} \qquad
\overleftfreccia{AB}
\,\text{ rather than }\, \overleftarrow{AB}.
\]

The \texttt{amsmath} package also provides the command \verb|\boldsymbol| for obtaining bold mathematical
symbols, which can be used together with \texttt{old-arrows}. For example, the commands
\begin{center}
    \verb|$\boldsymbol{A \to B}$| \quad and \quad \verb|$\boldsymbol{\overrightarrow{AB}}$|
\end{center}
produce $\boldsymbol{A \to B}$ and $\boldsymbol{\overrightarrow{AB}}$, respectively.

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \W{\overleftarrow}{AB} & \W{\underleftarrow}{AB}    \\
 \W{\overrightarrow}{AB} & \W{\underrightarrow}{AB}  \\
 \W{\overleftrightarrow}{AB} & \W{\underleftrightarrow}{AB} \\
\end{symbols}
\caption{Old-style over and under arrows provided by \texttt{amsmath}.}\label{tab-frecce-vecchie-amsmath}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xleftarrow}{ABCDEF} & \W{\xrightarrow}{ABCDEF}          \\
\end{symbols}
\caption{Old-style extensible arrows provided by \texttt{amsmath}.}\label{tab-frecce-vecchie-ext}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\varinjlim} & \X{\varprojlim} \\
\end{symbols}
\caption{Old-style operator names provided by \texttt{amsmath}.}\label{tab-operatori-vecchi}
\end{table}

\subsubsection{\texttt{lmodern}}

The \texttt{old-arrows} package is fully compatible with the Latin Modern fonts, provided that you load the
\texttt{lmodern} package \emph{before} \texttt{old-arrows}.
\begin{verbatim}
    \usepackage{lmodern}
    \usepackage{old-arrows}
\end{verbatim}

\subsubsection{\texttt{stmaryrd}}

The \texttt{old-arrows} package is also fully compatible with the St Mary's Road symbol font, always provided
that you load the \texttt{stmaryrd} package \emph{before} \texttt{old-arrows}.
\begin{verbatim}
    \usepackage{stmaryrd}
    \usepackage{old-arrows}
\end{verbatim}

The \texttt{stmaryrd} package provides several arrow characters with small arrowheads, like
\verb|\shortrightarrow| ($\shortrightarrow$) and \verb|\nnearrow| ($\nnearrow$). However, without
\texttt{old-arrows}, the commands \verb|\mapsfrom| and \verb|\longmapsfrom| produce the new-style arrows
$\varmapsfrom$ and $\varlongmapsfrom$. Instead, the \texttt{old-arrows} package allows you to obtain the
old-style version of these arrows, as shown in table~\ref{tab-frecce-vecchie-smr}.

\begin{table}[!htbp]
 \begin{symbols}{*2{cl}}
 \X{\mapsfrom} & \X{\longmapsfrom} \\
\end{symbols}
\caption{Old-style arrows provided by \texttt{stmaryrd}.}\label{tab-frecce-vecchie-smr}
\end{table}

%\pagebreak

\subsubsection{\texttt{mathtools}}


The \texttt{old-arrows} package can be used together with the \texttt{mathtools} package, always on condition
that you load it \emph{before} \texttt{old-arrows}.
\begin{verbatim}
    \usepackage{mathtools}
    \usepackage{old-arrows}
\end{verbatim}

The \texttt{mathtools} package makes additional extensible arrows available
(table~\ref{tab-frecce-vecchie-mt}).

\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xleftrightarrow}{ABCDEF} & \W{\xmapsto}{ABCDEF}         \\
 \W{\xhookleftarrow}{ABCDEF}  & \W{\xhookrightarrow}{ABCDEF} \\
\end{symbols}
\caption{Old-style extensible arrows provided by \texttt{mathtools}.}\label{tab-frecce-vecchie-mt}
\end{table}

Every extensible arrow can take an optional argument that produces a subscript. For example, the commands
\begin{center}
    \verb|\xrightarrow[G]{ABCDEF}| \quad and \quad \verb|\xmapsto[G]{ABCDEF}|
\end{center}
produce
\begin{center}
$\xrightarrow[G]{ABCDEF}$ \quad and \quad $\xmapsto[G]{ABCDEF}$
\end{center}

%\pagebreak

\begin{remark}
It is very important that you load \texttt{old-arrows} \emph{after} \texttt{amsmath}, \texttt{stmaryrd},
\texttt{lmodern} and \texttt{mathtools}, because many commands of these packages must be redefined by
\texttt{old-arrows}. Otherwise, \texttt{old-arrows} won't work properly.
\begin{verbatim}
    \usepackage{lmodern}
    \usepackage{amsmath}
    \usepackage{stmaryrd}
    \usepackage{mathtools}
    \usepackage{old-arrows}
\end{verbatim}
\end{remark}


\subsection{The option \textsf{new}}\label{sec:new}

Loading \texttt{old-arrows} with the option \texttt{new}
\begin{verbatim}
    \usepackage[new]{old-arrows}
\end{verbatim}
allows you to use the new-style and the old-style arrows simultaneously. In order to obtain new-style arrows,
just put \verb|\var| before every ordinary command, as shown in tables~\ref{tab-frecce-nuove},
\ref{tab-frecce-nuove-amsmath}, \ref{tab-frecce-nuove-ext}, \ref{tab-operatori-nuovi},
\ref{tab-frecce-nuove-smr} and \ref{tab-frecce-nuove-mt}.

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \X{\varleftarrow}or \verb|\vargets|& \X{\varlongleftarrow}     & \X{\varuparrow}          \\
 \X{\varrightarrow}or \verb|\varto|& \X{\varlongrightarrow}    & \X{\vardownarrow}        \\
 \X{\varleftrightarrow}    & \X{\varlongleftrightarrow}& \X{\varupdownarrow}      \\
 \X{\varmapsto}            & \X{\varlongmapsto}        & \X{\varnearrow}          \\
 \X{\varhookleftarrow}     & \X{\varhookrightarrow}    & \X{\varsearrow}          \\
 \X{\leftharpoonup}     & \X{\rightharpoonup}    & \X{\varswarrow}          \\
 \X{\leftharpoondown}   & \X{\rightharpoondown}  & \X{\varnwarrow}          \\
\end{symbols}
\caption{New-style arrows provided by option \texttt{new}.}\label{tab-frecce-nuove}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varoverleftarrow}{AB} & \W{\varunderleftarrow}{AB}    \\
 \W{\varoverrightarrow}{AB} & \W{\varunderrightarrow}{AB}  \\
 \W{\varoverleftrightarrow}{AB} & \W{\varunderleftrightarrow}{AB} \\
\end{symbols}
\caption{New-style over and under arrows provided by \texttt{amsmath} and the option \texttt{new} of
\texttt{old-arrows}.}
\label{tab-frecce-nuove-amsmath}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varxleftarrow}{ABCDEF} & \W{\varxrightarrow}{ABCDEF}          \\
\end{symbols}
\caption{New-style extensible arrows provided by \texttt{amsmath} and the option \texttt{new} of
\texttt{old-arrows}.}
\label{tab-frecce-nuove-ext}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\varvarinjlim} & \X{\varvarprojlim} \\
\end{symbols}
\caption{New-style operator names provided by \texttt{amsmath} and the option \texttt{new} of
\texttt{old-arrows}.}\label{tab-operatori-nuovi}
\end{table}
\begin{table}[!htbp]
 \begin{symbols}{*2{cl}}
 \X{\varmapsfrom} & \X{\varlongmapsfrom} \\
\end{symbols}
\caption{New-style arrows provided by \texttt{stmaryrd} and the option \texttt{new} of
\texttt{old-arrows}.}\label{tab-frecce-nuove-smr}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varxleftrightarrow}{ABCDEF} & \W{\varxmapsto}{ABCDEF}         \\
 \W{\varxhookleftarrow}{ABCDEF}  & \W{\varxhookrightarrow}{ABCDEF} \\
\end{symbols}
\caption{New-style extensible arrows provided by \texttt{mathtools} and the option \texttt{new} of
\texttt{old-arrows}.}\label{tab-frecce-nuove-mt}
\end{table}
\pagebreak

Note that the commands
\begin{center}
\verb|\leftharpoonup|, \verb|\rightharpoonup|, \verb|\leftharpoondown|, \verb|\rightharpoondown|
\end{center}
have not been redefined by \texttt{old-arrows}, because the corresponding characters
$\leftharpoonup,\rightharpoonup,\leftharpoondown,\rightharpoondown$ have not been modified by the
introduction of the new-style arrows.

The commands \verb|\varrightarrowfill| and \verb|\varleftarrowfill| allow to fill empty spaces with
extensible arrows. For example, the first command written at the end of this paragraph gives the following
result: \varrightarrowfill

If you want to use the option \texttt{new} and the option \texttt{only} provided by the \texttt{stmaryrd}
package, you must write the command you wish to define in both ordinary and \verb|\var| versions in the
option list. For example:
\begin{verbatim}
    \usepackage[only,mapsfrom,varmapsfrom]{stmaryrd}
    \usepackage[new]{old-arrows}
\end{verbatim}
says that only the symbols $\mapsfrom$ and $\varmapsfrom$ will be defined by \texttt{stmaryrd}.

Furthermore, with the option \texttt{new} it is also possibile to use the command \verb|\boldsymbol| provided
by \texttt{amsmath}. The following commands
\begin{center}
    \verb|$\boldsymbol{A \varto B}$| \quad and \quad \verb|$\boldsymbol{\varoverrightarrow{AB}}$|
\end{center}
produce $\boldsymbol{A \varto B}$ and $\boldsymbol{\varoverrightarrow{AB}}$ respectively.

\subsection{The option \textsf{old}}\label{sec:old}

If you want to use the old-style arrows only in a few cases, and maintain the new-style by default, then it
is available the option \texttt{old}
\begin{verbatim}
    \usepackage[old]{old-arrows}
\end{verbatim}
that associates all of the commands with prefix \verb|\var| to the old-style rather than the new one, which
remains associated to the ordinary commands. For example, with the option \texttt{old} the commands
\begin{center}
    \verb|$A \varleftarrow B$| \quad and \quad \verb|$A \varto B$|
\end{center}
produce $A \leftarrow B$ and $A \to B$ respectively, while
\begin{center}
    \verb|$A \leftarrow B$| \quad and \quad \verb|$A \to B$|
\end{center}
produce $A \varleftarrow B$ and $A \varto B$, respectively.

It is not possible to load the options \texttt{new} and \texttt{old} simultaneously (if so, you will get an
error message).

\subsection{Additional arrow commands provided by \texttt{old-arrows}}

The \texttt{old-arrows} package provides additional arrow commands that are listed in
table~\ref{tab-comandi-nuovi}.
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\longhookrightarrow}          & \X{\longhookleftarrow}    \\
 \X{\varlonghookrightarrow}$^a$   & \X{\varlonghookleftarrow}$^a$ \\
 \X{\longleftharpoonup}           & \X{\longleftharpoondown}  \\
 \X{\longrightharpoonup}          & \X{\longrightharpoondown} \\
\end{symbols}
\centerline{\footnotesize $^a$Available with the option \texttt{new}.}
\caption{Arrow commands provided by \texttt{old-arrows}.}\label{tab-comandi-nuovi}
\end{table}

Finally, there are extensible ``mapsfrom'' arrows (table~\ref{tab-frecce-mt-smr}) that are available only if
both \texttt{mathtools} and \texttt{stmaryrd} are loaded together with \texttt{old-arrows} (as they depend on
commands defined by these two packages).
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xmapsfrom}{ABCDEF} & \W{\varxmapsfrom}{ABCDEF}$^a$  \\
\end{symbols}
\centerline{\footnotesize $^a$Available with the option \texttt{new}.}
\caption{Extensible arrows provided by \texttt{old-arrows} together with \texttt{mathtools} and
\texttt{stmaryrd}.}
\label{tab-frecce-mt-smr}
\end{table}

\end{document}
