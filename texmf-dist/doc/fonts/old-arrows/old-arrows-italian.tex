%% start of file `old-arrows-italian.tex'.
%% Copyright 2015-2017 Riccardo Dossena (riccardo.dossena@gmail.com).
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.
%%
\documentclass[11pt,a4paper]{ltxdoc}

\usepackage[utf8]{inputenc}
\usepackage[english,italian]{babel}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[shortlabels]{enumitem}
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage[new]{old-arrows}
\usepackage[hmargin=3.3cm,vmargin=2.5cm,footskip=30pt]{geometry}
\usepackage[colorlinks=true]{hyperref}
\usepackage[all]{hypcap} % needed to help hyperlinks direct correctly;
\usepackage[chicago]{ellipsis}

\def\fileversion{2.0}
\def\filedate{5 gennaio 2017}

%Logo per MiKTeX e TeXLive
\newcommand{\MiKTeX}{MiK\TeX}
\newcommand{\TeXLive}{\mbox{\TeX\ Live}}
%
%
% Symbol Entry for Math Symbol Tables
%
\newcommand{\X}[1]{$#1$&\texttt{\string#1}\hspace*{1ex}}

\newcommand{\W}[2]{$#1{#2}$&
  \texttt{\string#1}\texttt{\string{\string#2\string}}\hspace*{1ex}}

\theoremstyle{remark}
\newtheorem*{nota}{Nota}

% Mathsymbol Table
\newsavebox{\symbbox}
\newenvironment{symbols}[1]%
{\par\vspace*{2ex}
\renewcommand{\arraystretch}{1.1}
\begin{lrbox}{\symbbox}
\hspace*{4ex}\begin{tabular}{@{}#1@{}}}%
{\end{tabular}\end{lrbox}\makebox[\textwidth]{\usebox{\symbbox}}\par\medskip}

%Per l'esempio senza amsmath
\makeatletter
\def\overrightfreccia#1{\vbox{\m@th\ialign{##\crcr
      \rightfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\overleftfreccia#1{\vbox{\m@th\ialign{##\crcr
      \leftfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}%
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\varoverrightfreccia#1{\vbox{\m@th\ialign{##\crcr
      \varrightfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\varoverleftfreccia#1{\vbox{\m@th\ialign{##\crcr
      \varleftfrecciafill\crcr\noalign{\kern-\p@\nointerlineskip}%
      $\hfil\displaystyle{#1}\hfil$\crcr}}}
\def\rightfrecciafill{$\m@th\smash\meno\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash\meno\mkern-2mu$}\hfill
  \mkern-7mu\mathord\rightarrow$}
\def\leftfrecciafill{$\m@th\mathord\leftarrow\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash\meno\mkern-2mu$}\hfill
  \mkern-7mu\smash\meno$}
\def\varrightfrecciafill{$\m@th\smash-\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash-\mkern-2mu$}\hfill
  \mkern-7mu\mathord\varrightarrow$}
\def\varleftfrecciafill{$\m@th\mathord\varleftarrow\mkern-7mu%
  \cleaders\hbox{$\mkern-2mu\smash-\mkern-2mu$}\hfill
  \mkern-7mu\smash-$}
\makeatother

\begin{document}

\title{Il pacchetto \textsf{old-arrows}}
\author{Riccardo Dossena\thanks{%
E-mail: \href{mailto:riccardo.dossena@gmail.com}{\tt riccardo.dossena@gmail.com}}}
\date{Versione \fileversion, \filedate}

\frenchspacing

\maketitle

\begin{abstract}
Questo pacchetto rende disponibili le frecce Computer Modern vecchio stile~($\rightarrow$), con una punta
più stretta, associandole agli usuali comandi \LaTeX. Può essere usato in un documento che prevede altri
tipi di frecce definite dal pacchetto \texttt{amssymb}, come $\twoheadrightarrow$, che similmente
presentano una punta stretta. È possibile usare le frecce Computer Modern nuovo stile ($\varrightarrow$)
insieme a quelle vecchio stile.
\end{abstract}

\tableofcontents

\section{Introduzione}

Nel 1992 Donald E. Knuth introdusse diverse correzioni significative ai font
\foreignlanguage{english}{Computer Modern}\footnote{Vedi
\url{http://www-cs-faculty.stanford.edu/~uno/cm.html}}. In seguito a tali aggiustamenti, i caratteri
corrispondenti alle frecce risultarono modificati. Per intenderci, da
\[
A \to B
\]
si era passati a
\[
A \varto B
\]
cioè il carattere $\to$ era stato sostituito da $\varto$, che presenta una punta più larga. Lo stesso
avvenne per altri caratteri affini. Tuttavia, molti altri caratteri definiti da \texttt{amssymb}, come
$\nrightarrow$, $\rightarrowtail$, $\twoheadrightarrow$ e altri ancora, hanno mantenuto una punta piccola e
appaiono molto dissimili da $\varto$.

Il pacchetto \texttt{old-arrows}, con la famiglia di font Old Arrows, consente di usare le frecce vecchio
stile ($\to$, $\gets$, \ldots) associandole ai comandi tradizionali (\verb|\rightarrow|, \verb|\leftarrow|,
\ldots). Inoltre, le opzioni \texttt{new} e \texttt{old} permettono di ottenere le frecce nuovo stile
($\varto$, $\vargets$, \ldots) insieme a quelle vecchio stile, semplicemente anteponendo  \verb|\var| ai
comandi corrispondenti (\verb|\varrightarrow|, \verb|\varleftarrow|, \ldots)\footnote{Vedi le sezioni
\ref{sec:new} e \ref{sec:old}.}.

La famiglia di font Old Arrows deriva da una vecchia versione dei  \foreignlanguage{english}{Blue Sky
Computer Modern Math Symbols} (1991-1992, rilasciati da AMS) tramite la cancellazione con FontForge di
diversi caratteri.

\section{Licenze}

Il codice \LaTeX\ di questo pacchetto è rilasciato sotto la \LaTeX\ Project Public License, v1.3.

I font di questo pacchetto sono rilasciati sotto la SIL Open Font License, v1.1.

\section{Installazione}

Il pacchetto \texttt{old-arrows} è contenuto nelle distribuzioni \MiKTeX\ e \TeXLive\ più recenti. Tuttavia,
se si vuole procedere all'installazione manuale, seguire le indicazioni fornite di seguito.

\subsection{Copiare i file nell'albero locale del sistema \TeX}

I file della famiglia Old Arrows sono:

\begin{center}
\begin{tabular}{p{3cm}p{3cm}p{3cm}l}
\hline
\texttt{oasy5.afm}   & \texttt{oasy5.pfm}   & \texttt{oasy5.tfm}   &  \texttt{oasy5.pfb}   \\
\texttt{oasy6.afm}   & \texttt{oasy6.pfm}   & \texttt{oasy6.tfm}   &  \texttt{oasy6.pfb}   \\
\texttt{oasy7.afm}   & \texttt{oasy7.pfm}   & \texttt{oasy7.tfm}   &  \texttt{oasy7.pfb}   \\
\texttt{oasy8.afm}   & \texttt{oasy8.pfm}   & \texttt{oasy8.tfm}   &  \texttt{oasy8.pfb}   \\
\texttt{oasy9.afm}   & \texttt{oasy9.pfm}   & \texttt{oasy9.tfm}   &  \texttt{oasy9.pfb}   \\
\texttt{oasy10.afm}  & \texttt{oasy10.pfm}  & \texttt{oasy10.tfm}  &  \texttt{oasy10.pfb}  \\
\texttt{oabsy5.afm}  & \texttt{oabsy5.pfm}  & \texttt{oabsy5.tfm}  &  \texttt{oabsy5.pfb}  \\
                     &                      & \texttt{oabsy6.tfm}  &                       \\
\texttt{oabsy7.afm}  & \texttt{oabsy7.pfm}  & \texttt{oabsy7.tfm}  &  \texttt{oabsy7.pfb}  \\
                     &                      & \texttt{oabsy8.tfm}  &                       \\
                     &                      & \texttt{oabsy9.tfm}  &                       \\
\texttt{oabsy10.afm} & \texttt{oabsy10.pfm} & \texttt{oabsy10.tfm} &  \texttt{oabsy10.pfb} \\
\hline
\end{tabular}
\end{center}

Questi file derivano dai font \texttt{cmbsy5}, \texttt{cmbsy7}, \texttt{cmbsy10}, \texttt{cmsy5},
\texttt{cmsy7}, \texttt{cmsy8}, \texttt{cmsy9} e \texttt{cmsy10} della famiglia Computer Modern.

Indichiamo con \texttt{<localtexmf>} la radice dell'albero locale del proprio sistema \TeX. Nelle
distribuzioni \TeXLive\ si trova in genere in \texttt{/usr/local/texlive/texmf-local}; nella distribuzione
\MiKTeX\ può essere invece impostata in qualsiasi directory, attraverso la scheda \textsf{Roots} delle
``\MiKTeX\ Options''.

\begin{enumerate}
\item Copiare i file \texttt{*.afm} e \texttt{*.tfm} nelle corrispondenti directory \texttt{old-arrows}
    (che devono essere create, come mostrato di seguito):
    \begin{verbatim}
    <localtexmf>/fonts/afm/old-arrows
    <localtexmf>/fonts/tfm/old-arrows
    \end{verbatim}

\item Copiare i file \texttt{*.pfb} e \texttt{*.pfm} nella directory
    \begin{verbatim}
    <localtexmf>/fonts/type1/old-arrows
    \end{verbatim}

\item Copiare i file \texttt{oasy.enc} e \texttt{oasy.map}, rispettivamente, nelle directory
    \begin{verbatim}
    <localtexmf>/fonts/enc/dvips/old-arrows
    <localtexmf>/fonts/map/dvips/old-arrows
    \end{verbatim}

\item Copiare il file \texttt{old-arrows.sty} nella directory
    \begin{verbatim}
    <localtexmf>/tex/latex/old-arrows
    \end{verbatim}
\end{enumerate}

\subsection{Aggiornare il database dei nomi dei file}\label{agg-database-file}

\begin{description}[font=\bfseries\sffamily, leftmargin=5em, style=sameline]
\item[\MiKTeX] Nella scheda \textsf{General} delle ``MiKTeX Options (Admin)'' cliccare sul pulsante
    \textsf{Refresh~FNDB}. In alternativa, eseguire dalla linea di comando DOS
    \begin{verbatim}
    initexmf --update-fndb
    \end{verbatim}

\item[\TeXLive] Avviare il ``\TeXLive\ Manager''. Dal menu \textsf{Actions} selezionare
    \foreignlanguage{english}{\textsf{Update filename database}}. In alternativa, eseguire dalla linea di
    comando di un terminale
    \begin{verbatim}
    mktexlsr
    \end{verbatim}
\end{description}

\subsection{Aggiornare i font map file}

\begin{description}[font=\bfseries\sffamily, leftmargin=5em, style=sameline]
\item[\MiKTeX]
Per aggiornare il file di configurazione \texttt{updmap.cfg}, eseguire dalla linea di comando DOS
\begin{verbatim}
    initexmf --edit-config-file updmap
\end{verbatim}
aggiungere al file \texttt{updmap.cfg} (che verrà aperto) la seguente linea
\begin{verbatim}
    Map oasy.map
\end{verbatim}
salvare, chiudere ed eseguire (sempre dalla linea di comando DOS)
\begin{verbatim}
    initexmf --mkmaps
\end{verbatim}

\item[\TeXLive]
Eseguire dalla linea di comando del terminale
\begin{verbatim}
    updmap-sys --enable Map=oasy.map
\end{verbatim}
\end{description}
È possibile che a questo punto si renda necessario un nuovo aggiornamento del database dei nomi dei file
(vedi~\ref{agg-database-file}). Eseguirlo in ogni caso per sicurezza.

\section{Come usare il pacchetto \textsf{old-arrows}}

\subsection{Uso di base}

È sufficiente scrivere nel preambolo del proprio documento \LaTeX\
\begin{verbatim}
    \usepackage{old-arrows}
\end{verbatim}
e tutti i comandi relativi alle frecce verranno associati al ``vecchio stile'', come riportato nella
tabella~\ref{tab-frecce-vecchie}.

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \X{\leftarrow}o \verb|\gets|& \X{\longleftarrow}     & \X{\uparrow}          \\
 \X{\rightarrow}o \verb|\to| & \X{\longrightarrow}    & \X{\downarrow}        \\
 \X{\leftrightarrow}         & \X{\longleftrightarrow}& \X{\updownarrow}      \\
 \X{\mapsto}                 & \X{\longmapsto}        & \X{\nearrow}          \\
 \X{\hookleftarrow}          & \X{\hookrightarrow}    & \X{\searrow}          \\
 \X{\leftharpoonup}          & \X{\rightharpoonup}    & \X{\swarrow}          \\
 \X{\leftharpoondown}        & \X{\rightharpoondown}  & \X{\nwarrow}          \\
\end{symbols}
\caption{Frecce vecchio stile, disponibili con \texttt{old-arrows}.}\label{tab-frecce-vecchie}
\end{table}

I comandi \verb|\rightarrowfill| e \verb|\leftarrowfill| consentono di riempire spazi vuoti con frecce
estendibili. Per esempio, il primo comando dato alla fine di questo stesso paragrafo produce:
\rightarrowfill

\subsection{Uso con altri pacchetti}

\subsubsection{\texttt{amsmath}}

Il pacchetto \texttt{old-arrows} non richiede \texttt{amsmath}. In ogni caso, per usarlo insieme ad
\texttt{amsmath}, quest'ultimo deve essere caricato \emph{prima} di \texttt{old-arrows}:
\begin{verbatim}
    \usepackage{amsmath}
    \usepackage{old-arrows}
\end{verbatim}

Il pacchetto \texttt{amsmath} rende disponibili ulteriori soprassegni, sottosegni
(tabella~\ref{tab-frecce-vecchie-amsmath}), frecce estendibili (tabella~\ref{tab-frecce-vecchie-ext}) e nomi
di operatori (tabella~\ref{tab-operatori-vecchi}). Da notare che \texttt{amsmath} produce soprassegni
leggermente più distanziati dai caratteri sottostanti, con una resa tipografica migliore. Infatti, i comandi
\verb|\overrightarrow{AB}| e \verb|\overleftarrow{AB}|, senza \texttt{amsmath}, producono rispettivamente
\[
\overrightfreccia{AB} \,\text{ anziché }\, \overrightarrow{AB} \qquad \text{e} \qquad \overleftfreccia{AB}
\,\text{ anziché }\, \overleftarrow{AB}.
\]

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \W{\overleftarrow}{AB} & \W{\underleftarrow}{AB}    \\
 \W{\overrightarrow}{AB} & \W{\underrightarrow}{AB}  \\
 \W{\overleftrightarrow}{AB} & \W{\underleftrightarrow}{AB} \\
\end{symbols}
\caption{Frecce vecchio stile come soprassegni e sottosegni, attivabili
\texttt{amsmath}.}\label{tab-frecce-vecchie-amsmath}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xleftarrow}{ABCDEF} & \W{\xrightarrow}{ABCDEF}          \\
\end{symbols}
\caption{Frecce vecchio stile estendibili, attivabili con \texttt{amsmath}.}\label{tab-frecce-vecchie-ext}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\varinjlim} & \X{\varprojlim} \\
\end{symbols}
\caption{Nomi di operatori vecchio stile, attivabili con \texttt{amsmath}.}\label{tab-operatori-vecchi}
\end{table}

Il pacchetto \texttt{amsmath} fornisce il comando \verb|\boldsymbol| per ottenere i caratteri
matematici in grassetto. Questo può essere anche utilizzato con \texttt{old-arrows}. Per esempio, i comandi
\begin{center}
    \verb|$\boldsymbol{A \to B}$| \quad e \quad \verb|$\boldsymbol{\overrightarrow{AB}}$|
\end{center}
producono rispettivamente $\boldsymbol{A \to B}$ e $\boldsymbol{\overrightarrow{AB}}$.

\subsubsection{\texttt{lmodern}}

Il pacchetto \texttt{old-arrows} è completamente compatibile con i font Latin Modern, sempre a condizione che
il relativo pacchetto \texttt{lmodern} venga caricato \emph{prima} di \texttt{old-arrows}:
\begin{verbatim}
    \usepackage{lmodern}
    \usepackage{old-arrows}
\end{verbatim}

\subsubsection{\texttt{stmaryrd}}

Il pacchetto \texttt{old-arrows} è anche pienamente compatible con i font St Mary's Road, ancora a condizione
di caricare il pacchetto \texttt{stmaryrd} \emph{prima} di \texttt{old-arrows}.
\begin{verbatim}
    \usepackage{stmaryrd}
    \usepackage{old-arrows}
\end{verbatim}

Il pacchetto \texttt{stmaryrd} fornisce diverse frecce con punta stretta, come ad esempio
\verb|\shortrightarrow| ($\shortrightarrow$) e \verb|\nnearrow| ($\nnearrow$). Tuttavia, i comandi
\verb|\mapsfrom| e \verb|\longmapsfrom|, senza \texttt{old-arrows}, generano le frecce nuovo stile
$\varmapsfrom$ e $\varlongmapsfrom$. Il pacchetto \texttt{old-arrows} permette invece di ottenere la versione
vecchio stile di tali frecce, come mostra la tabella~\ref{tab-frecce-vecchie-smr}.
\begin{table}[!htbp]
 \begin{symbols}{*2{cl}}
 \X{\mapsfrom} & \X{\longmapsfrom} \\
\end{symbols}
\caption{Frecce vecchio stile disponibili con \texttt{stmaryrd}.}\label{tab-frecce-vecchie-smr}
\end{table}

\subsubsection{\texttt{mathtools}}

Il pacchetto \texttt{old-arrows} può essere usato congiuntamente al pacchetto \texttt{mathtools}, purché
quest'ultimo venga caricato \emph{prima} di \texttt{old-arrows}.
\begin{verbatim}
    \usepackage{mathtools}
    \usepackage{old-arrows}
\end{verbatim}

Il pacchetto \texttt{mathtools} mette a disposizione ulteriori frecce estendibili
(tabella~\ref{tab-frecce-vecchie-mt}).

\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xleftrightarrow}{ABCDEF} & \W{\xmapsto}{ABCDEF}         \\
 \W{\xhookleftarrow}{ABCDEF}  & \W{\xhookrightarrow}{ABCDEF} \\
\end{symbols}
\caption{Frecce vecchio stile estendibili disponibili con \texttt{mathtools}.}\label{tab-frecce-vecchie-mt}
\end{table}

Ogni freccia estendibile può ricevere un argomento facoltativo che produce un pedice. Per esempio, i comandi
\begin{center}
    \verb|\xrightarrow[G]{ABCDEF}| \quad e \quad \verb|\xmapsto[G]{ABCDEF}|
\end{center}
danno come risultato
\begin{center}
$\xrightarrow[G]{ABCDEF}$ \quad e \quad $\xmapsto[G]{ABCDEF}$
\end{center}

\begin{nota}
È molto importante che \texttt{old-arrows} venga caricato \emph{dopo} \texttt{amsmath}, \texttt{stmaryrd},
\texttt{lmodern} e \texttt{mathtools}, dato che \texttt{old-arrows} ridefinisce diversi comandi di questi
pacchetti. In caso contrario, \texttt{old-arrows} non funzionerà correttamente.
\begin{verbatim}
    \usepackage{lmodern}
    \usepackage{amsmath}
    \usepackage{stmaryrd}
    \usepackage{mathtools}
    \usepackage{old-arrows}
\end{verbatim}
\end{nota}

\subsection{L'opzione \textsf{new}}\label{sec:new}

Caricando \texttt{old-arrows} con l'opzione \texttt{new}
\begin{verbatim}
    \usepackage[new]{old-arrows}
\end{verbatim}
vengono rese disponibili le frecce nuovo stile contemporaneamente a quelle vecchio stile. Per ottenere le
frecce nuovo stile basta anteporre \verb|\var| a tutti i comandi ordinari, come mostrano le
tabelle~\ref{tab-frecce-nuove}, \ref{tab-frecce-nuove-amsmath}, \ref{tab-frecce-nuove-ext},
\ref{tab-operatori-nuovi}, \ref{tab-frecce-nuove-smr} e \ref{tab-frecce-nuove-mt}.

\begin{table}[!htbp]
\begin{symbols}{*3{cl}}
 \X{\varleftarrow}o \verb|\vargets|& \X{\varlongleftarrow}     & \X{\varuparrow}          \\
 \X{\varrightarrow}o \verb|\varto|& \X{\varlongrightarrow}    & \X{\vardownarrow}        \\
 \X{\varleftrightarrow}    & \X{\varlongleftrightarrow}& \X{\varupdownarrow}      \\
 \X{\varmapsto}            & \X{\varlongmapsto}        & \X{\varnearrow}          \\
 \X{\varhookleftarrow}     & \X{\varhookrightarrow}    & \X{\varsearrow}          \\
 \X{\leftharpoonup}     & \X{\rightharpoonup}    & \X{\varswarrow}          \\
 \X{\leftharpoondown}   & \X{\rightharpoondown}  & \X{\varnwarrow}          \\
\end{symbols}
\caption{Frecce nuovo stile, attivabili con l'opzione \texttt{new}.}\label{tab-frecce-nuove}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varoverleftarrow}{AB} & \W{\varunderleftarrow}{AB}    \\
 \W{\varoverrightarrow}{AB} & \W{\varunderrightarrow}{AB}  \\
 \W{\varoverleftrightarrow}{AB} & \W{\varunderleftrightarrow}{AB} \\
\end{symbols}
\caption{Frecce nuovo stile come soprassegni e sottosegni, attivabili con \texttt{amsmath} e l'opzione
\texttt{new} di \texttt{old-arrows}.}
\label{tab-frecce-nuove-amsmath}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varxleftarrow}{ABCDEF} & \W{\varxrightarrow}{ABCDEF}          \\
\end{symbols}
\caption{Frecce nuovo stile estendibili, attivabili con \texttt{amsmath} e l'opzione \texttt{new} di
\texttt{old-arrows}.}
\label{tab-frecce-nuove-ext}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\varvarinjlim} & \X{\varvarprojlim} \\
\end{symbols}
\caption{Nomi di operatori nuovo stile, attivabili con \texttt{amsmath} e l'opzione \texttt{new} di
\texttt{old-arrows}.}\label{tab-operatori-nuovi}
\end{table}
\begin{table}[!htbp]
 \begin{symbols}{*2{cl}}
 \X{\varmapsfrom} & \X{\varlongmapsfrom} \\
\end{symbols}
\caption{Frecce nuovo stile disponibili con \texttt{stmaryrd} e l'opzione \texttt{new} di
\texttt{old-arrows}.}\label{tab-frecce-nuove-smr}
\end{table}
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\varxleftrightarrow}{ABCDEF} & \W{\varxmapsto}{ABCDEF}         \\
 \W{\varxhookleftarrow}{ABCDEF}  & \W{\varxhookrightarrow}{ABCDEF} \\
\end{symbols}
\caption{Frecce nuovo stile estendibili, disponibili con \texttt{mathtools} e l'opzione \texttt{new} di
\texttt{old-arrows}.}\label{tab-frecce-nuove-mt}
\end{table}

\pagebreak

Segnaliamo che i comandi
\begin{center}
\verb|\leftharpoonup|, \verb|\rightharpoonup|, \verb|\leftharpoondown|, \verb|\rightharpoondown|
\end{center}
non sono stati ridefiniti  da \texttt{old-arrows}, perché i corrispondenti caratteri $\leftharpoonup,
\rightharpoonup,\leftharpoondown,\rightharpoondown$ non sono stati modificati con l'introduzione del nuovo
stile.

I comandi \verb|\varrightarrowfill| e \verb|\varleftarrowfill| permettono di riempire spazi vuoti con frecce
estendibili. Il primo comando dato alla fine di questo stesso paragrafo produce: \varrightarrowfill

Se si vuole usare l'opzione \texttt{new} insieme all'opzione \texttt{only} del pacchetto \texttt{stmaryrd}, è
necessario scrivere nell'elenco delle opzioni il comando da definire in entrambe le versioni: normale e
\verb|\var|. Ad esempio, con le opzioni seguenti
\begin{verbatim}
    \usepackage[only,mapsfrom,varmapsfrom]{stmaryrd}
    \usepackage[new]{old-arrows}
\end{verbatim}
verranno definiti da \texttt{stmaryrd} solo i simboli $\mapsfrom$ e $\varmapsfrom$.

Infine, anche con l'opzione \texttt{new} è possibile usare il comando \verb|\boldsymbol| previsto da
\texttt{amsmath}. I seguenti comandi
\begin{center}
    \verb|$\boldsymbol{A \varto B}$| \quad e \quad \verb|$\boldsymbol{\varoverrightarrow{AB}}$|
\end{center}
producono rispettivamente $\boldsymbol{A \varto B}$ e $\boldsymbol{\varoverrightarrow{AB}}$.

\subsection{L'opzione \textsf{old}}\label{sec:old}

Qualora si volesse usare il vecchio stile solo in alcuni casi, mantenendo di default il nuovo stile, è
disponibile l'opzione \texttt{old}
\begin{verbatim}
    \usepackage[old]{old-arrows}
\end{verbatim}
che associa a tutti i comandi con prefisso \verb|\var| il vecchio stile anziché il nuovo, il quale rimane
associato ai comandi ordinari. Per esempio, con l'opzione \texttt{old} i comandi
\begin{center}
    \verb|$A \varleftarrow B$| \quad e \quad \verb|$A \varto B$|
\end{center}
producono rispettivamente $A \leftarrow B$ e $A \to B$, mentre
\begin{center}
    \verb|$A \leftarrow B$| \quad e \quad \verb|$A \to B$|
\end{center}
producono rispettivamente $A \varleftarrow B$ e $A \varto B$.

Non è possibile caricare contemporaneamente le opzioni \texttt{new} e \texttt{old} (nel caso, si otterrà un
messaggio di errore).

\subsection{Comandi aggiuntivi disponibili con \texttt{old-arrows}}

Il pacchetto \texttt{old-arrows} fornisce comandi aggiuntivi per nuovi tipi di frecce, come mostra la
tabella~\ref{tab-comandi-nuovi}.
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \X{\longhookrightarrow}          & \X{\longhookleftarrow}    \\
 \X{\varlonghookrightarrow}$^a$   & \X{\varlonghookleftarrow}$^a$ \\
 \X{\longleftharpoonup}           & \X{\longleftharpoondown}  \\
 \X{\longrightharpoonup}          & \X{\longrightharpoondown} \\
\end{symbols}
\centerline{\footnotesize $^a$Attivabile con l'opzione \texttt{new}.}
\caption{Frecce aggiuntive disponibili con \texttt{old-arrows}.}\label{tab-comandi-nuovi}
\end{table}

Infine, sono previsti comandi per frecce estendibili del tipo ``mapsfrom'' (tabella~\ref{tab-frecce-mt-smr})
che sono disponibili solo se anche \texttt{mathtools} e \texttt{stmaryrd} vengono caricati con
\texttt{old-arrows} (in quanto dipendono a loro volta da altri comandi definiti da questi due pacchetti).
\begin{table}[!htbp]
\begin{symbols}{*2{cl}}
 \W{\xmapsfrom}{ABCDEF} & \W{\varxmapsfrom}{ABCDEF}$^a$  \\
\end{symbols}
\centerline{\footnotesize $^a$Attivabile con l'opzione \texttt{new}.}
\caption{Frecce estendibili fornite da \texttt{old-arrows} insieme a \texttt{mathtools} e
\texttt{stmaryrd}.}
\label{tab-frecce-mt-smr}
\end{table}

\end{document}
