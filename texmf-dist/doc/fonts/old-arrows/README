The old-arrows package v2.0 2017/01/05
--------------------------------------------------------------------------
Riccardo Dossena
Email: riccardo.dossena@gmail.com
--------------------------------------------------------------------------
copyright 2015-2017 by Riccardo Dossena

This package provides cm old-style arrows with smaller arrowheads,
associated with ordinary LaTeX commands. It can be used in a document
that contains other amssymb arrow characters, like \twoheadrightarrow,
which also have small arrowheads. The options `new' and `old' allow the
user to use the usual new-style cm arrows together with the old-style 
ones. The option `new' allows you to maintain the old-style by default 
and get the old-style by putting \var before every command; conversely, 
the option `old' allows you to maintain the new-style by default and get 
the old-style by putting \var before every command.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
   http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Riccardo Dossena.

Old Arrows fonts were derived from an old version of
Blue Sky Computer Modern Math Symbols (1991-1992, released by AMS)
by deleting many characters with FontForge.
Old Arrows Font Software is licensed under the SIL Open
Font License, Version 1.1.
--------------------------------------------------------------------------
If you have any ideas, questions, suggestions, feedback or bugs to
report, please feel free to contact me.
--------------------------------------------------------------------------
