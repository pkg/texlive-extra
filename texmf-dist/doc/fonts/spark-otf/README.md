# README #
Package spark-otf supports the spark fonts.
The fonts are distributed under the SIL Open Font License.
(https://github.com/aftertheflood/sparks)


Following font files are supported:

- Sparks-Bar-???.otf
- Sparks-Dotline-???.otf
- Sparks-Dot-???.otf


%% This file is distributed under the terms of the LaTeX Project Public
%% License from CTAN archives in directory  macros/latex/base/lppl.txt.
%% Either version 1.3 or, at your option, any later version.
%% The OpenType fonts are distributed under the SIL Open Font License


hvoss@tug.org