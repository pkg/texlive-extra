Euler-Math package
====================

## Description

'Euler-Math.otf' (formerly named 'Neo-Euler.otf') is an OpenType version of
Hermann Zapf’s Euler maths font. It is the continuation of the Euler project
initiated by Khaled Hosny in 2009 and abandoned in 2016.  
A style file 'euler-math.sty' is provided as a replacement of the
'eulervm.sty' package for LuaLaTeX and XeLaTeX users.

## Contents

* Euler-Math.otf     OpenType maths font
* euler-math.sty     LaTeX style file: replaces eulervm.sty for LuaTeX/XeTeX
* neo-euler.sty      For compatibility with the former name
* Euler-Math.pdf     Documentation in PDF format
* Euler-Math.ltx     LaTeX source of Euler-Math.pdf
* unimath-euler.pdf Modified version of unimath-symbols.pdf
                    showing available Euler-Math symbols compared to
		            LatinModern, STIXTwo, Erewhon, TeXGyrePagella and Asana.
* unimath-euler.ltx LaTeX source of unimath-concrete.pdf
* README.md         (this file)

## Installation

This package is meant to be installed automatically by TeXLive, MikTeX, etc.
Otherwise, the package can be installed under TEXMFHOME or TEXMFLOCAL, f.i.
Euler-Math.otf in directory  texmf-local/fonts/opentype/public/euler-math/
and euler-math.sty in directory  texmf-local/tex/latex/euler-math/.  
Documentation files and their sources can go to directory
texmf-local/doc/fonts/public/euler-math/

Don't forget to rebuild the file database (mktexlsr or so) if you install
under TEXMFLOCAL.

Finally, make the system font database aware of the Euler-Math font
(fontconfig under Linux).

## License

* The font 'Euler-Math.otf' is licensed under the SIL Open Font License,
Version 1.1. This license is available with a FAQ at:
http://scripts.sil.org/OFL
* The other files are distributed under the terms of the LaTeX Project
Public License from CTAN archives in directory macros/latex/base/lppl.txt.
Either version 1.3 or, at your option, any later version.

## Changes

* First public version: 0.20

* v0.21:
	 - Delimiters, integrals, sum, prod etc. are now vertically
       centred by design on the maths axis (required by luametatex).
* v0.22:
     - Sizes of vertical variants of \lAngle, \rAngle, \langle and
       \rangle corrected.
	 - Added extensible integral for U+222B (usable with luametatex).
* v0.30:
     - *Name of the package changed from Neo-Euler to Euler-Math*;  
	 the former 'neo-euler.sty' style file has been kept for compatibility,
	 it now loads 'euler-math.sty' and warns about the change.
  
---
Copyright 2009-2016  Khaled Hosny  
Copyright 2022-      Daniel Flipo  
E-mail: daniel (dot) flipo (at) free (dot) fr
