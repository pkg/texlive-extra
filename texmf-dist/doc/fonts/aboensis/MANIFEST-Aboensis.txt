Font: Aboensis
Design: Tommi Syrjänen
Author: Tommi Syrjänen
Version: 1.0
Date: 26 VI 2021

Copyright (c) 2021 Tommi Syrjänen

The font Aboensis is released under the SIL Open Font License 1.1
(OFL) with Reserved Font Name Aboensis. See file OFL.txt for details.
The main documentation and style files are released under the Latex
Project Public License (LPPL) version 1.3c. See file lppl.txt for
details. 

This work has the LPPL maintenance status "maintained".
The Current Maintainer of this work is Tommi Syrjänen.
This work consists of the files listed in this file. 

1. The actual OpenType/PS font file:
 - Aboensis-Regular.otf

2. Documentation files:
 - aboensis.pdf  - the users' manual
 - README        - a short description

3. Font source code:
 - Aboensis.glyphs - the Glyphs 3.3 source of the font

4. XeLaTeX style file:
 - aboensis.sty

5. Documentation source files:
 - doc/aboensis.tex              - LaTeX source for manual
 - doc/aboensis.bib              - the bibliography file
 - doc/ab_symbols.tex            - list of symbols in the font
 - doc/Makefile                  - driver for document generation
 - doc/pics/DF1423.jpg           - image file for documentation
 - doc/pics/DF3001_fol2r.jpg     - image file for documentation
 - doc/pics/REA-001r.jpg         - image file for documentation
 - doc/pics/REA-001r2.jpg        - image file for documentation
 - doc/pics/SDHK20000.jpg        - image file for documentation
 - doc/pics/SDHK27022.jpg        - image file for documentation
 - doc/pics/aboensis-18v.png     - image file for documentation
 - doc/pics/aboensis-27r.jpg     - image file for documentation
 - doc/pics/aboensis-27v.jpg     - image file for documentation
 - doc/pics/aboensis-30v.jpg     - image file for documentation
 - doc/pics/aboensis-37v.jpg     - image file for documentation
 - doc/pics/aboensis-38v.jpg     - image file for documentation
 - doc/pics/aboensis-39r.jpg     - image file for documentation
 - doc/pics/aboensis-39v.jpg     - image file for documentation
 - doc/pics/aboensis-78v.png     - image file for documentation
 - doc/pics/aboensis-e14v.jpg    - image file for documentation
 - doc/pics/budde-9v.png         - image file for documentation
 - doc/pics/fields.png           - image file for documentation
 - doc/pics/kalliala_fol16r.jpg  - image file for documentation
 - doc/pics/kununx_balk.jpg      - image file for documentation
 - doc/pics/photoshop-alternatives.png - image file for documentation
 - doc/pics/photoshop-one.png    - image file for documentation
 - doc/pics/photoshop-two.png    - image file for documentation
 - doc/pics/puupuntari.png       - image file for documentation
 - doc/pics/rea-A.jpg            - image file for documentation
 - doc/pics/rea-B.jpg            - image file for documentation
 - doc/pics/rea-C.jpg            - image file for documentation
 - doc/pics/rea-D.jpg            - image file for documentation
 - doc/pics/rea-E.jpg            - image file for documentation
 - doc/pics/rea-F.jpg            - image file for documentation
 - doc/pics/rea-G.jpg            - image file for documentation
 - doc/pics/rea-H.jpg            - image file for documentation
 - doc/pics/rea-I.jpg            - image file for documentation
 - doc/pics/rea-J.jpg            - image file for documentation
 - doc/pics/rea-K.jpg            - image file for documentation
 - doc/pics/rea-L.jpg            - image file for documentation
 - doc/pics/rea-M.jpg            - image file for documentation
 - doc/pics/rea-N.jpg            - image file for documentation
 - doc/pics/rea-O.jpg            - image file for documentation
 - doc/pics/rea-P.jpg            - image file for documentation
 - doc/pics/rea-Q.jpg            - image file for documentation
 - doc/pics/rea-R.jpg            - image file for documentation
 - doc/pics/rea-S.jpg            - image file for documentation
 - doc/pics/rea-T.jpg            - image file for documentation
 - doc/pics/rea-U.jpg            - image file for documentation
 - doc/pics/rea-V.jpg            - image file for documentation
 - doc/pics/rea-W.jpg            - image file for documentation
 - doc/pics/rea-Y.jpg            - image file for documentation
 - doc/pics/viipuri.jpg          - image file for documentation

6. Licenses
 - OFL.txt                   - the SIL Open Font License text
 - lppl.txt                  - the Latex Project Public License 1.3c 
 - ccby4.txt                 - the CC BY 4.0 license text 


If you create derived work from this work, please rename the files
that contain the word 'aboensis' in their filenames. 

The Condition 3 of the Latex Project Public License allows you to
distribute a Compiled Work version of this work. Such a version should
contain the files Aboensis-Regular.otf, aboensis.pdf, and optionally
aboensisi.sty if the distribution is intended to be used with the
XeLaTeX system. Strictly speaking only Aboensis-Regular.otf is
absolutely necessary but the font is complex and non-standard enough
that without the user's manual it is difficult to get all use out of
it. Including the license files is a good idea, too.
