This small package provides a math package that amounts to modifications of the STIX sans serif Roman and Greek letters with most symbols taken from newtxmath which must of course be installed (version 1.5 or higher) and its map file enabled. The best use of this package is for making slides for projection at reasonably good resolution. The four font files in this package are in pfb format, licensed under the SIL OFL, version 1.1. All other LaTeX support files are subject to the LaTeX Project Public License. See 
http://www.ctan.org/license/lppl1.3
for the details of that license. 

Current version: 1.054 2020-05-16

Changes in version 1.054
Modified the metric setting affexting \Big, \bigg and \Bigg so that they work as expected.
(Thanks Yaosheng Chen for catching this problem.)


Changes in version 1.053
Added some newer features of newtxmath that were not included in prior versions of newtxsf. This package now has adaptive vector accents as well as special symbols for transpose and hermitian transpose. See the documentation of newtx for usage details.

Changes in version 1.052
Corrected newtxsf.sty to point to correct locations for dot and related accents. (Thanks Fengnan Gao.)

Changes in version 1.051
1. Corrected the declaration of the brace extension glyph, so that a number of horizontally extendable math constructions now work properly.
2. Updated documentation.

Changes in version 1.05
1. Brought newtxsf into conformance with newtxmath 1.5. New options upint (for an upright integral) smallerops (for smaller large operators). 
2. Corrected an error which was leading to incomplete horizontal extendible delimiters.

Changes in version 1.04
Changes to definitions of \widehat and \widetilde in newtxmath.sty so they bahave as expected in nested accents.

Changes in version 1.03
1. Small corrections to accent positions.
2. Changed newtxsf.sty to add definitions of \nabla, \nablaup, \varkappa, \varkappaup, and to change most Greek letters to \mathalpha, where appropriate.
3. The smallest size of parentheses now comes from the text font.

Changes in version 1.02
1. Corrected definitions of \textsquare and \openbox in newtxsf.sty. (Thanks, Jean-François Burnol.)

Changes in version 1.01
Changed AMSa and AMSb to their replacements in newtx 1.43. This should not affect output, but may make more efficient use of math families.

Please send comments and bug reports or suggestions for improvement to

msharpe at ucsd dot edu