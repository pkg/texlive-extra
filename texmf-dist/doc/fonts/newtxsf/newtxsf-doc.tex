% !TEX TS-program = pdflatex
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
\pdfmapfile{+newtxsf.map}
%SetFonts
% FiraSans+newtxsf
\usepackage[sfdefault,scaled=.85]{FiraSans} % use sb in place of bold
%\let\rmdefault\sfdefault
\usepackage[T1]{fontenc} 
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata
\usepackage{amsmath,amsthm}
\usepackage[cmintegrals]{newtxsf}
%\usepackage[scr=boondoxo]{mathalfa}% less slanted than STIX cal
\usepackage{bm}
%SetFonts
\title{\protect\texttt{newtxsf}---A Sans Serif Math Package}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date
\begin{document}
\maketitle
\section{Introduction}
I was spurred to make this package by one of the excellent features of the STIX fonts---mathematical Roman and Greek sans serif alphabets with all glyphs unmistakable, unlike most sans serif fonts. These are sized to match Times, with xheights near 440{\tt em}, rather smaller than most sans serif fonts. So, this package was constructed by replacing the existing math italic in {\tt newtxmath} with the sans serif glyphs from STIX and simplifying the options.  The result is a math package that blends well with sans serif text fonts with an xheight similar to that of  Times. Surprisingly, it does seem to work well with some other sans serif fonts with larger xheights, such as \emph{FiraSans} at about 530{\tt em}. With modern-day projectors having considerably higher resolutions than a few years ago, large xheight may be less of a concern than it once was. (Note: \emph{FiraSans} also works well with the \emph{Arev Sans} package, where the xheight is larger.)

\section{Package Options}
The options are much simpler than in {\tt newtxmath}, as I have ruled out a number which, in my opinion, were of interest only for backward compatibility. These are the options you may specify.
\begin{itemize}
\item
{\tt scaled} allows you to scale all fonts in this math package to match a chosen text font family.
\item {\tt nosymbolsc} saves you a math group of mostly rather obscure symbols.
\item {\tt cmintegrals} makes use of integrals drawn from Computer Modern rather than the more upright, but unattractive, {\tt txfonts} integrals. (This option no longer has any effect since {\tt newtx} now uses a similar collection of integral signs.)
\item {\tt upint} produces upright integral signs. (The default shape is slanted.)
\item {\tt noamssymbols} saves you one or two math groups (the AMS symbols) if you have no need of them. If you use an AMS document class, like {\tt amsart}, be sure to specify {\tt noamsfonts} among its options, if using a recent version of the AMS class. 
\item {\tt amssymbols} does the opposite---this is the default.
\item {\tt uprightGreek} specifies the use of upright rather than slanted Greek symbols for uppercase only.
\item {\tt slantedGreek} does the opposite.
\item {\tt frenchmath} forces uppercase and lowercase Greek to upright shape and makes uppercase math Roman letters render in upright rather than slanted shape.
\end{itemize}
\section{Package features}
There is little difference between the symbols available using this package and those available in {\tt newtxmath}, taking into account the reduced set of options, and the latter are adequately described in the documentation for {\tt newtx}. The differences are as follows.
\begin{itemize}
\item
Unlike {\tt newtxmath}, {\tt newtxsf} defines its own figures and makes no use of (lining) figures from the text package, but does use the text font for names of operators such as \verb|\sin|. For this reason, you need to match xheights of the text and math fonts at least approximately.
\item The only blackboard bold fonts built into {\tt newtxsf} is an expanded version of those from the STIX package. For example, in math mode, \verb|\mathbb{012ABC}| produces $\mathbb{012ABC}$, and this works also in bold math.
\item All math accents are taken from {\tt newtxsf} rather than the text package.
\item The option {\tt bigdelims} from {\tt newtxmath} is no longer needed---it is applied automatically in {\tt newtxsf}.
\item \verb|\forall|, \verb|\exists|, \verb|\nexists| and \verb|\emptyset| have been \verb|\let| to alternate versions which look better, in my opinion: $\forall$, $\exists$, $\nexists$, $\emptyset$.
\item The math fonts now have upright versions of \verb|\imath| and \verb|\jmath| (i.e., \verb|\dotlessi| and \verb|\dotlessj|) named \verb|\upimath| and \verb|\upjmath|. Bold versions are available as well. For example, $x{\bm{\vec\upimath}}+y{\bm{\vec\upjmath}}$, $2\hat\upimath+3\hat\upjmath$.
\end{itemize}

\section{Package examples}
In this documentation, the text font is \emph{FiraSans} scaled down to  more closely match \emph{newtxsf}. (As \emph{FiraSans} has an xheight about 17\% greater than \emph{newtxsf}, some scaling is important.)
It uses the following preamble.
\begin{verbatim}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage[T1]{fontenc} 
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata typewriter
\usepackage{amsmath,amsthm}
\usepackage[cmintegrals]{newtxsf}
\end{verbatim}

Here's a piece math to illustrate mathematical text---note the use of upright forms of $\uppi$ and $\mathrm{d}$, following the ISO rules.

\textbf{Simplest form of the \textit{Central Limit Theorem}:} \textit{Let
$X_1$, $X_2,\cdots$ be a sequence of iid random variables with mean $0$ 
and variance $1$ on a probability space $(\Omega,\mathcal{F},\mathbb{P})$. Then}
\[\mathbb{P}\left(\frac{X_1+\cdots+X_n}{\sqrt{n}}\le y\right)\to\mathfrak{N}(y)\coloneq
\int_{-\infty}^y \frac{\mathrm{e}^{-t^2/2}}{\sqrt{2\uppi}}\,
\mathrm{d}t\quad\mbox{as $n\to\infty$,}\]
\textit{or, equivalently, letting} $S_n\coloneq\sum_1^n X_k$,
\[\mathbb{E} f\left(S_n/\sqrt{n}\right)\to \int_{-\infty}^\infty f(t)
\frac{\mathrm{e}^{-t^2/2}}{\sqrt{2\uppi}}\,\mathrm{d}t
\quad\mbox{as $n\to\infty$, for every $f\in\mathrm{b}
\mathcal{C}(\mathbb{R})$.}\]

\end{document}
