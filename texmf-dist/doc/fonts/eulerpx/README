================================================================================
=                                                                              =
=             Eulerpx: A Modern Interface to the Euler math fonts              =
=                                                                              =
================================================================================
=                                                                              =
=     Version 1.0                                 Luuk Tijssen                 =
=     2022-07-14                         <ltijssen2502 at gmail dot com>       =
=                                                                              =
================================================================================

License
-------
Copyright 2016, 2017 Jabir Ali Ouassou
Copyright 2021, 2022 Luuk Tijssen

This work may be distributed and/or modified under the conditions of the LaTeX 
Project Public License, either version 1.3 of this license or (at your option) 
any later version. The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Luuk Tijssen.

This work consists of the file eulerpx.sty.

History
-------
2022-07-14:
    v1.0: First major release!
          - Now with pdf documentation;
          - Increased coverage of Euler symbols;
          - Deprecated `sansmath' and `unicode' options;
          - Added a key-value option scale to enable scaling the font;
          - It is now possible to use eulerpx with math fonts other than
            newpxmath with option `nonpxmath';
          - Other miscellaneous and under-the-hood improvements.
2021-07-12:
  v0.3.1: Fixed a bug introduced in v0.3.0 causing the \mathnormal alphabet to
          output glyphs in newpxmath instead of Euler Roman.
2021-07-08:
  v0.3.0: Major overhaul in the way that the eulervm fonts are loaded
          internally. This fixes many bugs (caused by interfering with 
          newpxmath). The sansmath implementation is now turned off by default.
          (It was also causing inconsistent output.) \mathscr and \mathfrak
          alphabets now yield Euler Script and Euler Fraktur fonts,
          respectively. \leq, \geq, and variations are now slanted by default
          (much like the original Euler fonts). Many other changes which should
          be reflected above in this README.
2017-04-10:
  v0.2.1: Fixed a bug that prevented \infty from displaying correctly.
          [Thanks to Georg Bube for reporting the problem and solution.]
2017-02-03:
  v0.2:   Fixed a bug that messed up symbols like braces and brackets. Redefined
          the macros \sffamily and \rmfamily so that they automatically change 
          the alphabet used for operators and numbers to match the environment.
2016-10-01:
  v0.1:   Initial eulerpx package created.

Credits
-------
This package is based on the following existing packages:
 - The package `eulerpx' v0.2.1, originally written by Jabir Ali Ouassou.
 - The package `newpxmath' is loaded as a base font, to provide parentheses, 
   brackets, relations, \mathbb, etc. This was done because in the authors'
   opinion, the newpx brackets are much more aesthetic than the Euler ones.
 - The Euler symbols for latin and greek letters are then loaded. This part
   is based directly on the `eulervm' package, but it has been modified to 
   not use e.g. Euler digits and brackets. As of v0.3, support for encodings
   other than T1 has returned. It is now possible to use any encoding supported
   by newpxmath.
