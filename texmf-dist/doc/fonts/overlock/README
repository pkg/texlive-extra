This is the README for the Overlock package, version 2022-09-23.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX support
for the Overlock family of fonts, designed by
Dario Manuel Muhafara of the TIPO foundry (http://www.tipo.net.ar).
These are "rounded" sans-serif fonts in three weights (Regular,
Bold, Black) with italic variants for each of them. There are also
small-caps in the Regular weight.

To install this package on a TDS-compliant TeX system download
the file "tex-archive"/install/fonts/overlock.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org. Unzip
the archive at the root of an appropriate texmf tree, likely a
personal or local tree. If necessary, update the file-name database
(e.g., texhash). Update the font-map files by enabling the Map file
overlock.map.

To use, add

\usepackage{overlock}

to the preamble of your document. This will activate
Overlock as the sans-serif text fonts. To also set these as
the main text fonts, use

\usepackage[sfdefault]{overlock}

The black option will use that weight as the default bold series.

The oldstyle and lining options will set those figures as the default.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or who wish
to avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to scale the
fonts.

Font encodings supported are OT1, T1, LY1 and TS1.

Command \overlook selects the Overlock family and
\overlockBlack selects the Black series. To select oldstyle
or lining figures locally, use \oldstylenums{...} or
\liningnums{...}, respectively.

The opentype fonts are available at
https://www.fontsquirrel.com/fonts/overlock and are licensed
under the SIL Open Font License, (version 1.1); the text
may be found in the doc directory. The type1 versions were
created using cfftot1 and re-named in compliance with the
Reserved Font Name provision of the OFL license. The support
files were created using autoinst and are licensed under the
terms of the LaTeX Project Public License. The maintainer of
this package is Bob Tennent (rdt at cs.queensu.ca).
