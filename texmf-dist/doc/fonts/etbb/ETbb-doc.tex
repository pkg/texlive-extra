% !TEX TS-program = pdflatexmk
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line, no indent
\pdfmapfile{=ETbb.map}
\usepackage{multicol}
\usepackage{enumitem}
\setlist[description]{style=sameline,font=\mdseries\scshape}
\setlength\unitlength{1pt}% for picture
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{upquote}
\usepackage{fancyvrb}
\usepackage{trace}
\def\yellow#1{\setlength{\fboxrule}{0pt}%
\setlength{\fboxsep}{0pt}%
\colorbox{yellow}{#1}}
%SetFonts
%ETbb plus newtxmath
\usepackage[lining]{ETbb}
\usepackage[T1]{fontenc}
\usepackage[scaled=.95,type1]{cabin}
\usepackage[varqu,varl]{zi4}% typewriter
\usepackage[libertine,vvarbb]{newtxmath}
%\usepackage[bb=boondox,frak=boondox]{mathalfa}
%SetFonts
\font\altr=ETbb1-Regular-tlf-t1 at 11pt
\font\altrsc=ETbb1-Regular-tlf-sc-t1 at 11pt
\usepackage{pgffor}
\usepackage{etoolbox}
%\usepackage{lipsum}
\makeatletter
\long\def\thegrid#1#2{\edef\scanp@gcmd{\noexpand\put(0,0){\noexpand\line(1,0){#1}}
\noexpand\put(0,0){\noexpand\line(0,1){#2}}
\noexpand\put(#1,0){\noexpand\line(0,1){#2}}
\noexpand\put(0,#2){\noexpand\line(1,0){#1}}}
\foreach\y in{-500,-400,...,1000}{%
  \put(0,\y){\color{red!20}\line(1,0){4000}}%
  }
\foreach\x in {0,100,...,4000}{%
  \put(\x,-500){\color{red!20}\line(0,1){1500}}%
}
\foreach\y in{-500,0,...,1000}{%
  \put(-100,\y){\color{red!50}\line(1,0){4100}}%
  \put(-200,\y){\y}  
  }
\foreach\x in {0,500,...,4000}{% 
  \put(\x,-500){\color{red!50}\line(0,1){1500}}%
  \put(\x,-150){\x}}
}
\setlength\unitlength{.1088bp}
\usepackage{scalefnt}
\usepackage{fonttable}
\usepackage{url,hyperref}
\title{The \textbf{ETbb} package---Edward Tufte's version of Bembo}
\author{Michael Sharpe}
\date{}
%\pagestyle{empty}
\begin{document}
%\SS \ss \textsc{\ss} \MakeUppercase{\ss}
\maketitle
\section*{Background}


The fonts in this package were derived ultimately from the collection of fonts commissioned by Edward Tufte for his own books, and released in 2015 as {\tt ET-Bembo} under the MIT license. (The sources for that collection were fonts using the family name ET-book.) That collection was enhanced in 2019 under the name {\tt XETBook} by  Daniel Benjamin Miller, and it is his package which was the starting point for {\tt ETbb}, where the {\tt bb} denotes the Berry abbreviation for Bembo. The final section of this document makes a detailed comparison with the earlier {\tt fbb} package, which is also  Bembo-like, derived from {\tt Cardo.} The most significant differences are that {\tt ETbb} has a regular upright that is about 20\% darker than the corresponding {\tt fbb}, and its ascender height is noticeably less. These differences make {\tt ETbb} have a less spindly appearance that is closer in spirit to the print produced by traditional metal versions of Bembo.


\section*{Package properties}

The package makes a number of changes to the {\tt XETBook} fonts:
\begin{itemize}
\item  The  released version of ET-Bembo lacks kerning tables---a serious omission---rectified in {\tt ETbb}.  
\item
The scale has been increased by 3.36\% so that the x-height of the upright regular face is 431, very close to Computer Modern and Libertine.
\item The lining figures in some faces were reduced so as to be a bit less than the cap-heights.
\item The lining figures in {\tt XETBook} were proportional rather than tabular. I've added new tabular lining and old-style figures.
\item Added superior letters and figures to all faces. E.g., \verb|\textsu{ABCabc123}| renders as \textsu{ABCabc123}.
\item Added inferior figures to all faces with baseline at -112{\tt em}.
\item Added denominator figures to all faces with baseline at 0{\tt em}.
\item The originals comprised glyphs in the Adobe Standard Encoding, forming a rather sparse subset of the T1 encoding. I've added accented and composite glyphs that provide complete coverage of the T1 encoding as well as many glyphs required in the orthography of a number Eastern European countries.
\item Prior to version 1.02, coverage of TS$1$ encoding was meager. The coverage is now close to full. (See the table at the end of this document.)
\item Small caps have been added to all faces.
\item There is a new glyph for the German capital sharp S ({\altr\char223}, \textit{gro\ss es  eszett}, {\tt U+1E9E}), approved in 2017 for optional use in German orthography. Small cap versions are also provided.
\item The glyph capital P has been changed from its default closed shape, as used in almost all modern digital renderings of Bembo, to the more historically accurate open shape. See, for example, the reproduction of Pietro Bembo's \textit{De Aetna} at \\
\url{https://ia601405.us.archive.org/34/items/ita-bnc-ald-00000673-001/ita-bnc-ald-00000673-001.pdf}.\\ 
(A higher resolution rendering of  a two-page sample is available from \url{https://upload.wikimedia.org/wikipedia/commons/8/89/De_Aetna_1495.jpg.})
\end{itemize}

\section*{Package options and macros}


This package has most of the same features and options as the {\tt fbb} package and even includes the {\tt altP} option, though that has no effect because the  alternate P shapes in {\tt fbb} are the default in {\tt ETbb}. 

In the original {\tt XETBook}, the dollar and cent currency symbols were oldstyle. I've added the new style symbols and made them the default, but option {\tt osdollar} to {\tt ETbb} changes back to the oldstyle symbols.


\textbf{New in version $1.05$:} A swash version of Q has been added to all faces. You may enable it globally using option {\tt swashQ} to {\tt ETbb}, or specify it locally with the macro \verb|\Qswash|, which renders as \Qswash. This document did not specify option {\tt swashQ}. If you had enabled it globally, you have access to the ordinary Q with the macro \verb|\Qnoswash|. You may find it simpler to use the macro \verb|\altQ| which renders Q as ordinary Q if you had specified option {\tt swashQ} and as \altQ\ if not.

Text figures may be selected from four types:
\begin{itemize}
\item
Proportional lining (LF), selected by options {\tt lining, proportional [or p]}; ({\tt lining}, or {\tt lf}, is the default figure style;)
\item
Tabular lining (TLF), selected by options {\tt lining [or lf], tabular [or t]}; ({\tt tabular} is the default figure alignment;)
\item
Proportional oldstyle (OsF), selected by option {\tt oldstyle [or osf], proportional [or p]};
\item
Tabular oldstyle (TOsF), selected by options {\tt oldstyle [or osf], tabular [or t]}.
\end{itemize}
The package also defines macros that allow you use alternate figure styles locally:

\begin{center}
  \begin{tabular}{@{} lll @{}}
    \toprule
    Macro & result & comment \\ 
    \midrule
\verb|\textlf{0123456789}| &\textlf{0123456789} & print 0123456789 in proportional lining figures\\
\verb|\texttlf{0123456789}|&\texttlf{0123456789}&  print 0123456789 in tabular lining figures\\
\verb|\textosf{0123456789}| &\textosf{0123456789}& print 0123456789 in proportional oldstyle figures\\
\verb|\texttosf{0123456789}| &\texttosf{0123456789}& print 0123456789 in tabular oldstyle figures\\
\verb|\textsu{0123456789}| &\textsu{0123456789}& print 0123456789 in superior figures\\
\verb|\textinf{0123456789}| &\textinf{0123456789}& print 0123456789 in inferior figures\\
\verb|\textde{0123456789}| &\textde{0123456789}& print 0123456789 in denominator figures\\
    \bottomrule
  \end{tabular}
\end{center}
The macro \verb|\textlf{123}| is identical in effect to \verb|{\lfstyle 123}|, and similarly for the other lining and oldstyle macros, while \verb|\textsu{123}| has the same effect as \verb|{\sufigures 123}|
and \verb|\textinf{123}| has the same effect as \verb|{\infigures 123}|.
If you prefer typing longer names, you may use \verb|\textinferior| as a synonym for \verb|\textinf|, and similarly for \verb|\textsu|. Likewise, 
\verb|\textde{123}| has the same effect as \verb|{\defigures 123}| or \verb|{\denomfigures 123}|, and you may use \verb|\textdenom| as a synonym for \verb|\textde|. Note the difference in baseline between \verb|\textinf| and \verb|\textde|: \textinf{123} versus \textde{123}.

The \verb|\textfrac| macro constructs fractions using \verb|\textsu| and \verb|\textde| with baseline aligned with the text baseline. The behavior is somewhat configurable, there being two parameters available to control the kerns before and after the fraction solidus. The two parameters are passed as options to {\tt ETbb}, named
\begin{verbatim}
foresolidus % default value -.05em
aftsolidus  % default value -.05em
\end{verbatim}
%raisefrac   % default value 0em
(The values should always be {\tt em} or {\tt ex} units in order to behave correctly with respect to scaling.)
%If you were to change the default behavior with the option
%\begin{verbatim}
%raisefrac=-112em
%\end{verbatim}
%you would get a fraction with the denominator's baseline  at the baseline of the \verb|\textin| figures, namely {\tt -.112em}. 
%%Those who wish the fractional part to be vertically centered with respect to lining figures should specify
%%\begin{verbatim}
%%raisefrac=-.056em
%%\end{verbatim}
%All the above have to do with globally defined settings for \verb|\textfrac|, but that macro allows one optional argument that can override the effect of {\tt raisefrac}, with, e.g., \verb|\textfrac[.1em][6}{11}| raising the fraction 6/11 by {\tt .1em} instead of the default specified in the original options.

\textsc{Example:}\\
$\bullet$ \verb|\textfrac[2]{17}{32}| renders as \textfrac[2]{17}{32} with default settings. (The optional argument 2 will always print in lining figures, no matter the choice of the text figure style.)\\

If you load the {\tt ETbb} package by means of option {\tt etbb} to the {\tt newtx} package, version 1.71 or higher, there is a stacked fraction construction available using the macro \verb|\textsfrac| which behaves like \verb|\textfrac| except with the fractional part stacked vertically rather than diagonally. See the {\tt newtx} documentation for details and examples.

%$\bullet$ \verb|2\textfrac[.053em]{17}{32}| produces a fraction centered on the mid-height of lining figures: 2\textfrac[.053em]{17}{32}.\\
%$\bullet$ \verb|2\textfrac[0em]{17}{32}| produces a fraction with numerator and denominator at the normal heights of superior and inferior figures: 2\textfrac[0em]{17}{32}.\\

%The \verb|\textfrac| macro uses spacing control for each individual digit, one for the numerator and one for the denominator. It is optimized for regular weight, upright shape, but works satisfactorily in bold weight, upright shape. Italic shapes are not handled with any precision. Currently, the spacing settings are specified by two macros in {\tt ETbb.sty}: \verb|\tx@addNkern| and \verb|\tx@addDkern| for numerator and denominator respectively. These may be redefined in your preamble after loading the {\tt ETbb} package with a block like
%\begin{verbatim}
%\makeatletter
%\renewcommand*{\tx@addNkern}[1]{%
%.
%.
%.
%}
%\renewcommand*{\tx@addDkern}[1]{%
%.
%.
%.
%}
%\end{verbatim}
%following the pattern of the definitions in {|tt ETbb.sty}. You should be careful to follow those patterns precisely, as it is very easy to introduce inadvertent space characters in the output.


Option {\tt sups} changes the form of footnote markers to use {\tt ETbb}'s superior figures, unless you have redefined the meaning of \verb|\thefootnote| prior to loading {\tt ETbb}. For more control over size, spacing and position of footnote markers, use the \textsf{superiors} package: E.g.,
\begin{verbatim}
\usepackage[supstfm=ETbb-Regular-sup-t1]{superiors}
\end{verbatim}

Option {\tt sharpS} replaces {\tt SS} in the {\tt T1} encoding by the new {\tt U+1E9E} glyph and replaces the small cap \textsc{ss} by the small cap version of\hspace{.5em}{\tt U+1E9E}. Only figure-styles {\tt TLF}, {\tt LF}, {\tt OsF} and {\tt TOsF} are handled, and only in the {\tt T1} encoding.
%\SS \ss \textsc{\ss}. {\usefont{T1}{ETbb-TLF}{m}{n}\SS\ss}

Option {\tt scosf} forces the use of {\tt OsF} figures in a small caps block, no matter what the default figure settings.

There is a {\tt scaled [or scale]} option (\emph{e.g.}, {\tt scaled=.97}) that allow you to adjust the text size against, say, a math package. 


\section*{A suggested math companion}

This text package works well with {\tt newtxmath} with the {\tt libertine} option, because the latter has italics of the same italic angle as {\tt ETbb} and of very similar xheight and weight. If you have the MinionPro fonts (version 2.0 or higher) and have set them up with \textsf{FontPro} and the {\tt minion2newtx} \textsc{ctan} package, then the {\tt minion} option to {\tt newtxmath} provides a very good math companion with better Greek letters than {\tt libertine}. The suggested invocation for {\tt libertine} math is:
\begin{verbatim}
% load babel package and options here
\usepackage[p,osf]{ETbb} % osf in text, tabular lining figures in math
\usepackage[scaled=.95,type1]{cabin} % sans serif in style of Gill Sans
\usepackage[varqu,varl]{zi4}% inconsolata typewriter
\usepackage[T1]{fontenc} % LY1 also works
\usepackage[libertine,vvarbb]{newtxmath}
%\usepackage[cal=boondoxo,bb=boondox,frak=boondox]{mathalfa}
\end{verbatim}
Here is a short sample based on this preamble:\\[4pt]
\def\Pr{\ensuremath{\mathbb{P}}}
\def\rmd{\mathrm{d}}
The typeset math below follows the ISO recommendations that only variables
be set in italic. Note the use of upright shapes for $\rmd$, $\mathrm{e}$
and $\uppi$. (The first two are entered as \verb|\mathrm{d}| and
\verb|\mathrm{e}|, and in fonts derived from {\tt newtxmath} or {\tt mtpro2},
 the latter is entered as \verb|\uppi|.)

\textbf{Simplest form of the \textit{Central Limit Theorem}:} \textit{Let
$X_1$, $X_2,\cdots$ be a sequence of iid random variables with mean $0$ 
and variance $1$ on a probability space $(\Omega,\mathcal{F},\Pr)$. Then}
\[\Pr\left(\frac{X_1+\cdots+X_n}{\sqrt{n}}\le y\right)\to\mathfrak{N}(y)\coloneq
\int_{-\infty}^y \frac{\mathrm{e}^{-t^2/2}}{\sqrt{2\uppi}}\,
\mathrm{d}t\quad\mbox{as $n\to\infty$,}\]
\textit{or, equivalently, letting} $S_n\coloneq\sum_1^n X_k$,
\[\mathbb{E} f(S_n/\sqrt{n})\to \int_{-\infty}^\infty f(t)
\frac{\mathrm{e}^{-t^2/2}}{\sqrt{2\uppi}}\,\mathrm{d}t
\quad\mbox{as $n\to\infty$, for every $f\in\mathrm{b}
\mathcal{C}(\mathbb{R})$.}\]

%\section{Text effects under \texttt{fontaxes}}
%This package loads the {\tt fontaxes} package in order to access italic small caps. You should pay attention to the fact that {\tt fontaxes} modifies the behavior of some basic \LaTeX\ text macros such as \verb|\textsc| and \verb|\textup|. Under normal \LaTeX, some text effects are combined, so that, for example, \verb|\textbf{\textit{a}}| produces bold italic {\tt a}, while other effects are not, eg, \verb|\textsc{\textup{a}}| has the same effect as \verb|\textup{a}|, producing the letter {\tt a} in upright, not small cap, style. With {\tt fontaxes}, \verb|\textsc{\textup{a}}| produces instead upright small cap {\tt a}. It offers a macro \verb|\textulc| that undoes small caps, so that, eg, \verb|\textsc{\textulc{a}}| produces {\tt a} in non-small cap mode, with whatever other style choices were in force, such as bold or italics.

%\section{Superior figures}
%The TrueType versions of GaramondNo8 have a full set of superior figures, unlike their PostScript counterparts. The superior figure glyphs in regular weight only have been copied to \texttt{NewG8-sups.pfb} and \texttt{NewG8-sups.afm} and provided with a tfm named \texttt{NewG8-sups.tfm} that can be used by the \textsf{superiors} package to provide adjustable footnote markers. See \textsf{superiors-doc.pdf} (you can find it in \TeX Live by typing \texttt{texdoc superiors} in a Terminal window.) The simplest invocation is
%\begin{verbatim}
%\usepackage[supstfm=NewG8-sups]{superiors}
%\end{verbatim}
\section*{Glyphs in TS1 encoding}
The layout of the TS1 encoded Text Companion font, which is rendered \emph{in regular style only}, is as follows. 

\fonttable{ETbb-Regular-tosf-ts1}


\textbf{List of macros to access the TS1 symbols in text mode:}\\
%(Note that slots 0--12 and 26--29 are accents, used like \verb|\t{a}| for a tie accent over the letter a. Slots 23 and 31 do not contain visible glyphs, but have heights indicated by their names.)
(The commented lines are in {\tt fbb} but not {\tt ETbb}.)
\begin{verbatim}
  0 \capitalgrave
  1 \capitalacute
  2 \capitalcircumflex
  3 \capitaltilde
  4 \capitaldieresis
  5 \capitalhungarumlaut
  6 \capitalring
  7 \capitalcaron
  8 \capitalbreve
  9 \capitalmacron
 10 \capitaldotaccent
 11 \capitalcedilla
 12 \capitalogonek
 13 \textquotestraightbase
 18 \textquotestraightdblbase
 21 \texttwelveudash
 22 \textthreequartersemdash
 23 \textcapitalcompwordmark
 24 \textleftarrow
 25 \textrightarrow
 26 \t % tie accent, skewed right
 27 \capitaltie % skewed right
 28 \newtie % tie accent centered
 29 \capitalnewtie % ditto
 31 \textascendercompwordmark
 32 \textblank
 36 \textdollar
 39 \textquotesingle
 42 \textasteriskcentered
 45 \textdblhyphen
 47 \textfractionsolidus
 48 \textzerooldstyle
 49 \textoneoldstyle
 50 \texttwooldstyle
 49 \textthreeoldstyle
 50 \textfouroldstyle
 51 \textfiveoldstyle
 52 \textsixoldstyle
 53 \textsevenoldstyle
 54 \texteightoldstyle
 55 \textnineoldstyle
 60 \textlangle
 61 \textminus
 62 \textrangle
 77 \textmho
 79 \textbigcircle
 87 \textohm
 91 \textlbrackdbl
 93 \textrbrackdbl
 94 \textuparrow
 95 \textdownarrow
 96 \textasciigrave
 98 \textborn
 99 \textdivorced
100 \textdied
108 \textleaf
109 \textmarried
%110 \textmusicalnote
126 \texttildelow
127 \textdblhyphenchar
128 \textasciibreve
129 \textasciicaron
%130 \textacutedbl
%131 \textgravedbl
132 \textdagger
133 \textdaggerdbl
134 \textbardbl
135 \textperthousand
136 \textbullet
137 \textcelsius
138 \textdollaroldstyle
139 \textcentoldstyle
140 \textflorin
141 \textcolonmonetary
142 \textwon
143 \textnaira
144 \textguarani
145 \textpeso
146 \textlira
147 \textrecipe
148 \textinterrobang
149 \textinterrobangdown
150 \textdong
151 \texttrademark
152 \textpertenthousand
153 \textpilcrow
154 \textbaht
155 \textnumero
156 \textdiscount
157 \textestimated
158 \textopenbullet
159 \textservicemark
160 \textlquill
161 \textrquill
162 \textcent
163 \textsterling
164 \textcurrency
165 \textyen
166 \textbrokenbar
167 \textsection
168 \textasciidieresis
169 \textcopyright
170 \textordfeminine
171 \textcopyleft
172 \textlnot
173 \textcircledP
174 \textregistered
175 \textasciimacron
176 \textdegree
177 \textpm
178 \texttwosuperior
179 \textthreesuperior
180 \textasciiacute
181 \textmu
182 \textparagraph
183 \textperiodcentered
184 \textreferencemark
185 \textonesuperior
186 \textordmasculine
187 \textsurd
188 \textonequarter
189 \textonehalf
190 \textthreequarters
191 \texteuro
214 \texttimes
246 \textdiv
%\end{verbatim}
There is a macro \verb|\textcircled| that may be used to construct a circled version of a single letter using \verb|\textbigcircle|. The letter is always constructed from the small cap version, so, in effect, you can only construct circled uppercase letters: \verb|\textcircled{M}| and \verb|\textcircled{m}| have the same effect, namely~\textcircled{M}.

\section*{Usage with fontspec}
Because the package supplies a file named {\tt ETbb.fontspec} whose contents list the {\tt otf} files that correspond to each of Regular, Bold, Italic and BoldItalic, you may load {\tt ETbb} with just
\begin{verbatim}
\usepackage{fontspec}
\setmainfont{ETbb}
\end{verbatim}
Other than the usual choices of figure style, the only remaining choice available is through {\tt StylisticSet=2}, which substitutes the new Sharp S glyphs in place of the familiar \ss, \SS\ and \textsc{\ss}. See the table in the next section for details.

\section*{Selection of the new Sharp S in LaTeX}
There is now an {\tt ETbb} option {\tt sharpS} whose effect in legacy LaTeX is summarized below.

Behavior of the text macros \verb|\SS|, \verb|\ss| and the macro \verb|\MakeUppercase|.

\begin{center}
  \begin{tabular}{@{} ccccc @{}}
    \hline
    sharpS option & \verb|\ss| & \verb|\SS| & \verb|\MakeUppercase{\ss}| & \verb|\textsc{\ss}| \\ 
    \hline
    Not set & \ss & \SS & \MakeUppercase{\ss} & \textsc{\ss}\\ 
    sharpS & {\altr\char255} & {\altr\char223} & {\altr\char223} & {\altrsc \char255}\\ 
    \hline
  \end{tabular}
\end{center}
In unicode TeX,  the behavior laid out in the table above is achieved using {\tt StylisticSet=2}. 

%\begin{center}
%  \begin{tabular}{@{} lcl @{}}
%    \hline
%    Glyph name & glyph & macro\\ 
%    \hline
%    {\tt uni1E9E} & \symbol{"1E9E} &\verb|\symbol{"1E9E}|\\ 
%    {\tt uni1E9E.ss01} & {\addfontfeature{StylisticSet=1}\symbol{"1E9E}} & \verb|{\addfontfeature{StylisticSet=1}\symbol{"1E9E}}| \\ 
%    {\tt germandbls.sc} & \textsc{\ss} & \verb|{\textsc{\ss}}| \\ 
%    {\tt germandbls.sc.ss01} & {\addfontfeature{StylisticSet=1}\textsc{\ss}} & \verb|{\addfontfeature{StylisticSet=1}\textsc{\ss}}| \\ 
%    \hline
%  \end{tabular}
%\end{center}  
 
%\noindent \textbf{Effect of choice of {\tt StylisticSet}:}
% 
%\begin{center}
%  \begin{tabular}{@{} ccccc @{}}
%    \hline
%    StylisticSet & \verb|\ss| & \verb|\SS| & \verb|\MakeUppercase{\ss}| & \verb|\textsc{\ss}| \\ 
%    \hline
%    None & \ss & \SS & \MakeUppercase{\ss} & \textsc{\ss}\\ 
%    
%    =1 & {\addfontfeature{StylisticSet=1}\ss} & {\addfontfeature{StylisticSet=1}\SS} & {\addfontfeature{StylisticSet=1}\MakeUppercase{\ss}} & {\addfontfeature{StylisticSet=1}\textsc{\ss}}\\ 
%    \hline
%  \end{tabular}
%\end{center}


%\section{Historical Background}
%Humanist scholar Pietro Bembo, a seminal figure in literature and music of the Italian Renaissance, who later became Cardinal Bembo, wrote an essay in the last decade of the 15th century about his travels to Mt.\ Aetna, which work was published by the Venetian printer Aldus Manutius (whose name gave us \emph{Aldine}) using a new Roman font designed by his punch-cutter, Francesco Griffo that improved on the earlier efforts of Jenson, another printer in Venice. That font seems to have played a similarly seminal r\^ole in  typography. It was the direct progenitor of the many Garamond fonts, and has seen numerous modern revivals whose names make use of every known historical connection to the figures named above, such as Lucrezia Borgia who was for several years Bembo's  lover.
%
%The metal form of the Bembo font developed by Stanley Morison for English Monotype in the 1920's was widely used in book printing due to its handsome appearance and readability. Commercial digital versions have not had much love from critics until recently.  Adobe's MinionPro and WarnockPro arguably deserve the prizes for the best modern revivals of oldstyle fonts not too distant from Bembo. (Both lack Bembo's tall ascenders and its characteristic overarching f.)
%twoone free source for a Bembo--like font family, one being David Perry's \emph{Cardo} (a contraction of \emph{Cardinal Bembo}), which is not readily accessible to 
%\LaTeX\ users and which lacks Bold Italic as well as a full range of Small Caps and figure styles. The other is Edward Tufte 

 
 
% On screen and paper, {\tt ETbb} appears close in weight to Libertine, though of greater  ascender height and slightly less plain.  The following two sentences are written in {\tt ETbb} and Libertine respectively. The third example sentence is written using {\tt garamondx}, whose natural xheight is comparable to Libertine, but which should normally be scaled down to resemble more familiar Garamonds. Perhaps {\tt ETbb} will be prove to be more suitable for older eyes.
%
%\textit{\textsc{Comparison between ETbb and Libertine}}: 
%
%Both ETbb and Libertine are highly readable fonts in their standard Roman forms, each has a wide range of figures and small caps, but Libertine has the advantage in the number of supported scripts and the variety of weights.
%
%{\fontfamily{LinuxLibertineT-LF}\selectfont Both ETbb and Libertine are highly readable fonts in their standard Roman forms, each has a wide range of figures and small caps, but Libertine has the advantage in the number of supported scripts and the variety of weights.}

\section*{Detailed comparison with {\tt fbb}}
The following picture, in which the units are approximately in {\tt bp}, 
shows some of the differences between {\tt ETbb-Regular} and {\tt fbb-Regular}, the first scaled up by 10 and the second by 9.8 so that their x-heights (and Cap-heights) are the same. From the picture below you can note the following.
\begin{itemize}
\item
The serifs are much more substantial in {\tt ETbb}.
\item The ascenders a considerably higher in {\tt fbb}---in fact, by 50 em units. Those very tall ascenders make for poor positioning of quotes, superscripts and the like.
\item Stems are a little thicker (by about 10\%) in {\tt ETbb}.
\item There is a slight bowing out in the letter h and similar letters like n of {\tt ETbb} that is not present in {\tt fbb}, making for more visual interest, IMO. This would not be of any importance at small print sizes.
\item Overall, {\tt ETbb} has lower contrast (ratio of thickest to thinnest strokes) than {\tt fbb}, making for a more uniform gray appearance on the printed page.
\end{itemize}
\picture(4500,1300)
\thegrid{5000}{1500}
\put(0,0){{\usefont{T1}{ETbb-TLF}{m}{n}\scalefont{10}xXh{\color{red}:}} {\usefont{T1}{fbb-TLF}{m}{n}\scalefont{9.8}xXh}}
\put(0,1100){\hbox to 2000\unitlength{\hfil ETbb scaled 1000\%\hfil}}
\put(2000,1100){\hbox to 2000\unitlength{\hfil fbb scaled 980\%\hfil}}
\endpicture
\vspace*{1in}
The following page presents a comparison of a {\tt ETbb} and {\tt fbb} with identical text rendered in two columns. For me, there is no question that {\tt ETbb} is the preferable font for document text.
\newpage
\begin{multicols}{2}[\textbf{ETbb on left, fbb on right, normalized to same x-height}]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac tortor dignissim convallis aenean. Adipiscing elit duis tristique sollicitudin nibh. At imperdiet dui accumsan sit. Posuere sollicitudin aliquam ultrices sagittis. In hac habitasse platea dictumst quisque sagittis purus sit. Vulputate mi sit amet mauris commodo quis imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat. Tortor vitae purus faucibus ornare suspendisse. Non consectetur a erat nam at lectus. Curabitur gravida arcu ac tortor. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Nisl nisi scelerisque eu ultrices vitae auctor eu. A lacus vestibulum sed arcu non odio euismod lacinia at. Ut venenatis tellus in metus vulputate eu. Ornare massa eget egestas purus viverra accumsan in nisl. Mauris augue neque gravida in fermentum et sollicitudin ac orci. Turpis egestas sed tempus urna et pharetra pharetra. Nunc lobortis mattis aliquam faucibus.

Nulla facilisi morbi tempus iaculis urna id volutpat lacus. Est ultricies integer quis auctor elit. Risus quis varius quam quisque id. Mus mauris vitae ultricies leo integer malesuada nunc vel risus. Sit amet purus gravida quis blandit turpis cursus in hac. Nam at lectus urna duis convallis convallis tellus. Amet dictum sit amet justo. Tortor consequat id porta nibh venenatis cras. Ante metus dictum at tempor. Senectus et netus et malesuada fames ac turpis.

Non tellus orci ac auctor augue mauris augue neque gravida. Bibendum at varius vel pharetra vel turpis nunc. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Quis enim lobortis scelerisque fermentum dui faucibus in. Scelerisque eu ultrices vitae auctor. Blandit volutpat maecenas volutpat blandit. Morbi leo urna molestie at elementum eu. Tristique magna sit amet purus gravida quis blandit. Felis eget nunc lobortis mattis aliquam faucibus purus in. Pharetra diam sit amet nisl suscipit. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus. Vitae nunc sed velit dignissim. Nulla pellentesque dignissim enim sit. Sem viverra aliquet eget sit amet tellus.

\columnbreak
{\usefont{T1}{fbb-TLF}{m}{n}\scalefont{.98}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac tortor dignissim convallis aenean. Adipiscing elit duis tristique sollicitudin nibh. At imperdiet dui accumsan sit. Posuere sollicitudin aliquam ultrices sagittis. In hac habitasse platea dictumst quisque sagittis purus sit. Vulputate mi sit amet mauris commodo quis imperdiet. Vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat. Tortor vitae purus faucibus ornare suspendisse. Non consectetur a erat nam at lectus. Curabitur gravida arcu ac tortor. Tempus urna et pharetra pharetra massa massa ultricies mi quis. Nisl nisi scelerisque eu ultrices vitae auctor eu. A lacus vestibulum sed arcu non odio euismod lacinia at. Ut venenatis tellus in metus vulputate eu. Ornare massa eget egestas purus viverra accumsan in nisl. Mauris augue neque gravida in fermentum et sollicitudin ac orci. Turpis egestas sed tempus urna et pharetra pharetra. Nunc lobortis mattis aliquam faucibus.

Nulla facilisi morbi tempus iaculis urna id volutpat lacus. Est ultricies integer quis auctor elit. Risus quis varius quam quisque id. Mus mauris vitae ultricies leo integer malesuada nunc vel risus. Sit amet purus gravida quis blandit turpis cursus in hac. Nam at lectus urna duis convallis convallis tellus. Amet dictum sit amet justo. Tortor consequat id porta nibh venenatis cras. Ante metus dictum at tempor. Senectus et netus et malesuada fames ac turpis.

Non tellus orci ac auctor augue mauris augue neque gravida. Bibendum at varius vel pharetra vel turpis nunc. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Quis enim lobortis scelerisque fermentum dui faucibus in. Scelerisque eu ultrices vitae auctor. Blandit volutpat maecenas volutpat blandit. Morbi leo urna molestie at elementum eu. Tristique magna sit amet purus gravida quis blandit. Felis eget nunc lobortis mattis aliquam faucibus purus in. Pharetra diam sit amet nisl suscipit. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus. Vitae nunc sed velit dignissim. Nulla pellentesque dignissim enim sit. Sem viverra aliquet eget sit amet tellus.
}
\end{multicols}

\textbf{ETbb-Regular-tlf-t1}

\fonttable{ETbb-Regular-tlf-t1}
\newpage
\textbf{ETbb1-Regular-tlf-t1}

\fonttable{ETbb1-Regular-tlf-t1}
\newpage
\textbf{ETbb-Regular-tlf-sc-t1}

\fonttable{ETbb-Regular-tlf-sc-t1}
\newpage
\textbf{ETbb1-Regular-tlf-sc-t1}

\fonttable{ETbb1-Regular-tlf-sc-t1}

%This is a fraction \textfrac{23}{52}, \textfrac{71}{12}, \textfrac{17}{75}, \textfrac{34}{43}.

%The {\tt ETbb} package offers a family of Bembo--like fonts in the  four basic styles. There are of course similarities to {\tt fbb}, the main distinctions being:
%\begin{itemize}
%\item
%The ascenders in {\tt ETbb} are considerably reduced compared with those in {\tt fbb};
%\item The stroke widths in {\tt ETbb} Regular are about 10\% larger than those in {\tt fbb} Regular, so the gray level is about 20\% higher.
%\item The serifs in {\tt ETbb} are not nearly as thin as those in {\tt fbb};
%\end{itemize}

%\textit{\textsc{Same sentence in garamondx}}: 
%
%{\fontfamily{zgmx}\selectfont Both ETbb and Libertine are highly readable fonts in their standard Roman forms, each has a wide range of figures and small caps, but Libertine has the advantage in the number of supported scripts and the variety of weights.}

\end{document}