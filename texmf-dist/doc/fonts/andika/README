This is the README for the andika package, version 6.101,
release 2022-09-28.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the Andika family of fonts designed by SIL
International especially for literacy use, taking into
account the needs of beginning readers. The focus is on
clear, easy-to-perceive letterforms that will not be readily
confused with one another.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/andika.tds.zip 
where the preferred URL for "tex-archive" is
http://mirrors.ctan.org. Unzip the archive at the root of an
appropriate texmf tree, likely a personal or local tree. If
necessary, update the file-name database (e.g., texhash).
Update the font-map files by enabling the Map file
andika.map.

To use, add

\usepackage{andika}

to the preamble of your document. This will activate
Andika as the default sans text font. To also set Andika as
the main text font, use

\usepackage[sfdefault]{andika}

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or
who wish to avoid fontspec may use the type1 option.

Regular, Italic, Bold and BoldItalic styles are available  .
The only figure style is tabular-lining                    .

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

The fonts are licensed under the SIL Open Font License,
version 1.1, with Reserved Font Names "Andika" and "SIL".
The text may be found in the doc directory. The rest of the
package is licensed under the terms of the LaTeX Project
Public License. The maintainer of this package is Bob
Tennent (rdt at cs.queensu.ca).
