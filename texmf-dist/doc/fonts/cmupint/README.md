The cmupint package
version 1.1 (April 13, 2020)
Uroš Stefanović

This material is subject to The LaTeX Project Public License.

The cmupint package contains various upright integral symbols
to match Computer Modern font (default LaTeX font).

This package is very simple to use: just put
	\usepackage{cmupint}
in preamble of the document.

Happy TeXing!
