# Chivo fonts for LaTeX #

This work provides the necessary files to use the Chivo fonts with
LaTeX.  Chivo is a set of eighteen fonts provided by Héctor Gatti &
[Omnibus-Type](https://www.omnibus-type.com/fonts/chivo/) Team under
the Open Font License [(OFL)](http://scripts.sil.org/OFL), version
1.1.  The fonts are copyright (c) 2019, Omnibus-Type.

The LaTeX package is released under the LaTeX Project Public License
[(LPPL)](http://www.latex-project.org/lppl.txt) v1.3c or later,
copyright (c) 2016-2019 Arash Esbati.  The fonts were obtained from
[Omnibus-Type GitHub repository](https://www.omnibus-type.com/fonts/chivo/).

v2.2, 2022/11/15
* Update the fonts to upstream v2.002
* Update the package to support new font weights
* **Incompatible change:** The macros `\textin` `\textsu`, `\textnu`
  and `\textde` are renamed in order to reduce clashes with other
  packages, most notably `hyperref` and `babel`.

v2.1, 2019/07/19
* Fix a bug in handling of alias keys

v2.0, 2019/07/07
* Add the `scaled' key
* Use NFSS scheme for fonts mapping
* Rewrite major part of the code supporting 14 fonts.  Harmonize
  the usage of package options over different TeX engines

v1.0, 2016/05/05
* Initial release
