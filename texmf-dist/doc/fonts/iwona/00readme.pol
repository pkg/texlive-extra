% --- iso8859-2 ---
Font: Iwona
Autor czcionek Kurier: Ma�gorzata Budyta, Warszawa
Autor font�w Iwona: Janusz Marian Nowacki, Grudzi�dz
Wersja: 0.995b
Data: sierpie� 2010
Licencja:
 % Copyright 2005-2010 Janusz M. Nowacki.
 % This work is released under the GUST Font License
 %     -- see GUST-FONT-LICENSE.txt
 % This work has the LPPL maintenance status "author-maintained".
 % This work consists of the files listed in the MANIFEST.txt file.

Iwona jest dwuelementow� czcionk� bezszeryfow�. Powsta�a jako
alternatywna wersja czcionek Kurier, kt�re by�y prac� dyplomow�
z liternictwa drukarskiego na warszawskiej Akademii Sztuk
Pi�knych, pod okiem Romana Tomaszewskiego w 1975 roku.

Kurier powstawa� jako pismo przeznaczone dla gazet i innej prasy
do sk�adania technik� linotypow�. Fonty Iwona s� alternatywn� wersj�
font�w Kurier. R�ni� si� brakiem charakterystycznych dla Kuriera
pu�apek farbowych. Zainteresowanych rekonstrukcj� oryginalnych czcionek
Kurier odsy�am do http://www.jmn.pl/kurier.html

Niniejsza dystrybucja zawiera znacznie poszerzony zestaw znak�w,
uwzgl�dniaj�cy wsp�czesne alfabety: �aci�skie (w tym wietnamski),
cyryliczne, grecki oraz dodatkowe symbole (w tym matematyczne).
Fonty udost�pniono w formatach Type 1 oraz OpenType. Dla wykorzystania
w systemie TeX przygotowano odpowiednie pliki przekodowa�: T1 (ec),
T2(abc) i OT2 -- cyryliczne, T5 (wietnamski), OT4, QX, texnansi oraz
niestandardowych (IL2 dla font�w czeskich), jak te� wparcie
w postaci odpowiednich makr i plik�w definiuj�cych fonty dla LaTeX-a.
W katalogu doc/fonts/iwona/ zawarto kilka przyk�ad�w zar�wno dla plain
TeX, jak i LaTeX-a, a tak�e przyk�ad sk�adu matematycznego.

Historia:
Wersja 0.9 zosta�a zaprezentowana podczas konferencji Grupy
U�ytkownik�w Systemu TeX w Polsce - BachoTeX-2005, w maju 2005 r.

W wersji 0.92 uwzgl�dniono zg�oszone uwagi i propozycje.

W wersji 0.97 dodano znaki:
Breveinverted, Hbar.small, Ubreveinvertedlow, Ubreveinvertedlow.small,
breveinverted, breveinvertedlow, dotlessi.small, dotlessj.small,
ubreveinvertedlow. Poprawiono r�wnie� pliki .enc.

W wersji 0.98 poprawiono tzw. korekty kursywy w fontach
matematycznych oraz ligatury exclamdown i questiondown (w plikach .tfm).

W wersji 0.99 poprawiono pliki .tfm dla font�w matematycznych, gdy�
zawiera�y nieprawid�owe parametry.

W wersji 0.991 poprawiono znak negationslash.

W wersji 0.995:
  - zmieniono nazwy znak�w kapitalikowych
    A.small --> a.sc
    B.small --> b.sc
    C.small --> c.sc
    itp.;
  - dodano brakuj�ce znaki:
    Aogonekacute, Ddotbelow, Dlinebelow, E.reversed, Eogonekacute,
    Germandbls, Hbrevebelow, Hdotbelow, Htilde, Iogonekacute, Jacute,
    Ldot, Oogonek, Oogonekacute, Rdotaccent, Sdotbelow, Tcedilla,
    Tdotbelow, Tlinebelow, Ttilde, Zdotbelow, ain, angleleft, angleright,
    aogonekacute, bigcircle, brevebelow, cent.oldstyle, centigrade,
    copyleft, ddotbelow, dieresis.alt, dlinebelow, dmacron, e.reversed,
    ell, eogonekacute, hamza, hbrevebelow, hdotbelow, htilde, hyphen.alt,
    hyphen.prop, hyphendbl, hyphendbl.alt, i.TRK, imacron.alt, iogonekacute,
    jacute, ldot, linebelow, macron.alt, nomero, oogonek, oogonekacute,
    permyriad, perthousandzero, rdotaccent, sdotbelow, servicemark,
    tcedilla, tdotbelow, tlinebelow, ttilde, zdotbelow
  - dodano cyfry proporcjonalne (xxx.prop) i cyfry nautyczne
    proporcjonalne (xxx.oldstyle);
  - UWAGA!!! aktualnie dost�pne TeX-owe kodowania (pliki tfm, enc, map)
    wymieniono w pliku MANIFEST.txt;

W wersji 0.995a poprawiono jedynie pliki dokumentacji.

W wersji 0.995b ponownie dodano brakuj�ce pliki .fd i nieco zmieniono
uk�ad katalog�w TDS.

http://www.jmn.pl/kurier.html
e-mail: janusz@jmn.pl
