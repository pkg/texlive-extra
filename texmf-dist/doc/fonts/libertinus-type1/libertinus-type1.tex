\documentclass[11pt]{article}
\usepackage[textwidth=5.5in,textheight=8.5in]{geometry}
\usepackage[T1]{fontenc}
\usepackage[osf,proportional]{libertinus-type1}
\usepackage{lettrine}
\renewcommand{\ttfamily}{\fontencoding{OT1}\fontfamily{cmtt}\selectfont}
\PassOptionsToPackage{urlcolor=black,colorlinks}{hyperref}
\RequirePackage{hyperref}
\usepackage{xcolor}
\usepackage{longtable}
\usepackage{multicol}
\newcommand{\typeone}{Type~\liningnums{1}}
\begin{document}
\title{Libertinus for Traditional \LaTeX\ Processors}
\author{Bob Tennent\\
\small\url{rdt@cs.queensu.ca}}
\date{\today}
\maketitle 
\thispagestyle{empty}
\begin{small}
\tableofcontents
\end{small}
\sloppy
\section{Introduction}

The Libertinus OpenType fonts are derived from the Linux Libertine and Biolinum
families of fonts.  To use them with \verb|xelatex| or \verb|lualatex|, install
the \verb|libertinus-fonts| and \verb|libertinus-otf| packages.
This package provides support for use of Libertinus fonts
with traditional processing engines
(\verb|latex| with \verb|dvips| or \verb|dvipdfmx|, or \verb|pdflatex|).


\section{Installation}

To install this package on a TDS-compliant \TeX\ system, download the
file 
\begin{list}{}{}\item \verb\tex-archive/install/fonts/libertinus.tds.zip\ 
\end{list}
and unzip at the root of an appropriate
\verb\texmf\ tree, likely a personal or local tree. If necessary, update
the file-name database (e.g., \verb\texhash texmf\). Update the font-map files by
enabling the Map file \verb\libertinus.map\.


\section{Basic Usage}

For most purposes, simply add

\begin{list}{}{}\item
\verb|\usepackage{libertinus}|
\end{list}
to the preamble of your document. This will activate Libertinus Serif as
the main text family, Libertinus Sans as the sans family,
and Libertinus Mono as the monospaced family. 
It is
recommended that the font encoding be set to \verb\T1\ or \verb\LY1\ but the default
\verb\OT1\  encoding is also supported.  The \verb|LGR| encoding is supported for the Serif and Sans
families. The T2A/B/C encodings are supported and may be selected using 
\verb|{\fontencoding{T2x}\selectfont ...}|.

Available shapes in all series (except \texttt{tt}, which
only has \texttt{it}) include:
\begin{list}{}{}\item
\begin{tabular}{ll}
\texttt{it} &               italic\\
\texttt{sc} &            small caps\\
\texttt{scit} &            italic small caps
\end{tabular}
\end{list}
Slanted variants are not supported; the designed italic variants will be
automatically substituted. The exceptions are the monospaced font and the bold series of Libertinus Sans,
for which designed italics are not available. Artificially
slanted variants have been generated and treated as if they were italic.

To activate Libertinus Serif  without Libertinus Sans or Mono, use the \texttt{serif} (or \texttt{rm})
option. Similarly, to activate Libertinus Sans (without Libertinus Serif or Libertinus Mono) use the
\texttt{sans} (or \texttt{sf} or \texttt{ss}) option. To use Libertinus Sans as the main text font (as
well as the sans font), use the option \texttt{sfdefault}.
Use the \verb|mono=false| (or \verb|tt=false|) option to suppress
activating Libertinus Mono. 

\section{Advanced Usage}

The following options are available in all styles (except monospaced):
\begin{list}{}{}\item
\begin{tabular}{ll}
\texttt{oldstyle} (\texttt{osf}) &  old-style figures\\
\texttt{lining} (\texttt{nf}, \texttt{lf}) &  lining figures\\
\texttt{proportional} (\texttt{p})&  varying-width figures\\
\texttt{tabular} (\texttt{t}) &     fixed-width figures\\
\end{tabular}
\end{list}
These apply to both Libertinus Serif and Libertinus Sans.
The defaults are \texttt{lining} and \texttt{tabular}. 

The \texttt{semibold} (\texttt{sb}) option will enable use of the
semi-bold series of Libertinus Serif; this has no effect on the Sans fonts,
for which there is no semi-bold variant. 
Any of the ``Boolean'' options, such as \texttt{osf}, may 
also be used
in the form \verb|osf=true| or \verb|osf=false|.

The option 
\verb|ScaleRM=|<\emph{number}> will scale the Serif fonts but have no effect on the
Sans or Mono fonts. Similarly, the options
\verb|ScaleSF=|<\emph{number}>
and \verb|ScaleTT=|<\emph{number}>
will scale the Serif and Mono fonts, respectively.  

Superior numbers (for footnote markers) are available using \verb|\sufigures|
or \verb|\textsup{|\ldots\verb|}|.


Command \verb|\useosf| switches the default figure style for Libertinus Serif and Libertinus Sans to old-style figures; this is
primarily for use \emph{after} calling a math package 
with lining figures as the default.

The following macros select the font family indicated:
\begin{center}
\begin{tabular}{ll}
\verb|\LibertinusSerif| & Libertinus Serif \\
\verb|\LibertinusSerifSB|& Libertinus Serif with semibold \\
\verb|\LibertinusSerifOsF| & Libertinus Serif with proportional oldstyle figures \\
\verb|\LibertinusSerifTLF| & Libertinus Serif with tabular lining figures \\
\verb|\LibertinusSerifLF| & Libertinus Serif with proportional lining figures \\
\verb|\LibertinusSans| & Libertinus Sans \\
\verb|\LibertinusSansOsF|& LibertinusSans with proportional oldstyle figures \\
\verb|\LibertinusSansTLF| & Libertinus Sans with tabular lining figures \\
\verb|\LibertinusSansLF| & Libertinus Sans with proportional lining figures \\
\verb|\LibertinusMono| & Libertinus Mono\\
\end{tabular}
\end{center}
Special fonts Libertinus Serif Display and Libertinus Serif Initials may be
activated by commands
\verb|\LibertinusDisplay|  and \verb|\LibertinusInitials|, respectively.

\section{Concluding Remarks}


The original Linux Libertine and Biolinum OpenType fonts were created by Philipp H. Poll 
(\url{gillian@linuxlibertine.org}) and are licensed under the terms of the GNU General
Public License (Version~2, with font exception) and under the terms of
the Open Font License. For details look into the \verb|doc| directory of the
distribution.
The basic Libertinus OpenType fonts are from 
\begin{list}{}{}\item
\url{https://github.com/libertinus-fonts/libertinus}
\end{list}
with additional fonts obtained by using \verb|fontforge|.
The support
files were created using \verb|autoinst|. The support files are licensed under
the terms of the LaTeX Project Public License.  

Thanks to Herbert Voss
and Khaled Hosny for their assistance.
The maintainer of this
package is Bob Tennent (\url{rdt@cs.queensu.ca})

\end{document}
