% !TEX TS-program = pdflatexmk
% Template file for TeXShop by Michael Sharpe, LPPL
\documentclass[11pt]{amsart}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
%\pdfmapfile{+bboldx.map}
%\usepackage{graphicx}
%\usepackage{amssymb}% don't use with newtxmath
%SetFonts
% libertine+newtxmath
\usepackage{trace}
\usepackage{newpxtext} % use sb in place of bold
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata
\usepackage[vvarbb]{newpxmath}
%\usepackage[symbols]{bboldx}
\usepackage{bboldx}
% option vvarbb gives you stix blackboard bold
\useosf % use oldstyle figures except in math
%\usepackage[cal=boondoxo]{mathalfa}% less slanted than STIX cal
%\usepackage{bm}
%SetFonts
\usepackage{fonttable}
\title{{\tt BBOLDX}---an extension of {\tt BBOLD}}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date

\begin{document}
\maketitle
%\[\mathbb{0123456789}\text{\txtbbGamma \txtbfbbGamma}\]
%\[\mathbb{0123456[](),.!\#\$:;<>?/}\]
%\end{document}
In the 1990's, Alan Jeffrey developed an extensive Blackboard Bold font with weight a good match to Computer Modern, with upper and lower case Latin and Greek letters as well as punctuation and a number of symbols. The font was the property of Y\&{}Y, and, after their dissolution, the copyright was gifted to TUG in 2007, with the freest license imaginable. This package extends the original by adding a couple of glyphs and, more important, adding two new weights. Where the original stem widths were 40 units, the additions have stem widths of 56 units and 90 units respectively. For a comparison, here are the three, labelled {\tt Thin}, {\tt Regular} and {\tt Bold} respectively.

Thin: {\usefont{U}{bboldx}{l}{n} \char"06\char"0C Hk1234567890}\\
Regular: {\usefont{U}{bboldx}{m}{n} \char"06\char"0C Hk1234567890}\\
Bold: {\usefont{U}{bboldx}{b}{n} \char"06\char"0C Hk1234567890}

\textsc{Font Table for BBOLDX-Regular}:\\
\fonttable{BBOLDX-Regular}
\newpage
This package creates two basic commands, \verb|\bbxfamily| and \verb|\bbbxfamily| to identify the fonts that should be considered regular and bold, and uses them to define  macros \verb|\textbb| and \verb|\textbfbb| by which you may access any character, though not necessarily by convenient means as the fonts do not completely follow any standard encoding. Nonetheless, it does understand Roman letters and digits, as well as some punctuation and symbols: e.g., \verb|\textbb{(A)b[1].,!?<>-}| renders as \textbb{(A)b[1].,!?<>-}. Characters may also be accessed by consulting the above table for the position of the character. For example, lowercase psi is in slot 32 decimal={"20} (HEX), so \verb|\textbb{\char32}| or \verb|\textbb{\char"20}| yields \textbb{\char"20}. It also creates macros \verb|\mathbb|, \verb|\mathbfbb| that are specific to math mode but are more limited in scope but work as expected for all numeric and Roman alphabetic arguments.

There are two ways this package may be used.

\textbf{\textsc{1: Load the {\tt bboldx} package directly.}}

To do this, place one of the lines
\begin{verbatim}
\usepackage{bboldx} % for Regular and Bold weights
\usepackage[light]{bboldx} % for Thin and Regular weights
\end{verbatim}
in your document preamble following any other lines that may load math alphabets. Other options are available:
\begin{itemize}
\item
{\tt scaled} (or {\tt scale}) controls the relative size, as in
\begin{verbatim}
\usepackage[scaled=1.03]{bboldx}
\end{verbatim}
which results in scaling up {\tt bboldx} fonts by 3\%.
\item
{\tt bbsymbols} (or {\tt symbols}) controls whether the  font is set up as a symbol font. (The default is to set it up as a MathAlphabet, which handles with greater efficacy the [La]TeX limitation to just 16 math families.)
\item {\tt bfbb} forces the package to not load the regular and light weights, but use \verb|\mathbb| as if bold were the regular weight. 
\end{itemize}
%The macros \verb|\mathbb| and its bold counterpart \verb|\mathbfbb| understand all alphanumeric arguments but no others, at least directly. 

\textsc{Usage as a symbol font}\\
With a symbol font, each character may be defined as a math symbol, but, unlike one created as a MathAlphabet, it uses up one of your 16 math families even if never used in the document. In symbol font mode, \verb|\mathbb| and its bold counterpart \verb|\mathbfbb| work as expected for digits as well as Roman alphabetic arguments.
The symbols defined with special names are the Greek and dotless characters with names
\begin{verbatim}
\bbdotlessi, \bbdotlessj or \imathbb, \jmathbb
\bbGamma ... \bbOmega
\bbalpha ... \bbomega
\end{verbatim}
and the delimiter symbols
\begin{verbatim}
\bbLbrack
\bbRbrack
\bbLangle
\bbRangle
\bbLparen
\bbRparen
\end{verbatim}

\textsc{Usage as a MathAlphabet}\\
The package defines (as text symbols only) the following macros for use with the Greek letters as well as {\tt dotlessi} and {\tt dotlessj}.
\begin{verbatim}
\txtbbdotlessi, \txtbbdotlessj
\txtbbGamma ... \txtbbOmega
\txtbbalpha ... \txtbbomega
\txtbbLparen \txtbbRparen \txtbbLbrack \txtbbRbrack \txtbbLangle \txtbbRangle
\end{verbatim}
as well as their ``bold'' counterparts
\begin{verbatim}
\txtbfbbdotlessi, \txtbfbbdotlessj
\txtbfbbGamma ... \txtbfbbOmega
\txtbfbbalpha ... \txtbfbbomega
\txtbfbbLparen \txtbfbbRparen \txtbfbbLbrack \txtbfbbRbrack \txtbfbbLangle \txtbfbbRangle
\end{verbatim}

To use these in math you could use the \verb|\text| command from {\tt amsmath}, like
\begin{verbatim}
\[\text{\txtbbalpha}+\mathbb{1}_\{\text{\txtbbbeta}>\delta\}}\]
\end{verbatim}
which renders in MathAlphabet mode as
\[\text{\txtbbalpha}+\mathbb{1}_{\{\text{\txtbbbeta}>\delta\}}\]
\textbf{\textsc{2: Through the {\tt mathalpha} package}}


Place one of the lines
\begin{verbatim}
\usepackage[bb=bboldx]{mathalpha}
\usepackage[bb=bboldx-light]{mathalpha}
\end{verbatim}
in your document preamble following any other lines that may load math alphabets. These have the same effect  as loading the {\tt bboldx} package described above. For example,
\begin{verbatim}
\usepackage[bb=bboldx-light,bbscaled=.97]{mathalpha}
\end{verbatim}
has the same effect as
\begin{verbatim}
\usepackage[light,scale=.97]{bboldx} 
\end{verbatim}
The options {\tt bbsymbols} and {\tt bfbb} to {\tt mathalpha} are simply passed through to the {\tt bboldx} package for processing.
({\tt Mathalpha} does of course allow you to set many other options for other math alphabets.)
\end{document}
 