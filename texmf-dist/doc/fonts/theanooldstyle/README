This is the README for the TheanoOldStyle package, 
version 2022/09/26.

This package provides the TheanoOldStyle font designed by
Alexey Kryukov, in both TrueType and Type1 formats, with
support for both traditional and oldstyle LaTeX processors. An
artificially-emboldened variant has been provided but there
are no italic variants. The package is named after Theano,
a famous Ancient Greek woman philosopher, who was first
a student of Pythagoras, and supposedly became his wife.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/theanooldstyle.tds.zip where the
preferred URL for "tex-archive" is http://mirrors.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file TheanoOldStyle.map.

To use, add

\usepackage{TheanoOldStyle}

to the preamble of your document. This will activate
TheanoOldStyle-Regular as the main (serifed) text font. Font
encodings supported are OT1, T1, TS1, LY1 and LGR. The default
figure style is lining but OldStyle figures may be
obtained by using the option osf (or oldstyle) or
the command \theanooldstyleosf. 

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

The fonts are licensed under the SIL Open Font License,
version 1.1; the text may be found in the doc directory.
The type1 versions were created using fontforge. The LaTeX
support files were created using autoinst and are licensed
under the terms of the LaTeX Project Public License.
The maintainer of this package is Bob Tennent (rdt at
cs.queensu.ca).
