This is the README for the CrimsonPro package, 
version 2022-09-30.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the CrimsonPro family of fonts (version 1.001),
designed by Sebastian Kosch and Jacques Le Bailly. 

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/CrimsonPro.tds.zip where the
preferred URL for "tex-archive" is http://mirrors.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file CrimsonPro.map.

To use, add

\usepackage{CrimsonPro}

to the preamble of your document. This will activate CrimsonPro
as the main (serifed) text font.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or who wish
to avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to scale the
fonts.

Options semibold (or sb), extrabold (or eb), and black 
(or k) will activate those variants as the default bold face.

Options medium (or m), light (or l) and extralight (or el)
will activate those variants as the default regular face.

Font encodings supported are OT1, T1, LY1 and TS1.

The TrueType fonts were obtained from

https://github.com/Fonthausen/CrimsonPro

in the fonts folder and are licensed under the SIL Open
Font License, version 1.1; the text may be found in the
doc directory. The type1 versions were created using
fontforge. The support files were created using autoinst and
are licensed under the terms of the LaTeX Project Public
License. The maintainer of this package is Bob Tennent 
(rdt at cs.queensu.ca)
