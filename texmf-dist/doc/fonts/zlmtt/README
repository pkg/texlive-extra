This small package provides a means of accessing all features (plus scaling) of Latin Modern Typewriter (lmtt) as Typewriter font family to accompany any other chosen font packages. For weight selection, the mweights code bis required. Since the 2020 revision of LaTeX, this code is present in the LaTeX kernel. It is also available in the mweights package for those without  a sufficiently recent LaTeX.

This material is subject to the LaTeX Project Public License. See 
http://www.ctan.org/license/lppl1.3
for the details of that license.

Current version: 1.032

Changes in version 1.032
1. Added code to zlmtt.sty to specify sub-encoding 0. (Latex assigned sub-encoding 9, preventing some textcomp glyphs from rendering correctly.)
2. Small changes to documentation.

Changes in version 1.031
Code and documentation enhancements. (Thanks Maurice Hansen.)

Changes in version 1.03
Change in handling the detection of mweights code. (Thanks Maurice Hansen.)

Changes in version 1.02
Modified the definitions of \proptt and \monott to take into account the font weight specified in the preamble. (Thanks, Frank Mittelbach.)

Changes in version 1.01
Added macro \lctt to access light-condensed weight.

Please send comments and bug reports or suggestions for improvement to

msharpe at ucsd dot edu