This small package provides a math package that amounts to modifications of the NotoSerif and NotoSans Roman and Greek letters with most symbols taken from newtxmath which must of course be installed (version 1.629 or higher) and its map file enabled.  The eight font files in this package are in pfb format, licensed under the SIL OFL, version 1.1. All other LaTeX support files are subject to the LaTeX Project Public License. See 
http://www.ctan.org/license/lppl1.3 for the details of that license. 
Copyright (c) 2020-2021 Michael Sharpe

Current version: 1.02 2021-03-24

Changes in version 1.02
Brought math blackboard bold alphabets into conformance with additions to newtxmath.

Changes in version 1.01
Added some Greek Blackboard glyphs.

Maintainer: Michael Sharpe, msharpe@ucsd.edu