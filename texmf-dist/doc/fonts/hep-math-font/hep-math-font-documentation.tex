%%
%% This is file `hep-math-font-documentation.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% hep-math-font-implementation.dtx  (with options: `documentation')
%% This is a generated file.
%% Copyright (C) 2019-2020 by Jan Hajer
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. The latest version of this license is in:
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.

\ProvidesFile{hep-math-font-documentation.tex}[2022/11/01 v1.1 hep-math-Font documentation]

\RequirePackage[l2tabu, orthodox]{nag}

\documentclass{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\AtBeginDocument{\MakeShortVerb{\"}}

\EnableCrossrefs
\CodelineIndex
\RecordChanges

\usepackage[parskip]{hep-paper}

\bibliography{bibliography}

\acronym{CM}{computer modern}
\acronym{LM}{latin modern}

\usepackage{hologo}
\usepackage{fonttable}

\MacroIndent=1.5em
\AtBeginEnvironment{macrocode}{\renewcommand{\ttdefault}{clmt}}

\GetFileInfo{hep-math-font.sty}

\title{The \software{hep-math-font} package\thanks{This document corresponds to \software{hep-math-font}~\fileversion.}}
\subtitle{Extended Greek and sans-serif math}
\author{Jan Hajer \email{jan.hajer@tecnico.ulisboa.pt}}
\date{\filedate}

\begin{document}

\newgeometry{vscale=.8, vmarginratio=3:4, includeheadfoot, left=11em, marginparwidth=4.6cm, marginparsep=3mm, right=7em}

\maketitle

\begin{abstract}
The \software{hep-math-font} package adjust the math fonts to be italic sans-serif if the document is sans-serif.
Additionally Greek letters are redefined to be always italic and upright in math and text mode, respectively.
Some math font macros are adjusted to give more consistently the naively expected results.
\end{abstract}

The package is loaded using "\usepackage{hep-math-font}".

If the document "\familydefault" font is switched to the sansserif "\sfdefault" font the math font is adjusted accordingly using fonts compatible to \LM and \CM.
\DescribeMacro{warning}
In order to be able to easily switch large chucks of math from serif to sans-serif documents the meaning of "\mathrm" and "\mathsf" is adjusted in this case so that the first generates upright sans-serif math and the second serif math.
This is is neither the literal meaning of the macros nor the best behaviour if a single large document is written in sans-serif.
However, it simplifies working in an environment where one copies pieces of math between serif and sans-serif documents \eg publications \vs talks and funding applications.

Using the \software{fixmath} \cite{fixmath} and \software{textalpha} \cite{textalpha} packages Greek letter are adjusted so that they are always italic and upright in math and text mode, respectively.
Greek letters can be written by using their unicode characters.

\DescribeMacro{symbols}
The "symbols"=\meta{family} class option sets the family of the symbol fonts.
"symbols=ams" loads the two \hologo{AmS} fonts \cite{amsfonts} and the \software{bm} bold fonts.
The default "symbols=true" replaces additionally the blackboard font with the \software{dsfont} \cite{dsfont}.
"symbols=minion" switches the symbol fonts to the Adobe MinionPro companion font from the \software{MnSymbol} package \cite{MnSymbol}.
"symbols=false" deactivates loading any additional symbol fonts, effectively restricting the package to only switch the math font according to the sans-serif property of the main text.

\section{Macros}

\DescribeMacro{\text}
\DescribeMacro{\mathrm}
The "\mathrm"\marg{math}  macro and the "\text"\marg{text} macro from \software{amstext} \cite{amstext} are adjusted to produce upright Greek letters, \ie ($ \text A  \text b  \text \Gamma \text \delta \text{\textbf A} \text{\textbf b} \text{\textbf \Gamma} \text{\textbf \delta}$), by adjusting the code from the \software{alphabeta} \cite{alphabeta} package.

\DescribeMacro{\mathbf}
Bold math, via "\mathbf" is improved with the \software{bm} package \cite{bm}, \ie ($ A  b  \Gamma \delta \mathbf A \mathbf b \mathbf \Gamma \mathbf \delta$).
Macros switching to "bfseries" such as "\section"\marg{text} are ensured to also typeset math in bold.

\DescribeMacro{\mathsf}
The math sans-serif alphabet is redefined to be italic sans-serif if the main text is serif and italic serif if the main text is sans-serif, \ie ($\mathsf A \mathsf b \mathsf \Gamma \mathsf \delta \mathbf{\mathsf A} \mathbf{\mathsf b} \mathbf{\mathsf \Gamma} \mathbf{\mathsf \delta}$).
Ensuring that the distinction between these fonts is also kept if the \prefix{sans}{serif} option of the document is switched.

\DescribeMacro{\mathscr}
The "\mathcal" font \ie ($\mathcal{ABCD}$) is accompanied by the "\mathscr" font \ie ($\mathscr{ABCD}$).

\DescribeMacro{\mathbb}
The "\mathbb" font is improved by the \software{doublestroke} package \cite{dsfont} and adjusted depending on the \prefix{sans}{serif} option of the document \ie ($\mathbb{Ah1}$).

\DescribeMacro{\mathtt}
The "\mathtt" macro switches to \LM typewriter font \ie ($\mathtt A \mathtt b \mathtt \Gamma \mathbf{\mathtt A} \mathbf{\mathtt b} \mathbf{\mathtt \Gamma}$).

\DescribeMacro{\mathfrak}
Finally, the "\mathfrak" font is also available \ie ($\mathfrak{AaBb12}$).

Details about the font handling in \hologo{TeX} can be found in \ccite{fntguide}.

\section{Math alphabet allocation} \label{sec:allocation}

\bgroup
\makeatletter
\renewcommand{\arraystretch}{0}
\setlength{\tabcolsep}{0pt}
\nodecimals
\nohexoct
\fntcolwidth=0pt
\setlength\arrayrulewidth{0pt}

\begin{figure}
\begin{panels}[t]{4}
\fonttable{rm-\ifhepmathfont@serif lmr\else lmss\fi10}
\caption{Text}
\panel
\fontrange{0}{127}
\fonttable{\ifhepmathfont@serif lm\else cmss\fi mi10}
\caption{Math}\vspace{2ex}
\fonttable{\ifhepmathfont@serif lm\else cmss\fi sy10}
\caption{Symbol}
\panel
\fontrange{0}{127}
\fonttable{\ifhepmathfont@serif\else ss\fi msam10}
\caption{AMS a}\vspace{2ex}
\fontrange{0}{79}
\fonttable{\ifhepmathfont@serif\else ss\fi msbm10}
\fontrange{96}{127}
\fonttable{\ifhepmathfont@serif\else ss\fi msbm10}
\caption{AMS b}
\panel
\fontrange{0}{8}
\fonttable{eufm10}
\fontrange{32}{127}
\fonttable{eufm10}
\caption{Euler fraktur}\vspace{2ex}
\fontrange{64}{95}
\fonttable{eusm10}
\caption{Euler caligraphy}\vspace{2ex}
\fonttable{MnSymbolS10}
\caption{Minion caligraphy}\vspace{2ex}
\fonttable{ds\ifhepmathfont@serif rom\else ss\fi10}
\caption{Doublestroke}
\end{panels}
\caption{Basic math fonts}
\end{figure}

\begin{figure}
\hspace*{-2cm}%
\begin{panels}[t]{.3}
\fontrange{0}{127}
\fonttable{cm\ifhepmathfont@serif\else ss\fi ex10}
\caption{Computer modern}
\panel{.22}
\fontrange{0}{143}
\fonttable{MnSymbolE5}
\caption{Mn Symbol E 1}
\panel{.6}
\fontrange{144}{215}
\fonttable{MnSymbolE5}
\caption{Mn Symbol E 2}\vspace{2ex}
\begin{minipage}{.48\linewidth}
\fontrange{0}{127}\fonttable{MnSymbolF10}
\caption{Mn Symbols F}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\fontrange{0}{47}\fonttable{\ifhepmathfont@serif\else ss\fi esint10}
\caption{Extended set of integrals}
\end{minipage}
\end{panels}
\caption{Math extension fonts}
\end{figure}

\begin{figure}
\begin{panels}[t]{.26}
\fonttable{MnSymbolA10}
\caption{Mn Symbol A}
\panel{.26}
\fonttable{MnSymbolB10}
\caption{Mn Symbol B}
\panel{.26}
\fonttable{MnSymbolC10}
\caption{Mn Symbol C}
\panel{.2}
\fonttable{MnSymbolD10}
\caption{Mn Symbol D}
\end{panels}
\makeatother
\caption{Minion symbol fonts}
\end{figure}
\egroup

Of the 16 available math alphabets, \hologo{TeX} loads four by default
\begin{enumdescript}[start=0,label=\arabic*)]
\item{OT1} \label{it:math text} Text (latin, upper case greek, numerals, text symbols)

The text font \ref{it:math text}\strut\ of \CM is \textbf{cmr10} "\OT1/cmr/m/n/10", which is replaced by \LM to be \textbf{rm-lmr10} "\OT1/lmr/m/n/10", the "sansserif" option uses \textbf{rm-lmss10} "\OT1/lmss/m/n/10".
\item{OML} \label{it:math italic} Math Italic (latin, greek, numerals, text symbols)

The italic math font \ref{it:math italic} of \CM is \textbf{cmmi10} "\OML/cmm/m/it/"\allowbreak"10", and is replaced by \LM to be \textbf{lmmi10} "\OML/lmm/m/it/10", the "sansserif" options uses \textbf{cmssmi10} "\OML/cmssrm/m/it/10" from the \software{sansmathfonts} package \cite{sansmathfonts}.
\item{OMS} \label{it:math symbol} Symbol ("\mathcal", operators)

The symbol font \ref{it:math symbol}\strut\ of \CM is \textbf{cmsy10} "\OMS/cmsy/m/n/10", and is replaced by \LM to be \textbf{lmsy10} "\OMS/lmsy/m/n/10", the "sansserif" options uses \textbf{cmsssy10} "\OMS/cmsssy/m/n/10" from the \software{sansmathfonts} package \cite{sansmathfonts}.
\item{OMX} \label{it:math extension} Math Extension (big operators, delimiters)

The extension font \ref{it:math extension}\strut\ of \CM is \textbf{cmex10} "\OMX/cmex/m/n/5", and is replaced by the \software{exscale} package \cite{exscale} to be \textbf{cmex10} "\OMX/cmex/m/n/10", the "sansserif" option loads \textbf{cmssex10} "\OMX/cmssex/m/n/10".
\end{enumdescript}

The \software{amssymb} (\software{amsfonts}) packages \cite{amssymb} load two more symbol fonts
\begin{enumdescript}[start=4,label=\arabic*)]
\item{msam10} \label{it:math ams a} "\U/msa/m/n/10" AMS symbol font A (special math operators)
\item{msbm10} \label{it:math ams b} "\U/msb/m/n/10" AMS symbol font B ("\mathbb", negated operators)
\end{enumdescript}
The "sansserif" option replaces them with \textbf{ssmsam10} "\U/ssmsa/m/n/10" and \textbf{ssmsbm10} "\U/ssmsb/m/n/10" from the \software{sansmathfonts} package \cite{sansmathfonts}, respectively.

The \software{bm} package \cite{bm} loads the bold version for the fonts \labelcref{it:math text,it:math italic,it:math symbol}.

Other math alphabets are only loaded on demand, \eg "\mathsf" uses a sans-serif font and "\mathbf" without the \software{bm} package uses a bold font.
The "\mathscr" macro uses the script font from the \software{mathrsfs} package \cite{mathrsfs}
\begin{enumdescript}[start=9,label=\arabic*)]
\item{rsfs10} "\U/rsfs/m/n/10" Math script font (capital letters)
\end{enumdescript}
The "\mathbb" macro loads the double stroke font from the \software{dsfont} package \cite{dsfont}, this can be prevented with the "symbols=ams" option.
\begin{enumdescript}[start=10,label=\arabic*)]
\item{dsrom10} "\U/dsrom/m/n/10" Double stroke font
\end{enumdescript}
The "\mathfrak" macro loads the fractur font from the \software{amssymb} package \cite{amssymb}
\begin{enumdescript}[start=11,label=\arabic*)]
\item{eufm10} "\U/euf/m/n/10" Math fraktur (Basic Latin)
\end{enumdescript}

The \software{hep-math-font} package uses nine of the available 16 math alphabets.
This number can be reduced by three using "\newcommand{\bmmax}{0}" from the \software{bm} package \cite{bm} and brought down to the default of four with the option "symbols=false".

The "symbols=minion" options replaces the fonts \labelcref{it:math symbol,it:math extension,it:math ams a,it:math ams b} with corresponding fonts from the \software{MnSymbol} package \cite{MnSymbol}.
Additionally, two more symbol alphabets are allocated, the \software{bm} package \cite{bm} loads one more font and now "\mathcal" triggers the use of one additional alphabet.
Hence, the minion option uses three to four more math alphabets than a usual setup.

\printbibliography

\end{document}

\endinput
%%
%% End of file `hep-math-font-documentation.tex'.
