

# The `hep-math-font` package

Extended Greek and sans-serif math

## Introduction

The `hep-math-font` package adjust the math fonts to be sans-serif if the document is sans-serif.
Additionally Greek letters are redefined to be always italic and upright in math and text mode respectively.
Some math font macros are adjusted to give more consistently the naively expected results.

The package is loaded using `\usepackage{hep-math-font}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

