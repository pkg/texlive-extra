This is the README for the librebodoni package, 
version 2022-09-18.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the Libre Bodoni family of fonts, designed by
Pablo Impallari (http://www.impallari.com) and Rodrigo
Fuenzalida (http://www.rfuenzalida.com). The Libre Bodoni
fonts are based on the 19th century Morris Fuller Benton's
ATF design.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/librebodoni.tds.zip where the
preferred URL for "tex-archive" is http://mirrors.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file LibreBodoni.map.

To use, add

\usepackage{LibreBodoni}

to the preamble of your document. These will activate Libre
Bodoni as the main (serifed) text font.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or who wish
to avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to scale the
fonts.

Font encodings supported are OT1, T1, LY1 and TS1.

Superior or inferior numbers are available using \sufigures,
\infigures, \textsu{...} or \textin{...}.

Local use of LibreBodoni is supported by the command

\librebodoni 

The OpenType fonts were obtained from

https://github.com/impallari/Libre-Bodoni

and are licensed under the SIL Open Font License, version
1.1; the text may be found in the doc directory. The type1
versions were created by otftotfm. The support files were
created using autoinst and are licensed under the terms of
the LaTeX Project Public License. The maintainer of this
package is Bob Tennent (rdt at cs.queensu.ca)
