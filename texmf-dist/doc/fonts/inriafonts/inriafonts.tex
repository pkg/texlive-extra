\documentclass{article}

\usepackage{ifluatex}
\ifluatex\else
\errmessage{This file requires luaLaTeX to compile.^^J^^J}
\fi

\usepackage[a4paper]{geometry}
\usepackage{fontspec}
\usepackage{hyperref}
\usepackage{multicol}
\usepackage{fancyvrb}
\usepackage{microtype}
\usepackage{hologo}
\usepackage{boxedminipage,xspace}
\newfontfamily{\inria}{Inria Serif}
\newfontfamily{\inriasf}{Inria Sans}


\title{\inria Inria fonts (serif \& sans-serif)}
\author{Nicolas Markey}
\date{\today}

\def\cmd#1{\texttt{\textbackslash#1}}
\def\pack#1{\texttt{#1}\xspace}

\def\lipsum{Lorem ipsum dolor sit amet, consectetuer
  adipiscing elit. Ut~purus elit, vestibulum~ut, placerat~ac,
  adipiscing vitae, felis. Curabitur dictum gravida mauris. Nam~arcu
  libero, nonummy eget, consectetuer~id, vulputate~a, magna. Donec
  vehicula augue eu neque.}


\begin{document}
\maketitle

\begin{abstract}
{\inria Inria} is a free font designed by
\href{https://black-foundry.com/blog/inria-serif-and-inria/}{Black[Foundry]}
for \href{http://www.inria.fr/}{Inria} research institute. The~font is
available for free.  It~comes Serif and Sans Serif, each with 3
weights and matching italics.

Using these fonts with \hologo{XeLaTeX} and \hologo{LuaLaTeX} is easy,
by using the \pack{fontspec} package; we~refer to the documentation of
\pack{fontspec} for more information.
%
The present package provides a way of using them with \hologo{LaTeX}
and \hologo{pdfLaTeX}: it~provides two style files,
\texttt{InriaSerif.sty} and \texttt{InriaSans.sty}, together with the
postscript version of the fonts and their associated files. These~were
created using \texttt{autoinst}.
\end{abstract}


\section{Installation}
To install on a TDS-compliant \TeX\ system, first download the
\texttt{inriafonts.tds.zip} file from CTAN, and unzip it at the root
of the texmf tree where you want to install the font. Then add maps
\pack{InriaSerif.map} and \pack{InriaSans.map} to \texttt{updmap}, and
update the font maps and list of files.


\section{Usage with \LaTeX\slash \hologo{pdfLaTeX}}

Simply use package \texttt{InriaSerif} (which sets the serif font as
the \cmd{rmdefault} font and the sans-serif font as the
\cmd{sfdefault} font) or \texttt{InriaSans} (which sets the sans-serif
font as \cmd{rmdefault}).

These packages admit the following (main) options (automatically
provided by \texttt{autoinst}):
\begin{itemize}
\item \texttt{lining} (default) and \texttt{oldstyle}: use lining
  glyphs (\bgroup\inria 1234567890\egroup) vs. old-style glyphs
  (\bgroup\inria\addfontfeature{Numbers=OldStyle}1234567890\egroup)
  for figures. This applies only to figures in text mode by default,
  since these packages do not affect the fonts of the math mode; use
  package \texttt{mathastext} to have the same fonts in math mode as
  in text mode.
\item \texttt{tabular} and \texttt{proportional} (default): (lining)
  figures may have fixed size
  (\bgroup\inria\addfontfeature{Numbers=Monospaced} 1234567890\egroup) or
  proportional size (\bgroup\inria 1234567890\egroup).
\item \texttt{regular} (default) and \texttt{light} font series;
\item \texttt{scale=$\mathtt{1.1}$} scales the font by the given factor.
\end{itemize}
Both packages provide superscript figures (in text mode) using
\cmd{textsu} and \cmd{sufigures}: for instance,
\cmd{textsu\string{abc1234567890\string}} produces
{\inria\addfontfeature{VerticalPosition=Superior}abc1234567890}
(notice how this only applies to figures, and not to letters).


\section{Samples and list of glyphs}

\enlargethispage{5mm}
On the next page, we present short samples for the serif- and
sans-serif versions of the font, for all three weights, and for their
italics shapes. Tables with full lists of glyphs (for regular serif
and sans-serif fonts) are displayed on the last two pages.



\newgeometry{textwidth=160mm,textheight=240mm}

\subsection{Inria Serif Light}

\bgroup\inria
  \addfontfeature{UprightFont={* Light}}
  \addfontfeature{ItalicFont={* Light Italic}}

  \begin{multicols}{2}
  \lipsum
  \par\itshape\lipsum
  \end{multicols}
\egroup

\subsection{Inria Serif Regular}

\bgroup\inria
  \addfontfeature{UprightFont={* Regular}}
  \addfontfeature{ItalicFont={* Italic}}
  \begin{multicols}{2}
  \lipsum
  \par\itshape\lipsum
  \end{multicols}
\egroup

\subsection{Inria Serif Bold}
\bgroup\inria
  \addfontfeature{UprightFont={* Bold}}
  \addfontfeature{ItalicFont={* Bold Italic}}
  \begin{multicols}{2}
  \lipsum
  \par\itshape\lipsum
  \end{multicols}
\egroup





\subsection{Inria Sans Light}

\bgroup\inriasf
  \addfontfeature{UprightFont={* Light}}
  \addfontfeature{ItalicFont={* Light Italic}}

  \begin{multicols}{2}
  \lipsum\medskip
  \par\itshape\lipsum
  \end{multicols}
\egroup

\subsection{Inria Sans Regular}

\bgroup\inriasf
  \addfontfeature{UprightFont={* Regular}}
  \addfontfeature{ItalicFont={* Italic}}
  \begin{multicols}{2}
  \lipsum\medskip
  \par\itshape\lipsum
  \end{multicols}
\egroup

\subsection{Inria Sans Bold}
\bgroup\inriasf
  \addfontfeature{UprightFont={* Bold}}
  \addfontfeature{ItalicFont={* Bold Italic}}
  \begin{multicols}{2}
  \lipsum
  \par\itshape\lipsum
  \end{multicols}
\egroup





\newgeometry{textwidth=175mm,textheight=269mm}

\subsection{List of glyphs -- Inria Serif Regular}
\newcount\charcount
\newcount\nbcharcount
\parindent=0pt

\bgroup\inria
\offinterlineskip
\def\dochar{\iffontchar\font\charcount\advance\nbcharcount1
  \makebox[15mm][l]{\strut\vrule\,{\tiny\number\charcount}\hfill\char\charcount}\hskip1mm plus 1mm
  \fi}
\loop\dochar
\ifnum\charcount<"10FFFF\relax
\advance\charcount1
\repeat
\global\advance\nbcharcount0
\egroup

\vskip5mm
(\the\nbcharcount\ chars displayed)



\subsection{List of glyphs -- Inria Sans Regular}
\newcount\charcount
\newcount\nbcharcount
\parindent=0pt

\bgroup\inriasf
\offinterlineskip
\def\dochar{\iffontchar\font\charcount\advance\nbcharcount1
  \makebox[15mm][l]{\strut\vrule\,{\tiny\number\charcount}\hfill\char\charcount}\hskip1mm plus 1mm
  \fi}
\loop\dochar
\ifnum\charcount<"10FFFF\relax
\advance\charcount1
\repeat
\global\advance\nbcharcount0
\egroup

\vskip5mm
(\the\nbcharcount\ chars displayed)



\end{document}
