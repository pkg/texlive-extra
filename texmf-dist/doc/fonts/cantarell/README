------------------------
 Cantarell, version 3.1
------------------------

Cantarell is a contemporary humanist sans serif, and is used by the GNOME
project for its user interface.

Cantarell was originally designed by Dave Crossland as part of his coursework
for the MA Typeface Design program at the Department of Typography in the
University of Reading, England. After the GNOME project adopted the typeface in
November 2010, minor modifications and slight expansions were made to it over
the years, notably by Jakub Steiner. Pooja Saxena initially worked on the
typeface as a participant of the GNOME outreach program and later developed her
own Devanagari typeface Cambay, which included a redesigned latin version of
Cantarell. It was backported to the GNOME branch of Cantarell by Nikolaus
Waxweiler, who also performed other janitorial tasks on it.

This font family, delivered under the OFL version 1.1, is available on the GNOME
download server as CFF-flavored OpenType files.

This package provides support for Cantarell in LaTeX, including XeLaTeX and
LuaLaTeX. It includes the original OpenType fonts, as well as Type 1 versions,
converted for this package using cfftot1 for full support with LaTeX and Dvips.

This package is released under the LaTeX Project Public License, either version
1.3c or above, with the exception of the .pfb and .sfd files, released under the
Open Font License version 1.1.

If you have comments about the package, please contact Mohamed El Morabity
(melmorabity AT fedoraproject DOT org).
