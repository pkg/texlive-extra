LXfonts bundle version 2.0b OF 2013-12-07

(C) Claudio Beccari 2007-2013

This work is subject to the LaTeX Project Public Licence (LPPL), version 1.3 
or any other successive version as you like; the LPPL licence is downloadable
from any CTAN archive.

This package contains a revival of the slide fonts created long ago by Leslie 
Lamport, in his initial distribution of LaTeX209, for typesetting slides on 
methacrylate transparencies. 

The advent of the T1 Cork encoding brought in the T1 encoded slides fonts.

New packages for beamer presentations became available after the availability
of the typesetting engine pdftex; therefore the usage of the very legible 
fonts of the old SliTeX format were abandoned.

This bundle offers a revival of those fonts so as to eliminate the inconveniences 
of the modest initial set-up; they include now all the math fonts, including the 
AMS ones. Text fonts are complemented with the Text Companion fonts. These fonts 
are coherent with the slide fonts of the Greek CB font collection, so that slides 
can be typeset in Greek script also.

An enclosed demo file, typeset with the beamer class, is available for documentation 
and for evaluating the benefits of these revieved fonts.

Installation is best described in the accompanying file Lxfonts.readme file.

