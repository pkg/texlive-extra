This is the manifest file for the package lxfonts, version 2.0

(c) Claudio Beccari 2007-2013

This work is subject to the LaTeX Project Public Licence (LPPL), version 1.3
or any other successive version as you like; the LPPL licence is downloadable
from any CTAN archive.

The work includes the following files:

lxfonts.dtx and the derived files .pdf, .sty, and .fd obtained by extracting
them by running pdfLaTeX on it; there is no .ins file because the .dtx file
is an auto extracting one.

The .mf font source files, .tfm metric files and .pfb Type 1 vector files
are also covered by the above licence, together with the .map file necessary
to use in order that pdfLaTeX and other programs may use these LXfonts.

Read the LXfonts.readme (text) file for detailed installation instructions.


This work has the status of author maintained; for suggestions, bugs, and the
like, write to

claudio dot beccari at gmail dot com

Claudio Beccari



