\documentclass{article}

\usepackage[OT2, TS1, X2, ECMSRB1, ECMSRB2, T2A, T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,top=2.0cm,left=3cm,right=2.5cm,bottom=2cm,includefoot,includehead]{geometry}

\usepackage{cmsrb}
\usepackage{cmupint}
\let\enint\intop

\usepackage{amssymb}

\input glyphtounicode.tex
\pdfgentounicode=1

\usepackage{fancyvrb}
\usepackage{tabularx}

\newcommand\textcmsuper[1]{{\fontencoding{T2A}\fontfamily{cmr}\selectfont #1}}
\newcommand{\example}[1]{{\fontencoding{T2A}\selectfont #1} & \textcmsuper{#1}}

\newcommand\otinput[1]{{\fontencoding{T1}\selectfont \verb"#1"} & {\fontencoding{OT2}\selectfont #1}}
\newcommand\ecminput[1]{{\fontencoding{T1}\selectfont \verb"#1"} & {\fontencoding{ECMSRB1}\selectfont #1} & {\fontencoding{ECMSRB2}\selectfont #1}}

\DeclareTextSymbolDefault{\dj}{T1}

\newcounter{primer}
\newcounter{rezultat}
\newenvironment{primer}{\VerbatimEnvironment\refstepcounter{primer}\par\medskip
	\noindent\textbf{Example~\theprimer: }
	\begin{Verbatim}
	}{\end{Verbatim}\medskip}
\newenvironment{rezultat}{\refstepcounter{rezultat}\par\medskip
	\noindent\textbf{Result~\therezultat: }\\ }{\medskip}

\makeatletter
\newcommand{\manuallabel}[2]{\def\@currentlabel{#2}\label{#1}}
\makeatother

\usepackage{fonttable}

\begin{document}
	\title{The \texttt{cmsrb} package}
	\author{Uroš Stefanović\footnote{\texttt{urostajms@gmail.com}}}
	\date{\today{} v4.0}
	\maketitle
	
	\section{Why \textsf{cmsrb}?}
	
	The \textsf{cm-super} package provides great support for Cyrillic script in various languages, but there's a problem with italic variants of some letters for Serbian and Macedonian. The \textsf{cmsrb} package includes the correct shapes for italic letters \verb|\cyrg|, \verb|\cyrd|, \verb|\cyrp|, \verb|\cyrt| and letter \verb|\cyrb|. This package also has some improvements in letters and accents used in Serbian language.
	
	\section{Package Features}
	
	The \textsf{cmsrb} fonts are the extensions of the Computer Modern fonts for Serbian and Macedonian languages for \TeX{} (\LaTeX). Support files are provided for \texttt{T1}, \texttt{TS1}, \texttt{T2A}, \texttt{X2} and \texttt{OT2} encodings, but also for the experimental \texttt{ECMSRB1} and \texttt{ECMSRB2} encodings. Supported font styles are serif, sans-serif and mono, for size 10pt.
	
	This package is very simple to use: just put
	\begin{verbatim}
	\usepackage{cmsrb}
	\end{verbatim}
	in preamble of the document.
	
	\begin{table}[h!]
		\newcolumntype{C}{>{\centering\arraybackslash}X}%
		\begin{tabularx}{\textwidth}{|C C|C C|C C|}
			\hline
			cmsrb & cm-super & cmsrb & cm-super & cmsrb & cm-super  \\ \hline
			\hline
			\example{\textit{бгдпт}} & \example{\textbf{\textit{бгдпт}}} & \example{ђћ} \\ \hline
			\example{б\textbf{б}\textit{б}\textsl{б}} & \example{\={а}\textsc{\={а}\f{и}}} & \example{\textit{đ\textbf{đ}}} \\ \hline
		\end{tabularx}
		\caption{Some \textsf{cm-super} and \textsf{cmsrb} differences.}\label{t2}
	\end{table}
	
	\section{OT2 Features}
	
	The \texttt{OT2} encoding is modified to match the rules of Latin to Cyrillic transcription in Serbian language. Therefore, the \textsf{ts}, \textsf{kh}, \textsf{ch} and similar ligatures are removed from the encoding (see Table~\ref{t1}).
	
	In \texttt{OT2} encoding Macedonian letters {\fontencoding{T2A}\selectfont Ѓ} and {\fontencoding{T2A}\selectfont Ќ} are added, with Montenegrin letters {\fontencoding{T2A}\selectfont\CYRSJE} and {\fontencoding{T2A}\selectfont\CYRZJE}, and also some orthographic symbols are included.
	
	However, it is not recommended to use the \texttt{OT2} encoding; better choice for Serbian language is \texttt{T2A} encoding and utf8 input. One can use \texttt{OT2} encoding if they already have a document written in Latin---in which case it would be easy to transcribe it to Cyrillic.
	
	\begin{table}
	\newcolumntype{C}{>{\centering\arraybackslash}X}%
	\begin{tabularx}{\textwidth}{|C C|C C|C C|C C|}
	\hline
	Input & Output & Input & Output & Input & Output & Input & Output \\ \hline
	\hline
	\otinput{A} & \otinput{B} & \otinput{C} & \otinput{D} \\ \hline
	\otinput{E} & \otinput{F} & \otinput{G} & \otinput{H} \\ \hline
	\otinput{I} & \otinput{J} & \otinput{K} & \otinput{L} \\ \hline
	\otinput{M} & \otinput{N} & \otinput{O} & \otinput{P} \\ \hline
	\otinput{Q} & \otinput{R} & \otinput{S} & \otinput{T} \\ \hline
	\otinput{U} & \otinput{V} & \otinput{W} & \otinput{X} \\ \hline
	\otinput{Y} & \otinput{Z} & \otinput{\#} & \otinput{} \\ \hline
	\hline
	\otinput{a} & \otinput{b} & \otinput{c} & \otinput{d} \\ \hline
	\otinput{e} & \otinput{f} & \otinput{g} & \otinput{h} \\ \hline
	\otinput{i} & \otinput{j} & \otinput{k} & \otinput{l} \\ \hline
	\otinput{m} & \otinput{n} & \otinput{o} & \otinput{p} \\ \hline
	\otinput{q} & \otinput{r} & \otinput{s} & \otinput{t} \\ \hline
	\otinput{u} & \otinput{v} & \otinput{w} & \otinput{x} \\ \hline
	\otinput{y} & \otinput{z} & \otinput{+} & \otinput{} \\ \hline
	\hline
	\otinput{C1} & \otinput{D1} & \otinput{D2} & \otinput{D3} \\ \hline
	\otinput{E0} & \otinput{E1} & \otinput{E2} & \otinput{I0} \\ \hline
	\otinput{I1} & \otinput{J1} & \otinput{J2} & \otinput{L1} \\ \hline
	\otinput{N0} & \otinput{N1} & \otinput{P1} & \otinput{P2} \\ \hline
	\otinput{Z1} & \otinput{\v{C}} & \otinput{\'C} & \otinput{\DJ} \\ \hline
	\otinput{\v{S}} & \otinput{\v{Z}} & \otinput{LJ} & \otinput{Lj} \\ \hline
	\otinput{NJ} & \otinput{Nj} & \otinput{D\v{Z}} & \otinput{D\v{z}} \\ \hline
	\hline
	\otinput{c1} & \otinput{d1} & \otinput{d2} & \otinput{d3} \\ \hline
	\otinput{e0} & \otinput{e1} & \otinput{e2} & \otinput{i0} \\ \hline
	\otinput{i1} & \otinput{j1} & \otinput{j2} & \otinput{l1} \\ \hline
	\otinput{\i} & \otinput{n1} & \otinput{p1} & \otinput{p2} \\ \hline
	\otinput{z1} & \otinput{\v{c}} & \otinput{\'c} & \otinput{\dj} \\ \hline
	\otinput{\v{s}} & \otinput{\v{z}} & \otinput{lj} & \otinput{} \\ \hline
	\otinput{nj} & \otinput{} & \otinput{d\v{z}} & \otinput{} \\ \hline
	\hline
	\otinput{\char20} & \otinput{\char21} & \otinput{\char28} & \otinput{\char29} \\ \hline
	\otinput{\'G} & \otinput{\'K} & \otinput{\'g} & \otinput{\'k} \\ \hline
	\otinput{\'S} & \otinput{\'Z} & \otinput{\'s} & \otinput{\'z} \\ \hline
	\end{tabularx}
	\caption{\texttt{OT2} encoding in \textsf{cmsrb} package.}\label{t1}
	\end{table}

\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[OT2,T1]{fontenc}
\usepackage[serbian]{babel}
\newcommand{\test}%
{Ljubazni fenjerd\v zija \v ca\dj avog lica ho\'ce da mi poka\v ze \v stos.}
\begin{document}
\test \\
\fontencoding{OT2}\selectfont \test \\
Akcenti: \'a\`a\C a\f a\=a\^a\"a\u a
\end{document}
\end{primer}

\begin{rezultat}
\indent {\fontencoding{T1}\selectfont Ljubazni fenjerd\v zija \v ca\dj avog lica ho\'ce da mi poka\v ze \v stos.} \\
{\fontencoding{OT2}\selectfont Ljubazni fenjerd\v zija \v ca\dj avog lica ho\'ce da mi poka\v ze \v stos. \\ Akcenti: \'a\`a\C a\f a\=a\^a\"a\u a}
\end{rezultat}

\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[OT2,T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[serbian]{babel}
\newcommand{\test}%
{Ljubazni fenjerdžija čađavog lica hoće da mi pokaže štos.}
\begin{document}
\textit{\test} \\
\fontencoding{OT2}\selectfont \textit{\test}
\end{document}
\end{primer}

\begin{rezultat}
\indent{\fontencoding{T1}\selectfont \textit{Ljubazni fenjerd\v zija \v ca\dj avog lica ho\'ce da mi poka\v ze \v stos.}} \\
{\fontencoding{OT2}\selectfont \textit{Ljubazni fenjerd\v zija \v ca\dj avog lica ho\'ce da mi poka\v ze \v stos.}}
\end{rezultat}

	\section{T1 Features}

	The \texttt{T1} encoding now support conversion from Cyrillic to Latin script (see Example~\ref{prim:t1}).

	On the position 208 now is the letter Đ (\texttt{U+0110}) instead of the letter {\fontencoding{TS1}\selectfont \DH} (\texttt{U+00D0}), which enable copying and searching in the Serbian or Croatian Latin text.

\fontencoding{T2A}\selectfont
\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[T2A,T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[serbian]{babel}
\begin{document}
Љубазни фењерџија чађавог лица хоће да ми покаже штос.
\end{document}
\end{primer}
\manuallabel{prim:t1}{\theprimer}
\fontencoding{T1}\selectfont

\begin{rezultat}
\indent Ljubazni fenjerdžija čađavog lica hoće da mi pokaže štos.
\end{rezultat}

	\section{T2A Features}
	
	The Cyrillic letters {\fontencoding{T2A}\selectfont\texttt{Ј}} and {\fontencoding{T2A}\selectfont\texttt{ј}} are not supported in the \texttt{T2A} encoding, so they are placed instead of the Latin letters \texttt{J} and \texttt{j}.
	This give us the better results in the Cyrillic text, but that also means this Latin letter are not present in the \texttt{T2A} encoding, so be carefull during searching and copying Latin text in this encoding.
	
	The letters {\fontencoding{X2}\selectfont \char"81, \char"89, \char"86, \char"91, \char"A1, \char"A9, \char"A6, \char"B1} are removed from the \texttt{T2A} encoding, and on their places now are the letters {\fontencoding{T2A}\selectfont \char"81, \char"89, \char"86, \char"91, \char"A1, \char"A9, \char"A6, \char"B1}.

\fontencoding{T2A}\selectfont
\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[serbianc]{babel}
\DeclareTextSymbolDefault{\dj}{T1}
\begin{document}
\textit{Ljubazni fenjerdžija čađavog lica hoće da mi pokaže štos.\\
Љубазни фењерџија чађавог лица хоће да ми покаже штос.} \\
Акценти: \'{и}\`{и}\C{и}\f{и}\={и}\^{и}\"{и}\u{и}
\end{document}
\end{primer}

\begin{rezultat}
\indent {\textit{Ljubazni fenjerdžija čađavog lica hoće da mi pokaže štos.\\ Љубазни фењерџија чађавог лица хоће да ми покаже штос.} \\ Акценти: \'{и}\`{и}\C{и}\f{и}\={и}\^{и}\"{и}\u{и}}
\end{rezultat}

\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[serbianc]{babel}
\begin{document}
\textit{абвгдђѓежз\'{з}ѕијклљмнњопрс\'{с}тћќуфхцчџш}
\end{document}
\end{primer}

\begin{rezultat}
\indent {\textit{абвгдђѓежз\'{з}ѕијклљмнњопрс\'{с}тћќуфхцчџш}}
\end{rezultat}
\fontencoding{T1}\selectfont

\section{TS1 Features}

In the \texttt{TS1} encoding we can find the letter {\fontencoding{TS1}\selectfont \DH} (\texttt{U+00D0}), which is removed from the \texttt{T1} encoding.
Also, in the \texttt{TS1} encoding now are the letters {\fontencoding{T2A}\selectfont б, г, д, п, т} with Russian italic shape.

\fontencoding{T2A}\selectfont
\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[TS1,T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[serbianc]{babel}
\begin{document}
\textit{бгдпт} \fontencoding{TS1}\selectfont \textit{бгдпт \DH}
\end{document}
\end{primer}

\begin{rezultat}
\indent \textit{бгдпт} {\fontencoding{TS1}\selectfont \textit{бгдпт \DH}}
\end{rezultat}
\fontencoding{T1}\selectfont

	\section{Math}
	
	The \textsf{cmsrb} package load default Computer Modern math fonts, but load only fonts with size lower or equal 10pt, for better visual effect.
	If the option \textsf{math} is used, then the package \textsf{cmupint} will be loaded, and the integral sign will be changed from $\enint$ to $\int$ (upright integral is traditionally used in Serbian language, with \verb|\limits| option).
	Also, in Serbian language is better to use the symbols $\leqslant$ and $\geqslant$ than the symbols $\leq$ и $\geq$. If the \textsf{math} option is used the package \textsf{amssymb} will be loaded and this symbols will be changed (also $\nleqslant$ and $\ngeqslant$ will be used).
	
	We use this option like this:
	\begin{verbatim}
	\usepackage[math]{cmsrb}
	\end{verbatim}
	
\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\newcommand{\ud}{\,\mathrm{d}}
\begin{document}
$$ \int_0^1 e^x\ud x \geq 0 $$
\end{document}
\end{primer}

\begin{rezultat}
$$ \enint\nolimits_0^1 e^x\,\mathrm{d} x \geq 0 $$
\end{rezultat}

\begin{primer}
\documentclass{article}
\usepackage[math]{cmsrb}
\newcommand{\ud}{\,\mathrm{d}}
\begin{document}
$$ \int_0^1 e^x\ud x \geq 0 $$
\end{document}
\end{primer}

\begin{rezultat}
$$ \int_0^1 e^x\,\mathrm{d} x \geqslant 0 $$
\end{rezultat}

	\section{Experimental encodings}

	The encodings \texttt{ECMSRB1} and \texttt{ECMSRB2} are created for this package only. They are similar to \texttt{OT2} encoding, but they contain specific ligatures and accents (see Table~\ref{t3}). For the complete list of characters used in this encodings see Tables on Pages~\pageref{ecs1} and \pageref{ecs2}. The purposes of this encodins are writting Serbian text using only \texttt{ASCII} characters, and making simple conversions from Latin to Cyrillic.
	
	\begin{table}[h!]
		\newcolumntype{C}{>{\centering\arraybackslash}X}%
		\begin{tabularx}{\textwidth}{|C C C|C C C|C C C|}
			\hline
			Input & \texttt{ECMSRB1} & \texttt{ECMSRB2} & Input & \texttt{ECMSRB1} & \texttt{ECMSRB2} &Улаз & \texttt{ECMSRB1} & \texttt{ECMSRB2} \\ \hline
			\hline
			\ecminput{CX} & \ecminput{CY} & \ecminput{DX} \\ \hline
			\ecminput{DY} & \ecminput{EY} & \ecminput{GY} \\ \hline
			\ecminput{KY} & \ecminput{LJ} & \ecminput{LY} \\ \hline
			\ecminput{NJ} & \ecminput{NY} & \ecminput{SX} \\ \hline
			\ecminput{SXY} & \ecminput{ZX} & \ecminput{ZY} \\ \hline
			\ecminput{ZXY} & \ecminput{\EZH} & \ecminput{} \\ \hline
			\hline
			\ecminput{Cx} & \ecminput{Cy} & \ecminput{Dx} \\ \hline
			\ecminput{Dy} & \ecminput{Ey} & \ecminput{Gy} \\ \hline
			\ecminput{Ky} & \ecminput{Lj} & \ecminput{Ly} \\ \hline
			\ecminput{Nj} & \ecminput{Ny} & \ecminput{Sx} \\ \hline
			\ecminput{Sxy} & \ecminput{Zx} & \ecminput{Zy} \\ \hline
			\ecminput{Zxy} & \ecminput{} & \ecminput{} \\ \hline
			\hline
			\ecminput{cx} & \ecminput{cy} & \ecminput{dx} \\ \hline
			\ecminput{dy} & \ecminput{ey} & \ecminput{gy} \\ \hline
			\ecminput{ky} & \ecminput{lj} & \ecminput{ly} \\ \hline
			\ecminput{nj} & \ecminput{ny} & \ecminput{sx} \\ \hline
			\ecminput{sxy} & \ecminput{zx} & \ecminput{zy} \\ \hline
			\ecminput{zxy} & \ecminput{\ezh} & \ecminput{} \\ \hline
			\hline
			\ecminput{\`a} & \ecminput{\'a} & \ecminput{\^a} \\ \hline
			\ecminput{\T a} & \ecminput{\D a} & \ecminput{\H a} \\ \hline
			\ecminput{\r a} & \ecminput{\R a} & \ecminput{\v a} \\ \hline
			\ecminput{\u a} & \ecminput{\U a} & \ecminput{\=a} \\ \hline
			\ecminput{\b a} & \ecminput{\.a} & \ecminput{\d a} \\ \hline
			\ecminput{\c a} & \ecminput{\k a} & \ecminput{\"a} \\ \hline
			\ecminput{\C a} & \ecminput{\~a} & \ecminput{\f a} \\ \hline
		\end{tabularx}
		\caption{Encodings \texttt{ECMSRB1} and \texttt{ECMSRB2} for the package \textsf{cmsrb}.}\label{t3}
	\end{table}
	
\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[ECMSRB1]{fontenc}
\usepackage[serbian]{babel}
\begin{document}
Ljubazni fenjerdxija cxadyavog lica hocye da mi pokazxe sxtos.
\end{document}
\end{primer}

\begin{rezultat}
\indent {\fontencoding{ECMSRB1}\selectfont Ljubazni fenjerdxija cxadyavog lica hocye da mi pokazxe sxtos.}
\end{rezultat}

\begin{primer}
\documentclass{article}
\usepackage{cmsrb}
\usepackage[ECMSRB2]{fontenc}
\usepackage[serbian]{babel}
\begin{document}
Ljubazni fenjerdxija cxadyavog lica hocye da mi pokazxe sxtos.
\end{document}
\end{primer}

\begin{rezultat}
\indent {\fontencoding{ECMSRB2}\selectfont Ljubazni fenjerdxija cxadyavog lica hocye da mi pokazxe sxtos.}
\end{rezultat}


    \section{Version history}

	\subsection*{4.0}

	\begin{itemize}
		\item All options are removed, the option \textsf{math} is added.
		\item Fixed encodings \texttt{T1}, \texttt{T2A}, \texttt{TS1}, \texttt{OT2}.
		\item Letter {\fontencoding{T2A}\selectfont\textsf{б}} is significantly improved in all styles.
		\item Experimental encodings \texttt{ECMSRB1} and \texttt{ECMSRB2} are added.
	\end{itemize}
    
    \subsection*{3.1}
    
    \begin{itemize}
    	\item Bugs fixed.
    	\item Options \textsf{noint} and \textsf{nosymb} are active.
    	\item Symbols $\leq$, $\geq$, $\nleq$, $\ngeq$ are changed.
    \end{itemize}
    
    \subsection*{3.0}
    
    \begin{itemize}
    	\item Letter б is modificated.
    	\item Cyrillic to Latin conversion is added.
    	\item Math fonts are changed (\textsf{nomath} option is active).
    	\item Upright integral sign is added.
    \end{itemize}
    
    \subsection*{2.0}
    
    \begin{itemize}
    	\item Added support for the letters {\fontencoding{OT2}\selectfont \'g, \'k, \'s, \'z}.
    \end{itemize}
    
    \subsection*{1.1}
    
    \begin{itemize}
    	\item The separate \verb|.map| files merged into one.
    \end{itemize}

	\pagebreak

	\appendix

	\section{Encoding ECMSRB1}\label{ecs1}

	\fonttable{ecmsrb1cmsrbrr}

	\pagebreak

	\section{Encoding ECMSRB2}\label{ecs2}

	\fonttable{ecmsrb2cmsrbrr}
	
\end{document}
