The cmsrb font package
version 4.0 (March 30, 2020)
Uroš Stefanović

This material is subject to the GNU General Public License.

The cmsrb family provides Adobe Type 1 Computer Modern font for Serbian and Macedonian language.
Supported encodings are: T1, T2A, TS1, X2 and OT2; plus the experimental encodings ECMSRB1 and ECMSRB2.
The cmsrb package includes the correct shapes for italic letters \cyrb, \cyrg, \cyrd, \cyrp and \cyrt.

This package is very simple to use: just put
	\usepackage{cmsrb}
or
	\usepackage[math]{cmsrb}
in preamble of the document.

See 'cmsrb.pdf' of 'cmsrb-SR.pfd' for more informations.

Happy TeXing!
