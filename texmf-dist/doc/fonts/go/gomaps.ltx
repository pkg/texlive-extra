\documentstyle[11pt,go]{article}

\textwidth=6.5truein
\textheight=8.9truein
\oddsidemargin=0pt
\topmargin=-24pt
\font\gosign=gosign50 at 50truept

\title{Go diagrams with \TeX \\[6pt] \gosign\char0}
\author{Hanna Ko{\l}odziejska\thanks{I wrote this paper in April 1990,
                during my work in the Institute of Informatics at the
                Warsaw University.} \\
       {\sf MacroSoft}, Ltd. \\
       ul.~Chro\'scickiego 49 \\
       02-414 Warsaw, Poland}

\begin{document}
\maketitle

\noindent
Encoureged by Zalman Rubinstein, who described his chess
diagrams in TUGboat vol.~10 no.~2 [1], I have prepared some
special fonts and \TeX\ macros to be used in typesetting go
diagrams. For all the people who have never yet played go I found
the following introduction to the game~[2]:

\begin{quotation}
\em
Go is one of the most ancient, interesting, and rewarding of all
board games. [\ldots] It is played on a wooden board marked with
nineteen vertical and nineteen horizontal lines. The pieces used
are disks of slate and white shell slightly more than two
centimeters in diameter. These, even made of plastic or glass,
as in mass-produced sets, are called stones. They are played on
the intersections formed by the lines on the board, not within
the squares. The board is empty at the beginning of the game,
and the two players take turns placing stones on it one at a
time, one player playing black the other playing white. Once
played, a stone remains in its place, not moving about from
point to point.\par
\end{quotation}

\noindent
Go diagrams are easy to read. \textblack{1} is the first stone
played, \textwhite{2} the second, and so on.

In order to facilitate inserting go diagrams in a text both in
Plain \TeX\ and \LaTeX, I decided to generate with Metafont all
the symbols needed, even lines and circles, and to put them in
three kinds of fonts:
\begin{enumerate}
\item fonts with black stones, eg.\ go1bla10, go2bla10 (go black stones at 10pt);
\item fonts with white stones, eg.\ go1whi10, go2whi10 (go white stones at 10pt);
\item fonts with additional symbols, like intersections of
      lines, border lines, etc., eg.\ go10 (go symbols at 10pt).
\end{enumerate}
Probably two more fonts will be needed with black and white
stones numbered over~255, because games which last over
300~moves are not seldom!

The macros for coding go diagrams are gathered in the `go.sty'
file. In the macros each line intersection is identified by the
row label (one of the letters: a, b, c, d, e, f, g, h, i, k, l,
m, n, o, p, q, r, s,~t) and the column number (from 1 to~19).
After issuing the command:
\begin{center}
  \verb+\input go.sty+
\end{center}
the current go diagram is initialized (with no stones on it).
Later in your text you can clear the whole diagram or only a
part of it by introducing one of the commands:
\begin{center}
  \verb+\inifulldiagram+
\end{center}
or
\begin{center}
  \verb+\inidiagram+ \it with parameters.
\end{center}
For example, {\tt \verb+\inifulldiagram+} is equivalent to:
\begin{center}
  \verb*+\inidiagram a-t:1-19 +
\end{center}
(with a space limiting the fourth parameter).

The same rule stands also for showing diagrams:
\begin{center}
  \verb+\showfulldiagram+
\end{center}
is equivalent to:
\begin{center}
  \verb*+\showdiagram a-t:1-19 +
\end{center}
Partial diagrams are often used to show go problems, their
solutions and different variations of moves.

Putting a stone on the board is coded by the command:
\begin{center}
  \verb+\pos+ \it with parameters.
\end{center}

Lets consider an example: a problem to solve (Dia.~1) and its
solution (Dia.~2).

\gofontsize{20}
\pos{a}{5}=\white.
\pos{a}{6}=\black.
\pos{b}{3}=\white.
\pos{b}{4}=\white.
\pos{b}{5}=\white.
\pos{b}{6}=\black.
\pos{c}{3}=\white.
\pos{c}{4}=\black.
\pos{c}{5}=\black.
\pos{d}{3}=\white.
\pos{e}{2}=\black.
\pos{e}{3}=\black.
\pos{e}{4}=\black.
\pos{e}{6}=\black{\triangle}
\begin{figure}[htb]
$$
\showdiagram a-g:1-9
$$
\begin{center} Dia. 1 \end{center}
\end{figure}

\vspace*{-12truept}
\begin{verbatim}
\input go.sty          %  inputs macros
\gofontsize{20}        %  chooses the size of stones and other
                       %  symbols (default=10pt)
\pos{a}{5}=\white.     %  puts a white stone (without any number)
\pos{a}{6}=\black.     %  puts a black stone (without any number)
                       %  on the a6 intersection
\pos{b}{3}=\white.
\pos{b}{4}=\white.
\pos{b}{5}=\white.
\pos{b}{6}=\black.
\pos{c}{3}=\white.
\pos{c}{4}=\black.
\pos{c}{5}=\black.
\pos{d}{3}=\white.
\pos{e}{2}=\black.
\pos{e}{3}=\black.
\pos{e}{4}=\black.
\pos{e}{6}=\black{\triangle}  %  puts a black stone with
                              %  a triangle
$$                     %  centering
\showdiagram a-g:1-9   %  the result is shown in Dia. 1
$$
\pos{b}{1}=\black{1}   %  puts a black stone with 1
\pos{c}{2}=\letter{b}  %  puts a letter `b' on
                       %  the c2 intersection
\pos{d}{2}=\letter{a}
\gofontsize{10}        %  changes the size of stones and
                       %  other symbols
$$
\showfulldiagram       %  as in Dia. 2.
$$
\inifulldiagram        %  clears a board
\end{verbatim}
\smallskip

\pos{b}{1}=\black{1}
\pos{c}{2}=\letter{b}
\pos{d}{2}=\letter{a}
\gofontsize{10}
\begin{figure}[htb]
$$
\showfulldiagram
$$
\begin{center} Dia. 2 \end{center}
\end{figure}
\inifulldiagram

\noindent
An example of a real game is shown in Dia.~3 [3].

% DIAGRAM 3
%
\pos{a}{1}=\white{194}
\pos{a}{3}=\black{191}
\pos{a}{5}=\black{187}
\pos{a}{6}=\white{186}
\pos{a}{8}=\black{181}
\pos{a}{9}=\white{190}
\pos{a}{12}=\black{175}
%
\pos{b}{1}=\black{135}
\pos{b}{2}=\white{184}
\pos{b}{3}=\white{182}
\pos{b}{4}=\black{183}
\pos{b}{5}=\black{193}
\pos{b}{6}=\white{178}
\pos{b}{8}=\black{179}
\pos{b}{9}=\white{172}
\pos{b}{10}=\white{174}
\pos{b}{12}=\black{93}
\pos{b}{13}=\white{92}
\pos{b}{14}=\white{142}
%
\pos{c}{1}=\white{132}
\pos{c}{2}=\black{133}
\pos{c}{3}=\white{196}
\pos{c}{4}=\black{3}
\pos{c}{5}=\black{189}
\pos{c}{6}=\white{176}
\pos{c}{7}=\black{17}
\pos{c}{8}=\black{173}
\pos{c}{9}=\black{171}
\pos{c}{10}=\white{168}
\pos{c}{11}=\black{195}
\pos{c}{12}=\black{57}
\pos{c}{13}=\black{141}
\pos{c}{14}=\white{10}
\pos{c}{15}=\white{52}
%
\pos{d}{1}=\white{134}
\pos{d}{2}=\black{137}
\pos{d}{3}=\black{13}
\pos{d}{4}=\white{188}
\pos{d}{5}=\black{15}
\pos{d}{6}=\black{177}
\pos{d}{7}=\white{180}
\pos{d}{8}=\black{21}
\pos{d}{9}=\white{170}
\pos{d}{10}=\black{169}
\pos{d}{11}=\black{9}
\pos{d}{13}=\white{154}
\pos{d}{14}=\black{143}
\pos{d}{15}=\black{51}
\pos{d}{16}=\white{2}
\pos{d}{17}=\white{54}
\pos{d}{18}=\white{24}
%
\pos{e}{1}=\white{140}
\pos{e}{2}=\white{136}
\pos{e}{4}=\white{12}
\pos{e}{7}=\white{14}
\pos{e}{8}=\white{192}
\pos{e}{9}=\white{20}
\pos{e}{13}=\white{160}
\pos{e}{16}=\black{53}
%
\pos{f}{2}=\white{62}
\pos{f}{3}=\white{64}
\pos{f}{4}=\white{58}
\pos{f}{6}=\white{16}
\pos{f}{7}=\white{70}
\pos{f}{8}=\white{166}
\pos{f}{10}=\white{158}
\pos{f}{11}=\white{50}
\pos{f}{13}=\white{144}
\pos{f}{17}=\black{23}
%
\pos{g}{1}=\white{84}
\pos{g}{2}=\black{63}
\pos{g}{3}=\white{60}
\pos{g}{4}=\black{19}
\pos{g}{5}=\black{59}
\pos{g}{6}=\black{65}
\pos{g}{7}=\white{66}
\pos{g}{8}=\black{69}
\pos{g}{9}=\white{148}
\pos{g}{10}=\black{157}
\pos{g}{11}=\white{76}
\pos{g}{14}=\black{81}
\pos{g}{15}=\black{87}
\pos{g}{18}=\black{139}
%
\pos{h}{2}=\white{82}
\pos{h}{3}=\black{61}
\pos{h}{7}=\black{67}
\pos{h}{8}=\white{68}
\pos{h}{9}=\white{22}
\pos{h}{10}=\black{147}
\pos{h}{11}=\black{75}
\pos{h}{12}=\black{77}
\pos{h}{14}=\black{79}
\pos{h}{15}=\white{80}
\pos{h}{17}=\black{123}
\pos{h}{18}=\white{138}
%
\pos{i}{1}=\white{94}
\pos{i}{2}=\black{83}
\pos{i}{4}=\black{5}
\pos{i}{6}=\white{18}
\pos{i}{7}=\black{71}
\pos{i}{15}=\white{74}
\pos{i}{17}=\black{55}
\pos{i}{18}=\white{120}
%
\pos{k}{2}=\black{95}
\pos{k}{7}=\black{73}
\pos{k}{8}=\white{86}
\pos{k}{9}=\white{72}
\pos{k}{10}=\black{165}
\pos{k}{11}=\black{11}
\pos{k}{17}=\black{119}
\pos{k}{18}=\white{118}
%
\pos{l}{4}=\black{49}
\pos{l}{6}=\black{89}
\pos{l}{7}=\white{88}
\pos{l}{8}=\black{127}
\pos{l}{9}=\white{128}
\pos{l}{10}=\black{115}
\pos{l}{13}=\white{116}
\pos{l}{15}=\white{78}
\pos{l}{16}=\black{121}
\pos{l}{17}=\white{56}
\pos{l}{18}=\black{117}
%
\pos{m}{6}=\black{91}
\pos{m}{7}=\white{90}
\pos{m}{8}=\black{129}
\pos{m}{9}=\white{114}
\pos{m}{10}=\black{125}
\pos{m}{12}=\black{145}
\pos{m}{16}=\white{124}
\pos{m}{17}=\white{122}
%
\pos{n}{7}=\black{113}
\pos{n}{9}=\black{159}
\pos{n}{10}=\black{167}
\pos{n}{11}=\white{164}
%
\pos{o}{2}=\black{131}
\pos{o}{4}=\black{7}
\pos{o}{9}=\black{153}
\pos{o}{12}=\white{156}
\pos{o}{16}=\white{28}
%
\pos{p}{1}=\white{130}
\pos{p}{2}=\black{111}
\pos{p}{6}=\black{85}
\pos{p}{8}=\white{152}
\pos{p}{9}=\black{151}
\pos{p}{11}=\white{162}
\pos{p}{12}=\black{155}
\pos{p}{13}=\white{104}
\pos{p}{14}=\white{108}
\pos{p}{15}=\white{26}
\pos{p}{16}=\black{25}
\pos{p}{17}=\white{106}
\pos{p}{18}=\white{34}
%
\pos{q}{2}=\white{110}
\pos{q}{3}=\black{45}
\pos{q}{4}=\black{1}
\pos{q}{7}=\black{103}
\pos{q}{8}=\white{150}
\pos{q}{9}=\black{149}
\pos{q}{10}=\white{8}
\pos{q}{11}=\black{161}
\pos{q}{12}=\white{100}
\pos{q}{13}=\black{101}
\pos{q}{14}=\black{107}
\pos{q}{15}=\white{30}
\pos{q}{16}=\black{29}
\pos{q}{17}=\white{36}
%
\pos{r}{1}=\white{112}
\pos{r}{2}=\black{47}
\pos{r}{3}=\white{42}
\pos{r}{4}=\black{43}
\pos{r}{6}=\white{6}
\pos{r}{7}=\black{99}
\pos{r}{8}=\white{146}
\pos{r}{9}=\black{97}
\pos{r}{11}=\white{40}
\pos{r}{12}=\black{39}
\pos{r}{15}=\black{31}
\pos{r}{16}=\white{4}
\pos{r}{17}=\black{27}
\pos{r}{18}=\white{96}
%
\pos{s}{2}=\white{46}
\pos{s}{3}=\white{48}
\pos{s}{4}=\white{44}
\pos{s}{7}=\white{102}
\pos{s}{8}=\white{126}
\pos{s}{9}=\black{185}
\pos{s}{11}=\white{98}
\pos{s}{12}=\black{41}
\pos{s}{13}=\black{109}
\pos{s}{15}=\black{35}
\pos{s}{16}=\white{32}
\pos{s}{17}=\black{33}
%
\pos{t}{16}=\black{37}

\begin{figure}[htb]
\gofontsize{15}
$$
\showfulldiagram
$$
\begin{center} Dia. 3 \end{center}
\end{figure}

\noindent
Diagrams are put in a text like ordinary vboxes.

The stones can be also put directly in a paragraph. To do this
you should use the \verb+\textwhite+ and \verb+\textblack+
commands instead of \verb+\white+ and \verb+\black+. For
example, the sentence from the beginning of this article:
``\textblack{1} is the first stone played, \textwhite{2} the
second, and so on'' was written as: ``\verb+\textblack{1}+ is the
first stone played, \verb+\textwhite{2}+ the second, and so on.''

There are no other secrets in coding go diagrams. Macros for
making $9\times 9$ or $13 \times 13$ diagrams can easily be
added to `go.sty' by a simple modification of the existing
macros for $19 \times 19$ diagrams.



\subsection*{References}

\begin{itemize}
\item[{[1]}] Zalman Rubinstein: ``Chess printing via Metafont
     and \TeX'', TUGboat Vol.~10, No.~2.
\item[{[2]}] Iwamoto Kaoru, 9 dan: ``Go for beginners'', Ishi
     Press, Inc., Tokyo, Japan.
\item[{[3]}] Janusz Kraszek: ``\'Swiat Go'', COK, Warsaw 1989
     (in Polish).
\end{itemize}

\end{document}
