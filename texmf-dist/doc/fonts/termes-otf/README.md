# README #
Package termes-otf supports the free TeX Gyre fonts Termes
as OpenType. TeX Gyre Termes has text and math fonts.

This package defines also the missing typefaces (bold math, slanted text)
and shows the meaning of the possible font features. The package
needs lualatex or xelatex. It does not work with pdflatex.



% This file is distributed under the terms of the LaTeX Project Public
% License from CTAN archives in directory  macros/latex/base/lppl.txt.
% Either version 1.3 or, at your option, any later version.
%
%

% Copyright 2022 Herbert Voss hvoss@tug.org

