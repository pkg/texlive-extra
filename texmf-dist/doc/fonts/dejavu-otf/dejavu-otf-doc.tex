%% $Id: lucida-otf-doc.tex 556 2017-09-18 06:22:29Z herbert $
% Copyright 2012-2015 TeX Users Group.
% 
% Copying and distribution of this file, with or without modification,
% are permitted in any medium, without royalty.

\listfiles
\documentclass[11pt]{article}
\usepackage{geometry}

\usepackage[usefilenames=false,
            TT={Scale=0.88,FakeStretch=0.9},
            SS={Scale=0.9},
            RM={Scale=0.9},
            DefaultFeatures={Ligatures=TeX}]{dejavu-otf}  % support opentype math fonts

\usepackage{biblatex}
\addbibresource{\jobname.bib}
\usepackage{array}
\usepackage{metalogo} % for \XeTeX logo
\usepackage{booktabs} % for examples
\usepackage{ltablex} % for examples
\usepackage{listings} % for code
\usepackage{dtk-logos} % for Wikipedia W

\pagestyle{headings}

\usepackage[colorlinks,hyperfootnotes=false]{hyperref}
% define \code for url-like breaking of typewriter fragments.
\ifx\nolinkurl\undefined \let\code\url \else \let\code\nolinkurl \fi

% Define \cs to prepend a backslash, and be unbreakable:
\DeclareRobustCommand\cs[1]{\mbox{\texttt{\char`\\#1}}}

% An environment like quote, but less space above and more below:
\newenvironment{demoquote}
   {\tabularx{\dimexpr\linewidth+\marginparwidth}{@{} X >{\ttfamily}l @{}}}
   {\endtabularx}


\title{Support for the DejaVu TrueType fonts and Math OpenType font}
\author{Herbert Voß}
\begin{document}
\maketitle
\tableofcontents

\section{Introduction}

The DejaVu fonts are modifications of the Bitstream Vera fonts designed for greater coverage of 
Unicode, as well as providing more styles. The Bitstream Vera family was limited mainly to the 
characters in the Basic Latin and Latin-1 Supplement portions of Unicode, roughly equivalent to 
ISO/IEC 8859-15, but was released with a license that permitted changes. The DejaVu fonts project 
was started with the aim to "provide a wider range of characters ... while maintaining the original 
look and feel through the process of collaborative development". The development of the fonts is 
done by many contributors, and is organized through a wiki and a mailing list.~\cite{dejavu}

A brief overview:

\begin{itemize}
\item Math fonts: TeXGyre DejaVu Math Regular, created by the Polish \TeX\ User group GUST.

\item Three text families (regular, italic, bold, bold italic)  
coming from the original Bitstream Vera.

\end{itemize}

The package \texttt{dejavu-otf} supports all families with specific optional
arguments:

\begin{tabular}{@{} >{\ttfamily}l l l @{}}\\\toprule
\emph{name} & \emph{value} &\emph{meaning}\\\midrule
mono & true/false & use only the DejaVu Sans Mono\\
serif & true/false & use only the DejaVu Serif\\
sans & true/false & use only the DejaVu Sans\\
math & true/false & use only the \TeX\ Gyre DejaVu Math \\
RM & code & options for DejaVu Serif\\
SS & code &  options for DejaVu Sans\\
TT & code &  options for DejaVu Sans Mono\\
MM & code &  options for TeXGyre DejaVu Math\\
%MMbold & TeXGyre DejaVu Boldmath\\
DefaultFeatures & code &  for all font styles\\\bottomrule
\end{tabular}

\bigskip
Except math all fonts are preset with \texttt{Scale=0.92}. For this documentation
we use instead:

\begin{verbatim}
\usepackage[TT={Scale=0.88,FakeStretch=0.9},
            SS={Scale=0.9},
            RM={Scale=0.9},
            DefaultFeatures={Ligatures=TeX}]{dejavu-otf}  % support opentype DejaVu fonts
\end{verbatim}


The standard font families \texttt{rm}, \texttt{sf}, and \texttt{tt} 
can be used as usual for any \LaTeX\  engine. 
By default the fonts are defined by their symbolic name. As an alternative you can load 
the package with the option \texttt{usefilenames}. 

\begin{verbatim}
\usepackage[usefilenames=true,
            TT={Scale=0.88,FakeStretch=0.9},
            SS={Scale=0.9},
            RM={Scale=0.9},
            DefaultFeatures={Ligatures=TeX}]{dejavu-otf}  % supports also opentype math fonts
\end{verbatim}


Then all definitions uses the
extension \texttt{.ttf} (roman, sans serif, mono)  and  \texttt{.otf} (math)  for the filenames of the fonts:

\begin{verbatim}
DejaVuSans-BoldOblique.ttf
DejaVuSans-Bold.ttf
DejaVuSansCondensed-BoldOblique.ttf
DejaVuSansCondensed-Bold.ttf
DejaVuSansCondensed-Oblique.ttf
DejaVuSansCondensed.ttf
DejaVuSans-ExtraLight.ttf
DejaVuSansMono-BoldOblique.ttf
DejaVuSansMono-Bold.ttf
DejaVuSansMono-Oblique.ttf
DejaVuSansMono.ttf
DejaVuSans-Oblique.ttf
DejaVuSans.ttf
DejaVuSerif-BoldItalic.ttf
DejaVuSerif-Bold.ttf
DejaVuSerifCondensed-BoldItalic.ttf
DejaVuSerifCondensed-Bold.ttf
DejaVuSerifCondensed-Italic.ttf
DejaVuSerifCondensed.ttf
DejaVuSerif-Italic.ttf
DejaVuSerif.ttf
texgyredejavu-math.otf
\end{verbatim}

The condensed and extra-light versions are definied by \cs{newfontfamily} and can be accessed by
the macros:


\begin{verbatim}
\DejaVuSerifCondensed
\DejaVuSansCondensed
\DejaVuSansLight
\end{verbatim}




\section{Text examples}

The DejaVu fonts have no Small Caps and no oldstyle figures!


\newcommand\demotext{%
  For \textsterling 45, almost anything can
  be found floating in fields. 
  !`THE DAZED BROWN FOX QUICKLY GAVE 12345--67890 JUMPS!
  --- ?`But aren't Kafka's Schlo\ss{} and
  \AE sop's \OE uvres often na\"\i ve vis-\`a-vis the d\ae monic
  ph\oe nix's official r\^ole in fluffy souffl\'es?}

\newcommand*\demotextsc{\textsc{\ Sphinx of black quartz, judge my vow}.}

The basic text family is \code{DejaVuSerif}, with the usual four
variants---\allowbreak regular, italic, bold, and bold italic. %, all with
%oldstyle figures; small caps are available in the upright shapes:

\noindent
\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\demotext\demotextsc & DejaVu Serif\\[4pt]
\itshape\demotext & DejaVu Serif Italic\\[4pt]
\bfseries\demotext & DejaVu Serif Bold\\[4pt]
\bfseries\itshape\demotext & DejaVu Serif BoldItalic\\[12pt]
%\end{tabularx}
~ & ~ \\
%\noindent
%\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\sffamily\demotext & DejaVu Sans\\[4pt]
\sffamily\itshape\demotext & DejaVu Sans Italic\\[4pt]
\sffamily\bfseries\demotext & DejaVu Sans Bold \\[4pt]
\sffamily\bfseries\itshape\demotext & DejaVu Sans BoldItalic\\[12pt]
%\end{tabularx}
~ & ~ \\

%\noindent
%\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\ttfamily\demotext &    DejaVu Sans Mono\\[4pt]
\ttfamily\itshape\demotext & DejaVu Sans Mono Italic\\[4pt]
\ttfamily\bfseries\demotext & DejaVu Sans Mono Bold\\[4pt]
\ttfamily\bfseries\itshape\demotext & DejaVu Sans Mono Bold Italic\\[12pt]
%\end{tabularx}
~ & ~ \\%

%\noindent
%\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\DejaVuSerifCondensed\demotext &    DejaVu Serif Condensed\\[4pt]
\DejaVuSerifCondensed\itshape\demotext & DejaVu Serif Condensed Italic\\[4pt]
\DejaVuSerifCondensed\bfseries\demotext & DejaVu Serif Condensed Bold\\[4pt]
\DejaVuSerifCondensed\bfseries\itshape\demotext & DejaVu Serif Condensed Bold Italic\\[12pt]
%\end{tabularx}
~ & ~ \\%

%\noindent
%\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\DejaVuSansCondensed\demotext &    DejaVu Sans Condensed\\[4pt]
\DejaVuSansCondensed\itshape\demotext & DejaVu Sans Condensed Italic\\[4pt]
\DejaVuSansCondensed\bfseries\demotext & DejaVu Sans Condensed Bold\\[4pt]
\DejaVuSansCondensed\bfseries\itshape\demotext & DejaVu Sans Condensed Bold Italic\\[12pt]
%\end{tabularx}
~ & ~ \\%

%\noindent
%\begin{tabularx}{\dimexpr\linewidth+\marginparwidth}{@{} >{\raggedright}X >{\ttfamily}l @{}}
\DejaVuSansLight\demotext &    DejaVu Sans Light\\[4pt]
%\end{tabularx}
~ & ~ \\%
\end{tabularx}



\section{Math examples}

There is only the regular version of the math font \cs{mathnormal}. With running \XeLaTeX\ 
it is possible to fake the fonts for a bold version:

\begin{verbatim}
\setmathfont{texgyredejavu-math.otf}[AutoFakeBold=1.6,\DejaVuMM@features,version=normal]
\end{verbatim}






\begin{verbatim}
Here's some text. And here's some math:
\[ 
  \phi(x)=\int_{-\infty}^{x} e^{-x^{2}/2} 
\]
And now bold math:
\boldmath % works only for xelatex
\[ 
  \phi(x)=\int_{-\infty}^{x} e^{-x^{2}/2} 
\]
\unboldmath
Euro and copyright symbols are available: \texteuro\ \textcopyright.
\end{verbatim}

Here's some text. And here's some math:
\[ 
  \phi(x)=\int_{-\infty}^{x} e^{-x^{2}/2} 
\]
And now bold math:

\boldmath

\[ 
 \phi(x)=\int_{-\infty}^{x} e^{-x^{2}/2} 
\]
\unboldmath
Euro and copyright symbols are available: \texteuro\ \textcopyright.



$f(x)=13\int$ \boldmath$f(x)=13 \int$\unboldmath
\section{Closing}


The font list of this documentation is:


\scriptsize\ttfamily
\expandafter\IfFileExists\expandafter{\jobname.fonts}%
  {\lstinputlisting{\jobname.fonts}}{}

\normalfont\rmfamily

\nocite{*}
\printbibliography


\end{document}

https://en.wikipedia.org/wiki/DejaVu_fonts