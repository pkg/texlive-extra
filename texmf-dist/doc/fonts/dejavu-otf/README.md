# README #
Package dejavu-otf supports the free ttf-fonts from
the DejaVu project which are available from GitHub
or already part of your system (Windows/Linux/...)
and the OpenType version of the TeXGyre Math which
is part of any TeX distribution.


Following font files are supported:

- DejaVuSans-BoldOblique.ttf
- DejaVuSans-Bold.ttf
- DejaVuSansCondensed-BoldOblique.ttf
- DejaVuSansCondensed-Bold.ttf
- DejaVuSansCondensed-Oblique.ttf
- DejaVuSansCondensed.ttf
- DejaVuSans-ExtraLight.ttf
- DejaVuSansMono-BoldOblique.ttf
- DejaVuSansMono-Bold.ttf
- DejaVuSansMono-Oblique.ttf
- DejaVuSansMono.ttf
- DejaVuSans-Oblique.ttf
- DejaVuSans.ttf
- DejaVuSerif-BoldItalic.ttf
- DejaVuSerif-Bold.ttf
- DejaVuSerifCondensed-BoldItalic.ttf
- DejaVuSerifCondensed-Bold.ttf
- DejaVuSerifCondensed-Italic.ttf
- DejaVuSerifCondensed.ttf
- DejaVuSerif-Italic.ttf
- DejaVuSerif.ttf
- texgyredejavu-math.otf
