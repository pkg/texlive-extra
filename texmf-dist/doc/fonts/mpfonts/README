mpfonts
Maintained by Daniel Benjamin Miller
January 30, 2020

***

The Computer Modern fonts are available in Type 1 format, but these renditions
are somewhat thin and spindly, and produce much lighter results than the
originals. It is alternatively possible to use METAFONT bitmaps, but this has
its disadvantages in comparison with vector fonts.

These fonts are conversions to Type 3 fonts, done entirely in METAPOST;
they are vector fonts which are a direct conversion from the original METAFONT
files, so they are the design most authentic to the originals.

However, these fonts, because they are PostScript Type 3 fonts, are not
suitable for on-screen reading, and should probably only be used for printing.

In order to produce a document using these fonts, first produce a DVI with
TeX or LaTeX, and then run dvips as follows:

dvips -D 10000 -u +mpfonts.map myfile.dvi

This will output a PostScript file using these fonts. It is recommended to keep
the resolution at 10000 DPI, since this allows for the most accurate glyph
positioning. As the fonts are vector fonts, this will not increase file size
in comparison with lower DPI settings.

***

The following font sets are supported:

* Computer Modern (cm)
* LaTeX fonts (latex-fonts)
* Frege symbols (fge)
* Extra logic symbols (cmll)
* Fraktur (cmfrak)
* AMS fonts (amsfonts)
* IPA (tipa)

Note that the T1 encoding is not directly supported. When using this package,
use the classic OT1 encoding (this is the default encoding, and will be used
unless you choose to change it).
