    The runic bundle provides fonts and packages for the Anglo-Saxon futharc 
script, used in England until just after printing was established. 
The bundle is one of a series for archaic scripts.

Changes in version 1.1 (2005/03/31)
o Font supplied as Postscript Type1 instead of MetaFont

Changes in version 1.0 (1999/03/14)
o First public release

------------------------------------------------------------------
  Author: Peter Wilson (Herries Press) herries dot press at earthlink dot net
  Copyright 1999--2005 Peter R. Wilson

  This work may be distributed and/or modified under the
  conditions of the Latex Project Public License, either
  version 1.3 of this license or (at your option) any
  later version.
  The latest version of the license is in
    http://www.latex-project.org/lppl.txt
  and version 1.3 or later is part of all distributions of
  LaTeX version 2003/06/01 or later.

  This work has the LPPL maintenance status "author-maintained".

  This work consists of the files:
    README (this file)
    runic.dtx
    runic.ins
    runic.pdf
  and the derived files
    runic.sty
    ot1fut10.fd
    t1fut10.fd
    fut10.map
    fut10.mf

------------------------------------------------------------------
     The distribution consists of the following files:
README (this file)
runic.dtx
runic.ins
runic.pdf     (user manual)
tryrunic.tex  (example usage)
tryrunic.pdf
fut10.afm
fut10.pfb
fut10.tfm

    To install the package:
o run: latex runic.ins, which will generate:
       runic.sty
       *.fd files
       fut10.map
o Move *.sty and *.fd files to a location where LaTeX will find them
    e.g., .../texmf-local/tex/latex/runic
o Move *.afm, *.pfb and *.tfm files to where LaTeX looks for font information
    e.g., .../texmf-var/fonts/afm/public/archaic/*.afm
          .../texmf-var/fonts/type1/public/archaic/*.pfb
          .../texmf-var/fonts/tfm/public/archaic/*.tfm
o Add the *.map information to the dvips/pdftex font maps
o Refresh the database
 (for more information on the above see the FAQ).

o run: (pdf)latex tryrunic for a test of the font

    To generate a second copy of the manual (which is already supplied as a PDF file):
o run: latex runic.dtx
o (for an index run: makeindex -s gind.ist *.idx)
o run: latex *.dtx
o Print *.dvi for a hardcopy of the package manual

2005/03/31
Peter Wilson
herries dot press at earthlink dot net

