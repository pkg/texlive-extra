    The cypriot bundle provides packages and fonts for a syllabic 
script which was used on Cyprus for writing Greek. The script was 
in use between approximately the tenth and third centuries BC.
The bundle is one of a series for archaic fonts.

Changes in version 1.2 (2009/05/22)
o Bug fixes in package code and documentation

Changes in version 1.1 (2005/06/13)
o Font supplied as Postscript Type1 as well as MetaFont

Changes in version 1.0 (1999/03/14)
o First public release

------------------------------------------------------------------
  Author: Peter Wilson (Herries Press) herries dot press at earthlink dot net
  Copyright 1999--2009 Peter R. Wilson

  This work may be distributed and/or modified under the
  conditions of the Latex Project Public License, either
  version 1.3 of this license or (at your option) any
  later version.
  The latest version of the license is in
    http://www.latex-project.org/lppl.txt
  and version 1.3 or later is part of all distributions of
  LaTeX version 2003/06/01 or later.

  This work has the LPPL maintenance status "author-maintained".

  This work consists of the files:
    README (this file)
    cypriot.dtx
    cypriot.ins
    cypriot.pdf
  and the derived files
    cypriot.sty
    ot1cypr10.fd
    t1cypr10.fd
    cypriot.map
    cypr10.mf

------------------------------------------------------------------
     The distribution consists of the following files:
README (this file)
cypriot.dtx
cypriot.ins
cypriot.pdf     (user manual)
trycypriot.tex  (example usage)
trycypriot.pdf
cypr10.afm
cypr10.pfb
cypr10.tfm

    To install the bundle:
o If you want MetaFont sources uncomment the appropriate lines in cypriot.ins.
o run: latex cypriot.ins, which will generate:
       cypriot.sty
       *.fd files
       cypriot.map
       and possibly cypr10.mf
o Move *.sty and *.fd files to a location where LaTeX will find them
    e.g., .../texmf-local/tex/latex/cypriot
o Move *.afm, *.pfb and *.tfm files to where LaTeX looks for font information
    e.g., .../texmf-var/fonts/afm/public/archaic/*.afm
          .../texmf-var/fonts/type1/public/archaic/*.pfb
          .../texmf-var/fonts/tfm/public/archaic/*.tfm
o Add the *.map information to the dvips/pdftex font maps
   If you want the MetaFont version as well:
   o Move the *.mf files to, e.g., .../texmf-var/fonts/source/public/cypriot
   o Add a line like the following to the (texmf/fontname/)special.map file:
     cypr10.mf    public      cypriot 
o Refresh the database
 (for more information on the above see the FAQ).

o run: (pdf)latex trycypriot for a test of the font

    If you want a full manual with all the MetaFont and LaTeX code and commentry, 
comment out the \OnlyDescription line in cypriot.dtx.
o run: (pdf)latex cypriot.dtx
o (for an index run: makeindex -s gind.ist *.idx)
o run: (pdf)latex *.dtx
o Print *.(pdf|dvi) for a hardcopy of the package manual

2009/05/22
Peter Wilson
herries dot press at earthlink dot net

