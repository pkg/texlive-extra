
# The `hep-font` package

Latin modern extended by computer modern.

## Introduction

The `hep-font` package loads standard font packages and extends the usual Latin Modern implementations by replacing missing fonts with Computer Modern counterparts.

The package is loaded with `\usepackage{hep-font}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

