\PassOptionsToPackage{svgnames}{xcolor}
\documentclass{article}
\usepackage{fontspec,symbats3,array,parskip,fancyhdr,setspace,relsize}
\usepackage[backend=biber,style=authoryear]{biblatex}
\usepackage{enumitem,xcolor,multicol,menukeys,sectsty,metalogo}
\usepackage[normalem]{ulem}
\usepackage{fancybox}
%\usepackage{draftwatermark}
%\SetWatermarkColor{LightSteelBlue}
\addbibresource{symbats3.bib}
\ULdepth=0pt
\makeatletter
\UL@height=.4pt
\renewcommand\uline{%
  \bgroup\markoverwith{\textcolor{Red}{\rule[\ULdepth]{2pt}{\UL@height}}}\ULon}
\allsectionsfont{\sffamily}
\makeatother
\setmainfont{Alegreya}
\setsansfont{Alegreya Sans}
\setmonofont[Scale=MatchLowercase]{InconsolataN}
\usepackage[a4paper,margin=1cm,top=2cm,bottom=2cm]{geometry}
\def\,{,}
\def\<{\texttt{<}}
\def\|{\textbar}
\def\>{\texttt{>}}
\setcounter{tocdepth}{1}
\newcommand{\vstrut}{\vrule height1.2em width0pt}
\pagestyle{fancy}
\rhead{\scriptsize\leftmark}
\lhead{\scriptsize\rightmark}
\renewcommand{\abstractname}{Summary}
\usepackage{hyperref}
\hypersetup{colorlinks=true,linkcolor=Crimson,urlcolor=MediumBlue,citecolor=Green}
\AtBeginDocument{\raggedright\thispagestyle{empty}}
\begin{document}
\title{Symbats 3.0 font macros for \LaTeX}
\author{Peter Flynn\\Silmaril Consultants\\\texttt{peter@silmaril.ie}}
\maketitle
\begin{center}
\begin{minipage}{.75\textwidth}
\begin{abstract}\onehalfspacing\raggedright\parindent1em\noindent
{S\smaller YMBATS} is a Neopagan dingbats typeface family that includes
various religious, astrological and other symbols, plus support for
Runic and Ogham script. The designer, Feòrag, has kindly released the
fonts under the SIL Open Font License Version 1.1 (26 February 2007).

This \LaTeX\ package (\textsf{symbats}) is released under the
\LaTeX\ Project Public License (LPPL), version 1.3c and is available
from the Comprehensive \TeX\ Archive Network (CTAN).

\subsubsection*{Current status}
\begin{enumerate}
  \item Names for use as commands in \LaTeX\ have been specified for
    all identified glyphs
  \item Names for glyphs positioned for older versions of the fonts
    have been prefixed `old' where this was identifiable;
  \item Names for Runic are prefixed `RU' and names for Ogham are
    prefixed `OG';
  \item Names which otherwise appear to be duplicates have been
    prefixed `dup'.
\end{enumerate}
\subsubsection*{Errors and omissions}
The maintainer of this package is Peter Flynn
\href{mailto:peter@silmaril.ie}{\nolinkurl{peter@silmaril.ie}}. Please
notify any errors or omissions, especially corrections to the naming,
to the maintainer.
\end{abstract}
\tableofcontents
\subsection*{History}
\begin{itemize}[nosep]
  \item[0.9] 2022-06-21 Edits and corrections to descriptions
  \item[0.8] 2022-01-18 Baseline adjustment option added,
    documentation updated
  \item[0.7] 2022-01-11 Last duplicated names fixed, .sty file created
    manually
  \item[0.6] 2022-01-08 Names edited and normalised, out for checking
  \item[0.5] 2022-01-06 Got feedback, edited names as suggested,
    created Makefile
  \item[0.4] 2022-01-04 Sent name list to Feòrag for checking
  \item[0.3] 2022-01-03 Created CSV and \LaTeX\ files
  \item[0.2] 2022-01-02 First pass at constructing names for the
    glyphs
  \item[0.1] 2021-12-20 Downloaded font, extracted list of glyph
    codepoints
\end{itemize}
\end{minipage}
\end{center}
\clearpage
\begin{multicols*}{2}
\large
\section{Symbats 3.0}
Symbats is a Neopagan dingbats font which has been developed by Feòrag
over a couple of decades, and recently re-released in an updated
version announced in a Twitter
post\footnote{\url{https://twitter.com/Feorag/status/1473048711601266690?s=20}}
on Winter Solstice Eve 2021.

The font comes in three weights, and has many additions and
redesigns. The files are distributed in OpenType format
(\texttt{.otf}) from the author's
website.\footnote{\url{https://www.feorag.com/freestuff/symbats.html}}\label{intro}

\subsection{\LaTeX\ version}

For the \LaTeX\ version, I have assigned control sequences
(\LaTeX\ commands) to all the glyphs. They are listed in this document
three times, in different orders for ease of reference:
\begin{enumerate}[nosep]
\item index order of Unicode codepoint;
\item alphabetical order of \LaTeX\ name;
\item alphabetical order of Unicode name.
\end{enumerate}

\subsection{Numeric character positions}\label{numpos}

For historical reasons, some of the glyphs are in the code positions
of ASCII printable characters (U+0021 to U+007E), where they were
originally placed for ease of typing in pre-Unicode days.

For compatibility with older versions, these glyphs are still in
place, but in cases where there are now real Unicode codepoints for the
exact characters by name, the glyph is duplicated at the new position
and the glyph at the original location has its name prefixed with
`\texttt{old}'. To the best of my understanding, the two glyphs are
identical in each case.

These duplications are noted in the following tables in the
\textbf{Dupes} column with a clickable link to the other position
(note that all these links go to the first listing, the index order of
codepoint, not to the listing in which you clicked).

For example, Earth (\oldearth) was originally at U+003C, but
Unicode now defines an actual codepoint named `Earth' at U+2641, so
the glyph is named in that location as \verb+\earth+ and the
original at U+003C is named \verb+\oldearth+; but they are identical.

\subsection{Naming}

In choosing the names, the methodology used was:

\begin{enumerate}

\item If the glyph has an established name (Sun, Earth, Pentagram)
  that name is used in all lowercase, eg \verb+\earth+;

\item Glyphs with multiple variants (moon, new moon, waxing moon,
  waning moon, etc) are named with the principal component first, eg
  \verb+\moonnew+, \verb+\moonfull+, \verb+\moonwaningoutline+,
  \verb+\moonwaningsolid+, etc.

  These can best be compared in the Unicode name order listing in
  \hyperref[unicodename]{\S\ref{unicodename}} starting on
  p.\pageref{unicodename}.

\item Variations in \textbf{pentagrams} include:
  \begin{itemize}[nosep]
  \item \textbf{rough} (sketched lines);
  \item \textbf{inverted} (abbreviated to \texttt{inv} to save typing);
  \item \textbf{interlaced} (lines pass in front and behind others);
  \item handedness of the interlacing (\textbf{Right} \textit{vs}
    \textbf{Left}) using capital \texttt{R} and \texttt{L};
  \item \textbf{circled} (ring around) \textit{vs} \textbf{solid}
    (reversed out).
  \end{itemize}
  For the first three (rough, inverted, and interlaced) there is no
  explicit opposite in the name, so the complement of
  \verb+\pentagramroughcircled+ is just \verb+\pentagramcircled+:
  there is no `smooth' in the name.

  Similarly, upright symbols are not
  named as such, only the inverted versions get an `\texttt{inv}' in
  their name.

  Where there is an obvious complement, this is linked in the
  \textbf{Compl} column in a similar manner to the \textbf{Dupes}
  method described in \hyperref[numpos]{\S\ref{numpos}}.

\item Variations in \textbf{sun} and \textbf{moon} include:
  \begin{itemize}[nosep]
  \item glyph in \textbf{outline} or \textbf{solid} (ie empty or filled);
  \item sun — number of \textbf{spokes} [is this the correct term? rays?];
  \item sun — \textbf{corona} (external ring);
  \item sun — \textbf{hub} (centre ring);
  \item moon — phase (new, first quarter, third quarter, full, waxing,
    waning).
  \end{itemize}
  As with the pentagrams, the absence of an effect is signalled by
  the absence of the term in the name.

\item Ogham letter names are prefixed with `\texttt{OG}';

\item Runic letter names are prefixed with `\texttt{RU}';

\item For any existing glyph for which a formal Unicode codepoint now
  exists, the name in the original location is prefixed by
  `\texttt{old}' as described in \hyperref[numpos]{\S\ref{numpos}};

\item A few names are not known, or not readily identifiable, or lost
  to the mists of time. These have been guessed at, and I would
  welcome suggestions.
  
\end{enumerate}

\section{Using the \textit{symbats3} package}

Download the fonts (see \S\ref{intro}) and install them into
  your computer according to your operating system's instructions
  (see instructions at the end of this page).

\subsection{Package installation}

\subsubsection{Normal (automated) installation}

If you are using (eg) MiK\TeX\ or another system with automated font
installation, just invoking the package in a document is enough to
make it install automatically from CTAN, eg:

\verb+\usepackage{symbats3}+

\subsubsection{Manual installation}

If you have to (or want to) install the package manually:

\begin{enumerate}
  
\item Download the package zip file from CTAN.

\item Unzip the file into your Personal \TeX\ Folder (see
  page\thinspace\pageref{ptf}) 

\item Create the subdirectories \verb+tex/latex/symbats3/+ and
  \verb+doc/latex/symbats3/+ within
    your Personal \TeX\ Folder \verb+texmf+.
    
\item Move the two package files \verb+symbats3.sty+ and
  \verb+symbats3.def+ into the new \LaTeX\ subdirectory
  \verb+texmf/tex/latex/symbats3/+.

\item Move the remaining package files into the new documentation
    subdirectory \verb+texmf/doc/latex/symbats3/+.

\item Add the line \verb+\usepackage{symbats3}+ to the Preamble of
  your \LaTeX\ document.

  \end{enumerate}

Note that the \textsf{supertramp} package \textsf{\smaller REQUIRES}
the \textsf{fontspec} package for Unicode and OpenType fonts
\parencite{fontspec}. The \textsf{supertramp} package \textsf{\smaller
  CANNOT} be used with the old \textsf{fontenc} and \textsf{inputenc}
packages, which means you \textsf{\smaller MUST} use \XeLaTeX\ or
  \LuaLaTeX\ as your processor, not \textit{pdflatex} or plain
  \textit{latex}.

\subsection{Options}

There is only one option to this version of the package:
\verb+[descenders]+, which lowers the height of the glyphs to align with
the descenders of the surrounding text.

By default, the glyphs print with their baseline aligned with the
baseline of the surrounding text, eg

\uline{\hbox{\sffamily\huge Baseline: \pentagram}}

In many cases the glyph fits better if it is aligned instead with the
bottom of the descenders (q, y, p, j, g) of the surrounding text, eg

\uline{\hbox{\sffamily\huge Descenders (qypjg): \pentagram[-.5ex]}}

The package option \verb+[descenders]+ will automatically calculate the
depth of the descenders for the current font and lower the glyph to
align with them, eg

\verb+\usepackage[descenders]{symbats3}+

In addition, there is an optional argument to every glyph command to
specify a different height or depth, eg

\verb+\pentagraminvsolid[-.666ex]+

will produce

\uline{\hbox{\sffamily\huge Manual: \pentagraminvsolid[-.666ex]}}

\bigskip\hrule\bigskip

\section*{Note on required features}
In this document, the keywords
    {\sffamily {\smaller MUST}}, {\sffamily {\smaller MUST NOT}}, {\sffamily {\smaller REQUIRED}},
    {\sffamily {\smaller SHALL}}, {\sffamily {\smaller SHALL NOT}}, {\sffamily {\smaller SHOULD}},
    {\sffamily {\smaller SHOULD NOT}},
    {\sffamily {\smaller RECOMMENDED}},
    {\sffamily {\smaller MAY}}, and
    {\sffamily {\smaller OPTIONAL}} have a specific
    meaning when shown in {\sffamily {\smaller THIS TYPESTYLE}}, and
    {\sffamily {\smaller MUST}} be interpreted as described in
    RFC 2119 \parencite{rfc2119}.
When shown in another typestyle, these words keep their conventional
    contextual degree of meaning.\par

\printbibliography


\section*{Installing OpenType™ and TrueType™ fonts}

  \begin{description}
    \item[Linux:] Run the \textsf{Font Manager} app, click
      \keys{\texttt{+}} and select the font files to install.
    \item[Mac OS X] Run the \textsf{Font Book} app, click the
      \textsf{Add} button in the Font Book toolbar, locate and select
      a font, then click \textsf{Open}. Drag the font file to the Font
      Book app icon in the Dock. Double-click the font file in the
      Finder, then click \textsf{Install Font} in the dialog that
      appears.
    \item[Windows:] Run \textsf{Directory Explorer} (what used to be
      called \textsf{My Computer}), right-click on
      each font file where you downloaded it, and select \textsf{Install}.
  \end{description}


  \begingroup
  \fboxsep1em
  \begin{Sbox}
    \begin{minipage}{.9\hsize}
      \raggedright\sffamily
      Your Personal \TeX\ Folder should normally be something like this;\\
      create it now if it does not already exist:
      \par\smallskip
      \begin{description}[noitemsep,leftmargin=1em,font=\sffamily]
      \item[Linux:] \verb+~/texmf+
      \item[Mac OS\thinspace X:]
        \verb+~/Library/texmf+
      \item[Windows:] (\textsf{Win95–XP})
        \verb+C:\texmf\tex\latex\symbats3+\\
        (\textsf{Win 7–11})
        \verb+Computer\System\Users\+\texttt{\itshape yourname}\verb+\texmf+
        \\[2mm]
        \textbf{Note} Windows MiK\TeX\ users (only) \textsf{\smaller
          MUST} add their Personal \TeX\ Folder to their MiK\TeX\ root
        when it is first created; and \textsf{\smaller MUST} refresh their
        Font Name DataBase (FNDB) utility after installation: see the
        link below. No such action is needed for other systems.
      \end{description}
      \par\smallskip
      See the online documentation at
      \url{http://latex.silmaril.ie/formattinginformation/personal.html}
      for more information about your
      Personal \TeX\ Folder and about how to use it in MiK\TeX\ \parencite{fi}.
    \end{minipage}
  \end{Sbox}
  \fbox{\TheSbox}
  \endgroup\label{ptf}

\end{multicols*}

  \clearpage
\section{Symbats3 in index order of Unicode codepoint}\label{codepoint}
\input{codepoint}
\clearpage
\section{Symbats3 in alphabetical order of \LaTeX\ name}\label{latexname}
\input{latexname}
\clearpage
\section{Symbats3 in alphabetical order of Unicode name}\label{unicodename}
\input{unicodename}
%\clearpage
%\input{test}
\end{document}
