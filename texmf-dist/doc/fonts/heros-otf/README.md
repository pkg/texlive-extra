# README #
Package heros-otf supports the free TeX Gyre font Heros (sans serif)
as OpenType and defines all missing typefaces. There is no math support
and for pdflatex use package newpx instead.

% This file is distributed under the terms of the LaTeX Project Public
% License from CTAN archives in directory  macros/latex/base/lppl.txt.
% Either version 1.3 or, at your option, any later version.
%
%

% Copyright 2022 Herbert Voss hvoss@tug.org

