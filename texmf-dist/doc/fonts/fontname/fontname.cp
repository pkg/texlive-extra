\entry{introduction}{1}{introduction}
\entry{bugs, reporting}{1}{bugs, reporting}
\entry{reporting bugs}{1}{reporting bugs}
\entry{tex-fonts@tug.org}{1}{\code {tex-fonts@tug.org}}
\entry{distribution}{1}{distribution}
\entry{encoding files}{1}{encoding files}
\entry{mapping files}{1}{mapping files}
\entry{utilities}{1}{utilities}
\entry{ftp.cs.umb.edu}{1}{\code {ftp.cs.umb.edu}}
\entry{ftp.tug.org}{1}{\code {ftp.tug.org}}
\entry{history}{1}{history}
\entry{Mittelbach, Frank}{1}{Mittelbach, Frank}
\entry{Schoepf, Rainer}{1}{Schoepf, Rainer}
\entry{Fontname version 2}{1}{Fontname version 2}
\entry{contributors}{1}{contributors}
\entry{Beebe, Nelson}{1}{Beebe, Nelson}
\entry{Beeton, Barbara}{1}{Beeton, Barbara}
\entry{Bernstein, Rocky}{1}{Bernstein, Rocky}
\entry{Bouche, Thierry}{1}{Bouche, Thierry}
\entry{Cugley, Damian}{1}{Cugley, Damian}
\entry{Horn, Berthol}{1}{Horn, Berthol}
\entry{Jeffrey, Alan}{1}{Jeffrey, Alan}
\entry{Lang, Russell}{1}{Lang, Russell}
\entry{MacKay, Pierre}{1}{MacKay, Pierre}
\entry{Rahtz, Sebastian}{1}{Rahtz, Sebastian}
\entry{Rivlin, Jean}{1}{Rivlin, Jean}
\entry{Rokicki, Tom}{1}{Rokicki, Tom}
\entry{references}{1}{references}
\entry{related files}{1}{related files}
\entry{TeX{} Directory Structure standard}{1}{\TeX {} Directory Structure standard}
\entry{TDS standard}{1}{TDS standard}
\entry{modes.mf}{2}{\code {modes.mf}}
\entry{PSfonts distribution}{2}{PSfonts distribution}
\entry{Fontinst distribution}{2}{Fontinst distribution}
\entry{Dvips translator}{2}{Dvips translator}
\entry{Dviljk translator}{2}{Dviljk translator}
\entry{terminology}{2}{terminology}
\entry{Southall, Richard}{2}{Southall, Richard}
\entry{limitations}{3}{limitations}
\entry{eight-character limitation}{3}{eight-character limitation}
\entry{DOS filesystems}{3}{DOS filesystems}
\entry{ISO 9660}{3}{ISO 9660}
\entry{CD-ROM distribution}{3}{CD-ROM distribution}
\entry{filenames, basics}{3}{filenames, basics}
\entry{design size specification}{3}{design size specification}
\entry{hexadecimal design size}{3}{hexadecimal design size}
\entry{font scaling}{3}{font scaling}
\entry{Mittelbach, Frank}{3}{Mittelbach, Frank}
\entry{suppliers}{3}{suppliers}
\entry{foundries}{3}{foundries}
\entry{type foundries}{3}{type foundries}
\entry{Avant Garde}{3}{Avant Garde}
\entry{Lubalin, Herb}{3}{Lubalin, Herb}
\entry{individual font designers}{3}{individual font designers}
\entry{small foundries}{3}{small foundries}
\entry{attribution of fonts, missing}{3}{attribution of fonts, missing}
\entry{unattributed fonts}{3}{unattributed fonts}
\entry{raw fonts}{3}{raw fonts}
\entry{bizarre font names}{3}{bizarre font names}
\entry{nonstandard names, escape for}{3}{nonstandard names, escape for}
\entry{escape for nonstandard names}{3}{escape for nonstandard names}
\entry{supplier.map}{4}{\code {supplier.map}}
\entry{typefaces}{4}{typefaces}
\entry{Garamond, versions of}{4}{Garamond, versions of}
\entry{pi fonts}{5}{pi fonts}
\entry{typeface.map}{5}{\code {typeface.map}}
\entry{FontName}{5}{\code {FontName}}
\entry{weight}{18}{weight}
\entry{weight.map}{18}{\code {weight.map}}
\entry{variants}{19}{variants}
\entry{multiple variants}{19}{multiple variants}
\entry{letterspacing}{19}{letterspacing}
\entry{small caps fonts}{19}{small caps fonts}
\entry{semisans variant}{19}{semisans variant}
\entry{semiserif variant}{19}{semiserif variant}
\entry{fax variant}{19}{fax variant}
\entry{bright variant}{19}{bright variant}
\entry{phonetic encodings}{19}{phonetic encodings}
\entry{Cyrillic encodings}{19}{Cyrillic encodings}
\entry{7-bit encodings}{19}{7-bit encodings}
\entry{8-bit encodings}{19}{8-bit encodings}
\entry{expertised encodings}{19}{expertised encodings}
\entry{SuperFont}{19}{\code {SuperFont}}
\entry{9s variant}{19}{\code {9s \r {variant}}}
\entry{ligatures and encodings}{19}{ligatures and encodings}
\entry{Lslash}{19}{\code {Lslash}}
\entry{lslash}{19}{\code {lslash}}
\entry{T1.etx}{19}{\code {T1.etx}}
\entry{encodings, unspecified}{19}{encodings, unspecified}
\entry{Afm2tfm encoding}{19}{Afm2tfm encoding}
\entry{LaserJet 4 fonts}{19}{LaserJet 4 fonts}
\entry{scripts}{20}{scripts}
\entry{Greek fonts}{20}{Greek fonts}
\entry{Cyrillic fonts}{20}{Cyrillic fonts}
\entry{math variants}{20}{math variants}
\entry{informal variant}{20}{informal variant}
\entry{schoolbook variant}{20}{schoolbook variant}
\entry{Stone Informal}{20}{Stone Informal}
\entry{typewriter variant}{20}{typewriter variant}
\entry{sans variant}{20}{sans variant}
\entry{Mittelbach, Frank}{20}{Mittelbach, Frank}
\entry{expert encoding}{20}{expert encoding}
\entry{expertised font}{20}{expertised font}
\entry{variant.map}{21}{\code {variant.map}}
\entry{widths}{23}{widths}
\entry{compression}{23}{compression}
\entry{expansion}{23}{expansion}
\entry{automatic expansion}{23}{automatic expansion}
\entry{character scaling, automatic}{23}{character scaling, automatic}
\entry{width.map}{23}{\code {width.map}}
\entry{long names}{24}{long names}
\entry{fontname mapping file}{24}{fontname mapping file}
\entry{mapping file}{24}{mapping file}
\entry{texfonts.map}{24}{\code {texfonts.map}}
\entry{Knuth, Donald E.}{24}{Knuth, Donald E.}
\entry{long naming scheme}{24}{long naming scheme}
\entry{X Window System font names}{24}{X Window System font names}
\entry{font_coding_scheme}{25}{\code {font_coding_scheme}}
\entry{category codes}{25}{category codes}
\entry{35 standard PostScript fonts}{26}{35 standard PostScript fonts}
\entry{standard PostScript fonts}{26}{standard PostScript fonts}
\entry{PostScript fonts, standard}{26}{PostScript fonts, standard}
\entry{adobe.map}{27}{\code {adobe.map}}
\entry{apple.map}{85}{\code {apple.map}}
\entry{bitstrea.map}{85}{\code {bitstrea.map}}
\entry{dtc.map}{103}{\code {dtc.map}}
\entry{itc.map}{103}{\code {itc.map}}
\entry{linotype.map}{120}{\code {linotype.map}}
\entry{monotype.map}{210}{\code {monotype.map}}
\entry{urw.map}{237}{\code {urw.map}}
\entry{PostScript encoding vectors}{238}{PostScript encoding vectors}
\entry{encoding vectors}{238}{encoding vectors}
\entry{8a.enc}{238}{\code {8a.enc}}
\entry{Adobe standard encoding}{238}{Adobe standard encoding}
\entry{encodings, Adobe standard}{238}{encodings, Adobe standard}
\entry{8r.enc}{244}{\code {8r.enc}}
\entry{TeX{} base encoding}{244}{\TeX {} base encoding}
\entry{base encoding, for TeX{}}{244}{base encoding, for \TeX {}}
\entry{encodings, TeX{} base}{244}{encodings, \TeX {} base}
\entry{Windows ANSI}{244}{Windows ANSI}
\entry{Lucida Bright}{244}{Lucida Bright}
\entry{cork.enc}{247}{\code {cork.enc}}
\entry{Cork encoding}{247}{Cork encoding}
\entry{EC encoding}{247}{EC encoding}
\entry{TeX{} Latin 1 encoding}{247}{\TeX {} Latin 1 encoding}
\entry{encodings, Cork}{247}{encodings, Cork}
\entry{encodings, EC}{247}{encodings, EC}
\entry{encodings, TeX{} Latin 1}{247}{encodings, \TeX {} Latin 1}
\entry{dvips.enc}{249}{\code {dvips.enc}}
\entry{Dvips encoding}{249}{Dvips encoding}
\entry{encoding, Dvips}{249}{encoding, Dvips}
\entry{texmext.enc}{250}{\code {texmext.enc}}
\entry{TeX{} math extension encoding}{250}{\TeX {} math extension encoding}
\entry{math extension, TeX{} encoding}{250}{math extension, \TeX {} encoding}
\entry{extension, TeX{} math encoding}{250}{extension, \TeX {} math encoding}
\entry{encodings, TeX{} math symbol}{250}{encodings, \TeX {} math symbol}
\entry{texmsym.enc}{254}{\code {texmsym.enc}}
\entry{TeX{} math symbol encoding}{254}{\TeX {} math symbol encoding}
\entry{math symbol, TeX{} encoding}{254}{math symbol, \TeX {} encoding}
\entry{symbol, TeX{} math encoding}{254}{symbol, \TeX {} math encoding}
\entry{encodings, TeX{} math symbol}{254}{encodings, \TeX {} math symbol}
\entry{texmital.enc}{258}{\code {texmital.enc}}
\entry{TeX{} math italic encoding}{258}{\TeX {} math italic encoding}
\entry{math italic, TeX{} encoding}{258}{math italic, \TeX {} encoding}
\entry{italic, TeX{} math encoding}{258}{italic, \TeX {} math encoding}
\entry{encodings, TeX{} math italic}{258}{encodings, \TeX {} math italic}
\entry{xl2.enc}{276}{\code {xl2.enc}}
\entry{ISO Latin 2 extended, TeX{} encoding}{276}{ISO Latin 2 extended, \TeX {} encoding}
\entry{xt2.enc}{283}{\code {xt2.enc}}
\entry{ISO Latin 2 extended and typewriter, TeX{} encoding}{283}{ISO Latin 2 extended and typewriter, \TeX {} encoding}
\entry{legalisms}{290}{legalisms}
\entry{typeface design protection}{290}{typeface design protection}
\entry{copyright on fonts}{290}{copyright on fonts}
\entry{font copyrighting}{290}{font copyrighting}
\entry{patenting of fonts}{290}{patenting of fonts}
\entry{Bigelow, Charles}{290}{Bigelow, Charles}
\entry{Holmes, Kris}{290}{Holmes, Kris}
\entry{United States, font design protection law}{290}{United States, font design protection law}
\entry{Lucida, protection of}{290}{Lucida, protection of}
\entry{Stone, protection of}{290}{Stone, protection of}
\entry{Germany, font design protection law}{290}{Germany, font design protection law}
\entry{Bauer, Paul}{290}{Bauer, Paul}
\entry{Futura}{290}{Futura}
\entry{England, font design protection law}{290}{England, font design protection law}
\entry{Times Roman, protection of}{290}{Times Roman, protection of}
\entry{Morison, Stanley}{290}{Morison, Stanley}
\entry{Lardent, Victor}{290}{Lardent, Victor}
\entry{Monotype}{290}{Monotype}
\entry{France, font design protection law}{290}{France, font design protection law}
\entry{romain du roi}{290}{romain du roi}
\entry{Grandjean, Philippe}{290}{Grandjean, Philippe}
\entry{Vienna treaty}{290}{Vienna treaty}
