These are the kpfonts package, originally provided by
		Christophe Caignaert		174 rue Charles Lebon
		59650 Villeneuve d'Ascq		France
		c.caignaert@free.fr
		
I designed the roman fonts from, at first, URW Palladio. 
URW++ is in agreement with the kpfonts project and, consequently, approves it.
For further information, read:		tug.ctan.org/pub/tex-archive/fonts/urw/base35/README.base35

%% This program can be redistributed and/or modified under the terms
%% of the LaTeX Project Public License Distributed from CTAN archives
%% in directory macros/latex/base/lppl.txt.

Because of the (very?) large set of options,			
read the doc  'kpfonts.pdf'

Almost all kpfonts options in one page: 'kpfonts-abstract.pdf'
The math table fonts : 'kpfonts-math-symbols-tables.pdf'
			

The version 3.34/3.35 are provided by 
hvoss@tug.org
