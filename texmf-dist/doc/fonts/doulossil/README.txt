This is a package providing IPA font Doulos SIL. The package is maintained by Niranjan Vikas Tambe on the following link.
https://gitlab.com/niranjanvikastambe/doulossil

It includes the following files -

1) Doulos_SIL_Regular.ttf
2) doulossil.tex
3) doulossil.pdf
4) README.txt

Doulos SIL is distributed under the SIL font license. The latest copy of this license can be read on the following link.
https://scripts.sil.org/cms/scripts/page.php?id=OFL
