adfsymbols
----------

adfsymbols consists of:
1. the ArrowsADF and BulletsADF fonts developed by Hirwen Harendel, Arkandis
Digital Foundry (ADF) and released under the terms set out in the files COPYING
and NOTICE in postscript type 1 format; 
2. (La)TeX support by Clea F. Rees released under the LPPL. All files covered
by the LPPL are listed in the file manifest.txt.

Information and resources concerning ArrowsADF and BulletsADF, including
opentype versions of the fonts, and other ADF fonts  can be found on the
foundry's homepage:
	http://arkandis.tuxfamily.org/

(La)TeX Support
---------------

For details, please see adfsymbols.pdf.

The (La)TeX support should be considered somewhat experimental. If you find any
problems, please let me know and I will try to correct them. If you can send a
fix, so much the better.

Contact Details
---------------

If you have comments about the fonts themselves, please contact Hirwen Harendal
(harendalh <at> hotmail <dot> com). 

Clea F. Rees (ReesC21 <at> cardiff <dot> ac <dot> uk)

Changes
-------

Version 1.2a fixes a bug **if** I've understood the problem correctly, which I'm
far from sure about. (All it does is add a pair of curly brackets in the .sty.)

Version 1.2b hopefully includes both the PDF *and* the TFM.

2019/10/13
