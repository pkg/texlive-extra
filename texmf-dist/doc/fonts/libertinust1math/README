LibertinusT1Math fonts
======================

The LibertinusT1Math font a Type1 font (accompanied by LaTeX support files) derived mainly from a fork of the Khaled Hosny's Libertinus Math font, an OpenType math companion for the Libertine font family. 

The LibertinusT1Math font is available under the terms of the Open Font License version
1.1, and the support files are released under the LPPL.

Current version 2.0.2 2022-01-25

Changes in version 2.0.2
1. Corrected name of the pfb to LibertinusT1Math.pfb,
from LibertinusT1math.pfb.
2. Corrected pdf version of documentation for sans fonts.

Changes in version 2.0.1
Corrected the sans math part which was missing many common glyphs.

Changes in version 2.0
1. Now based on recent version of LibertinusMath, with raised math axis and larger symbols.
2. Copied a number of macros and options from newtxmath:
  (a) made amsthm an option to libertinust1math so that loading order is correct.
  (b) added subscriptcorrection as an option.
  (c) \widebar makes an adaptive bar over a mathematical expression.
  (d) \vv make an adaptive vector over a mathematical expression.
3. Reworked the spacing and kerning of Roman and Greek letters.

Changes in version 1.2.5
Corrections to examples in documentation, mostly about loading amsmath before amsthm. (Thanks Maurice Hansen.)

Changes in version 1.2.4
Modified the code for \vdots to reference the correct fd file and now works as expected in text mode. (Thanks Maurice Hansen.)

Changes in version 1.2.3
1. Modified the sty file to increase script and scriptscript sizes, which were too small in some cases.
2. Modified the code in the sty file affecting the behavior of \bigg and \Bigg so they work as expected.

Changes in version 1.2.2
Changed the type of \smwhtcircle, and thereby \circ, from \mathord to \mathbin, matching that in fontmath.ltx. (Thanks Gijs Pennings.)

Changes in version 1.2.1
Fixed a conflict between amsmath and libertinust1math affecting extended arrows. (Thanks Davide Campagnari.)

Changes in version 1.2.0
Corrected mismatch between math extension glyphs related to \vert and \Vert relative to other large delimiters. (Thanks Davide Campagnari.)

Changes in version 1.1.9
The big operators (product, summation, etc.) in prior versions were not resizing correctly in displaystyle. Now fixed.

Changes in version 1.1.8
1. Corrected glyph "MATHEMATICAL ITALIC CAPITAL N" (U+1D441) which had inadverently become malformed at the last update.
2. Changed the fd files so they recognize scale setting from both the libertine and the new libertinus-type1 packages.

Changes in version 1.1.7
Version 1.1.6 was missing some items from its map file, now corrected.

Changes in version 1.1.6
Modified several math symbols. The multiply symbol in particular had overly generous side bearings and did not match the dimensions of plus, divide, etc, while plusminus and minusplus had their verticals trimmed to make for better symmetry about the math axis.

Changes in version 1.1.5
Corrected a typo in libusBMI.enc which caused bold italic T to not render. (Thanks Quirin Schroll.)

Changes in version 1.1.4
Corrected the encoding files for sans Greek so that \phi and \varphi work as expected in sans math.

Changes in version 1.1.3a
Includes some other most recent versions of files in 1.1.3.

Changes in version 1.1.3 (never placed on CTAN)
Includes correct version of sty file. An old version was included in 1.1.2.

Changes in version 1.1.2
Corrected the map file. (Wrong one was sent with version 1.1.1.)

Changes in version 1.1.1
Reworked side-bearings and top accent positions of a number of blackboard symbols.

Changes in version 1.1
Substantial changes to sans serif. As of version 1.1, the package supports fully ISO typeseting rules. It may be used (with option sansmath) as a math package with sans serif Roman and Greek letters.

Changes in version 1.0.6
Added macros for math sans serif: \mathsf, \mathsfit, \mathbfsf. (Thanks Jürgen Gilg.)

Changes in version 1.0.5
1. Adjusted shapes of some glyphs in math extension, leading to better rounding of heights and improved rendering of radicals. (If using latex+dvips+ps2pdf, you should increase the resolution in dvips with the command line option " -D 9000 ".)
2. Adjusted left side bearings of radicals so that the index, if present, is placed better.

Changes in version 1.0.4
1. Decreased widths of extension elements of horizontal parens and braces to get better horizontal constructions.
2. Fixed an encoding problem for double vertical arrow (uni21D0.ex).

Changes in version 1.0.3a
Fixed another error in map file affecting only use with dvips/gs.

Changes in version 1.0.3
1. Modified some glyphs in libertinust1math.pfb.
2. Added fd for mathbb, changed sty to define\mathbb.
3. Changed encoding files to get a real \setminus (uni29F5) rather than the small version (uni2216).

Changes in version 1.0.2
Corrected errors in encoding names in map file, affecting only processing with gs. (Thanks Uwe Siart.)

Changes in version 1.0.1
1. Corrected errors in libertinust1math.sty, so slanted \oint and \oiint work as expected.
2. Corrected errors in ls2libertinust1mathex.fd so that unusual NFSS entries are correctly redirected.

Michael Sharpe (msharpe at ucsd dot edu)