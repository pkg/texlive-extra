cmexb
================
2016, Petr Olsak

License: public domain

Files: this README
       cmexb10.tfm
       cmexb10.pfb
       cmexb.map

Description:
This package implements PFB font which is accessible normally by Sauter
extrapolation only as bitmap font. The cmexb10.tfm is exactly the output of
METAFONT using Sauter extrapolation. The cmexb10.pdf is generated using
mftrace. This small package is a part of CSTeX.

Notes: CSplain includes ams-math.tex macro where cmexb10 font is used for
bold version of math typesetting. If cmexb package is not installed then
usage of ams-math.tex and \boldmath yields to auto-generating metric file
and bitmap font (Type3) using METAFONT and Sauter extrapolation but all
another fonts are typically outlined. This is strange. If cmexb package is
installed then all math fonts used by ams-math.tex macro file are in Type1
format (outlined).

Installation:
Put cmexb10.tfm to tfm tree, cmexb10.pfb to type1 tree of your TeX
distribution. Then ensure that the map line

cmexb10 CMEXB10 <cmexb10.pfb

is added to all .map files used in your distribution (using updmap-sys or
something similar).

