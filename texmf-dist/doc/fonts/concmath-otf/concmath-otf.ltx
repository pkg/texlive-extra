\RequirePackage{pdfmanagement-testphase}
\DeclareDocumentMetadata{pdfstandard=A-2b, lang=en-GB}
\documentclass[a4paper,12pt]{scrartcl}

\usepackage{amsmath, array, varioref}
\usepackage[british]{babel}
\usepackage{concmath-otf}
\usepackage{subfig}
\captionsetup[subtable]{position=top}
\usepackage{realscripts}
\usepackage{microtype}
\usepackage{hyperref}
\hypersetup{pdftitle={Concrete-Math OpenType User’s Guide},
            pdfauthor={Daniel FLIPO},
            bookmarksopen,
            colorlinks
            }
\newcommand*{\hlabel}[1]{\phantomsection\label{#1}}

\newcommand*{\CCM}{\pkg{concmath-otf}}
\newcommand*{\pkg}[1]{\texttt{#1}}
\newcommand*{\file}[1]{\texttt{#1}}
\newcommand*{\opt}[1]{\texttt{#1}}
\newcommand*{\cmd}[1]{\texttt{\textbackslash #1}}\newcommand*{\showtchar}[1]{\cmd{#1}~\csname #1\endcsname}
\newcommand*{\showmchar}[1]{\cmd{#1}~$(\csname #1\endcsname)$}
\newcommand*{\showmchardollar}[1]{\texttt{\$\cmd{#1}\$}~$(\csname #1\endcsname)$}

\title{Concrete Math font, OTF version}
\author{Daniel Flipo \\ \texttt{daniel.flipo@free.fr}}

\newcommand*{\version}{0.24}

\begin{document}
\maketitle

\section{What is \CCM{}?}

The \CCM{} package offers an OpenType version of the Concrete Math font
created by Ulrik Vieth in MetaFont. \file{concmath-otf.sty} is a replacement
for the original \file{concmath.sty} package.

It requires LuaTeX or XeTeX as engine and the \pkg{unicode-math} package%
\footnote{Please read the documentation \file{unicode-math.pdf}.}.

Please note that the current version (\version) is \emph{experimental,
do expect metrics and glyphs to change} until version 1.0 is reached.
Comments, suggestions and bug reports are welcome!

\section{Usage}

\subsection{Calling \cmd{setmathfont}}

A basic call for \CCM{} would be:
\begin{verbatim}
\usepackage{unicode-math}
\setmathfont{Concrete-Math.otf} % Call by file name or
\setmathfont{Concrete Math}     % Call by file name
\end{verbatim}
this loads \CCM{} as maths font %
\footnote{Both calls work equally well with LuaTeX; with XeTeX a call by font
  name will fail unless the font is declared as a \emph{system font}.}
with the default options, see subsections~\vref{ssection-um},
\vref{ssection-cv} and~\vref{ssection-ss} for customisation.

Please note that the three sets of text fonts have to be chosen separately,
f.i. if you want the Concrete text fonts%
\footnote{They are part of the \pkg{cm-unicode} package.}
as Roman font:%\\[.25\baselineskip]

\pagebreak[4]\noindent
\verb+\setmainfont{cmunorm.otf}        +\\
\verb+  [BoldFont =       cmunobx.otf ,+\\
\verb+   ItalicFont =     cmunoti.otf ,+\\
\verb+   BoldItalicFont = cmunobi.otf ]+\\[.25\baselineskip]
otherwise you would get Latin Modern for text fonts (rm, sf and tt).

\subsection{Calling \pkg{concmat-otf.sty}}

A (recommended) alternative is:\\[.5\baselineskip]
\verb+\usepackage[ +\textit{options}
\footnote{Possible \textit{options} are \opt{loose}, \opt{no-text}, \opt{Scale=}
  or any of the options described in sections \ref{ssection-um},
  \ref{ssection-cv} and \ref{ssection-ss}.}%
\verb+ ]{concmath-otf}+\\[.5\baselineskip]
it loads \pkg{unicode-math} with the default options, sets Concrete-Math
as maths font and Concrete text fonts as Roman fonts
(families \textit{sf} and \textit{tt} left unchanged) but does a bit more:
\begin{enumerate}
\item it checks at \verb+\begin{document}+ if packages \pkg{amssymb} or
  \pkg{latexsym} are loaded and issues warnings in case they are;
\item it provides aliases for glyphs named differently in Unicode, so that
  \pkg{latexsym} or AMS names are also available;
\item it reduces spacing in maths mode: \cmd{thinmuskip}, \cmd{medmuskip}
  and \cmd{thickmuskip} are reduced as in \file{fourier.sty}.
%  \verb+\thinmuskip=2mu+,\\
%  \verb+\medmuskip=2.5mu plus 1mu minus 2.5mu+,\\
%  \verb+\thickmuskip=3.5mu plus 2.5mu+.\\
  The option \opt{loose} disables these settings.
\end{enumerate}

Apart from the \opt{loose} option mentioned above, \pkg{concmath-otf.sty}
provides an option \opt{no-text} to be used for loading the \CCM{} font
together with roman text fonts other than Concrete.

\section{What is provided?}

\CCM{} provides all glyphs available in the \pkg{concmath}, \pkg{amssymb} and
\pkg{latexsym} packages and more.
Therefore, these two packages \emph{should not} be loaded as they might
override \CCM{} glyphs.

Sans-serif, typewriter glyphs are not supplied.
A full list of available glyphs is shown in file \file{unimath-concrete.pdf}.

See in section~\vref{ssec-math-alphabets} how to choose
from other maths fonts for these styles.

\subsection{Upright or slanted?}
\label{ssection-um}

Package \pkg{unicode-math} follows \TeX{} conventions for Latin and Greek
letters: in math mode, the default option (\opt{math-style=TeX}) prints
Latin letters $a$…$z$ $A$…$Z$ and lowercase Greek letters $\alpha$…$\omega$
slanted (italic) while uppercase Greek letters $\Alpha \Beta \Gamma$…$\Omega$
are printed upright.
This can be changed by option \opt{math-style} as shown in
table~\vref{math-style}.

\begin{table}[ht]
  \centering
  \caption{Effects of the \opt{math-style} package option.}
  \hlabel{math-style}
  \begin{tabular}{@{}>{\ttfamily}lcc@{}}
    \hline
      \rmfamily Package option & Latin & Greek \\
    \hline
      math-style=ISO & $(a,z,B,X)$ & $\symit{(\alpha,\beta,\Gamma,\Xi)}$ \\
      math-style=TeX & $(a,z,B,X)$ & $(\symit\alpha,\symit\beta,\symup\Gamma,\symup\Xi)$ \\
      math-style=french & $(a,z,\symup B,\symup X)$ & $(\symup\alpha,\symup\beta,\symup\Gamma,\symup\Xi)$ \\
      math-style=upright & $(\symup a,\symup z,\symup B,\symup X)$ & $(\symup\alpha,\symup\beta,\symup\Gamma,\symup\Xi)$ \\
    \hline
  \end{tabular}
\end{table}

Bold letters are printed upright except lowercase Greek letters
which are slanted (the default option is \opt{bold-style=TeX}). This can be
changed by option \opt{bold-style} as shown in table~\vref{bold-style}.

\begin{table}[ht]
  \centering
  \caption{Effects of the \opt{bold-style} package option.}
  \hlabel{bold-style}
  \begin{tabular}{@{}>{\ttfamily}lcc@{}}
    \hline
      \rmfamily Package option & Latin & Greek \\
    \hline
      bold-style=ISO & $(\symbfit a, \symbfit z, \symbfit B, \symbfit X)$ & $(\symbfit\alpha, \symbfit\beta, \symbfit\Gamma, \symbfit\Xi)$ \\
      bold-style=TeX & $(\symbfup a,\symbfup z,\symbfup B,\symbfup X)$ & $(\symbfit\alpha, \symbfit\beta,\symbfup \Gamma,\symbfup \Xi)$ \\
      bold-style=upright & $(\symbfup a,\symbfup z,\symbfup B,\symbfup X)$ & $(\symbfup \alpha,\symbfup \beta,\symbfup \Gamma,\symbfup \Xi)$ \\
    \hline
  \end{tabular}
\end{table}

Other possible customisation: $\nabla$ is printed upright and $\partial$ is
printed slanted by default, but \opt{nabla=italic} and
\opt{partial=upright} can change this.

All these options are offered by the \pkg{unicode-math} package but they can
be added to the \cmd{setmathfont} call%
\footnote{IMHO it is easier to add \emph{all options} to the \cmd{setmathfont}
  command.}, for example:

\verb+\setmathfont{Concrete-Math.otf}[math-style=french,partial=upright]+\\
will print for the code
\begin{verbatim}
\[ \frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma
                                 + \symbf{\beta}\symbf{M} \]
\end{verbatim}
\setmathfont{Concrete-Math.otf}[math-style=french,partial=upright]
\[\frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma +
              \symbf{\beta}\symbf{M} \]
while the default settings would print
\setmathfont{Concrete-Math.otf}[math-style=TeX,partial=italic]
\[\frac{\partial f}{\partial x} = \alpha \symbf{V} + a\nabla\Gamma +
              \symbf{\beta}\symbf{M} \]

Both shapes remain available anytime: \verb+$\uppi,\itpi$+
prints $\uppi, \itpi$.

If your text editor is able to handle Greek letters or maths symbols, they can
be entered in the code instead control sequences (i.e.
$\symup{α}$, $\symup{β}$, $\symup{Γ}$,… for \cmd{alpha}, \cmd{beta},
\cmd{Gamma},…).

\subsection{Character variants}
\label{ssection-cv}

\CCM{} provides ten ``Character Variants’’ options, listed on
table~\vref{cv}, to choose between different glyphs for Greek characters
and some others.

\begin{table}[ht]
  \centering
  \caption{Character variants.}
  \hlabel{cv}
  \begin{tabular}{@{}>{\ttfamily}lccl@{}}
    \hline
           & Default       & Variant          & Name\\
    \hline
      cv01 & $\hslash$     & $\mithbar$       & \cmd{hslash} \\
      cv02 & $\emptyset$   & $\varemptyset$   & \cmd{emptyset} \\
      cv03 & $\epsilon$    & $\varepsilon$    & \cmd{epsilon} \\
      cv04 & $\kappa$      & $\varkappa$      & \cmd{kappa} \\
      cv05 & $\pi$         & $\varpi$         & \cmd{pi} \\
      cv06 & $\phi$        & $\varphi$        & \cmd{phi} \\
      cv07 & $\rho$        & $\varrho$        & \cmd{rho} \\
      cv08 & $\sigma$      & $\varsigma$      & \cmd{sigma} \\
      cv09 & $\theta$      & $\vartheta$      & \cmd{theta} \\
      cv10 & $\Theta$      & $\varTheta$      & \cmd{Theta}\\
    \hline
  \end{tabular}
\end{table}

For instance, to get \cmd{epsilon} and \cmd{phi} typeset as $\varepsilon$
and $\varphi$ instead of $\epsilon$ and $\phi$, you can add option
\verb+CharacterVariant={3,6}+ to the \cmd{setmathfont} call:
\begin{verbatim}
\setmathfont{Concrete-Math.otf}[CharacterVariant={3,6}]
\end{verbatim}

This  works for all shapes and weights of these characters: f.i.\ %
\verb+$\symbf{\epsilon}$+, \verb+$\symbf{\phi}$+ are output as
\setmathfont{Concrete-Math.otf}[CharacterVariant={3,6}]$\symbf{\epsilon}$,
$\symbf{\phi}$ instead of
\setmathfont{Concrete-Math.otf}$\symbf{\epsilon}$, $\symbf{\phi}$.

Similarly with \opt{math-style=french}, \verb+\epsilon+ and \verb+\phi+
are output as $\symup{\varepsilon}$ and $\symup{\varphi}$ (upright).

Please note that curly braces are mandatory whenever more than one
``Character Variant’’ is selected.

Note: \pkg{unicode-math} defines \cmd{hbar} as
\cmd{hslash} (U+210F) while \pkg{amsmath} provides two different glyphs
(italic h with horizontal or diagonal stroke).\\
\pkg{concmath-otf} follows \pkg{unicode-math}; the italic h with horizontal
stroke can be printed using \cmd{hslash} or \cmd{hbar} together with character
variant \opt{cv01} or with \cmd{mithbar} (replacement for AMS’ command
\cmd{hbar}).


\subsection{Stylistic sets}
\label{ssection-ss}

\CCM{} provides four ``Stylistic Sets’’ options to choose between different
glyphs for families of maths symbols.

\verb+StylisticSet=4+, alias%
\footnote{These \opt{Style} aliases are provided by \file{concmath-otf.sty}.}
\verb+Style=leqslant+, converts (large) inequalities into their slanted
variants as shown by table~\vref{ss04}.

\verb+StylisticSet=5+, alias \verb+Style=smaller+, converts some symbols into
their smaller variants as shown by table~\vref{ss05}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Sets 4 and 5}
  \subfloat[\texttt{Style=leqslant\quad (+ss04)}]{\hlabel{ss04}%
  \begin{tabular}[t]{@{}lcc@{}}
    \hline
      Command           & Default         & Variant \\
    \hline
      \cmd{leq}         & $\leq$         & $\leqslant$ \\
      \cmd{geq}         & $\geq$         & $\geqslant$ \\
      \cmd{nleq}        & $\nleq$        & $\nleqslant$ \\
      \cmd{ngeq}        & $\ngeq$        & $\ngeqslant$ \\
      \cmd{eqless}      & $\eqless$      & $\eqslantless$ \\
      \cmd{eqgtr}       & $\eqgtr$       & $\eqslantgtr$ \\
      \cmd{lesseqgtr}   & $\lesseqgtr$   & $\lesseqslantgtr$ \\
      \cmd{gtreqless}   & $\gtreqless$   & $\gtreqslantless$ \\
      \cmd{lesseqqgtr}  & $\lesseqqgtr$  & $\lesseqqslantgtr$ \\
      \cmd{gtreqqless}  & $\gtreqqless$  & $\gtreqqslantless$ \\
   \hline
  \end{tabular}
  }\hspace{10mm} % eof subfloat
  \subfloat[\texttt{Style=smaller\quad (+ss05)}]{\hlabel{ss05}%
  \begin{tabular}[t]{@{}lcc@{}}
    \hline
      Command                & Default             & Variant \\
    \hline
      \cmd{mid}              & $\mid$              & $\shortmid$ \\
      \cmd{nmid}             & $\nmid$             & $\nshortmid$ \\
      \cmd{parallel}         & $\parallel$         & $\shortparallel$ \\
      \cmd{nparallel}        & $\nparallel$        & $\nshortparallel$ \\
   \hline
  \end{tabular}
  }% eof subfloat
\end{table}

\verb+StylisticSet=6+, alias \verb+Style=subsetneq+, converts some inclusion
symbols as shown by table~\vref{ss06}.
\begin{table}[ht]
  \centering
  \caption{Stylistic Sets 6}\hlabel{ss06}
  \begin{tabular}[t]{@{}lcc@{}}
    \hline
      Command           & Default         & Variant \\
    \hline
      \cmd{subsetneq}   & $\subsetneq$    & $\varsubsetneq$ \\
      \cmd{supsetneq}   & $\supsetneq$    & $\varsupsetneq$ \\
      \cmd{subsetneqq}  & $\subsetneqq$   & $\varsubsetneqq$ \\
      \cmd{supsetneqq}  & $\supsetneqq$   & $\varsupsetneqq$ \\
   \hline
  \end{tabular}
\end{table}

To enable Stylistic Sets 4 and 6 for \CCM{}, you should enter
\begin{verbatim}
\setmathfont{Concrete-Math.otf}[StylisticSet={4,6}]  or
\usepackage[Style={leqslant,subsetneq}]{concmath-otf}
\end{verbatim}
then, \verb+\[x\leq y \quad A \subsetneq B\]+
will print as\\
\setmathfont{Concrete-Math.otf}[StylisticSet={4,6}]
$x\leq y \quad A \subsetneq B$\qquad
instead of\qquad
\setmathfont{Concrete-Math.otf}%
$x\leq y \quad A \subsetneq B$

\subsection{Standard \LaTeX{} math commands}
\label{ssec-math-commands}

All standard \LaTeX{} maths commands, all \pkg{amssymb} commands and all
\pkg{latexsym} commands are supported by \CCM{}, for some of them loading
\pkg{concmath-otf.sty} is required.

Various wide accents are also supported:
\begin{itemize}
\item \cmd{wideoverbar} and \cmd{mathunderbar}%
  \footnote{\cmd{overline} and \cmd{underline} are not font related,
     they are based on \cmd{rule}.}
  \[\wideoverbar{x}\quad \wideoverbar{xy}\quad \wideoverbar{xyz}\quad
    \wideoverbar{A\cup B}\quad \wideoverbar{A\cup (B\cap C)\cup D}\quad
    \mathunderbar{m+n+p}\]

\item \cmd{widehat} and \cmd{widetilde}
\[\widehat{x}\; \widehat{xx} \;\widehat{xxx} \;\widehat{xxxx}\;
  \widehat{xxxxx} \;\widehat{xxxxxx} \;\widetilde{x}\; \widetilde{xx}\;
  \widetilde{xxx} \;\widetilde{xxxx} \;\widetilde{xxxxx}\;
  \widetilde{xxxxxx}\]

\item \cmd{overparen} and \cmd{underparen}
  \[\overparen{x}\quad \overparen{xy}\quad \overparen{xyz}\quad
    \mathring{\overparen{A\cup B}}\quad
    \overparen{A\cup (B\cap C)\cup D}^{\smwhtcircle}\quad
    \overparen{x+y}^{2}\quad \overparen{a+b+...+z}^{26}\]

\[\underparen{x}\quad \underparen{xz} \quad \underparen{xyz}
  \quad \underparen{x+z}_{2}\quad \underparen{a+b+...+z}_{26}\]

\item \cmd{overbrace} and \cmd{underbrace}
  \[\overbrace{a}\quad \overbrace{ab}\quad \overbrace{abc}\quad
  \overbrace{abcd}\quad \overbrace{abcde}\quad
  \overbrace{a+b+c}^{3}\quad \overbrace{ a+b+. . . +z }^{26}\]

\[\underbrace{a}\quad\underbrace{ab}\quad\underbrace{abc}\quad
  \underbrace{abcd}\quad \underbrace{abcde}\quad
  \underbrace{a+b+c}_{3}  \quad \underbrace{ a+b+...+z }_{26}\]

\item \cmd{overrightarrow} and \cmd{overleftarrow}
  \[\overrightarrow{v}\quad \overrightarrow{M}\quad \overrightarrow{vv}
  \quad \overrightarrow{AB}\quad \overrightarrow{ABC}
  \quad \overrightarrow{ABCD} \quad \overrightarrow{ABCDEFGH}.
\]

\[\overleftarrow{v}\quad \overleftarrow{M}\quad \overleftarrow{vv}
  \quad \overleftarrow{AB}\quad \overleftarrow{ABC}
  \quad \overleftarrow{ABCD} \quad \overleftarrow{ABCDEFGH}\]

\item Finally \cmd{widearc} and \cmd{overrightarc} (loading
  \pkg{concmath-otf.sty} is required)
\[\widearc{AMB}\quad \overrightarc{AMB}\]
\end{itemize}

\subsection{Mathematical alphabets}
\label{ssec-math-alphabets}

\begin{itemize}
\item  All Latin and Greek characters are available in italic, upright, bold
  and bold italic via the \verb+\symit{}+, \verb+\symup{}+, \verb+\symbf{}+
  and \verb+\symbfit{}+ commands.

\item Calligraphic alphabet (\cmd{symscr} or \cmd{symcal} or
  \cmd{mathcal} command), uppercase:

  $\symscr{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$

\item Blackboard-bold alphabet (\cmd{symbb} or \cmd{mathbb} command),
  uppercase only except lowercase \verb+\Bbbk+ (AMS)

  $\symbb{ABCDEFGHIJKLMNOPQRSTUVWXYZ}\quad \Bbbk$

\item Fraktur alphabet, borrowed from Latin Modern

  $\symfrak{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$

  but this can overwritten, i.e.
\begin{verbatim}
\setmathfont{Asana-Math.otf}[range=frak,Scale=MatchUppercase]
$\symfrak{ABCDEFGHIJKL...XYZ abcdefghijkl...xyz}$
\end{verbatim}
\setmathfont{Asana-Math.otf}[range=frak,Scale=MatchUppercase]
$\symfrak{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$

\item Sans serif and Typewriter alphabets have to be imported, i.e.
\begin{verbatim}
\setmathfont{STIXTwoMath-Regular.otf}[range={sfup,sfit},
                            Scale=MatchUppercase]
$\symsfup{ABCD...klm}\quad\symsfit{NOPQ...xyz}$
\end{verbatim}
\setmathfont{STIXTwoMath-Regular.otf}[range={sfup,sfit},Scale=MatchUppercase]
$\symsfup{ABCDEFGHIJKLM abcdefghijklm}\quad
    \symsfit{NOPQRSTUVWXYZ nopqrstuvwxyz}$

\begin{verbatim}
\setmathfont{STIXTwoMath-Regular.otf}[range=tt,Scale=MatchUppercase]
$\symtt{ABCDE...XYZ abcde...xyz}$
\end{verbatim}
\setmathfont{STIXTwoMath-Regular.otf}[range=tt,Scale=MatchUppercase]
$\symtt{ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz}$
\end{itemize}

\subsection{Missing symbols}

\CCM{} does not aim at being as complete as \file{STIXTwoMath-Regular} or
\file{Cambria}, the current glyph coverage compares with TeXGyre maths fonts.
In case some symbols do not show up in the output file, you will see warnings
in the \file{.log} file, for instance:

\setmathfont{STIXTwoMath-Regular.otf}[range={"2964}]
\texttt{Missing character: There is no }$⥤$%
\texttt{ (U+2964) in font ErewhonMath}

Borrowing them from a more complete font, say \file{Asana-Math},
is a possible workaround:
\verb+\setmathfont{Asana-Math.otf}[range={"2964},Scale=1.02]+\\
scaling is possible, multiple character ranges are separated with commas:\\
\verb+\setmathfont{Asana-Math.otf}[range={"294A-"2951,"2964,"2ABB-"2ABE}]+

Let’s mention \pkg{albatross}, a useful tool to find out the list of fonts
providing a given glyph: f.i. type in a terminal ``\texttt{albatross U+2964}’’,
see the manpage or \file{albatross-manual.pdf}.

\section{Acknowledgements}

The original Metafont glyphs have been converted first to Type\,1 (pfa) using
\pkg{mftrace} and \pkg{fontforge}. The \pkg{cm-unicode} package has also
helped a lot while cleaning the glyphs.

I am grateful to George Williams and his co-workers for providing and
maintaining FontForge and to Ulrik Vieth for his illuminating paper published
in TUGboat~2009 Volume~30 about OpenType Math.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
