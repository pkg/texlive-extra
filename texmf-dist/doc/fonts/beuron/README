The beuron package provides the script used in the works of the Beuron art
school for use with TeX and LaTeX. It is a monumental script consisting of
capital letters only.
The fonts are provided as METAFONT sources, in the Type 1, and in the Opentype
format.
The package includes suitable font selection commands for use with LaTeX.

Package author: K. Wehr
Version: 1.3
Date: 17th January 2018

The beuron package is subject to the LaTeX Project Public License, version 1.3
or later. The Opentype fonts may also be used independantly of the package under
the SIL Open Font License, version 1.1.

Package files in the doc/ subtree:
beuron-de.pdf -- German manual
beuron-de.tex -- Source of the German manual
beuron-en.pdf -- English manual
beuron-en.tex -- Source of the English manual
Literatur.bib -- References

Package files in the fonts/ subtree:
beuronbuchst.mf -- Metafont source file containing the characters
beuronc.mf -- Metafont source file for the condensed font
BeuronCondensed-Regular.otf -- Opentype version of the condensed font
beuronc.pfb -- Type 1 file for the condensed font
beuronc.tfm -- TeX font metrics for the condensed font
BeuronExtended-Regular.otf -- Opentype version of the extended font
beuronkern.mf -- Metafont source file containing the kerning
beuron.map -- Map file for the use of the Type 1 version of the fonts
beuron.mf -- Metafont source file for the normal font
beuron.pfb -- Type 1 file for the normal font
Beuron-Regular.otf -- Opentype version of the normal font
beuron.tfm -- TeX font metrics for the normal font
beuronx.mf -- Metafont source file for the extended font
beuronx.pfb -- Type 1 file for the extended font
beuronx.tfm -- TeX font metrics for the extended font

Package files in the tex/ subtree:
beuron.sty -- Package code
t1beuron.fd -- Font definition file
