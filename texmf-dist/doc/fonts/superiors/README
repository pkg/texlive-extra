This small package provides a means of adding to or modifying the superior figures used for footnote and endnote markers in any font collection.

Current version: 1.06

This material is subject to the LaTeX Project Public License. See 
http://www.ctan.org/license/lppl1.3
for the details of that license.

Changes in 1.06
Added more abbreviations and added a check for KOMA, with which it is incompatibe.

Changes in 1.05
1. Added option to allow the tfm containing the superior figures to be specified by an abbreviation.
2. Modified code so that options supspaced and raised actually do what they are claimed to do.

Changes in 1.04
1. Corrected the code to eliminate an unwanted space following the footnote mark.

Changes in 1.03:
1. Changed code so that footnotes within minipages are not handled using superior figures, but use whatever was determined by \thempfootnote and the existing \@makefnmark.

Changes in 1.02:
1. libertinesups now points to a snapshot taken from LinLibertineO, and no longer depends on any other Libertine package.

The package provides one superior figures font drawn from the Linux Libertine collection---installation requires enabling superiors.map. The default superior figures font is distributed with newtx, and provides figures based on STIX superior figures, compatible with Times and other heavy fonts.

Any existing fonts collection, such as might be constructed by otfinst, may be used provided it has a complete set of superior digits.

Please send comments and bug reports or suggestions for improvement to

msharpe at ucsd dot edu