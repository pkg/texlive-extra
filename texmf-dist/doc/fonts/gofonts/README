This is the README for the gofonts package, version 2022-09-12.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the GoSans and GoMono families of fonts designed
by the Bigelow & Holmes foundry for the Go project. GoSans
is available in three weights: Regular, Medium and Bold
(with corresponding italics). GoMono is available in Regular
and Bold, with italics. Regular and italic small caps are provided.

Notes on the design may be found at

https://blog.golang.org/go-fonts

To install this package on a TDS-compliant TeX system
download the file "tex-archive"/install/fonts/gofonts.tds.zip
where the preferred URL for "tex-archive" is
http://mirrors.ctan.org. Unzip the archive at the root of an
appropriate texmf tree, likely a personal or local tree. If
necessary, update the file-name database (e.g., texhash).
Update the font-map files by enabling the Map file go.map.

To use, add

\usepackage{GoSans}

to the preamble of your document. These will activate GoSans
as the sans-serif text font. To activate it as the main text
font, use

\usepackage[sfdefault]{GoSans}

Use 

\usepackage{GoMono} 

to activate GoMono as the monospaced text font. The nomap
option will suppress the tex-text mapping of TeX "smart
quotes" and other ligatures into unicode glyphs for xeLaTeX
and luaLaTeX.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or
who wish to avoid fontspec may use the type1 option. 

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

Font encodings supported are OT1, T1, TS1, LY1 and LGR.

For GoSans, the medium option activates that series as the
default bold series. Commands \gomedium and \gobold allow for
localized use of those series.

Commands \golgr and \gomonolgr allow for localized use
of LGR-encoded type1 fonts.


The original TrueType fonts were obtained from

https://go.googlesource.com/image

and are licensed under the same open source license as the
rest of the Go project's software. The text may be found in
the doc directory. The type1 versions were created using
fontforge. The support files were created using autoinst and
are licensed under the terms of the LaTeX Project Public
License. The maintainer of this package is Bob Tennent at
rdt(at)cs(dot)queensu(dot)ca.
