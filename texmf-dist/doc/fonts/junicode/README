Junicode
Version 1.0.2
(c) 1998-2018 Peter S. Baker

This is a TrueType font for medievalists (and others) with many
OpenType features. See the documentation, Junicode.pdf, for
specimens and detailed instructions.

License: Open Font License (see COPYING).

***

Since the font is a native OpenType font with many features, it is best to
use with fontspec and LuaLaTeX or XeLaTeX. For details on the available
OpenType features, see doc/Junicode.pdf.

For use with Type 1-based engines, simply invoke \usepackage{junicode},
though some features will not be available.

***

The LaTeX support was added by Daniel Benjamin Miller. This support has been
dedicated to the public domain. Where this is not possible, you are hereby
given permission to use this contribution for any purpose whatsoever.
