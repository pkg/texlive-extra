# README #
Package firamath-otf supports the free math font for Fira Sans

% This file is distributed under the terms of the LaTeX Project Public
% License from CTAN archives in directory  macros/latex/base/lppl.txt.
% Either version 1.3 or, at your option, any later version.
%
%
% Copyright 2019-20...  Herbert Voss hvoss@tug.org
%

