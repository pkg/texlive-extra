This is the README for the universalis package, version 2022-09-25.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX support
for the UniversalisADFStd family of fonts, designed by Hirwin Harendal.
This font is suitable as an alternative to fonts such as Adrian
Frutiger's Univers and Frutiger.

To install this package on a TDS-compliant TeX system download
the file "tex-archive"/install/fonts/universalis.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org/. Unzip
the archive at the root of an appropriate texmf tree, likely a
personal or local tree. If necessary, update the file-name database
(e.g., texhash). Update the font-map files by enabling the Map file
universalis.map.

To use, add

\usepackage{universalis}

to the preamble of your document. This will activate Universalis as
the sans-serif text font. To set Universalis as the main text font,
use

\usepackage[sfdefault]{universalis}

The Universalis family includes condensed variants. Use the
condensed option to select them.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or who wish to
avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to scale the
fonts.

The only figure style is proportional-lining.

Font encodings supported are OT1, T1, LY1 and TS1.

Commands \univrs and \univrscondensed may be used to select the
standard and condensed variants, respectively, for localized use.

The original fonts (version 1.009) were obtained from

http://arkandis.tuxfamily.org/adffonts.html

and are licensed under the GPL License (version 2 or later) with
font exception; the text may be found in the doc directory. The
type1 versions were created using cfftot1. The support files were
created using autoinst and are licensed under the terms of the
LaTeX Project Public License. The maintainer of this package is Bob
Tennent (rdt at cs.queensu.ca)
