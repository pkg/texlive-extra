Note: This README file contains information about LaTeX package eczar in
      languages Marathi, Hindi & English. As this package is targeted more
      towards the speakers of language Marathi & Hindi the description is
      initially given in those languages, but for general readers an English
      description is provided at the end.
--------------------------------------------------------------------------
आज्ञासंच:    eczar
लेखक:      निरंजन
आवृत्ती:      ०.१ (फेब्रुवारी, २०२१)
माहिती:      रोझेटा संस्थेचा एग्झार हा युनिकोड आधारित मुक्त टंक आहे. ह्याच्या निर्मितीसाठी
	   गूगलकडून अर्थसाहाय्य लाभले आहे. ह्या टंकात ४५+३ भाषा लॅटिन व देवनागरी लिपीत
	   पुरस्कृत केल्या जातात. वैभव सिंह ह्यांनी ह्या टंकाचा अभिकल्प केला आहे व आज्ञावली तसेच
	   निर्मिती डेव्हिड ब्रेझीना ह्यांनी केली आहे.
संग्राहिका:    https://github.com/rosettatype/eczar
अडचणी:     https://github.com/rosettatype/eczar/issues
परवाना:     एसआयएल ओपन फॉन्ट परवाना. आवृत्ती १.१.
--------------------------------------------------------------------------
अन्य दुवे -
https://github.com/rosettatype/eczar/releases
--------------------------------------------------------------------------
टंककार -
वैभव सिंह
--------------------------------------------------------------------------
टंकाच्या दृश्य-उदाहरणांकरिता eczar.pdf पाहा.
--------------------------------------------------------------------------
आज्ञासमुच्चय:    eczar
लेखक:        निरंजन
संस्करण: 	     ०.१ (फ़रवरी, २०२१)
जानकारी:      एग्ज़ार यह रोज़ेटा द्वारा प्रकाशित युनिकोड आधारित मुक्त टंक है। इस टंक की
	    निर्मिति के लिए गूगल द्वारा अर्थसाहाय्य प्राप्त हुआ है। इस टंक द्वारा ४५+३ भाषाएँ
	    लैटिन तथा देवनागरी लिपि में पुरस्कृत की जाती है। वैभव सिंह ने इस का अभिकल्प किया
	    है और आज्ञावलि तथा निर्मिति डेविड ब्रेज़ीना द्वारा की गयी है। 
कड़ी:         https://github.com/rosettatype/eczar
त्रुटि:         https://github.com/rosettatype/eczar/issues
अनुज्ञप्ति:      एसआइएल ओपन फॉन्ट अनुज्ञप्ति। संस्करण १.१।
--------------------------------------------------------------------------
अन्य कड़ियाँ -
https://github.com/rosettatype/eczar/releases
--------------------------------------------------------------------------
टंककार -
वैभव सिंह
--------------------------------------------------------------------------
टंक के दृश्य उदाहरणों के लिए eczar.pdf देखिए.
--------------------------------------------------------------------------
Package:      eczar
Author:       Niranjan
Version:      0.1  (Feb, 2021)
Description:  Eczar is an open-source type family published by Rosetta with
	      generous financial support from Google. The fonts support over
	      45+3 languages in Latin and Devanagari scripts in 5 weights. It
	      was designed by Vaibhav Singh, code and production is by David
	      Březina.
Repository:   https://github.com/rosettatype/eczar
Bug tracker:  https://github.com/rosettatype/eczar/issues
License:      SIL Open Font License. v1.1.
--------------------------------------------------------------------------
Links -
https://github.com/rosettatype/eczar/releases
--------------------------------------------------------------------------
Font authors -
Vaibhav Singh
--------------------------------------------------------------------------
For font samples see eczar.pdf.
--------------------------------------------------------------------------
