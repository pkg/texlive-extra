% !TEX TS-program = pdflatexmk
\documentclass{article}

\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
\pdfmapfile{=newtxtt.map}
\usepackage{graphicx}
%SetFonts
\usepackage{XCharter}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[zerostyle=a,scale=1.1]{newtxtt} % TX typewriter
\usepackage[libertine,bigdelims]{newtxmath}
\usepackage{upquote}
\useosf
\font\fonta newtxtta at 11pt
\font\fontb newtxttb at 11pt
\font\fontc newtxttc at 11pt
\font\fontd newtxttd at 11pt
\font\fonte newtxtte at 11pt
\font\fontf newtxttsce at 11pt
%\font\fontg cmtt10 at 11pt
\renewcommand*{\sfdefault}{lmtt}% sans serif is otherwise not used
%SetFonts
\def\TeX{T\kern-.1667em\lower.5ex\hbox{E}\kern-.09emX\@}
\DeclareRobustCommand{\LaTeX}{L\kern-.28em%
\raise.4ex\hbox{\textsc{a}}%
        \kern-.1em%
        \TeX}
\title{The \texttt{newtxtt} Package\thanks{It is a pleasure to thank Jean-Fran\c{c}ois Burnol who has offered very useful advice and feedback on this package. For an extensive example of its usage as body text, see  the documentation of his remarkable {\tt xint} package.}
}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date

\begin{document}
\maketitle
There is a relative paucity of free serifed typewriter fonts available for use  in \LaTeX---{\tt courier}
 and (extensions of) {\tt cmtt} are the most common.  In my opinion, {\tt cmtt} and its enhancements, especially {\tt zlmtt}, are a much better choice than {\tt courier} in almost every circumstance, as the latter is so light and so wide that it looks poor on screen and causes endless problems with overfull boxes. (The ratio of their glyph widths is $723/525\approx1.38$.) This package provides an interface to another alternative---the typewriter fonts provided with {\tt txfonts}, with some enhancements. They have the same widths as {\tt cmtt}, but are taller, heavier, more geometric and less shapely, with  very low contrast, and are more suited to match Roman fonts of height and weight approximating that of Times. This package, loaded with
\begin{verbatim}
\usepackage{newtxtt} % options can be added
\end{verbatim}
provides access to its features, no matter what other text fonts you might be using. It should be placed after all your other text font loading packages that might contain instructions to change \verb|\ttdefault|, and before loading math packages so that the math packages can make a suitable definition of \verb|\mathtt|. With no options specified, as above, you'll get full functionality as a monospaced typewriter font family, with typewriter text rendered using essentially {\tt txtt}, but with a five choices for the glyph  `zero'. In addition, the package  provides italic (slanted) and bold versions, plus small caps in regular (medium) and bold weights, upright shape only. It is offered  only in T$1$ (plus full TS$1$) encoding. The macros \verb|\ttdefault|, \verb|\ttfamily|, \verb|\texttt| and the obsolete but convenient macro \verb|\tt| may be used to access this font. 
 The package provides, by means of options or macros, an alternate form of {\tt newtxtt} differing from it in some important ways:
 \begin{itemize}
 \item 
 the interword spacing is no longer the same as the glyph spacing, but is variable though generally smaller---{\tt fontdimen} settings have been changed to resemble those of text fonts;
 \item
 where monospaced typewriter fonts typically add an an extra space at the end of a sentence, the modified version does not;
 \item
 hyphenation is not suppressed.
 \end{itemize}
These features may be accessed by means of the new macros \verb|\ttzdefault|, \verb|\ttzfamily|, \verb|\textttz| and  \verb|\ttz| which are in all ways analogous to their monospace cousins. (Verbatim modes will continue to use the monospaced version.) The purpose of the {\tt ttz} version to allow use of \texttt{newtxtt} for blocks of {\tt TypeWriter}-like text, though not monospaced and respecting  right justification. Eg,
\begin{verbatim}
{\ttz Block of text, perhaps many lines long, will be rendered right-justified.}
\end{verbatim}

The options you may use in loading this package are:
\begin{itemize}
\item {\tt scaled=.97} will load the fonts scaled to $.97$ times natural size. This is useful with Roman fonts having an x-height smaller than Times, for which {\tt txtt} was designed.
\item
{\tt zerostyle} selects the form of {\tt `zero'} from one of five possibilities: {\tt a, b, c, d, e}, ({\tt a} being the default) which result respectively in\\[6pt] 
{\fonta 0} ---form {\tt a}, narrower than capital {\tt O};\\
{\fontb 0} ---form {\tt b}, original version from {\tt txtt};\\
{\fontc 0} ---form {\tt c}, slashed, narrower than capital {\tt O};\\
{\fontd 0} ---form {\tt d}, dotted, narrower than capital {\tt O};\\ 
{\fonte 0} ---form {\tt e}, narrower than capital {\tt O}, more oblong.\\
The option {\tt zerostyle} also affects the oldstyle figures that are available in \textsc{Small Caps}. That is, to obtain oldstyle typewriter figures, you have to use something like \verb|\texttt{\textsc{012}}|---the result using {\tt zerostyle=e} would be {\fontf 012}. 
\item
{\tt nomono} changes the {\tt tt} macro definitions replacing them, in effect,  by their {\tt ttz} versions. I do not necessarily recommend this, but I find it useful when text alignment is not important, and I do not wish to change all existing \verb|\tt| to \verb|\ttz|. It affects verbatim modes also.
\item
{\tt straightquotes} affects the rendering of single and double quotes in all {\tt newtxtt} modes:
Single left and right quotes entered in \TeX\ source code as \verb|`| and \verb|'| normally render as curly quotes, {\tt `} and {\tt '}. With {\tt straightquotes}, they will render as \verb|`| and \verb|'|, and double quotes will render as {\tt \char`"}.
\item The option {\tt ttdefault} sets \verb|\familydefault| to \verb|\ttdefault| so the default Roman text will be rendered using {\tt newtxtt}. 
\item The option {\tt ttzdefault} works similarly, but sets Roman text to use the non-monospaced  {\tt newtxttz}.
\end{itemize}
\textsc{New Macros:}
\begin{itemize}
\item
\verb|\ttz| switches to non-monospace typewriter mode; \\
eg, \verb|{\ttz text in ttz mode}| renders as \\
{\ttz text in ttz mode}.
\item Essentially the same effect with \verb|{\ttzfamily text in ttz mode}|.
\item \verb|\textttz{}| renders its argument in {\tt ttz} mode.
\end{itemize}

This document uses the following font settings:
\begin{verbatim}
\usepackage[osf]{XCharter} % osf in text, lining figures in math
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[zerostyle=a]{newtxtt} % TX typewriter
\usepackage[libertine,bigdelims]{newtxmath}
\end{verbatim}

If you use {\tt microtype} and have blocks of verbatim of typewriter text, you may find it best to prevent protrusion in that mode with the command (following \verb|\usepackage{microtype}|)\begin{verbatim}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
\end{verbatim}

Comparison with Latin Modern Typewriter: 

{\usefont{T1}{lmtt}{m}{n}\fontsize{11.22pt}{13}\selectfont LM Typewriter: This is just a line to illustrate typewriter 0123456789.}\\
\texttt{TX Typewriter: This is just a line to illustrate typewriter 0123456789.}\\
\textttz{TX Typewriter: This is just a line to illustrate typewriter 0123456789. (ttz)}

\textsc{Notes:} The first two are standard monospaced with the same spacing---the : is considered to be the end of a sentence---while the words in the third are  spaced more compactly, it would hyphenate if necessary, and lacks the extra space at the end of a ``sentence''.

\textbf{Using the fonts without using the package:} If you wish to use these fonts without making one of the them the \verb|\ttdefault|, you may call them directly from the {\tt fd}:
\begin{verbatim}
{\fontfamily{newtxtt}\selectfont ...} % or newtxttz
\end{verbatim}
or, for finer control of the \textsc{nfss} parameters,
\begin{verbatim}
{\usefont{T1}{newtxtt}{b}{n} ...} % or {newtxttz}{m}{sc}, etc
\end{verbatim}
For further control, you may add in your preamble lines like the following to select the zero style (five choices, a to e) and the shape of quotes:
\begin{verbatim}
\makeatletter
\edef\newtxtt@fig{c} % one of a--e to determine the zerostyle, defaults to a
\newif\iftxtt@upq\txtt@upqtrue % same effect as option straightquotes 
% remove the \txtt@upqtrue for not straightquotes, the default
\makeatother

\end{verbatim}

\end{document}  