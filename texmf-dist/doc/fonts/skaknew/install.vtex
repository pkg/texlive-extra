Installing the files
---------------------
Copy the files into the following directories; the paths are
appropriate for all TDS-compliant TeX systems auch as teTeX,
fpTeX, MikTeX or VTeX/Free:

*.pfb        ->  texmf/fonts/type1/public/skaknew
*.afm        ->  texmf/fonts/afm/public/skaknew
*.tfm        ->  texmf/fonts/tfm/public/skaknew
SkakNew.map  ->  texmf/dvips/config
SkakNew.ali  ->  texmf/fonts/map/vtex


|  The file SkakNew.ali is only required for VTeX;
|  SkakNew.map is only required with systems other than
|  VTeX.

Certain TeX systems require manually updating of a "filename
database" after adding of new files.  Please, consult the
documentation of your TeX system!



Configuring your TeX system
---------------------------
The required actions depend on the particular TeX system.
Particular instructions provided for

          * teTeX 2.0 and later,
          * VTeX/Free 8.x and later.

In case you have a different TeX system, consult its
documentation how to make it use the additional font map
file named "SkakNew.map", which resides in the directory
texmf/dvips/config/.


Configuring teTeX:
Use the shell script "updmap" to add the font map file named
"SkakNew.map" to the configuration.  See the documentation
of your particular teTeX version how to use the script.
With teTeX 2.0 and above, execute the following commands:

  texhash
  updmap --enable Map SkakNew.map


Configuring VTeX/Free:
Make VTeX read the additional font map ("aliasing") file
SkakNew.ali.  This is usually accomplished by putting a
record for this file into each of the configuration files

  texmf/vtex/config/pdf.fm
and
  texmf/vtex/config/ps.fm

The name "SkakNew.ali" is to be added to the TYPE1 section
of the above-mentioned files:

TYPE1 {
  ...
  SkakNew.ali
  }
