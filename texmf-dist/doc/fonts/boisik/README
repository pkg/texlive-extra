Boisik
============
Font Boisik is a serifed font (inspired by Baskerville typeface) written in
Metafont. It comprises roman and italic text fonts and math text fonts.

Font is licensed under GPL version 2. Font is in development and its application
is not recommended in instances, where stability of typesetting is important.
The font is provided "as is" without warranty of any kind. 

For more info see homepage http://mitek.webpark.cz/boisik.

Usage
============
LaTeX: \usepackage{boisik}
(add option `czech' for IL2 encoding and/or `arrows' for additional arrows)
TeX: use font directly, e.g. \font\rm bskr10

Installation
============
1) put *.mf files to directory used by Metafont,
   e.g. /usr/share/temxf/fonts/source/public/boisik,
2) put LaTeX files (boisik.sty, *.fd) to directory used by LaTeX,
   e.g. /usr/share/temxf/tex/latex/boisik,
3) rebuild database if needed (e.g. texhash).

Contact
============
Homepage: http://mitek.webpark.cz/boisik
Email: mitek at email dot cz (add text `[BSK]' to subject)

Basic fonts
============
Roman:  bskr10, bskrb10
Italic: bski10, bskib10 
Math:   bskmi10, bsksy10, bskex10, bskms10, bskma10

Warning
============
When you encounter "strange path" or "paths don't intersect" errors, try
higher resolution (e.g. dvips -mode supre -D 2400 test.dvi). 

Examples
============
http://mitek.webpark.cz/boisik/boisik.pdf
http://mitek.webpark.cz/boisik/boisik-idiot.pdf
