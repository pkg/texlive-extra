Domitian font family, version 1.0.1
May 26, 2020

The Domitian fonts are a free and open-source OpenType font family, based on
the Palatino design by Hermann Zapf (1918-2015), as implemented in Palladio,
the version distributed as part of URW's free Core 35 PostScript fonts (2.0).

Domitian is meant as a drop-in replacement for Adobe's version of Palatino.
It extends Palladio with small capitals, old-style figures and scientific
inferiors. The metrics have been adjusted to more closely match Adobe Palatino,
and hinting has been improved.

To use, simply add the following line to your preamble:

\usepackage{domitian}

For along with mathpazo for mathematics, add instead:

\usepackage{mathpazo,domitian}

See the documentation for more details.

Domitian is licensed under your choice of the GNU Affero GPL 3 (with a font
exception), the LaTeX Project Public License 1.3c or the SIL Open Font License
1.1. For more details, see COPYING.

For any questions, email Daniel Benjamin Miller <dbmiller@dbmiller.org>.
