This is the README for the josefin package, version
2022-10-01.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the Josefin Sans family of fonts, designed
by Santiago Orozco of the Typemade foundry in Monterey,
Mexico. Josefin Sans is available in seven weights, with
corresponding italics.

The idea of this typeface is to be geometric, elegant,
with a vintage feeling, for use at larger sizes. It is
inspired by geometric sans serif designs from the 1920s. The
x-height is half way from baseline to cap height, an unusual
proportion. It has been suggested as a free alternative to
the Neutraface family.

To install this package on a TDS-compliant TeX system
download the file "tex-archive"/install/fonts/josefin.tds.zip
where the preferred URL for "tex-archive" is
http://mirrors.ctan.org. Unzip the archive at the root of an
appropriate texmf tree, likely a personal or local tree. If
necessary, update the file-name database (e.g., texhash).
Update the font-map files by enabling the Map file josefin.map.

To use, add

\usepackage{josefin}

to the preamble of your document. These will activate
Josefin Sans as the sans-serif text font. To activate it as
the main text font, use

\usepackage[sfdefault]{josefin}

Use 

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or
who wish to avoid fontspec may use the type1 option. 

Options scaled=<number> or scale=<number> may be used to
scale the fonts.

The figure style for Josefin Sans is proportional-lining.

Font encodings supported are OT1, T1, and LY1.

The medium and semibold options activate those series as the
default bold series. The light, extralight, and thin options
activate those series as the default regular series.

Commands \josefinthin, \josefinlight etc. allow for
localized use of those series.

The TrueType fonts were obtained from

https://fonts.google.com/specimen/Josefin+Sans

and are licensed under the SIL Open Font License, version
1.1; the text may be found in the doc directory. The
type1 versions were created using fontforge.
The support files were created using autoinst and are
licensed under the terms of the LaTeX Project Public
License. The maintainer of this package is Bob Tennent at
rdt(at)cs(dot)queensu(dot)ca.
