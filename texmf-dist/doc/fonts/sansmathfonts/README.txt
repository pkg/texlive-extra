The sansmathfonts package provides math fonts designed to work with the Computer Modern sans serif typeface, as well as some missing Computer Modern sans serif text fonts.

This version was released 2022/10/09.

This package is released under the LaTeX Project Public License. See
  https://www.latex-project.org//lppl/
or
  https://ctan.org/license/lppl1.3c
for the details of that license.

This package has the LPPL maintenance status ``maintained''. The package author is Ariel Barton; she may be contacted at origamist@gmail.com.

For more information, see the documentation file sansmathfonts.pdf.

