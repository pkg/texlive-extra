# Rosario fonts for LaTeX #

This work provides the necessary files to use the Rosario fonts with
LaTeX.  Rosario is a set of eight fonts provided by Héctor Gatti,
Adobe Typekit &
[Omnibus-Type](https://www.omnibus-type.com/fonts/rosario/) Team
under the Open Font License [(OFL)](http://scripts.sil.org/OFL),
version 1.1.  The fonts are copyright (c) 2012-2019, Omnibus-Type.

The LaTeX package is released under the LaTeX Project Public License
[(LPPL)](http://www.latex-project.org/lppl.txt) v1.3c or later,
copyright (c) 2016-2019 Arash Esbati.

    v2.1, 2019/07/19
    * Fix a bug in handling of alias keys

    v2.0, 2019/07/07
    * Remove `Rosario.fontspec' from the bundle since the
      functionality is now provided by the package itself
    * Add the `scaled' key
    * Use NFSS scheme for fonts mapping
    * Rewrite major part of the code supporting 8 fonts.  Harmonize
      the usage of package options over different TeX engines

    v1.0, 2016/05/01
    * Initial release
