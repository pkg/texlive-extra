The SticksToo Font Package
--------------------------

SticksToo is a reworking of some of the fonts in the newly released STIX2 font package and the STIX2 beta font package from 2016 to allow LaTeX users to make better use of some of the text features of STIX2. A companion release of newtx, version 1.55, contains an option stix2 that uses Roman and Greek letters together with newtxmath symbols, as a work-around for some current problems with STIX2 math rendering in LaTeX.

The fonts are provided in PostScript format. As the STIX2 fonts are released under the SIL's OFL, so are the fonts in SticksToo. The details of this license are spelled out in the file OFL.txt accompanying this package.

The TeX font metrics, font definition files and other support files for SticksToo,
i.e., the other files in the archive SticksToo.zip, may be distributed and/or modified
under the conditions of the LaTeX Project Public License, either version 1.3 of
this license or (at your option) any later version.  The latest version of this
license is in http://www.latex-project.org/lppl.txt and version 1.3 or later is
part of all distributions of LaTeX version 2003/12/01 or later. 

The font files and the LaTeX support files are

  Copyright (c) 2018 Michael Sharpe

Files provided "as is", with no warranties.


Current version: 1.035 2021-10-17

Changes in version 1.035
1. Added a more capable \textfrac macro.
2. Changed name \textin to texinf (for inferior figures) in order to avoid
a conflict with hyperref.
3. Minor documentation corrections and additions.

Changes in version 1.034
Added some Greek Blackboard Bold glyphs, bringing sticktoo math into compliance with newtxmath 1.640.

Changes in version 1.033
Version 1.032 lacked some of the latest versions of fonts and tfm files. Corrected in 1.033.

Changes in version 1.032
Changed the math italic and math upright tfm and vf that allow these fonts to produce pdf files that conform to PDF/A-1b when used in conjunction with newtxmath. See the newtx documentation for details.

Changes in version 1.031
Corrected the ot1G fonts, the originals having found to be the cause of some subtle issues with the microcode package. I owe Ulrike Fischer my sincere gratitude for tracking down the true source of the problem.

Changes in version 1.03
Corrected problem with Math-BoldItalic where internal font name was incorrect. (Thanks, Uwe Siart.)

Changes in version 1.02
Fixed problems testing for KOMA classes.

Changes in version 1.01
1. Corrected documentation errors. (Thanks to J-F Burnol.)
2. Fixed typo in sty file, line 97

Changes in version 1.00a
Removed untxmia.fd as duplicate of file in newtx


Please send comments and bug reports or suggestions for improvement to

msharpe at ucsd dot edu
