% !TEX TS-program = pdflatexmk
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
%\pdfmapfile{+nimbus15.map}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage[OT2,LGR,T2A,T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{substitutefont}
\usepackage[greek,russian,english]{babel}
\usepackage[largesc,p]{newtxtext} % 
\usepackage[varqu,varl]{zi4}
%\usepackage{nimbusmononarrow}% Courier Narrow
\usepackage{nimbussans}% Helvetica as sans serif
\usepackage[vvarbb]{newtxmath}
\useosf % must come immediately after newtxmath
\substitutefont{LGR}{\rmdefault}{NimbusSerif}
%\substitutefont{LGR}{\sfdefault}{NimbusSans}
\substitutefont{T2A}{\rmdefault}{NimbusSerif}
\substitutefont{OT2}{\rmdefault}{NimbusSerif}
\usepackage{fonttable}
\newcommand\cyrtext{\fontencoding{OT2}\selectfont} % declaration
\DeclareTextFontCommand{\textcyr}{\cyrtext}  %macro with argument
%
\title{The Nimbus15 package}
\author{Michael Sharpe}
\date{\today} 
\begin{document}
\maketitle
{\tt Nimbus15} is derived from the Nimbus fonts, metric clones of Courier, Helvetica and Times, issued in 2015 by URW++ by way of Artifex, makers of Ghostscript. (The latest versions for 2015 appeared with an update to the {\tt gs} distribution in October, 2015.) The novelty here is that there are now Greek and Cyrillic glyphs in all the Nimbus fonts. To summarize the changes from those supplied by Artifex and those in this distribution, aside from the trivial addition of {\tt cyrbreve (uniF6D4)}, {\tt low asterisk (uni204E)} ({\tt zco} only), {\tt visiblespace (uni2423)} and {\tt dotlessj (uni0237)} so in each typewritten font  {\tt *} is correctly rendered and the {\tt ot1} and {\tt ot2} encodings are complete in all cases:
\begin{itemize}
\item{\tt Courier clone:}
\begin{verbatim}
NimbusMono-Regular->zco-Light
NimbusMono-Bold->zco-Bold
NimbusMono-Oblique->zco-LightOblique
NimbusMono-BoldOblique->zco-BoldOblique
\end{verbatim}
A new weight, intermediate between {\tt Light} and {\tt Bold}, was created with names\\
\verb|zco-Regular|, \verb|zco-Oblique|\\
The glyphs in Light, Regular and Bold have stem widths {\tt 41em}, {\tt64em} and {\tt 100em} respectively. A few glyphs required modification prior to and following the thickening process. The Greek glyphs support only monotonic Greek typography. Several Greek glyphs were modified from the originals, most importantly {\tt alpha} (less fish-like), {\tt nu} (curved, not v-shaped) and {\tt Phi} (less tall.) Thanks are due to Dimitrios Filippou for his important feedback on  Greek typographic issues. Additionally, \verb|zco-Regular| was modified to a narrow version, \verb|zcoN-Regular|, starting with some FontForge transformations and finishing with manual adjustments to shorten serifs where necessary and make circular outlines narrower.
\item{\tt Helvetica clone:}
\begin{verbatim}
NimbusSanL*->zhv-*
\end{verbatim}
The upright tonos accent in the originals was modified to a slanted form, along with the prebuilt letters with tonos and tonosdieresis accents. Only a few modifications were made to the spacing and kerning tables. The Greek glyphs support only monotonic Greek typography.

\item{\tt Times clone:}
\begin{verbatim}
NimbusRomNo9L*->ztm-*
\end{verbatim}
The original fonts' Greek glyph coverage was relatively modest, supporting only monotonic Greek. This distribution  adds glyphs to cover polytonic and some ancient forms. Three Cyrillic glyphs were changed substantially, and the spacing and kerning tables were modified considerably. (The Cyrillic part covers all but eight glyphs in T$2$A encoding, but has serious gaps in T$2$B and T$2$C.) In addition, slanted versions were created for the benefit of those switching from other font families like Latin Modern, where both \emph{italic} and {\usefont{T1}{NimbusSerif}{m}{sl}slanted} are available and may have different semantic connotations.

The glyphs missing from T$2$A are:
\begin{verbatim}
134 ("86) uni0498 CYRILLIC CAPITAL LETTER ZE WITH DESCENDER
138 ("8A) uni04A0 CYRILLIC CAPITAL LETTER BASHKIR KA
140 ("8C) uni04D4 CYRILLIC CAPITAL LIGATURE A IE
142 ("8E) uni04A4 CYRILLIC CAPITAL LIGATURE EN GHE
166 ("A6) uni0499 CYRILLIC SMALL LETTER ZE WITH DESCENDER
170 ("AA) uni04A1 CYRILLIC SMALL LETTER BASHKIR KA
172 ("AC) uni04D5 CYRILLIC SMALL LIGATURE A IE
174 ("AE) uni04A5 CYRILLIC SMALL LIGATURE EN GHE
\end{verbatim}
\end{itemize}

This package is intended to be an add-on to a comprehensive Times-like text font package, such as {\tt newtxtext} or {\tt tgtermes}, adding the possibility of writing parts in Greek (monotonic, polytonic and ancient) and Cyrillic. (Note that the Courier and Helvetica clones support only monotonic Greek.) All \LaTeX\ support files are provided in encodings T$1$, TS$1$, LGR, T$2$A, T$2$B, T$2$C, OT$1$ and OT$2$. The OT$2$ encoding and its usage is described in the last section of this document.

Another option for serifed Greek and Cyrillic in a font matching Times is the recently released Tempora package.

\section*{The Courier clone}
All weights of {\tt zco} have an advance width of {\tt 600em}. When used at {\tt 12pt}, this amounts to {\tt 10} characters per inch. While Courier and its clones don't seem very interesting as typewriter fonts because they appear to be too wide and too light (see however the narrower version {\tt NimbusMonoN} below), they are essential to screenwriters who choose to make use of \LaTeX. Unlike John Pate's  {\tt screenplay} package, {\tt screenplay-pkg} (Alan Munn's reworked version) does not hard-code the {\tt courier} package for use as its output font, but allows the use of whatever \verb|\ttdefault| is defined to be when {\tt screenplay-pkg} is loaded. So, to write a screenplay using {\tt 10cpi} Courier, as the screenplay rules require, you could set it up with preamble
\begin{verbatim}
\documentclass[12pt]{article}
\usepackage{nimbusserif} %Times for roman text, if any
\usepackage{nimbusmono} %Courier at 10cpi, regular (medium) weight
\usepackage{screenplay-pkg}
\end{verbatim}
The following three lines illustrate the three weights provided by {\tt zco}.\\
{\usefont{T1}{NimbusMono}{l}{n}zco-Light---the default weight of traditional Courier;}\\
{\usefont{T1}{NimbusMono}{m}{n}zco-Regular---the default Courier medium weight in this package;}\\
{\usefont{T1}{NimbusMono}{b}{n}zco-BoldBold---the default weight of traditional Bold Courier.}

The next two lines show for comparison {\tt NimbusMonoN} (the narrower version of {\tt zco-Regular}) and {\tt cmtt10}.
{\usefont{T1}{NimbusMonoN}{m}{n} NimbusMonoN (Narrow)---a new, narrower Courier. (Regular weight, upright and {\usefont{T1}{NimbusMonoN}{m}{sl}oblique} shapes only.)}\\
{\usefont{OT1}{cmtt}{m}{n} Computer Modern Typewriter (cmtt)---the traditional {\tt tt for} TeX users.}

{\tt NimbusMonoN} is more compact (advance widths {\tt 500em} vs.\ {\tt 525em}) but a bit lighter ({\tt 64em} stems vs.\ {\tt 69em} stems) than Computer Modern Typewriter. IMO, both are good for rendering lines of code in \TeX.
 
Note that the {\tt courier} package brings up the old URW courier clone (unless you have the Adobe version of the PS35 and have chosen to prefer them to URW), which is essentially the same for basic Latin glyphs as {\tt zco} and so it renders in this case the same as {\tt Light}.

The package {\tt nimbusmono} has a {\tt scaled} (or, equivalently, {\tt scale}) option. It has four other options that may be used to select the weights that \LaTeX\ will render as {\tt medium} and {\tt bold}. These are:
\begin{itemize}
\item
{\tt light}---\LaTeX\ medium renders with {\tt zco-Light};
\item
{\tt regular}---\LaTeX\ medium renders with {\tt zco-Regular};
\item
{\tt semibold}---\LaTeX\ bold renders with {\tt zco-Regular};
\item
{\tt bold}---\LaTeX\ bold renders with {\tt zco-Bold}.
\end{itemize}
Unless specified otherwise, {\tt nimbusmono} sets the options {\tt regular, bold}.

The package {\tt nimbusmononarrow} has a {\tt scaled} (or, equivalently, {\tt scale}) option, but no others, and offers only one weight (regular) in two styles style---upright and oblique. It is illustrated above. As for  advance widths, {\tt nimbusmono} is {\tt600em}, {\tt nimbusmononarrow} is {\tt500em} and {\tt cmtt10} is {\tt525em}.

\subsection*{A comparison of widths of some free Typewriter fonts} This list is not meant to be exhaustive or even extensive. 
\def\mystr{A sample of 46 characters to illustrate width.}
\font\zin=t1-zi4nr-0 at 11 pt
\font\cmtt=ec-lmtt12 at 11 pt
\font\cmttc=ec-lmtlc10 at 11 pt 
\font\ntxtt=t1xtt at 11 pt
\font\cor=zco-Regular-t1 at 11 pt
%\font\corn=zcon-Regular-t1 at 11 pt
\begin{center}
  \begin{tabular}{@{} ll @{}}
    \toprule
    Font & The same 46 characters \\ 
    \midrule
    Inconsolata & \texttt{\mystr} \\ 
    InconsolataNarrow & {\zin\mystr} \\ 
    cmtt (lmtt) & {\cmtt\mystr} \\ 
    lmtt condensed & {\cmttc\mystr} \\ 
    ntxtt & {\ntxtt\mystr} \\ 
    nimbusmono & {\cor\mystr} \\ 
    nimbusmononarrow & {\usefont{T1}{NimbusMonoN}{m}{n}\mystr} \\ 
    \bottomrule
  \end{tabular}
\end{center}
It appears that {\tt lmtt condensed} is the narrowest free monospace font, but is very light and not so easy to read. Of the regular weight free monospaced fonts, the narrowest  is {\tt InconsolataN} (sans serif), followed by {\tt nimbusmononarrow} (serifed.)
\newpage
\section*{Some Font tables}
\def\tfmname{zco-Regular-t1}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}


\newpage
\def\tfmname{zcoN-Regular-t1}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

\newpage
\def\tfmname{zcoN-Regular-ot1}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

Mimics {\tt cmtt} layout very closely.
\newpage
\def\tfmname{ztm-Reg-ot2}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

\newpage
\def\tfmname{ztm-Reg-lgr}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

Example:  \foreignlanguage{greek}{χοίρος}.

\newpage
\def\tfmname{zhv-Reg-lgr}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

\newpage
\def\tfmname{ztm-Reg-t2a}
\textbf{\tfmname} \textsc{glyph table}:\hspace{.3in} 
\fonttable{\tfmname}

Example:  \foreignlanguage{russian}{который}.
\newpage
\section*{Usage}
There are two basic pathways that can be followed, one based on {\tt fontspec} (XeLaTeX or LuaLaTeX), the other on traditional LaTeX ({\tt pdflatex} or {\tt latex/dvips}).
\subsection*{Fontspec} With {\tt fontspec}, the setup is fairly simple. Nimbus15 supplies a file named {\tt ztm.fontspec} for Times with content
\begin{verbatim}
\defaultfontfeatures[ztm]
  {
  Extension = .otf ,
  UprightFont = *-Reg,
  BoldFont = *-Med,
  ItalicFont = *-Ita,
  BoldItalicFont = *-MedIta,
  SlantedFont = *-RegObl,
  BoldSlantedFont = *-MedObl,
  }
\end{verbatim}
This file will be read by {\tt fontspec} whenever {\tt ztm} is loaded as a font, thereby simplifying the information you have to provide. (There are similar files {\tt zco.fontspec} for Courier,  {\tt zcoN.fontspec} for Courier Narrow and {\tt zhv.fontspec} for Helvetica.)

\textsc{Example:}
\begin{verbatim}
\usepackage{fontspec}
\defaultfontfeatures{Mapping=tex-text}
\setmainfont{TeX Gyre Termes}% assumes it to be in one of your fonts folders
\newfontfamily{\nim}{ztm} % reads ztm.fontspec
\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Gill Sans}
\setmonofont{zco}[Scale=MatchLowercase]% reads zco.fontspec
\end{verbatim}
so that {\tt utf8}-encoded text within a \verb|\nim{}| container will be rendered using {\tt ztm} and, by default, all other text will be rendered using {\tt TeX Gyre Termes}. You will most likely also wish to load the {\tt polyglossia} package to replace {\tt babel}.
\subsection*{LaTeX} The loading order of packages is important here. See the documentation of the {\tt newtx} package for details. Here's an example of using {\tt newtx} text and math, set up to allow the use of  Greek, Russian and English as the main language.
\begin{verbatim}
\usepackage[OT2,LGR,T2A,T1]{fontenc} % spell out all text encodings to be used
\usepackage[utf8]{inputenc} % 
\usepackage{substitutefont} % so we can use fonts other than those specified in babel
\usepackage[greek,russian,english]{babel}
\usepackage[largesc]{newtxtext} % 
\usepackage{nimbusmononarrow} % Courier Narrow
\usepackage{nimbussans} % Helvetica 
\usepackage[bigdelims,vvarbb]{newtxmath}
\useosf % use oldstyle figures except in math
\substitutefont{LGR}{\rmdefault}{NimbusSerif} % use nimbusserif to render Greek text
\substitutefont{T2A}{\rmdefault}{NimbusSerif} % use nimbusserif to render Russian 
\substitutefont{OT2}{\rmdefault}{NimbusSerif} % poor man's version
\end{verbatim}
Any {\tt utf8}-encoded text typed outside of a \verb|\foreignlanguage{}{}| block will be rendered as T$1$-encoded {\tt newtxtext}, while that within \verb|\foreignlanguage{greek}{}| will be rendered as LGR-encoded polytonic Greek, and that within \verb|\foreignlanguage{russian}{}| will render as T$2$A-encoded Cyrillic. 

 
The macro \verb|\textgreek| made available by {\tt babel-greek} may be used to avoid unicode. For example, \verb+\textgreek{>agaj\~{h}| t'uqh|?}+ renders as \textgreek{>agaj\~{h}| t'aqh|?}. 

 The macro \verb|\LGCscale| can be set if you wish to rescale the {\tt NimbusSerif} text. For example, \verb|\def\LGCscale{1.05}| will scale it up by 5\%. This is handled automatically for you by {\tt newtxtext} if you set its scale using the {\tt scaled} option. (In fact, there is another macro that takes priority over {\tt LGCscale}, named {\tt NimbusSerifscale}---it is automatically set if you load the package {\tt nimbusserif}, which should not be loaded if you also use {\tt newtxtext}.)

\section{The OT2 encoding}
Though OT$2$ is considered obsolete, as an essentially $7$-bit encoding, it is quite useful to those who work with a non-Cyrillic keyboard but wish to produce documents containing a limited amount of Cyrillic text,  Latin characters being appropriately transliterated to Cyrillic, and with predefined macros available for each Cyrillic character. These are specified in {\tt ot2enc.def} in your \TeX\ distribution.

 The following table, taken from a document on OT$2$ by Walter Schmidt that seems not to be publicly available at the moment, shows the transliteration scheme specified by ligatures in the {\tt OT2} encoding.
In each column of this table, each row shows first a Cyrillic character pair (upper and lower case), then the pair of keys or key sequences required on the input side. In some cases alternatives are also specified.

\includegraphics{ot2trans}

To use OT$2$ transliterated from Latin characters, accented characters and ligatures doesn't require {\tt babel}. For example:
\begin{verbatim}
\documentclass{article} 
\usepackage[OT2,T1]{fontenc} % loads ot2enc.def
\newcommand\cyrtext{\fontencoding{OT2}\selectfont} % declaration
\DeclareTextFontCommand{\textcyr}{\cyrtext}  %macro with argument
\end{verbatim}
The Russian part of the following sentence is entered as \verb|\textcyr{a e1to --- po-russki}|.

This is text in English, then Russian:
\textcyr{a e1to --- po-russki}.

\end{document}  