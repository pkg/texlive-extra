
## niceframe-type1

The Type1 versions of the fonts

-  bbding10.mf
-  dingbat.mf
-  karta15.mf
-  umranda.mf
-  umrandb.mf

are created using the font editor `fontforge' with a bit 
handwork for `umranda.pfb'. The fonts have the same license 
as the original Metafont sources 
(the LaTeX Project Public Li­cense).

Rolf Niepraschk <Rolf.Niepraschk@gmx.de>, 2017-06-21
