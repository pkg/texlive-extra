
    %% start of file `typicon.tex'.
    %% Copyright 2015 Arthur Vigil (arthur.h.vigil@gmail.com).
    %%
    %% Modified from fontawesome package by Xavier Danaux
    %% Copyright 2013 Xavier Danaux (xdanaux@gmail.com).
    %
    % This work may be distributed and/or modified under the
    % conditions of the LaTeX Project Public License version 1.3c,
    % available at http://www.latex-project.org/lppl/.
    
    \documentclass{ltxdoc}
    %\GetFileInfo{\jobname.sty}
    \def\fileversion{2.0.7}
    \def\filedate{May 20, 2015}
    \usepackage{lmodern}
    \usepackage[numbered]{hypdoc}
    \usepackage{hologo}
    \usepackage{hyperref, xcolor}
    \definecolor{myblue}{rgb}{0.22,0.45,0.70}% light blue
    \hypersetup{colorlinks=true, linkcolor=myblue, urlcolor=myblue, hyperindex}
    \usepackage{longtable, booktabs}
    \usepackage{tikz}
    \usepackage{xparse, ifthen}
    \usepackage{\jobname}
    \EnableCrossrefs
    \CodelineIndex
    \RecordChanges
    
    \begin{document}
    \title{The \textsf{\jobname} package\\Another set of high quality web icons}
    \author{%
    Arthur Vigil\thanks{E-mail: \href{mailto:arthur.h.vigil@gmail.com}{\tt arthur.h.vigil@gmail.com}} (\hologo{LaTeX} code)\\%
    Xavier Danaux (more \hologo{LaTeX} code)\\%
    Stephen Hutchings (font and icons design)}
    \date{Version \fileversion, released on \filedate}
    \maketitle
    
    \begin{abstract}
    The \textsf{\jobname} package grants access to 336 web-related icons provided by the included \emph{Typicon} free font, designed by Stephen Hutchings and released\footnote{See \url{http://www.typicons.com/} for more details about the font itself} under the open SIL Open Font License\footnote{Available at \url{http://scripts.sil.org/OFL}.}.
    
    This package requires the \textsf{fontspec} package and either the \hologo{Xe}\hologo{(La)TeX} or Lua\hologo{(La)TeX} engine to load the included ttf font.
    \end{abstract}
    
    \changes{v2.0.7}  {2015/05/20}{First public release (version number set to match the included typicons.ttf font version).}
    \makeatletter
    \let\PrintMacroName@original\PrintMacroName
    %\let\PrintDescribeMacro\@gobble
    %\let\PrintDescribeEnv\@gobble
    \let\PrintMacroName\@gobble
    %\let\PrintEnvName\@gobble
    \begin{macro}{\faTextHeight}
    \changes{v3.0.2-1}{2013/03/23}{Corrected binding.}
    \end{macro}
    \begin{macro}{\faTextWidth}
    \changes{v3.0.2-1}{2013/03/23}{Corrected binding.}
    \end{macro}
    \let\PrintMacroName\PrintMacroName@original
    \makeatother
    
    \bigskip
    
    \section{Introduction}
    The \textsf{\jobname} package aims to enable easy access in \hologo{(La)TeX} to high quality
    \hyperref[section:icons]{icons}.
    It is a redistribution of the free (as in beer) \emph{Typicons} ttf font with specific bindings for \hologo{(La)TeX}, based on the similar \textsf{fontawesome} package.
    
    \section{Requirements}
    The \textsf{\jobname} package requires the \textsf{fontspec} package and either the \hologo{Xe}\hologo{(La)TeX} or Lua\hologo{(La)TeX} engine to load the included ttf font.
    
    \section{Usage}
    \DescribeMacro{\ticon}
    Once the \textsf{\jobname} package loaded, icons can be accessed through the general \cs{ticon}, which takes as mandatory argument the \meta{name} of the desired icon, or through a direct command specific to each icon. The full list of icon designs, names and direct commands are showcased next.
    
    \newenvironment{showcase}%
    {%
    %   \begin{longtable}{ccp{4cm}p{5cm}p{1cm}}% debug: shows icons with both generic and specific commands
    \begin{longtable}{cp{4cm}p{5cm}p{1cm}}
    \cmidrule[\heavyrulewidth]{1-3}% \toprule
    %   \bfseries Icon& \bfseries Icon& \bfseries Name& \bfseries Direct command& \\% debug
    \bfseries Icon& \bfseries Name& \bfseries Direct command& \\
    \cmidrule{1-3}\endhead}
    {\cmidrule[\heavyrulewidth]{1-3}% \bottomrule
    \end{longtable}}
    \NewDocumentCommand{\showcaseicon}{mmg}{%
    %  \ticon{#1}& \csname#2\endcsname& \itshape #1& \ttfamily \textbackslash #2\index{\ttfamily \textbackslash #2}& \IfNoValueTF{#3}{}{\tag{#3}}\\}% debug
    \ticon{#1}& \itshape #1& \ttfamily \textbackslash #2\index{\ttfamily \textbackslash #2}& \IfNoValueTF{#3}{}{\tag{#3}}\\}
    \newcommand{\tag}[1]{{%
    \small\sffamily%
    \ifthenelse{\equal{#1}{new}}{%
    \tikz[baseline={(TAG.base)}]{
    \node[white, fill=myblue, rounded corners=3pt, inner sep=1.5pt] (TAG) {new!\vphantom{Ay!}};
    }}{}%
    \ifthenelse{\equal{#1}{gone}}{%
    \tikz[baseline={(TAG.base)}]{
    \node[black!50, fill=black!25, rounded corners=3pt, inner sep=1.5pt] (TAG) {gone (?)\vphantom{Ay!}};
    }}{}%
    \ifthenelse{\equal{#1}{alias}}{%
    \textcolor{black!50}{(alias)}}{}%
    }}
    
    \subsection{Typicon icons\label{section:icons}}
    \begin{showcase}
    	\showcaseicon{adjust-brightness}{tiAdjustBrightness}
	\showcaseicon{adjust-contrast}{tiAdjustContrast}
	\showcaseicon{anchor-outline}{tiAnchorOutline}
	\showcaseicon{anchor}{tiAnchor}
	\showcaseicon{archive}{tiArchive}
	\showcaseicon{arrow-back-outline}{tiArrowBackOutline}
	\showcaseicon{arrow-back}{tiArrowBack}
	\showcaseicon{arrow-down-outline}{tiArrowDownOutline}
	\showcaseicon{arrow-down-thick}{tiArrowDownThick}
	\showcaseicon{arrow-down}{tiArrowDown}
	\showcaseicon{arrow-forward-outline}{tiArrowForwardOutline}
	\showcaseicon{arrow-forward}{tiArrowForward}
	\showcaseicon{arrow-left-outline}{tiArrowLeftOutline}
	\showcaseicon{arrow-left-thick}{tiArrowLeftThick}
	\showcaseicon{arrow-left}{tiArrowLeft}
	\showcaseicon{arrow-loop-outline}{tiArrowLoopOutline}
	\showcaseicon{arrow-loop}{tiArrowLoop}
	\showcaseicon{arrow-maximise-outline}{tiArrowMaximiseOutline}
	\showcaseicon{arrow-maximise}{tiArrowMaximise}
	\showcaseicon{arrow-minimise-outline}{tiArrowMinimiseOutline}
	\showcaseicon{arrow-minimise}{tiArrowMinimise}
	\showcaseicon{arrow-move-outline}{tiArrowMoveOutline}
	\showcaseicon{arrow-move}{tiArrowMove}
	\showcaseicon{arrow-repeat-outline}{tiArrowRepeatOutline}
	\showcaseicon{arrow-repeat}{tiArrowRepeat}
	\showcaseicon{arrow-right-outline}{tiArrowRightOutline}
	\showcaseicon{arrow-right-thick}{tiArrowRightThick}
	\showcaseicon{arrow-right}{tiArrowRight}
	\showcaseicon{arrow-shuffle}{tiArrowShuffle}
	\showcaseicon{arrow-sorted-down}{tiArrowSortedDown}
	\showcaseicon{arrow-sorted-up}{tiArrowSortedUp}
	\showcaseicon{arrow-sync-outline}{tiArrowSyncOutline}
	\showcaseicon{arrow-sync}{tiArrowSync}
	\showcaseicon{arrow-unsorted}{tiArrowUnsorted}
	\showcaseicon{arrow-up-outline}{tiArrowUpOutline}
	\showcaseicon{arrow-up-thick}{tiArrowUpThick}
	\showcaseicon{arrow-up}{tiArrowUp}
	\showcaseicon{at}{tiAt}
	\showcaseicon{attachment-outline}{tiAttachmentOutline}
	\showcaseicon{attachment}{tiAttachment}
	\showcaseicon{backspace-outline}{tiBackspaceOutline}
	\showcaseicon{backspace}{tiBackspace}
	\showcaseicon{battery-charge}{tiBatteryCharge}
	\showcaseicon{battery-full}{tiBatteryFull}
	\showcaseicon{battery-high}{tiBatteryHigh}
	\showcaseicon{battery-low}{tiBatteryLow}
	\showcaseicon{battery-mid}{tiBatteryMid}
	\showcaseicon{beaker}{tiBeaker}
	\showcaseicon{beer}{tiBeer}
	\showcaseicon{bell}{tiBell}
	\showcaseicon{book}{tiBook}
	\showcaseicon{bookmark}{tiBookmark}
	\showcaseicon{briefcase}{tiBriefcase}
	\showcaseicon{brush}{tiBrush}
	\showcaseicon{business-card}{tiBusinessCard}
	\showcaseicon{calculator}{tiCalculator}
	\showcaseicon{calendar-outline}{tiCalendarOutline}
	\showcaseicon{calendar}{tiCalendar}
	\showcaseicon{camera-outline}{tiCameraOutline}
	\showcaseicon{camera}{tiCamera}
	\showcaseicon{cancel-outline}{tiCancelOutline}
	\showcaseicon{cancel}{tiCancel}
	\showcaseicon{chart-area-outline}{tiChartAreaOutline}
	\showcaseicon{chart-area}{tiChartArea}
	\showcaseicon{chart-bar-outline}{tiChartBarOutline}
	\showcaseicon{chart-bar}{tiChartBar}
	\showcaseicon{chart-line-outline}{tiChartLineOutline}
	\showcaseicon{chart-line}{tiChartLine}
	\showcaseicon{chart-pie-outline}{tiChartPieOutline}
	\showcaseicon{chart-pie}{tiChartPie}
	\showcaseicon{chevron-left-outline}{tiChevronLeftOutline}
	\showcaseicon{chevron-left}{tiChevronLeft}
	\showcaseicon{chevron-right-outline}{tiChevronRightOutline}
	\showcaseicon{chevron-right}{tiChevronRight}
	\showcaseicon{clipboard}{tiClipboard}
	\showcaseicon{cloud-storage}{tiCloudStorage}
	\showcaseicon{cloud-storage-outline}{tiCloudStorageOutline}
	\showcaseicon{code-outline}{tiCodeOutline}
	\showcaseicon{code}{tiCode}
	\showcaseicon{coffee}{tiCoffee}
	\showcaseicon{cog-outline}{tiCogOutline}
	\showcaseicon{cog}{tiCog}
	\showcaseicon{compass}{tiCompass}
	\showcaseicon{contacts}{tiContacts}
	\showcaseicon{credit-card}{tiCreditCard}
	\showcaseicon{css3}{tiCss3}
	\showcaseicon{database}{tiDatabase}
	\showcaseicon{delete-outline}{tiDeleteOutline}
	\showcaseicon{delete}{tiDelete}
	\showcaseicon{device-desktop}{tiDeviceDesktop}
	\showcaseicon{device-laptop}{tiDeviceLaptop}
	\showcaseicon{device-phone}{tiDevicePhone}
	\showcaseicon{device-tablet}{tiDeviceTablet}
	\showcaseicon{directions}{tiDirections}
	\showcaseicon{divide-outline}{tiDivideOutline}
	\showcaseicon{divide}{tiDivide}
	\showcaseicon{document-add}{tiDocumentAdd}
	\showcaseicon{document-delete}{tiDocumentDelete}
	\showcaseicon{document-text}{tiDocumentText}
	\showcaseicon{document}{tiDocument}
	\showcaseicon{download-outline}{tiDownloadOutline}
	\showcaseicon{download}{tiDownload}
	\showcaseicon{dropbox}{tiDropbox}
	\showcaseicon{edit}{tiEdit}
	\showcaseicon{eject-outline}{tiEjectOutline}
	\showcaseicon{eject}{tiEject}
	\showcaseicon{equals-outline}{tiEqualsOutline}
	\showcaseicon{equals}{tiEquals}
	\showcaseicon{export-outline}{tiExportOutline}
	\showcaseicon{export}{tiExport}
	\showcaseicon{eye-outline}{tiEyeOutline}
	\showcaseicon{eye}{tiEye}
	\showcaseicon{feather}{tiFeather}
	\showcaseicon{film}{tiFilm}
	\showcaseicon{filter}{tiFilter}
	\showcaseicon{flag-outline}{tiFlagOutline}
	\showcaseicon{flag}{tiFlag}
	\showcaseicon{flash-outline}{tiFlashOutline}
	\showcaseicon{flash}{tiFlash}
	\showcaseicon{flow-children}{tiFlowChildren}
	\showcaseicon{flow-merge}{tiFlowMerge}
	\showcaseicon{flow-parallel}{tiFlowParallel}
	\showcaseicon{flow-switch}{tiFlowSwitch}
	\showcaseicon{folder-add}{tiFolderAdd}
	\showcaseicon{folder-delete}{tiFolderDelete}
	\showcaseicon{folder-open}{tiFolderOpen}
	\showcaseicon{folder}{tiFolder}
	\showcaseicon{gift}{tiGift}
	\showcaseicon{globe-outline}{tiGlobeOutline}
	\showcaseicon{globe}{tiGlobe}
	\showcaseicon{group-outline}{tiGroupOutline}
	\showcaseicon{group}{tiGroup}
	\showcaseicon{headphones}{tiHeadphones}
	\showcaseicon{heart-full-outline}{tiHeartFullOutline}
	\showcaseicon{heart-half-outline}{tiHeartHalfOutline}
	\showcaseicon{heart-outline}{tiHeartOutline}
	\showcaseicon{heart}{tiHeart}
	\showcaseicon{home-outline}{tiHomeOutline}
	\showcaseicon{home}{tiHome}
	\showcaseicon{html5}{tiHtml5}
	\showcaseicon{image-outline}{tiImageOutline}
	\showcaseicon{image}{tiImage}
	\showcaseicon{infinity-outline}{tiInfinityOutline}
	\showcaseicon{infinity}{tiInfinity}
	\showcaseicon{info-large-outline}{tiInfoLargeOutline}
	\showcaseicon{info-large}{tiInfoLarge}
	\showcaseicon{info-outline}{tiInfoOutline}
	\showcaseicon{info}{tiInfo}
	\showcaseicon{input-checked-outline}{tiInputCheckedOutline}
	\showcaseicon{input-checked}{tiInputChecked}
	\showcaseicon{key-outline}{tiKeyOutline}
	\showcaseicon{key}{tiKey}
	\showcaseicon{keyboard}{tiKeyboard}
	\showcaseicon{leaf}{tiLeaf}
	\showcaseicon{lightbulb}{tiLightbulb}
	\showcaseicon{link-outline}{tiLinkOutline}
	\showcaseicon{link}{tiLink}
	\showcaseicon{location-arrow-outline}{tiLocationArrowOutline}
	\showcaseicon{location-arrow}{tiLocationArrow}
	\showcaseicon{location-outline}{tiLocationOutline}
	\showcaseicon{location}{tiLocation}
	\showcaseicon{lock-closed-outline}{tiLockClosedOutline}
	\showcaseicon{lock-closed}{tiLockClosed}
	\showcaseicon{lock-open-outline}{tiLockOpenOutline}
	\showcaseicon{lock-open}{tiLockOpen}
	\showcaseicon{mail}{tiMail}
	\showcaseicon{map}{tiMap}
	\showcaseicon{media-eject-outline}{tiMediaEjectOutline}
	\showcaseicon{media-eject}{tiMediaEject}
	\showcaseicon{media-fast-forward-outline}{tiMediaFastForwardOutline}
	\showcaseicon{media-fast-forward}{tiMediaFastForward}
	\showcaseicon{media-pause-outline}{tiMediaPauseOutline}
	\showcaseicon{media-pause}{tiMediaPause}
	\showcaseicon{media-play-outline}{tiMediaPlayOutline}
	\showcaseicon{media-play-reverse-outline}{tiMediaPlayReverseOutline}
	\showcaseicon{media-play-reverse}{tiMediaPlayReverse}
	\showcaseicon{media-play}{tiMediaPlay}
	\showcaseicon{media-record-outline}{tiMediaRecordOutline}
	\showcaseicon{media-record}{tiMediaRecord}
	\showcaseicon{media-rewind-outline}{tiMediaRewindOutline}
	\showcaseicon{media-rewind}{tiMediaRewind}
	\showcaseicon{media-stop-outline}{tiMediaStopOutline}
	\showcaseicon{media-stop}{tiMediaStop}
	\showcaseicon{message-typing}{tiMessageTyping}
	\showcaseicon{message}{tiMessage}
	\showcaseicon{messages}{tiMessages}
	\showcaseicon{microphone-outline}{tiMicrophoneOutline}
	\showcaseicon{microphone}{tiMicrophone}
	\showcaseicon{minus-outline}{tiMinusOutline}
	\showcaseicon{minus}{tiMinus}
	\showcaseicon{mortar-board}{tiMortarBoard}
	\showcaseicon{news}{tiNews}
	\showcaseicon{notes-outline}{tiNotesOutline}
	\showcaseicon{notes}{tiNotes}
	\showcaseicon{pen}{tiPen}
	\showcaseicon{pencil}{tiPencil}
	\showcaseicon{phone-outline}{tiPhoneOutline}
	\showcaseicon{phone}{tiPhone}
	\showcaseicon{pi-outline}{tiPiOutline}
	\showcaseicon{pi}{tiPi}
	\showcaseicon{pin-outline}{tiPinOutline}
	\showcaseicon{pin}{tiPin}
	\showcaseicon{pipette}{tiPipette}
	\showcaseicon{plane-outline}{tiPlaneOutline}
	\showcaseicon{plane}{tiPlane}
	\showcaseicon{plug}{tiPlug}
	\showcaseicon{plus-outline}{tiPlusOutline}
	\showcaseicon{plus}{tiPlus}
	\showcaseicon{point-of-interest-outline}{tiPointOfInterestOutline}
	\showcaseicon{point-of-interest}{tiPointOfInterest}
	\showcaseicon{power-outline}{tiPowerOutline}
	\showcaseicon{power}{tiPower}
	\showcaseicon{printer}{tiPrinter}
	\showcaseicon{puzzle-outline}{tiPuzzleOutline}
	\showcaseicon{puzzle}{tiPuzzle}
	\showcaseicon{radar-outline}{tiRadarOutline}
	\showcaseicon{radar}{tiRadar}
	\showcaseicon{refresh-outline}{tiRefreshOutline}
	\showcaseicon{refresh}{tiRefresh}
	\showcaseicon{rss-outline}{tiRssOutline}
	\showcaseicon{rss}{tiRss}
	\showcaseicon{scissors-outline}{tiScissorsOutline}
	\showcaseicon{scissors}{tiScissors}
	\showcaseicon{shopping-bag}{tiShoppingBag}
	\showcaseicon{shopping-cart}{tiShoppingCart}
	\showcaseicon{social-at-circular}{tiSocialAtCircular}
	\showcaseicon{social-dribbble-circular}{tiSocialDribbbleCircular}
	\showcaseicon{social-dribbble}{tiSocialDribbble}
	\showcaseicon{social-facebook-circular}{tiSocialFacebookCircular}
	\showcaseicon{social-facebook}{tiSocialFacebook}
	\showcaseicon{social-flickr-circular}{tiSocialFlickrCircular}
	\showcaseicon{social-flickr}{tiSocialFlickr}
	\showcaseicon{social-github-circular}{tiSocialGithubCircular}
	\showcaseicon{social-github}{tiSocialGithub}
	\showcaseicon{social-google-plus-circular}{tiSocialGooglePlusCircular}
	\showcaseicon{social-google-plus}{tiSocialGooglePlus}
	\showcaseicon{social-instagram-circular}{tiSocialInstagramCircular}
	\showcaseicon{social-instagram}{tiSocialInstagram}
	\showcaseicon{social-last-fm-circular}{tiSocialLastFmCircular}
	\showcaseicon{social-last-fm}{tiSocialLastFm}
	\showcaseicon{social-linkedin-circular}{tiSocialLinkedinCircular}
	\showcaseicon{social-linkedin}{tiSocialLinkedin}
	\showcaseicon{social-pinterest-circular}{tiSocialPinterestCircular}
	\showcaseicon{social-pinterest}{tiSocialPinterest}
	\showcaseicon{social-skype-outline}{tiSocialSkypeOutline}
	\showcaseicon{social-skype}{tiSocialSkype}
	\showcaseicon{social-tumbler-circular}{tiSocialTumblerCircular}
	\showcaseicon{social-tumbler}{tiSocialTumbler}
	\showcaseicon{social-twitter-circular}{tiSocialTwitterCircular}
	\showcaseicon{social-twitter}{tiSocialTwitter}
	\showcaseicon{social-vimeo-circular}{tiSocialVimeoCircular}
	\showcaseicon{social-vimeo}{tiSocialVimeo}
	\showcaseicon{social-youtube-circular}{tiSocialYoutubeCircular}
	\showcaseicon{social-youtube}{tiSocialYoutube}
	\showcaseicon{sort-alphabetically-outline}{tiSortAlphabeticallyOutline}
	\showcaseicon{sort-alphabetically}{tiSortAlphabetically}
	\showcaseicon{sort-numerically-outline}{tiSortNumericallyOutline}
	\showcaseicon{sort-numerically}{tiSortNumerically}
	\showcaseicon{spanner-outline}{tiSpannerOutline}
	\showcaseicon{spanner}{tiSpanner}
	\showcaseicon{spiral}{tiSpiral}
	\showcaseicon{star-full-outline}{tiStarFullOutline}
	\showcaseicon{star-half-outline}{tiStarHalfOutline}
	\showcaseicon{star-half}{tiStarHalf}
	\showcaseicon{star-outline}{tiStarOutline}
	\showcaseicon{star}{tiStar}
	\showcaseicon{starburst-outline}{tiStarburstOutline}
	\showcaseicon{starburst}{tiStarburst}
	\showcaseicon{stopwatch}{tiStopwatch}
	\showcaseicon{support}{tiSupport}
	\showcaseicon{tabs-outline}{tiTabsOutline}
	\showcaseicon{tag}{tiTag}
	\showcaseicon{tags}{tiTags}
	\showcaseicon{th-large-outline}{tiThLargeOutline}
	\showcaseicon{th-large}{tiThLarge}
	\showcaseicon{th-list-outline}{tiThListOutline}
	\showcaseicon{th-list}{tiThList}
	\showcaseicon{th-menu-outline}{tiThMenuOutline}
	\showcaseicon{th-menu}{tiThMenu}
	\showcaseicon{th-small-outline}{tiThSmallOutline}
	\showcaseicon{th-small}{tiThSmall}
	\showcaseicon{thermometer}{tiThermometer}
	\showcaseicon{thumbs-down}{tiThumbsDown}
	\showcaseicon{thumbs-ok}{tiThumbsOk}
	\showcaseicon{thumbs-up}{tiThumbsUp}
	\showcaseicon{tick-outline}{tiTickOutline}
	\showcaseicon{tick}{tiTick}
	\showcaseicon{ticket}{tiTicket}
	\showcaseicon{time}{tiTime}
	\showcaseicon{times-outline}{tiTimesOutline}
	\showcaseicon{times}{tiTimes}
	\showcaseicon{trash}{tiTrash}
	\showcaseicon{tree}{tiTree}
	\showcaseicon{upload-outline}{tiUploadOutline}
	\showcaseicon{upload}{tiUpload}
	\showcaseicon{user-add-outline}{tiUserAddOutline}
	\showcaseicon{user-add}{tiUserAdd}
	\showcaseicon{user-delete-outline}{tiUserDeleteOutline}
	\showcaseicon{user-delete}{tiUserDelete}
	\showcaseicon{user-outline}{tiUserOutline}
	\showcaseicon{user}{tiUser}
	\showcaseicon{vendor-android}{tiVendorAndroid}
	\showcaseicon{vendor-apple}{tiVendorApple}
	\showcaseicon{vendor-microsoft}{tiVendorMicrosoft}
	\showcaseicon{video-outline}{tiVideoOutline}
	\showcaseicon{video}{tiVideo}
	\showcaseicon{volume-down}{tiVolumeDown}
	\showcaseicon{volume-mute}{tiVolumeMute}
	\showcaseicon{volume-up}{tiVolumeUp}
	\showcaseicon{volume}{tiVolume}
	\showcaseicon{warning-outline}{tiWarningOutline}
	\showcaseicon{warning}{tiWarning}
	\showcaseicon{watch}{tiWatch}
	\showcaseicon{waves-outline}{tiWavesOutline}
	\showcaseicon{waves}{tiWaves}
	\showcaseicon{weather-cloudy}{tiWeatherCloudy}
	\showcaseicon{weather-downpour}{tiWeatherDownpour}
	\showcaseicon{weather-night}{tiWeatherNight}
	\showcaseicon{weather-partly-sunny}{tiWeatherPartlySunny}
	\showcaseicon{weather-shower}{tiWeatherShower}
	\showcaseicon{weather-snow}{tiWeatherSnow}
	\showcaseicon{weather-stormy}{tiWeatherStormy}
	\showcaseicon{weather-sunny}{tiWeatherSunny}
	\showcaseicon{weather-windy-cloudy}{tiWeatherWindyCloudy}
	\showcaseicon{weather-windy}{tiWeatherWindy}
	\showcaseicon{wi-fi-outline}{tiWiFiOutline}
	\showcaseicon{wi-fi}{tiWiFi}
	\showcaseicon{wine}{tiWine}
	\showcaseicon{world-outline}{tiWorldOutline}
	\showcaseicon{world}{tiWorld}
	\showcaseicon{zoom-in-outline}{tiZoomInOutline}
	\showcaseicon{zoom-in}{tiZoomIn}
	\showcaseicon{zoom-out-outline}{tiZoomOutOutline}
	\showcaseicon{zoom-out}{tiZoomOut}
	\showcaseicon{zoom-outline}{tiZoomOutline}
	\showcaseicon{zoom}{tiZoom}

            \end{showcase}

            \end{document}
            
            
            %% end of file `typicons.tex'.
    