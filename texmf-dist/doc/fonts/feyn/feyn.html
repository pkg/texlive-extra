<?xml version="1.0" encoding='utf-8'?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Feyn font</title>
<link href="https://nxg.me.uk" rev="author"/>
<link type="text/css" rel="stylesheet" href="style.css"/>
</head>

<body>
<h1>Feyn font</h1>
<div class="abstract">
<p>A package of fonts for displaying Feynman diagrams.</p>
<p>Version 0.4.3, 2022 July 20.</p>
<ul>
<li>Canonical URL: <code style='font-size: larger;'>https://purl.org/nxg/dist/feyn</code></li>
<li><a href='https://heptapod.host/nxg/feyn'>Repository</a></li>
</ul>
</div>


<p>This package is intended to produce relatively simple Feynman diagrams,
primarily for use within equations.</p>

<p><img class="smallimage" src="sample-two-loop.png" alt="Two-loop diagram"
/>This package differs from Thorsten Ohl’s
<code>feynmf/feynmp</code> package (at <a href="https://ctan.org/pkg/feynmf">CTAN</a>,
or see the
<a href="https://texfaq.org/FAQ-drawFeyn" >TeX FAQ</a>).
FeynMP works by creating Metafont or MetaPost figures using a
preprocessor.  It’s more general than this package, but is at its best
when creating relatively large diagrams, for figures.  In contrast,
the present system consists of a carefully-designed font with which
you can easily write simple diagrams, within equations or within text, in a
size matching the surrounding text size.</p>

<p><img class="smallimage" src="sample-massive.png" alt="Massive fermions"
/>The propagators and vertices which are implemented are those which
seem to appear most often in non-figure displays (the practical
definition of ‘appear most often’ is ‘which I wanted’ and ‘which people
asked me for’).  I have no current plans to extend the package
further, but I’m willing add other features if you can make a case for
them.</p>

<p>There is further documentation, plus numerous examples, in the
<a href="feyn.pdf" >package documentation</a>.</p>

<p><strong>[By the way, I have little idea of how many people actually
use this package.  If you do use it, I’d be most grateful if you could
drop me a line just to say so]</strong></p>

<h2>Licence</h2>

<p>This software is copyright 1991, 1994, 2001-2, 2005, 2008-17, Norman Gray..
It is distributed under the terms of the <a href='LICENCE'>2-clause BSD Licence</a>.</p>

<h2>History</h2>

<dl>

<!-- @RELEASENOTES@ -->
<dt><span class='attention'>Version 0.4.3, 2022 July 20</span></dt>
<dd>Documentation update, including fixes for broken URIs, and more
consistent use of the canonical homepage URI,
<a href='https://purl.org/nxg/dist/feyn'><code>https://purl.org/nxg/dist/feyn</code></a>.</dd>

<dt><span class='attention'>Version 0.4.2, 2022 July 18</span></dt>
<dd>
<p>No code changes, but updates to URLs, including the repository location.</p>
</dd>

<dt><span class='attention'>Version 0.4.1, 2017 November 3</span></dt>
<dd><p>The package is now licensed under the terms of the 2-clause BSD licence.</p>
<p>The only other changes from v0.4b2 are mild documentation adjustments
(sorry for the long delay between 0.4b2 and 0.4).</p>
<p>Version 0.4 was not widely released.</p></dd>

<dt>Version 0.4b3, 2013 April 12</dt>
<dd><ul>
<li>Changed <code>\below</code> to <code>\belowl</code>, and added
companion <code>\belowr</code>.</li>
<li>Documentation updates</li>
</ul></dd>

<dt>Version 0.4b2, 2011 April 9</dt>
<dd><p><img class="smallimage" src="sample-phi4.png" alt="Vertex"/> As
for version 0.4b1, but with the addition of <code>aV</code> for a
backward arrow (rarely used, but included for consistency; thanks to
Thomas Pruschke for the suggestion).  Also added: <code>flo</code> for
the closed fermion loop, for phi^4 theory (plus <code>floV</code> and
<code>floA</code> for completeness; thanks to Hoang Nghia Nguyen for the suggestion;
the font should <em>now</em> be able to produce all scalar theory
diagrams).</p></dd>

<dt>Version 0.4b1, 2010 January 23</dt>
<dd><p>Addition of the extended-size `feynx' fonts, and the <code>\FEYN</code> macro
(thanks to Luciano Pandola for the suggestion).</p>
<p>Adjustments to the sizing of half-loops
(the half-loops are now squashed, though the quarter-loops aren't).
Added ghost loops; we can now do all the diagrams of 2-loop QCD.
The quarter-loop gluons now taper at both ends, so that two quarter-loops
are no longer the same as one half-loop.</p></dd>


<dt>...</dt>
<dd><a href='release-notes.html'>Earlier release notes</a> are available</dd>

</dl>

<h2>Download and installation</h2>

<p>The nominal distribution point for the Feyn package is CTAN,
under <a href="https://ctan.org/pkg/feyn">fonts/feyn</a>.</p>

<p>The feyn package is included in the
<a href='https://www.tug.org/texlive/'>TeXLive distribution</a>, so
you may have it on your machine already.  If not, or if you need a
more up-to-date version, read on.</p>

<p>Since the package is on CTAN, it should be easily installed and
updated using the tools which support this in your TeX distribution.
For TeXLive, that's <a href='https://www.tug.org/texlive/tlmgr.html'>tlmgr</a>,
and for MikTeX see the maintenance section of
<a href='https://docs.miktex.org'>the manual</a>.</p>

<h3>Doing the installation by hand</h3>

<p>...how thoroughly old-skool of you!</p>

<ol>
<li>Download either
<a href="feyn-0.4.3.tar.gz" >feyn-0.4.3.tar.gz</a>
or
<a href="feyn-0.4.3.zip" >feyn-0.4.3.zip</a>
and unpack it.</li>

<li>Run LaTeX on the file <code>feyn.ins</code> – this will unpack the
style file <code>feyn.sty</code> amongst other files.  Place this
‘somewhere where TeX can find it’.  Similarly, place the
<code>.mf</code> files somewhere where the font-generation mechanism
for your platform can find them.  For example, in the TeXLive
distribution, the feyn <code>*.mf</code> files appear in the directory
<code>/usr/local/texlive/2008/texmf-dist/fonts/source/public/feyn/</code>
and the <code>feyn.sty</code> file in
<code>/usr/local/texlive/2008/texmf-dist/tex/latex/feyn/feyn.sty</code>,
so that if I were installing them ‘privately’, I would put them in
<code>/usr/local/texlive/texmf-local/fonts/source/local/feyn/</code>
and
<code>/usr/local/texlive/texmf-local/tex/latex/feyn/feyn.sty</code>.
With the teTeX and TeXLive Unix distributions, the output of
<code>kpsepath mf</code> shows the list of places where TeX will look
for Metafont source files.  The font files <em>should</em> be
generated on the fly when you first refer to them.  See also the
relevant discussion in the TeX FAQ about <a
href='https://texfaq.org/FAQ-inst-wlcf'
>‘where LaTeX can find them’</a>.</li>

<li>Run LaTeX on the file <code>feyn.dtx</code> to obtain the
documentation, which is prebuilt in <code><a href="feyn.pdf"
>feyn.pdf</a></code> (this should
generate the font files as a side-effect).</li>
</ol>

<p>You should be able to find generic instructions for installing LaTeX files at
<a href='https://texfaq.org/#installing' >the TeX FAQ</a>.</p>



<div class="signature">
<a href="https://nxg.me.uk" >Norman Gray</a><br/>
2022 July 20
</div>

</body>
</html>


<!-- Local Variables: -->
<!-- mode: nxml -->
<!-- End: -->
