% !TEX TS-program = pdflatexmk 
% Template file for TeXShop by Michael Sharpe, LPPL
\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage[parfill]{parskip}% Begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\pdfmapfile{=AlgolRevived.map}
%SetFonts
% algolrevived
\usepackage{algolrevived} 
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl,scaled=1.04]{zi4}% inconsolata
%\usepackage[tt]{algolrevived}
%\usepackage{amsmath,amsthm}
%\usepackage[libertine,bigdelims,vvarbb]{newtxmath}
% option vvarbb gives you stix blackboard bold
%SetFonts
\usepackage{trace}
\usepackage{fonttable}
\title{Algol Revived\footnote{Creation of this package was spurred by Barbara Beeton's column in a recent TUGBoat, conveying a request from Jacques Andr\'e for someone to digitize Frutiger's Algol alphabet.}}
\author{Michael Sharpe}
\date{\today}  % Activate to display a given date or no date

\begin{document}
\maketitle
\emph{AlgolRevived} is a revival of the (photo)font \emph{Algol} by Adrian Frutiger  whose sole use was for printing ALGOL code in a manual.  It  is not meant to be a general purpose text font---the spacing is not optimized for that, being designed instead for printing computer code, where each letter should be distinct and text ligatures are banished. It seems to work well with the {\tt listings} package, designed for exactly that purpose. Unusually for such a font, it is not monospaced, though perhaps this is no longer the issue it was in the days of FORTRAN. 

Nonetheless, if you don't object to a typewritten appearance, I think the font doesn't really look as bad as you might think it should. (This document uses it as its main text font.)

Both opentype and type1 fonts are provided, along with LaTeX support files. Most characters in the T1 encoding are provided, except for f-ligatures and the Sami characters Eng, eng. There are no kerning tables, and the spacing is not uniform so that even without hyphenation, lines spread out quite well.

\textbf{Usage with fontspec:}

The package provides algolrevived.fontspec, with contents:

\begin{verbatim}
\defaultfontfeatures[algolrevived]
{
	Extension = .otf,
	UprightFont = AlgolRevived ,
	BoldFont = AlgolRevived-Medium ,
	BoldItalicFont = AlgolRevived-MediumSlanted ,
	ItalicFont = AlgolRevived-Slanted ,
}
\end{verbatim}
which allows you to set up your preamble using simply
\begin{verbatim}
...
\usepackage{fontspec}
\setmainfont{algolrevived} % for use as main font
%\setmonofont{algolrevived} % for use as typewriter font only
...
\end{verbatim}
\newpage

\textbf{Usage with LaTeX}

The package offers OT1, LY1, T1 and TS1 encodings, and sets T1 as the default. To change to LY1, you will need something like

\begin{verbatim}
\usepackage{algolrevived}
%\usepackage[tt]{algolrevived} for just typewriter
\usepackage[LY1]{fontenc}
\end{verbatim}
The package has two rather different modes, one intended for use as main text and the other intended for use as a typewriter font defining \verb|\ttdefault| for use with Typewriter Text and Verbatim text environments such as \verb|\texttt| and \verb|\verb|. In the example above, {\tt algolrevived} becomes the default Roman font, with the following features, details of which are described in more detail later in this document.
\begin{itemize}
\item
Hyphenation is enabled but may be turned off with the option {\tt nohyphens};
\item
the asterisk character renders as the high asterisk *. The low asterisk is available using  \verb|\textasteriskcentered|, like \textasteriskcentered;
\item lining figures do not have a slashed zero:  0123456789;
\item
there is a {\tt compoundwordmark} in the T1 encoding, and there are {\tt capitalwordmark} and {\tt ascenderwordmark} in the TS1 encoding;
\item
the TS1 encoding is quite complete and has many additional symbols that are not part of the standard TS1 encoding;
\item oldstyle figures are available---they may be made the default using the option {\tt osf} or {\tt oldstyle}, or from the TS1 encoding;
\item inferior and superior figures are provided along with a \verb|\textfrac| macro;
\item a \verb|\textcircled| macro is provided.
\end{itemize}

\textbf{Usage as Typewriter Font}

The option {\tt tt} to {\tt algolrevived} changes the mode to {\tt Typewriter} with different behavior.
\begin{itemize}
\item
Hyphenation is suppressed;
\item
the asterisk is low in this case, with the same effect as \verb|\textasteriskcentered|.
\item \verb|\ttdefault| is set to {\tt AlgolRevived-TLF-TT}---in particular, oldstyle figure settings are ignored;
\item the default lining figure zero is slashed---you may change this using the option {\tt nozeroslash};
\item the figure style macros such as \verb|\textosf| and the \verb|\textcircled| remain available.
\end{itemize}

\newpage
\textbf{AlgolRevived-tlf-t1.tfm:}
\fonttable{AlgolRevived-tlf-t1--base}

\newpage
\textbf{Package options and macros:}\\
 The option \texttt{scaled=.95} or \texttt{scale=.95} renders at 95\% of the default size. Option \texttt{tt} species typewriter. The macros \verb|{\sufigures 9}| (same effect as \verb|\textsu{9}|) render the figure as a superscript, \textsu{9}, and similarly with \verb|\infigures|, \verb|\textinf| for inferior figures. (Note that the usual macro \verb|\textin| is being used for the ``belongs to''  glyph, \textin, added as a non-standard part of {\tt textcomp}.)

The macro \verb|\textcircled| that may be used to construct a circled version of a single letter or numeral using \verb|\textbigcircle| for the encircling glyph. The argument is always constructed from uppercased letters and numerals, so, in effect, you can only construct circled uppercase: \verb|\textcircled{M}| and \verb|\textcircled{m}| have the same effect, namely~\textcircled{M}. Circled numerals are also available: \verb|\textcircled{0}...| produces \textcircled{0}\dots\textcircled{9}.
\begin{itemize}
\item
\verb|\textlf{}| and \verb|{\lfstyle }| give lining figures, as do 
\verb|\texttlf{}| and \verb|{\tlfstyle }|;
\item
\verb|\textosf{}| and \verb|{\osfstyle }| give proportional oldstyle figures as do 
\verb|\texttosf{}| and \verb|{\tosfstyle }|;
\item \verb|\textfrac{3}{4}| uses superior and inferior figures to make the fraction \textfrac{3}{4}, and an optional argument is available for the whole number part: \verb|\textfrac[2]{31}{32}| renders as \textfrac[2]{31}{32}. The spacing may be tinkered with by redefining the macro \verb|\textfrac| and by setting the two options {\tt foresolidus} and {\tt aftsolidus} which modify the spacing around the fraction solidus.
\end{itemize}

%2\textfrac{1}{2}\textfrac{2}{3} \textfrac{3}{4} \textfrac{4}{5} \textfrac{5}{6} \textfrac{6}{7} \textfrac{7}{8} \textfrac{8}{9} \textfrac{9}{0} \textfrac{0}{1} \$\textdollar\textdollaroldstyle

The sty file requires textcomp so there is no need to load it separately. Textcomp adds the following glyphs. (The mathematical symbols in the otherwise vacant slots in positions 192 and up were mostly borrowed from the STIX math fonts, which use the same SIL OFL as this package. The names below were in those cases are the same as the STIX names, prefixed by "text".)
\newpage

\textbf{AlgolRevived-ts1.tfm:}
\fonttable{AlgolRevived-ts1}
\newpage

\textbf{List of LaTeX macros to access the TS1 symbols in text mode:}

\begin{verbatim}
  0 \capitalgrave
  1 \capitalacute
  2 \capitalcircumflex
  3 \capitaltilde
  4 \capitaldieresis
  5 \capitalhungarumlaut
  6 \capitalring
  7 \capitalcaron
  8 \capitalbreve
  9 \capitalmacron
 10 \capitaldotaccent
 11 \capitalcedilla
 12 \capitalogonek
 13 \textquotestraightbase
 18 \textquotestraightdblbase
 21 \texttwelveudash
 22 \textthreequartersemdash
 23 \textcapitalcompwordmark
 24 \textleftarrow
 25 \textrightarrow
 26 \t % tie accent, skewed right
 27 \capitaltie % skewed right
 28 \newtie % tie accent centered
 29 \capitalnewtie % ditto
 31 \textascendercompwordmark
 32 \textblank
 36 \textdollar
 39 \textquotesingle
 42 \textasteriskcentered
 45 \textdblhyphen
 47 \textfractionsolidus
 48 \textzerooldstyle
 49 \textoneoldstyle
 50 \texttwooldstyle
 49 \textthreeoldstyle
 50 \textfouroldstyle
 51 \textfiveoldstyle
 52 \textsixoldstyle
 53 \textsevenoldstyle
 54 \texteightoldstyle
 55 \textnineoldstyle
 60 \textlangle
 61 \textminus
 62 \textrangle
 77 \textmho
 79 \textbigcircle
 87 \textohm
 91 \textlbrackdbl
 93 \textrbrackdbl
 94 \textuparrow
 95 \textdownarrow
 96 \textasciigrave
 96 \textasciigrave
 98 \textborn
 99 \textdivorced
100 \textdied
108 \textleaf
109 \textmarried
110 \textmusicalnote
126 \texttildelow
127 \textdblhyphenchar
128 \textasciibreve
129 \textasciicaron
130 \textacutedbl
131 \textgravedbl
132 \textdagger
133 \textdaggerdbl
134 \textbardbl
135 \textperthousand
136 \textbullet
137 \textcelsius
138 \textdollaroldstyle
139 \textcentoldstyle
140 \textflorin
141 \textcolonmonetary
142 \textwon
143 \textnaira
144 \textguarani
145 \textpeso
146 \textlira
147 \textrecipe
148 \textinterrobang
149 \textinterrobangdown
150 \textdong
151 \texttrademark
152 \textpertenthousand
153 \textpilcrow
154 \textbaht
155 \textnumero
156 \textdiscount
157 \textestimated
158 \textopenbullet
159 \textservicemark
160 \textlquill
161 \textrquill
162 \textcent
163 \textsterling
164 \textcurrency
165 \textyen
166 \textbrokenbar
167 \textsection
168 \textasciidieresis
169 \textcopyright
170 \textordfeminine
171 \textcopyleft
172 \textlnot
173 \textcircledP
174 \textregistered
175 \textasciimacron
176 \textdegree
177 \textpm
178 \texttwosuperior
179 \textthreesuperior
181 \textmu
182 \textparagraph
183 \textperiodcentered
184 \textreferencemark
185 \textonesuperior
186 \textordmasculine
187 \textsurd
188 \textonequarter
189 \textonehalf
190 \textthreequarters
191 \texteuro
192 \textprime
193 \textdprime
196 \textleftrightarrow
197 \textupdownarrow
198 \textLeftarrow
199 \textUparrow
200 \textRightarrow
201 \textDownarrow
202 \textLeftrightarrow
203 \textUpdownarrow
204 \textforall
205 \textcomplement
206 \textpartial
207 \textexists
208 \textnexists
209 \textvarnothing
210 \textincrement
211 \textnabla
212 \textin
213 \textnotin
214 \texttimes
215 \textsmallin
216 \textni
217 \textnni
218 \textsmallni
219 \textsmallsetminus
220 \textlargebullet
221 \textland
222 \textlor
224 \textcap
225 \textcup
226 \textcoloneq
227 \texteqcolon
228 \textneq
229 \textequiv
230 \textneqiv
231 \textleq
232 \textgeq
233 \textsubset
234 \textsupset
235 \textnsubset
236 \textnsupset
237 \textsubseteq
238 \textsupseteq
239 \textnsubseteq
240 \textnsupseteq
241 \textsqsubset
242 \textsqsubset
243 \textsqsupset
244 \textsqsubseteq
245 \textsqcap
246 \textdiv
247 \textsqcup
\end{verbatim}

For example, typing in \verb|A\textcoloneq B| results in A\textcoloneq B.

\textbf{Package Details}

The package is laid out a bit differently from the typical one, and it may help some to know a few details.

There are six {\tt fd} files corresponding to each of the text encodings {\tt OT1, T1, LY1}, and two for {\tt TS1}, the text companion encoding. These are:
\begin{verbatim}
LY1AlgolRevived-Inf.fd
LY1AlgolRevived-Sup.fd
LY1AlgolRevived-OsF.fd
LY1AlgolRevived-TLF.fd
LY1AlgolRevived-OsF-TT.fd
LY1AlgolRevived-TLF-TT.fd
OT1AlgolRevived-Inf.fd
OT1AlgolRevived-Sup.fd
OT1AlgolRevived-OsF.fd
OT1AlgolRevived-TLF.fd
OT1AlgolRevived-OsF-TT.fd
OT1AlgolRevived-TLF-TT.fd
T1AlgolRevived-Inf.fd
T1AlgolRevived-Sup.fd
T1AlgolRevived-OsF.fd
T1AlgolRevived-TLF.fd
T1AlgolRevived-OsF-TT.fd
T1AlgolRevived-TLF-TT.fd
TS1AlgolRevived-OsF.fd
TS1AlgolRevived-TLF.fd 
\end{verbatim} 

The last two have essentially identical effect, each pointing to {\tt tfm} files of the form {\tt AlgolRevived-ts1.tfm}, {\tt AlgolRevived-Medium-ts1.tfm} and so on. These were all crafted with {\tt afm2tfm} because {\tt otftotfm} had repeated problems with distinguishing {\tt dollar} from {\tt dollaroldstyle} and {\tt cent} from {\tt centoldstyle}. To get the compond word marks right involved modifying the afm files after each edit to make their heights correct, then running {\tt afm2tfm} with the {\tt -v} option to get a virtual font which would register those heights correctly.

Each of the text font encodings have essentially the same structure, so I'll  limit my remarks mainly to {\tt T1}. All except the {\tt -TT} were generated by {\tt autoinst}.

The {\tt -Inf} and {\tt -Sup} are for the inferior and superior letters respectively, while {\tt -TLF} and {\tt OsF} are for the two normal figure styles you might choose from in text mode. The {\tt -TT} case is more interesting, having been generated by {\tt afm2tfm} because {\tt autoinst} and {\tt otftotfm} don't give you much help with making the changes needed for  a Typewriter font as distinct from a normal text font. For {\tt T1} and {\tt LY1} encoded fonts, the main difference, aside from lacking f-ligatures and the like, is that the asterisk is low in Typewriter and high in normal text. However, for {\tt OT1}, there are sixteen additional changes required in order to match the typewriter encoding used in {\tt cmtt}.

Each of the {\tt -TT fd} files points to {\tt tfm} files with names like
{\tt AlgolRevived-t1tt.tfm} with no figure style mentioned, because the figure style there is always {\tt TLF}. That is, {\tt T1AlgolRevived-OsF-TT.fd} and 
{\tt T1AlgolRevived-TLF-TT.fd} have essentially identical contents, but both are required to be there to conform to the LaTeX font selection scheme. There is in all cases a 0 or 1 in the name, signifying the slashed zero (1) or the unslashed 0 (0).

\end{document}