This is the README for the linguisticspro package, version
2022-10-28.

This package provides LaTeX, pdfLaTeX, XeLaTeX and LuaLaTeX
support for the LinguisticsPro family of fonts.

This family is derived from the Utopia Nova font family,
by Andreas Nolda.

To install this package on a TDS-compliant
TeX system download the file
"tex-archive"/install/fonts/linguisticspro.tds.zip, where the
preferred URL for "tex-archive" is http://mirror.ctan.org.
Unzip the archive at the root of an appropriate texmf tree,
likely a personal or local tree. If necessary, update the
file-name database (e.g., texhash). Update the font-map
files by enabling the Map file LinguisticsPro.map.

To use, add

\usepackage{linguisticspro}

to the preamble of your document. This makes LinguisticsPro
the default (serif) family. 

The default figure style is oldstyle but lining figures are
available with the lf (or lining) option and locally with
the \linguisticsproLF command.

LuaLaTeX and xeLaTeX users who might prefer type1 fonts or
who wish to avoid fontspec may use the type1 option.

Options scaled=<number> or scale=<number> may be used to adjust
fontsizes.

Font encodings supported are OT1, T1, TS1, LY1, T2A/B/C,
T3, TS3 and LGR. The commands \linguisticsprolgr and
\linguisticsprot3 select LGR and T3-encoded type1 fonts,
respectively.

The original fonts are available at
https://localfonts.eu/shop/cyrillic-script/serbian/serbian-cyrillic-serif/linguistics-pro/
and are licensed under the SIL Open Font License, (version 1.1); 
the text may be found in the doc directory. The support files
and type1 fonts were created using autoinst and otftotfm,
and are licensed under the terms of the LaTeX Project Public
License. The maintainer of this package is Bob Tennent 
(rdt at cs.queensu.ca)
