$Id: README 60 2022-10-05 22:42:54Z karl $
(This file is public domain.)

This file describes the Eplain (expanded or extended plain, take your
pick) distribution.  The current version number is at the beginning and
end of eplain.tex, among other places.

See INSTALL for installation and basic usage hints.

eplain.tex is the macro file.  This file is generated (by the Unix shell
scripts in the util/ directory) from xeplain.tex, btxmac.tex, arrow.tex,
and other files.  Unless you are hacking the macros, you should have no
need to regenerate the file; just use the one that's provided.  If you
do regenerate it, you will need a date program which understands the %B
format (GNU date does).

The doc subdirectory is documentation in various formats.

The files in the test subdirectory are of course for testing of various
features, but also provide some examples of usage.

The util subdirectory contains scripts to help with "see" and "see also"
index entries, and making index entries with hyperlinks.

The original Eplain macros and most of the manual are in the public
domain.  Other pieces are covered by other licenses;
see the file headers for specifics.  It is entirely free software.

Eplain was originally developed for the book TeX for the Impatient,
published by Addison-Wesley in 1990. That book is now freely available,
from https://ctan.org/pkg/impatient, along with several translations.

Mailing list: https://lists.tug.org/tex-eplain (tex-eplain@tug.org)
The Eplain home page is https://tug.org/eplain.

P.S. Alternatives to Eplain:
1) If you just want to load the LaTeX color or graphics package,
the LaTeX team's miniltx.tex suffices:
  \input miniltx.tex
  \input graphicx.tex

2) If you're interested in a fully-fledged format that shares much of
Eplain's (and plain.tex's) philosophy, check out OpTeX:
  https://ctan.org/pkg/optex

