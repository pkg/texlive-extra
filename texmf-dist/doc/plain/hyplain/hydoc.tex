%%% This is part of the hyplain package by Enrico Gregorio
%%% You are allowed to do anything you want with this as long
%%% as you cite the source and change name to the file

\hoffset=.46truecm
\hsize=15truecm
\advance\voffset by -2.5\baselineskip
\advance\vsize by 5\baselineskip

\font\tensf=cmss10

\def\manyhyphens{%
\hyphenpenalty=-1000
\pretolerance=-1
\tolerance=1000
\doublehyphendemerits=-100000
\finalhyphendemerits=-100000
}

\def\LaTeX{L\kern-.36em\setbox0=\hbox{T}\vbox to\ht0
  {\hbox{\sevenrm A}\vss}\kern-.15em\TeX}
\def\LaTeXe{\LaTeX$2_{\textstyle\varepsilon}$}

\def\cs#1{\char`\\#1}
\def\ar#1{\char`\{#1\char`\}}
\def\showcommand#1{\par\nobreak\medskip\begingroup\tt#1\endgroup\par\medskip
  \noindent\ignorespaces}

\addto{it}{IT}{\frenchspacing}{\nonfrenchspacing}

\begingroup\manyhyphens

\centerline{\bf How to use hyplain --- version 1.0}

\medskip

Many (well, at least more than twelve) years ago, \LaTeX{} was not as usable as it is today.
So we were forced to use AMS-\TeX{} for typesetting mathematical papers.

At the very beginning there were two plain formats to use, the original one and the one
(called plainit) for Italian hyphenation. With the advent of \TeX3 we could use only one and
define some switch for selecting the language.

I found a package called HyMaster that could be used for this purpose; it was developed by
Daniel Flipo and Laurent Siebenmann for French. Since Italian typesetting rules do not
require the contortions of the French, it was simple to adapt only one file for canning a
format file.

With the advent of \LaTeXe, everything was forgotten. Until recently, when a user in a
discussion forum showed some code that I recognized as my modifications to HyMaster! He
proposed some procedure to build the format which was very complicated (you see, at that
time we were using Oz\TeX; nowadays, te\TeX{} or MiK\TeX{} require different actions). So I
tried to rethink to the business.

\IT

\bigskip

\hrule

\medskip

\centerline{\bf Come usare hyplain}

\medskip

Parecchi (be', almeno dodici) anni fa, \LaTeX{} non era cos\`{\i} facile da usare come \`e
oggi. Per comporre testi matematici eravamo quindi costretti a usare AMS-\TeX{}.

All'inizio c'erano due formati plain da usare, l'originale e quello (chiamato plainit) per
la sillabazione italiana. Con l'avvento di \TeX3, si pot\'e usare un solo formato con la
possibilit\`a di cambiare le regole di sillabazione con un comando.

Trovai un pacchetto chiamato HyMaster che poteva essere usato per questo scopo; era stato
sviluppato da Daniel Flipo e Laurent Siebenmann per il francese. Siccome la tipografia
italiana non richiede le contorsioni di quella francese, fu semplice adattare un solo {\it
file\/} per costruire un formato.

Con l'avvento di \LaTeXe, tutto fu dimenticato. Finch\'e, recentemente, un utente di un
gruppo di discussione esib\`{\i} del codice che riconobbi come le mie modifiche a HyMaster!
Proponeva una procedura piuttosto complicata per costruire il formato (a quel tempo usavamo
Oz\TeX; ora te\TeX{} e MiK\TeX{} richiedono azioni diverse). Perci\`o ho provato a ripensare
alla faccenda.

\medskip

\hrule

\bigskip

\endgroup

\US

As you can see, hyphenation in US~English and in Italian behave properly. The text above has
been typeset with more hyphens than usual.

\medskip

The package consists of three files: {\tt hyplain.tex}, {\tt hyrules.tex} and {\tt
hylang.tex}. Only the third one needs to be modified by the users who want to define
languages to employ.

The basic definition of a language is given by a command
\showcommand{\cs{definelanguage}\ar{xx}\ar{YY}\ar{filename}} where {\tt xx} is the ISO
abbreviation for the language ({\tt en} for English, {\tt it} for Italian, {\tt de} for
German), {\tt YY} is the ISO abbreviation for the nation and {\tt filename} is the name of
the hyphenation pattern file.  Thus
\showcommand{\cs{definelanguage}\ar{it}\ar{IT}\ar{ithyph}} defines Italian.

There is also {\tt\cs{definedialect}} for defining a language which shares hyphenation
patterns with another; for example
\showcommand{\cs{definedialect}\ar{de}\ar{AT}\ar{de}\ar{DE}} defines ``German for Austria'',
assuming that \showcommand{\cs{definelanguage}\ar{de}\ar{DE}\ar{dehypht}} has already been
given.

After a {\tt\cs{definelanguage}} command, {\tt\cs{refinelanguage}} should be given; it has
four arguments

\nobreak\smallskip
\item{\tt\#1} the two-letter language abbreviation,

\item{\tt\#2} the two-letter nation abbreviation,

\item{\tt\#3} the code to be executed when the language comes into action,

\item{\tt\#4} the code to be executed when another language is called.
\smallskip

This command should always contain in the third argument the left and right hyphenation
minima: for example, we set
\showcommand{\cs{refinelanguage}\ar{en}\ar{US}%
 \ar{\cs{hyphenmins}\ar{2}\ar{3}}\ar{}}
in order to enforce the usual conventions for American English; as another example, we set
\showcommand{\cs{refinelanguage}\ar{it}\ar{IT}%
  \ar{\cs{hyphenmins}\ar{2}\ar{2}\cs{lccode}`\char`\\'=`\char`\\'}%
  \ar{\cs{lccode}`\char`\\'=0 }}
because the lower case code of the right quote should not be zero for correct Italian
hyphenation. We need to ``undo'' the effect since we don't know whether the user calls the
language switch inside a group or not. The command
\showcommand{\cs{hyphenmins}\ar{x}\ar{y}}
is an abbreviation for {\tt\cs{lefthyphenmin}=x\cs{righthyphenmin}=y} and it is not
necessary to ``undo'' it.

The commands {\tt\cs{definedialect}} and {\tt\cs{refinedialect}} (analog to
{\tt\cs{refinelanguage}}) can be given also in the user's document where, of course, it is
too late to define new languages with their hyphenation patterns. Anyway, users can simulate
new language definitions by defining a dialect to language {\tt\cs{zz\_ZZ}}, which is a
fallback without hyphenation tables.

If one wants to extend the functionalities of a language, there is also the command
\showcommand{\cs{addto}\ar{xx}\ar{YY}\ar{do}\ar{undo}}
which can be used, for example, in the following way
\showcommand{\cs{addto}\ar{it}\ar{IT}%
  \ar{\cs{frenchspacing}}\ar{\cs{nonfrenchspacing}}}
in order to enforce French spacing for Italian (in Italy).

Finally, there is a command to choose a defined language (or even an undefined one, try it)
\showcommand{\cs{selectlanguage}\ar{xx}\ar{YY}}
which selects language {\tt xx} for {\tt YY}. Users can add aliases for this with
\showcommand{\cs{addalias}\ar{\cs{command}}\ar{xx}\ar{YY}}
(see {\tt hylang.tex} for examples). After saying
\showcommand{\cs{addalias}\ar{\cs{IT}}\ar{it}\ar{IT}}
you can simply say {\tt\cs{IT}} to switch to Italian. This alias is actually predefined in
the present version of {\tt hylang.tex} as well as {\tt\cs{US}} for US~English.

\bigskip

\centerline{\bf Installation}

\nobreak\medskip

Put the three files in some directory and process with
\showcommand{tex -ini hyplain}
and after this put the produced format in some suitable place of the \TeX{} tree. After this
\showcommand{tex -fmt hyplain filename}
will typeset {\tt filename.tex} using the new format. This is for a *nix system and a
{\tensf Web2C} distribution like te\TeX{} or \TeX{}Live; for MiK\TeX{} find the way, I don't
use it. You can use also {\tt pdftex} and you can use the newly created {\tt hyplain.fmt} to
build AMS-\TeX{} over it. For {\tt pdftex} you should use
\showcommand{pdftex -ini hypdfplain.ini}
and then
\showcommand{pdftex -fmt hypdfplain filename}
If you want to access the extended features of $\varepsilon$-\TeX{}, then do
\showcommand{pdftex -ini '*hypdfplain.ini'}

\vfill

\rightline{Enrico Gregorio}
\rightline{October 17, 2006}
\bye
