\input stmary
\def\br{\hfil\break}
\def\cmd#1{{\tt\string#1}}
\font\headfont=cmr12
\font\headsf=cmss12
\hsize 6.5in \vsize 8.9in \hoffset 0pt \voffset 0pt

\centerline{\headsf
    plstmary: \headfont St.\ Mary's Road Font for Plain \TeX}
\centerline{Version: \plstmaryversion}
\medskip
\centerline{Dan Luecking}
\centerline{\tt luecking $\inplus$ uark $\boxdot$ edu}
\bigskip

\noindent The plstmary package provides plain TeX support for the St.\
Mary's Road symbol font. After inputting the macro file {\tt
stmary.tex}, all the symbols in the tables below become defined. The two
commands {\cmd\oast} and {\cmd\ocircle} require the AMS symbol font {\tt
msam}, so {\tt stmary.tex} automatically inputs {\tt amssym.tex}. (These
symbols are the same as {\cmd\circledast} and {\cmd\circledcirc},
defined in {\tt amssym.tex}. The point of defining them is to have
matching `{\cmd\o}\dots' and `{\cmd\varo}\dots' pairs for all circles.)

All the command names are the same as those in the LaTeX package {\tt
stmaryrd.sty}. More details on the symbols and their usage may be found
in the documentation of the LaTeX stmaryrd package.

Some command names defined in the {\tt stmary.tex} overwrite command
names in the {\tt amssym.tex}. The original definitions are stored in
alternative commands. The affected commands are {\cmd\bigtriangledown},
{\cmd\bigtriangleup} and {\cmd\boxdot}. The AMS versions are obtained
with {\tt \string\amsbigtriangledown}, {\cmd\amsbigtriangleup} and
{\cmd\amsboxdot}.

The plstmary package allows minimal size changing. By default it
produces symbols intended for ten point documents. It also supports
eight, nine, eleven, and twelve point sizes. Use one of the following
commands to change size:\hfil\break
\indent{\cmd\stmaryrdeightpoint},\quad
{\tt\ \string\stmaryrdninepoint},\br
\indent{\cmd\stmaryrdelevenpoint},\quad
{\cmd\stmaryrdtwelvepoint},\br
\indent{\cmd\stmaryrdtenpoint} \ (to restore the default if it has been
changed).\br
These commands affect only the St.\ Mary Road symbols and, for
convenience, the AMS fonts. They have no effect on the body text font
nor on any other mathematical fonts. For changing the sizes of standard
plain TeX fonts, you must either load a package for that purpose, of
define your own macros.

In order to get actual error-free output from these commands, one needs
to have the St.\ Mary Road fonts and the AMS fonts installed, but those
are available in all the major free TeX distributions: TeX Live, MiKTeX
and MacTeX.

\bigskip
\leftline{\bf Legal stuff}

\medskip
\noindent The St.\ Mary's Road metafont code is copyright \copyright
1991-1994 by Jeremy Gibbons and Alan Jeffrey. The fonts in PS type1
format (i.e., {\tt .afm} and {\tt .pfb} files) are copyright \copyright
1998 by Taco Hoekwater. All rights are reserved to the respective
authors.

This plstmary distribution is placed in the public domain. Do with it as
you wish.

While every effort has been made to make plstmary useful, it comes with
no warranty, expressed or implied.

\bigskip
\leftline{\bf Acknowledgements}
I am indebted to the creators of the fonts and of the stmaryrd
LaTeX package. The file {\tt stmary.tex} is largely a reduction to plain
TeX macros and TeX primitives of the code in {\tt stmaryrd.sty}.

\bigskip
\leftline{\bf Caveats}

\medskip
\noindent Some features of the LaTeX package have not been implemented
in plstmary. For example, normally the symbol obtained with
{\cmd\oplus} has thin strokes while {\cmd\varoplus} has
thicker strokes. The LaTeX package can be loaded with the {\tt
heavycircles} option to reverse this for all the circled symbols. This
is not implemented in plstmary. Also, the LaTeX package has the {\tt
only} option, allowing selected commands to be defined and no others.
This is not implemented in plstmary.

Also, I have made the following corrections (or possibly mistakes).
(1)~In {\tt stmaryrd.sty}, the commands {\cmd\lbag} and
{\cmd\rbag} are defined as binary operations, but the
documentation says they are delimiters. In {\tt stmary.tex}, I have
defined them as delimiters. (2)~In {\tt stmaryrd.sty}, the commands
{\cmd\binampersand} and {\cmd\bindnasrepma} are defined as
delimiters, but their names clearly imply that they are intended to be
binary operations (and the stmaryrd package documentation does not list
them among the delimiters). In {\tt stmary.tex}, I have defined them as
binary operations. I have no idea if I have chosen the correct usages
for these four commands, and I would be happy to change them back if
someone would let me know.

\medskip
\bigskip
\filbreak
\halign{%
\strut\quad\tt#\hfil\quad
&             #\hfil\qquad
&          \tt#\hfil\quad
&             #\hfil\enspace\cr
\bf Binary operations\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
 \string\Yup
&$\Yup$
&\string\Ydown
&$\Ydown$\cr
 \string\Yleft
&$\Yleft$
&\string\Yright
&$\Yright$\cr
 \string\binampersand
&$\binampersand$
&\string\bindnasrepma
&$\bindnasrepma$\cr
 \string\varcurlyvee
&$\varcurlyvee$
&\string\varcurlywedge
&$\varcurlywedge$\cr
 \string\minuso
&$\minuso$
&\string\baro
&$\baro$\cr
 \string\sslash
&$\sslash$
&\string\bbslash
&$\bbslash$\cr
 \string\moo
&$\moo$
&\string\merge
&$\merge$\cr
 \string\nplus
&$\nplus$
&\string\boxbar
&$\boxbar$\cr
 \string\boxdot
&$\boxdot$
&\string\amsboxdot
&$\amsboxdot$\cr
 \string\boxslash
&$\boxslash$
&\string\boxbslash
&$\boxbslash$\cr
 \string\boxcircle
&$\boxcircle$
&\string\boxbox
&$\boxbox$\cr
 \string\boxempty
&$\boxempty$
&\string\boxast
&$\boxast$\cr
 \string\vartimes
&$\vartimes$
&\string\fatsemi
&$\fatsemi$\cr
 \string\fatslash
&$\fatslash$
&\string\fatbslash
&$\fatbslash$\cr
 \string\varbigcirc
&$\varbigcirc$
&\string\leftslice
&$\leftslice$\cr
 \string\rightslice
&$\rightslice$
&\string\varotimes
&$\varotimes$\cr
 \string\oast
&$\oast$
&\string\varoast
&$\varoast$\cr
 \string\obar
&$\obar$
&\string\varobar
&$\varobar$\cr
 \string\obslash
&$\obslash$
&\string\varobslash
&$\varobslash$\cr
 \string\ocircle
&$\ocircle$
&\string\varocircle
&$\varocircle$\cr
 \string\varoplus
&$\varoplus$
&\string\varominus
&$\varominus$\cr
 \string\olessthan
&$\olessthan$
&\string\varolessthan
&$\varolessthan$\cr
 \string\ogreaterthan
&$\ogreaterthan$
&\string\varogreaterthan
&$\varogreaterthan$\cr
 \string\ovee
&$\ovee$
&\string\varovee
&$\varovee$\cr
 \string\owedge
&$\owedge$
&\string\varowedge
&$\varowedge$\cr
 \string\varoslash
&$\varoslash$
&\string\varodot
&$\varodot$\cr
 \string\oblong
&$\oblong$
&\string\talloblong
&$\talloblong$\cr
 \string\interleave
&$\interleave$
&&\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\filbreak
\halign{%
\strut\quad  \tt#\hfil\quad & #\hfil\qquad
           & \tt#\hfil\quad & #\hfil\enspace\cr
\bf Relations\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
%
 \string\inplus
&$\inplus$
&\string\niplus
&$\niplus$\cr
 \string\subsetplus
&$\subsetplus$
&\string\supsetplus
&$\supsetplus$\cr
 \string\subsetpluseq
&$\subsetpluseq$
&\string\supsetpluseq
&$\supsetpluseq$\cr
 \string\shortuparrow
&$\shortuparrow$
&\string\shortdownarrow
&$\shortdownarrow$\cr
 \string\nnwarrow
&$\nnwarrow$
&\string\nnearrow
&$\nnearrow$\cr
 \string\sswarrow
&$\sswarrow$
&\string\ssearrow
&$\ssearrow$\cr
 \string\curlywedgeuparrow
&$\curlywedgeuparrow$
&\string\curlywedgedownarrow
&$\curlywedgedownarrow$\cr
 \string\curlyveedownarrow
&$\curlyveedownarrow$
&\string\curlyveeuparrow
&$\curlyveeuparrow$\cr
 \string\trianglelefteqslant
&$\trianglelefteqslant$
&\string\trianglerighteqslant
&$\trianglerighteqslant$\cr
 \string\ntrianglelefteqslant
&$\ntrianglelefteqslant$
&\string\ntrianglerighteqslant
&$\ntrianglerighteqslant$\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\filbreak
\halign{%
\strut\quad  \tt#\hfil\quad & #\hfil\qquad
           & \tt#\hfil\quad & #\hfil\enspace\cr
\bf Arrows\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
%
 \string\leftrightarroweq
&$\leftrightarroweq$
&\string\shortrightarrow
&$\shortrightarrow$\cr
 \string\shortleftarrow
&$\shortleftarrow$
&\string\rightarrowtriangle
&$\rightarrowtriangle$\cr
 \string\leftarrowtriangle
&$\leftarrowtriangle$
&\string\leftrightarrowtriangle
&$\leftrightarrowtriangle$\cr
\string\Mapsto
&$\Mapsto$
&\string\Longmapsto
&$\Longmapsto$\cr
\string\mapsfrom
&$\mapsfrom$
&\string\Mapsfrom
&$\Mapsfrom$\cr
\string\longmapsfrom
&$\longmapsfrom$
&\string\Longmapsfrom
&$\Longmapsfrom$\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\noindent The commands {\cmd\arrownot} and {\cmd\Arrownot}
can be placed in front of horizontal arrows to negate them:
{\cmd\arrownot\cmd\rightarrow} produces
        $\arrownot\rightarrow$ and
        {\cmd\Arrownot\cmd\Rightarrow} produces
        $\Arrownot\Rightarrow$.
For long arrows, there are {\cmd\longarrownot}{\cmd\longrightarrow}:
$\longarrownot\longrightarrow$ \ and {\cmd\Longarrownot}{\cmd\Longrightarrow}:
                                $\Longarrownot\Longrightarrow$.
\medskip
\bigskip
\filbreak
\halign{%
\strut\quad \tt#\hfil\quad & #\hfil\qquad
           &\tt#\hfil\quad & #\hfil\enspace\cr
\bf Big operators\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
 \string\bigtriangledown
&$\bigtriangledown$
&\string\bigtriangleup
&$\bigtriangleup$\cr
 \string\amsbigtriangledown
&$\amsbigtriangledown$
&\string\amsbigtriangleup
&$\amsbigtriangleup$\cr
 \string\bigcurlyvee
&$\bigcurlyvee$
&\string\bigcurlywedge
&$\bigcurlywedge$\cr
 \string\bigsqcap
&$\bigsqcap$
&\string\bigbox
&$\bigbox$\cr
 \string\bigparallel
&$\bigparallel$
&\string\biginterleave
&$\biginterleave$\cr
 \string\bignplus
&$\bignplus$\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\filbreak
\halign{%
\strut\quad \tt#\hfil\quad & #\hfil\qquad
           &\tt#\hfil\quad & #\hfil\enspace\cr
\bf Delimiters\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
 \string\lbag
&$\lbag$
&\string\rbag
&$\rbag$\cr
 \string\Lbag
&$\Lbag$
&\string\Rbag
&$\Rbag$\cr
 \string\llparenthesis
&$\llparenthesis$
&\string\rrparenthesis
&$\rrparenthesis$\cr
 \string\llfloor
&$\llfloor$
&\string\rrfloor
&$\rrfloor$\cr
 \string\llceil
&$\llceil$
&\string\rrceil
&$\rrceil$\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\filbreak
\halign{%
\strut\quad \tt#\hfil\quad & #\hfil\qquad
           &\tt#\hfil\quad & #\hfil\enspace\cr
\bf Expandable delimiters\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
 \string\llbracket
&$\llbracket$
&\string\rrbracket
&$\rrbracket$\cr
\noalign{\smallskip\hrule}
}

\vskip-\baselineskip
\noindent
Examples of expanded double brackets:\quad
$
\left\llbracket \matrix{a\cr} \right\rrbracket\quad
\left\llbracket \matrix{a\cr b\cr} \right\rrbracket\quad
\left\llbracket \matrix{a\cr b\cr c\cr} \right\rrbracket\quad
\left\llbracket \matrix{a\cr b\cr c\cr d\cr} \right\rrbracket\quad
\left\llbracket \matrix{a\cr b\cr c\cr d\cr e\cr} \right\rrbracket\quad
\left\llbracket \matrix{a\cr b\cr c\cr d\cr e\cr f\cr} \right\rrbracket
$

\vskip-\baselineskip
\filbreak
\halign{%
\strut\quad \tt#\hfil\quad & #\hfil\qquad
           &\tt#\hfil\quad & #\hfil\enspace\cr
\bf Miscellaneous\hidewidth\cr
\noalign{\smallskip\hrule\smallskip}
 \string\lightning
&$\lightning$
&\string\varcopyright
&$\varcopyright$\cr
\noalign{\smallskip\hrule}
}

\medskip
\bigskip
\filbreak
\noindent
\leftline{\bf Size changing}

\medskip\noindent
{8pt: \stmaryrdeightpoint $\lbag x \inplus a \rbag \amsboxdot b$.\quad
 9pt: \stmaryrdninepoint$\lbag x \inplus a \rbag \amsboxdot b$.\quad
10pt: \stmaryrdtenpoint$\lbag x \inplus a \rbag \amsboxdot b$.\quad
11pt: \stmaryrdelevenpoint$\lbag x \inplus a \rbag \amsboxdot b$.\quad
12pt: \stmaryrdtwelvepoint$\lbag x \inplus a \rbag \amsboxdot b$.}

\bigskip
\leftline{\bf Version history}

\medskip
\noindent
    0.5 --- 2013/05/09\quad   Autoloading AMS symbols. Size changing for
                              AMS, saving AMS definitions.\br
    0.4 --- 2013/04/16\quad   Corrected \cmd\lbag, \cmd\Lbag,
                              \cmd\binampersand{} and \cmd\bindnasrepma\br
    0.3 --- 2013/03/28\quad   Reorganized internal macros\br
    0.2 --- 2010/12/05\quad   Typos corrected\br
    0.1 --- 2007/04/02\quad   Initial version


\medskip
\noindent

\bye
