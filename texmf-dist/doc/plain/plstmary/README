plstmary: St. Mary's Road symbols for plainTeX
----------------------------------------------

LEGAL STUFF:

  This is public domain software. All files of the plstmary distribution
  (listed below) may be distributed and/or modified without restriction.

  While every effort has been made to make plstmary useful, it comes
  with no warranty, expressed or implied.

MANIFEST:

This distribution consists of the files

  * stmary.tex          Macros for use in plain TeX.
  * README              What you are reading now.
  * plstmary-doc.pdf    Documentation.
  * plstmary-doc.tex    Documentation source (tex, etex or pdftex).

DESCRIPTION:

(See plstmary-doc for more details.)

The command names used to produce the symbols are the same as those used
in the stmaryrd package for LaTeX.

The file stmary.tex loads amssym.tex. Three of the commands defined in
amssym.tex are redefined by stmary.tex, but the originals are saved
under different names.

By default, the symbols are provided for use in 10 point documents.
Commands for selecting different sizes are defined. These commands also
affect the size of the AMS symbols, but have no effect on other math
fonts nor on the text fonts.

VERSIONS:
    0.5 -- 2013/05/09   Autoloading of amssym, size changing for AMS
                        fonts, and saving of changed AMS commands.
    0.4 -- 2013/04/16   Correct \lbag, \Lbag, \binampersand and
                        \bindnasrepma
    0.3 -- 2013/03/28   Complete reorganization of internal macros,
    0.2 -- 2010/12/05   Typos corrected
    0.1 -- 2007/04/02   Initial version

--
Dan Luecking        luecking at uark dot edu
Department of Mathematical Sciences
1 University of Arkansas
Fayetteville, AR 72701 USA
