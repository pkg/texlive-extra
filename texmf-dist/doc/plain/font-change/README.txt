This is version 2015.2 of the package "font-change". This is the fourth version, the earlier versions being version 2015.1, 2009.1 and 2010.1.

The package includes 46 macro files of which 45 are macros to change text and math fonts in TeX. 

These fonts called by these macros are free fonts and are included in MiKTeX and TeX Live distributions.
 
All these macros should work smoothly with a full installation of MiKTeX (tested version 2.9.4503) and TeX Live 2014. 

For more information please refer to the documentation (font-change.pdf). 

***User's comments and suggestions are welcomed.***

=========== Licence / License ===========


This work was released under Creative Commons Attribution-Share Alike 3.0 Unported License on July 19, 2010. 

You are free to Share (to copy, distribute and transmit the work) and to Remix (to adapt the work) 
provided you follow the Attribution and Share Alike guidelines of the licence. For the full licence text, 
please visit: http://creativecommons.org/licenses/by-sa/3.0/legalcode.						

Amit Raj Dhawan 					amitrajdhawan@gmail.com
  (Author)