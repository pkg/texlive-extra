.\" Manpage for pdfxup.
.\" Contact pdfxup@markey.fr to correct errors or typos.
.TH man 1 "25 april 2021" "2.10" "pdfxup man page"
.SH NAME
pdfxup \- n-up tool with reduced margins
.SH SYNOPSIS
pdfxup [OPTIONS] [FILE]
.SH DESCRIPTION
pdfxup creates a PDF document where each page is obtained by combining
several pages of a PDF file given as output. The important feature of
pdfxup, compared to similar programs, is that it tries to compute the
(global) bounding box of the input PDF file, in order to remove the
margins and to keep the text only. Instead of having the font size
divided by 2 (for the case of 2-up output), in some case you may end up
with almost the same font size as in the original document (as is the
case for a default 'article' document produced by LaTeX).

pdfxup uses ghostscript for computing the maximal bounding box of (some
of) the pages of the document, and then uses pdflatex (with graphicx
package) in order to produce the new document.
.SH OPTIONS
pdfxup accepts numerous options. The most important ones are:
.\" additional options should be lister here...
.TP
\fB\-x\fR m, \fB\-\-columns\fR m
sets the number of columns of the output file (default 2);
.TP
\fB\-y\fR n, \fB\-\-rows\fR n
sets the number of lines of the output file (default 1);
.TP
\fB\-nup\fR m\fBx\fRn, \fB\--nup\fR m\fBx\fRn
sets the number of rows and columns of the output file (default 2x1);
.TP
\fB\-l\fR (0|1), \fB\-\-landscape\fR, \fB\-\-portrait\fR
sets orientation of paper (of final document);
.TP
\fB\-cf\fR file, \fB\-\-config \fR file, \fB\-\-mode \fR file
reads file (with extension \fB.xup\fR) and uses options defined in that
file (see below for details about \fB.xup\fR files).
All options are processed in the order the appear on the command-line,
so that it is possible to modify the options set in the configuration
file (and it is even possible to include several configuration files, the
later one overwriting the options set by the previous ones).
Files are looked-up using kpsewhich (if available). 
.TP
\fB\-b\fR (le|se), \fB\-\-booklet\fR (le|se)
configure for printing as a booklet. Value 'le' (which is the default
value when \fB\-b\fR is used with no argument) means that two-sided
printing is in 'long-edge' mode (you turn from one page to the next
along the long edge of the paper). 'se' is the 'short-edge' option.
.TP
\fB\-c\fR, \fB\-\-clip\fR, \fB\-nc\fR, \fB\-\-no-clip\fR
clip (or don't clip) pages to the computed bounding box. By default,
content is  clipped, to avoid overlap between neighbouring pages.
With \fB--no-clip\fR, anything outside the bounding box will be
displayed.
.TP
\fB\-o\fR file, \fB\-\-output\fR file
name of output file.
.TP
\fB\-i\fR
ask before overwriting output file.
.TP
\fB\-ow\fR
overwrite output file without asking.
.TP
\fB\-ps\fR s, \fB\-\-paper\fR s
sets paper size (default a4). The name must be known by package geometry
(more precisely, "<s>paper" should be defined in that package).
.TP
\fB\-fw\fR d, \fB\-\-framewidth\fR d
width of the frame around each page (default 0.4pt). Set to 0pt to have no
frame at all.
.TP
\fB\-tf\fR [0|1], \fB\-\-tight-frame\fR [0|1]
whether the frame should be tight around the page, leaving horizontal
white space outside the frame, or should be wide and span the whole
available width.
.TP
\fB\-im\fR d, \fB\-\-innermargins\fR d
inner margin between frame and page (default 5pt).
.TP
\fB\-m\fR d, \fB\-\-margins\fR d
margin of pages of the new document (default 5pt).
.TP
\fB\-is\fR d, \fB\-\-intspaces\fR d
space between different pages (default 1pt).
.TP
\fB\-p\fR list, \fB\-\-pages\fR list
only consider sublist of pages of input document. List is a
comma-separated list of pages or ranges pages of the form a-b; a can
be omitted to start from first page, and b can be omitted to end at
the last page. Therefore, "\fB\-p\fR -" (which is the default)
includes all pages. Also allows modulo, so that "\fB\-p\fR 0%2" would
include only even-numbered pages.
.TP
\fB\-bb\fR list, \fB\-\-bb\fR list
only consider sublist of pages of input document for computing bounding box.
.TP
\fB\-nobb\fR list, \fB\-\-no-bb\fR list
omit list of pages of input document from computation of bounding box.
.TP
\fB\-g\fR, \fB\-\-get-bb\fR
only compute (and output) bounding box. Will not produce any output file.
.TP
\fB\-kbb\fR, \fB\-\-keep-bb\fR
do not compute bounding box, preserve current margins.
.TP
\fB\-s\fR x y W H, \fB\-\-set-bb\fR x y W H
set the bounding box to the given values. Values are in pt; the first two
elements correspond to the lower left corner, while the last two represent
the width and height of the part to be displayed.
.TP
\fB\-w\fR file, \fB\-\-watermark\fR file
use file as background watermark. file can be any format accepted by
pdflatex (e.g. png or pdf). If file is a multipage PDF file, page n of
the watermark file is used with page n of the input file, and the last
page of the watermark file is repeated if the input file has more pages.
.TP
\fB\-wp\fR p, \fB\-\-watermark-period\fR p
repeat the last p pages of the watermark file instead of only the last one.
.TP
\fB\-d\fR, \fB\-\-debug\fR
debug mode: keep intermediary files.
.TP
\fB\-col\fR, \fB\-\-column-mode\fR, \fB\-\-vertical\fR
fill in pages top-down first (instead of the default left-to-right mode).
By default, pages are inserted from left to right, until the line is full;
with this option, pages are inserted from top to bottom, until the column
is full. See option \fB\-bal\fR below for examples of both options.
.TP
\fB\-row\fR, \fB\-\-row-mode\fR, \fB\-\-horizontal\fR
fill in pages left to right (which is the default mode).
.TP
\fB\-bal\fR, \fB\-\-balance-last\fR
balance last page: when using column mode, the pages are filled in from
top to bottom, and the last page is no exception. Still, it may be prefered
that the columns in the last page remains "balanced", which is what this
option achieves. Symmetrically, in row mode, this option would balance the
rows.
.nf
                 ---------                        ---------
row, no-balance | 1  2  3 |  column, no-balance  | 1  4  7 |
(default mode)  | 4  5  6 |                      | 2  5  8 |
                | 7  8  9 |                      | 3  6  9 |
                 ---------                        ---------
                 ---------                        ---------
                |10 11 12 |                      |10 13    |
                |13 14    |                      |11 14    |
                |         |                      |12       |
                 ---------                        ---------

                 ---------                        ---------
   row, balance | 1  2  3 |     column, balance  | 1  4  7 |
                | 4  5  6 |                      | 2  5  8 |
                | 7  8  9 |                      | 3  6  9 |
                 ---------                        ---------
                 ---------                        ---------
                |10 11    |                      |10 12 14 |
                |12 13    |                      |11 13    |
                |14       |                      |         |
                 ---------                        ---------

.fi
.TP
\fB\-V [0-3]\fR, \fB\-\-verbose\fR [0-3]
select verbosity (default: 1).
.TP
\fB\-q\fR, \fB\-\-quiet\fR
run quietly (equiv. '-V 0').
.TP
\fB\-v\fR, \fB\-\-version\fR
print version number and exit.
.TP
\fB\-h\fR, \fB\-\-help\fR
print help message and exit.
.SH CONFIGURATION FILES
Configuration files (extension \fB.xup\fR) are bash scripts used
to set some variables. If option "\fB--config-file\fR <file>"
(or equivalent) is used, <file> is looked up with kpsewhich
first, if it contains no '/'. If kpsewhich does not find it,
then pdfxup checks if the file exists (using 'test -e') before
sourcing it (the \fBPATH\fR variable will not be used to find
it).

The script should only set some internal variables of pdfxup, such as
\fBNB_ROWS\fR, \fBNB_COLUMNS\fR, \fBLANDSCAPE\fR (to set up a
predefined layout). It can be used to set the bounding box, but it is
often better to compute it on the first few pages of the document.
Here are some examples of lines that can be put in a \fB.xup\fR file:
.TP
\fBNB_COLUMNS=1\fR
set the number of columns to 1
.TP
\fBNB_ROWS=2\fR
set the number of rows to 2
.TP
\fBLANDSCAPE=0\fR
set portrait mode
.TP
\fBKEEP_ORIG_BBOX=1\fR
do not crop margins
.TP
\fBSET_BBOX="75 47 540 755"
set bounding box: lower left=(75,47); upper right=(540,755)
.TP
\fBTIGHT_FRAME=1\fR
set tight frames around pages
.TP
\fBINNER_MARGINS=10pt\fR
set margin around each page (inside the frame) to 10pt
.TP
\fBINTERM_SPACES=10pt\fR
set space between pages to 10pt
.TP
\fBFRAME_WIDTH=2mm\fR
set frame width to 2mm

.PP
Other available variables can be found in the \fBsetdefaultvalues\fR function.

.SH EXAMPLES
.TP
\fB# pdfxup file.pdf\fR
produces 2-up pdf file from file.pdf.
.TP
\fB# pdfxup -bb 1-4 file.pdf\fR
same behaviour, but computes the bounding box only using the 
first 4 pages (this saves time when processing long documents).
.TP
\fB# pdfxup -b -o booklet.pdf file.pdf\fR
same behaviour, but creates a booklet (as booklet.pdf).
.TP
\fB# pdfxup -kbb -x1 -y2 -l0 beamer-frames.pdf\fR
arranges 2 beamer frames per page (not reducing margins).
.TP
\fB# pdfxup --mode beamer2 beamer-frames.pdf\fR
arranges PDF pages according to beamer2.xup configuration file.
.TP
\fB# pdfxup -kbb -x2 -y2 -l beamer-frames.pdf:1-12,15-19\fR
arranges 4 beamer frames per page (not reducing margins), including
only frames 1 to 12 and 15 to 19.
.SH SEE ALSO
gs(1), pdflatex(1)
.SH AUTHOR
Nicolas Markey (pdfxup@markey.fr)
