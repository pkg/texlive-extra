PDFXUP -- v2.10 (2021/04/25)
N. Markey <pdfxup@markey.fr>


pdfxup is a bash shell script that creates a PDF document where each
page is obtained by combining several pages of a PDF file given as
output. The important feature of pdfxup, compared to similar programs,
is that it tries to compute the (global) bounding box of the input PDF
file, in order to remove the margins and to keep the text only.
Instead of having the font size divided by 2 (for the case of 2-up
output), in some case you may end up with the same font size as in the
original document (as is the case for a default 'article' document
produced by LaTeX).

pdfxup uses ghostscript for computing the maximal bounding box of
(some of) the pages of the document, and then uses pdflatex (with
graphicx package) in order to produce the new document.



%% (c) 2021/04/25 Nicolas Markey <pdfxup at markey dot fr>
%%
%% This work may  be distributed and/or modified under  the conditions of
%% the LaTeX Project  Public License, either version 1.3  of this license
%% or (at  your option)  any later version.   The latest version  of this
%% license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX version
%% 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `maintained'.
%% The Current Maintainer of this work is Nicolas Markey.
