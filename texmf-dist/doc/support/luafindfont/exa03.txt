We are using Lua 5.3
Check for file /usr/local/texlive/2021/texmf-var/luatex-cache/generic/names/luaotfload-names
 1.              Arial Black.ttf               arialblack               /System/Library/Fonts/Supplemental/Arial Black.ttf
 2.        Arial Bold Italic.ttf                    arial         /System/Library/Fonts/Supplemental/Arial Bold Italic.ttf
 3.               Arial Bold.ttf                    arial                /System/Library/Fonts/Supplemental/Arial Bold.ttf
 4.             Arial Italic.ttf                    arial              /System/Library/Fonts/Supplemental/Arial Italic.ttf
 5. Arial Narrow Bold Italic.ttf              arialnarrow  /System/Library/Fonts/Supplemental/Arial Narrow Bold Italic.ttf
 6.        Arial Narrow Bold.ttf              arialnarrow         /System/Library/Fonts/Supplemental/Arial Narrow Bold.ttf
 7.      Arial Narrow Italic.ttf              arialnarrow       /System/Library/Fonts/Supplemental/Arial Narrow Italic.ttf
 8.             Arial Narrow.ttf              arialnarrow              /System/Library/Fonts/Supplemental/Arial Narrow.ttf
 9.       Arial Rounded Bold.ttf       arialroundedmtbold        /System/Library/Fonts/Supplemental/Arial Rounded Bold.ttf
10.            Arial Unicode.ttf           arialunicodems                                 /Library/Fonts/Arial Unicode.ttf
11.            Arial Unicode.ttf           arialunicodems             /System/Library/Fonts/Supplemental/Arial Unicode.ttf
12.                    Arial.ttf                    arial                     /System/Library/Fonts/Supplemental/Arial.ttf
13.                    Arial.ttf                    arial                        /Users/voss/Library/Fonts/Arial/Arial.ttf
14.                  ArialHB.ttc              arialhebrew                                /System/Library/Fonts/ArialHB.ttc
15.                  ArialHB.ttc arialhebrewdeskinterface                                /System/Library/Fonts/ArialHB.ttc
16.                  ArialHB.ttc       arialhebrewscholar                                /System/Library/Fonts/ArialHB.ttc
17.                  ArialHB.ttc              arialhebrew                                /System/Library/Fonts/ArialHB.ttc

Run otfinfo:6
Family:              Arial Narrow
Subfamily:           Bold
Full name:           Arial Narrow Bold
PostScript name:     ArialNarrow-Bold
Version:             Version 2.38.1x
Unique ID:           Arial Narrow Bold : 2007
Description:         Monotype Drawing Office 1982. A contemporary sans serif design, Arial contains more humanist characteristics than many of its predecessors and as such is more in tune with the mood of the last decades of the twentieth century. The overall treatment of curves is softer and fuller than in most industrial-style sans serif faces. Terminal strokes are cut on the diagonal which helps to give the face a less mechanical appearance. Arial is an extremely versatile family of typefaces which can be used with equal success for text setting in reports, presentations, magazines etc, and for display use in newspapers, advertising and promotions.
Designer:            Robin Nicholas, Patricia Saunders
Manufacturer:        The Monotype Corporation
Trademark:           Arial is a trademark of The Monotype Corporation in the United States and/or other countries.
Copyright:           © 2007 The Monotype Corporation. All Rights Reserved.
License Description: You may use this font to display and print content as permitted by the license terms for the product in which this font is included. You may only (i) embed this font in content as permitted by the embedding restrictions included in this font; and (ii) temporarily download this font to a printer or other output device to help print content.
Vendor ID:           TMC