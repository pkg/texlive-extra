%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Project:     Documentation Tools
%% Descr:       Latex -->  MAN-page (groff -man), LATEX documentation
%% Author:      Dr. Jürgen Vollmer, Juergen.Vollmer@informatik-vollmer.de
%% $Id: latex2man.inc,v 1.3 2017/04/13 14:47:28 vollmer Exp $
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Changes}
%@% IF LATEX %@%
{\small\verbatiminput{./CHANGES}}
%@% ELSE %@%
%@% IF HTML %@%
Please check the file \URL{latex2man-CHANGES.html} for the list of changes and
acknowledgment to people contributing bugfixes or enhancements.
%@% ELSE %@%
Please check the file \URL{latex2man-CHANGES} for the list of changes and
acknowledgment to people contributing bugfixes or enhancements.

%@% END-IF %@%
%@% END-IF %@%
