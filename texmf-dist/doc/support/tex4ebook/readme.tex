\hypertarget{introduction}{%
\section{Introduction}\label{introduction}}

\texttt{TeX4ebook} is a tool for conversion from \LaTeX~to ebook
formats, such as EPUB, MOBI and EPUB 3. It is based on
\texttt{TeX4ht}\footnote{\url{https://tug.org/tex4ht/}}, which provides
instructions for the actual \LaTeX~to HTML conversion, and on
\texttt{make4ht}\footnote{\url{https://ctan.org/pkg/make4ht?lang=en}}.

The conversion is focused on the logical structure of the converted
document and metadata. Basic visual appearance is preserved as well, but
you should use custom configurations if you want to make the document
more visually appealing. You can include custom \texttt{CSS} or fonts in
a configuration file.

\texttt{TeX4ebook} supports the same features as \texttt{make4ht}, in
particular build files and extensions. These may be used for
post-processing of the generated HTML files, or to configure the image
conversion. See the \texttt{make4ht} documentation to see the supported
features.

\hypertarget{license}{%
\subsection{License}\label{license}}

Permission is granted to copy, distribute and/or modify this software
under the terms of the LaTeX Project Public License, version 1.3.

\hypertarget{usage}{%
\section{Usage}\label{usage}}

Run on the command line:

\begin{verbatim}
tex4ebook [options] filename
\end{verbatim}

You don't have to modify your source files unless you want to use
commands defined by \texttt{tex4ebook} in the document, or when your
document uses a package which causes a compilation error.

If you want to use \texttt{tex4ebook} commands, add this line to your
document preamble:

\begin{verbatim}
\usepackage{tex4ebook}
\end{verbatim}

But it is optional. You shouldn't need to modify your \TeX~files

\hypertarget{available-commands}{%
\subsection{Available commands}\label{available-commands}}

\begin{itemize}
\tightlist
\item
  \texttt{\textbackslash{}coverimage{[}\textless{}graphicx\ options\textgreater{}{]}\{coverimage.name\}}
  - include cover image to the document. You can pass the same options
  as to \texttt{\textbackslash{}includegraphics} command in the optional
  argument.
\end{itemize}

For example:

\begin{verbatim}
\thispagestyle{empty}
\begin{document}
\coverimage[scale=0.8]{coverimage.name} % include scaled cover image
...
\pagestyle{headings}
\end{verbatim}

\hypertarget{command-line-options}{%
\section{Command line options}\label{command-line-options}}

\begin{description}
\item[\texttt{-a,-\/-loglevel}]
Set message log level. Possible values: debug, info, status, warning,
error, fatal. Default: status.
\item[\texttt{-c,-\/-config}]
specify custom config file for \texttt{TeX4ht}
\end{description}

\textbf{example config file}: File \texttt{sample.cfg}

\begin{verbatim}
  \Preamble{xhtml}
  \CutAt{section}
  \begin{document}
  \EndPreamble
\end{verbatim}

run

\begin{verbatim}
  tex4ebook -c sample filename.tex
\end{verbatim}

This config file will create \texttt{xhtml} file for every section. Note
that this behaviour is default.

\begin{description}
\item[\texttt{-e,-\/-build-file\ (default\ nil)}]
Specify make4ht build file\footnote{\url{https://github.com/michal-h21/make4ht\#build-file}}.
Default build file filename is \texttt{filename.mk4}, use this option if
you use different filename.
\item[\texttt{-f,-\/-format\ (default\ epub)}]
Output format. Possible values are \texttt{epub}, \texttt{epub3},
\texttt{mobi}, \texttt{azw} and \texttt{azw3}.
\item[\texttt{-j,-\/-jobname}]
Specify the output file name, without file extension.
\item[\texttt{-l,-\/-lua}]
Use LuaLaTeX as TeX engine.
\item[\texttt{-m,-\/-mode\ (default\ default)}]
This set \texttt{mode} variable, accessible in the build file. Default
supported values are \texttt{default} and \texttt{draft}. In
\texttt{draft} mode, document is compiled only once, instead of three
times.
\item[\texttt{-r,-\/-resolution}]
Resolution of generated images, for example math. It should meet
resolution of target devices, which is usually about 167 ppi.
\item[\texttt{-s,-\/-shell-escape}]
Enable shell escape in the \texttt{htlatex} run. This is necessary for
the execution of the external commands from your source files.
\item[\texttt{-t,-\/-tidy}]
process output html files with \texttt{HTML\ tidy} command\footnote{It
  needs to be installed separately}.
\item[\texttt{-x,-\/-xetex}]
Use xelatex for document compilation
\item[\texttt{-v,-\/-version}]
Print the version number.
\end{description}

\hypertarget{configuration}{%
\section{Configuration}\label{configuration}}

\texttt{TeX4ebook} uses \texttt{TeX4ht}\footnote{\url{http://www.tug.org/tex4ht/}}
for conversion from LaTeX to html. \texttt{TeX4ht} is highly
configurable using config files. Basic config file structure is

\begin{verbatim}
\Preamble{xhtml, comma separated list of options}
...
\begin{document}
...
\EndPreamble
\end{verbatim}

Basic info about command configurations can be found in a
work-in-progres \emph{TeX4ht tutorial}\footnote{\url{https://github.com/michal-h21/helpers4ht/wiki/tex4ht-tutorial}},
\emph{TeX4ht documentation}\footnote{\url{http://www.tug.org/applications/tex4ht/mn11.html}},
and in series of blogposts on CV Radhakrishnan's blog: \emph{Configure
part 1}\footnote{\url{https://web.archive.org/web/20180908234227/http://www.cvr.cc/?p=323}},
\emph{Configure part 2}\footnote{\url{https://web.archive.org/web/20180908201057/http://www.cvr.cc/?p=362}},
\emph{Low level commands}\footnote{\url{https://web.archive.org/web/20180909101325/http://cvr.cc/?p=482}}.
Available options for \texttt{\textbackslash{}Preamble} command are
listed in the article \emph{TeX4ht: options}\footnote{\url{https://web.archive.org/web/20180813043722/http://cvr.cc/?p=504}}.
\emph{Comparison of tex4ebook and Pandoc output}\footnote{\url{https://github.com/richelbilderbeek/travis_tex_to_epub_example_1}}

A great source of tips for \texttt{TeX4ht} configuration is \emph{tex4ht
tag on TeX.sx}\footnote{\url{http://tex.stackexchange.com/questions/tagged/tex4ht}}.
There is also a \emph{tag for tex4ebook}\footnote{\url{http://tex.stackexchange.com/questions/tagged/tex4ebook}}.

Examples of interesting questions are \emph{including images and fonts
in ebooks}\footnote{\url{http://tex.stackexchange.com/a/213165/2891}} or
\emph{setting image size in em units instead of pt}\footnote{\url{http://tex.stackexchange.com/a/195718/2891}}.

\hypertarget{provided-configurations}{%
\subsection{Provided configurations}\label{provided-configurations}}

\texttt{tex4ebook} provides some configurations for your usage:

\begin{verbatim}
\Configure{UniqueIdentifier}{identifier}
\end{verbatim}

Every EPUB file should have unique identifier, like ISBN, DOI, URI etc.
Default identifier is URI, with value
\texttt{http://example.com/\textbackslash{}jobname}.

\begin{verbatim}
\Configure{@author}{\let\footnote\@gobble}
\end{verbatim}

Local definitions of commands used in the
\texttt{\textbackslash{}author} command. As contents of
\texttt{\textbackslash{}author} are used in XML files, it is necessary
to strip away any information which don't belongs here, such as
\texttt{\textbackslash{}footnote}.

\begin{verbatim}
\Configure{OpfScheme}{URI}
\end{verbatim}

Type of unique identifier, default type is URI. It is used only in the
EPUB format, it is deprecated for EPUB 3.

\begin{verbatim}
\Configure{resettoclevels}{list of section types in descending order}
\end{verbatim}

Configure section types which should be included in the \texttt{NCX}
file. Default value is the whole document hierarchy, from
\texttt{\textbackslash{}part} to \texttt{\textbackslash{}paragraph}.

\begin{verbatim}
\Configure{DocumentLanguage}{language code}
\end{verbatim}

Each EPUB file must declare the document language. It is inferred from
\texttt{babel} main language by default, but you can configure it when
it doesn't work correctly. The \texttt{language\ code} should be in
\href{https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes}{ISO 639-1}
form.

\begin{verbatim}
\Configure{CoverImage}{before cover image}{after cover image}
\end{verbatim}

By default, cover image is inserted in
\texttt{\textless{}div\ class="cover-image"\textgreater{}} element, you
may use this configuration option to insert different markup, or even to
place the cover image to standalone page.

\begin{verbatim}
\Configure{CoverMimeType}{mime type of cover image}
\end{verbatim}

Default value is \texttt{image/png}, change this value if you use other
image type than \texttt{png}.

If you don't want to include the cover image in the document, use
command

\begin{verbatim}
\CoverMetadata{filename}
\end{verbatim}

in the config file.

\begin{verbatim}
\Configure{OpfMetadata}{item element}
\end{verbatim}

Add item to \texttt{\textless{}metadata\textgreater{}} section in the
\texttt{OPF} file.

\begin{verbatim}
\Configure{OpfManifest}{maifest element}
\end{verbatim}

Add item to \texttt{\textless{}manifest\textgreater{}} section in the
\texttt{OPF} file.

\begin{verbatim}
\Configure{xmlns}{prefix}{uri}
\end{verbatim}

Add \texttt{XML} name space to the generated \texttt{XHTML} files.
Useful in \texttt{EPUB\ 3}.

\hypertarget{example-config-file}{%
\subsubsection{Example config file}\label{example-config-file}}

\begin{verbatim}
\Preamble{xhtml}
\begin{document}
\Configure{DocumentLanguage}{de}
% Use following lines if your document has ISBN:
% \Configure{OpfScheme}{ISBN}
% \Configure{UniqueIdentifier}{3-0000-1111-X}
% Another possibility is URI that points for example to the ebook homepage:
\Configure{OpfScheme}{URI}
\Configure{UniqueIdentifier}
{https://de.wikipedia.org/wiki/Der_achte_Sch&ouml;pfungstag}
\Configure{CoverMimeType}{image/jpeg}
% If you don't use \coverimage in the document text, 
% add cover image using this command:
\CoverMetadata{coverimage.jpg}
% You can also add more authors to your ebook metadata:
\Configure{OpfMetadata}
{\HCode{<dc:publisher>Deutscher Bücherbund</dc:publisher>}}
\Configure{OpfMetadata}
{\HCode{<dc:contributor>Image Artist</dc:contributor>}}
\Configure{OpfMetadata}
{\HCode{<dc:contributor>Trans Lator</dc:contributor>}}
\Configure{OpfMetadata}
{\HCode{<dc:date opf:event='original-publication'>1888</dc:date>}}
\EndPreamble
\end{verbatim}

Remarks:

\begin{itemize}
\tightlist
\item
  Leading percent signs in the \texttt{.cfg} file introduce comments
\item
  If the unique identifier is a URI which contains diacritical
  characters, the\\
  equivalent HTML code needs to be inserted. \texttt{UTF8} is not
  recognized at that place.
\item
  \texttt{UTF8} characters may be used in the \texttt{OpfMetadata}
  sections.
\end{itemize}

\hypertarget{commands-available-in-the-config-file}{%
\subsection{Commands available in the config
file}\label{commands-available-in-the-config-file}}

\begin{description}
\item[\texttt{\textbackslash{}OpfRegisterFile{[}filename{]}}]
register file in the \texttt{OPF} file. Current output file is added by
default.
\item[\texttt{\textbackslash{}OpfAddProperty\{property\ type\}}]
add \texttt{EPUB3} property for the current file. See \emph{EPUB3
spec}\footnote{\url{http://www.idpf.org/epub/301/spec/epub-publications.html\#sec-item-property-values}}
\item[\texttt{\textbackslash{}OpfGuide{[}filename{]}\{title\}\{type\}}]
Add file to the \texttt{\textless{}guide\textgreater{}} section in the
\texttt{OPF} file. See \emph{Where do you start an ePUB and what is the
\texttt{\textless{}guide\textgreater{}} section of the \texttt{.OPF}
file?}\footnote{\url{http://epubsecrets.com/where-do-you-start-an-epub-and-what-is-the-guide-section-of-the-opf-file.php}}
for some details. Note that \texttt{\textless{}guide\textgreater{}} is
deprecated in \texttt{EPUB\ 3}.
\end{description}

\hypertarget{build-files}{%
\subsection{Build files}\label{build-files}}

\texttt{tex4ebook} uses \texttt{make4ht}\footnote{\url{https://github.com/michal-h21/make4ht}}
as a build system. See \texttt{make4ht} documentation for details on
build files.

\hypertarget{tex4ebook-configuration-file}{%
\subsection{\texorpdfstring{\texttt{.tex4ebook} configuration
file}{.tex4ebook configuration file}}\label{tex4ebook-configuration-file}}

It is possible to globally modify the build settings using the
configuration file. New compilation commands can be added, extensions
can be loaded or disabled and settings can be set.

\hypertarget{location}{%
\subsubsection{Location}\label{location}}

The configuration file can be saved either in
\texttt{\$HOME/.config/tex4ebook/config.lua} or in \texttt{.tex4ebook}
in the current directory or it's parents (up to \texttt{\$HOME}).

See the \texttt{make4ht} documentation for an example and more
information.

\hypertarget{troubleshooting}{%
\section{Troubleshooting}\label{troubleshooting}}

\hypertarget{kindle-formats}{%
\subsection{Kindle formats}\label{kindle-formats}}

\texttt{tex4ebook} uses \texttt{kindlegen} command for the conversion to
Kindle formats (\texttt{mobi}, \texttt{azw} and \texttt{azw3}).
Unfortunatelly, Amazon discontinued this command, so we use also
\texttt{ebook-convert} provided by Calibre if \texttt{kindlegen} fails.

\hypertarget{fixed-layout-epub}{%
\subsection{Fixed layout EPUB}\label{fixed-layout-epub}}

The basic support for the Fixed layout EPUB 3 can be enabled using the
following configurations:

\begin{verbatim}
\Configure{OpfMetadata}
{\HCode{<meta property="rendition:layout">pre-paginated</meta>}}
\Configure{OpfMetadata}
{\HCode{<meta property="rendition:orientation">landscape</meta>}}
\Configure{OpfMetadata}
{\HCode{<meta property="rendition:spread">none</meta>}}
\Configure{@HEAD}
{\HCode{<meta name="viewport" content="width=1920, height=1080"/>\Hnewline}}
\end{verbatim}

Modify the dimensions in the
\texttt{\textless{}meta\ name="viewport\textgreater{}} element according
to your needs.

\hypertarget{math-issues}{%
\subsection{Math issues}\label{math-issues}}

Note that while \texttt{mobi} is supported by Amazon Kindle, most
widespread ebook reader, it doesn't support \texttt{MathML}. This means
that math must be represented as images. The same issue is true for the
EPUB format as well. This is problematic especially for the inline math,
as you may experience wrong vertical alignment of the math content and
surrounding text. If your ebook contains math, a better solution is to
produce the \texttt{epub3} format, as it supports \texttt{MathML}. The
issue with EPUB 3 is that majority of \texttt{e-ink} ebook readers don't
support it. Reader applications exists mainly for Android and Apple
devices. For books which contains mainly prose, all formats should be
suitable, but EPUB 3 supports most features from web standards, such as
\texttt{CSS}.

\hypertarget{compilation-errors}{%
\subsection{Compilation errors}\label{compilation-errors}}

When compilation of the document breaks with error during \texttt{LaTeX}
run, it may be caused by some problem in \texttt{TeX4ht} configuration.
Comment out line \texttt{\textbackslash{}usepackage\{tex4ebook\}} in
your source file and run command:

\begin{verbatim}
htlatex filename 
\end{verbatim}

if same error as in \texttt{tex4ebook} run arises, the problem is in
some \texttt{TeX4ht} configuration. Try to identify the source of
problem and if you cannot find the solution, make minimal example
showing the error and ask for help either on \emph{TeX4ht mailing
list}\footnote{\url{http://tug.org/mailman/listinfo/tex4ht}} or on
\emph{TeX.sx}\footnote{\url{http://tex.stackexchange.com/}}.

\hypertarget{validation}{%
\subsection{Validation}\label{validation}}

In case of successful compilation, use command line tool
\texttt{epubcheck}\footnote{you need to install it separately, see
  \url{https://github.com/IDPF/epubcheck}} to check whether your
document doesn't contain any errors.

Type

\begin{verbatim}
epubcheck filename.epub
\end{verbatim}

\hypertarget{common-validation-issues}{%
\subsubsection{Common validation
issues:}\label{common-validation-issues}}

\begin{itemize}
\tightlist
\item
  WARNING: filename.epub: item (OEBPS/foo.boo) exists in the zip file,
  but is not declared in the OPF file
\end{itemize}

Delete the
\texttt{filename-(epub\textbar{}epub3\textbar{}mobi\textbar{}azw\textbar{}azw3)}
folder and \texttt{filename.epub}. Then run \texttt{tex4ebook} again.

\begin{itemize}
\item
  WARNING(ACC-009): hsmmt10t.epub/OEBPS/hsmmt10tch17.xhtml(235,15):
  MathML should either have an alt text attribute or annotation-xml
  child element.

  This is accessibility message. Unless you use some macro with
  annotations for each math instance, you will get lot of these
  messages. Try to use \texttt{epubcheck\ -e} to print only serious
  errors.
\end{itemize}
