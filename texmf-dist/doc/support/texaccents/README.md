# TeXaccents

A standalone utility designed to convert legacy (La)TeX (text mode, no math) and BibTeX codes for "accented" characters to Unicode equivalents. For example, `\={a}` ('a' with macron) will be converted to `ā`.

Current version: 2022/09/18 version 1.0.1
Windows version: 2022/09/18 version 1.0.1

This project is licensed under the MIT License: see https://opensource.org/licenses/MIT

