* spix 1.3.0 (2022-11-18)

    * Add Python3.11 support.
    * Add tests.
    * Flush output: it could lead to output lines being shuffled.

    -- Louis Paternault <spalax@gresille.org>

* spix 1.2.0 (2021-11-27)

    * Add Python3.10 support.

    -- Louis Paternault <spalax@gresille.org>

* spix 1.1.0 (2020-07-23)

    * Python 3.9 support.
    * Replace `pathlib.PosixPath` with `pathlib.Path`.
    * Merge `__init__.py` and `__main__.py` into a single `spix.py` file.
    * Add a man page.

    -- Louis Paternault <spalax@gresille.org>

* spix 1.0.1 (2020-06-23)

    * Do not crash when processing non-UTF8 files (closes #1).

    -- Louis Paternault <spalax@gresille.org>

* spix 1.0.0 (2020-06-19)

    * [doc] Minor documentation improvements.

    -- Louis Paternault <spalax@gresille.org>

* spix 1.0.0-beta2 (2020-06-12)

    * [command line] Use FILE instead of TEX in description of command line arguments.
    * Code snippet can use environment variables `$basename`, `$texname`.

    -- Louis Paternault <spalax@gresille.org>

* spix 1.0.0-beta (2020-06-11)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
