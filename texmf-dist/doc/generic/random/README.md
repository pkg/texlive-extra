#random.tex      ver 0.2   (Donald Arseneau)

Generating "random" numbers in TeX. 

   `\setrannum {<`*counter*`>} {<`*minimum*`>} {<`*maximum*`>}`  
   `\setrandimen {<`*dimen-register*`>} {<`*minimum*`>} {<`*maximum*`>}`

This software is contributed to the public domain.

Definitions are in `random.tex`. Documentation is in comments of
`random.tex` and in `random-doc.tex` and `random-doc.pdf`
