%% $Id: pst-antiprism-doc.tex 729 2018-02-13 17:48:53Z herbert $
\documentclass[11pt,english,BCOR10mm,DIV12,bibliography=totoc,parskip=false,headings=small,
    headinclude=false,footinclude=false,oneside=true]{pst-doc}
\listfiles

\usepackage[utf8]{inputenc}
\usepackage{pst-antiprism}
\let\pstFV\fileversion
\def\bgImage{\resizebox{7cm}{!}{\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism[a=1,n=15,hue=0 1 0.5 1,
             linecolor={[rgb]{0 0 0.5}}]
\end{pspicture}}}

\usepackage{showexpl,listings,xltabular,booktabs,animate}

\lstset{explpreset={pos=l,width=-99pt,overhang=0pt,hsep=\columnsep,vsep=\bigskipamount,rframe={}}}

\usepackage{biblatex}
\addbibresource{\jobname.bib}


\definecolor{Miel} {RGB}{218 179 10}
\definecolor{Maroon} {cmyk}{0 0.87 0.68 0.32}

\begin{document}
\title{\texttt{pst-antiprism}: Drawing an antiprism \\
\small v.\pstFV}
%\docauthor{}
\author{Manuel Luque\\Herbert Voß}

\maketitle

\tableofcontents



\section{Introduction}

An antiprism is a semiregular polyhedron constructed with 2 n-gons and 2n triangles. 
The nets are particularly simple, consisting of two n-gons on top and bottom, separated by a 
ribbon of 2n triangles, with the two n-gons being offset by one ribbon segment. 
The duals of the antiprisms are the trapezohedra.~\cite{weisstein}

The macro \Lcs{psAntiprism} has the following syntax:

\begin{BDef}
\Lcs{psAntiprism}\OptArgs
\end{BDef}

The special optional arguments with its default values are

\begin{xltabular}{\linewidth}{@{} >{\ttfamily}l l X @{}}\toprule
\rmfamily\emph{name} & \emph{default} & \emph{description}\\\midrule
n & 8 & number of the edges of the polygon\\
a & 1 &  the radius of the outer polygon circle\\
meshbases & true & A boolean to mesh the bases with triangles whose one vertex is the 
    center of the base and the two other two consecutive vertices of the polygon of the base.\\
colored & false & A boolean which will color the antiprism. This is  only possible with \texttt{meshbases=true}.
    The bases of the triangles allow a coloration by continuity of a triangle of the periphery of the 
     antiprisme and the corresponding triangle of the base. It is an adaptation of the idea of H.\,B.\,Meyer 
  for hexagonal antiprism.~\cite{meyer}\\
fan & false & draw the antiprism as a fan.\\
\bottomrule
\end{xltabular}


\section{Examples}
\subsection{The default behaviour}

For \Lkeyword{viewpoint} and \Lkeyword{Decran} see the documentation of \LPack{pst-solides3d}.~\cite{solides3d}

\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism
\end{pspicture}
\end{LTXexample}


\clearpage

\subsection{Using the optional arguments}


\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism[a=1,n=15,hue=0 1 0.5 1,
             linecolor={[rgb]{0 0 0.5}}]
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=75}
\psAntiprism[a=2,n=10,fillcolor=Miel,hollow,incolor=yellow!20,
             linecolor={[rgb]{0 0 0.5}},
             linewidth=1.5pt,
             opacity=0.9]
\end{pspicture}
\end{LTXexample}



\subsection{No lines for the base triangles: option \texttt{meshbases=false}}
In this case, the 2 bases have the numbers 0 and 1 and we can delete them with the optional
argument setting  \Lkeyword{rm}\texttt{=0 1}.







\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism[a=2,n=8,inouthue=1 0 0.5 1,
             meshbases=false,hollow,
             opacity=0.8]
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism[a=2,n=8,inouthue=1 0 0.5 1,
             meshbases=false,numfaces=,hollow,
             opacity=0.8,rm=0 1,affinage=]
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 60 30 rtp2xyz,Decran=100}
\psAntiprism[a=2,n=10,fillcolor=Maroon,
             incolor=yellow!20,
             linecolor=blue,
             meshbases=false,hollow,
             opacity=0.8,affinage=all]
\end{pspicture}
\end{LTXexample}


\section{Colored anitpriam}

This behaviour needs the setting \texttt{meshbases=true} and \texttt{colored=true}.

It allows coloring by continuity of a triangle
around the antiprism and the corresponding triangle of the base. The other options didn't changed
its meaning.

\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 90 30 rtp2xyz,Decran=100}
\psset{a=1,r=1}
\psAntiprism[colored,n=17]
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=0.35\linewidth]
\begin{pspicture}(-3,-3)(3,3)
\psset{viewpoint=100 90 -30 rtp2xyz,Decran=100}
\psset{lightsrc=viewpoint}
\psset{a=1,r=1,hollow,opacity=0.8,linecolor=blue}
\psAntiprism[colored,n=17]
\end{pspicture}
\end{LTXexample}


\section{An antiprism as a fan}

With the optional argument \Lkeyword{fan} the antiprism can be drawn like a fan:

\begin{LTXexample}[width=0.5\linewidth]
\begin{pspicture}(-4.5,-2.5)(4.5,2.5)
\psset{viewpoint=200 15 20 rtp2xyz,
  Decran=500}
\psAntiprism[fan,a=0.5,n=20,
  inouthue=0.1 1,hollow,opacity=0.9]
\end{pspicture}
\end{LTXexample}



\begin{LTXexample}[width=0.55\linewidth]
\begin{pspicture}(-4.5,-3)(4.5,3)
\psset{viewpoint=100 20 30 rtp2xyz,
  Decran=150}
\psAntiprism[fan,n=12,a=1.5,hollow,
  incolor=yellow,fillcolor=red,
  linecolor=blue,opacity=0.95,
  affinage=all,affinagecoeff=0.9]
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=0.55\linewidth]
\begin{pspicture}(-4.5,-3)(4.5,3)
\psset{viewpoint=200 2 25 rtp2xyz,
  Decran=500,solidmemory}
\psAntiprism[fan,n=20,a=0.5,hollow,
  inouthue=0.1 1,opacity=0.9,
  plansepare={[1 0 0 0.05]},
  name=eventail,action=none]
\psSolid[object=load,load=eventail1,
  deactivatecolor,hollow,opacity=0.8]
\end{pspicture}
\end{LTXexample}


\subsection{animation}
With the package \LPack{animate} one can create inline animations in an easy way:


\begin{center}
\begin{animateinline}[controls,loop,
                     begin={\begin{pspicture}(-4.5,-2.5)(4.5,2.5)},
                     end={\end{pspicture}}]{12}% 25 images/s
\multiframe{72}{iTheta=0+5}{%
\psset{viewpoint=200 \iTheta\space 20 rtp2xyz,
  Decran=500}
\psAntiprism[fan,a=0.5,n=20,inouthue=0.1 1,hollow,opacity=0.9]}
\end{animateinline}
\end{center}


\begin{lstlisting}
\begin{animateinline}[controls,loop,
                     begin={\begin{pspicture}(-4.5,-2.5)(4.5,2.5)},
                     end={\end{pspicture}}]{12}% 25 images/s
\multiframe{72}{iTheta=0+5}{%
\psset{viewpoint=200 \iTheta\space 20 rtp2xyz,
  Decran=500}
\psAntiprism[fan,a=0.5,n=20,inouthue=0.1 1,hollow,opacity=0.9]}
\end{animateinline}
\end{lstlisting}


\begin{center}
\begin{animateinline}[controls,loop,
                     begin={\begin{pspicture}(-4,-4)(4,4)},
                     end={\end{pspicture}}]{12}% 25 images/s
\multiframe{72}{iTheta=0+5}{%
\psset{viewpoint=100 90 20 rtp2xyz,Decran=120}
\psset{lightsrc=viewpoint}
\psset{a=1,r=1,hollow,opacity=0.8,linecolor=blue,RotSequence=zxy,RotX=\iTheta,RotZ=\iTheta}
\psAntiprism[colored,n=17]}
\end{animateinline}
\end{center}


\begin{lstlisting}
\begin{animateinline}[controls,loop,
                     begin={\begin{pspicture}(-4,-4)(4,4)},
                     end={\end{pspicture}}]{12}% 25 images/s
\multiframe{72}{iTheta=0+5}{%
\psset{viewpoint=100 90 20 rtp2xyz,Decran=120}
\psset{lightsrc=viewpoint}
\psset{a=1,r=1,hollow,opacity=0.8,linecolor=blue,RotSequence=zxy,RotX=\iTheta,RotZ=\iTheta}
\psAntiprism[colored,n=17]}
\end{animateinline}
\end{lstlisting}



\clearpage

\section{List of all optional arguments for \texttt{pst-antiprism}}

\xkvview{family=pst-antiprism,columns={key,type,default}}


\bgroup
\raggedright
\nocite{*}
\printbibliography
\egroup

\printindex
\end{document}

