Struktura \dg dvojice se pou��v� jako n�vratov� hodnota funkce
"uzasna_funkce" a sdru�uje dv� hodnoty typu "float".
\ins c dvojice

Funkce \dg [struct dvojice] uzasna_funkce() si vezme jeden parametr 
"p" a vr�t� ve struktu�e "dvojice" dvojn�sobek a trojn�sobek 
tohoto parametru. 
\ins c uzasna_funkce
