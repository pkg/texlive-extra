%% $Id: pst-abspos-doc.tex 135 2009-09-27 12:33:57Z herbert $
\documentclass[11pt,english,BCOR10mm,DIV12,bibliography=totoc,parskip=false,smallheadings
    headexclude,footexclude,oneside]{pst-doc}
\usepackage[utf8]{inputenc}
\usepackage[absolute]{pst-abspos}

\let\pstFV\fileversion
\lstset{pos=l,wide=false,language=PSTricks,
    morekeywords={multidipole,parallel},basicstyle=\footnotesize\ttfamily}
%
\definecolor{myGray}{rgb}{0.9,0.9,0.9}
\newcommand\x{blah blub laber text nonsense }
\begin{document}

\title{\texttt{pst-abspos}}
\subtitle{A PSTricks package for using abolute or relative coordinates of a page; v.\pstFV}
\author{Herbert Vo\ss}
\docauthor{}
\date{\today}
\maketitle

\tableofcontents

\newpage
\pstSetAbsoluteOrigin
A Nonsense text line with \verb|\noindent|.

\section{Introduction}
\pstPutAbs[position=rb,unit=1pt](0.95\paperwidth,-10){\texttt{pst-abspos}}
The package \LPack{pst-abspos} allows absolute positioning of objects. In \TeX{} the
actual ''cursor'' position is the \Index{origin} of the \Index{paper plane}. This makes it difficult
to put objects into absolute positions. The only constant position is the
origin of the paper plane, the upper left corner, which is 1 inch right
and 1 inch down. This absolute PostScript origin is not the one for the textarea
of \TeX, because it adds the horizontal margins \Lcs{oddsidemargin} and for the first 
line \Lskip{parskip}. In the vertical direction there are added \Lskip{topskip},
\Ldim{headheight} and \Ldim{headsep}. This is a bit confusing because there are different
origins of the paper plane:
\begin{itemize}
\item the upper left corner of the paper \pstPutAbs[position=lt](0,0){\qdisk(0,0){5pt}%
		\raisebox{-2ex}{\red Absolute Paper Origin}}
\item the PostScript origin 1 inch from left and 1 inch from top
		\pstPutAbs[unit=1in,position=lC](1,-1){\qdisk(0,0){5pt}\red ~~PostScript Origin}
\item the \TeX{} origin, adding \verb|\oddsidemargin| or \verb|\evensidemargin| to 1 inch and
		 substract \verb|\topskip|, \verb|\headheight| and \verb|\headsep| from the top
		 \qdisk(pstTextOrigin){5pt}\rput[rC](pstTextOrigin){\red Text Origin~~}
\item the \TeX{} relative origin, which describes the actual ''cursor'' position, like here: \qdisk(0,0){5pt}\rput[lC](0,0){\red "Cursor" Origin}.
\end{itemize}

\section{Package Options}

\begin{tabularx}{\textwidth}{lX}
\Loption{notitlepage} & When there is no titlepage it is easier to run the
	initialisation macros. In this case the user has only to set the package option.\\
\Loption{absolute} & the origin is the upper left paper corner\\
\Loption{lowerleft} & the origin is mirrored to the lower left of the paper plane\\
\Loption{PostScript} & the origin is 1inch from the left and 1 inch from the top of the paper\\
\Loption{relative}   & the origin is the upper left text area
\end{tabularx}

Especially the \Loption{lowerleft} option may be useful. Together with the \Loption{absolute}
option the origin of the paper plane is at the lower left corner (see section~\ref{sec:lowerleft}).

\section{The Macros}
The package tries to set the origin immediately after a newpage. This is not possible
with a \Index{title} because of an additional \Lcs{newpage} of the \Lenv{titlepage} environment. 
In this case the user should controll the setting manually with
the macros

\noindent
\begin{tabularx}{\linewidth}{@{}lX@{}}
\Lcs{pstSetAbsoluteOrigin}    &  the new origin is the upper left corner of the paper\\
\Lcs{pstSetPostScriptOrigin}  & the new origin is 1 inch from left and 1 inch from top of the upper left corner of the paper\\
\Lcs{pstSetRelativeOrigin}\Largr{\CAny} & the new origin is the current point for $(0;0)$. All other values are relative to this one.\\
\Lcs{pstPutAbs}\OptArgs\Largr{\CAny}\Largb{object} &  puts any object depending to the option at $(x;y)$.
\end{tabularx}

The first two macros have to be used immediately after a newpage.

\subsection{Optional Parameters}
The \Lcs{pstPutAbs} macro can be called with all vaild \verb|pstricks| options and additionally 
with \Lkeyword{position}\verb+=position+, where \Larg{position} can be replaced by the usual placing
values \Lnotation{lt}, \Lnotation{lC}, \Lnotation{lB}, \Lnotation{lb}, \ldots\ of the well known \Lcs{rput} macro. Important is the
setting of the \Lkeyword{unit} parameter, because \LPack{pst-abspos} knows nothing about
the unit. A valid call may be like

\begin{Xverbatim}{\xLcs{pstPutAbs}}
\pstPutAbs[unit=1in,position=lC](1,-1){%
  \qdisk(0,0){5pt}\textcolor{red}{~PostScript Origin}}
\end{Xverbatim}

\section{Examples}
It is no problem to switch between the different origin settings when calling the
macros immediately aften a \Lcs{newpage}. If you want to set the origin at the ''cursor''
position, then the macro can be called just on ''cursor'' position.

\newpage
\pstSetAbsoluteOrigin
\section{Absolute Coordinates}
The point (0,0) is in the upper left corner and is set by the package option \Loption{absolute}.

\begin{BDef}
\Lcs{usepackage}\Largs{absolute}\Largb{pst-abspos}
\end{BDef}

\psset{unit=1in}%
\pstPutAbs(0,0){\qdisk(0,0){2pt}\rput(0,0){\red Origin}}Nonsense Text\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}%
\verb|\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}%
\verb|\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}%
\verb|\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}|
\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}
\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}%
\verb|\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}|
\verb|\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}|

\vspace*{\fill}
\parbox{\textwidth}{%
  \x\x\x\x\textcolor{red}{\texttt{\textbackslash pstPutAbs(3,0.5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,0.5)\}}}%
  \pstPutAbs(3,0.5){\qdisk(0,0){4pt}\blue Point(3,0.5)}\x\x\x\x%
  \texttt{\textcolor{red}{\textbackslash pstPutAbs(3,-5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,-5)\}}}\x\x\x%
  \pstPutAbs(3,-5){\qdisk(0,0){4pt}\blue Point(3,-5)}}

\clearpage
\pstSetRelativeOrigin(0,0)%

\section{Relative Coordinates}
The point (0,0) is set as the relative origin and is set by the package option
\Loption{relative}.

\begin{BDef}
\Lcs{usepackage}\Largs{relative}\Largb{pst-abspos}
\end{BDef}

This relative point is the beginning of the first textline, which depends to all margins.
It allows to place the relative origin elsewhere depending to the relative point \verb|(0,0)|.

\psset{unit=1in}%
\pstPutAbs(0,0){\qdisk(0,0){2pt}\rput(0,0){\red Origin}}
Nonsense Text\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}%
\verb|\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}%
\verb|\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}%
\verb|\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}|
\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}
\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}%
\verb|\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}|
\verb|\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}|

\vspace*{\fill}
\parbox{\textwidth}{%
  \x\x\x\x\textcolor{red}{\texttt{\textbackslash pstPutAbs(3,0.5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,0.5)\}}}%
  \pstPutAbs(3,0.5){\qdisk(0,0){4pt}\blue Point(3,0.5)}\x\x\x\x%
  \texttt{\textcolor{red}{\textbackslash pstPutAbs(3,-5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,-5)\}}}\x\x\x%
  \pstPutAbs(3,-5){\qdisk(0,0){4pt}\blue Point(3,-5)}}

\clearpage
\pstSetPostScriptOrigin%
\section{PostScript Origin}
The point (1in,-1in) is set as the absolue PostScript origin and is set by the package option
\Loption{relative}.

\begin{Xverbatim}{\xLcs{usepackage}\xLoption{absolute}}
\usepackage[PostScript]{pst-abspos}
\end{Xverbatim}

The coordinates (1in; -1in) have some historical
background and have nowadays no more real sense. If all horizontal and vertical 
margins inside \TeX{} are set to zero, then this point
is (0,0).

\psset{unit=1in}%
\pstPutAbs(0,0){\qdisk(0,0){2pt}\rput(0,0){\red Origin}}%
Nonsense Text\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}%
\verb|\pstPutAbs(1,-2){\qdisk(0,0){4pt}\red Point(1,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}%
\verb|\pstPutAbs(0,-2){\qdisk(0,0){4pt}\red Point(0,-2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}%
\verb|\pstPutAbs(0,-1){\qdisk(0,0){4pt}\red Point(0,-1)}|
\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}
\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}%
\verb|\pstPutAbs(1,-10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}|
\verb|\pstPutAbs(1,-10){\qdisk(0,0){4pt}\red Point(1,-10)}|\\

\vspace*{\fill}
\parbox{\textwidth}{%
  \x\x\x\x\textcolor{red}{\texttt{\textbackslash pstPutAbs(3,0.5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,0.5)\}}}%
  \pstPutAbs(3,0.5){\qdisk(0,0){4pt}\blue Point(3,0.5)}\x\x\x\x%
  \texttt{\textcolor{red}{\textbackslash pstPutAbs(3,-5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,-5)\}}}\x\x\x%
  \pstPutAbs(3,-5){\qdisk(0,0){4pt}\blue Point(3,-5)}}

\newpage
\makeatletter
\pst@lowerlefttrue
\makeatother
\pstSetAbsoluteOrigin

\section{Absolute Coordinates with an Origin in the Lower Left}\label{sec:lowerleft}
The point (0,0) is in the lower left corner and is set by the package option
\Loption{lowerLeft}. This is only useful for absolute coordinates. This is the reason why
this option should implie also the option \Loption{absolute}.


\begin{Xverbatim}{\xLcs{usepackage}\xLoption{lowerLeft}\xLoption{absolute}}
\Lcs{usepackage}\Largs{lowerLeft,absolute}\Largb{pst-abspos}
\end{BDef}


\psset{unit=1in}%
\pstPutAbs(0,0){\qdisk(0,0){4pt}\rput(0,0){\red Origin}}%
Nonsense Text\pstPutAbs(1,2){\qdisk(0,0){4pt}\red Point(1,2)}%
\verb|\pstPutAbs(1,2){\qdisk(0,0){4pt}\red Point(1,2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,2){\qdisk(0,0){4pt}\red Point(0,2)}%
\verb|\pstPutAbs(0,2){\qdisk(0,0){4pt}\red Point(0,2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,1){\qdisk(0,0){4pt}\red Point(0,1)}%
\verb|\pstPutAbs(0,1){\qdisk(0,0){4pt}\red Point(0,1)}|
\pstPutAbs(1,10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}
\pstPutAbs(1,10){\qdisk(0,0){4pt}\red Point(1,10)}%
\verb|\pstPutAbs(1,10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}|\\
\verb|\pstPutAbs(1,10){\qdisk(0,0){4pt}\red Point(1,10)}|\\

\vspace*{\fill}
\parbox{\textwidth}{%
  \x\x\x\x\textcolor{red}{\texttt{\textbackslash pstPutAbs(3,-0.5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,-0.5)\}}}%
  \pstPutAbs(3,-0.5){\qdisk(0,0){4pt}\blue Point(3,-0.5)}\x\x\x\x%
  \texttt{\textcolor{red}{\textbackslash pstPutAbs(3,5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,5)\}}}\x\x\x%
  \pstPutAbs(3,5){\qdisk(0,0){4pt}\blue Point(3,5)}}

\newpage
\pstSetPostScriptOrigin

\section{Absolute PostScript Coordinates with an Origin in the Lower Left}
The origin point (0,0) is in the lower left corner with 1in from the left and 1inch
from the bottom and is set by the package option
\Loption{lowerLeft}. This is only useful for \Index{absolute coordinate}s.

\begin{Xverbatim{\xLcs{usepackage}\xLoption{lowerLeft}\xLoption{PostScript}}
\usepackage[lowerLeft,PostScript]{pst-abspos}
\end{Xverbatim}


\psset{unit=1in}%
\pstPutAbs(0,0){\qdisk(0,0){4pt}\rput(0,0){\red Origin}}%
Nonsense Text\pstPutAbs(1,2){\qdisk(0,0){4pt}\red Point(1,2)}%
\verb|\pstPutAbs(1,2){\qdisk(0,0){4pt}\red Point(1,2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,2){\qdisk(0,0){4pt}\red Point(0,2)}%
\verb|\pstPutAbs(0,2){\qdisk(0,0){4pt}\red Point(0,2)}|\\
Nonsense Text\\
Nonsense Text\pstPutAbs(0,1){\qdisk(0,0){4pt}\red Point(0,1)}%
\verb|\pstPutAbs(0,1){\qdisk(0,0){4pt}\red Point(0,1)}|
\pstPutAbs(1,10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}
\pstPutAbs(1,10){\qdisk(0,0){4pt}\red Point(1,10)}%
\verb|\pstPutAbs(1,10){\scalebox{8}{\textcolor{myGray}{pst-abspos}}}|\\
\verb|\pstPutAbs(1,10){\qdisk(0,0){4pt}\red Point(1,10)}|\\

\vspace*{\fill}
\parbox{\textwidth}{%
  \x\x\x\x\textcolor{red}{\texttt{\textbackslash pstPutAbs(3,-0.5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,-0.5)\}}}%
  \pstPutAbs(3,-0.5){\qdisk(0,0){4pt}\blue Point(3,-0.5)}\x\x\x\x%
  \texttt{\textcolor{red}{\textbackslash pstPutAbs(3,5)\{\textbackslash qdisk(0,0)\{4pt\}\textbackslash blue Point(3,5)\}}}\x\x\x%
  \pstPutAbs(3,5){\qdisk(0,0){4pt}\blue Point(3,5)}}

\section{List of all optional arguments for \texttt{pst-abspos}}

\xkvview{family=pst-abspos,columns={key,type,default}}


\bgroup
\raggedright
\nocite{*}
\bibliographystyle{plain}
\bibliography{\jobname}
\egroup

\printindex
\end{document}


\end{document}
