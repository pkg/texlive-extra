# pst-magnetiocfield: creating magnetic field lines in 2D and 3D

Save the files pst-magneticfield.sty|pro|tex in a directory, which is part of your 
local TeX tree. The pro file should go into $TEXMF/dvips/pstricks/
Then do not forget to run texhash to update this tree.

pst-magneticfield needs pstricks, which should 
be part of your local TeX installation, otherwise get it from a 
CTAN server, http://mirror.ctan.org

PSTricks is PostScript Tricks, the documentation cannot be run
with pdftex, use the sequence latex->dvips->ps2pdf or
pdflatex with package auto-pst-pdf or xelatex or lualatex.

%% This program can be redistributed and/or modified under the terms
%% of the LaTeX Project Public License Distributed from CTAN archives
%% in directory macros/latex/base/lppl.txt.


%% $Id: README.md 582 2022-07-06 07:49:15Z herbert $

