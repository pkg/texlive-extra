\documentclass[11pt]{article}
\usepackage[a4paper,margin=2cm]{geometry}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[garamond]{mathdesign}
\usepackage{multido,animate,pst-math}
\usepackage[colorlinks=true]{hyperref}
\usepackage{pst-contourplot}
\date{14 juillet 2018}
\author{Manuel Luque}
\title{The algorithm``\textit{marching squares}'' for  PSTricks v\fileversion}
\begin{document}
\maketitle
\section{The command \textbackslash{psContourPlot[options](x1,y1)(x2,y2)}}
If you do not know ``\textit{marching squares}'', the article that Wikipedia devotes to him, very nicely illustrated, seems to me very complete :

\centerline{\url{https://en.wikipedia.org/wiki/Marching_squares}}

This is an adaptation of this algorithm to PSTricks, used in \verb+\psContourPlot[options]+\footnote{Its name comes from  Mathematica:ContourPlot. } and has the following options:
\begin{enumerate}
  \item \texttt{[function=])} : implicit function $f(x,y)$ of the curve  in algebraic or postscript mode, it should be noted that the postscript mode is the fastest;
  \item \texttt{(x1,y1)(x2,y2)} :  coordinates of the lower left corner and the upper right corner of the study frame, as for \verb+\psframe(x1,y1)(x2,y2)+;
  \item \texttt{[a=0.025]} : side of a (square) cell;
  \item \texttt{[grid=false]} : set to \texttt{true} to draw the cell grid;
  \item \texttt{[Fill=false]} : set to \texttt{true} to color the interior with the PSTricks option \texttt{[fillcolor]};
  \item \texttt{[ReverseColors=false]} :  coloring inside an object is only valid for one object (a circle for instance). If there are several objects (see the 2 examples of the metaballs) it is the outside which is colored. Set to \texttt{true} this boolean to correct the problem.
  \item \texttt{[ChoicePoints= liste de num�ros de points]} : here we place the points where there will be an arrow on the curve, we indicate a negative value if for the positive value the arrow is not in the desired direction;
  \item \texttt{[WriteData]} : boolean option allowing  to save the coordinates of the points, the name of the file can be chosen with the option \texttt{[FileName=PointsCurve]}.
\end{enumerate}
To solve the 2 ambiguous cases of the algorithm, I adopted the solution proposed by Xiaoqiang Zheng and Alex Pang :

\centerline{\url{https://classes.soe.ucsc.edu/cmps161/Winter14/papers/tensor/projects/contour/paper.pdf}}

A second command \verb+\psReadData[FileName=...]+ allow us to  draw a registered curve, the [Fill] option is not allowed.
\section{Examples}
\subsection{Circle}
\begin{center}
\begin{pspicture}[showgrid](-4,-4)(4,4)
\psContourPlot[algebraic,a=0.5,linecolor=red,grid,function=x^2+y^2-16,ChoicePoints=-4 120 -45,WriteData,FileName=circle,showpoints](-4,-4)(4,4)
\psline{<->}(0,4.5)(0,0)(4.5,0)
\uput[ul](0,0){$O$}
\uput[u](0,4.5){$y$}
\uput[r](4.5,0){$x$}
\end{pspicture}
\end{center}
\begin{verbatim}
\psContourPlot[algebraic,a=0.5,linecolor=red,grid,function=x^2+y^2-16,showpoints,
               ChoicePoints=-4 120 -45,WriteData,FileName=circle](-4,-4)(4,4)
\end{verbatim}
This grid contains 16 cells along the 2 axes, the side of each is 0.5 cm.

\subsection{Coloring inside an object}
\begin{center}
\begin{pspicture}[showgrid=false](-6,-4)(6,4)
\psContourPlot[unit=0.5,algebraic,a=0.4,linecolor=blue,Fill,fillcolor=red,function=x*(x^2+y^2)-10*(x^2-y^2),grid](-10,-8)(10,8)
\psline{<->}(0,4.5)(0,0)(5.5,0)
\uput[d](0,0){$O$}
\uput[u](0,4.5){$y$}
\uput[r](5.5,0){$x$}
\end{pspicture}
\end{center}
\begin{verbatim}
\psContourPlot[unit=0.5,algebraic,a=0.4,
               linecolor=blue,Fill,fillcolor=red,
               function=x*(x^2+y^2)-10*(x^2-y^2),grid](-10,-8)(10,8)
\end{verbatim}
\subsection{2D metaballs}
\begin{center}
\begin{animateinline}[controls,palindrome,
                     begin={\begin{pspicture}(-8,-4)(8,4)},
                     end={\end{pspicture}}]{5}% 5 image/s
\multiframe{50}{r=-2+0.08}{%
\psframe*(-6.4,-4)(6.4,4)
\pstVerb{/xC \r\space def
         /FonctionMetaballs {
          1 x xC sub dup mul y dup mul add sqrt div
          0.5 x xC add dup mul y dup mul add sqrt div
          add
          1 sub
         } def}%
\psContourPlot[unit=2,a=0.1,linewidth=0.025,linecolor=red,fillcolor=cyan,Fill,ReverseColors,function=FonctionMetaballs](-4,-2)(4,2)
\psdots(! xC 2 mul 0)(! xC neg 2 mul 0)}
\end{animateinline}
\end{center}
\begin{verbatim}
\begin{animateinline}[controls,palindrome,
                     begin={\begin{pspicture}(-8,-4)(8,4)},
                     end={\end{pspicture}}]{5}% 5 image/s
\multiframe{50}{r=-2+0.08}{%
\psframe*(-6.4,-4)(6.4,4)
\pstVerb{/xC \r\space def
         /FonctionMetaballs {
          1 x xC sub dup mul y dup mul add sqrt div
          0.5 x xC add dup mul y dup mul add sqrt div
          add
          1 sub
         } def}%
\psContourPlot[unit=2,a=0.1,linewidth=0.025,linecolor=red,fillcolor=cyan,Fill,ReverseColors,
               function=FonctionMetaballs](-8,-4)(8,4)
\psdots(! xC 2 mul 0)(! xC neg 2 mul 0)}
\end{animateinline}
\end{verbatim}
% 5 metaballs
\begin{center}
%     1/((x-0.0001)^2+(y-1)^2)^2+
%     1/((x-0.95)^2+(y-0.309)^2)^2+
%     1/((x+0.5878)^2+(y+0.809)^2)^2+
%     1/((x-0.5878)^2+(y+0.809)^2)^2+
%     1/((x+0.95)^2+(y-0.309)^2)^2
%     -17
\begin{pspicture}[showgrid](-4,-4)(4,4)
\psset{unit=2.5}
\pstVerb{/FonctionMetaballs {
         1 x 0.0001 sub dup mul y 1     sub dup mul add dup mul div
         1 x 0.95   sub dup mul y 0.309 sub dup mul add dup mul div add
         1 x 0.5878 sub dup mul y 0.809 add dup mul add dup mul div add
         1 x 0.5878 add dup mul y 0.809 add dup mul add dup mul div add
         1 x 0.95   add dup mul y 0.309 sub dup mul add dup mul div add
         17 sub
        } def}%
\psContourPlot[a=0.025,linecolor=red,fillcolor=cyan,Fill,ReverseColors,
               function=FonctionMetaballs](-4,-4)(4,4)
\psdots(0,1)(0.95,0.309)(-0.95,0.309)(-0.5878,-0.809)(0.5878,-0.809)
\pspolygon(0,1)(-0.95,0.309)(-0.5878,-0.809)(0.5878,-0.809)(0.95,0.309)
\end{pspicture}
\end{center}
\begin{verbatim}
% 5 metaballs
\begin{center}
%     1/((x-0.0001)^2+(y-1)^2)^2+
%     1/((x-0.95)^2+(y-0.309)^2)^2+
%     1/((x+0.5878)^2+(y+0.809)^2)^2+
%     1/((x-0.5878)^2+(y+0.809)^2)^2+
%     1/((x+0.95)^2+(y-0.309)^2)^2
%     -17
\begin{pspicture}[showgrid](-4,-4)(4,4)
\psset{unit=2.5}
\pstVerb{/FonctionMetaballs {
         1 x 0.0001 sub dup mul y 1     sub dup mul add dup mul div
         1 x 0.95   sub dup mul y 0.309 sub dup mul add dup mul div add
         1 x 0.5878 sub dup mul y 0.809 add dup mul add dup mul div add
         1 x 0.5878 add dup mul y 0.809 add dup mul add dup mul div add
         1 x 0.95   add dup mul y 0.309 sub dup mul add dup mul div add
         17 sub
        } def}%
\psContourPlot[a=0.05,linecolor=red,fillcolor=cyan,Fill,ReverseColors,
               function=FonctionMetaballs](-4,-4)(4,4)
\psdots(0,1)(0.95,0.309)(-0.95,0.309)(-0.5878,-0.809)(0.5878,-0.809)
\pspolygon(0,1)(-0.95,0.309)(-0.5878,-0.809)(0.5878,-0.809)(0.95,0.309)
\end{pspicture}
\end{verbatim}
\subsection{The field lines of an Hertzian dipole}
\begin{center}
\begin{pspicture}[showgrid](-5,-5)(5,5)
\pstVerb{/ti 0 def /k0 2 PI mul def}%
\multido{\rc=-1.1+0.2}{11}{
\psContourPlot[unit=5,a=0.025,linewidth=0.01,linecolor={[rgb]{0 0.5 0}},
		   function=/ri x dup mul y dup mul add sqrt k0 mul def
                    /theta x y atan def
		   ri ti sub COS ri ti sub SIN ri div add theta sin dup mul mul \rc\space sub](-1,-1)(1,1)
}
\end{pspicture}
\end{center}
\begin{verbatim}
\pstVerb{/t 0 def /k0 2 PI mul def}%
\multido{\rc=-1.1+0.2}{11}{
\psContourPlot[unit=5,a=0.025,linewidth=0.01,linecolor={[rgb]{0 0.5 0}},
		   function=/r x dup mul y dup mul add sqrt k0 mul def
                    /theta x y atan def
		   r t sub COS r t sub SIN r div add theta sin dup mul mul \rc\space sub](-1,-1)(1,1)}
\end{verbatim}
\section{Complements}
Examples are included in the documentation, but you will find other examples on the blog :

\centerline{\url{http://pstricks.blogspot.com/}}

\noindent and as an application dedicated to physics, the drawing of magnetic field lines of parallel wires :

\centerline{\url{http://pstricks.blogspot.com/2018/07/champs-magnetiques-crees-par-des-fils.html}}
\end{document}
