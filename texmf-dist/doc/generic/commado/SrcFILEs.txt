
 *File List*
 commado.RLS  2015/11/16  r0.11a doc. `AllOf' in dowith
 commado.sty  2012/11/30  v0.11  iterate on CSL (UL)
 filesdo.sty  2012/11/27  v0.1   iterate on files (UL)
 commado.tex  2015/11/16   --    documenting commado.sty
srcfiles.tex  2015/11/16   --    file infos -> SrcFILEs.txt
 ---USED.---   --  -- --   --     ----
fifinddo.sty  2012/11/17  v0.61  filtering TeX(t) files by TeX (UL)
 makedoc.sty  2012/08/28  v0.52  TeX input from *.sty (UL)
niceverb.sty  2015/11/09  v0.61  minimize doc markup (UL)
texlinks.sty  2015/07/20  v0.83  TeX-related links (UL)
 makedoc.cfg  2013/03/25   --    documentation settings
mdoccorr.cfg  2012/11/13   --    `makedoc' local typographical corrections
 ***********

 List made at 2015/11/16, 06:27
 from script file srcfiles.tex

