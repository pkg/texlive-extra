This archive contains a MusiXTeX extension library
musixtnt.tex and C source code, binaries for Windows (32
bit and 64 bit) and MacOSX, and documentation for a program
msxlint.

musixtnt.tex provides a macro \TransformNotes that
enables transformations of the effect of notes
commands such as \notes. In general, the effect of
\TransformNotes{input}{output} is that notes commands in
the source will expect their arguments to match the input
pattern, but the notes will be typeset according to the
output pattern. An example is extracting single-instrument
parts from a multi-instrument score.

msxlint detects incorrectly formatted notes lines in a
MusiXTeX source file. This should be used before using
\TransformNotes.

To install:

Create texmf/tex/generic/musixtnt in a local or personal
texmf tree and move musixtnt.tex into it.

Create texmf/doc/generic/musixtnt and move there the files
in the doc directory.

Update the file-name database as required by your TeX
installation.

If your platform is UNIX-like, process the
musixtnt-<version>.tar.gz file as usual (tar zxvf ...,
./configure, make, make install).

If your platform is Windows, move the executable msxlint.exe
from either the 32bit or the 64bit folder to a folder on the
executable PATH.

If your platform is Mac OS-X, install the executable from
the macosx directory.

musixtnt.tex, utils.c and msxlint.c are licensed under the
GPL version 2 or (at your option) any later version.

Please report bugs and experience (good or bad) using these
programs to me.

Bob Tennent
rdt(at)cs(dot)queensu(dot)ca
2016-01-30
