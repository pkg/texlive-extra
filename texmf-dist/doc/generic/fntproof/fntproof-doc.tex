\documentclass[draft]{article}
\usepackage{mflogo}
\addtolength\textwidth{.5in}
\addtolength\oddsidemargin{-.25in}
\usepackage{verbatim}
\newcommand\prog[1]{\texttt{#1}}
\newcommand\file[1]{\texttt{#1}}
\newcommand\env[1]{\texttt{#1}}
\renewcommand\"{\verb"}

\newcommand\meta[1]{$\langle$\textit{#1}$\rangle$}
\title{How to use \file{fntproof.tex}\\ and \file{testfont.tex}}
\author{Dan Luecking\thanks{\copyright 2010
 by Daniel H. Luecking. This software is public domain. If you make any
 changes to this document, please remove or modify this copyright notice
 before distributing your version, preferably with a different name. 
 This documents version 1.0 of \file{fntproof}}}

\begin{document}

\maketitle

\begin{abstract}
 The macro file \file{fntproof.tex} is a noninteractive version of
 D.~E.~Knuth's \file{testfont.tex}. Instead of prompting for font names
 and commands, the user supplies them on the command line, or in a file.
\end{abstract}


\section{Introduction}

I used to often run \prog{tex} on \file{testfont.tex}, which prompts for
a font name and prompts for a command. Often, after ending the session,
I would want to run almost the same command on almost the same font and
would have to type almost everything over again, or else deal with my
\prog{tex} program's rather primitive line recall/editing mechanism.

Since I run \prog{tex} in a command shell which has better command line
recall and rather better line editing capabilities than my \prog{tex} 
has, I wrote \file{fntproof.tex} to give me the same capabilities as
\file{testfont.tex}, but allow me to specify the font and the commands
on the command line.

This is the documentation of \file{fntproof}. Since \file{testfont.tex}
has no documentation outside of ``The \MF book'', I am also also 
documenting it. The reader is assumed to have some minimal knowledge of 
plain \TeX{}, especially the ``\"\font"'' command.

Apart from the fact that \file{fntproof} can read the fontname from the 
command line, there are  few differences between \file{testfont.tex} and 
\file{fntproof.tex}:

\begin{itemize}
  \item In \file{testfont}, the \"\init" command prompts for a fontname, 
        while \file{fntproof} arranges to read one typed on the command 
        line or in a file.

  \item In \file{testfont}, \"\mixture", \"\alternation" and \"\series" 
        issue three prompts, while in \file{fntproof} they read three 
        arguments.

  \item In \file{testfont}, \"\alphabet" and \"\ALPHABET" issue a
        prompt, while in \file{fntproof} they read an argument.

  \item In a \LaTeX{} document, \file{testfont} redefines the \"\table"
        command, destroying the \env{table} environment, while
        \file{fntproof} tries to detect \LaTeX{} and leave \"\table" 
        alone. The command \"\fonttable" then takes the place of 
        \file{testfont}'s \"\table" command.

  \item In \file{testfont}, the primitive \TeX{} command \"\fontname" is 
        also overwritten, while \file{fntproof} uses a different 
        internal name for the same purpose. This require a couple of 
        other minor differences to be discussed later.

  \item There are a few useful commands added.
\end{itemize}

Like \file{testfont}, \file{fntproof} can print a table of the font's 
characters and a bit of sample text and run other tests. It has 
noninteractive versions of all the interactive commands allowed by 
\file{testfont}.


\section{Example command lines}\label{examples}

\begin{itemize}
 \item  Produce sample text of cmr10:
\begin{verbatim}
    tex fntproof cmr10 \text\bye
\end{verbatim}

 \item  Produce a table of cmr10 and sample text of cmti10 (pdf output).
        Note the use of ``\"\init"'' to change to the new font:
\begin{verbatim}
    pdftex fntproof cmr10 \table \init cmti10 \text\bye
\end{verbatim}

 \item  Produce a table and a test of digits for cmr10, only a table for
        cmti10 at 12pt:
\begin{verbatim}
    tex fntproof cmr10 \table\digits\init{cmti10 at 12pt}\table\bye
\end{verbatim}

 \item  Obtain a list of available commands and their meanings
\begin{verbatim}
    pdftex fntproof \help \end
\end{verbatim}
\end{itemize}

Your command line interpreter may require you to quote or escape
backslashes or other of its special characters in these examples. I am
able to run all of these as-is in a command window under Windows XP 
using TeX Live 2008--2010. Of course the output appears in 
\file{fntproof.dvi} or \file{fntproof.pdf}.


\section{The commands}

A font can be specified by its TFM filename followed by a space (or a 
line end). Font filenames are allowed to contain most of \TeX{}'s 
special characters when \file{fntproof} is used. However, they cannot 
contain spaces or other whitespace characters, and they cannot begin 
with a left brace~(\"{"). There are even more limitations on the TFM 
filename typed at \file{textfont}'s  prompt, as most special 
\TeX{} characters cause some problem or other.

A command applies to the font most recently specified. The following
commands are available. They are the same as those provided by the
interactive version, \file{testfont.tex}, except they do not prompt for
input, but require any arguments to follow the command.

\begin{itemize}
 \item \"\sample" \"\table" \"\text"\\
     \"\sample" produces a table and some sample text; \"\table" produces
     only the table, \"\text" only the text.

 \item \"\mixture"\\
      This takes three characters as arguments. The first character
      (the ``background'') is mixed among all the characters from the
      second argument to the third. For example, ``\"\mixture Xaz"'' produces
\begin{verbatim}
    XaXXaaXXXaaaXa
    XbXXbbXXXbbbXb
    ...
\end{verbatim}
      and so on, until z.

 \item  \"\alternation"\\
        This is like \"\mixture", except ``\"\alternation Xaz"''
        produces lines like
\begin{verbatim}
    XaXaXaXaXaXaXaXaX
    XbXbXbXbXbXbXbXbX
    ...
\end{verbatim}
      and so on.

 \item  \"\series"\\
        Like the previous two it takes 3 arguments, but produces a
        single line containing all the characters in the sequence. Thus,
        ``\"\series Xaz"'' produces
\begin{verbatim}
    XaXbXcXdXeXfXgXhXiXjXkXlXmXnXoXpXqXrXsXtXuXvXwXxXyXzX
\end{verbatim}

 \item  \"\alphabet" \"\ALPHABET"\\
        These take one character as argument. It is the ``background''
        and is mixed among all the lowercase, respectively uppercase
        letters. So ``\"\ALPHABET x"'' is almost like
        ``\"\series  xAZ"'', but also includes the characters \AE, \OE,
        and \O, while ``\"\alphabet X"'' is like ``\"\series Xaz"'', but
        also includes \ae, \oe, \o, and \ss. 

 \item  \"\lowers" \"\uppers" \"\digits" \"\names"
        \"\punct"\\
        These perform comprehensive tests of lowercase, uppercase,
        digits, names and punctuation. For example, \"\lowers" is
        like
\begin{verbatim}
    \alphabet a \alphabet b ...
\end{verbatim}

        and \"\digits" is like
\begin{verbatim}
    \series 009 \series 109 \series 209 ...
\end{verbatim}

 \item  \"\bigtest" \"\math"\\
        \"\bigtest" includes (more or less) all the previous
        commands. \"\math" produces a large set of tests of math
        (the font should be a math alphabetic font like cmmi10). 

 \item  \"\help" \"\init" \"\noinit"\\
        \"\help" produces a summary of available commands on the screen. 

        \"\init" prepares for the next font. In \file{testfont}, it 
        issues a prompt for a fontname and need not be invoke for the 
        first font, which is prompted for immediately after input. 
        In \file{fntproof}, it reads the following fontname, and it need 
        not be used before the first font (see the examples in
        section~\ref{examples}), unless the filename begins with a 
        \TeX{} special character. 

        \"\noinit" turns off this automatic initialization. Use it when 
        you want some text before the first font test, or if you want 
        the current font to be used. Its syntax differs between 
        \file{fntproof} and \file{testfont}, see below.
\end{itemize}

Note: In \file{fntproof}, \"\init" examines the first following
nonspace character. If that is a left brace (i.e., ``\"{"'')
\"\init" assumes the font name is contained between that left brace
and the first matching right brace~(``\"}"'').
Multi-part font specifications (such as ``\"cmr10 at 12pt"'') must be
enclosed in braces, and this \emph{must} be preceded by \"\init".

For the commands that take character arguments, the actual characters
might not be printed, especially in symbol fonts. Instead, the glyphs in
the corresponding positions will appear. Most printable ASCII characters
can be used in the argument, but not ``\"{"'', ``\"}"'', ``\"\"'' or space. In
place of a literal character one can supply a numerical position in the
font. This command:
\begin{verbatim}
    \mixture X{#97}{#122}
\end{verbatim}
has the same effect as this one:
\begin{verbatim}
    \mixture Xaz
\end{verbatim}
The numeric part of the argument (everything after after the \"#") can
be (probably) any valid \TeX{} number. Your command line interpreter may
require you to escape or ``quote'' the double quote marks which are used
in \TeX{} for hexadecimal, the single quote marks used for octal or the
back quote used in character code notation.

A numerical argument must be enclosed in braces: a ``\"#"'' not in braces
(or alone in braces) will be taken literally. For example,
\begin{verbatim}
    \mixture #120az
\end{verbatim}
would read ``\"#"'', ``\"1"'' and ``\"2"'' as the arguments and the rest,
``\"0az"'', as plain \TeX{} code (i.e., characters to be printed).

It should be noted that the implementation of \"\series" does not 
allow any ligatures or kerns between adjacent characters. This is also 
true of the commands based on \"\series": \"\alphabet", \"\ALPHABET", 
\"\lowers", \"\uppers", and \"\digits". It is true for both 
\file{testfont} and \file{fntproof}, which use the same internal 
implementation. If one wants to test these properties of a font, one 
must type in ones own text (see the following section).

Another limitation: in the ``\"\alphabet"'' and ``\"ALPHABET"'' 
commands, if the font has an encoding other than OT1, then the 
characters \ae, \oe, \o, \ss, \AE, \OE, and \O{} may not appear as 
advertised, since the implementation accesses them by position 
number. For PostScript fonts, a black box may appear where these 
characters should be.

In addition to the commands listed, which are common to \file{fntproof} 
and \file{testfont}, \file{fntproof} supplies the following extra 
commands, not available in \file{testfont}.  

\begin{itemize}
\item \"\initcurrentfont"\\
    Use this to perform the functions of \"\init" on the current font. 
    This would normally be preceded by commands selecting the desired 
    font. See section~\ref{latex} for a \LaTeX{} example.

\item \"\noinit"\\
    In \file{testfont}, the only way to use this is to say 
    \"\let\noinit=!" \emph{before} inputting \file{testfont.tex}. This 
    cancels the immediate prompt for a font filename. But in 
    \file{fntproof}, ``\"\noinit"'' can also be used immediately 
    \emph{after} inputting \file{fntproof}. Either use cancels 
    \file{fntproof}'s assumption that the first following word is the 
    name of the font to be selected. This allows ordinary text to be 
    inserted before selecting a font. After this, the command \"\init" 
    allows a font to be specified. 

\item \"\headersfalse" \"\headerstrue" \"\theheader"\\
    A side effect of the \"\init" command (including one implicitly
    invoked by a font name following the input of \file{fntproof}) is 
    the printing of a single header line similar to the following:
\begin{verbatim}
    Test of cmti10 on July 31 at 1244
\end{verbatim}
    Use ``\"\headersfalse"'' to force the omission of all such headers 
    for any subsequent font. Use ``\"\headerstrue"'' to resume the 
    default behavior. In any circumstance, the header text can be 
    printed using ``\"\theheader"''.

\item \"\thisfont" \"\getthisfont"\\ 
    The macro ``\"\thisfont"'' produces the name of the font most 
    recently specified by an explicit or implicit \"\init" or by
    \"\initcurrentfont". If no such initialization was done,  
    ``\"\getthisfont"'' will cause \"\thisfont" to be defined to the 
    text produced by the command sequence \"\fontname\font" (i.e., the 
    tfm filename of the current font---plus any ``\texttt{scaled}'' or 
    ``\texttt{at}'' clause).

\item \"\fonttable"\\
    As already mentioned, this is the alternative to \"\table" that must 
    be used in \LaTeX{}. It can also be used in plain \TeX{}.

\end{itemize}


\section{Other \TeX{} commands}

\TeX{} commands besides the above can be inserted at any point except
between \"\init" and the next fontname. No guarantee is given that they
they will have the expected effect. Text will be printed in the whatever 
font is current. No text should be typed between ``\file{fntproof}'' and 
the first fontname or else it will be mistaken for a font (but see 
\"\noinit" above and examples of its use below).

If the command line does not end in \"\end" or \"\bye", \TeX{} will wait
for more input in its usual way, with a ``\texttt{*}'' prompt. If additional
commands from the above list are entered (except \"\init"), they will be
applied to the previously named font. Moreover, a new font name can be
entered (after \"\init"). Of course, any plain \TeX{} commands can be
entered. The session can be ended by entering \"\end" or \"\bye".


\section{Use as a macro file}

Like \file{testfont.tex}, \file{fntproof.tex} allows the commands to be
used in a file rather than interactively, or on the command line. One
can do this by writing ``\"\let\noinit=!"'' before inputting
\file{testfont} or \file{fntproof}. This is somewhat more useful for
\file{fntproof} than for \file{testfont} as \emph{all} the \file{fntproof}
commands are noninteractive.

Unlike \file{testfont}, \file{fntproof} also allows you to simply type
``\"\noinit"'' \emph{immediately after} inputting it, for the same
effect. For example, with \file{testfont} one would initialize the font
oneself and then issue commands:
\begin{verbatim}
    \let\noinit=! \input testfont
    Ordinary text in default font.
    \def\fontname{cmti10}\startfont \table \text \bye
\end{verbatim}
Note that ``initializing a font'' after \file{testfont} is loaded 
amounts to defining \"\fontname" and then using the \"\startfont" 
command. The above example will produce a table and sample text of 
cmti10. Note that  \"\startfont" alone will not work.  If both steps are 
omitted, the \"\table" command will build a table of the current font, 
but certain preparatory steps will not be taken and some commands (but 
not \"\table") will produce errors.

The corresponding \file{fntproof} method would be the following. Note 
that this could also be typed on the command line (all on one line) 
after the tex command (in that case the ``\"\input"'' would be omitted):
\begin{verbatim}
    \input fntproof \noinit
    Ordinary text in default font.
    \init cmti10 \table \text \bye
\end{verbatim}
Note that \file{testfont.tex} causes the \TeX{} primitive command
\"\fontname" to be redefined. The macros in \file{fntproof} do not do 
this, but instead use \"\thisfont" for the same internal purposes as 
\file{testfont} uses \"\fontname".

If one wanted to initialize a font immediately (with no intervening
text in some other font), even this \"\noinit" hack is unnecessary:
\begin{verbatim}
    \input fntproof cmr10 \table \text \bye
\end{verbatim}
This would perform the auto-\"\init" on cmr10. In this case, any
\"\headersfalse" command would have to be put just before the font name.
Otherwise, it need only come before the next \"\init" command.

In \file{testfont}, a command like \"\mixture" would produce three
prompts to get the character or numeric arguments. For the corresponding
\file{fntproof} commands, these arguments must be supplied and no
interactive prompt occurs. One can get around these interactive prompts
in \file{testfont}, but it is rather inconvenient:

\begin{itemize}
\item with \file{testfont}:
\begin{verbatim}
\let\noinit! \input testfont
Some text in default font. Here is a mixture in cmti:
\chardef\background`X
\chardef\starting`a
\chardef\ending`z
\def\fontname{cmti10}\startfont
\domix\mixpattern% \altpattern for the effect of \alternation
\bye
\end{verbatim}

\item with \file{fntproof}:
\begin{verbatim}
\input fntproof \noinit
Some text in default font. Here is a mixture in cmti:
\init cmti10 \mixture Xaz \bye
\end{verbatim}
\end{itemize}

For completeness, here are the other ways to avoid the interactive
prompt when using \file{testfont} (right), compared to each of the
\file{fntproof} commands (left).

\begin{itemize}
 \item \"\series Xaz" $\Rightarrow$ \"\chardef\background`X\!\doseries{`a}{`z}\par"
 \item \"\alphabet X" $\Rightarrow$ \"\chardef\background`X\complower"
 \item \"\ALPHABET X" $\Rightarrow$ \"\chardef\background`X\compupper"
\end{itemize}

In the \"\doseries" command, the arguments can be numbers:
\"\doseries{97}{122}" is the same as \"\doseries{`a}{`z}". You can use
numbers in the \"\chardef" commands. For example
``\"\chardef\background88 "'' is the same as ``\"\chardef\background `X"''. 
Note: a space (or end-of-line) after explicit digits is almost
mandatory to terminate the number before any following macros are
expanded.

Instead of ``\"\init" \meta{tfmname}'', one can use normal font 
selection commands, followed by ``\"\initcurrentfont"'', and then the 
testing commands (e.g., \"\fonttable", \"\text"). For example,
\begin{verbatim}
    \input fntproof 
    \font\x=cmdunh10 at 12pt \x 
    \initcurrentfont \table \text \bye
\end{verbatim}
See the next section for an example in \LaTeX{}. To do this with 
\file{testfont} would require the following incantations:
\begin{verbatim}
    \let\originalfontname\fontname
    \let \noinit=! 
    \input testfont 
    \font\x=cmdunh10 at 12pt \x 
    \edef\fontname{\originalfontname\font}%
    \startfont  \table \text \bye
\end{verbatim}

Note that \"\init", \"\initcurrentfont" and some of the testing commands 
change some \TeX{} parameters. This could affect the typesetting of the 
rest of the document, so it might be wise to enclose a session of font 
testing in a group, if your document contains other material.

Note also that the mere loading of \file{fntproof} or \file{testfont} 
invokes the following settings:
\begin{verbatim}
    \tracinglostchars=0
    \tolerance=1000
    \raggedbottom
    \parindent=0pt
    \hyphenpenalty=200
    \doublehyphendemerits=30000
\end{verbatim}
These are suitable for the running of tests on fonts. If the rest of 
your document needs different parameters, it is your responsibility to 
reset these afterward. There are also numerous internal macros used by 
both \file{testfont} and \file{fntproof} that are not protected by 
having \texttt{@} in there name. This is another reason to input the 
macro file and use the testing macros inside a group. 

Finally, \file{testfont} turns off page numbering (in plain \TeX) when loaded.


\section{Use with \LaTeX{}}\label{latex}

In \LaTeX{}, printable text has to come after \"\begin{document}" so a
minimal command line could be (typed without pressing [Enter] or [Return]):
\begin{verbatim}
    latex fntproof \noinit \documentclass{article}\begin{document}
    \init cmr10 \fonttable \end{document}
\end{verbatim}

Thus with \LaTeX{}, \file{fntproof} might better be used in a file. Note 
also that \"\fonttable" is used instead of \"\table" to avoid destroying 
\LaTeX{}'s table environment. For example,
\begin{verbatim}
    \documentclass[12pt]{article}
    \begin{document}
    \input fntproof 
    \fontencoding{T1}\fontfamily{bch}\selectfont
    \initcurrentfont
    \fonttable
    \end{document}
\end{verbatim}
This would produce a table of the Bitstream Charter font..

The above font selection commands ultimately load the TFM file 
\texttt{bchr8t} magnified to \texttt{12pt}. Thus, the header
text actually looks something like this:
\begin{verbatim}
    Test of bchr8t at 12.0pt on October 7, 2010 at 1649
\end{verbatim}
Moreover, ``\"\thisfont"'' will acquire the definition ``\texttt{bchr8t 
at 12.0pt}''.

I have not tested \file{testfont.tex} much in \LaTeX{}. After the 
\"\let\noinit=!" hack, inputting \file{testfont} causes an
error because \"\nopagenumbers" is undefined. Then the command
\"\table", \"\text", or \"\sample" cause an error because \"\sevenrm" is 
undefined, but otherwise proceed correctly.


\section{History}

   2009/05/05 -- first version\\
   2009/07/31 -- add \"\noinit", \"\ifheaders", \"\theheader"\\
   2009/11/12 -- revised documentation (comments)\\
   2010/01/03 -- further documentation revisions\\
   2010/10/11 -- documentation moved to separate file, added
   \"\initcurrentfont" and \"\getthisfont".

\bigskip

\section{Appendix}

The output of \file{fntproof}'s \"\help" command:
\begin{verbatim}
=== Main commands ==============
\init switches to another font;
\end or \bye finishes the run;
\table prints the font layout in tabular format;
\fonttable must be used instead of \table in LaTeX;
 \text prints a sample text, assuming TeX text font conventions;
\sample combines \table and \text;
\mixture mixes a background character with a series of others;
\alternation interleaves a background character with a series;
\alphabet prints all lowercase letters within a given background
\ALPHABET prints all uppercase letters within a given background
\series prints a series of letters within a given background;
\lowers prints a comprehensive test of lowercase;
\uppers prints a comprehensive test of uppercase;
\digits prints a comprehensive test of numerals;
\math prints a comprehensive test of TeX math italic;
\names prints a text that mixes upper and lower case;
\punct prints a punctuation test;
\bigtest combines many of the above routines;
\help repeats this message;
and you can use ordinary TeX commands (e.g., to \input a file).
=== More commands ==============
\noinit turns off automatic initialization of the first word as a font;
\headersfalse \headerstrue turn off/resume printing of header text;
\theheader print the header text;
\thisfont print the name of font specified by \init;
\getthisfont define \thisfont to be the current font;
==================================
\end{verbatim}

\bigskip
\filbreak
\noindent The output of \file{testfont}'s \"\help" command:
\begin{verbatim}
\init switches to another font;
\end or \bye finishes the run;
\table prints the font layout in tabular format;
\text prints a sample text, assuming TeX text font conventions;
\sample combines \table and \text;
\mixture mixes a background character with a series of others;
\alternation interleaves a background character with a series;
\alphabet prints all lowercase letters within a given backgroun
\ALPHABET prints all uppercase letters within a given backgroun
\series prints a series of letters within a given background;
\lowers prints a comprehensive test of lowercase;
\uppers prints a comprehensive test of uppercase;
\digits prints a comprehensive test of numerals;
\math prints a comprehensive test of TeX math italic;
\names prints a text that mixes upper and lower case;
\punct prints a punctuation test;
\bigtest combines many of the above routines;
\help repeats this message;
and you can use ordinary TeX commands (e.g., to \input a file).
\end{verbatim}

\bigskip
\filbreak
\noindent Knuth's original \file{testfont.tex} comment:\\
\texttt{\% A testbed for font evaluation (see The METAFONTbook, Appendix H)}

\end{document}

