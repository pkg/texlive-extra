# README #
Package pst-calculate defines two macros
which uses the capabilities
from LaTeX3 for calculating the values of 
functions. It works only for LaTeX and not
for  real TeX documents.

%% This file is distributed under the terms of the LaTeX Project Public
%% License from CTAN archives in directory  macros/latex/base/lppl.txt.
%% Either version 1.3 or, at your option, any later version.

Herbert Voß <hvoss@tug.org>