\input musixtex
\input musixsty
\hsize=7.275in
\vsize=9.5in  
\hoffset=8.5in\advance\hoffset-\hsize\divide\hoffset2\advance\hoffset-1.0in
\voffset=11in\advance\voffset-\vsize\divide\voffset2\advance\voffset-1.0in
\input soul.sty
\sodef{\so}{}{0.15em}{0.5em}{0.5em}
\def\writebarno{\llap{\ninebf\the\barno\barnoadd}}%
\def\shiftbarno{1.5\Interligne}%
\def\raisebarno{5\internote}%
\def\tr#1{\zcn{#1}{\ppff tr}}
\def\trt#1{\loffset{0.65}{\shake{#1}}\roffset{0.65}{\mordent{#1}}}
\input musixplt
\input musixps
\def\psslurhgt{1} 
\setclef1\bass\setbassclefsymbol1\bassoct
\setclef5\treble\settrebleclefsymbol5\trebleoct
\catcodesmusic
\setname1{Bass Recorder}
\setname2{Tenor II Recorder}
\setname3{Tenor I Recorder}
\setname4{Alto Recorder}
\setname5{Soprano Recorder}
\advance\parindent by 18ex
\fulltitle{\BIgtype\sc\so{Concerto Grosso}}
\subtitle{\frtrm Opus~3, Number~4}
\author{Francesco Geminiani \sc(1687--1762)}
\othermention{Arranged for SATTB Recorders}
\maketitle
\makeatletter
\def\outmorceau{\shipout\vbox{\vbox to \vsize{\vss\pagecontents\vss}\line{%
\ifnum\pageno=1\relax\else\hss\rm--\ \number\pageno\ --\hss\fi}}%
 \global\advance\count0 by 1\relax
 \ifnum\outputpenalty>-20000 \else\dosupereject\fi}%
\makeatother
\generalsignature{-2}
\transpose3
\relativeaccid
\instrumentnumber5
\songbottom{1}\songtop{5}
\generalmeter{\allabreve}%
\stafftopmarg=6.5\Interligne%
\staffbotmarg=6.5\Interligne%
\interinstrument=2\internote
\setinterinstrument{4}{3\internote}
\startbarno=1
\nobarnumbers
\startpiece\addspace\afterruleskip%
\systemnumbers%
\znotes&&&&\uptext{\bf\kern-4ex Andante}\en
\anotes\ha D&\ha d&\ha f&\ha{h}&\qup d\qs\cca e\en
\anotes\hp&\qa{kh}&\hp&\qlp k\qs\cca l&\qlp f\qs\cca g\en
\bar%2
\anotes\qup D\qs\cca E&\qa{fh}&\qa k\qs\ibbl2k{-3}\qb2{kj}\tql2i&\qlp m\qs\cca n&\qa{hd}\en
\anotes\qup F\qs\cca G&\qa{dh}&\qa{hd}&\qa o\qs\ibbl3o{-3}\qb3{on}\tql3m&\qlp k\qs\cca k\en
\bar%3
\anotes\qa{HH}&\ha a&\qlp h\qs\cca h&\qlp l\qs\cca l&\qlp k\qs\cca j\en
\anotes\qa{DK}&\ha d&\ha h&\tr{o}\ha{^m}&\qa j\ibl4i{-3}\qb4{.i}\tbbl4\tql4h\en
\bar%4
\anotes\qa{GG}&\ha d&\qlp g\qs\cca g&\qa{nk}&\qlp i\qs\cca i\en
\anotes\qa{CJ}&\ha c&\ha g&\tinynotesize\qu m\tr{n}\ha l&\tr{k}\qlp i\qs\cca h\en
\bar%5
\anotes\ha F&\qa c\ibl1f2\qb1{.f}\tbbl1\tql1g&\ibl2f2\qb2{.f}\tbbl2\qb2g\qb2{.h}\tbbl2\tql2i&\ha m&\ha h\en
\anotes\ibu0F2\qb0{.F}\tbbu0\qb0G\qb0{.H}\tbbu0\tqu0I&\qa{hf}&\qa{jj}&\qp\qa m&\qp\qa h\en
\bar%6
\anotes\qlp J\qs\cca J&\qa{ce}&\qa{jc}&\qlp l\qs\cca l&\qlp g\qs\cca g\en
\anotes\qa{IH}&\qa{gj}&\hp&\qa l\tr{o}\qa{^m}&\qa g\tr{k}\qa h\en
\bar%7
\anotes\ha G&\qa i\ibl1g2\qb1{.g}\tbbl1\tql1h&\ibl2g2\qb2{.g}\tbbl2\qb2h\qb2{.i}\tbbl2\tql2j&\ha n&\ha i\en
\anotes\ibu0G2\qb0{.G}\tbbu0\qb0H\qb0{.I}\tbbu0\tqu0J&\qa{ig}&\qa{kk}&\qp\qa n&\qp\qa i\en
\bar%8
\anotes\qlp K\qs\cca K&\ibu1f0\qb1{.d}\tbbu1\qb1e\qb1{.f}\tbbu1\tqu1d&\qa{kd}&\qlp m\qs\cca m&\qlp h\qs\cca h\en 
\anotes\qlp J\qs\cca J&\qa{hh}&\qa{eh}&\qa l\itieu3h\qa h&\ibl4h{-3}\qb4{.h}\tbbl4\qb4g\qb4{.f}\tbbl4\tql4e\en
\bar%9
\anotes\ha I&\ha f&\ha{.d}&\ttie3\ibl3g0\qb3{.h}\tbbl3\qb3g\qb3{.h}\tbbl3\tqb3i&\ha d\en
\anotes\ha I&\qa{gd}&\qa{*g}&\tr{k}\ha g&\ha d\en
\Setvolta{1}%
\bar%10
\anotes\qa H\ibl0H2\qb0{.H}\tbbl0\tql0I\ibu0H{-3}\qb0{.H}\tbbu0\qb0G\qb0{.F}\tbbu0\tqu0E&\wh a&\wh e&\wh h&\wh{^c}\en
\endvoltabox%
\setrightrepeat%
\Setvolta{2}%
\bar%11
\anotes\ha H&\ha a&\ha e&\ha h&\ha{^c}\en
\anotes\ibu0H{-3}\qb0{.H}\tbbu0\qb0G\qb0{.F}\tbbu0\tqu0E&\hp&\hp&\qlp{^j}\qs\cca j&\qup{e}\qs\cca e\en
\bar%12
\anotes\ha D&\Cpause&\ibu2d3\qb2{.d}\tbbu2\qb2e\qb2{.f}\tbbu2\tqu2g&\ha k&\ha f\en
\anotes\ibl0K{-3}\qb0{.K}\tbbl0\qb0J\qb0{.I}\tbbl0\tql0H&&\qa{hd}&\qlp h\qs\cca h&\qlp{^f}\qs\cca f\en
\endvoltabox%
\alaligne%13
\advance\barno-1%
\leftrepeat%
\anotes\qa{G_L}&\Cpause&\Cpause&\qa{in}&\qlp g\qs\cca h\en
\anotes\qa{JK}&&&\tinynotesize\qu n\tr{o}\smallsh m\ha{m}&\tr{k}\qa{.h}\ibbl4g2\qb4g\tql4h\en
\bar%14
\anotes\qa{G}\ibl1K{-3}\qb1{.K}\tbbl1\tql1J&\ha d&\ibl2k{-3}\qb2{.k}\tbbl2\qb2j\qb2{.i}\tbbl2\tql2h&\ha n&\qa{ig}\en
\anotes\ibl1I3\qb1{.I}\tbbl1\tql1K\qa G&\qa N\ibl1g3\qb1{.g}\tbbl1\tql1i&\qa{gk}&\qlp k\qs\cca k&\qlp i\qs\cca i\en
\bar%15
\anotes\qa{JF}&\qa{gf}&\qa j\ha c&\qa{lm}&\ibl4g0\qb4{.i}\tbbl4\qb4g\qb4{.h}\tbbl4\tql4i\en
\anotes\qa{JC}&\qa{cN}&\qa{*c}&\tinynotesize\qu m\tr n\ha l&\tinynotesize\qu h\tr k\ha g\en
\bar%16
\anotes\ha F&\qa{af}&\ha c&\ibl3f0\qb3{.m}\tbbl3\qb3f\qb3{.h}\tbbl3\tql3j&\qa f\ibu4a3\qb4{.a}\tbbu4\tqu4c\en
\anotes\qup F\qs\cca F&\dsp\ibl1h{-5}\roff{\tbbl1}\qb1j\qb1{.h}\tbbl1\tql1c&\qp\qa f&\qlp h\qs\cca h&\qlp f\qs\cca e\en
\bar%17
\anotes\qlp I\qs\cca H&\qa{dd}&\ha f&\ibl3h0\qb3{.i}\tbbl3\qb3h\qb3{.i}\tbbl3\tql3k&\qup d\qs\cca d\en
\anotes\qup G\qs\cca G&\tr l\ha d&\qp\qa g&\tinynotesize\qu j\tr k\smallsh i\qlp{i}\qs\cca i&\ibl4g{-1}\qb4{.g}\tbbl4\qb4h\qb4{.g}\tbbl4\tql4f\en
\bar%18
\anotes\ibl0J{-1}\qb0{.J}\tbbl0\qb0K\qb0{.J}\tbbl0\tql0I&\ha c&\ha g&\ibl3i0\qb3{.j}\tbbl3\qb3i\qb3{.j}\tbbl3\tql3l&\qa{.e}\qs\cca e\en
\anotes\qa{.H}\qs\cca H&\ha e&\qp\qa h&\tinynotesize\qu k\smallsh j\qa{.j}\qs\cca j&\qa{.h}\qs\cca g\en
\bar%19
\anotes\qa{KD}&\qa{df}&\ha h&\ibl3h0\qb3{.k}\tbbl3\qb3h\qb3{.k}\tbbl3\tql3j&\qa{.f}\qs\cca f\en
\anotes\ha G&\ha d&\qa{gi}&\ibl3i{-3}\qb3{.i}\tbbl3\qb3h\qb3{.g}\tbbl3\tql3f&\qa{eg}\en
\bar%20
\anotes\ha H\qa{.M}\qs\cca M&\qa a\ha h\qa d&\qa{he}\ha h&\ha e\hp&\tinynotesize\qu d\tr k\smallsh c\ha{c}\itieu4h\ha h\en
\bar%21
\anotes
  \ha{^J}\ha H&
  \ibu1e{-3}\roff{\tbbu1}\qb1f\tqu1{.e}\ibu1e{-2}\qb1e\tqu1d\ibu1c2\qb1{.^c}\tbbu1\qb1c\qb1{.d}\tbbu1\tqu1e&
  \ha h\dsp\ibu2a3\roff{\tbbu2}\qb2a\smallsh b\qb2{.b}\tbbu2\tqu2{^c}&
  \ha h\itieu3l\ha l&
  \ttie4\qa{.h}\qs\cca g\qa g\tr n\ibu4f{-3}\qb4{.f}\tbbu4\tqu4e\en
\bar%22
\anotes\qa{.D}\qs\cca J&\qa{ad}&\qa{dh}&\ttie3\qa l\itieu3k\qa k&\qa{.f}\qs\cca f\en
\anotes\qa{IH}&\qa{gd}&\qa{dj}&\ttie3\ibl3k0\qb3{.k}\tbbl3\qb3k\qb3{.l}\tbbl3\tql3{^m}&\qa g\trt k\qa h\en
\bar%23
\anotes\qa{.G}\qs\cca J\qa I&\ha{.d}&\qa i\ha k&{\tinynotesize\qu{^m}}\qa{.n}\qs\cca o\qa{p}&{\tinynotesize\qu h}\qa{.i}\qs\cca h\qa g\en
\anotes\qa G&\qa d&\qa i&\tinynotesize\cu o\qa n&\tinynotesize\cu f\qa e\en
\bar%24
\anotes\qu H\ibu0H{-2}\qb0{.H}\tbbu0\tqu0G&\qa{ae}&\itieu2h\wh h&\tinynotesize\cu m\qa{lo}&\tinynotesize\qu d\smallsh c\qa{.c}\qs\cca c\en
\anotes\qa F\ibu0E{-2}\qb0{.E}\tbbu0\tqu0D&\qa{fh}&\ha{*}&\ibl3h{-2}\qb3{.h}\tbbl3\qb3h\qb3{.g}\tbbl3\tql3f&\tinynotesize\qu c\qa{.d}\qs\cca d\en
\bar%25
\anotes\qa{^CD}&\qa g\ibl1f2\qb1{.f}\tbbl1\tql1g&\ttie2\ha h&\qa{ek}&\qa e\ibu4f{-2}\qb4{.f}\tbbu4\tqu4e\en
\anotes\qa{HH}&\ha h&\ha a&\tinynotesize\qu k\tr m\smallsh j\ha{j}&\tinynotesize\qu f\tr l\ha e\en
\Setvolta{1}%
\bar%26
\anotes\ibu0F4\qb0{.D}\tbbu0\qb0F\qb0{.H}\tbbu0\tqu0K&\ha f&\ha a&\ha k&\ha d\en
\anotes\ibl0H0\qb0{.^J}\tbbl0\qb0H\qb0{.^I}\tbbl0\tql0J&\hp&\hp&\hp&\qa{.e}\qs\cca e\en
\bar%27
\anotes\ibu0J{-2}\qb0{.K}\tbbu0\qb0D\qb0{.F}\tbbu0\tqu0H&\Cpause&\Cpause&\hp&\ha f\en
\anotes\ibl0K{-2}\qb0{.K}\tbbl0\qb0J\qb0{.I}\tbbl0\tql0H&&&\qa{.h}\qs\cca h&\qa{.^f}\qs\cca f\en
\setrightrepeat%
\setendvoltabox%
\Setvolta{2}%
\mulooseness1%
\generalmeter{\meterfrac38}%
\alaligne%28
\znotes&&&&\zchar{18}{\bf\kern-4ex Allegro assai}\en
\anotes\qa D\ds&\ca f\ibu1d{0}\qb1{.d}\tbbu1\tqu1d&\ca a\qa d&\ca k\itieu3k\qa k&\ca d\ds\ds\en
\setendvoltabox%
\leftrepeat%29
\znotes&&&&\segno k\en
\anotes\Cpause&\ibbu1f0\qb1{edefg}\tqu1e&\itied2a\qa{.a}&\ttie3\ca k\trt m\qa{^j}&\Cpause\en
\bar%30
\anotes\ca D\qa K&\ca f\trt k\qa{^f}&\ttie2\ca a\itied2d\qa d&\ibl3h0\qb3k\nbbl3\qb3{hij}\tql3h&\Cpause\en
\bar%31
\anotes\ibl0H5\qb0{GK}\tql0N&\qa g\ca g&\ttie2\ibu2d3\qb2{dd}\tqu2g&\ibbl3h0\qb3{ihihg}\tql3i&\ds\itieu4g\qa g\en
\bar%32
\anotes\ibl0H0\qb0{KH}\tql0K&\ibl1d5\qb1{dh}\tql1k&\ibl2d5\qb2{dh}\tql2k&\ca h\itieu3k\qa k&\ttie4\ibl4f0\qb4g\nbbl4\qb4{fef}\tql4g\en
\bar%33
\anotes\ibl0G0\qb0H\qb0{.H}\tbbl0\tql0H&\ca h\ibu1b{0}\qb1{.a}\tbbu1\tqu1a&\ca h\ibu2b{0}\qb2{.a}\tbbu2\tqu2a&\ttie3\ibl3j0\qb3k\nbbl3\qb3{jkl}\tql3j&\ca e\itieu4h\qa h\en
\bar%34
\anotes\ibbl0H0\qb0{^IHIJK}\tql0I&\ibbu1c0\qb1{^babcd}\tqu1b&\ibbu2c0\qb2{^babcd}\tqu2b&\ibbl3k0\qb3{kjklm}\tql3k&\ttie4\ca h\qa{^g}\en
\bar%35
\anotes\ca J\trt M\qa{^J}&\ca c\itieu1h\qa h&\ca c\qa{^c}&\ibl3h0\qb3{lh}\itieu3l\tql3l&\ibl4e0\qb4h\nbbl4\qb4{efg}\tql4e\en
\bar%36
\anotes
  \ca K\ibu0D0\qb0{.D}\tbbu0\tqu0D&
  \ttie1\ca h\qa f&
  \ibl2d5\qb2{df}\tql2h&
  \ttie3\ibl3j0\qb3l\nbbl3\qb3{klm}\tql3k&
  \ibl4f5\qb4{fh}\itieu4k\tql4k\en
\bar%37
\anotes
  \ibbu0F0\qb0{EDEFG}\tqu0E&
  \ca e\qa g&
  \qa g\ca i&
  \ca n\tr n\qa l&
  \ttie4\ca k\tr l\qa{^j}\en
\bar%38
\anotes\ca F\tr N\qa{^F}&\ibl1f{-2}\qb1{fh}\tql1d&\qa{.h}&\ibl3k0\qb3{kk}\itieu3k\tql3k&\ibl4g0\qb4k\nbbl4\qb4{hij}\tql4h\en
\bar%39
\anotes\ibu0G{0}\qb0{GG}\tqu0{F}&\qa{.d}&\ca g\itieu2g\qa g&\ttie3\ibbl3j0\qb3{kjkij}\tql3k&\ibbl4h0\qb4{ihigh}\tql4i\en
\bar%40
\anotes\ibbu0F{0}\qb0{EFECD}\tqu0E&\ca g\qa g&\ttie2\ca g\qa e&\ibl3j{-1}\qb3{jj}\tql3i&\ibbl4j{-2}\qb4{jkjih}\tql4g\en
\bar%41
\anotes\ibu0F{0}\qb0{FF}\tqu0E&\qa f\ds&\ca c\itieu2f\qa f&\ibl3h0\qb3{.h}\nbbl3\qb3{hi}\tql3j&\ibbl4g0\qb4{hghfg}\tql4h\en
\bar%42
\anotes\ibu0F0\qb0{.D}\nbbu0\qb0{FE}\tqu0D&\Cpause&\ttie2\ca f\itied2d\qa d&\ibl3i{-1}\qb3{ii}\tql3h&\ibbl4i{-2}\qb4{ijihg}\tql4f\en
\bar%43
\anotes\ibu0E{0}\qb0{EE}\tqu0D&\Cpause&\ttie2\ibu2c0\qb2{d^c}\tqu2d&\ibl3g0\qb3{.g}\nbbl3\qb3{gh}\tql3i&\ibbl4f0\qb4{gfgef}\tql4g\en
\bar%44
\anotes\ibbu0E3\qb0{^CDCH}\qb0{^I}\tqu0{^J}&\itieu1h\qa{.h}&\tr l\qa{.e}&\ibl3e0\qb3{he}\tql3g&\ibbl4h{-2}\qb4{hihgf}\tql4e\en
\bar%45
\anotes\ibl0K{-1}\qb0{KM}\cna J\tql0J&\ttie1\ibl1d0\qb1{hd}\tql1h&\ca d\itieu2h\qa h&\ibl3f2\qb3f\qb3{.h}\tbbl3\tql3h&\ibu4f0\qb4{ff}\tqu4e\en
\bar%46
\anotes\ibl0J{-4}\qb0{KL}\tql0{E}&\ibbl1f0\qb1f\tbbl1\qb1h\qb1e\tql1e&\ttie2\ca h\tr k\qa{^g}&\ibbl3i0\qb3{^ihijk}\tql3i&\ibu4d{-2}\qb4{dc}\tqu4{^b}\en
\bar%47
\anotes\ca H\qa H&\ca e\itieu1h\qa h&\ibl2e0\qb2h\nbbl2\qb2{efg}\tql2e&\ca j\trt m\qa{^j}&\qa a\ds\en
\bar%48
\anotes\ibu0H3\qb0{FD}\tqu0K&\ttie1\ca h\qa f&\ibbl2f0\qb2{fefgh}\tql2i&\ca k\ibl3k0\qb3{.k}\tbbl3\tql3k&\ds\ibu4b4\qb4{a}\itied4d\tqu4d\en
\bar%49
\anotes\ibl0H0\qb0H\qb0{.H}\tbbl0\tql0H&\qa{.e}&\ca h\qa h&\ibbl3k0\qb3{lklmn}\tql3l&\ttie4\ca d\tr k\qa{^c}\en
\bar%50
\anotes\ibbl0L0\qb0{K}\qb0{^JKLM}\tql0K&\qa{.d}&\qa h\itied2d\ca d&\ibbl3m{-2}\qb3{mlkji}\tql3h&\ca d\ibl4f0\qb4{.f}\tbbl4\tql4f\en
\bar%51
\anotes\ibbl0M0\qb0{LKLMN}\tql0L&\qa{.g}&\ttie2\ca d\qa j&\ca g\ibl3n0\qb3{.n}\tbbl3\tql3n&\ibbu4g{-2}\qb4{gfedc}\tqu4b\en
\bar%52
\anotes\ibbl0N0\qb0{MLMNa}\tql0M&\qa{.f}&\qa j\ca f&\ibbl3o{-2}\qb3{onmlk}\tql3j&\ca a\ibl4h0\qb4{.h}\tbbl4\tql4h\en
\bar%53
\anotes\ibbl0a0\qb0{NMNab}\tql0N&\qa{.i}&\ca d\qa k&\ca i\itieu3p\qa p&\ibbu4i{-2}\qb4{ihgfe}\tqu4d\en
\bar%54
\anotes\ca L\qa M&\ibl1g1\qb1g\nbbl1\qb1{jij}\tql1h&\qa j\ca c&\ttie3\ca p\itieu3m\qa m&\ca c\ibbl4g{-1}\qb4{hgh}\tql4f\en
\bar%55
\anotes\ibl0J0\qb0J\nbbl0\qb0{JKL}\tql0J&\ibbl1g{-4}\qb1g\tql1e\qa c&\ibl2g0\qb2{jg}\tql2j&\ttie3\ibbl3l0\qb3{mnlmn}\tql3l&\ca g\itieu4j\qa j\en
\bar%56
\anotes\ibbl0L0\qb0{KJKLM}\tql0K&\ds\ibl1f5\qb1f\tql1i&\ca f\tr l\qa d&\ibbl3l{0}\qb3{mlmlk}\tql3m&\ttie4\ca j\itieu4i\qa i\en
\bar%57
\anotes\ibl0L{-2}\qb0{LM}\tql0H&\ibl1g{-2}\qb1{gh}\tql1f&\qa c\ca c&\ibl3l{-2}\qb3{nm}\tql3j&\ttie4\ca i\ibbl4g{-1}\qb4{hgh}\tql4f\en
\bar%58
\anotes\ibu0I0\qb0{IJ}\tqu0C&\ibl1k{-5}\qb1{kj}\tql1g&\ca d\qa c&\ca i\tr k\qa g&\ca g\tr l\qa e\en
\bar%59
\anotes\ca F\ibbl0M{-1}\qb0{MNM}\tql0L&\ca h\ibbl1f{-1}\qb1{fgf}\tql1e&\ca c\ibbl2f{-1}\qb2{fgf}\tql2e&\ca f\ibl3h0\qb3{.h}\tbbl3\tql3h&\qa f\ds\en
\bar%60
\anotes\ca K\qa N&\ca d\qa g&\ca d\qa g&\ibbl3i0\qb3{^ihijk}\tql3i&\ds\ibu4d0\qb4{.d}\tbbu4\tqu4d\en
\bar%61
\anotes\qa{.J}&\qa{.c}&\qa{.c}&\ibbl3j0\qb3{j}\qb3{^ijkj}\tql3{=i}&\ibbu4f0\qb4{edefg}\tqu4e\en
\bar%62
\anotes\qa{.K}&\qa{.d}&\qa{.d}&\ibbl3h0\qb3{hghij}\tql3h&\ibbu4f0\qb4{^fefgh}\tqu4f\en
\bar%63
\anotes\ca G\itieu0K\qa K&\ca N\qa d&\ca N\qa d&\ca i\ibbl3g{-1}\qb3{hgh}\tql3f&\ca g\ibbu4f0\qb4{fef}\tqu4d\en
\bar%64
\anotes\ttie0\ca K\ibbl0I{-1}\qb0{JIJ}\tql0H&\ca i\qa h&\ca i\itieu2h\qa h&\ca g\qa e&\ibu4h0\qb4{eh}\itied4a\tqu4a\en
\bar%65
\anotes\qa{.I}&\ca d\ibu1d0\qb1{.d}\tbbu1\tqu1d&\ttie2\ca h\trt k\qa g&\ca d\itieu3k\qa k&\ttie4\ca a\ds\ds\en
\bar%66
\anotes\itied0H\qu{.H}&\ibbu1e0\qb1{edefg}\tqu1e&\ca h\qa a&\ttie3\ca k\qa{^j}&\Cpause\en
\bar%67
\anotes\ttie0\ibu0H{-2}\qb0{HD}\tqu0F&\ca f\ibl1h0\qb1{.h}\tbbl1\tql1h&\ibbu2f0\qb2{fefed}\tqu2f&\ca k\itieu3h\qa h&\ds\ibu4a0\qb4{.a}\tbbu4\tqu4a\en
\bar%68
\anotes\qa{.E}&\ibbl1h0\qb1{^ihijk}\tql1i&\ca e\qa e&\ttie3\ca h\qa{^g}&\ibbu4c0\qb4{^babcd}\tqu4b\en
\bar%69
\anotes\ibu0H0\qb0{H.H}\tbbu0\tqu0H&\qa{.j}&\ibu2c5\qb2{ae}\tqu2h&\ca h\itieu3o\qa o&\ibbu4d0\qb4{c}\qb4{^bcde}\tqu4c\en
\bar%70
\anotes\ibbl0H0\qb0{^IHIJK}\tql0I&\qa{.^i}&\qa d\ca{^i}&\ttie3\ca o\qa{^n}&\ibbu4e0\qb4{dcdef}\tqu4d\en
\bar%71
\anotes\ibbl0I{0}\qb0{J^IJHI}\tql0J&\ca h\qa a&\ibl2e0\qb2{eh}\tql2e&\ibl3l0\qb3{ol}\tql3o&\ca e\itieu4j\qa j\en
\bar%72
\anotes\ibl0E2\qb0{Da}\tql0H&\ibbu1f0\qb1{defde}\tqu1f&\ibl2f0\qb2{.f}\nbbl2\qb2{fg}\tql2h&\ca m\itieu3m\qa m&\ttie4\ibl4e0\qb4j\qb4d\tql4j\en
\bar%73
\anotes\ibbu0G0\qb0{GHGFE}\tqu0D&\ca g\qa N&\ibbu2d0\qb2{dcd}\qb2{^bc}\tqu2d&\ttie3\ibl3i0\qb3{mg}\tql3m&\ca{^i}\itieu4i\qa i\en
\bar%74
\anotes\ibl0D2\qb0{CN}\tql0G&\ibbu1e0\qb1{cdecd}\tqu1e&\ibbu2f0\qb2{efgef}\tqu2g&\ca l\itieu3l\qa l&\ttie4\ibl4e0\qb4i\qb4c\tql4{^i}\en
\bar%75
\anotes\ibbu0F{0}\qb0{FGFED}\tqu0C&\ibbl1f2\qb1f\tql1g\qa h&\ibbu2c0\qb2{c}\qb2{^bcab}\tqu2c&\ttie3\ibl3h0\qb3l\qb3f\tql3l&\ca h\itieu4h\qa h\en
\bar%76
\anotes\ca D\qa M&\ibbu1c0\qb1{^bcdbc}\tqu1d&\ibu2d2\qb2{.d}\nbbu2\qb2{de}\tqu2f&\ca k\itieu3k\qa k&\ttie4\ibu4h0\qb4{h^b}\tqu4h\en
\bar%77
\anotes\ibbl0L{-2}\qb0{LMLKJ}\tql0{^I}&\itied1e\qa{.e}&\qa{^b}\ca e&\ttie3\ibl3g0\qb3{ke}\tql3k&\tr k\qa{.^g}\en
\bar%78
\anotes\ibbl0H0\qb0{H^GH}\qb0{^IJ}\tql0H&\ttie1\qa e\ca c&\qa{.h}&\ibbl3i{-2}\qb3{j}\qb3{^ijih}\tql3g&\ibu4g{-2}\qb4h\qb4a\itied4e\tqu4e\en
\bar%79
\anotes\ibbl0K0\qb0{KJKLM}\tql0K&\ca d\qa f&\qa{.h}&\ibbl3f0\qb3{femlk}\tql3j&\ttie4\ca e\ibbl4k{-2}\qb4{kj}\qb4{^i}\tql4h\en
\bar%80
\anotes\ibl0G{0}\qb0L\qb0E\tql0K&\itied1e\qa{.e}&\ds\ibbu2d{-2}\qb2{edc}\tqu2{^b}&\ibbl3i2\qb3{^il}\qb3{^n}\tql3l\ds&\ibbu4g{0}\qb4{^g}\qb4{^f}\qb4{e^be}\tqu4{^g}\en
\bar%81
\anotes\qa J\ca J&\ttie1\ca e\itieu1h\qa h&\ibbu2g0\qb2{cehgf}\tqu2e&\ds\ibl3h0\qb3{.h}\tbbl3\tql3h&\ibbu4h{-3}\qb4{h}\qb4{=gfed}\tqu4c\en
\bar%82
\anotes\ca{^I}\qa{L}&\ttie1\ca h\tr k\qa{^g}&\ibbu2f0\qb2d\tbbu2\qb2f\qb2e\itied2e\tqu2e&\ibbl3h0\qb3{^ihijk}\tql3i&\ca d\tr k\qa{^b}\en
\bar%83
\anotes\ca H\itieu0H\qa H&\qa h\ds&\ttie2\ibbu2e{-1}\qb2{edc}\qb2{^bc}\tqu2a&\ibbl3j0\qb3{j}\qb3{^ijkl}\tql3j&\qa a\ds\en
\bar%84
\anotes\ttie0\ibu0H{0}\qb0H\nbbu0\qb0{GFG}\tqu0E&\Cpause&\ca{^b}\tr k\qa{^c}&\ca k\qa l&\Cpause\en
\bar%85
\anotes\qa{.F}&\ibl1f0\qb1h\nbbl1\qb1{hgf}\tql1h&\ibbl2d1\qb2{dhfgh}\tql2f&\ca h\itieu3k\qa k&\ds\ibu4d0\qb4{.d}\tbbu4\tqu4d\en
\bar%86
\anotes\qa{.E}&\ibl1e0\qb1{ge}\tql1h&\ibbl2f0\qb2{gfgfe}\tql2g&\ttie3\ca k\tr l\qa{^j}&\ibbu4e0\qb4{edefg}\tqu4e\en
\bar%87
\anotes\ca D\itieu0K\qa K&\ibl1f{-2}\qb1{fh}\itieu1d\tql1d&\ca h\itied2d\qa d&\ibbl3k0\qb3{k}\qb3{^jklm}\tql3k&\ibbl4f0\qb4{fefgh}\tql4f\en
\bar%88
\anotes\ttie0\ibl0J{-2}\qb0K\nbbl0\qb0{JIJ}\tql0H&\ttie1\ca d\qa c&\ttie2\ca d\qa c&\ca l\trt p\qa{^m}&\ca g\qa h\en
\bar%89
\anotes\qa{.I}&\qa{.b}&\qa{b}\ca d&\ca n\itieu3k\qa k&\qa d\ds\en
\bar%90
\anotes\qa{.H}&\ca a\qa{h}&\ca e\tr k\qa{^f}&\ttie3\ibl3j{-2}\qb3k\nbbl3\qb3{jij}\tql3h&\Cpause\en
\bar%91
\anotes\qa{.G}&\qa{.d}&\qa{.g}&\ibbl3h0\qb3{ihihg}\tql3i&\ds\itieu4g\qa g\en
\bar%92
\anotes\ca K\ibu0E0\qb0{.D}\tbbu0\tqu0D&\qs\ibbl1h{-2}\qb1{hkhg}\tql1f&\ca d\ibl2f0\qb2{.f}\tbbl2\tql2f&\ca h\qa o&\ttie4\ibl4f0\qb4g\qb4f\itieu4k\tql4k\en
\bar%93
\anotes\ibbu0F0\qb0{EDEFG}\tqu0E&\ibl1e0\qb1{eh}\tql1e&\ibbl2g0\qb2{gfghi}\tql2g&\ca n\tr n\qa l&\ttie4\ca k\tr m\qa{^j}\en
\bar%94
\anotes\ibbu0G0\qb0{FEFGH}\tqu0F&\qa a\ds&\ibbl2f0\qb2{hghgf}\tql2h&\qa{.k}&\ca k\itieu4h\qa h\en
\bar%95
\anotes\ibbu0I0\qb0{GHIGH}\tqu0I&\ds\ibu1e5\qb1d\itied1g\tqu1g&\itieu2i\qa{.i}&\ibbl3i0\qb3{ijkij}\tql3k&\ttie4\ibl4h{-2}\qb4{hg}\tql4f\en
\bar%96
\anotes\ibbl0J{-2}\qb0{JKJIH}\tql0G&\tltie1\ibl1g{-2}\qb1{gj}\tql1c&\ttie2\ibl2e0\qb2{ic}\tql2i&\ibbl3f0\qb3{gfgef}\tql3g&\ca e\itied4e\qa e\en
\bar%97
\anotes\ibbu0H0\qb0{FGHFG}\tqu0H&\ibu1f{0}\qb1{ff}\tqu1c&\ca h\itieu2h\qa h&\ibbl3h0\qb3{hijhi}\tql3j&\ttie4\ibu4f0\qb4{ef}\tqu4e\en
\bar%98
\anotes\ibbu0I0\qb0{IJIHG}\tqu0F&\qa d\ca d&\ttie2\ibl2h0\qb2{hi}\tql2h&\ibu3f0\qb3{.f}\nbbu3\qb3{de}\tqu3f&\ca d\itied4d\qa d\en
\bar%99
\anotes\ibbu0F0\qb0{EFGEF}\tqu0G&\qa N\itied1e\ca e&\ca g\itieu2g\qa g&\ibl3g0\qb3{.g}\nbbl3\qb3{gh}\tql3i&\ttie4\ibu4e0\qb4{de}\tqu4d\en
\bar%100
\anotes\ibbu0H0\qb0{HIHGF}\tqu0E&\ttie1\ibu1f0\qb1{eh}\tqu1a&\ttie2\ibu2g0\qb2{ga}\tqu2e&\ibl3e0\qb3{he}\tql3g&\tr k\qa{.^c}\en
\bar%101
\anotes\ibu0I0\qb0{DK}\tqu0D&\ibbu1e0\qb1{d}\qb1{^cdef}\tqu1d&\qs\ibbl2f0\qb2{hfgh}\tql2f&\ibbl3f0\qb3{fefhk}\tql3j&\ca d\itieu4h\qa h\en
\bar%102
\anotes\ibbu0H0\qb0{GFGHI}\tqu0G&\qs\ibbl1j{-3}\qb1{kihg}\tql1f&\ca d\qa i&\ibbl3i0\qb3{ihikl}\tql3k&\ttie4\ca h\itieu4g\qa g\en
\bar%103
\anotes\ibu0J0\qb0{HK}\tqu0D&\ibbu1e{-1}\qb1e\tbbu1\qb1{^c}\tqu1d\ca h&\qa{.h}&\ibbl3j0\qb3{^jlkjk}\tql3l&\ttie4\ibbl4e0\qb4{gefef}\tql4g\en
\bar%104
\anotes\ibu0J0\qb0{HK}\tqu0D&\ibu1f{3}\qb1a\tqu1{h}\ds&\ibl2f{-3}\qb2h\tql2d\ds&\ibbl3j0\qb3{^jlkjk}\tql3l&\ibbl4e0\qb4{egfef}\tql4g\en
\bar%105
\anotes\ca H\ds\ds&\qs\ibbu1g0\qb1{heh^c}\tqu1e&\qs\ibbu2g0\qb2{heh^c}\tqu2e&\ca{^j}\ds\ds&\ca e\ds\ds\en
\bar%106
\anotes\itieu0H\qa{.H}&\qs\ibbu1c0\qb1{a}\qb1{^c}\qb1{^bd}\tqu1c&\qs\ibbu2c0\qb2{a^c^bd}\tqu2c&\Cpause&\Cpause\en
\bar%107
\anotes\ttie0\itieu0H\qa{.H}&\ibbu1f0\qb1{edfeg}\tqu1f&\ibbu2f0\qb2{edfeg}\tqu2f&\Cpause&\Cpause\en
\bar%108
\anotes\ttie0\itieu0H\qa{.H}&\itied1a\qa{.a}&\itied2a\qa{.a}&\ibbl3i0\qb3{hg}\qb3{^ih}\qb3{^j}\tql3i&\Cpause\en
\bar%109
\anotes\ttie0\itieu0H\qa{.H}&\ttie1\itied1a\qa{.a}&\ttie2\itied2a\qa{.a}&\ibbl3l0\qb3{k}\qb3{^jlkm}\tql3l&\Cpause\en
\bar%110
\anotes\ttie0\qa{.H}&\ttie1\qa{.a}&\ttie2\qa{.a}&\ibbl3m0\qb3{nlmno}\tql3{^p}&\Cpause\en
\bar%111
\anotes\ibbu0H0\qb0{HIHGF}\tqu0E&\ibbu1f0\qb1{aehef}\tqu1g&\ca h\ds\ds&\ca l\ds\ds&\ca{^j}\ds\ds\en
\bar%112
\anotes\ca D\ds\ds&\ibbl1f0\qb1{fhkfg}\tql1h&\ibbu2f0\qb2{defde}\tqu2f&\ca m\ds\ds&\ca k\ds\ds\en
\bar%113
\anotes\ibbu0F0\qb0{GHGFE}\tqu0D&\ibl1e0\qb1{.i}\nbbl1\qb1{de}\tql1f&\ca g\ds\ds&\ibl3k0\qb3{.k}\nbbl3\qb3{kn}\tql3m&\ca i\ds\ds\en
\bar%114
\anotes\ca C\ds\ds&\ibl1g0\qb1{gj}\tql1g&\ibbu2e0\qb2{cdecd}\tqu2e&\ibbl3l0\qb3{lmnlm}\tql3n&\ca i\ds\ds\en
\bar%115
\anotes\ibbl0I0\qb0{MLMHK}\tql0J&\ibl1d0\qb1{.h}\nbbl1\qb1{cf}\tql1h&\ca f\ds\ds&\ibl3j2\qb3{jo}\tql3m&\ca h\ds\ds\en
\bar%116
\anotes\ibbl0I0\qb0{IJKIJ}\tql0K&\qa d\ca f&\ca f\ds\ds&\ca k\ds\ds&\ca h\ds\ds\en
\bar%117
\anotes\ibbu0I0\qb0{GHIGH}\tqu0I&\ca d\ds\ds&\ca e\ds\ds&\ibl3i{-2}\qb3{.i}\nbbl3\qb3{ih}\tql3g&\itieu4g\qa{.g}\en
\bar%118
\anotes\ibu0J0\qb0H\qb0K\tqu0G&\ibu1e{-2}\qb1{ed}\tqu1b&\ibbu2f0\qb2{aefhi}\tqu2g&\ca{^j}\itieu3k\qa k&\ttie4\ibl4g{-2}\qb4{gf}\tql4e\en
\bar%119
\anotes\ca H\qa H&\ca a\qa h&\ibbu2f0\qb2f\tbbu2\qb2d\qb2h\tqu2a&\ttie3\ca k\qa{^j}&\ca f\tr m\qa e\en
\nnotes&&&&\en
\znotes&&&&\coda n\en
\generalmeter{\allabreve}%
\transpose=0%
\Changecontext%120
\znotes&&&&\uptext{\bf\kern4ex Adagio}\en
\anotes\fermataup N\qa G\qp\hp&\fermataup l\qa i\qp\hp&\fermataup l\qa d\qp\hp&\fermataup n\qa n\qp\qa{.k}\qs\cca k&\fermataup l\qa g\qp\hp\en
\bar%121
\anotes\wh{^L}&\wh{^g}&\wh{^i}&
  \qa{.^p}\uptuplet{s}{3.0}{-6}\ibbl3o{-2}\qb3{o}\qb3{^n}\tql3{^m}&
  \wh{^e}\en
\addspace\afterruleskip%
\anotes\ha{*}&\ha{*}&\ha{*}&\ibl3l{-2}\smallsh l\qb3{.l}\tbbl3\qb3k\tr n\qb3{.j}\tbbl3\tql3{^i}&\ha{*}\en
\bar%122
\anotes\wh H&\wh c&\ibl2i2\qb2{.^i}\tbbl2\tql2j\tr p\qa h&\ibl3h3\qb3{.h}\tbbl3\qb3o\qb3{.^l}\tbbl3\tql3o&\ha{^e}\en
\anotes\ha{*}&\ha{*}&\ibl2i{0}\qb2{.j}\tbbl2\qb2{^l}\qb2{.h}\tbbl2\tql2j&\qa{jh}&\usf n\qa{.h}\qs\cca h\en
\bar%123
\anotes\wh{^J}&\ibl1h0\qb1{.h}\tbbl1\qb1{^l}\qb1{.l}\tbbl1\tql1h&\ha{^e}&\wh h&
  \qa{.^l}\uptuplet{o}{3.0}{-6}\ibbl4k{-2}\qb4{k}\qb4{^j}\tql4{^i}\en
\anotes\ha{*}&\qa{^eg}&\ha{^l}&\ha{*}&\ibu4h{-2}\qb4{.h}\tbbu4\qb4g\tr o\qb4{.f}\tbbu4\tqu4{^e}\en
\alaligne%124
\anotes\wh K&\qa{fm}&\wh h&\ibl3k3\qb3{.k}\tbbl3\qb3r\qb3{.o}\tbbl3\tql3r&\ibu4e2\qb4{.^e}\tbbu4\tqu4f\tr n\qa d\en
\anotes\ha{*}&\qa{kh}&\ha{*}&\ibl3l{0}\qb3{.m}\tbbl3\qb3o\qb3{.k}\tbbl3\tql3m&\usf l\qa{.k}\qs\cca k\en
\bar%125
\anotes\wh{^M}&\wh k&\qa{hd}&\qa{ho}&\qa{.k}\qs\cca j\en
\anotes\ha{*}&\ha{*}&\qa{hk}&\qa{ko}&\qa j\ibl4j{-4}\qb4{.k}\tbbl4\tql4h\en
\bar%126
\anotes\ha N&\qa k\ibl1k0\qb1{.k}\tbbl1\tql1k&\ibl2h0\qb2{.k}\tbbl2\qb2g\qb2{.i}\tbbl2\tql2k&\qa{on}&\tinynotesize\qu h\qa{.i}\qs\cca i\en
\anotes\ha{^L}&\ibl1n{-2}\qb1{.n}\tbbl1\qb1m\qb1{.^l}\tbbl1\tql1k&\ibl2j0\qb2{.j}\tbbl2\tql2j\qa j&\ha n&\isluru4i\qa i\tslur4j\qa j\en
\bar%127
\anotes\ha M&\ha j&\dsp\ibu2g3\roff{\tbbu2}\qb2f\qb2{.h}\tbbu2\tqu2j&\qa{nm}&\qa{.h}\qs\cca h\en
\anotes\ha K&\ibl1m{-2}\qb1{.m}\tbbl1\cna l\qb1l\qb1{.k}\tbbl1\tql1j&\ibl2i0\qb2{.i}\tbbl2\tql2i\qa i&\ha m&\qa{hi}\en
\bar%128
\anotes\ha{LJ}&\qa{ige}\ibu1h4\qb1{.g}\tbbu1\tqu1j&\dsp\ibu2f3\roff{\tbbu2}\qb2e\qb2{.g}\tbbu2\tqu2i\qa h\itieu2j\qa j&\qa{ml}\ha l&\tinynotesize\qu h\qa{.g}\qs\cca g\qa j\ibl4k2\qb4{.k}\tbbl4\tql4l\en
\bar%129
\anotes\ha{.K}\qp&\ha{.h}\qa i&\ttie2\ibu2j{-5}\qb2{.j}\tbbu2\tqu2d\ha j\qa i&\qa{.k}\qs\cca k\qa k\tr n\qa{^l}&\tinynotesize\qu g\smallsh f\qa{.f}\qs\cca f\qa f\tr o\qa g\en
\bar%130
\anotes\qa{KGKK}&\qa{kd}\ha k&\qa{hg}\ha d&\qa{^m}\ha n\tr o\qa m&\qa h\ibu4i{-2}\qb4{.i}\tbbu4\tqu4h\tr p\qa{.h}\qs\cca g\en
\generalmeter{\meterfrac38}%
\transpose=3%
\Changecontext%
\anotes\qa D\ds&\ibu1f{-1}\qb1f\qb1{.d}\tbbu1\tqu1d&\ca a\qa d&\ca k\itieu3k\qa k\ttie3&\ca d\ds\ds\en
\transpose=0%
\znotes&&&&\lcn{q}{\bf D.S. al Coda}\en
\generalmeter{\allabreve}%
\setrightrepeat%
\Changecontext%132
\znotes&&&&\coda o\en
\anotes\qa{.G}\qs\cca N\qa{.M}\qs\cca M&\qa{.k}\qs\cca i\qa{.h}\qs\cca h&\qa{.d}\qs\cca k\qa{.k}\qs\cca k&\qa n\qs\ibbl3p{-2}\qb3{po}\tql3n\qa o\itieu3r\qa r&\qa g\qp\ha k\en
\bar%133
\anotes\wh L&\qa{gg}\ha j&\ha{ln}&\ttie3\ha r\tr s\ha q&\ha{gn}\en
\bar%134
\anotes\ha{.K}&\ha{.h}&\ha{.k}&\ha{.r}&\ha{.^m}\en
\generalmeter{\meterfrac68}%
\Changecontext%
\advance\barno-1%
\znotes&&&&\zcn{o}{\bf Allegro}\en
\anotes\qpp&\qpp&\qpp&\qpp&\qp\ca d\en
\mulooseness0%
\alaligne%
\leftrepeat%135
\advance\barno-1%
\anotes\qa{.N.K}&\Cpause&\Cpause&\qa{.k}&\qa{.i}\tr p\qa{.h}\en
\bar%136
\anotes\qa{.G.N}&\Cpause&\Cpause&\ibl3m{-6}\qb3{nk}\tql3i\ibl3g5\qb3{gi}\tql3n&\itied4g\qa{.g}\ttie4\qa g\ca i\en
\bar%137
\anotes\qa{.J.I}&\Cpause&\Cpause&\qa{.l}\qa k\ca n&\qa h\ca{^f}\qa g\ca i\en
\bar%138
\anotes\qa{.J.I}&\Cpause&\Cpause&\qa{.l.k}&\ibu4h2\qb4{hi}\tqu4j\tinynotesize\cu i\qa h\ca g\en
\bar%139
\anotes\qa{.H.G}&\Cpause&\Cpause&\qa{.j}\ibl3i2\qb3{ij}\tql3k&\tr o\qa{.^f}\qa{.g}\en
\bar%140
\anotes\itieu0K\ha{.K}&\Cpause&\qa{.^f.g}&\itieu3k\ha{.k}&\ibl4h2\qb4{hk}\tql4j\ibu4i{-2}\qb4{ih}\tqu4g\en
\bar%141
\anotes\ttie0\ha{.K}&\Cpause&\ibl4h2\qb4{hk}\tql4j\ibu4i{-2}\qb4{ih}\tqu4g&\ttie3\itieu3k\ha{.k}&\qa{.^f}\tr p\qa{.g}\en
\bar%142
\anotes\qa{.K.K}&\qa{.k}\ibl1j{-2}\qb1{ji}\tql1h&\ca{^f}\ds\ds\itieu2k\qa{.k}&\ttie3\ca k\ibl3k2\qb3{k}\tql3{^l}\ibl3m2\qb3{^mn}\tql3o&\ca h\ibu4d2\qb4d\tqu4{^e}\ibu4f2\qb4{^fg}\tqu4h\en
\bar%143
\anotes\qa{.G.I}&\qa{.g.n}&\ttie2\qa{.k.g}&\ibl3p{-2}\qb3{po}\tql3n\qa r\ca r&\ibu4i{-2}\qb4{ih}\tqu4g\qa n\ca n\en
\bar%144
\anotes\qa{.H.K}&\qa{.^l}\HQsk\cna m\qa{.m}&\ha{.h}&\qa{^q}\ca q\qa r\ca r&\qa n\ca n\cna m\qa m\ca m\en
\bar%145
\anotes\qa{.G.J}&\qa{.k}\cna l\qa{.l}&\ha{.g}&\qa{^p}\ca p\qa q\ca q&\qa m\ca g\qa l\ca l\en
\bar%146
\anotes\qa{.F.I}&\qa{.j.k}&\ha{.f}&\qa o\ca o\qa p\ca p&\qa l\ca f\qa k\ca k\en
\bar%147
\anotes\ha{.L}&\qa{.i.g}&\qa{.g.n}&\qa n\ca m\tr n\qa l\ca k&\ibl4k0\qb4{kl}\tql4k\ibl4j{-1}\qb4{jk}\tql4i\en
\bar%148
\anotes\qa{.M}\qpp&\ca j\ibu1f2\qb1f\tqu1g\ibu1h2\qb1{hi}\tqu1j&\ca m\ibu2f2\qb2f\tqu2g\ibu2h2\qb2{hi}\tqu2j&\ca j\ibu3f2\qb3f\tqu3g\ibu3h2\qb3{hi}\tqu3j&\ca h\ds\ds\qpp\en
\bar%149
\anotes\qpp\qa{.M}&\ibu1i0\qb1{fj}\tqu1i\ibu1h{-2}\qb1{hg}\tqu1f&\ibu2f2\qb2{fh}\tqu2i\ibl2j2\qb2{jk}\tql2l&\qa{.f}\itieu3m\qa{.m}&\ds\ibu4f2\qb4f\tqu4g\ibu4h2\qb4{hi}\tqu4j\en
\bar%150
\anotes\qa{.I.H}&\ibl1i2\qb1{ij}\tql1k\ibl1l{-2}\qb1{lk}\tql1j&\ibl1k2\qb1{kl}\tql1m\tr n\ibl1i0\qb1{ji}\tql1j&\ttie3\qa{.m}\itieu3m\qa{.m}&\ibl4k{-2}\qb4{kj}\tql4i\ibl4j2\qb4{jk}\tql4l\en
\bar%151
\anotes\qa{.I.G}&\ibl1k2\qb1{kl}\tql1m\qa i\ca l&\qa{.i.i}&\ttie3\ibl3m{-2}\qb3{ml}\tql3k\qa n\ca n&\ibl4k{-2}\qb4{kj}\tql4i\qa l\ca l\en
\bar%152
\anotes\qa{.F.I}&\ibl1j2\qb1{jk}\tql1l\qa k\ca f&\itieu2m\qa{.m}\ttie2\ibl2m{-2}\qb2{ml}\tql2k&\ibl3o2\qb3{op}\tql3q\qa p\ca k&\ibl4l{-2}\qb4{lk}\tql4j\qa k\ca i\en
\bar%153
\anotes\qa{.L.M}&\qa{.g.f}&\qa{.j.f}&\qa{.l}\tr n\qa{.j}&\qa{.j}\tr p\qa{.h}\en
\Setvolta{1}%
\bar%154
\anotes\qa{.I}\qpp&\qa{.f}\qpp&\qa{.d}\qpp&\qa{.i}\qpp&\qa{.i}\ds\ds\ca d\en
\setrightrepeat%
\setendvoltabox%
\Setvolta{2}%
\bar%155
\anotes\ca I\ibl0K2\qb0{K}\tql0L\ibl0M2\qb0{MN}\tql0a&\qa{.f}\qpp&\qa{.d}\qpp&\qa{.i}\qpp&\qa{.i}\qpp\en
\setleftrepeat%
\setendvolta%
\bar%156
\anotes\qa{.b}\qpp&\Cpause&\qa{.k.j}&\ibl3k2\qb3{kl}\tql3m\qa m\ca m&\Cpause\en
\bar%157
\anotes\qa{.K.J}&\qa{.f._h}&\qa{.^i.j}&\itieu3m\qa{.m}\ttie3\ibl3m0\qb3{mn}\tql3m&\Cpause\en
\bar%158
\anotes\qa{.^I.G}&\qa{.g}\ibl1m{-2}\qb1{ml}\tql1k&\qa{.k}\tr n\qa{.^i}&\ibl3m0\qb3{r^l}\tql3m\ibl3o{-2}\qb3{_on}\tql3m&\Cpause\en
\bar%159
\addspace\afterruleskip%
\anotes\qa{.J.M}&\qa{.j.j}&\qa j\ca g\qa{_h}\ca j&\cna l\ibl3l2\qb3{lq}\tql3p\ibl3o{-2}\qb3{_on}\tql3m&\qa{.n}\ibl4m{-2}\qb4{ml}\tql4k\en
\bar%160
\anotes\qa{.N.G}&\qpp\qa{.g}&\qa{.g.^i}&\qa{.l.k}&\qa{.j}\qpp\en
\bar%161
\anotes\qa{.J}\qpp&\qa{.e.c}&\ibu2j{-4}\qb2{jg}\tqu2e\qa{.c}&\itieu3j\qa{.j}\ttie3\ca j\ibu3h5\qb3g\tqu3j&\Cpause\en
\bar%162
\anotes\Cpause&\Cpause&\Cpause&\qa{.^l}\tr n\qa{.k}&\ibu4e2\qb4{^e^f}\tqu4g\qa g\ca g\en
\bar%163
\anotes\qa{.^L.K}&\qa{.g.i}&\qa{.g.i}&\qa{.^j.k}&\itied4g\qa{.g}\ttie4\ibu4h0\qb4{gh}\tqu4g\en
\bar%164
\anotes\qa{.^J.H}&\qa{.h}\ibu1g{-2}\qb1{gf}\tqu1{^e}&\qa{.h}\ibu2g{-2}\qb2{gf}\tqu2{^e}&\qa{.^l}\tr n\qa{.^j}&\ibl4g0\qb4{^l^f}\tql4g\ibu4i{-2}\qb4{ih}\tqu4g\en
\bar%165
\addspace\afterruleskip%
\anotes\qa{.K.^L}&\qa{.d}\qpp&\qa{.d}\qpp&\itieu3k\qa{.k}\ttie3\ibl3k{-2}\qb3{k}\HQsk\cna j\qb3j\tql3i&\ibu4f{-2}\cna f\qb4{f^e}\tqu4d\qa g\ca d\en
\bar%166
\anotes
  \qa{.M}\itieu0N\qa{.N}&
  \itieu1k\qa{.k}\ttie1\ibl1k{-2}\qb1{kj}\tql1i&
  \itieu2k\qa{.k}\ttie2\ibl2k{-2}\qb2{kj}\tql2i&
  \ibl3h5\qb3{hk}\tql3o\ibl3n2\qb3{no}\tql3p&
  \qa h\ca d\ibu4i{-2}\qb4{ih}\tqu4g\en
\bar%167
\anotes\ttie0\ibl0N0\qb0{Na}\tql0N\ibl0M{-2}\qb0{M^L}\tql0K&\itied1h\ha{.h}&\qa{.h.d}&\tr n\qa{.^l.o}&\tr n\qa{.^j}\qa k\ca d\en
\bar%168
\anotes\qa{.H.G}&\ttie1\qa{.h}\qpp&\qa{.h}\qpp&\ds\ds\ca k\ibl3j2\qb3{^jk}\tql3{^l}&\qa{.f}\tr n\qa{.^e}\en
\bar%169
\anotes\qa{.F}\qpp&\qa{.d.^e}&\qa{.d.^e}&\qa{.h}\qpp&\ibl4k1\qb4{km}\tql4{^l}\ibl4k{-2}\qb4{kj}\tql4i\en
\bar%170
\anotes\qa{.M.^L}&\qa{.f.g}&\qa{.f.g}&\qa{.o}\tr s\qa{.^q}&\ibl4h0\qb4{hk}\tql4h\ibu4j0\qb4{g^l}\tqu4{^e}\en
\bar%171
\anotes\qa{.K.G}&\ibu1h{-2}\qb1{hg}\tqu1f\ibu1g2\qb1{gh}\tqu1i&\ibu2h{-2}\qb2{hg}\tqu2f\ibu2g2\qb2{gh}\tqu2i&\qa{.r}\qpp&\ibu4f2\qb4{fg}\tqu4h\ibu4i{-5}\qb4{ig}\tqu4{^e}\en
\bar%172
\anotes\qa{.H.H}&\qa{.h.h}&\qa{.h.^j}&\Cpause&\ibu4j0\qb4{fk}\tqu4h\tr o\ibu4g{-2}\qb4{gf}\tqu4{^e}\en
\bar%173
\anotes\qa{.K}\qpp&\qa{.h}\qpp&\qa{.k}\qpp&\qa{.k}\qpp&\ibu4d2\qb4{d^e}\tqu4{^f}\qa f\ca f\en
\bar%174
\anotes\qa{.K}\qpp&\qa{.k}\ibl1i0\qb1{ji}\tql1j&\qa{.d}\qpp&\ibl3k2\qb3{k^l}\tql3{^m}\qa m\ca m&\ibu4f2\qb4{^fg}\tqu4h\qa h\ca h\en
\bar%175
\anotes\qa{.K.K}&\ibu1h{-2}\qb1{hg}\tqu1{^f}\ibu1f2\qb1{fg}\tqu1h&\itieu2k\ha{.k}&\ibl3m2\qb3{^mn}\tql3o\ibl3o{-2}\qb3{on}\tql3m&\ibl4h2\qb4{hi}\tql4j\ibl4j{-2}\qb4{ji}\tql4h\en
\bar%176
\anotes\qa{.G.J}&\qa{.g.g}&\ttie2\ibl2k{-2}\qb2{kj}\tql2i\qa j\ca j&\qa{.n.n}&\ibu4i{-2}\qb4{ih}\tqu4g\qa l\ca l\en
\bar%177
\anotes\qa{.H.I}&\qa{.c.d}&\qa{.j}\itieu2i\qa{.i}&\ha{.m}&\qa l\ca f\qa k\ca k\en
\bar%178
\anotes\ha{.G}&\ha{.g}&\ttie2\qa{.i}\qa{.j}&\ha{.l}&\qa k\ca e\qa j\ca j\en
\bar%179
\anotes\qa{.^F.G}&\qa{.h.i}&\qa{.h}\itied2g\qa{.g}&\itieu3k\qa{.k}\ttie3\qa k\ca k&\qa j\ca d\itieu4i\qa{.i}\en
\bar%180
\anotes\qa{.J.J}&\itieu1j\ha{.j}&\ttie2\ibu2g2\qb2{gh}\tqu2i\ibl2j2\qb2{jk}\tql2l&\qa l\ca k\tr n\qa j\ca i&\ttie4\qa{.i}\tr p\qa h\ca g\en
\bar%181
\anotes\qa{.K.G}&\ttie1\qa{.j}\ibu1i{-2}\qb1{ih}\tqu1g&\qa{.k.d}&\tr p\qa{.h}\itied3g\qa{.g}&\tr o\qa{.^f}\qa g\ca j\en
\bar%182
\anotes\qa{.K.K}&\qa{.d.k}&\qa{.g.^f}&\ttie3\qa{.g.k}&\qa{.i}\tr p\qa{.h}\en
\bar%183
\anotes\ca G\ibu0I4\qb0I\tqu0K\ibl0N{-4}\qb0{NL}\tql0K&\qa{.i}\qpp&\qa{.g}\qpp&\qa{.k}\itieu3n\qa{.n}&\qa{.g}\ds\ds\ca i\en
\bar%184
\anotes\qa J\ca K\qa L\ca K&\Cpause&\Cpause&\ttie3\qa{.n}\itieu3n\qa{.n}&\tinynotesize\qu i\qa{.h}\qa j\ca i\en
\bar%185
\anotes\qa{.J.K}&\Cpause&\Cpause&\ttie3\qa{.n}\itieu3n\qa{.n}&\tinynotesize\qu i\itied4h\qa{.h}\ttie4\qa h\ca i\en
\bar%186
\anotes\qa{.L.J}&\Cpause&\Cpause&\ttie3\qa{.n}\qa{.n}&\tinynotesize\qu i\qa{.h}\qa j\ca i\en
\bar%187
\anotes\qa{.K.K}&\ds\ibu1f2\qb1{^f}\tqu1g\ibu1h2\qb1{hi}\tqu1j&\qa{.d}\itieu2k\qa{.k}&\ca{^m}\ibl3k2\qb3k\tql3{^l}\ibl3m2\qb3{mn}\tql3o&\ca h\ibu4d2\qb4d\tqu4{^e}\ibu4f2\qb4{^fg}\tqu4h\en
\bar%188
\anotes\qa{.N.J}&\ibu1i{-2}\qb1{ih}\tqu1g\qa l\ca l&\ttie2\ibl2k{-2}\qb2{kj}\tql2i\qa j\ca j&\ibl3n2\qb3{no}\tql3p\ibl3m0\qb3n\HQsk\cna m\qb3m\tql3n&\ibl4i2\qb4{ij}\tql4k\qa l\ca l\en
\bar%189
\anotes\qa{.M.I}&\qa m\ca m\qa k\ca f&\qa{.j.i}&\ibl3m2\qb3{mn}\tql3o\ibl3o{-4}\qb3{pm}\tql3k&\ibl4l{-2}\qb4{lk}\tql4j\qa k\ca i\en
\bar%190
\anotes\qa{.L.M}&\qa{.g.j}&\qa{.g.f}&\qa{.l}\tr n\qa{.j}&\qa{.j}\tr p\qa{.h}\en
\bar%191
\anotes\qa{.I.I}&\ibu1i{-4}\qb1{if}\tqu1e\qa d\ca d&\qa f\ca i\qa g\ca k&\qa{.k}\itieu3n\qa{.n}&\qa{.i}\ds\ds\ca i\en
\bar%192
\anotes\ha{.J}&\itied1e\qa{.e}\ttie1\qa e\ca g&\qa j\ca i\qa h\ca g&\ttie3\qa{.n}\itieu3n\qa{.n}&\tinynotesize\qu i\qa{.h}\qa j\ca i\en
\bar%193
\anotes\ha{.K}&\itied1d\qa{.d}\ttie1\qa d\ca i&\qa k\ca d\qa k\ca g&\ttie3\qa{.n}\itieu3n\qa{.n}&\tinynotesize\qu i\itied4h\qa{.h}\ttie4\qa h\ca i\en
\bar%194
\anotes\qa{.L.J}&\qa{j}\ca k\qa l\ca n&\qa e\ca j\qa h\ca g&\ttie3\qa{.n}\qa{.n}&\tinynotesize\qu i\qa{.h}\qa j\ca i\en
\bar%195
\anotes\qa{.K}\qa I\ca J&\qa k\ca h\qa i\ca g&\itieu2k\qa{.k}\ttie2\qa k\itieu2n\ca n&\qa{.^m}\ibl3n{-2}\qb3{n=m}\tql3l&\qa h\ca k\qa g\ca j\en
\bar%196
\anotes\qa{.K.K}&\qa{.k.d}&\ttie2\ibl2m0\qb2{n^m}\tql2n\ibl2j0\qb2{kj}\tql2k&\qa{.k}\tr o\qa{.^m}&\qa{.i}\tr p\qa{.h}\en
\bar%197
\anotes\qa{.I.G}&\qa{.d}\qpp&\ibu2g0\qb2{g^f}\tqu2g\qpp&\qa{.n}\ibl3l{-2}\qb3{lk}\tql3{^j}&\qa{.g}\qpp\en
\bar%198
\anotes\qpp\qa{.N}&\qpp\qa{.g}&\qpp\qa{.k}&\qa{.k.n}&\qpp\qa{.i}\en
\bar%199
\anotes\qa{.^M}\qpp&\qa{.h}\qpp&\qa{.k}\qpp&\qa{.k}\ibl3l{-2}\qb3{lk}\tql3{^j}&\qa{.h}\qpp\en
\bar%200
\anotes\qpp\cna M\qa{.M}&\qpp\qa{.f}&\qpp\qa{.k}&\qa{.k.k}&\qpp\qa{.h}\en
\bar%201
\anotes\qa{^L}\qpp&\qa{.g}\qpp&\qa{.k}\qpp&\qa{.g}\ibl3k{-2}\qb3{kj}\tql3{^i}&\qa{.g}\qpp\en
\bar%202
\anotes\qpp\cna L\qa{.L}&\qpp\qa{.g}&\qpp\itieu2j\qa{.j}&\qa{.j}\qa{.n}&\qpp\qa{.g}\en
\bar%203
\anotes\ha{.K}&\qa{.h}\itied1h\qa{.h}&\ttie2\qa{.j}\qa{.h}&\ibl3m1\qb3{^mo}\tql3n\ibl3m{-2}\qb3{m^l}\tql3k&\qa{.d}\qpp\en
\bar%204
\anotes\ha{.^M}&\ttie1\ibu1i0\qb1{hj}\tqu1i\ibu1h2\qb1{hi}\tqu1j&\itieu2k\ha{.k}&\qa{.k}\itieu3o\qa{.o}&\ibl4j1\qb4{jl}\tql4k\ibl4j{-2}\qb4{ji}\tql4h\en
\bar%205
\anotes\qa{.N.I}&\itieu1i\qa{.i}\ttie1\ibl1i4\qb1{ik}\tql1n&\ttie2\ibl2j0\qb2{ki}\tql2j\ibl2k{-2}\qb2{kj}\tql2i&\ttie3\qa{.o}\itieu3n\qa{.n}&\ibl4i1\qb4{ik}\tql4j\ibu4i{-2}\qb4{ih}\tqu4g\en
\bar%206
\anotes\qa{.J.K}&\ibl1l{-2}\qb1{lk}\tql1j\tr n\ibl1j{-1}\qb1{ji}\tql1h&\qa{.h.d}&\ttie3\ibl3n{-2}\qb3{nm}\tql3l\itieu3k\qa{.k}&\qa{.h}\tr n\qa{.^f}\en
\bar%207
\anotes\qa{.L.J}&\qa{.g.g}&\qa{.g.h}&\ttie3\qa{.k}\ibl3q{-2}\qb3{qp}\tql3o&\ca g\ibl4k2\qb4k\tql4l\tr n\ibl4l{-2}\qb4{lk}\tql4j\en
\bar%208
\anotes\qa{.K.K}&\qa{.d.d}&\ibu2i{-2}\qb2{ih}\tqu2g\qa{.k}&\qa{.n}\tr o\qa{.^m}&\qa{.i}\tr p\qa{.h}\en
\Setvolta{1}%
\bar%209
\anotes\ca G\ibl0K5\qb0K\tql0N\ibl0N0\qb0{aN}\tql0a&\qa{.g}\qpp&\qa{.k}\qpp&\qa{.n}\qpp&\qa{.g}\qpp\en
\setrightrepeat%
\setendvoltabox%
\Setvolta{2}%
\bar%210
\anotes\qa{.G}\qpp&\qa{.g}\qpp&\qa{.k}\qpp&\qa{.n}\qpp&\qa{.g}\qpp\en
\setendvoltabox%
\mulooseness-2%
\Endpiece
\eject
\end
