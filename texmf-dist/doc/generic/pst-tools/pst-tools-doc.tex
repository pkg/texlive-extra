\RequirePackage{pdfmanagement-testphase}
\DeclareDocumentMetadata{}
%% $Id: pst-tools-doc.tex 256 2021-09-22 18:42:59Z herbert $
\documentclass[fontsize=11pt,english,BCOR=10mm,DIV=12,bibliography=totoc,parskip=false,headings=small,
    headinclude=false,footinclude=false,oneside]{pst-doc}
\listfiles
\usepackage{pstricks}
\usepackage{pst-tools}
\let\pstToolsFV\fileversion
\usepackage{xltabular}
\usepackage{pst-plot}
\renewcommand\bgImage{}

\lstset{language=PSTricks,
    morekeywords={psPrintValue},basicstyle=\footnotesize\ttfamily}
%
\usepackage{biblatex}
\addbibresource{pst-tools-doc.bib}
\begin{document}

\title{\texttt{pst-tools}}
\subtitle{Helper functions; v.\pstToolsFV}
\author{Herbert Voß}
\docauthor{}
\settitle

\begin{abstract}
This package defines some tools which are useful for all packages not only the PSTricks like packages.
Since the version 0.10 it includes the macros from \texttt{random.tex}.
\vfill
\noindent
Thanks to:  
Marcel Krüger;
Pablo Gonzáles Luengo;
Rolf Niepraschk;
\end{abstract}

\newpage

\tableofcontents
\psset{unit=1cm}


\section{Predefined styles}

The style \Lkeyword{mmpaper} is defined for \Lcs{psgrid}:
%\newpsstyle{mmpaper}{subgriddiv=5,gridlabels=0,gridwidth=1pt,gridcolor=orange,subgridwidth=0.1pt,subgridcolor=orange!90}

\begin{LTXexample}[width=0.5\linewidth,frame=,pos=r]
\begin{pspicture}(6,3)
\psgrid[style=mmpaper](6,3)
\end{pspicture}

\begin{pspicture}(6,3)
\psgrid[style=mmpaper,
  gridcolor=blue,subgridcolor=blue!80](6,3)
\end{pspicture}
\end{LTXexample}


\section{\Lcs{psPrintValue}}\label{sec:printValue}
This macro allows to \Index{print} single values of a math function. It has the syntax

\begin{BDef}
\Lcs{psPrintValue}\OptArgs\Largb{PostScript code}\\
\Lcs{psPrintValue}\OptArg{algebraic,\ldots}\Largb{x value, algebraic code}
\end{BDef}

Important is the fact, that \Lcs{psPrintValue} works on \PS\ side. For \TeX\ it is only a box of
zero dimension. This is the reason why you have to put it into a box, which reserves horizontal
space.


There are the following valid options for \Lcs{psPrintValue}:

\noindent\medskip
\begin{xltabular}{\linewidth}{@{}l|>{\ttfamily}l>{\ttfamily}lX@{}}
\textrm{name} & \textrm{value}  & \textrm{default}\\\hline
\endhead
\Lkeyword{printfont}        & font name & Times & only the current font (\texttt{printfont={}}) 
or valid \PS\ font names are possible, e.g. 
    \Lps{Times-Roman}, \Lps{Helvetica}, \Lps{Courier}, \Lps{Helvetica}, \Lps{Bookman}. If you want to embed the fonts
    use always the URW names NimbusRomNo9L-Regu, NimbusSanL-Regu and  NimbusMonL-Regu. However, the names
may vary on different operating systems. If you leave the argument empty, it will choose the currently active font.\\
\Lkeyword{postString} & <string>     & \{\}     & will be appended to the number string\\
\Lkeyword{trimSpaces} & <boolean>     & false     & will strip spaces on the right\\
\Lkeyword{fontscale} & <number>     & 10     & the font scale in pt\\
\Lkeyword{valuewidth} & <number>     & 10     & the width of the string for the converted 
    real number; if it is too small, no value is printed\\
\Lkeyword{decimals}  & <number>     & -1     & the number of printed decimals, a negative value
    prints all possible digits.\\ 
\Lkeyword{xShift}  & <number>     & 0     & the x shift in pt for the output, relative to the current point.\\ 
\Lkeyword{algebraic}  & <boolean>     & false     & function in algebraic notation.\\ 
\Lkeyword{VarName} & <string> & \{\} & saves the value in /<VarName> for further use\\
\Lkeyword{comma}  & <boolean>     & false     & comma instead of the dor for decimals\\ 
\end{xltabular}




\begin{center}
\psset{fontscale=12}
\makebox[2em]{x(deg)} \makebox[5em]{$\sin x$} \makebox[4em]{$\cos x$}\hspace{1em}
\makebox[5em]{$\sqrt x$}\makebox[7em]{$\sin x+\cos x$}\makebox[6em]{$\sin^2 x+\cos^2 x$}\\[3pt]
\multido{\iA=0+10}{18}{%
  \makebox[1em]{\iA}
  \makebox[5em]{\psPrintValue[printfont=NimbusRomNo9L-Regu,xShift=-10]{\iA\space sin}}
  \makebox[4em][r]{\psPrintValue[printfont={},fontscale=10,decimals=3,xShift=-20]{\iA\space cos}}\hspace{1em}
  \makebox[5em]{\psPrintValue[valuewidth=15,linecolor=blue,printfont=NimbusSanL-Regu]{\iA\space sqrt}}
  \makebox[7em]{\psPrintValue[comma,printfont=NimbusRomNo9L-ReguItal]{\iA\space dup sin exch cos add}}
  \makebox[6em]{\psPrintValue[printfont=Palatino-Roman]{\iA\space dup sin dup mul exch cos dup mul add}}\\
}
\end{center}


\bigskip

\begin{lstlisting}
\psset{fontscale=12}
\makebox[2em]{x(deg)} \makebox[5em]{$\sin x$} \makebox[4em]{$\cos x$}\hspace{1em}
\makebox[5em]{$\sqrt x$}\makebox[7em]{$\sin x+\cos x$}\makebox[6em]{$\sin^2 x+\cos^2 x$}\\[3pt]
\multido{\iA=0+10}{18}{
  \makebox[1em]{\iA}
  \makebox[5em]{\psPrintValue[printfont=NimbusRomNo9L-Regu,xShift=-10]{\iA\space sin}}
  \makebox[4em][r]{\psPrintValue[printfont={},fontscale=10,decimals=3,xShift=-20]{\iA\space cos}}\hspace{1em}
  \makebox[5em]{\psPrintValue[valuewidth=15,linecolor=blue,printfont=NimbusSanL-Regu]{\iA\space sqrt}}
  \makebox[7em]{\psPrintValue[comma,printfont=NimbusRomNo9L-ReguItal]{\iA\space dup sin exch cos add}}
  \makebox[6em]{\psPrintValue[printfont=Palatino-Roman]{\iA\space dup sin dup mul exch cos dup mul add}}\\}
\end{lstlisting}




With enabled \Lkeyword{algebraic} option there must be two arguments, separated by a comma.
The first one is the x value as a number, which can also be PostScript code, which leaves a
number on the stack. The second part is the function described in algebraic notation.
Pay attention, in algebraic notation angles must be in radian and not degrees.


\clearpage


\begin{center}
\psset{algebraic, fontscale=12}% All functions now in algebraic notation
\makebox[2em]{x(deg)} \makebox[5em]{$\sin x$} \makebox[4em]{$\cos x$}\hspace{1em}
\makebox[5em]{$\sqrt x$}\makebox[7em]{$\sin x+\cos x$}\makebox[6em]{$\sin^2 x+\cos^2 x$}\\[3pt]
\multido{\rA=0+0.1}{18}{\makebox[1em]{\rA}
  \makebox[5em]{\psPrintValue[printfont=NimbusSanL-Regu,xShift=-10]{\rA, sin(x)}}
  \makebox[4em][r]{\psPrintValue[printfont={},fontscale=10,decimals=3,xShift=-20]{\rA,cos(x)}}\hspace{1em}
  \makebox[5em]{\psPrintValue[valuewidth=15,linecolor=blue,printfont=NimbusSanL-Regu]{\rA,sqrt(x)}}
  \makebox[7em]{\psPrintValue[comma,printfont=NimbusRomNo9L-ReguItal]{\rA,sin(x)+cos(x)}}
  \makebox[6em]{\psPrintValue[printfont=Palatino-Roman]{\rA,sin(x)^2+cos(x)^2}}\\}
\end{center}

\bigskip

\begin{lstlisting}
\psset{algebraic, fontscale=12}% All functions now in algebraic notation
\makebox[2em]{x(deg)} \makebox[5em]{$\sin x$} \makebox[4em]{$\cos x$}\hspace{1em}
\makebox[5em]{$\sqrt x$}\makebox[7em]{$\sin x+\cos x$}\makebox[6em]{$\sin^2 x+\cos^2 x$}\\[3pt]
\multido{\rA=0+0.1}{18}{\makebox[1em]{\rA}
  \makebox[5em]{\psPrintValue[printfont=NimbusSanL-Regu,xShift=-10]{\rA, sin(x)}}
  \makebox[4em][r]{\psPrintValue[printfont={},fontscale=10,decimals=3,xShift=-20]{\rA,cos(x)}}\hspace{1em}
  \makebox[5em]{\psPrintValue[valuewidth=15,linecolor=blue,printfont=NimbusSanL-Regu]{\rA,sqrt(x)}}
  \makebox[7em]{\psPrintValue[comma,printfont=NimbusRomNo9L-ReguItal]{\rA,sin(x)+cos(x)}}
  \makebox[6em]{\psPrintValue[printfont=Palatino-Roman]{\rA,sin(x)^2+cos(x)^2}}\\}
\end{lstlisting}



\begin{center}
foo \makebox[2em][l]{\psPrintValue[comma]{3.14 10 mul round 10 div}}bar\\[3pt]
foo \makebox[2em][l]{\psPrintValue[comma,printfont=StandardSymL,
  postString=\string\260]{3.14 10 mul round 10 div}}bar\\[3pt]
foo \makebox[3.5em][l]{\psPrintValue[printfont=StandardSymL,decimals=6,
  postString=\string\260]{3.14 dup mul}}bar
\end{center}

\bigskip

\begin{lstlisting}
foo \makebox[2em][l]{\psPrintValue[comma]{3.14 10 mul round 10 div}}bar\\[3pt]
foo \makebox[2em][l]{\psPrintValue[comma,printfont=StandardSymL,
  postString=\string\260]{3.14 10 mul round 10 div}}bar\\[3pt]
foo \makebox[3.5em][l]{\psPrintValue[printfont=StandardSymL,decimals=6,
  postString=\string\260]{3.14 dup mul}}bar
\end{lstlisting}


\clearpage

\section{\nxLcs{psRegisterList}}\label{sec:getElement}
The macro defines for every list item an own macro for an easy access to the items. 
It must be a comma separated list.

\begin{BDef}
\Lcs{psRegisterList}\Largb{Name}\Largb{value list}\\
\nxLcs{\Larga{Name}}\Largb{Index}
\end{BDef}


\begin{lstlisting}
\psRegisterList{Color}{yellow,blue,green,red}% defines macro \Color
\begin{pspicture}(-7,-4.5)(7,5.5)
\psaxes{->}(0,0)(-6.5,-4.5)(6.75,5)
\psset{plotpoints=400,algebraic,linewidth=1pt,fillstyle=solid,opacity=0.4}
\multido{\iA=1+1}{4}{%
  \psplot[linecolor=\Color{\iA},
     fillcolor=\Color{\iA}!60]{-6.283}{6.283}{\iA*sin(\iA*x)}}%
\psset{plotpoints=400,algebraic}
\psforeach{\iA}{1,2,3,4}{%
  \psplot[linecolor=\Color{\iA}]{-6.28}{6.28}{\iA*sin(\iA*x)}}
\end{pspicture}
\end{lstlisting}

\psRegisterList{Color}{yellow,blue,green,red}% defines macro \Color
\begin{pspicture}(-7,-4.5)(7,5.5)
\psaxes{->}(0,0)(-6.5,-4.5)(6.75,5)
\psset{plotpoints=400,algebraic,linewidth=1pt,fillstyle=solid,opacity=0.4}
\multido{\iA=1+1}{4}{%
	\psplot[linecolor=\Color{\iA},fillcolor=\Color{\iA}!60]{-6.283}{6.283}{\iA*sin(\iA*x)}}%
\psset{plotpoints=400,algebraic}
\psforeach{\iA}{1,2,3,4}{%
  \psplot[linecolor=\Color{\iA}]{-6.28}{6.28}{\iA*sin(\iA*x)}}
\end{pspicture}



\section{Random numbers}
The file \LFile{random.tex} from Donald Arseneau is no more part of CTAN due to a missing licence statement. 
\LFile{pst-tools} at version 0.10 includes the code. The documentation was inside the package otself:

Random integers are generated in the range 1 to 2147483646 by the
macro \Lcs{nextrandom}.  The result is returned in the counter \Lcs{randomi}.
Do not change \Lcs{randomi} except, perhaps, to initialize it at some
random value.  If you do not initialize it, it will be initialized
using the time and date.  (This is a sparse initialization, giving
fewer than a million different starting values, but you should use
other sources of numbers if they are available--just remember that
most of the numbers available to TeX are not at all random.)

The \Lcs{nextrandom} command is not very useful by itself, unless you
have exactly 2147483646 things to choose from.  Much more useful
is the \Lcs{setrannum} command which sets a given counter to a random
value within a specified range.  There are three parameters:

\Lcs{setrannum\{<counter>\}\{<minimum>\}\{<maximum>\}}  

For example, to
simulate a die-roll: 

\verb|\setrannum{\die}{1}{6}| \verb|\ifcase\die...| .

If you need random numbers that are not integers, you will have to
use dimen registers and \Lcs{setrandimen}.  For example, to set a random
page width: 

\Lcs{setrandimen} \Lcs{hsize\{3in\}\{6.5in\}}

 The »\Lcs{pointless}« macro
will remove the »pt« that TeX gives so you can use the dimensions
as pure `real' numbers.  In that case, specify the range in pt units.
For example,

  \verb|\setrandimen\answer{2.71828pt}{3.14159pt}|

  The answer is \verb|\pointless\answer|.

The random number generator is the one by Lewis, Goodman, and Miller
(1969) and used as \texttt{ran0} in »Numerical Recipies« using Schrage's
method for avoiding overflows.  The multiplier is $16807 (7^5)$, the
added constant is 0, and the modulus is $2147483647 (2^{31}-1)$.  The
range of integers generated is $1 - 2147483646$.  A smaller range would
reduce the complexity of the macros a bit, but not much--most of the
code deals with initialization and type-conversion.  On the other hand,
the large range may be wasted due to the sparse seed initialization.



\section{List of the defined PostScript functions}

\footnotesize
\begin{verbatim}
/Pi2 1.57079632679489661925640 def
/factorial { % n on stack, returns n! 
/MoverN { % m n on stack, returns the binomial coefficient m over n
/ps@ReverseOrderOfPoints { % on stack [P1 P2 P3 ...Pn]=>[Pn,Pn-1,...,P2,P1]
/cxadd {		% [a1 b1] [a2 b2] = [a1+a2 b1+b2]
/cxneg {		% [a b]
/cxsub { cxneg cxadd } def  % same as negative addition
/cxmul {		% [a1 b1] [a2 b2]
/cxsqr {		% % [a b]^2 = [a^2-b^2 2ab] = [a2 b2]
/cxsqrt {		% 
/cxarg { 		% [a b]->arg(z)=atan(b/a)
/cxlog {		% [a b]->log[a b] = [a^2-b^2 2ab] = [a2 b2]
/cxnorm2 {		% [a b]->a^2+b^2
/cxnorm {		% 
/cxconj {		% [a b]->[a -b]
/cxre { 0 get } def	% real value
/cxim { 1 get } def	% imag value
/cxrecip {		% [a b]->1/[a b] = ([a -b]/(a^2+b^2)
/cxmake1 { 0 2 array astore } def % make a complex number, real given
/cxmake2 { 2 array astore } def	  % dito, both given
/cxdiv { cxrecip cxmul } def
/cxrmul {		% [a b] r->[r*a r*b]
/cxrdiv {		% [a b] r->[1/r*a 1/r*b]
/cxconv {		% theta->exp(i theta) = cos(theta)+i sin(theta) polar<->cartesian
/bubblesort { % on stack must be an array [ ... ]
/concatstringarray{  %  [(a) (b) ... (z)] --> (ab...z)  20100422
/concatstrings{ % (a) (b) -> (ab)  
/reversestring { % (aBC) -> (CBa)
/concatarray{ % [a c] [b d] -> [a c b d]  
/dot2comma {% on stack a string (...) 
/rightTrim { % on stack the string and the character number to be stripped  
/psStringwidth /stringwidth load def
/psShow /show load def
\end{verbatim}

\normalsize

\clearpage
\section{List of all optional arguments for \texttt{pst-tools}}

\xkvview{family=pst-tools,columns={key,type,default}}




\bgroup
\raggedright
\nocite{*}
%\bibliographystyle{plain}
\printbibliography
\egroup

\printindex



\end{document}

