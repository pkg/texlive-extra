% labels.doc  --- version 1.0 (24 October 1991)
% This is the documentation for labels.tex.
% To read this file, process it with (plain) TeX: tex labels.doc
% (To do that, you need manmac.tex, which has
% the macros for typesetting the TeXbook, as explained in appendix E.
% It comes free with the standard distribution of TeX.)
%
% NOTE: Processing this file with TeX will create empty files
%       index.tex and answers.tex in your directory. And if you
%	have a file pages.tex in your directory, TeX will probably
%	get very confused.
%	The manmac format will try to load a font called cmman; if
%	TeX complains that it cannot find that font you can savely
%	ignore that, since the font is not actually used.
%
% This file: copyright (c) 1991  Marcel van der Goot.
%	You may redistribute this file and printed versions of it.
%
% original: csvax.cs.caltech.edu [131.215.131.131] in pub/tex
%           (use anonymous ftp). Also in various archives.
%
% Marcel van der Goot
% marcel@cs.caltech.edu
%   Caltech 256--80
%   Pasadena, CA 91125
%   (818) 356--4603

% Doc history:
%	10 Sep 1991: Added paragraph about centering labels.
%	22 Apr 1991: First version

% ------ definitions for manual pages:
% The documentation is typeset using Knuth's ``manmac'' macros, with a
% few additional things defined here. You are free to use or adapt these
% extra definitions.

\input manmac % the macros used for typesetting The TeXbook (appendix E)
\catcode`^=7 % manmac makes ^ active for creating an index
	% we don't make an index, and we need ^ to be non-active

{\catcode`\^^M=12 \endlinechar=-1 % 12 = other
\long\gdef\manpagehead#1^^M#2^^M#3^^M^^M% tex-file, description, version
   {\egroup % matches \bgroup in \manpage
    \par\vfill\eject
    \edef\rhead{\manualname: #1\unskip}%
    \setbox1=\hbox{\titlefont #1\quad}%
    \setbox3=\vtop{\advance\hsize by-\wd1 \parindent=0pt \raggedright
		   #2\par
		   #3\par
		   \cnotice
		  }%
    \line{\box1\box3}%
    \bigskip\bigskip\bigskip\noindent
   }%
} % restore \catcode`\^^M

\outer\def\manpage
   {\bgroup\catcode`\^^M=12
    \manpagehead
   }

\hsize=6.5in\vsize=8.9in % default plain TeX values
\pagewidth=\hsize \pageheight=\vsize

\def\cnotice{Copyright \copyright\ 1991 Marcel R. van der Goot}
\def\manualname{Midnight Macros}

% LaTeX logo, from latex.tex (except for the capital `A': manmac
% doesn't have a real smallcaps font)
\def\LaTeX{{\rm L\kern-.36em\raise.3ex\hbox{\sc A}\kern-.15em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\tenpoint

% (end of extra definitions)

% -------------------- labels.doc:

\pageno=12 % follows dolines.doc

\manpage labels.tex
Macros to print address labels and bulk letters.
Version: 1.0 (24 October 1991) (doc: 10 Sep 1991)

The macros defined in labels.tex can be used to print address labels. Such
labels can be printed on normal paper, if you want to cut them yourself,
or on peel-off labels. (Peel-off labels are available in various sizes for
most types of printers and photo-copiers.)
In order to use the macros
you always have to start with |\input labels|, which reads
labels.tex\footnote*{To use labels.tex you also need Midnight/dolines.tex and
Midnight/loop.tex, which are input automatically.}.
We will first explain how to
typeset labels, then how to print bulk letters.

There are two slightly different ways to print labels. The first one is to say
\begintt
\beginlabels
...
\endlabels
\endtt
where in place of `|...|' you type the labels separated by empty lines.
The second is to simply say
\begintt
\labelfile{...}
\endtt
where `|...|' is the name of a file that contains the labels. In either case
a label consists of several lines, and labels are separated by one
or more empty lines. You end your file as usual, with |\bye|.
Labels.tex does not specify which font to use, which
in plain \TeX\ means that by default a 10 points Roman font is used.
However, for legibility
it is usually better to use a sans-serif font, for instance by writing
\begintt
\font\sf=cmss12
\sf \baselineskip=14pt
\endtt
In order to typeset labels, you must define the size and other attributes
of labels. (There are default values, but they are
unlikely to suit your needs, especially if you want to print on
peel-off labels.) These attributes must be set before you call
|\beginlabels| or |\labelfile|; therefore, a typical input file has the form
\beginlines
|\input labels|
\nobreak
|\vlbls=5 \hlbls=2|
\goodbreak
|\vlblsize=5cm|
|\labelfile{customers.lbl}|
\nobreak
|\bye|
\endlines
where file `customers.lbl' has the addresses that must be typeset. The
assignments to |\vlbls|, |\hlbls|, and |\vlblsize| define certain attributes.
Next we describe which attributes there are.

First you have to tell \TeX\ how labels must appear on a page. |\vlbls|
must denote the vertical number of labels per page, |\hlbls| the horizontal
number. |\vfirst| specifies the amount of white space between the top of the
page and the top of the first label; |\hfirst| is the amount of white space
to the left of the first label. Dimensions |\vinter| and |\hinter| specify
the white space between labels.

Then you have to specify the size of each label. |\vlblsize| and |\hlblsize|
specify the vertical and horizontal sizes of a single label. |\vindent| is
the white space between the top of the label and the baseline of the first
line. (|\vindent| is similar to \TeX's |\topskip| parameter.) |\hindent|
is the amount of white space between the left edge of a label and the
text of the label.

The parameters mentioned above are sufficient to typeset labels. There
are however a few special parameters that are sometimes useful.
By default, there is no outline around the labels. That is suitable for
printing on peel-off labels, but inconvenient for use with previewers or
plain paper. You can set |\lbloutline| to the thickness of the lines
you want around your labels, e.g., |\lbloutline=0.5pt|. The outlines
are drawn at the outside of the label (i.e., in the space between the
labels).

If there is too much text on a label, an error message is given; for reference,
the message contains the input text corresponding to the label. A label
with an error is not typeset.
When you typeset labels, by default a file `lblerror.tex' is created, which
has the text of all the labels that did not fit. This makes it convenient to
process those labels again, e.g., with |\labelfile|.
However, if you set |\erroraction=0| before you start typesetting labels,
file `lblerror.tex' is not created. Setting |\erroraction=1| does not only
suppress generation of the error file, it also forces printing of the labels
that have errors. Any other value of |\erroraction| causes the default
behavior.

\danger The error messages refer to the natural size of a label. If
|\vindent| and |\hindent|, which are glue parameters, have shrink components,
it is possible that the text will actually fit on the label. However, usually
these indentations have no stretch or shrink components.

\danger There are two token registers, |\beforelbl| and |\afterlbl|, that
can be used to put additional text on each label.
They contain \<vertical material> that is inserted just before and after
the text of the label, respectively. By default, these sequences are empty.
One can for instance specify
\begintt
\beforelbl={\line{---Confidential---\hfil}\smallskip}
\endtt
to put ``---Confidential---'' at the beginning of each address. {\sl Note:\/}
Since addresses are normally read from bottom to top, the post office
prefers it if you don't put text after the address, unless it is part
of the address. E.g., using |\afterlbl={\noindent USA}| is ok, but use
|\beforelbl| if you want to add an ``attention:\dots'' line to each label.

\danger If you want to use the labels for something else than addresses, you
may want to center the text on the label. Horizontal centering can be obtained
with |\hindent=0pt plus1fil|. Note that this centers the text according to
the widest line; it does not center each line separately.
Vertical centering is a bit more complicated, because of the way \TeX\ stacks
boxes vertically. The following centers the label text vertically:
\begintt
\count255=\baselineskip
\vindent=\count255sp plus1fil
\endtt
This works because the actual vertical indentation is |\vindent| minus
|\baselineskip|, since \TeX\ automatically inserts |\baselineskip| glue.

Within a label, you can type `|&|' and `|#|' for
`\&' and `\#'. (In \TeX, you usually have to type `|\&|' and `|\#|' to get
these symbols.) You can use macros in a label, for instance for font changes.
A line of a label must contain balanced braces; a `|%|' sign
will concatenate the next line. (Usually, labels do not contain macros, nor
braces, nor percentage-signs.)
If for some reason you want an empty line in the middle of a label,
write a `tie' (`|~|') on that line. (The |~| is replaced by a space in the
output.)

Address labels are often used to send an identical letter to each addressee.
The command |\bulk| makes it easy to print the address on each letter. You
need two files, one with all the addresses, and one with the standard letter.
Say that they are called `addr.tex' and `letter.tex', respectively. The command
\begintt
\bulk{addr}{letter}
\endtt
will typeset a copy of the letter for each address. In the letter you can
use the control sequence |\bulkaddress|. This stands for a vbox that contains
the address to which the letter corresponds. (The vbox contains an hbox for
each line of the address; parameters like |\vindent| and |\hlblsize| are not
important when |\bulk| is used.) Since `letter.tex' is processed many times,
it is best to put any definitions that apply to all letters in the file
that contains the |\bulk| command, rather than in the letter itself.
Each letter is read within a group, so all non-global definitions and
assignments in the letter are local to the letter. Also note that the
vbox containing the address is typeset before the letter is read.
The |\bulk| command does not generate address labels; for that you have
to use |\labelfile{addr}|.

\bye
