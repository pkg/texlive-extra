# pst-shell: plotting sea shells


Argonauta, Epiteonium, Lyria, Turritella, Tonna, Achatina,
Oxystele, Conus, Ammonite, Codakia, Escalaria, Helcion, Natalina, Planorbis,
and Nautilus, all with different parameters.

Save the files pst-shell.sty|pro|tex in a directory, which is part of your 
local TeX tree. The pro file should go into $TEXMF/dvips/pstricks/
Then do not forget to run texhash to update this tree.
For more information  see the documentation of your LATEX distribution 
on installing packages into your local TeX system or read the 
TeX Frequently Asked Questions:
(http://www.tex.ac.uk/cgi-bin/texfaq2html?label=instpackages).

pst-shell needs pst-solides3d and an up-to-date pstricks, which should 
be part of your local TeX installation, otherwise get it from a 
CTAN server, http://mirror.ctan.org

PSTricks is PostScript Tricks, the documentation cannot be run
with pdftex, use the sequence latex->dvips->ps2pdf or run it with xelatex.

This program can be redistributed and/or modified under the terms
of the LaTeX Project Public License Distributed from CTAN archives
in directory macros/latex/base/lppl.txt.

%% $Id: README.md 372 2016-12-27 21:40:59Z herbert $
