# **README** #
# pst-marble v. 1.6 #
# 2019/05/10 #

    Source:      pst-marble.tex, pst-marble.sty, pst-marble.pro
    Author:      Aubrey Jaffer
    Maintainers: Jürgen Gilg, Manuel Luque
    Info:        Draw marble-like patterns with PSTricks
    License:     LPPL 1.3c

---

# Short description #

The mathematical fascination with *ink marbling* is that while rakings across 
the tank stretch and deform the ink boundaries, they do not break or change 
the topology of the surface.  With mechanical guides, a raking can be undone 
by reversing the motion of the rake to its original position.  
Raking is thus a physical manifestation of a homeomorphism, a continuous 
function between topological spaces (in this case between a topological space 
and itself) that has a continuous inverse function.

The package comes with 15 predefined *basic actions* like:

- drop
- line-drops
- serpentine-drops
- coil-drops
- normal-drops
- uniform-drops
- concentric-rings
- rake
- stylus
- stir
- vortex
- jiggle
- wriggle
- shift
- turn

Also some *spray-actions* (post-marble actions) like:

- normal-spray
- uniform-spray 

Also a *shadings* option to generate shading effects (post-marble actions)

With these commands one can create quite artistic marble-like patterns following
the rules of fluid mechanics.
 

