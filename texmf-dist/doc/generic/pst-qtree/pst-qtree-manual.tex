\documentclass[11pt]{article}

\usepackage{txfonts}
\usepackage{fullpage}

\usepackage{pstricks}
\usepackage{pst-qtree}

\usepackage{graphicx}

\newcommand{\1}{\ensuremath{'}}
\usepackage{comment}

\title{\texttt{pst-qtree}: a Qtree-like front end for \texttt{pst-tree}}
\author{David Chiang}

\begin{document}

\maketitle

Basic trees are specified exactly as in Qtree:
\begin{verbatim}
\Tree [.S [.NP [.D the ] [.N cat ] ] 
          [.VP [.V sat ] 
               [.PP [.P on ] 
                    [.NP [.D the ] [.N mat ] ] ] ] ]
\end{verbatim}
\begin{center}
\Tree [.S [.NP [.D the ] [.N cat ] ] [.VP [.V sat ] [.PP [.P on ] [.NP [.D the ] [.N mat ] ] ] ] ]
\end{center}

If a node label starts with \verb|!|, then the node is not
automatically wrapped inside a \verb|\psnode| for you. You may use a
different node macro, and can pass options to it:

\begin{verbatim}
\newcommand{\dottededge}{\ncdiag[arm=0,angleA=-90,angleB=90,linestyle=dotted]}

\Tree [.S [.NP [.D the ] [.N cat ] ] 
          [.VP [.V sat ] 
               [.!\TR[edge=\dottededge]{PP} [.P on ] 
                                            [.NP [.D the ] [.N mat ] ] ] ] ]
\end{verbatim}

\newcommand{\dottededge}{\ncdiag[arm=0,angleA=-90,angleB=90,linestyle=dotted]}

\begin{center}
\Tree [.S [.NP [.D the ] [.N cat ] ] [.VP [.V sat ] [.!\TR[edge=\dottededge]{PP} [.P on ] [.NP [.D the ] [.N mat ] ] ] ] ]
\end{center}

One special type of (large) node defined by \verb|pst-qtree| is
\verb|\Troof|, which is similar to Qtree's
\verb|\qroof| but not identical in use.
\begin{verbatim}
\Tree [.S [.NP !\Troof{the cat} ] 
          [.VP [.V sat ] 
               [.PP [.P on ] 
                    [.NP !\Troof{the mat } ] ] ] ]
\end{verbatim}
\begin{center}
\Tree [.S [.NP !\Troof{the cat} ] 
          [.VP [.V sat ] 
               [.PP [.P on ] 
                    [.NP !\Troof{the mat } ] ] ] ]
\end{center}




\pagebreak
Arrows can be drawn between nodes using commands from
{\tt pstricks}: assign a label to each node using \verb|\rnode|,
then connect them using node connection commands such as
\verb|\nccurve| or \verb|\ncangle|.

{\small
\begin{verbatim}
\Tree 
[.TP [.NP \rnode{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\rnode{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \rnode{io}{ }IO$_l$
                 [.{\it v}\1 \rnode{subj2}t$_i$ 
                   [.AplP \rnode{v1}t$_n$  
                     [.Apl\1 \rnode{do}DO$_m$  
                       [.{\it v}\1  \rnode{io1}t$_l$
                         [.Apl\1 \rnode{apl1}t$_k$ 
                           [.VP [.V \rnode{V1}t$_j$ ]
                                \rnode{do1}t$_m$  ] ] ] ] ] ] ] ] ]
\psset{linewidth=0.3pt,arrowsize=4pt}
\psset{angleA=180,angleB=-90}
\nccurve{->}{subj2}{subj1}
\nccurve{->}{do1}{do}
\nccurve[linestyle=dashed]{->}{io1}{io}
\nccurve{->}{V1}{apl1}
\nccurve{->}{apl1}{v1}
\nccurve{->}{v1}{V}
\end{verbatim}
}

\resizebox{4in}{!}{
\Tree 
[.TP [.NP \rnode{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\rnode{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \rnode{io}{ }IO$_l$
                 [.{\it v}\1 \rnode{subj2}t$_i$ 
                   [.AplP \rnode{v1}t$_n$  
                     [.Apl\1 \rnode{do}DO$_m$  
                       [.{\it v}\1  \rnode{io1}t$_l$
                         [.Apl\1 \rnode{apl1}t$_k$ 
                           [.VP [.V \rnode{V1}t$_j$ ]
                                \rnode{do1}t$_m$  ] ] ] ] ] ] ] ] ]
\psset{linewidth=0.3pt,arrowsize=4pt}
\psset{angleA=180,angleB=-90}
\nccurve{->}{subj2}{subj1}
\nccurve{->}{do1}{do}
\nccurve[linestyle=dashed]{->}{io1}{io}
\nccurve{->}{V1}{apl1}
\nccurve{->}{apl1}{v1}
\nccurve{->}{v1}{V}
}

\pagebreak
Arrows can be
labeled using \verb|\lput{:U}| (on top of the arrow),
or \verb|\taput| \verb|\tbput| \verb|\tlput| or \verb|\trput|
(above, below, left of, or right of the arrow)
after the \verb|\nccurve| or \verb|\ncangle|
command.

{\small
\begin{verbatim}
\Tree 
[.TP [.NP \rnode{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\rnode{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \rnode{io}{ }IO$_l$
                 [.{\it v}\1 \rnode{subj2}t$_i$ 
                   [.AplP \rnode{v1}t$_n$  
                     [.Apl\1 \rnode{do}DO$_m$  
                       [.{\it v}\1  \rnode{io1}t$_l$
                         [.Apl\1 \rnode{apl1}t$_k$ 
                           [.VP [.V \rnode{V1}t$_j$ ]
                                \rnode{do1}t$_m$  ] ] ] ] ] ] ] ] ]
\psset{linewidth=0.3pt,arrowsize=4pt}
\psset{angleA=180,angleB=-90}
\nccurve{->}{subj2}{subj1}\lput{:U}{X}
\nccurve{->}{v1}{V}\tbput{below}
\end{verbatim}
}

\resizebox{4in}{!}{
\Tree 
[.TP [.NP \rnode{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\rnode{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \rnode{io}{ }IO$_l$
                 [.{\it v}\1 \rnode{subj2}t$_i$ 
                   [.AplP \rnode{v1}t$_n$  
                     [.Apl\1 \rnode{do}DO$_m$  
                       [.{\it v}\1  \rnode{io1}t$_l$
                         [.Apl\1 \rnode{apl1}t$_k$ 
                           [.VP [.V \rnode{V1}t$_j$ ]
                                \rnode{do1}t$_m$  ] ] ] ] ] ] ] ] ]
\psset{linewidth=0.3pt,arrowsize=4pt}
\psset{angleA=180,angleB=-90}
\nccurve{->}{subj2}{subj1}\lput{:U}{X}
\nccurve{->}{v1}{V}\tbput{below}
}


\begin{comment}
\pagebreak
Arrows can also be drawn using the {\tt tree-dvips} 
package from Emma Pease:

{\small
\begin{verbatim}
\Tree 
[.TP [.NP \node{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\node{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \node{io}{ }IO$_l$
               [.{\it v}\1 \node{subj2}t$_i$ 
                 [.AplP \node{v1}t$_n$  
                   [.Apl\1 \node{do}DO$_m$ 
                     [.{\it v}\1 \node{io1}t$_l$
                       [.Apl\1 \node{apl1}t$_k$ 
                         [.VP [.V \node{V1}t$_j$ ]
                              \node{do1}t$_m$  ] ] ] ] ] ] ] ] ]

\anodecurve[bl]{subj2}[bl]{subj1}{0.4in}
\anodecurve[bl]{do1}[bl]{do}{0.4in}
{\makedash{4pt}\anodecurve[t]{io1}[r]{io}{.5in}}
\anodecurve[bl]{V1}[bl]{apl1}{0.6in}
\anodecurve[bl]{apl1}[bl]{v1}{1in}
\anodecurve[bl]{v1}[bl]{V}{0.9in}
\end{verbatim} 

\resizebox{4in}{!}{
\Tree 
[.TP [.NP \node{subj1}subj$_i$ ] 
     [.T\1   [.T T+v$_n$+\node{V}V$_j$+Apl$_k$ ]
             [.{\it v}P \node{io}{ }IO$_l$
               [.{\it v}\1 \node{subj2}t$_i$ 
                 [.AplP \node{v1}t$_n$  
                   [.Apl\1 \node{do}DO$_m$ 
                     [.{\it v}\1 \node{io1}t$_l$
                       [.Apl\1 \node{apl1}t$_k$ 
                         [.VP [.V \node{V1}t$_j$ ]
                              \node{do1}t$_m$  ] ] ] ] ] ] ] ] ]

\anodecurve[bl]{subj2}[bl]{subj1}{0.4in}
\anodecurve[bl]{do1}[bl]{do}{0.4in}
{\makedash{4pt}\anodecurve[t]{io1}[r]{io}{.5in}}
\anodecurve[bl]{V1}[bl]{apl1}{0.6in}
\anodecurve[bl]{apl1}[bl]{v1}{1in}
\anodecurve[bl]{v1}[bl]{V}{0.9in}
}
\end{comment}

\begin{comment}
\resizebox{\textwidth}{!}{
\begin{tabular}{c}
\Large
\psset{levelsep=48pt,treesep=10pt,treefit=tight}
\Tree [.IP [.NP [.NP [.NR \rnode{Zhongguo}{Zhongguo} ] ] [.QP [.CD \rnode{shisi}{shisi} ] [.CLP [.M \rnode{ge}{ge} ] ] ] [.NP [.NN \rnode{bianjing}{bianjing} ] [.NN \rnode{kaifang}{kaifang} ] [.NN \rnode{chengshi}{chengshi} ] ] ] [.NP [.NN \rnode{jingji}{jingji} ] [.NN \rnode{jianshe}{jianshe} ] [.NN \rnode{chengjiu}{chengjiu} ] ]  [.{VP} [.VV \rnode{xianzhu}{xianzhu} ] ] ]\\
 \psset{treemode=U}              %make tree grow upward
\Tree  [.S [.NP  [.CD \rnode{fourteen}{14} ] [.NNP \rnode{Chinese}{Chinese} ][.JJ \rnode{open}{open} ] [.NN \rnode{border}{border} ] [.NNS \rnode{cities}{cities} ] ] [.VP [.VBP \rnode{make}{make} ] [.NP [.NP [.JJ \rnode{significant}{significant} ] [.NNS \rnode{achievements}{achievements} ] ] [.PP [.IN \rnode{in}{in} ] [.NP [.JJ \rnode{economic}{economic} ]  [.NN \rnode{construction}{construction} ]  ] ] ] ] ] 
\end{tabular}
}
\psset{linewidth=0.3pt,arrowsize=4pt}
\nccurve[angleA=-90,angleB=90]{->}{Zhongguo}{Chinese}
\nccurve[angleA=-90,angleB=90]{->}{shisi}{fourteen}
\nccurve[angleA=-90,angleB=90]{->}{bianjing}{border}
\nccurve[angleA=-90,angleB=90]{->}{kaifang}{open}
\nccurve[angleA=-90,angleB=90]{->}{chengshi}{cities}
\nccurve[angleA=-90,angleB=90]{->}{jingji}{economic}
\nccurve[angleA=-90,angleB=90]{->}{jianshe}{construction}
\nccurve[angleA=-90,angleB=90]{->}{chengjiu}{achievements}
\nccurve[angleA=-90,angleB=90]{->}{xianzhu}{significant}
\end{comment}

\pagebreak
Trees can be made to grow sideways, or upside down, using 
\verb|\psset{treemode=R}| for right, L for left, or U for up.
Other useful layout parameters inherited from {\tt pstricks}
include  {\tt levelsep}, {\tt ltreesep}, {\tt ltreefit} and {\tt nodesep}.

\begin{verbatim}
\psset{treemode=R}
\Tree [.{VV}:xianzhu [.NN:chengshi NR:Zhongguo [.CD:shisi M:ge ] 
                        NN:bianjing NN:kaifang ] 
          [.NN:chengjiu NN:jingji NN:jianshe ] ]
\end{verbatim}



\psset{treemode=R}
\psset{levelsep=148pt,treesep=12pt,treefit=tight}
\Tree [.{VV}:xianzhu [.NN:chengshi NR:Zhongguo [.CD:shisi M:ge ] NN:bianjing NN:kaifang ] [.NN:chengjiu NN:jingji NN:jianshe ] ]

\begin{comment}
\psset{treemode=L}
\psset{levelsep=148pt,treesep=12pt,treefit=tight}
\Tree [.VV:make [.NNS:cities [.CD:14 ] NNP:Chinese [.JJ:open ] [.NN:border ] ]  [.NNS:achievements [.JJ:significant ] [.IN:in [.NN:construction [.JJ:economic ] ] ] ] ] }
\end{comment}




\end{document}
