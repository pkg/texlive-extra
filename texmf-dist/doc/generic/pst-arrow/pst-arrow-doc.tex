%% $Id: pst-arrow-doc.tex 164 2021-08-25 19:26:42Z herbert $
\errorcontextlines=100
\documentclass[11pt,english,BCOR10mm,DIV=14,bibliography=totoc,parskip=false,smallheadings,
    headexclude,footexclude,twoside]{pst-doc}
\usepackage{pst-node,hvlogos,pst-arrow}
\let\pstArrowFV\fileversion
\lstset{preset=\centering,pos=l,wide=false,vsep=5mm,language=PSTricks,%width=0.5\linewidth,
    morekeywords={multidipole,parallel},basicstyle=\footnotesize\ttfamily}
%
\renewcommand\bgImage{\begin{pspicture}(5,6)
\psset{arrowscale=2}
  \pnode(3,4){A}\pnode(5,6){B}\pnode(5,0){C}
  \psbezier[ArrowInside=->,%
     showpoints=true](A)(B)(C)
  \psset{linestyle=none,ArrowInside=-<}
  \psbezier[ArrowInsideNo=4](0,0)(A)(B)(C)
  \psset{ArrowInside=-o}
  \psbezier[ArrowInsidePos=0.1](0,0)(A)(B)(C)
  \psbezier[ArrowInsidePos=0.9](0,0)(A)(B)(C)
  \psset{ArrowInside=-*}
  \psbezier[ArrowInsidePos=0.3](0,0)(A)(B)(C)
  \psbezier[ArrowInsidePos=0.7](0,0)(A)(B)(C)
\end{pspicture}
}

\addbibresource{\jobname.bib}
\begin{document}

\title{\texttt{pst-arrow}}
\subtitle{A PSTricks package for drawing arrows; v.\pstArrowFV}
\author{Herbert Voß}
%\docauthor{Herbert Vo\ss}
\date{\today}

\settitle

\tableofcontents

\begin{abstract}
\noindent
The \LPack{pstricks} related package provides more arrow types.

%\vfill\noindent
%Thanks to: \\
%    Jean-C\^ome Charpentier.
\end{abstract}

%--------------------------------------------------------------------------------------
\section{Arrows}
%--------------------------------------------------------------------------------------
\LPack{pstricks} defines the following "`arrows"':

  \def\myline#1{\psline[linecolor=red,linewidth=0.5pt,arrowscale=1.5]{#1}(0,1ex)(1.3,1ex)}%
  \def\mylineA#1{\psline[linecolor=red,linewidth=0.5pt,arrowscale=4.5]{#1}(0,1ex)(2,1ex)}%
  \psset{arrowscale=1.5}
  \begin{longtable}{@{} c @{\qquad} p{3cm} l @{}}%
    Value & Example & Name \\[2pt]\hline
    \Lnotation{-}      & \myline{-}      & None\\
    \Lnotation{<->}    & \myline{<->}    & Arrowheads.\\
    \Lnotation{>-<}    & \myline{>-<}    & Reverse arrowheads.\\
    \Lnotation{<{<}-{>}>}  & \myline{<<->>}  & Double arrowheads.\\
    \Lnotation{{>}>-{<}<}  & \myline{>>-<<}  & Double reverse arrowheads.\\
    \Lnotation{{|}-{|}}    & \myline{|-|}    & T-bars, flush to endpoints.\\
    \Lnotation{{|}*-{|}*}  & \myline{|*-|*}  & T-bars, centered on endpoints.\\
    \Lnotation{[-]}    & \myline{[-]}    & Square brackets.\\
    \Lnotation{]-[}    & \myline{]-[}    & Reversed square brackets.\\
    \Lnotation{(-)}    & \myline{(-)}    & Rounded brackets.\\
    \Lnotation{)-(}    & \myline{)-(}    & Reversed rounded brackets.\\
    \Lnotation{o-o}    & \myline{o-o}    & Circles, centered on endpoints.\\
    \Lnotation{*-*}    & \myline{*-*}    & Disks, centered on endpoints.\\
    \Lnotation{oo-oo}  & \myline{oo-oo}  & Circles, flush to endpoints.\\
    \Lnotation{**-**}  & \myline{**-**}  & Disks, flush to endpoints.\\
    \Lnotation{{|}<->{|}}  & \myline{|<->|}  & T-bars and arrows.\\
    \Lnotation{{|}>-<{|}}  & \myline{|>-<|}  & T-bars and reverse arrows.\\
    \Lnotation{h-h{}}   & \myline{h-h}    & left/right hook arrows.\\
    \Lnotation{H-H{}}   & \myline{H-H}    & left/right hook arrows.\\
    \Lnotation{v-v}   & \myline{v-v}    & left/right inside vee arrows.\\
    \Lnotation{V-V}   & \myline{V-V}    & left/right outside vee arrows.\\
    \Lnotation{f-f}   & \myline{f-f}    & left/right inside filled arrows.\\
    \Lnotation{F-F}   & \myline{F-F}    & left/right outside filled arrows.\\
    \Lnotation{t-t}   & \myline{t-t}    & left/right inside slash arrows.\\[5pt]
    \Lnotation{T-T}   & \myline{T-T}    & left/right outside slash arrows.\\
%
    \Lnotation{<D-D>}   & \mylineA{<D-D>}    & curved  arrows.\\
    \Lnotation{<D<D-D>D>}   & \mylineA{<D<D-D>D>}    & curved doubled arrows.\\
    \Lnotation{D>-<D}   & \mylineA{D>-<D}    & curved  arrows, tip inside.\\
    \Lnotation{<T-T>}   & \myline{<T-T>}    & curved lines.\\
%    \Lnotation{>T-T<}   & \mylineA{>T-T<}    & \TikZ\ like arrows.\\
    \hline
  \end{longtable}

%\def\pst@arrowtable{,-,<->,<<->>,>-<,>>-<<,(-),[-],)-(,]-[,|>-<|,%
%    <D-D>,D>-<D,<D<D-D>D>,<T-T>,|<*->|*,|<->|,v-v,V-V,f-f,F-F,t-t,T-T}


\psset{arrowscale=1}

You can also mix and match, e.g., \Lnotation{->}, \Lnotation{*-)} and \Lnotation{[->} are all valid values
of the \Lkeyword{arrows} parameter. The parameter can be set with

\begin{BDef}
\Lcs{psset}\Largb{arrows=<type>}
\end{BDef}

\noindent or for some macros with a special option, like\\[5pt]
\noindent\verb|\psline[<general options>]{<arrow type>}(A)(B)|\\
\noindent\verb/\psline[linecolor=red,tipcolor=blue,linewidth=2pt]{|->}(0,0)(0,2)/ \ 
\psline[linecolor=red,tipcolor=blue,linewidth=2pt]{|->}(0,0)(0,2)

\subsection{Multiple arrows}
There are two new options which are only valid for the arrow type \verb+<<+ or \verb+>>+.
\verb+nArrow+ sets both, the \verb+nArrowA+ and the  \verb+nArrowB+ parameter. The meaning
is declared in the following tables. Without setting one of these parameters the behaviour
is like the one described in the old PSTricks manual.

\begin{center}
\begin{tabular}{@{}lc@{}}%
    Value & Meaning \\[2pt]\hline
    \Lnotation{-{>}>}   & \ -A \\
    \Lnotation{{<}<-{>}>} & A-A\\
    \Lnotation{{<}<-}   & A-\ \\
    \Lnotation{{>}>-}   & B-\ \\
    \Lnotation{-{<}<}   & \ -B\\
    \Lnotation{{>}>-{<}<} & B-B\\
    \Lnotation{{>}>-{>}>} & B-A\\
    \Lnotation{{<}<-{<}<} & A-B\\\hline
  \end{tabular}
\end{center}




\begin{center}
  \bgroup
  \psset{linecolor=red,linewidth=1pt,arrowscale=2}%
  \begin{tabular}{lp{2.8cm}}%
    Value & Example \\[2pt]\hline
    \verb+\psline{->>}(0,1ex)(2.3,1ex)+  & \psline{->>}(0,1ex)(2.3,1ex) \\
    \verb+\psline[nArrowsA=3]{->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3]{->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=5]{->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline{<<-}(0,1ex)(2.3,1ex)+  & \psline{<<-}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<-}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3]{<<-}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<-}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=5]{<<-}(0,1ex)(2.3,1ex)\\
    \verb+\psline{<<->>}(0,1ex)(2.3,1ex)+  & \psline{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3]{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=5]{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline{<<-|}(0,1ex)(2.3,1ex)+  & \psline{<<-|}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<-<<}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3]{<<-<<}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<-o}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=5]{<<-o}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3,nArrowsB=4]{<<-<<}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3,nArrowsB=4]{<<-<<}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=3,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=1,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)+  & \psline[nArrowsA=1,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)\\\hline
  \end{tabular}
  \egroup
\end{center}



\subsection{\texttt{hookarrow}}
%\begin{LTXexample}
\bgroup
\psset{arrowsize=8pt,arrowlength=1,linewidth=1pt,nodesep=2pt,shortput=tablr}
\large
\begin{psmatrix}[colsep=12mm,rowsep=10mm]
        &   & $R_2$            \\
        &   &   0   &   & $R_3$\\
$e_b:S$ & 1 &       & 1 & 0    \\
        &   &   0              \\
        &   &   $R_1$          \\
\end{psmatrix}
\ncline{h-}{1,3}{2,3}<{$e_{r2}$}>{$f_{r2}$}
\ncline{-h}{2,3}{3,2}<{$e_1$}
\ncline{-h}{3,1}{3,2}^{$e_s$}_{$f_{s}$}
\ncline{-h}{3,2}{4,3}>{$e_3$}<{$f_3$}
\ncline{-h}{4,3}{3,4}>{$e_4$}<{$f_4$}
\ncline{-h}{3,4}{2,3}>{$e_2$}<{$f_2$}
\ncline{-h}{3,4}{3,5}^{$e_5$}
\ncline{-h}{3,5}{2,5}<{$e_{r3}$}>{$f_{r3}$}
\ncline{-h}{4,3}{5,3}<{$e_{r1}$}>{$f_{r1}$}
%\end{LTXexample}
\egroup

\begin{lstlisting}
\psset{arrowsize=8pt,arrowlength=1,linewidth=1pt,nodesep=2pt,shortput=tablr}
\large
\begin{psmatrix}[colsep=12mm,rowsep=10mm]
        &   & $R_2$            \\
        &   &   0   &   & $R_3$\\
$e_b:S$ & 1 &       & 1 & 0    \\
        &   &   0              \\
        &   &   $R_1$          \\
\end{psmatrix}
\ncline{h-}{1,3}{2,3}<{$e_{r2}$}>{$f_{r2}$}\ncline{-h}{2,3}{3,2}<{$e_1$}
\ncline{-h}{3,1}{3,2}^{$e_s$}_{$f_{s}$}    \ncline{-h}{3,2}{4,3}>{$e_3$}<{$f_3$}
\ncline{-h}{4,3}{3,4}>{$e_4$}<{$f_4$}      \ncline{-h}{3,4}{2,3}>{$e_2$}<{$f_2$}
\ncline{-h}{3,4}{3,5}^{$e_5$}              
\ncline{-h}{3,5}{2,5}<{$e_{r3}$}>{$f_{r3}$}
\ncline{-h}{4,3}{5,3}<{$e_{r1}$}>{$f_{r1}$}
\end{lstlisting}



\subsection{\texttt{hookrightarrow} and \texttt{hookleftarrow}}
This is another type of arrow and is abbreviated with \Lnotation{H}.
The length and width of the hook is set by the new options
\Lkeyword{hooklength} and \Lkeyword{hookwidth}, which are by default set
to
%
\begin{BDef}
\Lcs{psset}\Largb{hooklength=3mm,hookwidth=1mm}
\end{BDef}
%
If the line begins with a right hook then the line ends with a left hook and vice versa:

\begin{LTXexample}[width=3cm]
\begin{pspicture}(3,4)
\psline[linewidth=5pt,linecolor=blue,hooklength=5mm,hookwidth=-3mm]{H->}(0,3.5)(3,3.5)
\psline[linewidth=5pt,linecolor=red,hooklength=5mm,hookwidth=3mm]{H->}(0,2.5)(3,2.5)
\psline[linewidth=5pt,hooklength=5mm,hookwidth=3mm]{H-H}(0,1.5)(3,1.5)
\psline[linewidth=1pt]{H-H}(0,0.5)(3,0.5)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=7.25cm]
$\begin{psmatrix}
E&W_i(X)&&Y\\
&&W_j(X)
\psset{arrows=->,nodesep=3pt,linewidth=2pt}
\everypsbox{\scriptstyle}
\ncline[linecolor=red,arrows=H->,%
  hooklength=4mm,hookwidth=2mm]{1,1}{1,2}
\ncline{1,2}{1,4}^{\tilde{t}}
\ncline{1,2}{2,3}<{W_{ij}}
\ncline{2,3}{1,4}>{\tilde{s}}
\end{psmatrix}$
\end{LTXexample}


%--------------------------------------------------------------------------------------
\subsection{\nxLkeyword{ArrowInside} Option}
%--------------------------------------------------------------------------------------

It is now possible to have arrows inside lines and not only at the
beginning or the end. The new defined options

\psset{arrowscale=2,linecolor=red,unit=1cm,linewidth=1.5pt}
\begin{longtable}{l|>{\RaggedRight}p{8.5cm}|p{2.2cm}}
Name & Example & Output\\\hline
\endfirsthead
Name & Example & Output\\\hline
\endhead
\Lkeyword{ArrowInside} &
  \texttt{\textbackslash psline[ArrowInside=->](0,0)(2,0)} &
  \psline[ArrowInside=->](0,0.1)(2,0.1) \\
\Lkeyword{ArrowInsidePos} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsidePos=0.25](0,0)(2,0)}
& \psline[ArrowInside=->, ArrowInsidePos=0.25](0,0.1)(2,0.1) \\
\Lkeyword{ArrowInsidePos} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsidePos=10](0,0)(2,0)}
& \psline[ArrowInside=->, ArrowInsidePos=10](0,0.1)(2,0.1) \\
\Lkeyword{ArrowInsideNo} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsideNo=2](0,0)(2,0)}
& \psline[ArrowInside=->, ArrowInsideNo=2](0,0.1)(2,0.1) \\
\Lkeyword{ArrowInsideOffset} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsideNo=2,\%}\newline
  \hspace*{20pt}\texttt{ArrowInsideOffset=0.1](0,0)(2,0)}
& \psline[ArrowInside=->, ArrowInsideNo=2,ArrowInsideOffset=0.1](0,0.1)(2,0.1) \\
%
\Lkeyword{ArrowInside} & \texttt{\textbackslash psline[ArrowInside=->]\{->\}(0,0)(2,0)} &
  \psline[ArrowInside=->]{->}(0,0)(2,0)\\
\Lkeyword{ArrowInsidePos} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsidePos=0.25]\{->\}(0,0)(2,0)}
  & \psline[ArrowInside=->, ArrowInsidePos=0.25]{->}(0,0)(2,0) \\
\Lkeyword{ArrowInsidePos} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsidePos=10]\{->\}(0,0)(2,0)}
  & \psline[ArrowInside=->, ArrowInsidePos=10]{->}(0,0)(2,0) \\
\Lkeyword{ArrowInsideNo} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsideNo=2]\{->\}(0,0)(2,0)}
  & \psline[ArrowInside=->, ArrowInsideNo=2]{->}(0,0)(2,0) \\
\Lkeyword{ArrowInsideOffset} & \texttt{\textbackslash psline[ArrowInside=->,\%}
  \hspace*{20pt}\texttt{ArrowInsideNo=2,\%}\newline
  \hspace*{20pt}\texttt{ArrowInsideOffset=0.1]\{->\}(0,0)(2,0)}
  & \psline[ArrowInside=->, ArrowInsideNo=2,ArrowInsideOffset=0.1]{->}(0,0)(2,0) \\
%
\Lkeyword{ArrowFill} & \texttt{\textbackslash psline[ArrowFill=false,\%}
  \hspace*{20pt}\texttt{arrowinset=0]\{->\}(0,0)(2,0)} &
  \psline[ArrowFill=false,arrowinset=0]{->}(0,0)(2,0)\\
\Lkeyword{ArrowFill} & \texttt{\textbackslash psline[ArrowFill=false,\%}
  \hspace*{20pt}\texttt{arrowinset=0]\{<<->>\}(0,0)(2,0)} &
  \psline[ArrowFill=false,arrowinset=0]{<<->>}(0,0)(2,0)\\
\Lkeyword{ArrowFill} & \texttt{\textbackslash psline[ArrowInside=->,\%}\newline
  \hspace*{20pt}\texttt{arrowinset=0,\%}\newline
  \hspace*{20pt}\texttt{ArrowFill=false,\%}\newline
  \hspace*{20pt}\texttt{ArrowInsideNo=2,\%}\newline
  \hspace*{20pt}\texttt{ArrowInsideOffset=0.1]\{->\}(0,0)(2,0)}
  & \psline[ArrowInside=->, ArrowFill=false,ArrowInsideNo=2,ArrowInsideOffset=0.1]{->}(0,0)(2,0) \\
\end{longtable}

\medskip
Without the default arrow definition there is only the one inside
the line, defined by the type and the position. The position is
relative to the length of the whole line. $0.25$ means at $25\%$
of the line length. The peak of the arrow gets the coordinates
which are calculated by the macro. If you want arrows with an
absolute position difference, then choose a value greater than
\verb|1|, e.\,g. \verb|10| which places an arrow every 10~pt. The
default unit \verb|pt| cannot be changed.

\medskip
\noindent
\begin{tabularx}{\linewidth}{@{\color{red}\vrule width 2pt}lX@{}}
& The \Lkeyword{ArrowInside} takes only arrow definitions like \Lnotation{->} into account.
Arrows from right to left (\Lnotation{<-}) are not possible and ignored. If you need
such arrows, change the order of the pairs of coordinates for the line or curve macro.
\end{tabularx}

%--------------------------------------------------------------------------------------
\subsection{\nxLkeyword{ArrowFill} option}
%--------------------------------------------------------------------------------------

By default all arrows are filled polygons. With the option
\Lkeyset{ArrowFill=false} there are ''white`` arrows. Only for the
beginning/end arrows are they empty, the inside arrows are
overpainted by the line.


\begin{LTXexample}[width=3.5cm]
\psline[arrowscale=2.5,linecolor=red,arrowinset=0]{<->}(-1,0)(2,0)
\end{LTXexample}

\psset{arrowscale=1}
\begin{LTXexample}[width=3.5cm]
\psline[arrowscale=2.5,linecolor=red,arrowinset=0,ArrowFill=false]{<->}(-1,0)(2,0)
\end{LTXexample}

\begin{LTXexample}[width=3.5cm]
\psline[arrowscale=2.5,linecolor=red,arrowinset=0,arrowsize=0.2,
  ArrowFill=false]{<->}(-1,0)(2,0)
\end{LTXexample}

\begin{LTXexample}[width=3.5cm]
\psline[arrowscale=2.5,linecolor=blue,arrowscale=4,ArrowFill]{>>->>}(-1,0)(2,0)
\end{LTXexample}

\begin{LTXexample}[width=3.5cm]
\psline[linecolor=blue,arrowscale=3,ArrowFill=false]{>>->>}(-1,0)(2,0)
\end{LTXexample}

\begin{LTXexample}[width=3.5cm]
\psline[linecolor=blue,arrowscale=3,ArrowFill]{>|->|}(-1,0)(2,0)
\end{LTXexample}

\begin{LTXexample}[width=3.5cm]
\psline[linecolor=blue,arrowscale=4,ArrowFill=false]{>|->|}(-1,0)(2,0)
\end{LTXexample}


%--------------------------------------------------------------------------------------
\subsection{\nxLkeyword{tipcolor} option}
%--------------------------------------------------------------------------------------

It is possible to change the color of the arrow tip by setting \Lkeyword{tipcolor}:


\begin{center}
  \bgroup
  \psset{linecolor=black,linewidth=1pt,arrowscale=2}%
  \begin{tabular}{lp{2.8cm}}%
    Value & Example \\[2pt]\hline
    \rlap{\texttt{\textbackslash psset\{tipcolor=red\}}}\\
    \verb+\psline{->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red]{->>}(0,1ex)(2.3,1ex) \\
    \verb+\psline[nArrowsA=3]{->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red,nArrowsA=3]{->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red,nArrowsA=5]{->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline{<<-}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red]{<<-}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<-}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red,nArrowsA=3]{<<-}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<-}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=red,nArrowsA=5]{<<-}(0,1ex)(2.3,1ex)\\
    \rlap{\texttt{\textbackslash psset\{tipcolor=blue\}}}\\
    \verb+\psline{<<->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue]{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue,nArrowsA=3]{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue,nArrowsA=5]{<<->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline{<<-|}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue]{<<-|}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=3]{<<-<<}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue,nArrowsA=3]{<<-<<}(0,1ex)(2.3,1ex)\\
    \verb+\psline[nArrowsA=5]{<<-o}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=blue,nArrowsA=5]{<<-o}(0,1ex)(2.3,1ex)\\
    \verb+\psline[tipcolor=magenta,nArrowsA=3,nArrowsB=4]{<<-<<}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=magenta,nArrowsA=3,nArrowsB=4]{<<-<<}(0,1ex)(2.3,1ex)\\
    \verb+\psline[tipcolor=cyan,nArrowsA=3,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=cyan,nArrowsA=3,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)\\
    \verb+\psline[tipcolor=yellow,nArrowsA=1,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)+  & \psline[tipcolor=yellow,nArrowsA=1,nArrowsB=4]{>>->>}(0,1ex)(2.3,1ex)\\\hline
  \end{tabular}
  \egroup
\end{center}



\clearpage


\subsection{Big Arrows}

\begin{LTXexample}[width=5cm]
\begin{pspicture}(5,5)
\psset{doublesep=1cm}
\psBigArrow[fillstyle=solid,
  fillcolor=blue!30,linecolor=blue](0,3)(5,3)
\psBigArrow[fillstyle=solid,opacity=0.3,
  fillcolor=red,linecolor=red](0.5,0.5)(5,5)
\end{pspicture}
\end{LTXexample}




%--------------------------------------------------------------------------------------
\subsection{Examples}
%--------------------------------------------------------------------------------------

All examples are printed with \verb|\psset{arrowscale=2,linecolor=red}|.
\subsubsection{\nxLcs{psline}}

\bigskip
\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=->]{|<->|}(2,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-|]{|-|}(2,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=->,ArrowInsideNo=2]{->}(2,1)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=->,ArrowInsideNo=2,ArrowInsideOffset=0.1]{->}(2,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-*]{->}(0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-*,ArrowInsidePos=0.25]{->}(0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-*,ArrowInsidePos=0.25,ArrowInsideNo=2]{->}%
   (0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=->, ArrowInsidePos=0.25]{->}%
        (0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[linestyle=none,ArrowInside=->,ArrowInsidePos=0.25]{->}%
        (0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-<, ArrowInsidePos=0.75]{->}%
     (0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true,ArrowInside=-*}
\psline(0,0)(2,1)(3,0)(4,0)(6,2)
\psset{linestyle=none}
\psline[ArrowInsidePos=0](0,0)(2,1)(3,0)(4,0)(6,2)
\psline[ArrowInsidePos=1](0,0)(2,1)(3,0)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,5)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=->,ArrowInsidePos=20](0,0)(3,0)%
       (3,3)(1,3)(1,5)(5,5)(5,0)(7,0)(6,3)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,2)
\psset{arrowscale=2,ArrowFill=true}
\psline[ArrowInside=-|]{<->}(0,2)(2,0)(3,2)(4,0)(6,2)
\end{pspicture}
\end{LTXexample}


\clearpage

%--------------------------------------------------------------------------------------
\subsubsection{\nxLcs{pspolygon}}
%--------------------------------------------------------------------------------------
% Polygons (\pspolygon macro)

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,3)
\psset{arrowscale=2}
\pspolygon[ArrowInside=-|](0,0)(3,3)(6,3)(6,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,3)
\psset{arrowscale=2}
\pspolygon[ArrowInside=->,ArrowInsidePos=0.25]%
     (0,0)(3,3)(6,3)(6,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,3)
\psset{arrowscale=2}
\pspolygon[ArrowInside=->,ArrowInsideNo=4]%
       (0,0)(3,3)(6,3)(6,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,3)
\psset{arrowscale=2}
\pspolygon[ArrowInside=->,ArrowInsideNo=4,%
   ArrowInsideOffset=0.1](0,0)(3,3)(6,3)(6,1)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,3)
\psset{arrowscale=2}
 \pspolygon[ArrowInside=-|](0,0)(3,3)(6,3)(6,1)
 \psset{linestyle=none,ArrowInside=-*}
 \pspolygon[ArrowInsidePos=0](0,0)(3,3)(6,3)(6,1)
 \pspolygon[ArrowInsidePos=1](0,0)(3,3)(6,3)(6,1)
 \psset{ArrowInside=-o}
 \pspolygon[ArrowInsidePos=0.25](0,0)(3,3)(6,3)(6,1)
 \pspolygon[ArrowInsidePos=0.75](0,0)(3,3)(6,3)(6,1)
\end{pspicture}
\end{LTXexample}

\psset{linestyle=solid}

\begin{LTXexample}[width=6.5cm]
\begin{pspicture}(6,5)
\psset{arrowscale=2}
  \pspolygon[ArrowInside=->,ArrowInsidePos=20]%
    (0,0)(3,0)(3,3)(1,3)(1,5)(5,5)(5,0)(7,0)(6,3)
\end{pspicture}
\end{LTXexample}


\clearpage


%--------------------------------------------------------------------------------------
\subsubsection{\nxLcs{psbezier}}
%--------------------------------------------------------------------------------------
% Bezier curves (\psbezier macro)


\begin{LTXexample}[width=3.5cm]
\begin{pspicture}(3,3)
\psset{arrowscale=2}
  \psbezier[ArrowInside=-|](0,1)(1,0)(2,1)(3,3)
  \psset{linestyle=none,ArrowInside=-o}
  \psbezier[ArrowInsidePos=0.25](0,1)(1,0)(2,1)(3,3)
  \psbezier[ArrowInsidePos=0.75](0,1)(1,0)(2,1)(3,3)
  \psset{linestyle=none,ArrowInside=-*}
  \psbezier[ArrowInsidePos=0](0,1)(1,0)(2,1)(3,3)
  \psbezier[ArrowInsidePos=1](0,1)(1,0)(2,1)(3,3)
\end{pspicture}
\end{LTXexample}



\resetArrowOptions
\begin{LTXexample}[width=4.5cm]
\begin{pspicture}(4,3)
\psset{arrowscale=2}
\psbezier[ArrowInside=->,showpoints]%
  {*-*}(0,0)(2,3)(3,0)(4,2)
\end{pspicture}
\end{LTXexample}




\begin{LTXexample}[width=4.5cm]
\begin{pspicture}(4,3)
\psset{arrowscale=2}
  \psbezier[ArrowInside=->,showpoints=true,
      ArrowInsideNo=2](0,0)(2,3)(3,0)(4,2)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=4.5cm]
\begin{pspicture}(4,3)
\psset{arrowscale=2}
  \psbezier[ArrowInside=->,showpoints=true,
     ArrowInsideNo=2,ArrowInsideOffset=-0.2]%
      {->}(0,0)(2,3)(3,0)(4,2)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=5.5cm]
\begin{pspicture}(5,3)
\psset{arrowscale=2}
  \psbezier[ArrowInsideNo=9,ArrowInside=-|,%
    showpoints=true]{*-*}(0,0)(1,3)(3,0)(5,3)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=4.5cm]
\begin{pspicture}(4,3)
\psset{arrowscale=2}
  \psset{ArrowInside=-|}
  \psbezier[ArrowInsidePos=0.25,showpoints=true]{*-*}(2,3)(3,0)(4,2)
  \psset{linestyle=none}
  \psbezier[ArrowInsidePos=0.75](0,0)(2,3)(3,0)(4,2)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[width=5.5cm]
\begin{pspicture}(5,6)
\psset{arrowscale=2}
  \pnode(3,4){A}\pnode(5,6){B}\pnode(5,0){C}
  \psbezier[ArrowInside=->,%
     showpoints=true](A)(B)(C)
  \psset{linestyle=none,ArrowInside=-<}
  \psbezier[ArrowInsideNo=4](0,0)(A)(B)(C)
  \psset{ArrowInside=-o}
  \psbezier[ArrowInsidePos=0.1](0,0)(A)(B)(C)
  \psbezier[ArrowInsidePos=0.9](0,0)(A)(B)(C)
  \psset{ArrowInside=-*}
  \psbezier[ArrowInsidePos=0.3](0,0)(A)(B)(C)
  \psbezier[ArrowInsidePos=0.7](0,0)(A)(B)(C)
\end{pspicture}
\end{LTXexample}

\psset{linestyle=solid}

\begin{LTXexample}[pos=t]
\psset{unit=0.75}
\begin{pspicture}(-3,-5)(15,5)
  \psbezier[ArrowInsideNo=19,%
      ArrowInside=->,ArrowFill=false,%
      showpoints=true]{->}(-3,0)(5,-5)(8,5)(15,-5)
\end{pspicture}
\end{LTXexample}


\psset{unit=1cm}

%--------------------------------------------------------------------------------------
\subsubsection{\nxLcs{pcline}}
%--------------------------------------------------------------------------------------
These examples need the package \verb|pst-node|.

% Lines (\pcline macro)
\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,1)
\psset{arrowscale=2}
\pcline[ArrowInside=->](0,0)(2,1)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,1)
\psset{arrowscale=2}
\pcline[ArrowInside=->]{<->}(0,0)(2,1)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,1)
\psset{arrowscale=2}
\pcline[ArrowInside=-|,ArrowInsidePos=0.75]{|-|}(0,0)(2,1)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\psset{arrowscale=2}
\pcline[ArrowInside=->,ArrowInsidePos=0.65]{*-*}(0,0)(2,0)
\naput[labelsep=0.3]{\large$g$}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\psset{arrowscale=2}
\pcline[ArrowInside=->,ArrowInsidePos=10]{|-|}(0,0)(2,0)
\naput[labelsep=0.3]{\large$l$}
\end{LTXexample}



%--------------------------------------------------------------------------------------
\subsubsection{\nxLcs{pccurve}}
%--------------------------------------------------------------------------------------
These examples also need the package \verb|pst-node|.

\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2}
\pccurve[ArrowInside=->,ArrowInsidePos=0.65,showpoints=true]{*-*}(0,0)(2,2)
\naput[labelsep=0.3]{\large$h$}
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=2.5cm]
\begin{pspicture}(2,2)
\psset{arrowscale=2}
\pccurve[ArrowInside=->,ArrowInsideNo=3,showpoints=true]{|->}(0,0)(2,2)
\naput[labelsep=0.3]{\large$i$}
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=4.5cm]
\begin{pspicture}(4,4)
\psset{arrowscale=2}
\pccurve[ArrowInside=->,ArrowInsidePos=20]{|-|}(0,0)(4,4)
\naput[labelsep=0.3]{\large$k$}
\end{pspicture}
\end{LTXexample}


\subsection{Special arrows \texttt{v--V},\texttt{t--T}, and \texttt{f--F}}

Possible optional arguments are

\psset{linecolor=black}

\begin{center}
\begin{tabular}{@{}l|l@{}}\toprule
\emph{name} & \emph{meaning}\\\hline
\Lkeyword{veearrowlength} & default is 3mm\\
\Lkeyword{veearrowangle} & default is 30\\
\Lkeyword{veearrowlinewidth} & default is 0.35mm\\
\Lkeyword{filledveearrowlength} & default is 3mm\\
\Lkeyword{filledveearrowangle} & default is 15\\
\Lkeyword{filledveearrowlinewidth} & default is 0.35mm\\
\Lkeyword{tickarrowlength} & default is 1.5mm\\
\Lkeyword{tickarrowlinewidth} & default is 0.35mm\\
\Lkeyword{arrowlinestyle}     & default is solid\\\bottomrule
\end{tabular}
\end{center}


\begin{LTXexample}[width=4cm]
\psset{unit=5mm}
\begin{pspicture}(4,6)
  \psset{dimen=middle,arrows=c-c,
    arrowscale=2,linewidth=.25mm}
  \psline[linecolor=red,linewidth=.05mm](0,0)(0,6)
  \psline[linecolor=red,linewidth=.05mm](4,0)(4,6)
  \psline{v-v}(0,6)(4,6)
  \psline{v-V}(0,4)(4,4)
  \psline{V-v}(0,2)(4,2)
  \psline{V-V}(0,0)(4,0)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=4cm]
\psset{unit=5mm}
\begin{pspicture}(4,6)
  \psset{dimen=middle,arrows=c-c,
    arrowscale=2,linewidth=.25mm}
  \psline[linecolor=red,linewidth=.05mm](0,0)(0,6)
  \psline[linecolor=red,linewidth=.05mm](4,0)(4,6)
  \psline{f-f}(0,6)(4,6)
  \psline{f-F}(0,4)(4,4)
  \psline{F-f}(0,2)(4,2)
  \psline{F-F}(0,0)(4,0)
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[width=4cm]
\psset{unit=5mm}
\begin{pspicture}(4,6)
  \psset{dimen=middle,arrows=c-c,linewidth=.25mm}
  \psline[linecolor=red,linewidth=.05mm](0,0)(0,6)
  \psline[linecolor=red,linewidth=.05mm](4,0)(4,6)
  \psline{t-t}(0,6)(4,6)
  \psline{t-T}(0,4)(4,4)
  \psline{T-t}(0,2)(4,2)
  \psline{T-T}(0,0)(4,0)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,vsep=5mm]
\psset{unit=5mm}
 \begin{pspicture}(10,6)
 \psset{dimen=middle,arrows=c-c,arrowscale=2,linewidth=.25mm,
        arrowlinestyle=dashed,dash=1.5pt 1pt}
 \psline[linecolor=red,linewidth=.05mm](0,0)(0,6)
 \psline[linecolor=red,linewidth=.05mm](4,0)(4,6)
 \psline{v-v}(0,6)(4,6) \psline{v-V}(0,4)(4,4)
 \psline{V-v}(0,2)(4,2) \psline{V-V}(0,0)(4,0)
 \psline[linecolor=red,linewidth=.05mm](6,0)(6,6)
 \psline[linecolor=red,linewidth=.05mm](10,0)(10,6)
 \psset{arrowlinestyle=dotted,dotsep=0.8pt}
 \psline{v-v}(6,6)(10,6) \psline{v-V}(6,4)(10,4)
 \psline{V-v}(6,2)(10,2) \psline{V-V}(6,0)(10,0)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,vsep=5mm]
\psset{unit=5mm}
 \begin{pspicture}(10,7)
 \psset{dimen=middle,arrows=c-c,arrowscale=2,linewidth=.25mm,
        arrowlinestyle=dashed,dash=1.5pt 1pt}
 \psline[linecolor=red,linewidth=.05mm](0,0)(0,6)
 \psline[linecolor=red,linewidth=.05mm](4,0)(4,6)
 \psline{t-t}(0,6)(4,6) \psline{t-T}(0,4)(4,4)
 \psline{T-t}(0,2)(4,2) \psline{T-T}(0,0)(4,0)
 \psline[linecolor=red,linewidth=.05mm](6,0)(6,6)
 \psline[linecolor=red,linewidth=.05mm](10,0)(10,6)
 \psset{arrowlinestyle=dotted,dotsep=0.8pt}
 \psline{t-t}(6,6)(10,6) \psline{t-T}(6,4)(10,4)
 \psline{T-t}(6,2)(10,2) \psline{T-T}(6,0)(10,0)
\end{pspicture}
\end{LTXexample}






\subsection{Special arrow option \texttt{arrowLW}}

Only for the arrowtype \Lnotation{o} and \Lnotation{*} it is possible to
set the arrowlinewidth with the optional keyword \Lkeyword{arrowLW}.
When scaling an arrow by the keyword \Lkeyword{arrowscale} the width
of the borderline is also scaled. With the optional argument
\Lkeyword{arrowLW} the line width can be set separately and is not
taken into account by the scaling value.

\begin{LTXexample}[width=4cm]
\begin{pspicture}(4,6)
\psline[arrowscale=3,arrows=*-o](0,5)(4,5)
\psline[arrowscale=3,arrows=*-o,
  arrowLW=0.5pt](0,3)(4,3)
\psline[arrowscale=3,arrows=*-o,
  arrowLW=0.3333\pslinewidth](0,1)(4,1)
\end{pspicture}
\end{LTXexample}


\section{List of all optional arguments for \texttt{pst-arrow}}

\xkvview{family=pst-arrow,columns={key,type,default}}


\bgroup
\raggedright
\nocite{*}
\printbibliography
\egroup

\printindex
\end{document}

