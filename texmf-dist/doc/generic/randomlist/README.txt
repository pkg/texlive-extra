___________________________________

                The
          randomelist package
               v1.2

            2016/07/13
___________________________________

Maintainers: Jean-Côme Charpentier & Christian Tellechea
E-mails    : jean-come.charpentier@wanadoo.fr
             unbonpetit@openmailbox.org
             Comments, bug reports and suggestions are welcome.
Licence    : Released under the LaTeX Project Public License v1.3c or
             later, see http://www.latex-project.org/lppl.txt

----------------------------------------------------------------------

The main aim of package randomlist is to work on list, especially with
random operation. The hidden aim is to build personnal collection of
exercices with different data for each pupils.

In order to build such exercices, some features about databases and
about loops are necessary.

randomlist works under (La)eTeX, pdf(La)TeX, Xe(La)TeX, and
lua(La)TeX. TeX without eTeX primitives don't work since some eTeX
primitives are used.

----------------------------------------------------------------------

Le but premier de cette extension est de pouvoir travailler sur des
listes, en particulier avec des opérations aléatoires. Le but caché
est de réaliser une collection personnelle d'exercices avec
différentes données pour chaque élève.

Certaines fonctionnalités sur les base de données et les boucles vont
être nécessaires pour construire de tels exercices.

randomlist fonctionne sous La)eTeX, pdf(La)TeX, Xe(La)TeX et
lua(La)TeX. TeX sans les primitives eTeX ne fonctionnera pas puisque
certaines primitives eTeX sont utilisées.

                                                Jean-Côme Charpentier
                                                  Christian Tellechea

