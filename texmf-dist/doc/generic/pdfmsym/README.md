# PDF Math Symbols - pdfMsym
## version 1.1.0

The pdfMsym package provides mathematical symbols for PDF-dependent TeX compilers.
pdfMsym requires the font size in order to properly scale its symbols, so the following two lines must be added to your document in order to properly load it:

        \input pdfmsym
        \pdfmsymsetscalefactor{10}

assuming you want to load the package at a 10pt font, you can change the 10 to whatever suits your needs.
If you do not add the `\pdfmsymsetscalefactor` line, all of the macros which require pdf drawing won't work, as well as possibly some other macros.

The pdfMsym package also provides a relatively easy-to-use interface for creating new symbols.
Read the documentation (pdfmsym-doc.pdf) for more details.

The pdfMsym package is provided under the MIT license.

Compile the documentation via `pdftex pdfmsym-doc.tex`.

The pdfMsym package was created and is maintained by Slurp who can be reached via email: slurper04@gmail.com
Please report any bugs or issues with the package, doing so would be greatly appreciated.
Suggestions for features to add to the package will also be greatly appreciated.

Thank you for using pdfMsym,
    - Slurp
