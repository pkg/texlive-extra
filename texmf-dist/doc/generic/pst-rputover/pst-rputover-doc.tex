%  $Id: pst-rputover-doc.tex  2017-29-06 Martin, Thomas $
%% This is file `pst-rputover-doc.tex',
%%
%% IMPORTANT NOTICE:
%%
%% Package `pst-rputover.tex'
%%
%% Thomas S\"{O}LL and Martin J.OSBORNE
%%
%% This program can redistributed and/or modified under %%
%% the terms of the LaTeX Project Public License              %%
%% Distributed from CTAN archives in directory                %%
%% macros/latex/base/lppl.txt; either version 1.3c of       %%
%% the License, or (at your option) any later version.       %%
%%
%%    DESCRIPTION:
%%   `pst-rputover' is a PSTricks package that allows you to place text over objects without obscuring background colors
%%
%% Based partially on the answer of Werner at
%% http://tex.stackexchange.com/questions/353748/is-there-a-variant-of-rput-in-pstricks-that-preserves-background-fill-colors
%%

\documentclass[11pt,english,BCOR10mm,DIV12,bibliography=totoc,parskip=false,smallheadings,headexclude,footexclude,oneside,distiller]{pst-doc}

\usepackage[autostyle]{csquotes}
\usepackage{biblatex}
\addbibresource{pst-rputover-doc.bib}
\usepackage{pst-rputover}
\usepackage[utf8]{inputenc}
\let\pstrputoverFV\fileversion
\usepackage{pst-plot,pstricks-add}
\usepackage{etex} % increase the number of registers  (otherwise only 256)

\let\belowcaptionskip\abovecaptionskip

\parindent0pt

\psset{arrowlength=2.8,arrowinset=0.1}

\makeatletter
\renewcommand\maketitle{%
\thispagestyle{empty}%
\begin{titlepage}
\ifpdf
  \AddToShipoutPicture*{\includegraphics{pst-doc.pdf}}
  \vspace*{0.3\textheight}
  \parbox{17cm}{\sffamily\RaggedRight\bfseries\huge\@title}\\[5pt]
  \parbox{15cm}{\sffamily\Large\@subtitle}

  \vspace{5cm}
  \parbox{10cm}{\sffamily\@date}

  \vfill
  ~\\
  \makebox[.5\textwidth]{\put(0,0){\bgImage}}\\
  ~

  \vfill
  \parbox[b]{19cm}{\sffamily\RaggedRight
    \ifx\@docauthor\empty~\else Documentation by\fi\hfill\makebox[7cm][l]{Package authors:}\\
    \ifx\@docauthor\empty~\else{\bfseries\tabular[t]{@{}l@{}}\@docauthor\endtabular}\fi\hfill\makebox[7cm][l]{%
      \bfseries\tabular[t]{@{}l@{}}\@author\endtabular}}
\else
  \psset{unit=1cm}
  \begin{pspicture}(1.6in,0)(23cm,21.7cm)
    \psframe[fillstyle=solid,linecolor=lightgray,fillcolor=lightgray,linestyle=solid](0,-5.75)(23,10)
    \psframe[fillstyle=solid,linecolor=Orange!85!Red,fillcolor=Orange!85!Red,linestyle=solid](0,10)(23,10.5)
    \psframe[fillstyle=solid,linecolor=Orange!85!Red,fillcolor=Orange!85!Red,linestyle=solid](0,21.1)(23,21.2)
    \rput[lb](3,22){\Huge\sffamily\color{Orange!65!Red}\psscalebox{2}{\textbf{PSTricks}}}
    \rput[lb](3,14.1){\parbox{17cm}{\sffamily\RaggedRight\bfseries\huge\@title}}
    \rput[lb](3,12.6){\parbox{15cm}{\sffamily\Large\@subtitle}}
    \rput[lb](3,7.6){\parbox{15cm}{\sffamily\@date}}
    \rput[lb](3,-2.6){\parbox[b]{19cm}{\sffamily\RaggedRight
      \ifx\@docauthor\empty~\else Documentation by\fi\hfill\makebox[7cm][l]{Package authors:}\\
      \ifx\@docauthor\empty~\else{\bfseries\tabular[t]{@{}l@{}}\@docauthor\endtabular}\fi\hfill\makebox[7cm][l]{%
        \bfseries\tabular[t]{@{}l@{}}\@author\endtabular}}}
    \rput[C](11,4){\bgImage}%
  \end{pspicture}%
\fi
\end{titlepage}
\setcounter{footnote}{0}%
\global\let\thanks\relax%
\global\let\maketitle\relax%
\global\let\@thanks\@empty%
\global\let\@author\@empty%
\global\let\@docauthor\@empty%
\global\let\@date\@empty%
\global\let\@title\@empty%
\global\let\@subtitle\@empty%
\global\let\title\relax%
\global\let\author\relax%
\global\let\date\relax%
\global\let\and\relax%
}             
\makeatother

\renewcommand\bgImage{%
%{\psset{unit=3}%
\psscalebox{3}{%
\begin{pspicture}(2,2)
\psframe[fillstyle=solid,fillcolor=blue!40,linestyle=none,linewidth=0pt,opacity=0.8](0,0)(2,2)
\pscurve[fillstyle=solid,fillcolor=red!30,linestyle=none,opacity=0.8](0,2)(1,1)(2,2)
\pnode(.5,0){A}\psdot[linecolor=red](A)
\pnode(1.5,2){B}\psdot[linecolor=green](B)
\pclineover(A)(B){$\alpha$}
\end{pspicture}
}}

%%\usepackage[style=dtk]{biblatex}
%\usepackage{biblatex}
%%\addbibresource{pst-rputover-doc.bib}


\lstset{language=PSTricks,morekeywords={rputover,coverable,pclineover,pcarrowC,},basicstyle=\footnotesize\ttfamily}
%
\psset{labelFontSize=\scriptstyle}% for mathmode

\begin{document}

\title{pst-rputover v 1.0}
\subtitle{A PSTricks package to place text over lines and other objects without obscuring background fills}
\author{Martin J. \textsc{Osborne}\\
and\\
Thomas \textsc{S\"{o}ll}}
\date{\today}

\maketitle

\tableofcontents

\clearpage


\begin{abstract}
The command \verb+\rput*+ allows you to place text on top of other objects (e.g.\ lines), obscuring the parts of the objects that lie within a box surrounding the text, so that the objects do not interfere with the text. In doing so, it creates a white box that obscures \textit{all} objects that are under the text.  This style allows you to place text over objects without obscuring background colors (or other objects of your choice).
\end{abstract}

\section{Introduction}
You create a PSTricks figure with regions filled with various colors.

\bigskip

\hspace*{\fill}%
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
  \psline[liftpen=1](10,0)(0,0)
  \closepath
}
\end{pspicture}%
\hspace*{\fill}

\bigskip

\noindent
You add some lines over the colors.

\bigskip

\hspace*{\fill}%
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
  \psline[liftpen=1](10,0)(0,0)
  \closepath
}
\psline[linestyle=dotted](5.3,0)(5.3,2.3)(0,2.3)
\end{pspicture}%
\hspace*{\fill}

\bigskip

\noindent
Now you want to add some labels.  You'd like these
labels to block out the lines, so that they don't interfere with the characters in your labels.

\bigskip

\hspace*{\fill}%
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
  \psline[liftpen=1](10,0)(0,0)
  \closepath
}
\rputover[autoangle=false,fboxsep=2pt]{0}(5.3,1.7){label}%
\coverable{\psline[linestyle=dotted](5.3,0)(5.3,2.3)(0,2.3)}
\end{pspicture}%
\hspace*{\fill}

\bigskip

\noindent
This style allows you to do that.

Why do you need a style?  Why not use \verb+\rput*+?  That
blocks out the lines, but it does so by creating a white rectangle that blocks
the colors too.

\bigskip

\hspace*{\fill}%
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
  \psline[liftpen=1](10,0)(0,0)
  \closepath
}
\psline[linestyle=dotted](5.3,0)(5.3,2.3)(0,2.3)
\rput*(5.3,1.7){label}%
\end{pspicture}%
\hspace*{\fill}

\bigskip

\noindent
You could change the background color of that rectangle, but  if the rectangle
includes regions with two or more colors, this approach can get pretty complicated.

This style offers a simple solution.  You use \verb+\rputover+ instead of
\verb+\rput+ and include all the objects you want to be covered by the text in
the argument of \verb+\coverable+, like so:

\small
\begin{verbatim}
\usepackage{pstricks,rputover}
...
\begin{pspicture}(100,30)
...
\rputover[fboxsep=3pt](5.3,1.7){label}%
\coverable{%
  \psline[linestyle=dotted,linewidth=0.5](5.0,0)(5.0,2.3)(0,2.3)
}
...
\end{pspicture}
\end{verbatim}

(The setting \verb+\fboxsep=3pt+ means that the padding around `\texttt{label}'  is 3 points wide.)

The style combines two ideas.  The first idea, suggested by the StackExchange user Werner on \href{http://tex.stackexchange.com/questions/353748/is-there-a-variant-of-rput-in-pstricks-that-preserves-background-fill-colors}{this page}, is to use \verb+\psDefBoxNodes+ in \verb+pst-node.sty+ to get the coordinates of the corners of the box occupied by each label.  The second idea is to use these coordinates and  \verb+\psclip+ to remove the parts of the objects in the argument of \verb+\coverable+ that overlap with the labels. The only subtlety in implementing this second idea is that we want to do a `reverse clip': we want to keep the areas \textit{outside} the labels, not the ones inside.



\section{Commands}
\subsection{\textbackslash rputover}
\verb+\rputover+ has the following syntax, where \Largb{angle} and \coord1 (as well as \OptArgs) are optional.

\begin{BDef}
\Lcs{rputover}\OptArgs\Largb{angle}\coord0\coord1\Largb{any material}
\end{BDef}

\verb+\rputover+ is intended to be used in conjunction with \verb+\coverable+, but we begin by explaining its effect by itself.

In the absence of the coordinates \coord1, the effect and syntax of \verb+\rputover+ are similar to those of \verb+\rput+.  Specifically, \Lcs{rputover}\OptArgs\Largb{angle}\coord0\Largb{any material} places \textit{any material} at \coord0 rotated by \textit{angle}, with the placement relative to \coord0 determined by \OptArgs. The syntax of \verb+\rputover+ differs from that of \verb+\rput+ only in the specification of the  placement of the box containing \textit{any material} relative to \coord0. This placement is specified in \verb+\rput+ by writing
\begin{BDef}
\Lcs{rput[p]}\Largb{angle}\coord0\Largb{any material}
\end{BDef}
where \verb+p+ is \verb+c+ (center), \verb+t+ (top), \verb+b+ (bottom), \verb+l+ (left), or \verb+r+ (right), or some combination of these letters (e.g.\ \verb+tl+ for top left). The same placement is specified in \verb+\rputover+ by writing
\begin{BDef}
\text{\Lcs{rputover[boxpos=p]}\Largb{angle}\coord0\Largb{any material}}
\end{BDef}
As for \verb+\rput+, the default value of \verb+p+ is \verb+c+.

Here is an example.\begin{LTXexample}[pos=t,width=2cm]
\begin{pspicture}(0,0)(2,1)
\rputover[boxpos=t]{45}(1,0.5){\(x\)}
\psframe[linestyle=dotted](0,0)(2,1)
\end{pspicture}
\end{LTXexample}

The optional  pair of coordinates \coord1  provides an alternative way to position \textit{any material}.  If it is present, then \textit{any material} is placed on the  line segment joining \coord1 and \coord2, by default at the midpoint of this line segment, at the same angle as the line segment.

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
\rputover(0,0)(5,3){\(x^3-1\)}
\psframe[linestyle=dotted](0,0)(10,3)
\coverable{\psline[linestyle=dotted](0,0)(5,3)}
\end{pspicture}
\end{LTXexample}


 To make the label horizontal, use the option \verb+autoangle=false+.

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
\rputover[autoangle=false](0,0)(5,3){\(x^3-1\)}
\psframe[linestyle=dotted](0,0)(10,3)
\coverable{\psline[linestyle=dotted](0,0)(5,3)}
\end{pspicture}
\end{LTXexample}

To put the label not at the midpoint of the line segment but at the point that is the fraction \(z\) of the distance between  \coord0 and \coord1 from \coord0, use the setting \verb+npos=+\(z\).
\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
\rputover[npos=0.75](0,0)(5,3){\(x^3-1\)}
\psframe[linestyle=dotted](0,0)(10,3)
\coverable{\psline[linestyle=dotted](0,0)(5,3)}
\end{pspicture}
\end{LTXexample}

If you like, you can refer to points by labels that you assign.  Doing so is handy if you use the same points several times.

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
\pnode(0,0){A}
\pnode(5,3){B}
\pnode(10,3){C}
\rputover[npos=0.75](A)(B){\(x^3-1\)}
\psframe[linestyle=dotted](A)(C)
\coverable{\psline[linestyle=dotted](A)(B)}
\psdot[linecolor=red](A)
\psdot[linecolor=ForestGreen](B)
\end{pspicture}
\end{LTXexample}

\subsection{\textbackslash coverable}
\verb+\coverable+ has a single argument, which consists of all the objects that you want to be  covered by the objects that are arguments of \verb+\putover+ commands.

\begin{BDef}
\Lcs{coverable}\Largb{any material}
\end{BDef}

\subsection{Using \textbackslash rputover and \textbackslash coverable together}
%
Here is a simple example of the way in which \verb+\rputover+ and \verb+\coverable+ are used together.  The command
\begin{BDef}
\verb+\rputover[fboxsep=4pt]{0}(5.3,1.7){label}+
\end{BDef}
puts `label' in a box with padding 4pts wide at the point \verb+(5.3,1.7)+ and
\begin{BDef}
\verb+\coverable{\psline[linestyle=dashed](5.3,0)(5.3,2.3)(0,2.3)}}+
\end{BDef}
draws a (dashed) line that is `coverable' by the box containing the label.
\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
    \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
    \psline[liftpen=1](10,0)(0,0)
    \closepath
}
% Place `label' at (5.3,1.7)
\rputover[fboxsep=4pt]{0}(5.3,1.7){label}
% Draw a dashed line, but allow the label to cover it
\coverable{\psline[linestyle=dashed](5.3,0)(5.3,2.3)(0,2.3)}
\end{pspicture}
\end{LTXexample}

The next example illustrates the use of named nodes (\verb+A+ and \verb+B+) and the placement of a label over the midpoint of a line using the second pair of coordinates in \verb+\rputover+.
\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(0,0)(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
    \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}
    \psline[liftpen=1](10,0)(0,0)
    \closepath
}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Label, placed by default at the midpoint of the line segment from A to B
\rputover[fboxsep=2pt](A)(B){\(x^3+z-1\)}
% Line from A to B (with arrows at the ends) (\psline may be used instead of \pcline)
\coverable{\pcline{<->}(A)(B)}
\end{pspicture}
\end{LTXexample}

To set the label automatically you can use autoangle=true and autopoint=true, so the label will set between the last two (optional) points of \verb+\rputover+
like in the way of \verb+\cput*+.

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Label 40% of the way from A to B, rotated by 90 degrees, in a box.  You can use any
% Postscript expression for the angle (for example, you can write 20 4.5 mul instead
% of 90).
\rputover[fboxsep=0pt,npos=0.4]{90}(A)(B){%
  \psframebox[framesep=3pt]{Label: \(x=a^2\)}
}
% Line between A and B, coverable by the label
\coverable{\pcline{<->}(A)(B)}
% The (Postscript) constant delta1 is the angle of a line perpendicular to the line
% from A to B, and can be used elsewhere in the picture.  (The ! prefix in the argument
% of \rput indicates Postscript.)
\rput{!delta1}(0.4,1.4){test1}
\rput[bl]{!delta1 90 sub}(0.6,1.8){\color{blue}test2}
\end{pspicture}
\end{LTXexample}

\subsection{\textbackslash pclineover}

As shorthand for
\begin{BDef}
\Lcs{rputover}\OptArgs\coord1\coord2\Largb{any material}\\
\Lcs{coverable}\verb+{+\Lcs{pcline}\coord1\coord2\verb+}+
\end{BDef}
you can write
\begin{BDef}
\Lcs{pclineover}\OptArgs\coord1\coord2\Largb{any material}\\
\end{BDef}
as illustrated in the following examples.



\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B, covered by label `\(\alpha\) and others'
\pclineover(A)(B){\(\alpha\) and others}
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B, covered by label `\(\alpha\) and others' rotated by 90 degrees
\pclineover[angleadd=90](A)(B){$\alpha$ and others}
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to (8.5,2) with arrows and bars at the ends, covered by label `\(\alpha\)
% and others' in a box with a transparent cyan background
\pclineover[fboxsep=0pt,arrows=|->|](A)(8.5,2){%
  \psframebox[fillstyle=solid,fillcolor=cyan,opacity=0.2,linestyle=none,linewidth=0pt]{\(\alpha\) and others}}
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B offset by 5pt, covered by label `\(\alpha\) and more text' in a box
% with a transparent tan background
\pclineover[offset=5pt,fboxsep=0pt](A)(B){%
  \psframebox[fillstyle=solid,fillcolor=Tan,opacity=0.65,linestyle=none,linewidth=0pt]{\(\alpha\) and more text}}
% Bars at the end of the line
\pcline[linestyle=none,offset=5pt]{|*-|*}(A)(B)
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B offset by 5pt, covered by label `\(\alpha\) and ...' in a box
% with a transparent yellow background offset by 10pt to the left and 3pt up
\pclineover[offset=5pt,fboxsep=0pt,arrows=|<->|,absnodesep={-10pt,3pt}](A)(B){%
  \rnode{C}{\psframebox[fillstyle=solid,fillcolor=yellow,opacity=0.65,linestyle=none,linewidth=0pt]{\(\alpha\) and other material \(\int_{a}^{b}\)}}}
\end{pspicture}
\end{LTXexample}

\subsection{\textbackslash pcarrowC}

As shorthand for
\begin{BDef}
\Lcs{pclineover}\verb+[arrows=|<->|,other options]+\coord1\coord2\Largb{any material}\\
\end{BDef}
you can write
\begin{BDef}
\Lcs{pcarrowC}\verb+[other options]+\coord1\coord2\Largb{any material}
\end{BDef}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B with arrows and bars at the ends, covered by label `\(\beta\) ...'
\pcarrowC(A)(B){$\beta$ and other material}
\end{pspicture}
\end{LTXexample}


\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B offset by 8pt, with bars 0.4pt thick and 12pt long offset by 6pt,
% covered by label `\(\gamma\) ...'
\pcarrowC[offset=8pt,baroffset=6pt,Cbarwidth=0.4pt,tbarsize=12pt,addbars=|*-|*](A)(B){\(\gamma\) and other material \(\int_{a}^{b}\)}
\end{pspicture}
\end{LTXexample}

\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
% Colored background
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
% Red and green dots
\pnode(1.5,0){A}\psdot[linecolor=red](A)\pnode(8.5,3){B}\psdot[linecolor=green](B)
% Line from A to B with bars 0.2pt thick and 20pt long, covered by label `\(\beta\) ...'
\pcarrowC[addbars=|-|,Cbarwidth=0.2pt,tbarsize=20pt]{<->}(A)(B){\(\beta\)}
\end{pspicture}
\end{LTXexample}


\subsection{inverscl option for \textbackslash coverable}
An experimental feature is the option \verb+inverscl+ for \verb+\coverable+. This option inverts the clip.  All material above the figure will be clipped, so use this option only at the top of a page.  (Even then, any running head is clipped.)

\newpage


\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
\rputover(1,1){\sffamily label}
\rputover{30}(1.6,1.6){\sffamily \parbox[t][1cm][c]{2cm}{nur mal wieder Text}}
\rputover[boxpos=tr](4.5,1.4){aha, this is crazy}
\coverable[inverscl=true]{%
  \psgrid
  \psline(0,0)(2,2)
  \pscurve[linecolor=blue](1,0)(1.5,0.5)(0.5,1.5)(1,2)
  \psline(1,0)(2,2)
}
\end{pspicture}
\end{LTXexample}

Compare this figure with one produced by the same code with the omission of  \verb+inverscl=true+.
\begin{LTXexample}[pos=t,width=10cm]
\begin{pspicture}(10,3)
\psframe[fillstyle=solid,fillcolor=blue!20,linestyle=none](0,0)(10,3)
\pscustom[fillstyle=solid,fillcolor=red!30,linestyle=none]{%
  \psplot[plotpoints=500]{0}{10}{x 150 mul cos 1.2 mul 1.5 add}\psline[liftpen=1](10,0)(0,0)\closepath}
\rputover(1,1){\sffamily label}
\rputover{30}(1.6,1.6){\sffamily \parbox[t][1cm][c]{2cm}{nur mal wieder Text}}
\rputover[boxpos=tr](4.5,1.4){aha, this is crazy}
\coverable{%
  \psgrid
  \psline(0,0)(2,2)
  \pscurve[linecolor=blue](1,0)(1.5,0.5)(0.5,1.5)(1,2)
  \psline(1,0)(2,2)
}
\end{pspicture}
\end{LTXexample}


\newpage


\section{History}
\begin{description}
\item[Version 1.0, 2017.6.29] First version
\end{description}



\clearpage
\section{List of all optional arguments for \texttt{pst-rputover}}

\xkvview{family=pst-rputover,columns={key,type,default}}

\clearpage
%\bgroup
%\RaggedRight
%\nocite{*}
%\bibliographystyle{plain}
%\printbibliography{pst-rputover-doc}
%\egroup

%\printindex

\end{document}
