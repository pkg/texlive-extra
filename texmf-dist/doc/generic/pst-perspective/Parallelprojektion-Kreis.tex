\documentclass[a4paper,11pt,fleqn]{article}
\usepackage[T1]{fontenc}
\usepackage[sfmath,light]{kpfonts}
\usepackage[sfdefault]{libertine}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{etex} % um die Anzahl der Register zu erh\"{o}hen (sonst nur 256)

\usepackage{geometry}
\geometry{paperheight=298mm,paperwidth=210mm,tmargin=5mm,textwidth=180mm,textheight=260mm,
  rmargin=15mm,heightrounded,includeheadfoot,headheight=5mm,headsep=1mm,foot=18mm,
  marginparsep=0mm,marginparwidth=0mm}

\usepackage[distiller,cmyk]{pstricks}
\usepackage{pst-plot,pst-eucl}
\usepackage{pst-text}
\usepackage{pst-grad}
\usepackage{pst-perspective}
\usepackage{pstricks-add}

\pagestyle{empty}
\setlength\parindent{0pt}

\newcommand{\Zylinder}[9]{%  #1 L\"{a}nge, #2 Radius, #3 gradbegin, #4 gradend, #5 gradmidpoint
\pscustom[dimen=#9,fillstyle=gradient,gradbegin=#3,gradend=#4,gradmidpoint=#5,gradangle=90,linecolor=black,linewidth=#7,linestyle=#8]{%
\psellipticarc(0,0)(!#2 #2 0.3 mul){180}{360}
\psellipticarcn(0,#1)(!#2 #2 0.3 mul){0}{-180}
\closepath
}
\psellipse[fillstyle=gradient,gradbegin=#3,gradend=#4,gradangle=20,gradmidpoint=0.5,linestyle=solid,linewidth=#7](0,#1)(!#2 #2 0.3 mul)
}

\begin{document}


\section{Mathematische Grundlagen zur Parallelprojektion eines Kreises}

Das Schr\"{a}gbild eines Kreises ergibt eine Ellipse, allerdings stimmt der Radius der Ellipse nicht mit der gro{\ss}en Halbachse der Ellipse \"{u}berein. Au{\ss}erdem ist die Ellipsenachse gegen den Kreisdurchmesser um einen Winkel $\alpha$ gedreht.

{\psset{unit=1.75}
\begin{pspicture}[showgrid=false](-0.35,-1)(10,4.4)
\begin{psclip}%
{\psframe[linestyle=none](-0.3,-0.8)(9.8,4.3)}
\psgrid[subgriddiv=2,gridlabels=0,gridwidth=0.5pt,gridcolor=black!50,subgridwidth=0.4pt,subgridcolor=black!30](-1,-1)(12,6)
\end{psclip}
\psset{arrowsize=0.1,arrowinset=0.05,arrowlength=2}
\rput(4,0){\psaxes[labelFontSize=\scriptstyle,labels=none,ticks=none,Ox=4]{->}(0,0)(-4.2,0)(5.5,4.3)[$x$,-90][$y$,0]}
\pnode(4,0){M}
\psarc(M){4}{0}{180}
\psArcTS[linecolor=orange!60,originT={4,0}]{4}{-30}{180}
\pstransTS[translineA=true,translineB=true,originT={4,0}](4;30){A'}{A}
\pnode(A|0,0){PA}
\pnode(A'|0,0){PB}
\pcline[linecolor=red](4,0)(A)
\naput[nrot=:U,labelsep=1.4pt,npos=0.65]{\color{red}$R$}
\pcline[linecolor=green](4,0)(A')
\naput[nrot=:U,labelsep=1.4pt,npos=0.75]{\color{green}$r$}
\psdot[linecolor=blue,dotsize=1.8pt](A)\psdot[linecolor=green,dotsize=1.8pt](A')
\pstMarkAngle[LabelSep=2.0,MarkAngleRadius=2.4,linecolor=red,arrows=->]{5,0}{4,0}{A}{\color{red}$\alpha$}%
\pstMarkAngle[LabelSep=1.0,MarkAngleRadius=1.35,linecolor=green,arrows=->]{5,0}{4,0}{A'}{\color{green}$\beta$}%
\pstMarkAngle[LabelSep=0.4,MarkAngleRadius=0.75,linecolor=blue,arrows=->]{PA}{PB}{A}{\color{blue}$\varphi$}%
\pcline[offset=-0.4,linecolor=green]{|<->|}(4,0)(A'|0,0)
\ncput*{\color{green}$x$}
\pcline[offset=-0.45,linecolor=blue]{|<->|}(A|0,0)(A)
\nbput{\color{blue}$y'$}
\pcline[offset=-0.4,linecolor=blue]{|<->|}(A'|0,0)(A|0,0)
\nbput[labelsep=1pt]{\color{blue}$x'$}
\pcline[offset=-0.9,linecolor=green]{|<->|}(A'|0,0)(A')
\ncput*{\color{green}$y$}
\end{pspicture}
}

Drehwinkel und Halbachse der Ellipse sollen nun ermittelt werden.

F\"{u}r den Kreis im ersten und zweiten Quadranten gilt: $y(x) = \sqrt{r^{2} - x^{2}}$

Die Entfernung der Ellipsenpunkte vom Mittelpunkt des Kreises sei $R$.\\
Mit Pythagoras gilt dann:  $R(x) = \sqrt{(x+x')^{2}+y'^{2}}$, wobei\\ $y' = y\cdot v\cdot \sin(\varphi)$ mit dem Verk\"{u}rzungsfaktor $v$ und dem Verzerrungswinkel $\varphi$.\\
$x'$ und $y'$ erh\"{a}lt man \"{u}ber den Verzerrungswinkel und den Verk\"{u}rzungsfaktor:\\
$x' = y\cdot v\cdot \cos(\varphi)$ \quad und \quad $y' = y\cdot v\cdot \sin(\varphi)$.
%
\begin{equation*}
   R(x) = \sqrt{\left(x+y\cdot v\cdot \cos(\varphi)\right)^{2}+\left(y\cdot v\cdot \sin(\varphi)\right)^{2}}
\end{equation*}
%
\begin{equation*}
   R(x) = \sqrt{\left(x+\sqrt{r^{2} - x^{2}}\cdot v\cdot \cos(\varphi)\right)^{2}+\left(\sqrt{r^{2} - x^{2}}\cdot v\cdot \sin(\varphi)\right)^{2}}
\end{equation*}
%
Ausmultiplizieren und zusammenfassen (trigonometrischer Pythagoras!):
%
\begin{equation*}
  R(x)= \sqrt{x^{2}\cdot \left(1-v^{2}\right) + r^{2}v^{2} + 2xv\cdot \cos(\varphi)\sqrt{r^{2} - x^{2}}}
\end{equation*}
%
Die beiden Extremwerte dieser Funktion liefern die $x$-Werte des Kreises, die zur gro{\ss}en und zur kleinen Halbachse der Ellipse geh\"{o}ren. Diese werden nun bestimmt indem nicht $R(x)$, sondern $\left(R(x)\right)^{2}$ betrachtet wird, denn die Extremwerte von $\left(R(x)\right)^{2}$ sind auch die von $R(x)$; man spart sich die Wurzel!)
%
\begin{equation*}
  R(x)^{2}= x^{2}\cdot \left(1-v^{2}\right) + r^{2}v^{2} + 2xv\cdot \cos(\varphi)\sqrt{r^{2} - x^{2}}
\end{equation*}
\begin{equation*}
  \left(R(x)^{2}\right)' = 2x(1-v^{2}) + 2v\cos(\varphi)\cdot \frac{r^{2}-2x^{2}}{\sqrt{r^{2}-x^{2}}}
\end{equation*}
%
Nullsetzen dieser Ableitung liefert vier L\"{o}sungen, die paarweise symmetrisch sind:
%
\begin{equation*}
  x_{1/3} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{\frac{v^{4} + 4 v^{2}\cos^{2}(\varphi) - 2v^{2} + 1 + \sqrt{\left(v^{2} - 1\right)^{2}\left(v^{4} + 4v^{2}\cos^{2}(\varphi) - 2v^{2} + 1\right)}}{v^{4} + 4v^{2}\cos^{2}(\varphi) - 2v^{2} + 1}}
\end{equation*}
%
\begin{equation*}
  x_{2/4} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{\frac{v^{4} + 4 v^{2}\cos^{2}(\varphi) - 2 v^{2} - \sqrt{\left(v^{2} - 1\right)^{2} \left(v^{4} + 4v^{2}\cos^{2}(\varphi) - 2v^{2} + 1\right)}}{v^{4} + 4v^{2}\cos^{2}(\varphi) - 2v^{2} + 1}}
\end{equation*}
%
Eine weitere Vereinfachung l\"{a}sst sich durch die Beziehung $2\cos^{2}(\varphi) -1 = \cos(2\varphi)$ erreichen:
%
\begin{equation*}
  x_{1/3} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{\frac{v^{4} + 2 v^{2}\cos(2\varphi) + 1 + \left(v^{2} - 1\right)\sqrt{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}
\end{equation*}
%
\begin{equation*}
  x_{2/4} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{\frac{v^{4} + 2 v^{2}\cos(2\varphi) + 1 - \left(v^{2} - 1\right)\sqrt{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}
\end{equation*}
%
Nochmals unter der Wurzel gek\"{u}rzt und zusammengefasst und mit der Abk\"{u}rzung : $t=\frac{1 - v^{2}}{\sqrt{v^{4}+2v^{2}\cos(2\varphi) + 1}}$:
%
\begin{equation*}
  x_{1/2/3/4} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{1 \pm \frac{v^{2} - 1}{\sqrt{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}} = \pm\frac{r}{2}\sqrt{2}\cdot\sqrt{1 \pm t}
\end{equation*}
%
Die Ergebnisse nun wieder in $R(x)$ eingesetzt liefert die beiden Halbachsen:
%
\begin{equation*}
  R(x_{1})= r\cdot\sqrt{\frac{1}{2}\left( 1 + \frac{1 - v^{2}}{\sqrt{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}\right)\cdot \left(1-v^{2}\right) + v^{2} + v\cdot \cos(\varphi)\sqrt{1 - \frac{\left(1 - v^{2}\right)^{2}}{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}}
\end{equation*}
%
\begin{equation*}
    R(x_{1})= r\cdot\sqrt{\frac{1}{2}\left( 1 + t\right)\cdot \left(1-v^{2}\right) + v^{2} + v\cdot \cos(\varphi)\sqrt{1-t^{2}}}
\end{equation*}
%
\begin{equation*}
    R(x_{2})= r\cdot\sqrt{\frac{1}{2}\left( 1 - t\right)\cdot \left(1-v^{2}\right) + v^{2} - v\cdot \cos(\varphi)\sqrt{1-t^{2}}}
\end{equation*}
%
Der Skizze entnimmt man, dass sich der Winkel $\beta$ \"{u}ber $\arccos\left(\frac{x}{r}\right)$ bzw. $\arcsin\left(\frac{x}{r}\right)$ berechnen l\"{a}sst:
\begin{equation*}
  \beta = \arccos\left(\frac{x_{1}}{r}\right) = \arcsin\left(\frac{x_{2}}{r}\right) = \arcsin\left( \frac{1}{2}\sqrt{2}\cdot\sqrt{1 - \frac{1 - v^{2}}{\sqrt{v^{4} + 2 v^{2}\cos(2\varphi) + 1}}}\right)
  = \arcsin\left( \frac{1}{2}\sqrt{2}\cdot\sqrt{1 - t}\right)
\end{equation*}
Der Winkel $\alpha$ ergibt sich folgenderma{\ss}en:
\begin{equation*}
  \alpha = \arcsin\left( \frac{y'}{R}\right) = \arcsin\left( \frac{y\cdot v\cdot \sin(\varphi)}{R}\right)
   = \arcsin\left( \frac{\frac{\sqrt{2}}{2}\cdot\sqrt{1 - t}\cdot v\cdot \sin(\varphi)}{\sqrt{\frac{1}{2}\left( 1 + t\right)\cdot \left(1-v^{2}\right) + v^{2} + v\cdot \cos(\varphi)\sqrt{1-t^{2}}}}\right)
\end{equation*}


\subsection{Polarkoordinaten}

\begin{equation*}
\tan2\beta=\frac{2v\cos\varphi}{1-v^2}\quad \tan\alpha = \frac{\tan\varphi}{\frac{1}{v\cos\varphi\tan\beta}+1}
\end{equation*}
%
\begin{equation*}
R_{\max} = r\sqrt{\frac{1}{2}\left(1+\cos\left(\arctan\left[\frac{2v\cos\varphi}{1-v^2}\right]\right)\right)
(1-v^2)+v^2+v\cos\varphi\cdot\sin\left(\arctan\left[\frac{2v\cos\varphi}{1-v^2}\right]\right)}
\end{equation*}



\subsection{Spezialfall $\varphi=45^{\circ}$, $v=0,5$}

Die Terme lassen sich nochmals vereinfachen und man erh\"{a}lt mit $t=\frac{3\cdot \sqrt{17}}{17}$:
\begin{equation*}
  x_{1} = \frac{r}{34}\sqrt{578+102\sqrt{17}} \approx r\cdot 0,9294102631; \qquad x_{2} = -\frac{r}{34}\sqrt{578-102\sqrt{17}} \approx -r\cdot 0,369048184
\end{equation*}
Daraus berechnet man die gro{\ss}e und die kleine Halbachse, sowie die beiden Winkel zu:
\begin{equation*}
  a = R(x_{1}) = \frac{r}{4}\sqrt{10 + 2\sqrt{17}} \approx r\cdot 1,067889602
\end{equation*}
\begin{equation*}
  b = R(x_{2}) = \frac{r}{4}\sqrt{10 - 2\sqrt{17}} \approx r\cdot 0,3310767232
\end{equation*}
%
\begin{equation*}
  \beta = \arcsin\left( \frac{1}{34}\sqrt{578-102\sqrt{17}}\right) \approx 21,65692833^{\circ}
\end{equation*}
%
\begin{equation*}
  \alpha = \arcsin\left( \frac{\sqrt{1-\frac{3\cdot \sqrt{17}}{17}}}{\sqrt{10 + 2\cdot\sqrt{17}}}\right) \approx 7,018121736^{\circ}
\end{equation*}

\begin{pspicture}[showgrid=true](1,-1.8)(16,6)
%
\psset{toplinewidth=0.3pt,opacity=0.3}
{\psset{vkf=0.4,phi=33}
\psboxTS[linewidth=0.5pt,opacity=0.2,linejoin=1,hideline=true,dash=2pt 2pt,hidelinewidth=0.3pt](-2,2,1){4}{4}{4}{yellow}
\rput(4,1){%
\psZylinderTS[topfillcolor=red,opacity=0.6,linewidth=0.5pt,fillstyle=gradient,gradbegin=black!90!blue!80,gradend=black!40!blue!30,gradangle=90,gradmidpoint=0.25]{2}{4}}
\psboxTS[linewidth=0.5pt,opacity=0.1,linejoin=1](-2,2,1){4}{4}{4}{yellow}
}

{\psset{vkf=0.6,phi=60}
\psboxTS[linewidth=0.5pt,opacity=0.2,linejoin=1,hideline=true,dash=2pt 2pt,hidelinewidth=0.3pt](-2,8,1){4}{4}{4}{cyan}
\rput(10,1){\psZylinderTS[opacity=0.6,linewidth=0.5pt,fillstyle=gradient,gradbegin=black!90!blue!80,gradend=black!40!blue!30,gradangle=90,gradmidpoint=0.25]{2}{4}}
\psboxTS[linewidth=0.5pt,opacity=0.1,linejoin=1](-2,8,1){4}{4}{4}{cyan}
}

\rput{-90}(1,-1){\psZylinderTS[phi=-70,vkf=0.4,linewidth=0.5pt,fillstyle=gradient,gradbegin=black!90!green!80,%
gradend=black!40!green!30,gradangle=90,gradmidpoint=0.25,topfillstyle=gradient,topmidpoint=0.5,topangle=25]{0.5}{13}}
\end{pspicture}

\begin{pspicture}[showgrid=true](1,-0.3)(13,6)
%
\psset{toplinewidth=0.3pt,opacity=0.3}
\pnode(4,3){MPK}
{\psset{vkf=0.3,phi=20}
\psboxTS[linewidth=0.5pt,opacity=0.2,linejoin=1,hideline=true,dash=2pt 2pt,hidelinewidth=0.3pt](-2,8,1){4}{4}{4}{green}
\rput(10,1){\psZylinderTS[opacity=0.6,linewidth=0.5pt,fillstyle=gradient,gradbegin=black!90!blue!80,gradend=black!40!blue!30,gradangle=90,gradmidpoint=0.25]{2}{4}}
\psboxTS[linewidth=0.5pt,opacity=0.1,linejoin=1](-2,8,1){4}{4}{4}{green}
}

\psrotate(MPK){0}{\psCircleTS[fillstyle=solid,fillcolor=green,opacity=0.5,linestyle=dashed,originT={MPK}]{3}}
\psCircleTSX[fillstyle=solid,fillcolor=green,opacity=0.5,linestyle=solid,originT={MPK},linewidth=0.3pt]{3}

\pnode(4,4){MPK}
\psArcTS[linestyle=dashed,originT={MPK}]{3}{0}{80}
\end{pspicture}

\begin{pspicture}[showgrid=false,shift=0](0,0)(13,6)%

\pnode(1,1){A}
\pnode(1,5){B}
\pnode(5,1){C}

\rput{0}(3,1){%
\psZylinderTS[hideline=true,hidelinewidth=0.3pt,toplinewidth=0.5pt,dash=2pt 2pt,opacity=0.6,linewidth=0.5pt,fillstyle=gradient,%
gradbegin=black!90!blue!80,gradend=black!40!blue!30,gradangle=90,gradmidpoint=0.25]{2}{4}}

\pcline[linestyle=dashed,dash=3pt 2pt,nodesepA=2pt,linewidth=0.6pt](A)([nodesep=-0.9]A)
\pcline[linestyle=dashed,dash=3pt 2pt,nodesepA=2pt,linewidth=0.6pt](B)([nodesep=-0.9]B)

\pcline[linestyle=dashed,dash=3pt 2pt,nodesepA=3pt,linewidth=0.6pt](A)([offset=-1.1]A)
\pcline[linestyle=dashed,dash=3pt 2pt,nodesepA=3pt,linewidth=0.6pt](C)([offset=-1.1]C)

\pcline[offset=0.6,arrowsize=0.15,arrowlength=2,arrowinset=0.03]{<->}(A)(B)
\ncput*{$h$}

\pcline[offset=-1,arrowsize=0.15,arrowlength=2,arrowinset=0.03]{<->}(A)(C)
\ncput*{$d$}

\psboxTS[opacity=0.2,vkf=0.43,linejoin=1,hideline=true](-2,6.9,1){4}{4.2}{4}{red}
\rput{0}(9,1){\Zylinder{4}{2}{black!90!blue!80}{black!40!blue!30}{0.2}{black!60!blue!70}{0.5pt}{solid}{middle}}
\psboxTS[opacity=0.3,vkf=0.43,linejoin=1](-2,6.9,1){4}{4.2}{4}{red}

%\psboxTS[opacity=0.2,vkf=0.43,linejoin=1,hideline=true](-2,6.9,1){4}{4.2}{4}{red}
%\rput{0}(9,1){\Zylinder{4}{2}{black!90!blue!80}{black!40!blue!30}{0.2}{black!60!blue!70}{0.5pt}{solid}{middle}}
%\psboxTS[opacity=0.3,vkf=0.5,linejoin=1,hideline=true](-2,7,1){4}{4}{4}{red}
%
%\psrotate(9,1){6.38}{\psellipticarc[linecolor=red](9,1)(2.12,0.68){180}{360}}

\end{pspicture}




\end{document} 