The pst-geometrictools package Author: Thomas Söll

"pst-geometrictools" is a PSTricks package to draw a protractor, a ruler, a compass and pencils 

Dated: 2021/12/28 Version 1.3

pst-geometrictools contains the following:

1) pst-geometrictools.sty
2) pst-geometrictools.tex

- psProtractor
- psRuler
- psPencil
- psCompass

Save the files pst-geometrictools.sty|tex in a directory, which is part of your 
local TeX tree.
Then do not forget to run texhash to update this tree.
For more information  see the documentation of your LATEX distribution 
on installing packages into your local TeX system

pst-geometrictools needs pst-eucl, pstricks-xkey and pstricks, which should 
be part of your local TeX installation, otherwise get it from a 
CTAN server, https://mirror.ctan.org

PSTricks is PostScript Tricks, the documentation cannot be run with pdftex, 
use the sequence latex->dvips->ps2pdf or latex->dvips->distiller or lualatex.

T. Söll

(Bugfixes by Herbert Voß hvoss@tug.org)
