pst-eucl.pro --------
1.04 2020/09/29     - some new functions for 1.75
1.03 2020/01/09     - some new functions for 1.69
1.02 2019/11/21     - add DeterminantTwo,DeterminantThree,DeterminantFour,DeterminantFive.
1.01 2012/09/21     - fix for introduced bug
1.00 2011/08/05     - fix bug in /InterLines


pst-eucl.tex --------
1.75  2020/09/29 - add macro to calc the coefficents of general conic $ax^2+bxy+cy^2+dx+ey+f=0$ through given five points, \pstGeneralConicEquation.
                 - add macro to calc the coefficents of general conic $ax^2+bxy+cy^2+dx+ey+f=0$ of the given ellipse, \pstGeneralEllipseEquation.
                 - add macro to calc the coefficents of general conic $ax^2+bxy+cy^2+dx+ey+f=0$ of the given hyperbola, \pstGeneralHyperbolaEquation.
                 - add macro to calc the coefficents of general conic $ax^2+bxy+cy^2+dx+ey+f=0$ of the given parabola, \pstGeneralParabolaEquation.
                 - add macro to draw the intersections of line AB and general conic $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicLineInter.
                 - add macro to draw the intersections of circle OA and general conic $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicCircleInter.
                 - add macro to draw the intersections of ellipse and general conic $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicEllipseInter.
                 - add macro to draw the intersections of hyperbola and general conic $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicHyperbolaInter.
                 - add macro to draw the intersections of parabola and general conic $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicParabolaInter.
                 - add macro to draw the intersections of two general conics $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralConicInter.
                 - fix an issue in \pstGeneralHyperbolaCoef that it gets the wrong symmetrical axis when draw quadratic curve 2xy-x+2y+1=0.
                 - update \pstGeneralHyperbolaABCDE to support the quadratic curve equation with a=0, e.g, given five points from curve xy=1.  
                 - update the (pos) parameter U|D|L|R to :U|:D|:L|:R in \pstTriangleSSS, \pstTriangleSAS etc,
                   and support (node) to specify a base line to draw the triangle.
1.74  2020/07/15 - add macro to draw the equilateral triangle on a given side AB, \pstETriangleAB.
                 - add macro to draw the square on a given side AB, \pstSquareAB.
                 - add macro to draw the regular polygon on a given side AB, \pstRegularPolygonAB.
                 - add macro to draw the regular polygon with center O and base point A, \pstRegularPolygonOA.
                 - add macro to draw the circle with radius length and two nodes A, B, \pstCircleABR.
1.73  2020/06/07 - Allow the star version for \pstLabelAB to use \cput* or \cput for the label
1.72a 2020/06/07 - fix a typo in the macro definition \pstMarkAngle
1.72  2020/04/18 - revert the change of \pstTriangle in v1.69, we should use \pst@object to clear \pst@par.
                 - add macro to draw the Lemonie Point of the given triangle, \pstTriangleLC.
1.71  2020/02/26 - add macro to wrap the native macro \pspolygon just group the parameters as local, \pstPolygon.
                 - revert the change of \pstCircleOA and \pstCircleAB in v1.66, we should use \pst@object to clear \pst@par.
                 - add macro to draw the nine point circle and its center, \pstTriangleNC.
                 - add macro to draw the general ellipse by its focus and one node on it, \pstGeneralEllipseFFN.
                 - add macro to draw the general hyperbola by its focus and one node on it, \pstGeneralHyperbolaFFN.
1.70  2020/01/29 - add optional argument RightAngleDotDistance to controll the the dot
                   position for right angles (type german or swissromand)
1.69  2020/01/09 - add macro to get the chord with specified length, \pstCircleChordNode.
                 - add macro to draw the center of the triangle's escribed circle, \pstTriangleEC.
                 - add macro to draw the orthocenter of triangle, \pstTriangleHC.
                 - add macro to draw the gravity center of triangle, \pstTriangleGC.
                 - update macro \pstTriangleIC and \pstTriangleOC how to control the output points.
                 - update macro \pstMediatorAB to work with option PointSymbolA and PointSymbolB.
                 - update macro \pstLineAB to group the parameters as local to avoid affected the other macros.
                 - update macro \pstTriangle to group the parameters as local to avoid affected the other macros.
1.68  2019/11/21 - add macros to construct a triangle by SSS, SAS, ASA, AAS, \pstTriangleSSS, \pstTriangleSAS, etc.
                 - add macro to get the bisector node of angle AOB, \pstBisectorAOB, refer to pstBissectBAC, pstOutBissectBAC.
                 - add macro to get the Golden Mean node of a given segment, \pstGoldenMean.
                 - add macro to define an ellipse by its focus(F) and directrix line(l) and eccentricity(e), \pstGeneralEllipseFle.
                 - add macro to define an ellipse by quadratic curve equation $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralEllipseCoef.
                 - add macro to define an ellipse by given five points, \pstGeneralEllipseABCDE.
                 - add macro to define an parabola by its focus(F) and directrix line(l), \pstGeneralParabolaFl.
                 - add macro to define an parabola by quadratic curve equation $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralParabolaCoef.
                 - add macro to define an parabola by given five points, \pstGeneralParabolaABCDE.
                 - add macro to define an hyperbola by its focus(F) and directrix line(l) and eccentricity(e), \pstGeneralHyperbolaFle.
                 - add macro to define an hyperbola by quadratic curve equation $ax^2+bxy+cy^2+dx+ey+f=0$, \pstGeneralHyperbolaCoef.
                 - add macro to define an hyperbola by given five points, \pstGeneralHyperbolaABCDE.
                 - add macro to draw the line Ax+By+C=0, \pstLineCoef.
1.67  2019/10/28 - add macros to add/subtract/divide the length of two segment, \pstDistAdd, \pstDistSub, and \pstDistDiv etc.
                 - add macros to reduce or enlarge the distance, \pstDistMul, \pstDistCoef.
                 - add macro to get the distance from point C to line AB, \pstDistABC.
                 - add macro to get the definite proportion node on segment, \pstProportionNode.
                 - add macro to get the fourth harmonic point, \pstFourthHarmonicNode.
                 - add macro to put the label for the segment, \pstLabelAB.
                 - add macro to locate a point on segment with specified length, \pstLocateAB.
                 - add macro to extend a segment to a new point with specified length, \pstExtendAB.
                 - add macro to get the inversion mapping of a point to the inversion center, \pstInversion.
                 - add macro to get the Geometric Mean of two segment, \pstGeometricMean.
                 - add macro to get the Harmonic Mean of two segment, \pstHarmonicMean.
                 - add macro to get the radical axis of two circles, \pstCircleRadicalAxis.
1.66  2019/10/20 - add macros to operate the node coordinates, \pstAbscissa, \pstOrdinate, \pstMoveNode etc.
                 - add optional parameters angleA and angleB for \pstCircleOA and \pstCircleAB.
                 - add optional parameters to output the inner circle center and outer circle center for \pstTriangleIC and \pstTriangleOC.
                 - add macros to draw the tangent line and tangent node of circle.
                 - add macros to draw the external and internal common tangent lines of two circles.
                 - add macros to draw conics (ellipse, parabola and hyperbola) and their geometrical elements, such as focus, directrix and intersections.
1.65  2019/08/19 - new type for angle
1.64  2019/01/31 - fix for PointName and pstInterCC
1.63  2019/01/27 - fix for PointSymbol=none for pstTriangle
1.62  2019/01/13 - added fillstyle for angles
1.61  2018/12/11 - added macros for inner and outer circle of a triangle
1.60a 2018/12/08 - fix for  typos in the documentation
1.60 2018/10/06 - added \pstDistCalc, which use algebraic
                  notation for the argument
                - fix for PointSymbol=none, PointName=Z
1.59 2018/09/01 - fix introduced bug in \Pst@geonodelabel
1.58 2018/08/07 - allow PointSymbol?=none
1.57 2017/11/28 - fix bug with StandardSyml->StandardSymL
1.56 2017/04/18 - \psGetAngleABC:
                  - added   dec -1 le { /dec 15 def } if
                  - added \pst@usecolor\pslinecolor in line 1616
1.55 2016/10/11 - fix for \pstRightAngle
1.54 2016/09/01 - added MarkArrow, MarkArroww,MarkArrowww
1.53 2016/05/03 - revert changes of CodeFig(A|B)
1.52 2015/10/19 - added more optional arguments (ts)
1.51 2014/05/17 - added two new functions for angles and distances
1.50 2014/04/05 - added MarkHashLength|Sep (hv)
1.49 2014/03/17 - allow algebraic mode for interlines (hv)
1.48 2013/05/02 - take \MarkAngle into account (hv)
1.47 2013/03/12 - insert \ignorespaces in \pstRightAngle (hv)
1.46 2013/01/09 - use the eucl dictionary when only needed (hv)
1.45 2012/12/31 - fixed introduced bug with PtoC (hv)
                   moved loading of pst-xkey to the beginning
1.44 2012/09/28 - allow filling of angles (hv)
1.43 2012/08/22 - delete the node macro from pst-node (hv)
1.42 2011/12/22 - modified code for marks (hv)
1.41 2011/11/19 - added different marks for angles (hv)
1.40 2011/11/16 - fix for \pstMarkAngle (hv)
1.39 2011/09/22 - fixed introduced bug in \psInterCC
1.38 2011/08/05 - set nodes directly with \pnode(!..) istead of \rput
1.37 2011/05/05 - rewrite of \pstMarkAngle (hv)
1.36 2010/08/23 - fix for \pstMarkAngle (hv)
1.35 2009/01/19 - new option labelColor (hv)
1.34 2006/01/28 - use \psscalebox instead of \scalebox
                - small tweaks
%% 2000-10-16 : creation of the file from a first LaTeX protype sty file
%% 2001-05-7  : distribution of the first beta version
%% 2002-03-21 : distribution of the second beta version
%% 2002-12-01 : distribution of the pre-release 1.0
%% 2003-03-23 : direct computation of coordinates for the center of gravity and
%%              the center of the circum circle, avoiding creation of intermediates
%%              nodes.
%% 2003-12-16 : Integration of modifications given by Alain DELPLANQUE
%%              automatic computation of PosAngle for several commands,
%%              and ability to give a list of point for pstGeonode, pstOIJGeonode
%% 2004-09-05 : Improvement of the management of the Point name end of the param lists
%% 2004-11-04 : Improvement of the management of the display of the Point name
%% 2004-12-10 : New parameters for coding the circum circle : SegmentSymbolA B & C
%% 2004-12-14 : New parametre RightAngleType for regional difference
%% 2005-01-17 : transition towards pst-xkey (thanks to "Hendri Adriaens" <Hendri@uvt.nl>)
%% 2005-02-21 : correction for spurious blank (thanks to Herbert Voss <Herbert.Voss@alumni.TU-Berlin.DE>)
%%              in pstTriangleABC (search for "-- hv")
%% 2005-03-25 : Modification of the transformations macros: management of a points list
%%              coding for rotation & translation
%%              draw a curve for a points list (geonode & oijgeonode & transform macros)
%% 2005-04-10 : Modification of the transformations macros: management of a points list
%%              management directly within the first point argument
%%              Plotting of a curve linking a list of points
%% 2005-10-09 : problem solved with CodeFigAB
%% 2005-12-31 : use \psscalbox instead of \scalebox (hv)
%% 2006-01-29 : minor changes for file version (hv)
%% 2006-01-30 : correction of pstArcOAB for pscustom (dr)


pst-eucl.sty --------
2018/09/28 - added pst-calculate (hv)
2006/01/28 - write some infos into the file list (hv)
