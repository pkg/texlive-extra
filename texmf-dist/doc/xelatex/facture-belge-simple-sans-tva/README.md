# Simple Belgian invoice without VAT, expenses reports.
*V. 2.1, 2018/10/27*

The facture-belge-simple-sans-tva extension is used to generate invoices for Belgian individuals who do not have a VAT number. and who wishes 

- do an occasional work;
- or to carry out paid additional activities during their free time up to 6,000 euros per calendar year (amount indexed annually) without having to pay tax or social security contributions (see the website [Activités complémentaires](https://www.activitescomplementaires.be/fr/index.html)). 

The extension also generates expense reports.

## Crédits

- Author    : Robert Sebille
- Maintainer: Robert Sebille
- Licence   : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
- % Depends of calctab for the invoices
- % Depends of fancyhdr, ifthen, eurosym, hyperref, multirow, color,  colortbl

#Facture belge simple sans TVA et note de frais
*V. 2.1, 2018/10/27*

Cette extension XeLaTeX permet de générer des factures pour des particuliers belges, ne possédant pas de numéro de TVA et qui souhaitent
 
- réaliser des travaux occasionnels;
- ou réaliser des activités complémentaires rémunérées pendant leur temps libre, jusqu'à 6.000 euros par année civile (montant indexé annuellement) sans devoir s'acquitter de cotisations fiscales ou sociales (voir le site [Activités complémentaires](https://www.activitescomplementaires.be/fr/index.html)). 

L'extension permet également de générer des notes de frais. 

##Déni de toute responsabilité: 
personnellement, je me sers de ces factures et je n'ai rencontré aucun problème à ce jour; mais je ne suis ni comptable, ni fiscaliste, et je ne peux garantir que tout soit officiellement en ordre, je partage, c'est tout. 

C'est votre responsabilité de vérifier. Si vous avez des remarques ou des suggestions pertinentes à ce propos, vous pouvez me les faire parvenir à partir du [bug tracker de la forge](https://gitlab.adullact.net/zenjo/factureBelgeSimpleSansTva/issues).

## Crédits

- Author    : Robert Sebille
- Maintainer: Robert Sebille
- Licence   : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
- % Depend de calctab pour les factures
- % Depend de fancyhdr, ifthen, eurosym, hyperref, multirow, color,  colortbl

## Files

- facture-belge-simple-sans-tva.sty % extension
- communs-facture-note-about.tex % alimente facture-belge-simple-sans-tva.sty
- note-de-frais.tex % alimente facture-belge-simple-sans-tva.sty
- communs-facture-note.tex % alimente facture-belge-simple-sans-tva.sty
- facture-sans-tva.tex % alimente facture-belge-simple-sans-tva.sty
- article-facture.tex % exemple à compiler pour la facture
- article-facture.pdf % visualisation d'un exemple pour la facture
- article-note-de-frais.tex % exemple à compiler pour la note de frais
- article-note-de-frais.pdf % visualisation d'un exemple pour la la note de frais
- facture-belge-simple-sans-tva-doc.tex % documentation à compiler
- facture-belge-simple-sans-tva-doc.pdf % visualisation de la documentation
- README.md
- LICENCE
- line_bas.png
- line_haut.png
- signature.png

## Website
- [home](https://gitlab.adullact.net/zenjo/facture-belge-simple-sans-tva/wikis/home) 
- [Annonces](https://gitlab.adullact.net/zenjo/facture-belge-simple-sans-tva/wikis/Annonces)
- [Change log](https://gitlab.adullact.net/zenjo/facture-belge-simple-sans-tva/wikis/Change-log)


