Installing StarTeX
==================

To install StarTeX on your system, you must do the following:

1) Extract the various source files from the distribution file:

	tex startex.ins

(This command reads the file startex.dtx and extracts startex.tex,
startex.lan, article.stx, a4-article.stx, ifi-article.stx and
ifi-artikkel.stx. It assumes you have the file docstrip.tex available;
this file is part of the standard LaTeX distribution.)

2) Modify startex.lan to load the hyphenation patterns for the
languages you intend to use. (The standard patterns are American
English and Norwegian which may not be your ideal choice.) American
English should always be present as language no 0. You add language
patterns by adding lines like

	\DefLang{1}{norsk}{nohyph2.tex}%

The three parameters are: (1) a unique number in the range 1-255, 
(2) the name of the language, and (3) the file from which the
hyphenation patterns can be read.

3) Make a StarTeX format:

	initex plain '\input startex' '<make-new-format>'
	mv plain.fmt startex.fmt

This produces the format file plain.fmt which is then renamed to
startex.fmt. (The commands given here are for Unix; your system might
use a different notation.)

4) Place the format file where `virtex' can find it, for instance in
/usr/local/texmf/ini.

	cp startex.fmt /usr/local/texmf/ini

5) Define the command `startex' as a link to `virtex':

	cd /usr/local/texmf/bin
	ln -s virtex startex

(Again: this is Unix notation; your system might differ.)

6) Put the .stx files where `virtex' can find them, for instance in 
/usr/local/texmf/tex/startex:
	
	cp *.stx /usr/local/texmf/tex/startex

And that should be it.


Documentation
-------------

If you want a commented listing of the StarTeX source code, you can
make that from the startex.dtx source file. That requires three passes
by LaTeX and use of `bibtex' and `makeindex':

	latex startex.dtx
	bibtex startex
	latex startex.dtx
	makeindex -s stxind.ist startex.idx
	makeindex -s stxglo.ist -o startex.gls startex.glo
	latex startex.dtx


Note
----

Some operating systems have a limit of 8+3 characters in file names.
Users of such systems may have to make some small modifications.
