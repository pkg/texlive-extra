StarTeX
-------

This is the StarTeX package, a TeX format designed to help students
write short reports and essays. It provides the user with a suitable
set of commands for such a task. It is also more robust than plain TeX
and LaTeX.

For more information, see the documentation files mentioned below.


Files
-----

The following files are included in the package:

README		This file

INSTALL		How to install StarTeX

startex.bib	The bibliography necessary for typesetting startex.dtx

startex.dtx	The StarTeX source file

startex.ins	The StarTeX installation file (see INSTALL)

startex.pdf	The typeset source code of StarTeX

stxglo.ist	Glossary definition used by `makeindex' (see INSTALL)
stxind.ist	Index definition used by `makeindex' (see INSTALL)

guide.pdf	A beginner's guide to StarTeX

ideas.pdf	StarTeX -- a TeX for beginners
		The talk at TUG'96 describing the ideas behind StarTeX


Copyright
---------

These files are in the public domain. This means that you can do what
you like with them, but I ask that if you redistribute modified files,
you clearly indicate the alterations made.

These files are provided in the hope that they will be useful, but no
guarantee is made. I will, however, strive to correct any errors
reported. 


Oslo, 11th March 1999
Dag Langmyhr
Dept of Informatics
University of Oslo
dag@ifi.uio.no
