% file: TXSinstl.doc - Installation Instructions - TeXsis version 2.18
%% @(#) $Id: TXSinstl.doc,v 18.2 2000/05/17 00:19:46 myers Exp $
%======================================================================*
% Copyright (c) 1990, 1992, 1995, 1997 by Eric Myers and Frank Paige.
% This file is a part of TeXsis.  Distribution and/or modifications
% are allowed under the terms of the LaTeX Project Public License (LPPL).
% See the file COPYING or ftp://ftp.texsis.org/texsis/LPPL
%======================================================================*
\ifx\undefined\meta \texsis\input TXSdocM.doc\input Manual.aux\draft\fi


\appendix{A}{Customization and Installation of \TeXsis \label{install}}

\centerline{(\TeX sis version \fmtversion, as of \revdate)}

\noindent
This appendix contains information about how to install \TeXsis\ on
several types of computer systems, and how to customize \TeXsis\ for a
particular installation.  The discussion below on ``Local
Modifications'' may be of interest to any user, but the rest of this
Appendix is probably only of interest to someone who is responsible for
installing or maintaining \TeXsis.


\subsection{Customization}

      There are three different mechanisms for making modifications to
the basic \TeXsis\ macro package. It is better to use one of these
mechanisms rather than modifying the original code.

\subsubsection{Local Modifications}

      \TeXsis\ will read the file {\tt TXSmods.tex}, if it exists, from
the current directory or search path every time you run \TeXsis.  This
lets you make your own \idx{custom modifications} or additions to
\TeXsis. For example, say you have a directory specifically for a
particular project you are working on and you are writing several
different papers or other documents related to this project.  You could
add a file called {\tt TXSmods.tex} to this directory to contain extra
macro definitions that are used in all of these documents, and these
will be loaded automatically whenever you run \TeXsis\ in this
directory.  If you run \TeXsis\ from another directory these macros will
not be loaded.

\subsubsection{Site Modifications}

      If a file called {\tt TXSsite.tex} exits in the current directory
at the time the \TeXsis\ format file is created (see below) then it is
automatically read in at this time.  The purpose of this file is to
allow you to add definitions and macros that are local to your computer
installation.  For example, in this file you would define the control
word \cs{ORGANIZATION} to give the name of your organization or
institution.  \cs{ORGANIZATION} is used at the top of memos and at the
top of the title page of a \cs{preprint}.  You might also put a
customized \cs{letterhead} macro in this file, and you may well want to
change the definition of \CS{LandscapeSpecial}, which controls how your
laser printer is put into landscape mode.  Note that the file {\tt
TXSsite.tex} is only read {\sl once}, when the \TeXsis\ format file is
created, not every time \TeXsis\ is run.
A prototype site file, called |TXSsite.000|, is included in the standard
distribution of \TeXsis.


\subsubsection{Patches}

      \TeXsis\ will also automatically read in a file called
{\tt TXSpatch.tex} from the standard input path, if it exists, 
every time \TeXsis\ is run.
This file should contain additions or corrections (``patches'') to
\TeXsis.  This patch file lets you correct minor problems or make minor
changes to \TeXsis\ without having to rebuild the format file.

Whenever we find a problem with \TeXsis\ we try to correct it as soon
as possible.
To make the correction available to users before the next version of
\TeXsis\ is released we issue patch files containing the corrections.
The most current patch file should always be available at
\URL{ftp://ftp.texsis.org/texsis}, or in {\tt
macros/texsis} on any CTAN server.



%==================================================*

\subsection{Installation of \TeXsis}

To use \TeXsis\ or any large macro package effectively you should run
the |initex| program to create a pre-compiled format. The general method
of doing this is machine independent, but the specific commands depend
on the operating system on your computer.  This appendix gives the
appropriate commands for running |initex| and installing \TeXsis\ on a
computer running UNIX, a VAX or Alpha running VMS, and for an IBM PC running
PC\TeX\ under \idx{MS-DOS}. We assume that you are familiar with the
operating system and that \TeX\ and the Plain format have already been
installed.
Notes on how to install \TeXsis\ on other kinds of machines (such as
Atari or Apple computers, or on Linux) are available on the \TeXsis\
ftp server   (\URL{ftp://ftp.texsis.org/texsis}). 

VMS users should note that \TeXsis\ is also available with the latest
release of DECUS \TeX, and much of the installation work has already
been done or can be done almost automatically.  You may want to get this
version from CTAN rather than the ``raw'' distribution from the TeXsis
ftp site.
If you are installing from the DECUS distribution you should read the
file |INSTALL.VMS| with that distribution.


\subsubsection{Quick Install for Unix and \TeX~3.x}

The current version for \TeX\ as of 1997 is 3.14159.  If you have
anything earlier than version 3.x then it is strongly recommended that
you first upgrade your \TeX\ installation.  Assuming that you have a
current \TeX\ installation on a Unix computer, you can build and
install \TeXsis\ by simply doing the following:


\EnvRightskip=\parindent
\enumerate\enumpoints
\itm\llap{\square\qquad}%
Edit the file |Makefile| and change parameters and paths as
appropriate, following the directions given in the comments.
The TEXDIR variable should point to the top-level \TeX\ directory
on your system.  The default directories under TEXDIR are those
recommended by the TeX Directory Structure (TDS) working group of the
\TeX\ Users Group.

\itm\llap{\square\qquad}%
(Optional) Copy the file |TXSsite.000| to |TXSsite.tex| and edit
this file to contain appropriate site dependent information, such as
definitions for the name of your |\ORGANIZATION|, or your
\cs{letterhead}, or change \cs{LandscapeSpecial}.
If you already have a |TXSsite.tex| file then copy it to the current
directory so that it will be included when building the format file.

\itm\llap{\square\qquad}%
Give the command `|make|' to  build the format file and the manual.

\itm\llap{\square\qquad}%
Give the command `|make install|' to install the files on the system.
\endenumerate
 

\pagecheck{0.20\vsize}
\subsubsection{Detailed Installation}

To install \TeXsis\ follow the steps below or their equivalents for
your computer and operating system.  If you are installing a system-wide
version of \TeXsis\ then you may want to review the appropriate
documentation on  how \TeX\ itself is installed on your system.
\enumpoints
\enumerate
\itm\llap{\square\qquad}%
Create a separate directory to hold the \TeXsis\ files.  This is not
strictly necessary, but it is strongly recommended, since there are
more than 70~files for the source code and 
documentation.  On a Unix machine we recommend using
|/usr/local/share/texmf/tex/texsis| or |/usr/local/lib/tex/texsis|, depending on
where your other \TeX\ files are kept.  For a VMS system,
you should use a subdirectory of the main \TeX\ directory, whatever that
happens to be.  On an IBM PC
it is best to use a top-level directory |C:\TEXSIS| on your hard disk,
rather than a subdirectory of |C:\PCTEX|, to simplify backing up your
files.

\itm\llap{\square\qquad}%
Copy all of the \TeXsis\ files into this directory.  The \TeXsis\ source
files all have names of the form |TXS|$xxx$|.tex|, and 
most of the documentation
files have names |TXS|$xxx$|.doc|.  \TeXsis\ style files end with
the extension |.txs|.  There will also be several other files in your
distribution of \TeXsis, such as |texsis.tex| and |Manual.tex|. 
A complete list of the files can be found in a file called |MANIFEST|.


\itm\llap{\square\qquad}%
Make any necessary changes in the source code. You will probably want to
copy the file |TXSsite.000| to |TXSsite.tex| and edit this file, 
changing the definition of \cs{ORGANIZATION}, \cs{LandscapeSpecial},
and the letterhead macros. 

On VMS systems you may (or may not) want to define |\TeXsisLib| to be
the \TeXsis\ directory and |\TXSpatches| to be the patch file in that
directory.

If you have problems with missing fonts, look at the documentation
in the file |TXSfonts.tex|.


\itm\llap{\square\qquad}%
Create the format file |texsis.fmt| by running |initex|. \index{initex}
On a Unix machine the command is:
\example
|% initex \&plain texsis '\dump'|
\endexample
You must remember the ``|\|'' to escape the ``|&|.''

On a VMS system replace the |initex| command with |TEX/INIT|. You must
also tell \TeX\ where to find the input files for both \TeXsis\ and
Plain \TeX.  The commands are:
\example\obeylines\advance\rightskip by -1cm
|$ DEFINE/USER  TEXIN  | \meta{texsis}  \meta{plain}
|$ TEX/INIT/NOFORMAT/TEXINPUTS=TEXIN: "&plain texsis \dump"|
\endexample
where \meta{texsis} is the specification for the device, directory, and
subdirectories of \TeXsis, and \meta{plain} is the specifiation for
the standard files like |PLAIN.TEX|.

For an IBM PC with PC\TeX\ give the command,
\example
|TEX TEXSIS /I|
\endexample
and reply to the |*| prompt with ``\cs{dump}.''  (In some versions of
PC\TeX\ the |TEX| command is replaced with |TEX386| or |TEX386b|.  Check
your manual to be sure.)  \TeXsis\ will also work with |emtex| or any
other version of ``big'' \TeX\ for the PC, but you will have to check
the documentation for that particular implementation of \TeX\ for
specific instructions for creating a preloaded format.

In any case |initex| will load the Plain \TeX\ format and then the
various pieces of \TeXsis, displaying a message for each piece. 
Finally it will spew out a list of fonts used and other information.
This should contain near the end the statement ``No pages of output
produced.'' (If it does not, something may be wrong with your distribution,
although it is probably a minor problem.  Check your editing of the
|TXSsite.tex| file.)  The preloaded format will be
written to the file |texsis.fmt|.

\itm\llap{\square\qquad}%
Copy or move various files to where they need to be:
\enumerate
\itm
Copy the format file to the place where your version of \TeX\ looks for
pre-loaded formats.  On Unix systems this is usually
\example
|/usr/local/share/texmf/tex/formats|.  
\endexample
or some variation of this.
For VMS machines make sure the file is in the directory named by
|$TEX_FORMATS|.
With PC-\TeX\ the file goes into |\PCTEX\TEXFMTS|.

\itm
Copy the \TeXsis\ style files (which end with the extension |.txs|) to
the place your version of \TeX\ usually looks for input.  Under Unix
this should be
\example\obeylines
|/usr/local/lib/tex/inputs|
(or sometimes |/usr/local/lib/tex/macros|).
\endexample
For VMS machines make sure these files are in the directory included
in the path defined by the |TEX_INPUTS| logical. For PC-\TeX\ the
proper place is |\PCTEX\TEXINPUT|.

\itm
If you have any run-time patches to add to \TeXsis\ copy the 
file |TXSpatch.tex| to the same \TeX\ inputs directory.
If there is a |TXSpatch.tex| from a previous release of \TeXsis\ you
should be sure to remove the old file if it is not being replaced.

\endenumerate

\itm\llap{\square\qquad}%
Having created the format file, you need to make a command |texsis| to
run \TeX\ using it. The way to do this is very dependent on the
operating system.

\enumerate

\itm
On a Unix system running \TeX~3.0 or higher you need only to make a link
to the executable |virtex|\index{virtex} with the same name as the
format you want to load.  |Virtex| probably lives in |/usr/local/bin| or
some similar place.  The command to make the link is something like:
\example
|ln /usr/local/bin/virtex /usr/local/bin/texsis|
\endexample

For \TeX\ versions 2.9x you can build an ``undumped'' version of
\TeXsis.  You will have to see the \TeX\ distribution notes for a
description of |undump|. \index{undump} However, this is now a very old
version of \TeX\ (as of this writing \TeX\ is up to version~3.1415), so
you should seriously consider upgrading to a newer version of \TeX.


If you are going to create a ``private'' version of \TeXsis\ rather than
installing it for the whole system then you need to add your \TeXsis\
directory to the environment variables |TEXFORMATS| and |TEXINPUTS| and
define the command |texsis| as follows.  For the C-shell, put in your
|.cshrc| file:
\example\obeylines
|alias texsis virtex \\\&texsis|
|setenv TEXSIS_LIB $HOME/texsis/|
|setenv TEXFORMATS ".:$TEXSIS_LIB:$TEXFORMATS"|
|setenv TEXINPUTS  ".:$TEXSIS_LIB:$TEXINPUTS"|
\endexample
(This assumes that |TEXFORMATS| and |TEXINPUTS| are already properly
defined.  If not, insert the proper directories.)  

\itm
Under VMS, place in your |login.com| file the commands
\example\obeylines
|$ DEFINE TEXSIS_LIB| \meta{disk}|:[|\meta{directory}|]|
|$ TEXSIS :== $TEX$:TEX.EXE &TEXSIS_LIB:TEXSIS|
\endexample
where |TEX$| is the logical name for the main \TeX\ directory. You can
also put these commands in a \TeX\ initialization |.COM| file.



\itm
For PC\TeX\index{PCTeX@{PC\TeX}} you should find in the distribution a file
|TEXSIS.BAT|. If you don't, you can create it.  The file should contain:
\example\obeylines
|ECHO OFF|
|\PCTEX\TEX  &TEXSIS %1 /F 25000|
\endexample
(Replace the |TEX| command with |TEX386| or |TEX386b| if appropriate for
your machine.  For PC-\TeX\ versions 3.14 and greater the |/F 25000| is
no longer needed.)
Either put this file
in a directory in your command path or add the directory it is in 
to your |PATH| statement. 
If you want to be able to load a patch file then you should also
include a line such as 
\example
|\def\TXSpatches{C:/TEXSIS/TXSpatch}|
\endexample
in the |TXSsite.tex| file.  Because \TeX\ and MS-DOS both use
backslashes for their own purposes the path is specified with {\sl
forward} slashes; 
DOS understands them to be the same as a backslash.

\endenumerate
\endenumerate

\bigskip\goodbreak

      After these steps have been carried out, \TeXsis\ should be
installed and available to you, and to other users if installed
system-wide. The command
\example
|texsis| {\sl name}
\endexample
should run \TeXsis\ on the file {\sl name}|.tex| and produce the file
{\sl name}|.dvi|, which can be printed like any |.dvi| file.
A good test is to go to the \TeXsis\ source directory (or the
documentation directory, if the documentation files |TXS|$xxx$|.doc|
have been put in a separate directory) and say
\example
|texsis Manual|
\endexample
On the first try this will produce several warning messages coming from
undefined tags for forward references. These will be defined later in
the document and saved in the file |Manual.aux|; Running \TeXsis\ a
second time should produce the \TeXsis\ manual with no errors.

%>>> EOF TXSinstl.doc <<<
