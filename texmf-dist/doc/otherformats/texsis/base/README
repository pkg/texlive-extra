			 TeXsis version 2.18
			     6 April 2001


                         GENERAL INFORMATION

   TeXsis is a Plain TeX macro package for typesetting physics
documents, including papers and preprints, conference proceedings,
books, theses, referee reports, letters, and memos.  Texsis macros
provide the following features:

  * Automatic numbering of and symbolic reference to equations,
    figures, tables, and references;

  * Simpified control of type sizes, line spacing, footnotes, running 
    headlines and footlines;

  * Specialized document formats for research papers, preprints and
    "e-prints," conference proceedings, theses, books, referee
    reports, letters, and memoranda; 

  * Table(s) of contents and lists of figures and tables, as well as
    list of figure and table captions;

  * Specialized environments for lists, theorems and proofs, centered
    or non-justified text, and listing computer code; 

  * Specialized macros for easily constructing ruled tables;

  * Simplified means of constructing an index for a book or thesis;

  * Easy to use double column formatting.

TeXsis was originally designed for use by physicists, but others may
also find it useful.  It is completely compatible with Plain TeX.

   You can find out more about TeXsis from the TeXsis Home Page, at
http://www.texsis.org


			     SOURCE CODE

   Source code and documentation for TeXsis can be obtained from 
ftp://ftp.texsis.org/texsis and is also mirrored by CTAN, the
Common TeX Archive Network, in CTAN:/tex-archive/macros/texsis/ .
Finger ctan@ftp.tex.ac.uk to find the nearest CTAN server.

   Small improvements and corrections to TeXsis are distributed as
patches in the patch file TXSpatch.tex.  If there is such a file in the
texsis directory you should copy it as well.


		      INDEPENDENT MACRO PACKAGES

   Several components of TeXsis can be used independently under Plain
TeX:

   * index: index.tex contains Plain TeX macros for constructing an
     index for a document, in conjunction with the MakeIndex program.

   * tables: ruled.tex (with TXSruled.tex) contain Plain TeX macros
     for making nice ruled tables.

   * dcol: TXSdcol.tex contains Plain TeX macros for double column
     formatting.

For details on how to use these see the documentation files that
accompany them on the TeXsis ftp site (each has a separate directory).
The comments in the files are also very complete.


			     INSTALLATION

   TeX runs on many different platforms, not just Unix, and TeXsis
will run on any installation of TeX.  It is best to perform a system-
wide installation of TeXsis so that everybody may use it, but it is
also possible to install it in a private directory for personal use.
Since TeXsis is a "format" the steps required to install it are the
same as the steps required to install any TeX "format", probably using
the `initex` program.  It would be best if you would review the
documentation on how to do this for your own TeX installation.

  Very complete installation instructions for Unix, VMS, and PC-TeX
are given in an appendix to the TeXsis manual.  You can print these
without having to have TeXsis installed by running the file
Install.tex through Plain TeX.

   That said, installation on Unix computers is rather simple.  Unless 
you have an unusual installation of TeX (in which case please read the
output from Install.tex) the steps are:

  1.  Edit the Makefile and change parameters and paths as
      appropriate, following the directions given in the comments.
      The defaults are set to work with teTeX, a common distribution
      of TeX for Unix.  You will probably only have to set the paths
      to your top-level "texmf" directory and your local "bin" directory.

  2.  (Optional) Copy the file TXSsite.000 to TXSsite.tex and edit
      this file to contain appropriate site dependent information,
      such as definitions for the name of your \ORGANIZATION, or your
      \letterhead, or change \LandscapeSpecial, etc...  If you already
      have a TXSsite.tex file then copy it to the current directory so
      that it will be included when building the format file.

  3.  Give the command `make` to build the format file and the manual.

  4.  Give the command `make install' to install the files in the
      appropriate directories.


			       MTEXSIS

   To make it easier for the casual reader of electronic preprints
("e-prints") to print a TeXsis document we have collected the core
TeXsis macros into one source file (called mtexsis.tex), with all the
comments and blank lines removed.  A reader who does not have TeXsis
on her/his system can then simply get this file, add

	\input mtexsis

to the manuscript file, and print the paper with Plain TeX.  You can
make your e-print manuscript files automatically load mtexsis.tex, if
it is needed, by adding the following line at the begining of your
manuscript file:

	\ifx\TeXsis\undefined\input mtexsis.tex\fi

It is suggested that you try running such a manuscript through Plain TeX
with mtexsis.tex first to make sure that it works.


			     MAILING LIST

  If you find TeXsis useful you may want to send a short e-mail
message to texsis@texsis.org to be put on the mailing list for any
notices of updates or changes.  We will only use this list to inform
you of updates or changes, and we won't give the list out to anybody
else (we hate spam too).


			       AUTHORS

 Eric Myers <myers@umich.edu>           Frank E. Paige <paige@bnl.gov>  
 Department of Physics		 and  	Physics Department              
 University of Michigan		       	Brookhaven National Laboratory  
 Ann Arbor, Michigan, 48109 USA    	Upton, New York 11973 USA       


			     --- ---- ---

@(#) $Id: README,v 18.3 2001/04/06 23:12:48 myers Exp $
