%% file: TXSdocM.doc - Macros for the Manual - TeXsis 2.18
%% @(#) $Id: TXSdocM.doc,v 18.2 2000/05/17 00:19:46 myers Exp $
%======================================================================*
% (C) Copyright 1989, 1992, 1997 by Eric Myers and Frank E. Paige
% This file is a part of TeXsis.  Distribution and/or modifications
% are allowed under the terms of the LaTeX Project Public License (LPPL).
% See the file COPYING or ftp://ftp.texsis.org/texsis/LPPL
%======================================================================*
% Document Format for the TeXsis manual:

\ATunlock                       % allow @ macros here
\twelvepoint
\singlespaced                   % single spaced
\singlelinetrue                 % for \description
\Contentsfalse                  % don't bother with table of contents (DRAFT) 

\parskip=\medskipamount         % some extra space between paragraphs
\EnvRightskip=0pt               % allow environments all the way right 
\RunningHeadstrue               % do show running headlines

\input epsf                     % epsf figures

% We begin a new \section on a new page.  This does it automatically:

\def\everysection{\vfill\eject}

\sectionminspace=0.25\vsize             % subsections too
\subsectionskip=2\baselineskip


% -- Font selections:

\def\Tbf{\sixteenpoint\bf}
\def\tbf{\sixteenpoint\bf}
\def\HeadFont{\tenpoint\sl}     % print running headlines in slanted type
\def\smalltt{\tenpoint\tt}      % for long \TeXexample's
\def\em{\sl}                    % \em for emphasis

% -- some useful symbols and such

\def\bs{\char92}                                % '\' for macro names
\def\VT{\char'174}                              % vertical bar
\def\offtt{\TeXquoteoff\tt}                     % \tt with | allowed
\def\ttdots{{\tt\phantom{0}...\phantom{0}}}% '...' with correct spacing
\def\lb{{\tt\char'173}}                         % { in \tt
\def\rb{{\tt\char'175}}                         % } in \tt
\let\rqbrack=\}                                 % save right curly bracket
\let\lqbrack=\{                                 % save left curly bracket


% \meta{stuff} was our way of doing <stuff> in text.  Knuth used
% \<stuff> in the TeXbook, so I use that too.  The only change
% I made was to force \rm                       -EAM

\def\<#1>{\leavevmode\hbox{$\langle$\rm #1\/$\rangle$}} % syntactic quantity
\def\meta#1{\leavevmode\hbox{$\langle$\rm #1\/$\rangle$}} % new \meta

% \arg is like \meta but it put's the <thing> in braces too.

\def\arg#1{\lb\<#1>\rb}

\def\Sect#1{Sect\-ion~\use{sect.#1}}            % section number
\obsolete\SEC\Sect                              % old synonym

% \clump will ``clump'' closer together the material in a \definition
% or \itemize

\def\clump{%                            % "clump" \definitions and \itemize
   \parskip=\baselineskip               % \parskip is what \baselineskip was
   \advance \parskip by 0pt plus 2pt    % add some stretch
   \singlespaced                        % singlespaced
   \singlelinetrue}                     % long guys on own line

% -- the ``competition''

\def\TeXbook{{\sl The \TeX book}\index{TeXbook@{\TeXbook}}} % The TeXbook
\def\LaTeX{% the LaTeX symbol
    {\rm L\kern -.36em\raise .3ex\hbox {A}\kern -.15em\TeX}}
\def\AmSTeX{% the AMSTeX symbol
   $\cal A\kern-.1667em\lower.5ex\hbox{$\cal M$}\kern-.075em S$-\TeX}

\def\BibTeX{{\rm B\kern-.05em\hbox{$\scriptstyle\rm IB$}\kern-.08em\TeX}}

% modify \emsg to recognize thes

\def\emsg#1{\relax% write an error/information message to the screen
   \begingroup                                  % disable special characters
     \def\@quote{"}%                            % to output "
     \def\TeX{TeX}\def\label##1{}\def\use{\string\use}%
     \def\BibTeX{BibTeX}\def\LaTeX{LaTeX}%
     \def\ { }\def~{ }% spaces are spaces
     \def\tt{}\def\bf{}\def\Tbf{}\def\tbf{}%
     \def\break{}\def\n{}\def\singlespaced{}\def\doublespaced{}%
     \immediate\write16{#1}% 
   \endgroup}


% -- some goodies from manmac.tex

\def\bull{\vrule height .9ex width .8ex depth -.1ex } % square bullet
\def\SS{{\it SS}}                       % scriptscript style
\def\|{\leavevmode\hbox{\tt\char`\|}}   % vertical line
\def\dn{\leavevmode\hbox{\tt\char'14}}  % downward arrow
\def\up{\leavevmode\hbox{\tt\char'13}}  % upward arrow

\def\pt{\,{\rm pt}}                     % units of points, in math formulas

\def\oct#1{\hbox{\rm\'{}\kern-.2em\it#1\/\kern.05em}} % octal constant
\def\hex#1{\hbox{\rm\H{}\tt#1}}         % hexadecimal constant
\def\cstok#1{\leavevmode\thinspace\hbox{\vrule\vtop{\vbox{\hrule\kern1pt
        \hbox{\vphantom{\tt/}\thinspace{\tt#1}\thinspace}}
      \kern1pt\hrule}\vrule}\thinspace} % control sequence token

% -- The index is constructed with index.tex and MakeIndex

\input index
\ATunlock                       % make sure @ is still a letter after index.tex
%%\markindextrue                  % proof marks for index entries  (DRAFT ONLY)

% Use \cs{foo} to get \foo in \tt type and make an entry in the
% index.  Use \CS{foo} to get \foo in \tt type and make an BOLD
% (defining) entry in the index.

\TeXquoteoff                            % be sure | in \index is not active

\def\cs#1{%
   {\tt\bs #1}\index{#1@{\tt\bs #1}|pg}%   % put it in text and index it.
   \ifmarkindex                         % mark index entries in right margin?
     \llap{\lower\jot\vbox to 0pt{\vss  % box it
        \tightboxit{\loosebox{\phantom{\tt\bs #1}}}}\hskip-\jot}%
   \fi}

\def\CS#1{%
   {\tt\bs #1}\index{#1@{\tt\bs #1}|bold}%   % put it in text and index it.
   \ifmarkindex                         % mark index entries in right margin?
     \llap{\lower\jot\vbox to 0pt{\vss  % box it
        \tightboxit{\loosebox{\phantom{\tt\bs #1}}}}\hskip-\jot}%
   \fi}

\TeXquoteon

%>>> EOF TXSdocM.doc <<<
