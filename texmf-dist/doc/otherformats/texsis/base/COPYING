		Texsis Copyright, Warranty (lack thereof),
		      and Distribution Restrictions


License and Distribution
========================

TeXsis is free software which can be redistributed and/or modified
under the terms of the LaTeX Project Public License (LPPL), as
reproduced below.   Any reference to "The Program" in the license
refers to TeXsis and its associated files.   A list of the files
covered by the LPPL is provided in the file called MANIFEST.

  Note that TeXsis is based on Plain TeX, not LaTeX, and so the rules
for naming files with the extensions .ins, .fd, and .cfg do not apply.
The only special extension used by TeXsis is .txs for style files, and
these should be treated the same as the other TeXsis source files
unless a statement in that file says otherwise.


COPYRIGHT
=========

Unless specified otherwise in individual files, TeXsis is Copyright
1987 1988 1989 1990 1993 1994 1995 1996 1997 1998 1999 2000, 2001 by 
Eric Myers and Frank E. Paige.   Other copyrights exist and are noted
in the individual files that are affected.


HISTORY
=======

Prior to TeXsis version 2.18 the source code to TeXsis was covered by a
simple copyright declaration and a statement that permission was granted
to use the software by any member the academic community in conjunction
with basic research, but that the software could not be used or
distributed for profit without permission.  This was sufficient in the
1980's when TeXsis was being developed specifically for physics
researchers, but by the end of the 1990's the issues surrounding software
licensing had become more developed and we decided that we needed a more
explicit licensing statement and something a bit more open than just
"academic use only".

  We originally considered the GNU Public License (GPL) and the
"Lesser" or "Library" version of that license (the LGPL), as well as
the BSD License, the "Artistic" License, and in fact we looked at most
of the licences approved by the Open Source Initiative (see
http://www.opensource.org/licenses/ ).  We also considered writing our
own custom licensing statement, but we eventually decided against
this.  We felt that it was important to choose a well-known license so
that users would know the basic terms of the license without having to
read all the details.  In the end we found that the LaTeX Project
Public Licence (LPPL) suited the needs and the spirit of TeXsis most
closely, and so versions 2.18 of TeXsis and beyond are covered by the
LPPL, which is reproduced below or can be obtained from the TeXsis
website.

  Please note that the LPPL only applies to the TeXsis source code and
code derived from it, but it does not apply to TeXsis style files (or
other files) which were written independently without using TeXsis
source code.  We hope that the authors of style files will choose to
follow our lead and license their works under the LPPL, but they are
free to choose whatever license they deem appropriate.  Note also that
under the terms of the LPPL it is possible to add further restrictions
to a file, as long as they are consistent with the "Conditions on
individual files" section of the LPPL (see below).

  TeXsis has always been an "Open Source" project, and so we chose the
LPPL as the best way to make TeXsis freely available to everyone but
protect it from the possible corruption of propagating different
versions of similar files.

----------------------------------------------------------------------



LaTeX Project Public License
============================
 
LPPL Version 1.0  1999-03-01

Copyright 1999 LaTeX3 Project
    Everyone is permitted to copy and distribute verbatim copies
    of this license document, but modification is not allowed.


Preamble
========

The LaTeX Project Public License (LPPL) is the license under which the
base LaTeX distribution is distributed. As described below you may use
this licence for any software that you wish to distribute. 

It may be particularly suitable if your software is TeX related (such
as a LaTeX package file) but it may be used for any software, even if
it is unrelated to TeX.

To use this license, the files of your distribution should have an
explicit copyright notice giving your name and the year, together
with a reference to this license.

A typical example would be

   %% pig.sty
   %% Copyright 2001 M. Y. Name

   % This program can redistributed and/or modified under the terms
   % of the LaTeX Project Public License Distributed from CTAN
   % archives in directory macros/latex/base/lppl.txt; either
   % version 1 of the License, or (at your option) any later version.

Given such a notice in the file, the conditions of this document would
apply, with:

`The Program' referring to the software `pig.sty'  and 
`The Copyright Holder' referring to the person `M. Y. Name'.

To see a real example, see the file legal.txt which carries the
copyright notice for the base latex distribution.

This license gives terms under which files of The Program may be
distributed and modified. Individual files may have specific further
constraints on modification, but no file should have restrictions on
distribution other than those specified below. 
This is to ensure that a distributor wishing to distribute a complete
unmodified copy of The Program need only check the conditions in this
file, and does not need to check every file in The Program for extra
restrictions. If you do need to modify the distribution terms of some
files, do not refer to this license, instead distribute The Program
under a different license. You may use the parts of the text of LPPL as
a model for your own license, but your license should not directly refer
to the LPPL or otherwise give the impression that The Program is
distributed under the LPPL. 



The LaTeX Project Public License
================================
Terms And Conditions For Copying, Distribution And Modification
===============================================================


WARRANTY
========

There is no warranty for The Program, to the extent permitted by
applicable law.  Except when otherwise stated in writing, The
Copyright Holder provides The Program `as is' without warranty of any
kind, either expressed or implied, including, but not limited to, the
implied warranties of merchantability and fitness for a particular
purpose.  The entire risk as to the quality and performance of the
program is with you.  Should The Program prove defective, you assume
the cost of all necessary servicing, repair or correction.

In no event unless required by applicable law or agreed to in writing
will The Copyright Holder, or any of the individual authors named in
the source for The Program, be liable to you for damages, including
any general, special, incidental or consequential damages arising out
of any use of The Program or out of inability to use The Program
(including but not limited to loss of data or data being rendered
inaccurate or losses sustained by you or by third parties as a result
of a failure of The Program to operate with any other programs), even
if such holder or other party has been advised of the possibility of
such damages.


DISTRIBUTION
============

Redistribution of unchanged files is allowed provided that all files
that make up the distribution of The Program are distributed.
In particular this means that The Program has to be distributed
including its documentation if documentation was part of the original
distribution.

The distribution of The Program will contain a prominent file
listing all the files covered by this license.

If you receive only some of these files from someone, complain!

The distribution of changed versions of certain files included in the
The Program, and the reuse of code from The Program, are allowed
under the following restrictions:

 * It is allowed only if the legal notice in the file does not
   expressly forbid it.
   See note below, under "Conditions on individual files".
 
 * You rename the file before you make any changes to it, unless the
   file explicitly says that renaming is not required.  Any such changed
   files must be distributed under a license that forbids distribution
   of those files, and any files derived from them, under the names used
   by the original files in the distribution of The Program.

 * You change any `identification string' in The Program to clearly 
   indicate that the file is not part of the standard system.

 * If The Program includes an `error report address' so that errors
   may be reported to The Copyright Holder, or other specified
   addresses, this address must be changed in any modified versions of
   The Program, so that reports for files not maintained by the
   original program maintainers are directed to the maintainers of the
   changed files. 

 * You acknowledge the source and authorship of the original version
   in the modified file.

 * You also distribute the unmodified version of the file or
   alternatively provide sufficient information so that the
   user of your modified file can be reasonably expected to be
   able to obtain an original, unmodified copy of The Program.
   For example, you may specify a URL to a site that you expect
   will freely provide the user with a copy of The Program (either
   the version on which your modification is based, or perhaps a
   later version).

 * If The Program is intended to be used with, or is based on, LaTeX,
   then files with the following file extensions which have special
   meaning in LaTeX Software, have special modification rules under the
   license:
 
    - Files with extension `.ins' (installation files): these files may
      not be modified at all because they contain the legal notices
      that are placed in the generated files.
 
    - Files with extension `.fd' (LaTeX font definitions files): these
      files are allowed to be modified without changing the name, but
      only to enable use of all available fonts and to prevent attempts
      to access unavailable fonts. However, modified files are not
      allowed to be distributed in place of original files.
 
    - Files with extension `.cfg' (configuration files): these files
      can be created or modified to enable easy configuration of the
      system.  The documentation in cfgguide.tex in the base LaTeX
      distribution describes when it makes sense to modify or generate
      such files.
 

The above restrictions are not intended to prohibit, and hence do
not apply to, the updating, by any method, of a file so that it
becomes identical to the latest version of that file in The Program.

========================================================================

NOTES
=====

We believe that these requirements give you the freedom you to make
modifications that conform with whatever technical specifications you
wish, whilst maintaining the availability, integrity and reliability of
The Program.  If you do not see how to achieve your goal whilst
adhering to these requirements then read the document cfgguide.tex
in the base LaTeX distribution for suggestions. 

Because of the portability and exchangeability aspects of systems
like LaTeX, The LaTeX3 Project deprecates the distribution of
non-standard versions of components of LaTeX or of generally available
contributed code for them but such distributions are permitted under the
above restrictions.

The document modguide.tex in the base LaTeX distribution details
the reasons for the legal requirements detailed above.
Even if The Program is unrelated to LaTeX, the argument in
modguide.tex may still apply, and should be read before
a modified version of The Program is distributed.


Conditions on individual files
==============================

The individual files may bear additional conditions which supersede
the general conditions on distribution and modification contained in
this file. If there are any such files, the distribution of The
Program will contain a prominent file that lists all the exceptional
files.

Typical examples of files with more restrictive modification
conditions would be files that contain the text of copyright notices.

 * The conditions on individual files differ only in the
   extent of *modification* that is allowed.

 * The conditions on *distribution* are the same for all the files.
   Thus a (re)distributor of a complete, unchanged copy of The Program
   need meet only the conditions in this file; it is not necessary to
   check the header of every file in the distribution to check that a
   distribution meets these requirements.
