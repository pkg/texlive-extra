<?xml version="1.0"?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Urlbst</title>
<link href="https://nxg.me.uk/" rev="author"/>
<link type="text/css" rel="stylesheet" href="/style/base.css"/>
</head>

<body>
<h1>Urlbst</h1>
<div class="abstract">
<p>A script to add a <code>webpage</code> BibTeX entry type, and add
support for general <code>url</code> and <code>lastchecked</code>
fields, to (most) BibTeX <code>.bst</code> files.  Optionally adds
basic support for <code>eprint</code>, <code>doi</code> and <code>pubmed</code> fields, and
hypertex/hyperref support.</p>

<p>The Persistent URL for this page is
<code>https://purl.org/nxg/dist/urlbst</code>
– <strong>please quote this rather than the URL it redirects to</strong>.
It is on CTAN at
<a href='https://ctan.org/pkg/urlbst'><code>https://ctan.org/pkg/urlbst</code></a>.
The code (and bugparade) is
<a href='https://heptapod.host/nxg/urlbst'>online</a>.</p>

<p>Version 0.9, 2022 December 1.</p>
</div>

<p>Contents</p>
<ul>
<li><a href='#usage'>Usage</a></li>
<li><a href='#example'>For example...</a></li>
<li><a href='#download'>Download and installation</a></li>
<li><a href='#licences'>Licences</a></li>
<li><a href='#information'>Further Information</a></li>
<li><a href='#history'>Version history, and release notes</a></li>
</ul>

<p>The standard BibTeX styles have no explicit support for the web, in
that they include no `webpage' type, and have no support for adding
URLs to references.  There are rumours that such support might appear
in BibTeX 1.0, but there is no estimate of when that is likely to arrive.</p>

<p>The Perl script <code>urlbst</code> can be used to add this support to an
arbitrary <code>.bst</code> file which has a reasonably `conventional'
structure.  The result is meant to be robust rather than pretty.</p>

<p>It has a different goal from Patrick Daly's `custom-bib' package --
that is intended to create a BibTeX style .bst file from scratch, and
supports `url' and `eprint' fields.  This package, on the other hand,
is intended for the case where you already have a style file that
works (or at least, which you cannot or will not change), and edits it
to add the new webpage entry type, plus the url, lastchecked and
eprint fields.  Fuller details are in the <a href="urlbst.pdf"
>manual</a>.</p>

<h2><a name='usage'>Usage</a></h2>
<pre>
urlbst [--literal key=value] [--setting key=value]
           [input-file [output-file]]
</pre>
<p>If either the input-file or the output-file is omitted, they
default to stdin and stdout respectively.  If the input file already
has a URL entry type, then the script objects.
By default (in the current version), the output
includes support for eprint, doi and pubmed.</p>

<p>The support in the generated <code>.bst</code> file can be adjusted
with the options <code>--setting</code> and <code>--literal</code>;
see <code>--setting help</code> and <code>--literal help</code> for
the available switches.  The options <code>--[no]eprint</code>,
<code>--[no]doi</code>, <code>--[no]pubmed</code>,
<code>--inlinelinks</code>, <code>--nohyperlinks</code>,
<code>--hypertex</code> and <code>--hyperref</code> are shortcuts for
some of these settings.

<p>The added fields are:</p>
<ul>
<li><code>url</code> and <code>lastchecked</code>, to associate a URL with
  a reference, along with the date at which the URL was last checked
  to exist;</li>
<li><code>doi</code>, for a reference's <a href='https://doi.org'>DOI</a>;</li>
<li><code>eprint</code>, for an <a href='http://arxiv.org'>arXiv</a> eprint reference; and</li>
<li><code>pubmed</code> for a reference's
  <a href='http://pubmed.gov'>PubMed</a> identifier (PMID).</li>
</ul>

<p>If setting <code>hrefform</code> is 1 or 2, then the generated
<code>.bst</code> file includes
support for hyperlinks in the generated eprint entries in the
bibliography, with the format being either HyperTeX (see <a
href="http://arxiv.org/hypertex/#implementation" >arXiv, again</a>),
supported by xdvi, dvips and others, or using the support available
from the hyperref package.  When
URLs are included in the bibliography, they are written out using the
<code>\url{...}</code> command, and hyperref automatically processes
that in the correct way to include a hyperlink.</p>

<p>The setting <code>inlinelinks</code> (option
<code>--inlinelinks</code>) tells urlbst not to write out the URL as
text within the bibliography entry.  Instead, urlbst will produce a
more conventional-looking and compact bibliography entry, but the
entry's title will now be a hyperlink to that URL.  This hyperlink may
be useful to anyone who reads your document online (this option can
only be used with <code>hrefform</code> 1 or 2.  Any DOI or eprint
text which would be produced as a result of the appropriate option
will still appear in this mode.</p>

<p>You may adjust various literal strings, either for the purposes of
internationalisation, or simply because you prefer different text.  To
do this, give the <code>--literal</code> option, followed by a
<code>key=value</code> pair, for example <code>--literal online="on
th'intert00bs"</code>, to replace the default <code>URL:</code> text.
The possible values, and their defaults, are below (say
<code>--literal help</code> to get this list printed out):</p>
<table>
<tr><th>literal</th><th>description</th><th>default</th></tr>
<tr><td>urlintro</td><td>prefix before URL; typically "Available from:" or "URL:"</td><td>URL: </td></tr>
<tr><td>pubmedprefix</td><td>text prefix printed before PUBMED ref</td><td>PMID:</td></tr>
<tr><td>pubmedurl</td><td>prefix to make URL from PUBMED</td><td>http://www.ncbi.nlm.nih.gov/pubmed/</td></tr>
<tr><td>eprintprefix</td><td>text prefix printed before eprint ref</td><td>arXiv:</td></tr>
<tr><td>linktextstring</td><td>anonymous link text</td><td>[link]</td></tr>
<tr><td>citedstring</td><td>label that something is cited by something else</td><td>cited </td></tr>
<tr><td>eprinturl</td><td>prefix to make URL from eprint ref</td><td>http://arxiv.org/abs/</td></tr>
<tr><td>doiurl</td><td>prefix to make URL from DOI</td><td>https://doi.org/</td></tr>
<tr><td>doiprefix</td><td>printed text to introduce DOI</td><td>doi:</td></tr>
<tr><td>onlinestring</td><td>label that a resource is online</td><td>online</td></tr>
</table>

<p>The list of settings (<code>--setting help</code>) is:</p>
<table>
<tr><th>setting</th><th>description</th><th>default</th></tr>
<tr><td>inlinelinks</td><td>0=URLs explicit; 1=URLs attached to titles</td><td>0</td></tr>
<tr><td>addpubmed</td><td>0=no PUBMED resolver; 1=include it</td><td>1</td></tr>
<tr><td>addeprints</td><td>0=no eprints; 1=include eprints</td><td>1</td></tr>
<tr><td>hrefform</td><td>0=no crossrefs; 1=hypertex xrefs; 2=hyperref refs</td><td>0</td></tr>
<tr><td>adddoi</td><td>0=no DOI resolver; 1=include it</td><td>1</td></tr>
<tr><td>doiform</td><td>0=with href; 1=with \doi{}</td><td>0</td></tr>
</table>

<p>The distribution includes preconverted versions of the four
standard BibTeX .bst style files.</p>

<p>Only the style files which result from conversion of the standard
styles are checked in the regression tests.  Other style files which
are known to work include</p>
<blockquote>
acm.bst, amsalpha.bst, amsplain.bst, apalike.bst, gerabbrv.bst,
geralpha.bst, gerapali.bst, gerplain.bst, gerunsrt.bst, ieeetr.bst,
siam.bst, mla.bst
</blockquote>

<p>Style files which are known to fail:</p>
<ul>
<li>Currently (as of at least v0.5), there exists a style <code>achicago.bst</code>
which seems to fox <code>urlbst</code>; it turns out that there's a
simpler but almost equivalent style <code>chicago.bst</code> which
works.</li>

<li>Some APA-like styles, including at least <code>apager.bst</code>,
seem to be sufficiently different from the most common styles, that
they confuse <code>urlbst</code> completely.</li>

<li>The <em>koma-script</em> styles and the <em>refer</em> styles are
not designed to produce conventional <code>.bbl</code> files, and
<code>urlbst</code> does not aim to produce anything useful from these.</li>
</ul>
<p>This might be a good time for me to revisit the rather
convoluted logic within the script, to make it a little more robust in
the face of variants like these, but don't hold your breath.  The
logic within the script is pretty hideous (it essentially works by
spotting patterns in the input style file, and replacing or extending
bits of BibTeX style-file code.  This is a fragile technique, and is
probably at the limits of its usefulness, therefore I'd take some
persuading to add significant extra functionality to the script.</p>

<p>The natbib and revtex style files already have URL fields.  If you have a
BibTeX style file which you think ought to work, but with which the
script fails, send it to me, and I'll try to work out what I've
missed (no promises, though).</p>

<p>Note that the command to invoke the script changed from `urlbst.pl'
to plain `urlbst' between versions 0.3 and 0.4.</p>

<h2><a name='example'>For example...</a></h2>
<p>To add URL support to the standard <code>siam.bst</code> file, you
can give the command</p>
<pre>
% urlbst /path/to/original/siam.bst siamurl.bst
</pre>
<p>Your TeX installation will likely have some way of helping you find where
the original <code>.bst</code> files are.  On teTeX-based systems, for
example, the command <code>kpsewhich siam.bst</code> returns the full
path of the <code>siam.bst</code> file which BibTeX would find.</p>

<p>The distributed files <code>abbrvurl.bst</code>,
<code>alphaurl.bst</code>, <code>plainurl.bst</code> and
<code>unsrturl.bst</code> are versions of the standard style files
which have been pre-converted.</p>

<p>There is more general discussion of including URLs in
bibliographies in the <a
href="https://texfaq.org/FAQ-citeURL" >TeX FAQ</a>.</p>

<h2><a name='download'>Download and installation</a></h2>

<p>Download the file as a
<a href="urlbst-0.9.tar.gz" >tarball</a>
or
<a href="urlbst-0.9.zip" >zip file</a>
and unpack it.  Or you can clone the source repository
<a href='https://heptapod.host/nxg/urlbst'>online</a>.</p>

<p>To install, you should simply copy the distributed
<code>urlbst</code> script to someplace on the path (such as
<code>/usr/local/bin</code>, or <code>~/local/bin</code>, depending on
your privileges and tastes).</p>

<p>If you (might) want to use the pre-converted standard
<code>.bst</code> files, then you'll need to copy these to somewhere
in the BibTeX search path.  Type <kbd>kpsepath bst</kbd> on Unix to
find out the list of places BibTeX searches, and pick either one of
the user-local locations, or a system-wide one.  If you're installing
in a system-wide location, you'll need admin privileges, obviously,
and you will probably need to use <code>texhash</code>,
<code>mktexlsr</code> or its equivalent, to update LaTeX's filename
cache.  For further hints here, see
the TeX FAQ entries on <a
href='https://texfaq.org/#installing'
>installing a package</a> and <a
href='https://texfaq.org/FAQ-inst-wlcf'
>where LaTeX puts files</a>.</p>

<p>That should be all you have to do.</p>

<p>The urlbst script is distributed with a path which should just work,
as long as the Perl binary is in your path, but if you have problems
here, then you might want to change the first line to something like</p>
<pre>
#! /usr/local/bin/perl
</pre>
<p>if that's where your Perl binary is.</p>

<p>You can also use the conventional <code>./configure; make; make
install</code> to configure and install the package (as root, or using
<code>sudo</code> if you're installing it in a system location),
though you still have to install the <code>.bst</code> files by hand.
This is rather heavyweight for the trivial amount of configuration
required, so it's almost always simpler just to do things by hand.</p>

<p>If you need to change some of the <code>urlbst</code> defaults,
however, or if your fingers type <code>./configure</code>
spontaneously, then you can be reassured that the configure script
supports the standard <code>--prefix</code> option, plus the following
options setting defaults:</p>
<dl>

<dt><code>--with-eprints=url</code>, <code>--without-eprints</code></dt>

<dd>This makes the <code>--eprints</code> option to urlbst available
by default, and allows you to optionally specify a prefix for creating
URLs from eprint numbers.  The default for this URL is
<code>http://arxiv.org/abs/</code> -- this is appropriate for arXiv,
obviously, but there are now a variety of other preprint archives
appearing, for which this might be inappropriate.  If you have
comments on this default, or alternatives, please let me know.  This
option is enabled by default; if you have some reason for disabling
the <code>--eprints</code> option for urlbst, the give the configure
option <code>--without-eprints</code></dd>

<dt><code>--with-doi=url</code>, <code>--without-doi</code></dt>

<dd>This makes available the <code>--doi</code> option to urlbst and,
as with the <code>--with-eprints</code> option, allows you to
customise the DOI resolver URL.  The <code>--doi</code> option to
urlbst is on by default.</dd>

<dt><code>--with-href=0/1/2</code></dt>

<dd>This allows you to specify the type of hyperlinks which are
inserted in the bibliography.  Values 0, 1 and 2, indicating no
hyperlinks, hypertex-style links, and hyperref-style links,
respectively.  The default is <code>--with-href=0</code>.  The
<code>hyperref</code>-style option is intended to complement the
increasingly standard <code>hyperref</code> package.</dd>

</dl>

<p>The first two options simply change defaults, and if you never use
the new <code>eprints</code> or <code>doi</code> fields, then the
defaults don't matter to you.  </p>

<p>Since the modified style generates URLs wrapped in the
<code>\url{}</code> macro, it is useful to use the resulting
bibliography style alongside the <code>url</code> package.  Since this
tends to work with <code>hyperref</code> anyway, where possible, you
don't get any extra goodness by changing the <code>--with-href</code>
default at configuration time.</p>

<h3>Documentation</h3>

<p>Basic documentation is in the file <code>urlbst.tex</code>.  This
is distributed as a PDF file, but if you want to regenerate it, use</p>
<pre>
% latex urlbst
% bibtex urlbst
% latex urlbst
% latex urlbst
</pre>

<h2><a name='licences'>Licences</a></h2>

<!-- this text copied from the README.md -->
<p>The copyright and licence position for the modified <code>.bst</code> files seems
slightly muddy to me.  On the grounds that any licence is better than
no licence, I therefore assert that the <em>modifications</em> which the
<code>urlbst</code> program makes to these files are copyright 2002-03, 2005-12, 2014, 2019, 2022,
Norman Gray, and that these modifications are available for
distribution under the terms of the LaTeX Project Public Licence.</p>

<p>The original <code>.bst</code> files are copyright Howard Trickey and Oren
Patashnik, with a set of permissions, in text at the top of the files,
which state that "Unlimited copying and redistribution of this file
are permitted as long as it is unmodified" (see the files for the
complete text).  The distribution terms above therefore appear to be
compatible with -- in the sense of being morally equivalent to --
these terms in the <code>.bst</code> file.  If anyone disagrees with
the logic here, I'd be very happy to discuss that.</p>

<p>The <code>urlbst</code> script itself is distributed under the GPL, version 2.0.</p>

<p>See the files <code>LICENCE-lppl.txt</code> and <code>LICENCE-gpl-2.0.txt</code> in the
distribution, for the relevant licence text.</p>


<h2><a name='information'>Further Information and acknowledgements</a></h2>

<p><code>urlbst</code> is archived <a href='https://ctan.org/pkg/urlbst' >on CTAN</a>,
and discussed in the
<a href="https://texfaq.org/FAQ-citeURL" >TeX FAQ</a>.</p>

<p>The source code for the package is maintained
<a href='https://heptapod.host/nxg/urlbst'>online</a>.
<!--
which also includes an issue tracker, where you can report bugs (or
just mail me).--></p>

<p>The <a href="https://purl.org/nxg/dist/urlbst"
>home page of urlbst</a> might possibly have more recent versions than the
one you're looking at now.</p>


<h2><a name='history'>Acknowledgements, and release notes</a></h2>

<!-- include:release-notes.html -->
<div xmlns='http://www.w3.org/1999/xhtml'>

<p>Thanks are due to many people for suggestions and requests:
to Jason Eisner for suggesting the <code>--inlinelinks</code> option;
to ‘ijvm’ for code contributions in the <code>urlbst</code> script;
to Paweł Widera for the suggestion to use <code>\path</code> when formatting DOIs;
to Michael Giffin for the suggestion to include PubMed URLs;
to Katrin Leinweber for the pull request which fixed the format of DOI
references.</p>

<dl>
<dt><strong>0.9, 2022 December 1</strong></dt>
<dd><ul>
  <li>Changed repository location to <a href='https://heptapod.host/nxg/urlbst'>heptapod</a>
  (when Bitbucket dropped support for Mercurial).
  The issue links below, pointing to Bitbucket, will therefore no
  longer work.</li>
  <li>Refactoring of the mechanism for configuring the generated
  <code>.bst</code> file, specifically adding the
  <code>--setting</code> option.</li>
  <li>Added the <code>doiform</code> setting to generate DOIs wrapped
  in <code>\doi{...}</code>.</li>
</ul>
</dd>

<dt><strong>0.8, 2019 July 1</strong></dt>
<dd><ul>
<li>The presence of a preexisting <code>format.doi</code>,
<code>format.eprint</code> or <code>format.pubmed</code> function is
now detected, and warned about.  The resulting <code>.bst</code> file
might still require some manual editing.
Resolves <a href='https://bitbucket.org/nxg/urlbst/issue/8'>issue 8</a>.</li>
<li>Clarified licences (I hope).</li>
<li>Adjust format of DOI resolver.
Resolves <a href='https://bitbucket.org/nxg/urlbst/issue/11'>issue 11</a>,
thanks to
<a href='https://bitbucket.org/nxg/urlbst/pull-requests/1/'
>code contributed by Katrin Leinweber</a>.</li>
</ul>
</dd>

<dt>0.7, 2011 July 20</dt>
<dd>Add <code>--nodoi</code>, <code>--noeprints</code> and
<code>--nopubmed</code> options (which defaulted on, and couldn't
otherwise be turned off)</dd>

<dt>0.7b1, 2011 March 17</dt>
<dd>Allow parameterisation of literal strings, with option <code>--literal</code>.</dd>

<dt>0.6-5, 2011 March 8</dt>
<dd>Adjust support for inline links (should now work for arXiv, DOI and Pubmed)</dd>

<dt>0.6-4, 2009 April 28</dt>
<dd>Work around BibTeX linebreaking bug (thanks to Andras Salamon for the bug report).</dd>

<dt>0.6-3, 2009 April 19</dt>
<dd>Fix inline link generation (thanks to Eric Chamberland for the bug report).</dd>

<dt>0.6-2, 2008 November 17</dt>
<dd>We now turn on inlinelinks when we spot format.vol.num.pages,
which means we include links for those styles which don't include a
title in the citation (common for articles in physical science styles,
such as aip.sty).</dd>

<dt>0.6-1, 2008 June 16</dt>
<dd>Fixed some broken links to the various citation standards
(I think in this context this should probably <em>not</em> be happening, yes?).
The distributed <code>*url.bst</code> no longer have the
<code>--inlinelinks</code> option turned on by default.</dd>

<dt><strong>0.6, 2007 March 26</strong></dt>
<dd><ul>
<li>Added the option <code>--inlinelinks</code>, which adds inline hyperlinks
to any bibliography entries which have URLs, but does so inline, rather
than printing the URL explicitly in the bibliography.  This is (primarily)
useful if you're preparing a version of a document which will be read
on-screen.  Thanks to Jason Eisner for the suggestion, and much testing.</li>

<li>Incorporate hyperref bugfixes from Paweł Widera.</li>

<li>Further reworkings of the inlinelinks support, so that it's now
fired by a <code>format.title</code> (or <code>format.btitle</code>) line, with a fallback in
<code>fin.entry</code>.  This should be more robust, and allows me to delete some
of the previous version's gymnastics.</li>

<li>Reworked <code>inlinelinks</code> support; should now be more
robust.  Incorporate hyperref bugfixes from Paweł Widera.</li>

<li>Added the option <code>inlinelinks</code>, which adds inline hyperlinks
to any bibliography entries which have URLs, but does so inline, rather
than printing the URL explicitly in the bibliography.  This is (only)
useful if you're preparing a version of a document which will be read
on-screen.</li>
</ul>
</dd>

<dt>0.5.2, 2006 September 6</dt>
<dd>Another set of documentation-only changes, hopefully clarifying
installation.</dd>

<dt>0.5.1, 2006 January 10</dt>
<dd>No functionality changes.  Documentation and webpage changes only,
hopefully clarifying usage and configuration</dd>

<dt><strong>0.5, 2005 June 3</strong></dt>
<dd>Added support for Digital Object Identifiers (DOI) fields in
bibliographies.</dd>

<dt>0.4-1, 2005 April 12</dt>
<dd>Documentation improvements -- there are now examples in the help text!</dd>

<dt><strong>0.4, 2004 December 1</strong></dt>
<dd>Bug fixes: now compatible with mla.bst and friends.
Now uses <code>./configure</code> (optionally).  Assorted reorganisation.</dd>

<dt><strong>0.3, 2003 June 4</strong></dt>
<dd>Added <code>--eprint</code>, <code>--hypertex</code> and <code>--hyperref</code> options.</dd>

<dt><strong>0.2, 2002 October 23</strong></dt>
<dd>The `editor' field is now supported in the webpage entry type.  Basic
documentation added.</dd>

<dt><strong>0.1, 2002 April</strong></dt>
<dd>Initial version</dd>
</dl>
</div>

<p>Copyright 2002-03, 2005-12, 2014, 2019, 2022, Norman Gray.  Released under the terms of the GNU
General Public Licence.</p>

<div class="signature">
<a href="https://nxg.me.uk"
	>Norman Gray</a><br/>
2022 December 1
</div>

</body>
</html>
