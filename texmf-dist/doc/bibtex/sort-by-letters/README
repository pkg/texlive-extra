This is an archive for bibliography style files:
abbrv-letters.bst
alpha-letters.bst
apalike-letters.bst
frplainnat-letters.bst
plain-letters.bst
plainnat-letters.bst
siam-letters.bst

(sort-by-letters v2)

(C) 2006-2012 - Thomas van Oudenhove <vanouden @ enstimac \dot fr>
Copying of this file is authorized only if either
 (1) you make absolutely no changes to your copy, including name, or
 (2) if you do make changes, you name it something other than all standard
     styles' names (for consistency reasons).

\alphabibitem is from Mael Hill\'ereau <mael \dot hillereau @ free \dot fr>;
many thanks to him.

These styles do the same stuff as the original files, but they write in the bbl
file the first letter of the first author or editor name. the references in the
bibliography are thus sorted by letter, like this :
A___
Aamport,...
K___
Knuth, ...

WARNINGS (for frplainnat-letters.bst):
- You MUST use this style with babel package (french).
- You MUST define these commands for all kinds of citations to work :
  \andname, \editorname, \editornames, \volumename, \ofname, \numbername,
  \Numbername, \inname, \pagesname, \pagename, \chaptername, \Inname,
  \technicalreportname, \Volumename, \janname, \febname, \marname, \aprname,
  \mayname, \junname, \julname, \augname, \sepname, \octname, \novname,
  \decname.
- Bibentries MUST have an 'author', 'editor' or 'organization' attribute.

AVERTISSEMENTS (pour frplainnat-letters.bst):
- Vous DEVEZ utiliser le package babel (option french) avec ce style.
- Pour que tous les types de citations fonctionnent correctement, vous DEVEZ
  d\'efinir ces commandes :
  \andname, \editorname, \editornames, \volumename, \ofname, \numbername,
  \Numbername, \inname, \pagesname, \pagename, \chaptername, \Inname,
  \technicalreportname, \Volumename, \janname, \febname, \marname, \aprname,
  \mayname, \junname, \julname, \augname, \sepname, \octname, \novname,
  \decname.
- Les entr\'ees DOIVENT avoir un 'author', un 'editor' ou une 'organization'.

Known bugs: 
  - sorting doesn't work with 'organization'
  - 'odd encoding' are not supported, especially with accents (e.g. \"U will go
    with the U, but � will not be supported... and may go with the next letter)

Workarounded or solved bugs:
  - problems with frplainnat-letters.bst: this style uses several commands to
    allow customization of dates and some labels of citation; the warnings
    above will be more explicit for the user. Example, with xspace package:
        \newcommand*{\technicalreportname}{Rapport Technique\xspace}
  - some characters were latin1 encoded, they are now typed with ascii only

Enjoy !
