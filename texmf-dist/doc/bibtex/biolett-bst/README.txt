This is the BibTeX style (.bst) file for the 
journal "Biology Letters" from the Royal Society.

Simply include it in your .tex file with command 

  \bibliography{myreferences}
  \bibliographystyle{biolett}

where 'myreferences' is actually the file 
'myreferences.bib', which should be your bibliographic 
databank in BibTeX format, of course!  

Please, be aware that this style was pro­duced in­de­pen­dently 
of the Royal So­ci­ety, and has no for­mal ap­proval 
from the Society.

If you spot any mistake in the .bst file, you'd better fixing 
it in the .dbj file, instead of trying to fix the .bst file. 
In such case, I can send you my original biolett.dbj file
to speed up your work. Just drop me an email!

Cheers

Og DeSouza <og.souza@ufv.br>
http://www.isoptera.ufv.br

PS: if you publish a paper using this bst file, please be 
kind to mention it in the acknowledgments, and send me a 
pdf of your paper for my records. It may improve the chances
to get my next research grant!

--------------- changelog
2016-10-05 first version
