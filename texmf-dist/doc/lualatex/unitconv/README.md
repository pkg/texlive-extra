# README #
Package unitconv can conver t a TeX length or a given
value with unit into another unit, Supported are all
TeX units and also m and km. The output can be in
scientic notation for large values.

This package needs lualatex!

% This file is distributed under the terms of the LaTeX Project Public
% License from CTAN archives in directory  macros/latex/base/lppl.txt.
% Either version 1.3 or, at your option, any later version.
%
% Copyright 2020 Herbert Voss hvoss@tug.org
%

%% $Id: README.md 1177 2020-05-07 15:38:43Z herbert $
