%% $Id: unitconv-doc.tex 1177 2020-05-07 15:38:43Z herbert $
\listfiles
\documentclass[english,parskip=half]{scrartcl}
\usepackage{libertinus}
\setmonofont[Scale=MatchLowercase,FakeStretch=0.9]{AnonymousPro}
\usepackage{babel}
\usepackage[autostyle]{csquotes}

\usepackage{unitconv}
\usepackage{showexpl,xltabular,nicefrac,booktabs}
\lstset{basicstyle=\ttfamily\small}

\title{Convert a length into one with another unit
with Lua\TeX}
\author{Herbert Voß}
\parindent=0pt

\begin{document}
\maketitle
\tableofcontents
\section{Using}

\begin{verbatim}
\usepackage{unitconv}
\end{verbatim}


\section{Macros}

\begin{verbatim}
\convTeXLength*[<unit>][<digits>]{<TeX length>}
\convLength*[<unit>][<digits>]{<value>}[<unit>]
\end{verbatim}

The star version prints the number in scientific notation. 
The default setting for the unit is \verb|cm| 
and for the number of digits \verb|-1| (print all digits).
The dynamic units \verb|em|, \verb|ex|, and \verb|mu| depend on the
current fontsize. 

\begin{itemize}
\item This package works only with \verb|lualatex|!
\item With \textsf{AmsMath} you have to load the package before \texttt{unitconv}.
\end{itemize}


\section{The units}

\def\Index#1{#1}%
\begin{tabular}{@{}>{\bfseries\ttfamily}c l @{}}\toprule
\rmfamily\normalfont\emph{Short}& \emph{Long}     \\\midrule
\Index{bp} & Big Point (72\,bp/in)            \\
\Index{cc} & Cîcero (1\,cc=12\,dd)            \\
\Index{cm} & Centimeter                       \\
\Index{dd} & Didôt ($1157\,\mathrm{dd}=1238\,\mathrm{pt}$)  \\
\Index{em} & Width of »M« in the current font       \\
\Index{ex} & Height of »x« in the current font                           \\
\Index{in} & Inch (72.27\,pt)                \\
\Index{km} & Kilometer                                     \\
\Index{m}  & Meter                                     \\
\Index{mm} & Millimeter                                     \\
\Index{mu} & Math unit (1\,mu=\nicefrac{1}{18}em) \\
\Index{pc} & Pica (12\,pt/pc)                            \\
\Index{pt} & (\TeX-)Points ($\nicefrac{1}{72.27}$\,Inch) \\
\Index{px} & Pixel, 1\,px=\nicefrac{1}{72}in (pdf\TeX)\\
\Index{sp} & Scaled Point (65536\,sp/pt)                   \\
\bottomrule
\end{tabular}


\section{Examples}
\subsection{Converting a \TeX\ length}

\begin{LTXexample}[width=0.5\linewidth]
The current example linewidth is 
\the\linewidth, which is 

\convTeXLength{\linewidth}\\
\convTeXLength[bp]{\linewidth}\\
\convTeXLength[cc]{\linewidth}\\
\convTeXLength[dd]{\linewidth}\\
\convTeXLength[em]{\linewidth}\\
\convTeXLength[ex]{\linewidth}\\
\convTeXLength[in]{\linewidth}\\
\convTeXLength[km]{\linewidth}\\
\convTeXLength[m]{\linewidth}\\
\convTeXLength[mm]{\linewidth}\\
\convTeXLength[mu]{\linewidth}\\
\convTeXLength[pc]{\linewidth}\\
\convTeXLength[pt]{\linewidth}\\
\convTeXLength[px]{\linewidth}\\
\convTeXLength*[sp]{\linewidth}
\end{LTXexample}

\begin{LTXexample}[width=0.5\linewidth]
The current character width of M
is 1\,em, which is

\convTeXLength{1em}\\
\convTeXLength[bp]{1em}\\
\convTeXLength[cc]{1em}\\
\convTeXLength[dd]{1em}\\
\convTeXLength[em]{1em}\\
\convTeXLength[ex]{1em}\\
\convTeXLength[in]{1em}\\
\convTeXLength[km]{1em}\\
\convTeXLength[m]{1em}\\
\convTeXLength[mm]{1em}\\
\convTeXLength[mu]{1em}\\
\convTeXLength[pc]{1em}\\
\convTeXLength[pt]{1em}\\
\convTeXLength[px]{1em}\\
\convTeXLength*[sp]{1em}
\end{LTXexample}


\begin{LTXexample}[width=0.5\linewidth]
The current example linewidth is 
\the\linewidth, which is 

\convTeXLength[bp][3]{\linewidth}\\
\convTeXLength[cc][3]{\linewidth}\\
\convTeXLength[cm][3]{\linewidth}\\
\convTeXLength[dd][3]{\linewidth}\\
\convTeXLength[em][3]{\linewidth}\\
\convTeXLength[ex][3]{\linewidth}\\
\convTeXLength[in][3]{\linewidth}\\
\convTeXLength[km][3]{\linewidth}\\
\convTeXLength[m][3]{\linewidth}\\
\convTeXLength[mm][3]{\linewidth}\\
\convTeXLength[mu][3]{\linewidth}\\
\convTeXLength[pc][3]{\linewidth}\\
\convTeXLength[pt][3]{\linewidth}\\
\convTeXLength[px][3]{\linewidth}\\
\convTeXLength*[sp][3]{\linewidth}
\end{LTXexample}

\begin{LTXexample}[width=0.5\linewidth]
The current width of the letter M is
1\,em, which is 

\convTeXLength[bp][3]{1em}\\
\convTeXLength[cc][3]{1em}\\
\convTeXLength[cm][3]{1em}\\
\convTeXLength[dd][3]{1em}\\
\convTeXLength[em][3]{1em}\\
\convTeXLength[ex][3]{1em}\\
\convTeXLength[in][3]{1em}\\
\convTeXLength[km][3]{1em}\\
\convTeXLength[m][3]{1em}\\
\convTeXLength[mm][3]{1em}\\
\convTeXLength[mu][3]{1em}\\
\convTeXLength[pc][3]{1em}\\
\convTeXLength[pt][3]{1em}\\
\convTeXLength[px][3]{1em}\\
\convTeXLength*[sp][3]{1em}
\end{LTXexample}

\begin{LTXexample}[width=0.5\linewidth]
The current example linewidth is 
\the\linewidth, which is 

\convTeXLength[bp][-1]{\linewidth}\\
\convTeXLength[cc][0]{\linewidth}\\
\convTeXLength[cm][1]{\linewidth}\\
\convTeXLength[dd][2]{\linewidth}\\
\convTeXLength[em][3]{\linewidth}\\
\convTeXLength[ex][4]{\linewidth}\\
\convTeXLength[in][5]{\linewidth}\\
\convTeXLength[km][6]{\linewidth}\\
\convTeXLength[m][7]{\linewidth}\\
\convTeXLength[mm][8]{\linewidth}\\
\convTeXLength[mu][9]{\linewidth}\\
\convTeXLength[pc][10]{\linewidth}\\
\convTeXLength[pt][11]{\linewidth}\\
\convTeXLength[px][12]{\linewidth}\\
\convTeXLength*[sp][13]{\linewidth}
\end{LTXexample}



\begin{LTXexample}[width=0.5\linewidth]
\convTeXLength[em][2]{1}\\ % default is cm
\convTeXLength[em][2]{2}\\
\convTeXLength[em][2]{3}
\end{LTXexample}



\begin{LTXexample}[width=0.5\linewidth]
The current example linewidth is 
\the\linewidth, which is 

\convTeXLength[em][2]{\linewidth}\\
\convTeXLength[ex][2]{\linewidth}\\
\convTeXLength[mu][2]{\linewidth}

\Huge Test\\
\convTeXLength[em][2]{\linewidth}\\
\convTeXLength[ex][2]{\linewidth}\\
\convTeXLength[mu][2]{\linewidth}
\end{LTXexample}

\subsection{Converting a value with given unit into another one}

\begin{LTXexample}[width=0.5\linewidth]
\convLength[m]{1.2345}[km]\\
\convLength[cm]{1.2345}[km]\\
\convLength*[mm]{1.2345}[km]\\
\convLength[bp]{1.2345}[km]\\
\convLength[km][4]{3499402.985833}[bp]
\end{LTXexample}



\end{document}
