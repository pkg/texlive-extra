-----------------------------------------------------------
Package:     datestamp
Version:     v0.3 (14 December, 2021)
Author:      निरंजन
Description: Fixed date-stamps with LuaLaTeX
Repository:  http://puszcza.gnu.org.ua/projects/datestamp
License:     GPLv3+, GFDLv1.3+
-----------------------------------------------------------
