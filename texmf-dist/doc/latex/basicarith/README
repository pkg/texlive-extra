+AMDG

This document is copyright 2014 by Donald P. Goodman, and is
released publicly under the LaTeX Project Public License.  The
distribution and modification of this work is constrained by the
conditions of that license.  See
	http://www.latex-project.org/lppl.txt
for the text of the license.  This document is released
under version 1.3 of that license, and this work may be distributed
or modified under the terms of that license or, at your option, any
later version.

This work has the LPPL maintenance status 'maintained'.

The Current Maintainer of this work is Donald P. Goodman
(dgoodmaniii@gmail.com).

This work consists of the files basicarith.ins and
basicarith.dtx, along with derived files basicarith.sty and
basicarith.pdf.

basicarith provides macros for typesetting basic arithmetic,
in the style typically found in textbooks.  It focuses on
the American style of performing these algorithms.  It is
written mostly in low-level TeX; the goal is to have it run
in either plain TeX or LaTeX, but there are two
constructions that currently prevent this.  It is highly
configurable, with macros and lengths described in the
documentation.

This package should run properly on any properly running LaTeX
system.  It's been tested specifically with TeXLive on Linux (3.2.0
kernel).
