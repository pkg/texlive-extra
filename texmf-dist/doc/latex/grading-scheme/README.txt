The grading-scheme package
---

Copyright (C) 2022 Maximilian Keßler <ctan@maximilian-kessler.de>

This material is subject to the LaTeX Project Public License 1.3c.
The latest version of this license is at: http://www.latex-project.org/lppl.txt


This bundle contains the files:

README.md            this file
grading-scheme.dtx   documented LaTeX source file
grading-scheme.pdf   documentation produced from the dtx file
grading-scheme.ins   run to generate the package

This work is author-maintained.

This is version 0.1.1 of the package, dated 2022-02-24

Description
---

This package aims at an easy-to-use interface to typeset
grading schemes in tabular format, in particular grading-schemes
of exercises of mathematical olympiads where multiple
solutions have to be graded and might offer mutual
exclusive ways of receiving points.

It can be run with LaTeX directly.
