# resmes
Restriction of measure in LaTeX

## Package usage
Just put in your preamble

    \usepackage{resmes}
    
It provides command `\resmes` that prints measure restriction symbol.
