

# The `hep-bibliography` package

Bibliographies for high energy physics

## Introduction

The `hep-bibliography` package extends the `biblatex` package with some functionality mostly useful for high energy physics.
In particular it makes full use of all `bibtex` fields provided by `inspirehep.net`.

The package can be loaded via `\usepackage{hep-bibliography}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

