
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %                                                                 %
     %     This is file    t-manual.tex    ( version 14.08.2006 )      %
     %                                                                 %
     %      Diagram macros for tangles and braided Hopf algebras       %
     %                                                                 %
     %                 (Yu. Bespalov, V. Lyubashenko)                  %
     %                                                                 %
     %       available from CTAN and http://www.math.ksu.edu/~lub/     %
     %                                                                 %
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{t-angles}
%\usepackage[emtex]{t-angles}
%\usepackage{t-erratr}

\setlength{\textwidth}{16 true cm}
\setlength{\textheight}{22.5 true cm}
\setlength{\topmargin}{-1.4 true cm}
\setlength{\oddsidemargin}{0mm}

\title{{\Huge\tt t-angles.sty }
\footnote{t-angles.sty is available from {\tt
http://www.math.ksu.edu/$\sim$lub/} or from CTAN}
\\
\mbox{\Large\bf(Diagram macros for tangles and braided Hopf algebras)}
}
\author{Yu. Bespalov \and V. Lyubashenko}
\date{Version 14.08.2006}

\def\subsec#1{\subsection*{#1}\addcontentsline{toc}{subsection}{#1}}

\def\hhmode{{{\tt hh}-mode }}
\def\Command#1{\hbox{\tt\string#1}}
\def\command#1{\ \hbox{\tt\string#1} \ }
\def\SHOW#1#2{\begin{array}{c}
                   \begin{tangle}#1\end{tangle}\\
                   \hbox{\tt\string#2}
              \end{array}}
\def\Show#1{\SHOW#1#1}
\def\Showh#1{\SHOW{\hh#1}#1}


\bibliographystyle{amsplain}

\begin{document}

\maketitle
\tableofcontents

\section{Introduction}

\subsection*{Usage:}
\addcontentsline{toc}{subsection}{Usage}
$$
\vcenter{\hbox{\command{\usepackage[emtex]\{t-angles\}}}
         \hbox{(for \command{emtex} drivers, \tt dviwin,}
         \hbox{ \tt dvips, yap )}}
\qquad \hbox{or} \qquad
\vcenter{\hbox{\command{\usepackage\{t-angles\}} $\equiv$}
         \hbox{\command{\usepackage[TPIC]\{t-angles\}}}
         \hbox{(for TPIC drivers such as \tt dviwin,}
         \hbox{ {\tt xdvi, dvips, yap, dvipdfm, kdvi} )}}
$$
To use with \command{kluwer.cls} add the option \command{kluwer}:\\
\command{\usepackage[emtex,kluwer]\{t-angles\}} or
\command{\usepackage[kluwer]\{t-angles\}}.

The main option \command{TPIC} is executed by default. It can be
overwritten by the antagonistic option \command{emtex}. These two
options give slightly different *.dvi output, when they are used with
\LaTeX. The package works also with pdf\LaTeX. In this case both
options produce identical *.pdf output. Actually, the third option
\command{pdflatex} is executed in this case automatically. You should
not type \command{\usepackage[pdflatex]\{t-angles\}} in your file
unless you want to prohibit its use with \LaTeX. Another way to produce
*.pdf file is to apply \command{dvipdfm} to the *.dvi output, obtained
with the \command{TPIC} option.

Under pdf\LaTeX{} the information about slanted lines is stored in a file
*.emp and read on the following pass. Consequently, the changes made in
a tangle diagram are not reflected immediately in the *.pdf output. You
may need several ($\simeq2$) runs of pdf\LaTeX{} to see the final
picture.


\subsec{Acknowledgments}
 An optional parameter for (co)actions is proposed by Bernhard Drabant.
The file \command{t-angles.sty} contains parts of
\command{emlines2.sty} by Georg Horn and Eberhard Mattes and parts of
\command{eepic.sty} by Conrad Kwok. PDF implementation of em\TeX{}
specials is due to Hans Hagen. We have incorporated his con\TeX t
support macros `em\TeX{} specials to PDF conversion' from
\command{supp-emp.tex} distributed with Te\TeX. These parts of the code
are used in the three options: \command{emtex}, \command{TPIC} and
\command{pdflatex} respectively. To understand them the reader is
invited to read comments in the original works. In order to distinguish
between ordinary \LaTeX{} and pdf\LaTeX{} modes, Heiko Oberdiek's
package \command{ifpdf.sty} is loaded.


\subsection*{Main features:}
\addcontentsline{toc}{subsection}{Main features}
\begin{itemize}
\item
The environments
$$
\vcenter{\hbox{\tt \{tangle\}}
         \hbox{\tt \{tanglec\}}
         \hbox{\tt \{tangler\}}
         \hbox{\tt \{tangles\}}}
\qquad
\vcenter{\hbox{are arrays with}
         \hbox{one or more}
         \hbox{column style:}}
\qquad
\vcenter{\hbox{\tt \{array\}\{l\}}
         \hbox{\tt \{array\}\{c\}}
         \hbox{\tt \{array\}\{r\}}
         \hbox{\tt \{array\}}   }
\quad
\vcenter{\hbox{(left)}
         \hbox{(centered)}
         \hbox{(right)}
         \hbox{(any)}}
$$
respectively. Likewise {\tt \{array\}}, the {\tt \{tangles\}}
environment allows an optional argument {\tt t} or {\tt b} to align
the upper base line or the bottom of the tangle with the exterior
base line: \verb+\begin{tangles}[b]{l*3cr}+.
\item
 \command\unitlens is the global length parameter.
 Default value is  \command{10 pt} .
\item
 \command\hstretch and \command\vstretch are relative length
 parameters, horizontal and vertical stretch:
\begin{equation*}
\begin{split}
\command\unith = \command\hstretch \%\;\; \text{of}\; \command\unitlens \,,
\\
\command\unitv = \command\vstretch \%\;\; \text{of}\; \command\unitlens \,,
\end{split}
\end{equation*}
 set to an integer number of percents before the beginning of a tangle.
\newline
 Default settings are
\command{\hstretch}{\tt 100} and   \command{\vstretch}{\tt 100}.
 The commands \command\hstretch and \command\vstretch should be used only
 outside of tangle environments (with an exception of embedded tangle
 environments).
\item
The commands \command{\hstr \{<number>\}} ,
\command{\vstr \{<number>\}} can be used inside tangle environments
instead of \command{\hstretch <number>} , \command{\vstretch <number>}.
They will act within their \LaTeX{} scope.
\item
The height of every row is \ {\tt 2}\command\unitv
or \command\unitv  if the command \command\hh (see below) is used;
\newline
the widths of standard fragments are \ {\tt 0, .5, 1, 2, 3} or {\tt 4}
\command\unith
\item
The command \command\hh obeys to \LaTeX{} scope rules.  The command
\command\HH acts in the same way as \command\hh but put at the
beginning of a row works for the whole row in the \ {\tt \{tangles\}}
environment.
\item
The style understands the commands
\command\thinlines and \command\thicklines .
\item
The command \command{\step[<number>]} is used to produce horizontal space
\command\kern{\tt <number>}\command\unith and works in any mode
(inside and outside of the \command{tangle} environment).
\begin{eqnarray*}
&\command\step = \command{\step[1]}
\qquad
&\command\Step = \command{\step[2]}
\\
&\command\hstep = \command{\step[.5]}
\quad
&\command\hhstep = \command{\step[-.5]}
\end{eqnarray*}
\item
Vertical spacing before the next row is produced by
\ $\backslash\backslash[\langle\hbox{\tt vertical\_space}\rangle]$ \
with optional argument (like in standard \ {\tt \{array\}} \
environment).
\item
The command \command{\object\#1} is used to put the object \command{#1}
directly over or under the end of the string (inside and outside of the
\ {\tt \{tangle\}} \ environment). It adds a vertical space below or
above as required. More space can be added as above.
\newline
The command
\command{\Put(x\_coord,y\_coord)[binding\_point]<object>} puts
{\tt <object>} into the intended position and works in
\ {\tt \{tangle[cs]\}} \ environment like a combination of \command\put
and \command\makebox. Coordinates are integers, measured in
{\tt .1}\command\unith, {\tt .1}\command\unitv units;
\command{binding\_point} is a combination of two letters \ {\tt lcr} \
and \ {\tt tcb} \ according to the usual \LaTeX{} rules.
\newline
The commands
\command{\nodeu\#1,} \command{\noded\#1,}
\command{\nodel\#1,} \command{\noder\#1,}
\command{\noderu\#1,} \command{\noderd\#1,}
\command{\nodelu\#1,} \command{\nodeld\#1}
produce zero boxes and put \ {\tt \#1} \ into the corresponding
position.
\newline
\begin{minipage}{0.4\textwidth}
The picture
\begin{equation*}
\vstretch 200  \hstretch 200
\begin{tangle}
\nodeu.\noded.\nodel.\noder.\noderu.\noderd.\nodelu.\nodeld.
\end{tangle}
\end{equation*}
is described by the lines:
\end{minipage}
\begin{minipage}{0.5\textwidth}
\begin{verbatim}
\vstretch 200  \hstretch 200
\begin{tangle}
\nodeu.\noded.\nodel.\noder.
\noderu.\noderd.\nodelu.\nodeld.
\end{tangle}
\end{verbatim}
\end{minipage}
\end{itemize}

\section{Macros in pictures}

\subsec{Straight lines and nodes}

\begin{itemize}
\item
The commands \command\id, \command\n, \command\s,\command\node,
\command\unit, \command\counit work also in \hhmode.
$$
\Show\id    \quad
\Show\idash \quad
\Show\n     \quad
\Show\s     \quad
\Show\nd    \quad
\Show\sd    \quad
\Show\node  \quad
\Show\unit  \quad
\Show\counit
$$
The command \command\FillCircDiam denotes the filled circle diameter.
It is set to an integer between 1 and 9 (here the measure unit is
0.1 \command\unith). Default value is 3.
\item
The command \command\hln{\tt <number>} \ produces horizontal line on
\ {\tt <number>}\command\unith.
\item
Argument of \command\ne,\command\nw,\command\se,\command\sw,
\command\ned,\command\nwd,\command\sed,\command\swd
is \ {\tt 0,1,2,3} or {\tt 4}; {\tt 0} produces empty box and
other produce (dash) lines with horizontal projections equal
\ {\tt <argument>}\command{\unith} cribbed into {\tt 1$\times$2} box.
The commands \command\ne,\command\nw,\command\se,\command\sw
produce {\tt 1$\times$1} box in \hhmode.
$$
\SHOW{\nw1}{\nw{1}}\quad
\SHOW{\nwd1}{\nwd{1}}\quad
\SHOW{\sw0}{\sw{0}}\quad
\SHOW{\se3}{\se{3}}\quad
\SHOW{\sed3}{\sed{3}}\quad
\SHOW{\nw3\nw2\nw1\n\ne1\ne2\ne3}{\nw{3}\string\nw{2}%
\string\nw{1}\string\n\string\ne{1}\string\ne{2}\string\ne{3}}
$$
\item
All the following commands work in \hhmode and produce
the similar diagrams in {\tt .5$\times$1} boxes.
$$
\Show\d    \quad
\Show\dd   \quad
\Show\hd   \quad
\Show\hdd  \quad
\Show\dh   \quad
\Show\ddh
$$
\end{itemize}


\subsec{(Under/over)crossings. Braiding and symmetry}
The following crossings and dashed crossings are shown in normal mode.
$$
\Show\x     \quad
\Show\xx    \quad
\Show\hx    \quad
\Show\hxx   \quad
\Show\X     \quad
\Show\XX    \quad
\Show\xd    \quad
\Show\xxd   \quad
\Show\hxd   \quad
\Show\hxxd
$$
The commands \command\X, \command\XX, \command\x, \command\xx work in
\hhmode and produce similar diagrams of half width and height
({\tt 1$\times$1} boxes).

\subsec{(Co)pairings.}
The commands \command\ev, \command\coev work in \hhmode and produce
the similar diagrams of half width and height ({\tt .5$\times$1} boxes).
For convenience in \hhmode \command\hev $\equiv$ \command\ev and
\command\hev $\equiv$ \command\ev.
\begin{align*}
\Show\hev        \quad
&\Show\ev         \quad
&\Show\Ev         \quad
&\Show\EV
&\\
\Show\hcoev      \quad
&\Show\coev       \quad
&\Show\Coev       \quad
&\Show\COEV
&\end{align*}


\subsec{Morphisms. Frame and dash boxes}
\begin{itemize}
\item
$$
\SHOW{\Q f}{\Q \ f}   \quad
\SHOW{\QQ f}{\QQ \ f} \quad
\SHOW{\O f}{\O \ f}   \quad
%\SHOW{\circ  f}{\circ  \ f} \quad
\SHOW{\morph f}{\morph \ f} \quad
\SHOW{\tu    f}{\tu    \ f} \quad
\SHOW{\td    f}{\td    \ f} \quad
\Show\S                     \quad
\Show\SS
$$
\item
$$
\SHOW{\ox f}{\ox \ f}            \qquad\qquad
\SHOW{\ro p}{\ro \ p}            \quad
\SHOW{\coro q}{\coro \ q}        \quad
\SHOW{\Ro p}{\Ro \ p}            \quad
\SHOW{\coRo q}{\coRo \ q}
$$
\item
The commands \command{\dbox\#1\#2,}\command{\ffbox\#1\#2,}
\command{\obox\#1\#2,}\command{\tbox\#1\#2} put {\tt \$\#2\$} in the middle
of \ {\tt \#1}$\times${\tt 2} (or {\tt \#1}$\times${\tt 1} \ in
\hhmode ) box with dash, rectangle, oval frame or without frame.
\par
For example, the text
\newline
\begin{minipage}{.4\textwidth}
\begin{verbatim}
\begin{tangles}{rcl}
\HH\obox 10&&\obox 10\\
\HH\d&&\dd\\
&\hhstep\obox 3V\hhstep&
\end{tangles}
\end{verbatim}
\end{minipage}
produces
\begin{minipage}{.4\textwidth}
\begin{equation*}
\begin{tangles}{rcl}
\HH\obox 10&&\obox 10\\
\HH\d&&\dd\\
&\hhstep\obox 3V\hhstep&
\end{tangles}
\end{equation*}
\end{minipage}
\end{itemize}

\subsec{(Co)multiplications and cocycles}
The commands \command\cu, {\tt \string\cu *},
\command\cd,  {\tt \string\cd *} work in \hhmode and produce the similar
diagrams of half width and height ({\tt .5$\times$1} boxes).
For convenience in \hhmode \command\hcu $\equiv$ \command\cu and
\command\hcd $\equiv$ \command\cd .
\begin{equation*}
\begin{split}
\Show\Cu      \quad
\Show\cu      \quad
\Show\hcu     \quad
\Show\hdcu    \quad
\Show\hddcu
\\
\SHOW{\Cu    *}{\Cu    *}  \quad
\SHOW{\cu    *}{\cu    *}  \quad
\SHOW{\hcu   *}{\hcu   *}  \quad
\SHOW{\hdcu  *}{\hdcu  *}  \quad
\SHOW{\hddcu *}{\hddcu *}
\\
\SHOW{\Cd    *}{\Cd    *}  \quad
\SHOW{\cd    *}{\cd    *}  \quad
\SHOW{\hcd   *}{\hcd   *}  \quad
\SHOW{\hdcd  *}{\hdcd  *}  \quad
\SHOW{\hddcd *}{\hddcd *}
\\
\Show\Cd      \quad
\Show\cd      \quad
\Show\hcd     \quad
\Show\hdcd    \quad
\Show\hddcd
\end{split}
\end{equation*}

\subsec{(Co)actions}
Commands \command\lu, \command\ld, \command\ru, \command\rd
have optional parameter \ {\tt [\#1]} which equals to width of the box:
\begin{align*}
&\command\hh
& \SHOW{\hh\Lu}{\Lu$\equiv\!\!\!\!$\command\lu$\!\!\!\!\!\!$[2]}   \quad
  \SHOW{\hh\lu} {\lu$\equiv\!\!\!\!$\command\lu$\!\!\!\!\!\!$[1]}   \quad
  \Showh\hlu \quad
  \Showh\hru  \quad
  \SHOW{\hh\ru} {\ru$\equiv\!\!\!\!$\command\ru$\!\!\!\!\!\!$[1]}  \quad
  \SHOW{\hh\Ru} {\Ru$\equiv\!\!\!\!$\command\ru$\!\!\!\!\!\!$[2]}
&\\
&&\SHOW\Lu{\Lu$\equiv\!\!\!\!$\command\lu$\!\!\!\!\!\!$[2]}  \quad
  \SHOW\lu{\lu$\equiv\!\!\!\!$\command\lu$\!\!\!\!\!\!$[1]}  \quad
  \Show\hlu \quad
  \Show\hru \quad
  \SHOW\ru{\ru$\equiv\!\!\!\!$\command\ru$\!\!\!\!\!\!$[1]}  \quad
  \SHOW\Ru{\Ru$\equiv\!\!\!\!$\command\ru$\!\!\!\!\!\!$[2]}
&\\
&&\SHOW\Ld{\Ld$\equiv\!\!\!\!$\command\ld$\!\!\!\!\!\!$[2]}  \quad
  \SHOW\ld{\ld$\equiv\!\!\!\!$\command\ld$\!\!\!\!\!\!$[1]}  \quad
  \Show\hld  \quad
  \Show\hrd  \quad
  \SHOW\rd{\rd$\equiv\!\!\!\!$\command\rd$\!\!\!\!\!\!$[1]}  \quad
  \SHOW\Rd{\Rd$\equiv\!\!\!\!$\command\rd$\!\!\!\!\!\!$[2]}
&\\
&\command\hh
& \SHOW{\hh\Ld}{\Ld$\equiv\!\!\!\!$\command\ld$\!\!\!\!\!\!$[2]}  \quad
  \SHOW{\hh\ld}{\ld$\equiv\!\!\!\!$\command\ld$\!\!\!\!\!\!$[1]}  \quad
  \Showh\hld  \quad
  \Showh\hrd  \quad
  \SHOW{\hh\rd}{\rd$\equiv\!\!\!\!$\command\rd$\!\!\!\!\!\!$[1]}  \quad
  \SHOW{\hh\Rd}{\Rd$\equiv\!\!\!\!$\command\rd$\!\!\!\!\!\!$[2]}
&\end{align*}

\subsec{Compositions}
$$
\Show\kk    \quad
\Show\luld  \quad
\Show\cucd  \quad
\Show\rurd  \quad
\Show\k
$$

\section{Examples}

\begin{minipage}{.3\textwidth}
\[
\begin{tangle}
\object{B}\step\object{B}\\
\cucd\\
\object{B}\step\object{B}
\end{tangle}
\;=\enspace
\begin{tangles}{lcr}
\HH \cd && \cd \\
\HH \id & \x & \id \\
\HH \cu && \cu
\end{tangles}
\]
\begin{equation*}
\hstretch 90  \vstretch 60
\begin{tangle}
\step\hcoev\step\coev\\
\dd\step\hxx\step\dd\\
\id\Step\id\step\hx\\
\d\step\hxx\step\d\\
\step\hev\step\ev
\end{tangle}
\end{equation*}
\end{minipage}
\begin{minipage}{.2\textwidth}
The first picture is produced by:
\end{minipage}
\quad
\begin{minipage}{.4\textwidth}
\begin{verbatim}
\[
\begin{tangle}
\object{B}\step\object{B}\\
\cucd\\
\object{B}\step\object{B}
\end{tangle}
\;=\enspace
\begin{tangles}{lcr}
\HH \cd && \cd \\
\HH \id & \x & \id \\
\HH \cu && \cu
\end{tangles}
\]
\end{verbatim}
\end{minipage}

\begin{minipage}{0.23\textwidth}
Nested environments:
\[
\begin{tanglec}
    \begin{tangles}[b]{c}
    \vstr{200} \xx \\ \vstr{200} \xx
    \end{tangles}
\step[4]
    \begin{tangles}[b]{*3c}
    & \object{F} & \\ & \xx & \\ \id && \O{u_1^2} \\
    & \xx & \\ \id && \O{u_1^2}
    \end{tangles}
\\
\id \Step \coRo\omega \Step \id \\
\hstr{200} \coRo\omega
\end{tanglec}
\]
The level of nesting depends on the save size of your \TeX.
\end{minipage}
\ \
\begin{minipage}{0.7\textwidth}
\begin{verbatim}
\[
\begin{tanglec}
    \begin{tangles}[b]{c}
    \vstr{200} \xx \\ \vstr{200} \xx
    \end{tangles}
\step[4]
    \begin{tangles}[b]{*3c}
    & \object{F} & \\ & \xx & \\ \id && \O{u_1^2} \\
    & \xx & \\ \id && \O{u_1^2}
    \end{tangles}
\\
\id \Step \coRo\omega \Step \id \\
\hstr{200} \coRo\omega
\end{tanglec}
\]
\end{verbatim}
\end{minipage}
\smallskip

Note the use of optional argument \verb+[b]+ to align the subtangles
at the bottom.

\section{Development}

\subsec{History and versions}

The style was produced by the first author in 1994. It was completely
modified and essentially improved by the second author in 1997 for
real--life applications in \cite{BKLT:int}.

{%beginenumerate
\setlength{\leftmargini}{34mm}
\begin{enumerate}
\item[04.04.99$\to$20.04.00]
The output of commands \command{\tu\#1,}\command{\td\#1,}
\command{\ro\#1,}\command{\coro\#1,}\command{\Ro\#1,}\command{\coRo\#1}
slightly differs. Now they fit their boxes.

\item[20.04.00$\to$10.09.00]
Dashed crossings are represented by the commands
\command{\xd,}\command{\xxd,}\command{\hxd,}\command{\hxxd.}

\item[10.09.00$\to$22.04.06]
It is possible to use the package with pdf\LaTeX.

\item[22.04.06$\to$14.08.06]
Behaviour of the package with pdfe\LaTeX{} of MiK\TeX{} 2.5 is
corrected.

\end{enumerate}
}%endenumerate

\begin{thebibliography}{1}

\bibitem{BKLT:int}
Yu.~N. Bespalov, T.~Kerler, V.~V. Lyubashenko, and V.~G. Turaev,
  \emph{Integrals for braided {H}opf algebras}, J. Pure and Appl. Algebra
  \textbf{148} (2000), no.~2, 113--164, Available as
  http://arXiv.org/abs/q-alg/9709020.

\end{thebibliography}

%\bibliography{yuri}

%\subsec{Bugs}
%\begin{itemize}
%\item
%\end{itemize}

\subsec{Directions for modification
%\protect\footnote{What can you suggest?}
}

\begin{itemize}
\item
In the future some problems can be solved by introducing global
(logical) parameters that switch configuration and behavior of certain
families of commands in questionable situations.
\item
To adopt commands like in {\tt \{picture\}} environment to
produce special fragments of one time use.
\item
To make the second argument of the command \command{\Put(\#1)[\#2]\#3}
optional.
\item
To produce command index for this manual.
\item
To add possibility to change size of circle in circled morphisms
(in particular, to turn \command\morph into a special case of \command\O).
\end{itemize}


\medskip
\begin{center}
\fbox{Suggestions are welcome.}
\end{center}


\appendix
\section{Exercises}
How to produce the following ?
\begin{equation*}
\vstretch 300
\begin{tanglec}
\hh\dd\d\step[1.5]\dd\d \\
\obox 6{\hstretch 85   \vstretch 80
        \begin{tangles}{rcl}
        \HH\obox 10&&\obox 10\\
        \HH\d&&\dd\\
        &\hhstep\obox 3V\hhstep&
        \end{tangles}   }
\end{tanglec}
\end{equation*}

\end{document}


\section{\tt t-erratr.sty}

Macros from the old versions of \ {\tt tangles.sty} \ are stored in
this file for compatibility.
\begin{itemize}
\item
The following commands are special cases of
\command{\ffbox\#1\#2,}\command{\dbox\#1\#2}:
\begin{align*}
&\SHOW{\dash f}{\dash \ f}           \enspace
&\SHOW{\Dash f}{\Dash \ f}           \enspace
&\SHOW{\DDash f}{\DDash \ f}         \quad
&\SHOW{\Frabox f}{\Frabox \ f}       \enspace
&\SHOW{\doubleFrabox f}{\doubleFrabox \ f}
\\
\command\hh
&\SHOW{\hh\dash f}{\dash \ f}           \enspace
&\SHOW{\hh\Dash f}{\Dash \ f}           \enspace
&\SHOW{\hh\DDash f}{\DDash \ f}         \quad
&\SHOW{\hh\Frabox f}{\Frabox \ f}       \enspace
&\SHOW{\hh\doubleFrabox f}{\doubleFrabox \ f}
\end{align*}
and \command{\frabox} is equivalent to \command{\Frabox}.
\item
The following morphisms are special cases of
\command{\ro\#1,} \command{\coro\#1,} \command{\Ro\#1,} \command{\coro\#1}:
\vstretch 150 \hstretch 150
\begin{align*}
&\hstep\Show\r
&\hstep\Show\rh
&\hstep\Show\ra
&\hstep\Show\rb
\\
&\hstep\Show\rr
&\hstep\Show\rrr
&\hstep\Show\rra
&\hstep\Show\rrb
\\
&\Show\R
&\Show\Rh
&\Show\Ra
&\Show\Rb
\\
&\Show\RR
&&\Show\RRa
&\Show\RRb
\end{align*}
\item
The following commands are special cases of \command{\O\#1}:
$$
\vstretch 150 \hstretch 150
\Show\tS                         \quad
\Show\tSS                        \quad
\SHOW{\Ointl H}{\Ointl \ H}      \;
\SHOW{\Ointr H}{\Ointr \ H}      \;
\SHOW{\Ocointl H}{\Ocointl \ H}  \;
\SHOW{\Ocointr H}{\Ocointr \ H}
$$
\item
$$
\SHOW{\pairing p}{\pairing \ p}  \quad
\SHOW{\Pairing P}{\Pairing \ P}  \quad
$$
\item
Functions of the old unsightly commands
\command{\obj\#1,} \command{\Obj\#1,} \command{\negobj\#1}
are shouldered by the command \command{\Put(\#1)[\#2]\#3}.
\end{itemize}

Commands \command{\harc}, \command{\inarc}, \command{\outarc} have two
arguments -- horizontal and vertical diameters of an ellipse
\vspace*{5mm}
\[
\SHOW{\inarc{15}{20}}{\inarc \{15\}\{20\}}       \qquad
\SHOW{\harc{20}{15}}{\harc \{20\}\{15\}}         \qquad
\SHOW{\outarc{30}{25}}{\outarc \{30\}\{25\}}
\]

\end{document}
