# udes-genie-these : Thesis class for the Faculté de génie at the Université de Sherbrooke

## Overview
The udes-genie-these class can be used for Ph.D. thesis, master's thesis and project definition at the Faculté de génie of the Université de Sherbrooke (Québec, Canada). The class file is coherent with the latest version of the *Protocole de rédaction aux études supérieures* which is available on the faculty's intranet.

The class file documentation is in French, the language of the typical user at the Université de Sherbrooke. An example of use is also distributed with the documentation.

Copyright (C) 2017-2022 Charles-Antoine Brunet

## Class file (udes-genie-these.cls)
The class file (udes-genie-these.cls) is generated with the installation file (udes-genie-these.ins) with the following command:
```
pdflatex udes-genie-these.ins
```

## File description
#### Documentation
- udes-genie-these.pdf: package documentation
- README.md: this file

#### Source files
- udes-genie-these.dtx: source file
- udes-genie-these.ins: installation file

#### Usage example
- document.tex: main file
- \*.tex, \*.bib: files used by main file

## Authors and maintainers
Charles-Antoine Brunet (Charles-Antoine.Brunet@USherbrooke.ca)

## License
The contents are distributed under the LaTeX Project Public License, version 1.3c or later.

## History
See the change log in the documentation.

## Distribution
CTAN: [udes-genie-these](https://www.ctan.org/pkg/udes-genie-these)

TDS archive: [udes-genie-these.tds.zip](https://mirrors.ctan.org/install/macros/latex/contrib/udes-genie-these.tds.zip)

Flat archive: [udes-genie-these.zip](https://mirrors.ctan.org/macros/latex/contrib/udes-genie-these.zip)
