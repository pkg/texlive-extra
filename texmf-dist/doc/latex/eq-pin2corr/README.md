The eq-pin2corr package
Author: D. P. Story
Dated: 2021-05-29

This package is an add-on to the quiz environment of the exerquiz
package (part of the acrotex bundle). It adds PIN security to a quiz created by 
the quiz environment. To correct a quiz, the document consumer must
press the Correct button of the quiz and successfully enter the correct
PIN number. The PIN security is designed for the instructor to mark and record
the students effort on that quiz.

The package works for a the usual workflows

What's New (2021-05-29) Added (optional) PIN security to the Begin Quiz button.
  Added (an optional) `warn and freeze' feature to a quiz. Added (an optional) 
  \qzResetTally field that holds the number of times a student retakes any given 
  quiz. Add (an optional) feature where the document author can set the maximum 
  number of times a student can retake a quiz. Demo files for these
  features are found on the AcroTeX Blog site:
  http://www.acrotex.net/blog/?p=1516
  http://www.acrotex.net/blog/?p=1519

D. P. Story 
www.acrotex.net
blog.acrotex.net
dpstory@uakron.edu
dpstory@acrotex.net

