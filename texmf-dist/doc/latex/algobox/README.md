# The [algobox](https://ctan.org/pkg/algobox) package

This [small LaTeX package](https://ctan.org/pkg/algobox) can typeset Algobox
programs almost exactly as displayed when editing with
[Algobox itself](http://www.xm1math.net/algobox/), using an input syntax very
similar to the actual Algobox program text.

It is far better than the LaTeX export of Algobox which does not look like the
editor rendition, produces standalone documents cumbersome to customize, and
has arbitrary and inconsistent differences between the input syntax and the
program text.

This package depends upon the following other LaTeX packages:
[expl3](https://ctan.org/pkg/expl3),
[TikZ](https://ctan.org/pkg/pgf),
[environ](https://ctan.org/pkg/environ),
[xparse](https://ctan.org/pkg/xparse),
[xcolor](https://ctan.org/pkg/xcolor).

This package may be distributed and/or modified under the conditions of the
General Public License (GPL), either version 3 of this license or (at your
option) any later version.
