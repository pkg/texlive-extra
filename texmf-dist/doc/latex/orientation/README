orientation 1.0 (January 10, 2021)
Set page orientation for dvips/Ghostscript
Daniel Benjamin Miller <dbmiller@dbmiller.org>

When using dvips with Ghostscript, the various PDF auto-rotation modes may
sometimes need some tweaking. Using the auto-rotation feature of Ghostscript
may suffice in most cases: it sets the orientation of the pages of the PDF
according to text direction (either per-page or for the whole document).

But sometimes we don't want this. For instance, Ghostscript may not obey
the orientation set by geometry or other packages.

We have eight commands available, in two categories.

These commands orient the current page and all following pages:

* \setportrait
* \setlandscape
* \setupsidedown
* \setcounterlandscape

These commands orient the current page only:

* \thispageportrait
* \thispagelandscape
* \thispageupsidedown
* \thispagecounterlandscape

If you don't process your file with dvips and Ghostscript/ps2pdf, then don't
expect any of these commands to do anything.

Made available under the CC-Zero Dedication, 2021
For more info: https://creativecommons.org/publicdomain/zero/1.0/
