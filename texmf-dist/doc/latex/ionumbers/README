SHORT DESCRIPTION
=================

(This is a copy of the LaTeX code of the documentation's abstract.)

\textsf{ionumbers} stands for `input/output numbers'.

This package restyles numbers in math mode. If a number in the input file
is written, e.g., as |$3,231.44$| as commonly used in English texts, this
package is able to restyle it to be output as `$3\,231{,}44$' as commonly
used in German texts (and vice versa). This may be very useful, if you have
a large table and want to include it in texts with different output
conventions without the need of changing the table.

Furthermore this package can automatically group digits left to the decimal
separator (\emph{thousands}) and right to the decimal separator
(\emph{thousandths}) without the need of specifing commas (English) or
points (German) as separators. E.g., the input |$1234.567890$| can be
output as `$1\,234.\,567\,890$'.  By default, thousands/thousandths are
grouped in triplets, but the grouping length is configurable, which is
useful for numerical data.

Finally, an |e| starts the exponent of the number. For example, |$21e6$|
may be output as `$26\times10\,^{6}$'.

See `ionumbers.pdf' for detailed information on usage.

BUILDING
========

If you do not want to use any pre-build `ionumber.sty' and `ionumber.pdf'
files, you will have to choose one of the following two ways for building them.
Otherwise you can skip this section.

1) Building with make
---------------------

Running `make' will create the LaTeX package, the documentation and the test
file:
  $ make

2) Building manually
--------------------

a) Generation of package (.sty file):

Run `ionumbers.ins' through LaTeX:
  $ pdflatex -interaction=nonstopmode ionumbers.ins

b) Generation of documentation:

Run `ionumbers.dtx' through LaTeX:
  $ pdflatex -interaction=nonstopmode ionumbers.dtx

Then create the index and change history:
  $ makeindex -s gind.ist -o ionumbers.ind ionumbers.idx
  $ makeindex -s gglo.ist -o ionumbers.gls ionumbers.glo

Finally, run `ionumbers.dtx' three time through LaTeX:
  $ pdflatex -interaction=nonstopmode ionumbers.dtx
  $ pdflatex -interaction=nonstopmode ionumbers.dtx
  $ pdflatex -interaction=nonstopmode ionumbers.dtx

Optionally you can compile the test file:
  $ pdflatex -interaction=nonstopmode ionumbers_test.tex
  $ pdflatex -interaction=nonstopmode ionumbers_text.tex
  $ pdflatex -interaction=nonstopmode ionumbers_text.tex

INSTALLATION
============

1) Installing with make
-----------------------

To install the package into a TDS (= TeX directory structure; typically its base
directory is named `texmf' or something similar), run the following command:
  $ make DESTDIR=<your TDS> install

Note: `$(DESTDIR)' defaults to `$(HOME)/.texmf'.

2) Installing manually
----------------------

In principle, you will only habe to copy the file `ionumbers.sty' into a
directory, where LaTeX can find it.  If you copy it into a TDS (TeX directory
structure), you will most probably have to run the following command afterwards:
  $ mktexlsr <your TDS>

LICENSE
=======

Copyright 2007--2009,2011 Christian Schneider <software(at)chschneider(dot)eu>

idea and parts of this package based on: ziffer.sty v2.1
Copyright Martin Vaeth <vaeth@mathematik.uni-wuerzburg.de>

ion@addto@macro: identical to l@addto@macro from koma-script
                 bundle v2.9t
Copyright 1994-2004 Markus Kohm and Frank Neukam

Thanks to Martin Vaeth and Markus Kohm for permitting to use their
code in this package. Please do not bother Martin, Markus, or Frank
with questions or bugs concering this package!

This file is part of ionumbers.

ionumbers is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation, not any later version.

ionumbers is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ionumbers.  If not, see <http://www.gnu.org/licenses/>.

WARNING: THIS IS ALPHA SOFTWARE AND MAY CONTAIN SERIOUS BUGS!
