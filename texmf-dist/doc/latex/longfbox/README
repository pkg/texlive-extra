This is the README file for the longfbox package.

VERSION

Version 1.0, December 2015

SUMMARY

The `longfbox` package provides convenient framed boxes that can be customized 
using standard CSS attributes and broken over multiple pages.
It was written to support precise rendering of Madoko documents in LaTeX.
Notable features are:

* Specify the boxes using standard CSS attributes like `border-style=dashed`
  or `border-top-left-radius=10pt`. Almost all of the CSS 2.0 attributes
  with regard to borders, background, padding, and margins are supported.

* Fast and portable: only uses the standard LaTeX `picture`
  environment for drawing and does not depend on loading big packages
  like `tikz` or `pstricks`, which makes running LaTeX much faster.

* Supports breakable boxes that span multiple columns or pages. This package
  builds on the `longbox` package to break boxes over multiple pages.

* Much care has been put in precise rendering with proper baseline 
  alignment and no spurious extra whitespace.

* In contrast to larger packages like `mdframed` or `tcolorbox`, the
  `longfbox` package does not have more extensive features like frame
  titles, middle lines, skins, etc. The focus of this package
  is on rendering boxes well with full CSS support where every
  corner and side can be styled separately with good looking transitions
  (and where we only depend on the standard `picture` environment).

LICENSE

Copyright (c) 2015 Daan Leijen.

This software is author-maintained. Permission is granted to copy,
distribute and/or modify this software under the terms of the
LaTeX Project Public License, version 1.3 or higher. This software
is provided 'as it is', without warranty of any kind, either
expressed or implied, including, but not limited to, the implied
warranties of merchantability and fitness for any particular purpose.

RELATED PACKAGES

options, ellipse, pict2e, mdframed, tcolorbox

AUTHOR

Daan Leijen (daan@microsoft.com)


