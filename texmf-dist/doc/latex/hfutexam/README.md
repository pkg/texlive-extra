# hfutexam: exam class for Hefei University of Technology (China)

The package provides an exam class for Hefei University of Technology (China). 

+ Package: An exam class for Hefei University of Technology
+ Author:  Shenxing Zhang <zhangshenxing@hfut.edu.cn>
+ License: The LaTeX Project Public License 1.3c
