historische-zeitschrift 2016/12/01 v1.2
Copyright (c) Dominik Waßenhoven <domwass(at)web.de>, 2014-2016

************************************************************

Inhalt der Datei LIESMICH
 1. Kurzbeschreibung
 2. Installationshinweise
 3. Benutzung
 4. Versionsgeschichte
 5. Lizenz
 6. Dateien

 
 1. Kurzbeschreibung
 -------------------
 
 historische-zeitschrift ist ein Zitierstil für das 
 Paket biblatex von Philipp Lehman. Es bietet einen Stil 
 nach den Richtlinien der 'Historischen Zeitschrift'. Das 
 Schema ist ein Vollzitat für die erste Zitation und 'Autor, 
 Kurztitel (wie Anm. N), S' für alle folgenden Zitationen 
 (wobei S für die Seitenzahl steht). Für weitere Details des 
 Stils sehen Sie sich bitte die Richtlinien unter 
 http://194.97.159.218/verlag/historische-zeitschrift/hz-richtlinien.htm
 an.
 
 Dieses Paket baut vollständig auf biblatex auf; achten Sie 
 auch auf die Mindestanforderungen von biblatex selbst. 
 Anregungen und Verbesserungsvorschläge sind jederzeit 
 willkommen.
 
 
 2. Installationshinweise
 ------------------------
 
 historische-zeitschrift ist in den Distributionen MiKTeX 
 und TeX Live enthalten und kann mit deren Paketmanagern 
 bequem installiert werden. Wenn Sie stattdessen eine 
 manuelle Installation durchführen, so gehen Sie wie folgt 
 vor: Entpacken Sie die zip-Datei in den $LOCALTEXMF-Ordner 
 ihres Systems. Aktualisieren Sie anschließend die 'filename 
 database' ihrer TeX-Distribution. Für weitere Informationen
 schauen Sie bitte in die Dokumentation Ihrer Distribution.
 
 
 3. Benutzung
 ------------
 
 Der hier angebotene Zitierstil wird wie die Standard-
 Stile beim Laden des Pakets biblatex eingebunden: 
   \usepackage[style=historische-zeitschrift]{biblatex}
 Beachten Sie, dass der Stil für Zitate in Fußnoten gedacht 
 ist. Zitierkommandos außerhalb von Fußnoten führen immer zu 
 Vollzitaten!
 
 Die zusätzliche Option 'postnote=inparen' bietet die 
 Möglichkeit, die Seitenzahl bei Folgezitaten auch innerhalb 
 der Klammer auszugeben, also 
   Autor, Kurztitel (wie Anm. N, S) 
 Die Voreinstellung ist 'postnote=afterparen', also  
   Autor, Kurztitel (wie Anm. N), S 

 Die Option 'dashed'
 Analog zu den Standard-Stilen von biblatex wird die Option
 'dashed' zur Verfügung gestellt. Bei 'dashed=true' werden
 wiederkehrende Autoren/Herausgeber in der Bibliographie
 durch einen langen Strich ersetzt. Mit 'dashed=false' 
 wird diese Funktion ausgeschaltet, dann erscheinen immer
 die Namen der Autoren/Herausgeber. Die Standardeinstellung
 ist 'dashed=true'.

 
 4. Versionsgeschichte
 ---------------------
 
 siehe Datei CHANGES
 
 
 5. Lizenz
 ---------
 
 Dieses Werk darf nach den Bedingungen der LaTeX Project 
 Public Lizenz, entweder Version 1.3 oder (nach Ihrer Wahl) 
 jede spaetere Version, verteilt und/oder verändert werden.
 Die neueste Version dieser Lizenz finden Sie unter
 http://www.latex-project.org/lppl.txt. Version 1.3 (oder 
 eine neuere) ist Teil aller Verteilungen von LaTeX
 Version 2005/12/01 oder spaeter.
 
 Dieses Werk hat den LPPL-Betreuungs-Status 'maintained' 
 (betreut).
 
 Der aktuelle Betreuer dieses Werkes ist Dominik Waßenhoven.
 
 
 6. Dateien
 ----------

 Dieses Werk (bzw. Paket) besteht aus folgenden Dateien:

 * LIESMICH                    % diese Datei, inkl. Lizenz
 * README                      % diese Datei in englisch
 * CHANGES                     % Versionsgeschichte (englisch)
 * historische-zeitschrift.bbx % Bibliographiestil
 * historische-zeitschrift.cbx % Zitierstil
 * historische-zeitschrift.lbx % Sprachdatei 
