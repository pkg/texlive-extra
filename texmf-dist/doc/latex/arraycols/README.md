# The arraycols package - New column types for array and tabular environments


## Presentation

This small package provides new column types for array and tabular environments,
horizontally and vertically centered,
or with adjusted height for big mathematical expressions.

The columns width can be fixed or calculated like in tabularx environments.
Macros for drawing vertical and horizontal rules of variable thickness are also provided.


## Installation

- run LaTeX on arraycols.ins, you obtain the file arraycols.sty,
- if then you run pdfLaTeX on arraycols.dtx you get the file arraycols.pdf which is also in the archive,
- put the files arraycols.sty and arraycols.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
