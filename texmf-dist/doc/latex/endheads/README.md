Readme for endheads

Endheads makes running headers of the form ``Notes to pp.~xx--yy'' for 
users of endnotes.sty. It also enables one to reset the endnotes counter 
by chapter. endheads requires fancyhdr, endnotes, and ifthen. Endheads will also work for memoir class, but in that case it won't require fancyhdr.

This material is subject to the LATEX Project Public License 1.3c.