            README for the `edfnotes' package
         critical annotations to footnote lines
                (C) Uwe Lueck 2011/02/16


`edfnotes.sty' extends `ednotes.sty' so that you can refer 
even to footnotes of the edited work by line numbers, building 
on the accompanying `fnlineno' package in the `lineno' bundle. 

KEYWORDs: critical editions; footnotes

The package file `edfnotes.sty' and the documentation files
`edfnotes.pdf' and `edfnotes.tex' can be redistributed and/or 
modified under the terms of the LaTeX Project Public License; 
either version 1.3c of the License, or any later version, see

    http://www.latex-project.org/lppl.txt

We did our best to help you, but there is NO WARRANTY. 

The `edfnotes' package is author-maintained in the sense of 
this license.

The latest public version of the package is available at 

    http://mirror.ctan.org/macros/latex/contrib/edfnotes/

A TDS version of the package is available as

    http://mirror.ctan.org/install/macros/latex/contrib/edfnotes.tds.zip 

`edfnotes' requires at least `lineno.sty' and `fnlineno.sty' from 
the `lineno' bundle, as well as `ednotes.sty', cf.

    http://ctan.org/pkg/ednotes
    http://ctan.org/pkg/lineno

Please report bugs, problems, and suggestions via 

    http://www.contact-ednotes.sty.de.vu 

