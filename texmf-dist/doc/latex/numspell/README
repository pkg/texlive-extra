numspell.sty package

Copyright 2017-2021 by Tibor Tómács

This package supports the spelling of cardinal and ordinal numbers.
Supported languages are English, French, German, Hungarian, Italian, and Latin.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Tibor Tomacs.

This work consists of the files

README (this file)
numspell.sty (main package file)
numspell-english.sty (English package for numspell)
numspell-french.sty (French package for numspell)
numspell-german.sty (German package for numspell)
numspell-italian.sty (Italian package for numspell)
numspell-latin.sty (Latin package for numspell)
numspell-magyar.sty (Hungarian package for numspell)
numspell.tex and the derived file numspell.pdf (documentation).