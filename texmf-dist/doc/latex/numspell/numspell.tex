%%
%% `numspell.tex' documentation of the numspell package
%%
%% Copyright 2017-2021 by Tibor Tomacs
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainer of this work is Tibor Tomacs.
%%
\documentclass{article}
\usepackage[a4paper]{geometry}
\usepackage[pdfstartview=FitH,colorlinks,allcolors=black,bookmarksnumbered,hyperfootnotes=false]{hyperref}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[magyar,italian,latin,ngerman,french,english]{babel}
\usepackage[symbol]{footmisc}
\usepackage{numspell,listings,xcolor,amsmath,upquote}
\flushbottom
\setlength{\labelsep}{0pt}
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}

\lstnewenvironment{examplelst}{\lstset{
aboveskip=\bigskipamount,
basicstyle=\ttfamily,
backgroundcolor=\color{black!10},
frame=trbl,
framesep=3pt,
columns=fullflexible,
xleftmargin=3.4pt,
xrightmargin=3.4pt, 
keepspaces}}{}

\newcommand{\commandinline}{\lstinline[
literate={<}{{$\langle$}}1{>}{{$\rangle$}}1,
delim={[is][\color{green!50!black}\normalfont\itshape]{!}{!}},
basicstyle=\color{blue!80!black}\ttfamily,
columns=fullflexible,
keepspaces]}

\newcommand{\verbinline}{\lstinline[
literate={<}{{$\langle$}}1{>}{{$\rangle$}}1,
delim={[is][\color{green!50!black}\normalfont\itshape]{!}{!}},
basicstyle=\ttfamily,
columns=fullflexible,
keepspaces]}

\begin{document}

\title{The {\bfseries\sffamily numspell} package\\{\large v1.5 (2021/11/23)}}
\author{Tibor Tómács\\{\normalsize\url{tomacs.tibor@uni-eszterhazy.hu}}}
\date{}
\maketitle

\section{Introduction}
The aim of the \texttt{numspell} package is to spell the cardinal and ordinal numbers from 0 to $10^{66}-1$ (i.e.~maximum 66 digits).

The supported languages are English (British and American), French, German, Hungarian, Italian, and Latin\footnote{Thanks to Keno Wehr for the Latin language module.} (classical, medieval, modern, and ecclesiastic).
The spelling will happen in the current language.

The \texttt{numspell} package requires the services of the \texttt{xstring} and \texttt{iflang} packages.

Load the package as usual, with
\begin{flushleft}
\commandinline|\usepackage{numspell}|
\end{flushleft}

\section{Commands}
\begin{description}
\item\commandinline|\numspell[!<zeros>!]{!<num>!}|\\
Spelling the cardinal number $n=\text{\color{green!50!black}\itshape$\langle$num$\rangle$}\cdot10^{\text{\color{green!50!black}\itshape$\langle$zeros$\rangle$}}$, where $0\leq n\leq 10^{66}-1$. The default value of \commandinline|!<zeros>!| is \texttt{0}. For example
\begin{flushleft}
\verb|\numspell{12000}| $\to$ \numspell{12000}\\
\verb|\numspell[3]{12}| $\to$ \numspell[3]{12}\\
\verb|\numspell[6]{12}| $\to$ \numspell[6]{12}\\
\verb|\numspell[63]{1}| $\to$ \numspell[63]{1}
\end{flushleft}

\item\commandinline|\thenumspell|\\
The \verb|\numspell| stores the result in this command. For example
\begin{flushleft}
\verb|\numspell{12000}; \thenumspell| $\to$ \numspell{12000}; \thenumspell\\
\verb|\numspell{1}; \numspell{2}; \thenumspell| $\to$ \numspell{1}; \numspell{2}; \thenumspell
\end{flushleft}

\item\commandinline|\numspellsave{!<name>!}|\\
It generates the \commandinline|\thenumspell!<name>!| command, which saves the current \verb|\thenumspell|. For example
\begin{verbatim}
\numspell{1};
\numspellsave{MyNum}
\numspell{2};
\thenumspell;
\thenumspellMyNum
\end{verbatim}
\numspell{1};
\numspellsave{MyNum}
\numspell{2};
\thenumspell;
\thenumspellMyNum

\item\commandinline|\numspelldashspace{!<length>!}|\\
In the number spelling, the spaces around the dashes are flexibility for the optimal hyphenation. Its value is \texttt{0pt plus} \commandinline|!<length>!|.
The default value of \commandinline|!<length>!| is \texttt{2pt}. For example
\begin{verbatim}
\selectlanguage{magyar}
\numspell{6512312354762547162546254756}\\[2mm]
\numspelldashspace{10pt}
\numspell{6512312354762547162546254756}
\end{verbatim}
{\selectlanguage{magyar}
\numspell{6512312354762547162546254756}\\[2mm]
\numspelldashspace{10pt}
\numspell{6512312354762547162546254756}}

\item\commandinline|\numspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\numspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\numspell[!<zeros>!]{!<num>!}|\\
\verbinline|\numspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\numspell*{1}
\numspellsave{MyNum}
\numspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
\numspell*{1}
\numspellsave{MyNum}
\numspell*{2}
\thenumspell;
\thenumspellMyNum

\item\commandinline|\Numspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\numspell|, but the first letter will be capital. For example
\begin{flushleft}
\verb|\Numspell{12000}| $\to$ \Numspell{12000}\\
\verb|\Numspell[3]{12}| $\to$ \Numspell[3]{12}\\
\verb|\Numspell[6]{12}| $\to$ \Numspell[6]{12}\\
\verb|\Numspell[63]{1}| $\to$ \Numspell[63]{1}
\end{flushleft}

\item\commandinline|\Numspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\Numspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\Numspell[!<zeros>!]{!<num>!}|\\
\verbinline|\Numspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\Numspell*{1}
\numspellsave{MyNum}
\Numspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
\Numspell*{1}
\numspellsave{MyNum}
\Numspell*{2}
\thenumspell;
\thenumspellMyNum

\item\commandinline|\ordnumspell[!<zeros>!]{!<num>!}|\\
Spelling the ordinal number
$n=\text{\color{green!50!black}\itshape$\langle$num$\rangle$}\cdot10^{\text{\color{green!50!black}\itshape$\langle$zeros$\rangle$}}$,
where $0\leq n\leq 10^{66}-1$. The default value of \commandinline|!<zeros>!| is \texttt{0}. For example
\begin{flushleft}
\verb|\ordnumspell{12000}| $\to$ \ordnumspell{12000}\\
\verb|\ordnumspell[3]{12}| $\to$ \ordnumspell[3]{12}\\
\verb|\ordnumspell[6]{12}| $\to$ \ordnumspell[6]{12}\\
\verb|\ordnumspell[63]{1}| $\to$ \ordnumspell[63]{1}
\end{flushleft}

\item\commandinline|\ordnumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\ordnumspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\ordnumspell[!<zeros>!]{!<num>!}|\\
\verbinline|\ordnumspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\ordnumspell*{1}
\numspellsave{MyNum}
\ordnumspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
\ordnumspell*{1}
\numspellsave{MyNum}
\ordnumspell*{2}
\thenumspell;
\thenumspellMyNum

\item\commandinline|\Ordnumspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\ordnumspell|, but the first letter will be capital. For example
\begin{flushleft}
\verb|\Ordnumspell{12000}| $\to$ \Ordnumspell{12000}\\
\verb|\Ordnumspell[3]{12}| $\to$ \Ordnumspell[3]{12}\\
\verb|\Ordnumspell[6]{12}| $\to$ \Ordnumspell[6]{12}\\
\verb|\Ordnumspell[63]{1}| $\to$ \Ordnumspell[63]{1}
\end{flushleft}

\item\commandinline|\Ordnumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\Ordnumspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\Ordnumspell[!<zeros>!]{!<num>!}|\\
\verbinline|\Ordnumspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\Ordnumspell*{1}
\numspellsave{MyNum}
\Ordnumspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
\Ordnumspell*{1}
\numspellsave{MyNum}
\Ordnumspell*{2}
\thenumspell;
\thenumspellMyNum
\end{description}

\section{Commands for English language}
If the \texttt{english}, \texttt{british}, \texttt{ukenglish} or \texttt{UKenglish} language is active, then the number spelling will happen in British English. But it will be in American English, if the \texttt{american}, \texttt{usenglish} or \texttt{USenglish} language is active.
\begin{description}
\item\commandinline|\numspellUS|\\
Using British English, you can rechange the number spelling to American English by this command.
\item\commandinline|\numspellGB|\\
Using American English, you can rechange the number spelling to British English by this command.
\end{description}

\section{Commands for French language}
The following commands only work, if \texttt{french} language is active.
\begin{description}
\selectlanguage{french}
\item\commandinline|\numspellpremiere|\\
By default, \verb|\ordnumspell{1}| $\to$ \ordnumspell{1},\\
but \verb|\numspellpremiere\ordnumspell{1}| $\to$ {\numspellpremiere\ordnumspell{1}}

\item\commandinline|\numspellpremier| (default)\\
\verb|\numspellpremiere\ordnumspell{1};|\\
\verb|\numspellpremier\ordnumspell{1}|\\[2mm]
{\numspellpremiere\ordnumspell{1}}; {\numspellpremier\ordnumspell{1}}
\end{description}

\section{Commands for Hungarian language}
The following commands only work, if \texttt{magyar} or \texttt{hungarian} language is active.
\begin{description}
\item\commandinline|\anumspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\numspell|, but the number spelling will start with Hungarian definite article. For example
\begin{flushleft}
{\selectlanguage{magyar}
\verb|\anumspell{1}| $\to$ \anumspell{1}\\
\verb|\anumspell{2}| $\to$ \anumspell{2}}
\end{flushleft}

\item\commandinline|\anumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\anumspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\anumspell[!<zeros>!]{!<num>!}|\\
\verbinline|\anumspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\anumspell*{1}
\numspellsave{MyNum}
\anumspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
{\selectlanguage{magyar}
\anumspell*{1}
\numspellsave{MyNum}
\anumspell*{2}
\thenumspell;
\thenumspellMyNum}

\item\commandinline|\Anumspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\anumspell|, but the first letter will be capital.

\item\commandinline|\Anumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\anumspell*|, but the first letter will be capital.

\item\commandinline|\aordnumspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\ordnumspell|, but the number spelling will start with Hungarian definite article. For example
\begin{flushleft}
{\selectlanguage{magyar}
\verb|\aordnumspell{1}| $\to$ \aordnumspell{1}\\
\verb|\aordnumspell{2}| $\to$ \aordnumspell{2}}
\end{flushleft}

\item\commandinline|\aordnumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\aordnumspell|, but the number spelling will not be printed.
In other words, the following two lines are equivalent:
\begin{flushleft}
\verbinline|\aordnumspell[!<zeros>!]{!<num>!}|\\
\verbinline|\aordnumspell*[!<zeros>!]{!<num>!}\thenumspell|
\end{flushleft}
For example
\begin{verbatim}
\aordnumspell*{1}
\numspellsave{MyNum}
\aordnumspell*{2}
\thenumspell;
\thenumspellMyNum
\end{verbatim}
{\selectlanguage{magyar}
\aordnumspell*{1}
\numspellsave{MyNum}
\aordnumspell*{2}
\thenumspell;
\thenumspellMyNum}

\item\commandinline|\Aordnumspell[!<zeros>!]{!<num>!}|\\
It works like \verb|\aordnumspell|, but the first letter will be capital.

\item\commandinline|\Aordnumspell*[!<zeros>!]{!<num>!}|\\
It works like \verb|\aordnumspell*|, but the first letter will be capital.
\end{description}

\section{Commands for Italian language}
The following commands only work, if \texttt{italian} language is active.
\begin{description}
\selectlanguage{italian}
\item\commandinline|\numspellitmasculine| (default)\\
The ordinal numbers will be printed in masculine form. For example\\
\verb|\ordnumspell{1}| $\to$ \ordnumspell{1}

\item\commandinline|\numspellitfeminine|\\
The ordinal numbers will be printed in feminine form. For example\\
\verb|\numspellitfeminine\ordnumspell{1};|\\
\verb|\numspellitmasculine\ordnumspell{1}|\\[2mm]
{\numspellitfeminine\ordnumspell{1}}; {\numspellitmasculine\ordnumspell{1}}
\end{description}

\section{Commands for the Latin language}
The following commands only work, if one of the next languages is active: \texttt{latin}, \texttt{classiclatin}, \texttt{medievallatin}, or \texttt{ecclesiasticlatin}.
\begin{description}
\selectlanguage{latin}
\item\commandinline|\numspelllamasculine| (default)\\
All numbers will be printed in masculine form. For example\\
\verb|\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}|\\[2mm]
\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}

\item\commandinline|\numspelllafeminine|\\
All numbers will be printed in feminine form. For example\\
\verb|\numspelllafeminine|\\
\verb|\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}|\\[2mm]
\numspelllafeminine
\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}

\item\commandinline|\numspelllaneuter|\\
All numbers will be printed in neuter form. For example\\
\verb|\numspelllaneuter|\\
\verb|\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}|\\[2mm]
\numspelllaneuter
\numspell{1}, \numspell{2}, \numspell{200}, \ordnumspell{1}
\end{description}

\section{Examples}

\begin{examplelst}
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[magyar,italian,latin,ngerman,french,english]{babel}
\usepackage{numspell}
\usepackage[group-separator={,}]{siunitx}
\begin{document}
\def\mynum{123456789}
\noindent
In British English the spelling of \num{\mynum} is
\emph{\numspell{\mynum}}.
\par\smallskip\noindent
In American English the spelling of \num{\mynum} is
\foreignlanguage{american}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In French the spelling of \num{\mynum} is
\foreignlanguage{french}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In German the spelling of \num{\mynum} is
\foreignlanguage{ngerman}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Hungarian the spelling of \num{\mynum} is
\foreignlanguage{magyar}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Italian the spelling of \num{\mynum} is
\foreignlanguage{italian}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Latin the spelling of \num{\mynum} is
\foreignlanguage{latin}{\em\numspell{\mynum}}.
\end{document}
\end{examplelst}
\def\mynum{123456789}
\noindent
In British English the spelling of 123,456,789 is
\emph{\numspell{\mynum}}.
\par\smallskip\noindent
In American English the spelling of 123,456,789 is
\foreignlanguage{american}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In French the spelling of 123,456,789 is
\foreignlanguage{french}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In German the spelling of 123,456,789 is
\foreignlanguage{ngerman}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Hungarian the spelling of 123,456,789 is
\foreignlanguage{magyar}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Italian the spelling of 123,456,789 is
\foreignlanguage{italian}{\em\numspell{\mynum}}.
\par\smallskip\noindent
In Latin the spelling of 123,456,789 is
\foreignlanguage{latin}{\em\numspell{\mynum}}.

\begin{examplelst}
\documentclass{article}
\usepackage{numspell}
\newcounter{mycount}
\begin{document}
The
\makeatletter
\@whilenum\value{mycount}<51
\do{\ordnumspell{\themycount}\stepcounter{mycount},\ }\dots
\makeatother
\end{document}
\end{examplelst}
\makeatletter
\newcounter{mycount}
The
\@whilenum\value{mycount}<51
\do{\ordnumspell{\themycount}\stepcounter{mycount},\ }\dots
\makeatother

\begin{examplelst}
\documentclass{article}
\usepackage{numspell}
\newcounter{mycount}
\def\themycount{\numspell{\arabic{mycount}}}
\begin{document}
\Numspell{0},
\makeatletter
\@whilenum\value{mycount}<30
\do{\stepcounter{mycount}\themycount,\ }\dots
\makeatother
\end{document}
\end{examplelst}
\def\themycount{\numspell{\arabic{mycount}}}
\setcounter{mycount}{0}
\makeatletter
\Numspell{0},
\@whilenum\value{mycount}<30
\do{\stepcounter{mycount}\themycount,\ }\dots
\makeatother

\section{Limitations}
Do not use the \verb|\numspell|, \verb|\numspell*|, \verb|\Numspell|, \verb|\Numspell*|, etc.\ commands inside sectioning commands and \verb|\MakeUppercase|.
An example for the illustration of the problem:
\begin{examplelst}
\documentclass{article}
\usepackage{hyperref}
\usepackage{numspell}
\pagestyle{headings}
\begin{document}
\section{The \ordnumspell{123} factor}
\MakeUppercase{\numspell{123}}
\newpage
Text
\end{document}
\end{examplelst}

The following mistakes occur:
\begin{enumerate}
\setlength{\labelsep}{5pt}
\item On the page 1: ``\numspell{123}''\\
Required: \numspell*{123}\MakeUppercase{``\thenumspell''}
\item On the heading: \emph{``THE \ordnumspell{123} FACTOR''}\\
Required: \ordnumspell*{123}\MakeUppercase{\em``The \thenumspell\ factor''}
\item On the pdf bookmark: ``The 123 factor''\\
Required: ``The \ordnumspell{123} factor''
\end{enumerate}

\subsubsection*{The solution}

\begin{examplelst}
\documentclass{article}
\usepackage{hyperref}
\usepackage{numspell}
\pagestyle{headings}
\begin{document}
\ordnumspell*{123}
\section{The \thenumspell\ factor}
\numspell*{123}
\MakeUppercase{\thenumspell}
\newpage
Text
\end{document}
\end{examplelst}

\end{document}