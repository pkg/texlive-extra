# The phfsvnwatermark package

Add a watermark on each page with version control information from SVN.

This package allows you to add version control information as a gray watermark
on each page of your document. The SVN info is read from keyword tags such as
$Id$, $Date$ and $Author$ via the `svn` or `svn-multi` packages.


# Documentation

Run 'make sty' to generate the style file and 'make pdf' to generate the package
documentation. Run 'make' or 'make help' for more info.
