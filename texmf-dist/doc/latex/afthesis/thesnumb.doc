%the examples below show how to create entries in a .bib file for your
%thesis. Use the thesnumb bibliography style as in \bibliographystyle{thesnumb}
%to get the standard AFIT bibliography style as shown in the ``Style Guide
%for Theses and Dissertations."  For the most part the standard types and
%fields are available.  Additions include: 
% New types: govpub  --  for government publications like regs, laws, etc.
%              Required fields: organization, title, year
%              Optional fields: type, number, publisher, address, month, note
%            brochure -- for company brochures, ads, etc which may be
%                        numbered.  The standard manual type is similar.
%              Required fields: title
%              Optional fields: author, organization, type, number, address,
%                               month, year, note
% New fields: dticnumber -- DTIC number (e.g., AD-A1111) added as an optional
%                           field to mastersthesis, phdthesis, techreport
%             designator -- AFIT thesis designator added as an optional
%                           field to mastersthesis
%             umfnumber  -- University Microfilm Number added as an optional
%                           field to phdthesis
% NOTE: this style only works with BIBTEX version .99a or higher
% QUESTION or SUGGESTIONS: email to mroth (Mark Roth, AFIT/ENG, 53576)

% Most of these examples taken from the Style Guide:

@book{book-simple-entry,
author="Peter S. Maybeck",
title="Stochastic Models, Estimating, and Control",
publisher="Academic Press",
address="New York",
year=1979
}

@book{book-two-authors,
author="John J. D'Azzo and Constantine H. Houpis",
title="Linear Control Systems Analysis and Design",
publisher="McGraw-Hill Book Company",
address="New York",
year=1981
}

@book{book-edition,
author="Harold B. Kepler",
title="Basic Graphical Kinematics",
edition="Second",
publisher="McGraw-Hill Book Company",
address="New York",
year=1973
}

@book{book-three-or-more-authors,
author="John A. Muller and others",  
title="Development and Validation of Effective Composition Instruments",
publisher="Jardin Publishing Company",
address="New York",
year=1983
}

@incollection{chapter-in-an-anthology,
author="Rehg, Virgil", 
title="Application of Quality Circles",
booktitle="New Directions in Effective Management",
volume=3,
editor="Juanita Klosterman",
publisher="Kenny Press",
address="Boston",
year=1980
}

@inbook{signed-foreword,
author="Triscari, Capt Thomas, Jr. and Maj Ronald W. Hitzelberger", 
title="New Directions in Systems Management",
type="Forward",
chapter="by Richard T. Taliferro",
publisher="Garden State Publishers",
address="River Edge NJ",
year=1982
}

@article{periodical-simple-entry,
author="Donn G. Shankland",
title="A Numerically Efficient Procedure for the Theil-van de Panne
       Quadratic Programming Methods",
journal="Journal of Optimization Theory and Application",
volume=31,
pages="117-123",
month=may,
year=1980
}

@article{periodical-two-authors,
author="Harrington, Maj Thomas C. and William A. Fischer",
title="Portfolio Modeling in Multiple-Criteria Situations Uncertainty",
journal="Decision Sciences",
volume=4,
pages="171-173",
month=jan,
year=1980
}

@article{periodical-three-or-more--authors,
author="Ernest A. Dorko and others",
title="Kinetic Studies of Kitan Red-S Photodecomposition Under Continuous
       Working and Flash Photolytic Conditions",
journal="Journal of Photochemistry",
volume=12,
pages="345-356",
month=apr,
year=1980
}

@article{periodical-nonconsective-pages,
author="Freda L. Stohrer",
title="Passive Voice Structures",
journal="Journal of Neurosemantics",
volume=23,
pages="58-64+",
month=jul,
year=1983
}

@article{unsigned-article-1,
title="Fifty and a Hundred Years Ago Today",
key="Fifty",
journal="Quarterly Review of Economics and Business",
volume=22,
pages="83-85",
month="Summer",
year=1982
}

@article{unsigned-article-2,
title="The View From the Top",
key="View",
journal="Management Quarterly",
volume=8,
pages="12-14",
month="Winter",
year=1986
}

@govpub{report,
organization="Bureau of the Census",
title="Population Estimates and Projections",
type="Report Series",
number="P25; No. 108",
address="Washington",
publisher="Government Printing Office",
year=1980
}

@govpub{regulation,
organization="Department of the Air Force",
title="Administrative Communications: Air Force Standard Functional
       Address System",
type="AFR",
number="10-6",
address="Washington",
publisher="HQ USAF",
month="22 " # jan,
year=1982
}

@govpub{manual,
organization="Department of the Air Force",
title="USAF Formal Schools Catalog",
type="AFM",
number="50-5",
address="Washington",
publisher="HQ USAF",
month="1 " # jun,
year=1980
}

@govpub{directive,
organization="Department of Defense",
title="Distribution Statements of Technical Documents",
type="DOD Directive",
number="5200.20",
address="Washington",
publisher="Government Printing Office",
month="26 " # mar,
year=1971
}

@govpub{law,
organization="U. S. Congress",
title="National Security Act of 1979",
type="Public Law",
number="No. 193, 96th Congress, 1st Session",
address="Washington",
publisher="Government Printing Office",
year=1979
}

@govpub{hearing,
organization="U. S. Congress, House of Representatives, Committee on the 
	      Judiciary, Subcommittee on Monopolies",
title="Hearings on Restoring Effective Enforcement of Antitrust Law",
type="Hearing,",
number="96th Congress, 2nd Session, 1979",
address="Washington",
publisher="Government Printing Office",
year=1979
}

@mastersthesis{thesis,
author="Carpenter, Capt Dennis M.",
title="Relating Expected Inventory Backorders of Safety Stock Investment 
       Levels",
designator="AFIT/GIM/LSM/86S-153",
school=log,
address=wpafb,
month=sep,
year=1981,
dticnumber="AD-A1103970"
}

@phdthesis{dissertation-afit,
author="Neumann, Capt David W.",
title="Observation and Analysis of LiCa and MiMg Excimers",
school=en,
address=wpafb,
month=jun,
year=1980,
dticnumber="AD-A1113137",
umfnumber="ON7229905"
}

@phdthesis{dissertation-civilian,
author="Miro, Donald J.",
title="A Comparative Evaluation of Relaxation Training Strategies
       Using EMG Biofeedback",
school="Loyola University of Chicago",
address="Chicago IL",
year=1981,
umfnumber="ON8119983"
}

@inproceedings{conference-paper,
author="DeWispelare, Capt Aaron R.",
title="Algorithm Efficiency in Generating Nondenominated Solution Sets",
booktitle="Proceedings of the IEEE 12th Annual Symposium in System Theory",
pages="218-222",
publisher="IEEE Press",
address="New York",
year=1980
}

@techreport{research-report,
author="Morton F. Blair and Milburn J. Werle",
title="The Influence of Freestream Turbulance in the Zero Pressure Gradient
       Fully Turbulent Boundary Layer: Interim Report, {\rm 1 June 1982--
       1 June 1983}",
type="Contract",
number="F4962078C00064",
address="East Hartford CN",
institution="Adkins Research Center",
month=sep,
year=1982,
dticnumber="AD-A1913094"
}

@brochure{company-brochure-1,
organization="Dynocanque Task Group",
title="Procedure for Retrofit of the Dynocanque II",
type="Dealer Maintenance Bulletin",
number="8-48",
address="Engineering Department, Hodges Manufacturing Company,
	 Philadelphia PA",
month=jul,
year=1982
}

@brochure{company-brochure-2,
title="Honing Supplies",
key="Hon",
type="Product Catalog",
number="X-SP-50502",
address="Sunnen Products Company, St. Louis MO",
year="undated"
}

@unpublished{speech,
author="Antonellis, Kevin B., Assistant Secretary of State-Middle East",
title="A Riddle Wrapped in an Enigma",
note="Address to AFIT students. " # afit # ", " # wpafb,
month="8 " # jan,
year=1985
}

@misc{class-lectures-or-handouts,
author="Dean, William A.",
howpublished="Class handout distributed in SYS 228, Basic Configuration
	      Management. " # log # ", " # wpafb,
month=jul,
year=1986
}

@misc{correspondence,
author="Murray, Doris, President",
howpublished="Personal Correspondence. Telemetrodynamics Corporation,
	      Dayton OH",
month="1 " # apr,
year=1986
}

@unpublished{electronic-message,
author="{HQ USAFE}", comment="prevent last name, first name reversal",
title="Advanced Contract Administration and Contract Law Site",
note="Electronic Message",
month="151500Z, 20 " # may,
year=1984
}

@misc{telephone-interview,
author="Smith, C. Doss, Vice President for Sales",
howpublished="Telephone interview. Telemetrodynamics Corporation,
	      Dayton OH",
month="9 " # apr,
year=1986
}

@misc{personal-interview-1,
author="May, Marian, Vice President for Manufacturing",
howpublished="Personal interview. Telemetrodynamics Corporation,
	      Dayton OH",
month="10 " # may,
year=1986
}

@misc{personal-interview-2,
author="Elrod, Lt Col William B., {Chief, Quality Assurance Division}",
howpublished="Personal interviews. HQ AFLC, " # wpafb,
month="8 " # jul # " through 9 " # sep,
year=1982
}

@misc{memorandum,
author="{Aeronautical Systems Division, Air Force Systems Command}",
howpublished="Memorandum of Agreement with Air Force Contract Maintenance
	      Center. " # wpafb,
month="18 " # sep,
year=1985
}

@misc{contract,
author="{Aeronautical Systems Division, Air Force Systems Command}",
howpublished="Contract F35980-81C-0396 with Northrup Corporation. " # wpafb,
month="12 " # oct,
year=1985
}

@unpublished{report-1,
author="McNichols, Lt Col Charles W. and T. Roger Manley",
title="Quality of Life in the United States Air Force, 1984 Quick Look Report",
note="Report to DCS Personnel. HQ USAF, Washington DC",
month=jun,
year=1984
}

@techreport{report-2,
author="Franke, Col Milton P.",
title="The Effects of High Altitude Ablation on Air Force Readiness",
type="Unpublished report",
number="No. 4328",
institution="Air War College",
address="Maxwell AFB AL",
year=1983
}

@misc{report-3,
author="Wyte, Charles E., Director of Production Engineering Activity",
title="{\em Schedule for 1982 Changeover in Assembly Plants\/}",
howpublished="Report to chairmen of GMC automotive divisions.
              Fisher Body Division, General Motors Corporation, Warren MI",
month="8 " # jul,
year=1986
}

@misc{tv-progam,
author="Doe, John J., {Col, USAF}",
title="Comments broadcast on {\em The McNeill-Lehrer News Hour\/}",
howpublished="Public Broadcasting System",
month="7 " # jun,
year=1986
}

