This is the README file for gastex package.

GasTeX: Graphs and Automata Simplified in TeX

GasTeX is a set of LaTeX macros which allow to draw very easily graphs,
automata, nets, diagrams, etc...  under the picture environment of LaTeX.

Author: 
-------
Paul Gastin <http://www.lsv.fr/~gastin>

Copyright: 2021 by Paul Gastin
----------
This program can be redistributed and/or modified under the terms
of the LaTeX Project Public License Distributed from CTAN:
https://www.ctan.org/license/lppl1.3

Installation:
-------------
In order to use gastex you only need two files:
- gastex.sty which contains the definition of all the LaTeX macros. This file
could be in your working folder or where other .sty files are, e.g.,
  .../texmf-dist/tex/latex/gastex/gastex.sty
  or 
  .../texmf-local/tex/latex/gastex/gastex.sty
- gastex.pro which contains all the postscript procedures.  This file
could be in your working folder or where other .pro files are, e.g.,
  .../texmf-dist/dvips/gastex/gastex.pro
  or
  .../texmf-local/dvips/gastex/gastex.pro

Usage: latex+dvips(+ps2pdf) filename.tex
------
- Compile the source file with latex (not pdflatex) and generate the ps file 
with dvips. One may then use ps2pdf to get a pdf file.

Usage: pdflatex --shell-escape filename.tex
------
- Requires the package auto-pst-pdf.  

Remarks:
--------
- gastex mainly generates postscript code so the pictures cannot be seen
with a dvi-viewer.  One has to generate a ps file (latex+dvips) or a pdf
file (latex+dvips+ps2pdf or pdflatex --shell-escape) and view it with a
ps-viewer or a pdf-viewer.

Documentation and examples:
---------------------------
- See http://www.lsv.fr/~gastin/gastex/index.html
- See also gastex-doc.tex for a quick startup guide on how to use gastex
with pdflatex and a few examples.
- Comments in the file gastex.sty also provide documentation for all macros.

Enjoy!