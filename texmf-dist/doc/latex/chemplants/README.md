# The chemplants package

Copyright 2018-2021 Elia Arnese Feffin.

Contact: <elia24913@me.com>.

Current version: 0.9.9 - 2019/09/25.

## Abstract

The `chemplants` package offers tools to draw simple or barely complex schemes of
chemical processes. The package defines several standard symbols and styles to
draw process units and streams. The guiding light of the package is the UNICHIM
regulation.

All of the symbols and styles are defined using tools of the `tikz` package, thus
a basic knowledge of the logic of this powerful tool is required to profitably use
`chemplants`.

## Licensing

The `chemplants` package is covered by the LaTeX Project Public License (LPPL),
version 1.3c or later. The latest version can be found at
<http://www.latex-project.org/lppl.txt>.

## Documentation

The documentation of the package can be found in `chemplants-doc.pdf`, provided
together with its source code. Changes to the package facilities are collected
in `chemplants-changes.pdf`, which is provided together with its source code.
However, notice that versions prior to 0.9.8 were never officially published.
