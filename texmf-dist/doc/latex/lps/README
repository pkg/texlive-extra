LaTeX Class: lps

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Files: lps.ins (installation), lps.dtx (documented source) 

Author: Gustavo Cevolani, comments to g.cevolani@gmail.com

Description: LaTeX class style (lps.cls) for the Logic & Philosophy of Science Journal (University of Trieste, Italy), http://www.univ.trieste.it/~episteme/

Class homepage: http://www.univ.trieste.it/~episteme/latex_L&PS.htm

License: This material is subject to the LaTeX Project Public License. See http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for the details of that license.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

* Brief description of the class:

The lps class modifies the standard \LaTeX2e article class in order to comply with the Logic and Philosophy of Science journal style specifications.


* Quick use:

  - run LaTeX on the file lps.ins to create the lps.cls class file and the lpstemplate.tex sample file;

  - run (PDF)LaTeX on the file lps.dtx to obtain the documentation for the class;
  
  - run (PDF)LaTeX (two times) on the file lps-template.tex to check if you have all packages needed and if all works fine;
  
  - rename lps-template.tex and modify it to create your own article.

  
* Installation:

  - move the file lps.cls in some directory searched by LaTeX, then refresh the file name database.

  Example of installation for a MiKTeX 2.5 distribution on a Windows system:

    1. move lps.cls to your \localtexmf folder, for instance in \localtexmf\tex\latex\lps\ (create the directories that do not exist);
    2. move the documentation files (lps.dvi or lps.pdf) to some folder of your choice (for instance: \localtexmf\tex\doc\latex\lps\);
    3. open the MiKTeX Options window (Start>Programs>MikTeX>MiKTeX Options) and refresh the file name database ("Refresh Now" button).
