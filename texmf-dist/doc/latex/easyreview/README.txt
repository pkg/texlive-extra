%% readme.txt, jmamatos
%% Copyright 2013- by jmamatos
%%
%% This work may be distributed and/or modified under the
%% conditions of the \LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.\LaTeX-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of \LaTeX
%% version 2005/12/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is jmamatos, led
%% by Jody Maick Matos. Further information are available on 
%% https://github.com/jmamatos/easyReview/
%%
%% 
%%
%% Revision history:
%%
%% 2015/09/11 jmamatos
%% 
%%

The Easy Review Package: Reviewing TeX documents in an easy way
author: Jody Maick Matos (jody.matos@inf.ufrgs.br)
2015/09/11

The easyReview package provide a way to review (or perform editorial process) in LaTeX. You can use the provided commands to claim attention in different ways to part of the text, or even to indicate that a text was added, needs to be removed, needs to be replaced and add comments to the text.


Software License:
The files easyReview.dtx, easyReview.ins, and all files generated from these files are referred to as ‘the Easy Review package’ or simply ‘the package’.

  Copyright: The Easy Review package is copyright 2013- by jmamatos (jody.matos@inf.ufrgs.br).
  
  Distribution and modification: This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or (at your option) any later version. The latest version of this license is in http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later. This work has the LPPL maintenance status `maintained'. The Current Maintainer of this work is jmamatos, led by Jody Maick Matos. Further information are available on https://github.com/jmamatos/easyReview/.
 
  Contacts: Read Section "Troubleshooting" (in the documentation files) on how to submit a bug report. Send all other comments and ideas to jody.matos@inf.ufrgs.br using easyReview as part of the subject.
  
