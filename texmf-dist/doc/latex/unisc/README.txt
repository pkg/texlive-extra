-------------------------------------------------------
Package:     unisc
Version:     v0.2 (29 April, 2022)
Author:      निरंजन
Description: Unicode small caps with Xe/LuaLaTeX.
Repository:  https://puszcza.gnu.org.ua/projects/unisc
License:     GPLv3+, GFDLv1.3+
-------------------------------------------------------


