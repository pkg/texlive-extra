                     The SIstyle package
              for SI units and number typesetting

                     Author:   Danie Els
                     Copyright (c) 2004-2008 Danie Els
                     Licence:  LaTeX Project Public License

DESCRIPTION
===========

  The SIstyle package typeset SI units and numbers according
  to the ISO requirements. Care is taken with font requirements.
  The emphasis is on ease of use, but it is the users
  responsibility to know the rules and how to use SI units.

      e.g.:  \SI{1}{N}=\SI{1}{kg.m/s^2}



INSTALLATION
============

   The most basic installation is to copy sistyle.sty to your
   working directory or to a directory where TeX can find it.

   For a full installation, copy the following files to you
   (local) TEXMF tree and into the specified directories.

    -- STYLE FILES --

       <texmf>\tex\latex\SIstyle\sistyle.sty

       The style file was obtain by running "latex sistyle.ins"

   -- SOURCE FILES --

       <texmf>\source\latex\SIstyle\sistyle.dtx
                                    sistyle.ins
                                    fig1.(mps/eps)
                                    fig2.(mps/eps)

                                    graphs_src\fig1.mp
                                    graphs_src\fig1.mp
                                    graphs_src\MPfig.bat
                                    graphs_src\readme_figs.txt
                                    graphs_src\unitstep.m
                                    graphs_src\grphset.m

    -- USER DOCUMENTATION --

       <texmf>\doc\latex\SIstyle\SIstyle-2.3a.pdf

       The user manual for SIstyle package was produced by executing:

          pdflatex sistyle.dtx
          pdflatex sistyle.dtx
          makeindex -s gglo.ist -o sistyle.gls sistyle.glo
          makeindex -s gind.ist -o sistyle.ind sistyle.idx
          pdflatex sistyle.dtx
          pdflatex sistyle.dtx
