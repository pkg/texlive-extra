%!PS
%%BoundingBox: 67 280 302 442 
%%HiResBoundingBox: 67.13551 280.6355 301.02805 441.5 
%%Creator: MetaPost 0.901
%%CreationDate: 2006.12.14:1208
%%Pages: 1
%*Font: ec-lmr10 9.96265 9.96265 2c:8000008000800544e98
%*Font: cmmi10 9.96265 9.96265 30:e8a0000000000000088
%*Font: ts1-lmr10 9.96265 9.96265 b5:8
%*Font: cmr10 9.96265 9.96265 6d:82
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0.5 dtransform truncate idtransform setlinewidth pop [] 0 setdash
 0 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 114.5 302.584 moveto
114.5 441.25 lineto
290.25 441.25 lineto
290.25 302.584 lineto
114.5 302.584 lineto stroke
186.18599 282.5727 moveto
(Time,) ec-lmr10 9.96265 fshow
214.96638 282.5727 moveto
(t) cmmi10 9.96265 fshow
gsave [0 1 -1 0 73.9273 336.8714 ] concat 0 0 moveto
(Displacemen) ec-lmr10 9.96265 fshow grestore
gsave [0 1 -1 0 73.9273 391.306 ] concat 0 0 moveto
(t,) ec-lmr10 9.96265 fshow grestore
gsave [0 1 -1 0 73.9273 401.26869 ] concat 0 0 moveto
(x) cmmi10 9.96265 fshow grestore
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 114.5 302.584 moveto
114.5 304.334 lineto stroke
newpath 114.5 441.25 moveto
114.5 439.5 lineto stroke
106.2126 295.2945 moveto
(0) cmmi10 9.96265 fshow
112.8547 295.2945 moveto
(\265) ts1-lmr10 9.96265 fshow
118.8577 295.2945 moveto
(s) cmr10 9.96265 fshow
newpath 143.833 302.584 moveto
143.833 304.334 lineto stroke
newpath 143.833 441.25 moveto
143.833 439.5 lineto stroke
newpath 173.083 302.584 moveto
173.083 304.334 lineto stroke
newpath 173.083 441.25 moveto
173.083 439.5 lineto stroke
164.7956 295.2945 moveto
(4) cmmi10 9.96265 fshow
171.4377 295.2945 moveto
(\265) ts1-lmr10 9.96265 fshow
177.44069 295.2945 moveto
(s) cmr10 9.96265 fshow
newpath 202.417 302.584 moveto
202.417 304.334 lineto stroke
newpath 202.417 441.25 moveto
202.417 439.5 lineto stroke
newpath 231.667 302.584 moveto
231.667 304.334 lineto stroke
newpath 231.667 441.25 moveto
231.667 439.5 lineto stroke
223.37961 293.1636 moveto
(8) cmmi10 9.96265 fshow
230.02171 293.1636 moveto
(\265) ts1-lmr10 9.96265 fshow
236.0247 293.1636 moveto
(s) cmr10 9.96265 fshow
newpath 261 302.584 moveto
261 304.334 lineto stroke
newpath 261 441.25 moveto
261 439.5 lineto stroke
newpath 290.25 302.584 moveto
290.25 304.334 lineto stroke
newpath 290.25 441.25 moveto
290.25 439.5 lineto stroke
279.47194 295.2945 moveto
(12) cmmi10 9.96265 fshow
291.09544 295.2945 moveto
(\265) ts1-lmr10 9.96265 fshow
297.09834 295.2945 moveto
(s) cmr10 9.96265 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 114.5 302.584 moveto
116.25 302.584 lineto stroke
newpath 290.25 302.584 moveto
288.5 302.584 lineto stroke
88.2534 301.43924 moveto
(0) cmmi10 9.96265 fshow
94.89551 301.43924 moveto
(mm) cmr10 9.96265 fshow
newpath 114.5 322.417 moveto
116.25 322.417 lineto stroke
newpath 290.25 322.417 moveto
288.5 322.417 lineto stroke
newpath 114.5 342.167 moveto
116.25 342.167 lineto stroke
newpath 290.25 342.167 moveto
288.5 342.167 lineto stroke
80.5047 340.99084 moveto
(0:4) cmmi10 9.96265 fshow
94.8956 340.99084 moveto
(mm) cmr10 9.96265 fshow
newpath 114.5 362 moveto
116.25 362 lineto stroke
newpath 290.25 362 moveto
288.5 362 lineto stroke
newpath 114.5 381.834 moveto
116.25 381.834 lineto stroke
newpath 290.25 381.834 moveto
288.5 381.834 lineto stroke
80.5047 378.6238 moveto
(0:8) cmmi10 9.96265 fshow
94.8956 378.6238 moveto
(mm) cmr10 9.96265 fshow
newpath 114.5 401.667 moveto
116.25 401.667 lineto stroke
newpath 290.25 401.667 moveto
288.5 401.667 lineto stroke
newpath 114.5 421.417 moveto
116.25 421.417 lineto stroke
newpath 290.25 421.417 moveto
288.5 421.417 lineto stroke
80.5047 419.27225 moveto
(1:2) cmmi10 9.96265 fshow
94.8956 419.27225 moveto
(mm) cmr10 9.96265 fshow
newpath 114.5 441.25 moveto
116.25 441.25 lineto stroke
newpath 290.25 441.25 moveto
288.5 441.25 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 145.25 398.917 moveto
146.75 403.167 lineto
148.167 407 lineto
149.667 410.583 lineto
151.083 413.75 lineto
152.583 416.5 lineto
154.083 419 lineto
155.5 421.083 lineto
157 422.833 lineto
158.416 424.25 lineto
159.916 425.333 lineto
161.333 426.083 lineto
162.833 426.583 lineto
164.333 426.75 lineto
165.749 426.666 lineto
167.249 426.416 lineto
168.666 425.916 lineto
170.166 425.25 lineto
171.582 424.333 lineto
173.082 423.333 lineto
174.582 422.167 lineto
175.999 420.917 lineto
177.499 419.583 lineto
178.915 418.167 lineto
180.415 416.667 lineto
181.832 415.167 lineto
183.332 413.667 lineto
184.832 412.167 lineto
186.248 410.667 lineto
187.748 409.167 lineto
189.165 407.75 lineto
190.665 406.334 lineto
192.081 405.084 lineto
193.581 403.834 lineto
195.081 402.584 lineto
196.498 401.501 lineto
197.998 400.501 lineto
199.414 399.584 lineto
200.914 398.751 lineto
202.414 398.001 lineto
203.831 397.418 lineto
205.331 396.834 lineto
206.747 396.334 lineto
208.247 396.001 lineto
209.664 395.668 lineto
211.164 395.501 lineto
212.664 395.335 lineto
214.08 395.252 lineto
215.58 395.252 lineto
216.997 395.335 lineto
218.497 395.418 lineto
219.913 395.585 lineto
221.413 395.835 lineto
222.913 396.085 lineto
224.33 396.335 lineto
225.83 396.668 lineto
227.246 397.001 lineto
228.746 397.334 lineto
230.163 397.751 lineto
231.663 398.084 lineto
233.163 398.501 lineto
234.579 398.834 lineto
236.079 399.25 lineto
237.496 399.584 lineto
238.996 400 lineto
240.412 400.333 lineto
241.912 400.667 lineto
243.412 401 lineto
244.829 401.333 lineto
246.329 401.583 lineto
247.745 401.833 lineto
249.245 402.083 lineto
250.662 402.333 lineto
252.162 402.5 lineto
253.662 402.666 lineto
255.078 402.833 lineto
256.578 402.916 lineto
257.995 402.999 lineto
259.495 403.083 lineto
260.995 403.166 lineto
262.411 403.249 lineto
263.911 403.249 lineto
265.328 403.249 lineto
266.828 403.249 lineto
268.244 403.249 lineto
269.744 403.166 lineto
271.244 403.166 lineto
272.661 403.083 lineto
274.161 402.999 lineto
275.577 402.916 lineto
277.077 402.833 lineto
278.494 402.75 lineto
279.994 402.666 lineto
281.494 402.583 lineto
282.91 402.5 lineto
284.41 402.333 lineto
285.827 402.25 lineto
287.327 402.167 lineto
288.743 402.083 lineto
290.243 402 lineto stroke
newpath 114.5 302.584 moveto
116 303.084 lineto
117.417 304.417 lineto
118.917 306.667 lineto
120.333 309.667 lineto
121.833 313.25 lineto
123.25 317.417 lineto
124.75 322.083 lineto
126.25 327.167 lineto
127.666 332.583 lineto
129.166 338.25 lineto
130.583 344.083 lineto
132.083 349.999 lineto
133.499 355.916 lineto
134.999 361.916 lineto
136.499 367.749 lineto
137.916 373.499 lineto
139.416 379.082 lineto
140.832 384.416 lineto
142.332 389.582 lineto
143.832 394.415 lineto
145.249 398.915 lineto stroke
showpage
%%EOF
