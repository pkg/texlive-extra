%%%% 
%%%
%%
%	DOCUMENTATION FOR THE ling-macros PACKAGE
%	by ANDREW MCKENZIE (andrew.mckenzie@ku.edu)
%	Version 2.2 
%	2016-10-12
%
% %% not to be confused with lingmacros.sty, a part of tree-dvips.sty
%%
%%%  Thanks to Lydia Newkirk for the name suggestion.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%%%%  %% ling-macros-doc.tex
%% Copyright 2016 Andrew McKenzie (andrew.mckenzie@ku.edu)
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Andrew McKenzie.
%
% This work consists of the files ling-macros.sty and ling-macros-doc.tex


\documentclass{article}
 
\usepackage{tipa}									% phonetic alphabet package
\usepackage{xcolor}
\usepackage{parskip}
 \definecolor{KUBlue}{RGB}{0,34,180}						% Kansas University Blue
 \definecolor{KUCrimson}{RGB}{232,0,13}					% Crimson (Rock Chalk!)
\usepackage[colorlinks=true,urlcolor=KUBlue]{hyperref}	
\usepackage{array}
\usepackage[]{ling-macros}								% uses the ling-macros package

\newcommand{\structure}{\color{KUBlue}}				% highlighting
\newcommand{\cbl}{{\color{green!60!black}{\{}}}			% green left curly braces
\newcommand{\cbr}{{\color{green!60!black}{\}}}}			% green right curly braces
\newcommand{\mathbit}[1]{{\color{green!60!black}{\$}}#1{\color{green!60!black}{\$}}}  % for typing things between math mode delimiters
\newcommand{\comm}[2]{{\rmfamily{\structure{$\backslash$#1}}\cbl#2\cbr}} % for typing commands. first arg is command, second is argument
\newcommand{\comopt}[3]{{\rmfamily{\structure{$\backslash$#1}}[#2]\cbl#3\cbr}} % for typing commands. first arg is command, second is argument
\newcommand{\decla}[1]{{\rmfamily{\structure{$\backslash$#1}}}}  %for typing declarations
\newcommand{\lat}[1]{{\color{KUBlue}\bfseries\ttfamily #1}}		% for typing names of LaTeX objects 


\title{The \lat{ling-macros} package \\ \Large Version 2.2}
\author{Andrew McKenzie \\ \href{mailto:andrew.mckenzie@ku.edu}{andrew.mckenzie@ku.edu} \\ \href{http://people.ku.edu/~a326m085}{http://people.ku.edu/$\sim$a326m085}
}
\date{October 12, 2016}

\begin{document}
\newcommand{\commb}[1]{\comm{#1}{\ldots}}

\maketitle

\setcounter{tocdepth}{2}
\tableofcontents

\section{About \texttt{ling-macros}}

The \lat{ling-macros} package is designed to allow easier use of formal symbols used in formal linguistics, especially in formal linguistics. The set arose from the macros that I have been using over the years for papers and class handouts.  Suggestions and additions are welcome.  Note: This is not the same package as \lat{ling{}macros}, which is part of \lat{tree-dvips}.

To call the package, type \comm{usepackage}{ling-macros} in the preamble of your document. The package has the following options:

\begin{tabular}{>{\bfseries\ttfamily}l p{3in}}
text-semantics & typesets formal semantic expressions in upright fashion rather than italicized or math fashion\\
shortspace & removes space before, after, and between examples in \lat{exe} environment (see sect.\ \ref{gbr3}) \\
leftflush  & pulls examples to the left edge in \lat{exe} environment  (see sect.\ \ref{gbr3}) \\
abstract  & combines {\bfseries\ttfamily shortspace} and {\bfseries\ttfamily leftflush}; useful for abstracts (see sect.\ \ref{gbr3})  \\
\end{tabular} 

The \lat{ling-macros} package calls for the following packages: \lat{stmaryrd}, \lat{ulem}, \lat{amssymb}, \lat{upgreek}, \lat{gb4e}, \lat{relsize}, and \lat{pbox}.   Also included are call commands for phonetic writing (\lat{tipa}) and tree structures (\lat{qtree}), but they are commented out to prevent option clashes (look for {\color{red}\%$\backslash${RequirePackage}\{qtree\}}, etc., if you want to turn them on for your version).

Using \lat{ling-macros} is pretty straightforward, since it is just a series of macros. The macros are organized by module of the grammar.

   

\section{General linguistics}

\subsection{Ordinary macros}
The following macros are used pretty generally throughout the subfields.

\begin{tabular}{>{\bfseries}l p{2in} l c }
 \mdseries command & purpose & in source file  & in print \\\hline
\decla{nl} &  null symbol & \decla{nl} & \nl  \\[3pt]
\comm{m}{\ldots} & small caps for morpheme gloss & \comm{m}{acc.pl} & \m{acc.pl} \\
\comm{mc}{\ldots} & small caps for morpheme gloss & \comm{mc}{acc.pl} & \m{acc.pl} \\
\decla{mb} & wider hyphen for morpheme breaks  (can be changed globally) & ant\decla{mb} s & ant\mb s  \\
\commb{ol} & object language text & \comm{ol}{clermontois} & \ol{clermontois} \\
\commb{alert} & highlights parts of examples & work\comm{alert}{horse}  & work\alert{horse}  \\
\commb{term} & highlights terminology being introduced &  \comm{term}{causative} & \term{causative} \\
 \\
\comm{ix}{\ldots} &   subscript index (upright text with \lat{text-semantics} option)  & Bill\comm{ix}{j}, x\comm{ix}{cat} & Bill\ix{j}, x\ix{cat} \\[3pt]
\comm{ux}{\ldots} &   subscript index with upright text (no matter what) & Bill\comm{ux}{j}, x\comm{ux}{cat} & Bill\ux{j}, x\ux{cat} \\[3pt] 
\comm{superx}{\ldots} & superscript index with upright text & x\comm{superx}{3}, y\comm{superx}{i} & x\superx{3}, y\superx{i} \\

\hline\end{tabular}

The \textbf{\commb{alert}} command is compatible with the \lat{beamer} class. It puts highlighted text in boldface. This can be changed with \lat{renewcommand}.

The \textbf{\decla{nl}} declaration requires the \lat{amssymb} package.  (\decla{null} is already used by \TeX\ for empty boxes, so it's best not to replace it).

The \textbf{\commb{ol}} command is for object language text.  It is currently set to italics, but you can change that globally or locally with a \decla{renewcommand} command.

 The \textbf{\comm{m}{\ldots}} command sets grammatical morphemes (m) in \textsc{small caps}.  This is helpful in glosses, notably using \lat{gb4e}, since it makes the source easier to read.  Also, if you need to change the morphemes' typesetting globally, a simple \decla{renewcommand} of \decla{m} will suffice.
 
 However, if you use the \lat{fontspec} package for Xe\LaTeX , beware, for it employs \decla{m} for various diacritics.  So you can comment it out and use \commb{mc} for morpheme caps instead.  Also, \comm{usepackage}{ling-macros} must be placed in your preamble after \comm{usepackage}{fontspec}.

 \subsection{Macros for \lat{gb4e} environment}\label{gbr3} 
 
The following declarations make the use of the \lat{gb4e} more streamlined and allow for easy tweaking of example alignment. 

 \begin{tabular}{>{\bfseries}l l l c}
  \mdseries declaration & command it replaces & purpose & name origin\\\hline
\decla{bex} & \comm{begin}{exe} & begin example environment & begin exe \\
\decla{fex} & \comm{end}{exe} & end example environment & finish exe \\
\\
\decla{bxl} & \comm{begin}{xlist} & begin example subenvironment & begin xlist \\
\decla{fxl} & \comm{end}{xlist} & end example subenvironment & finish xlist \\

\end{tabular}

These declarations also define a number of variable widths that can be used to reformat the example environments.   The reformatting is done globally by setting the package options \lat{leftflush}, \lat{shortspace}, or \lat{abstract}. 

\textbf{Warning:} The package options do not reformat \lat{exe} environments unless you use the \decla{bex} and \decla{bxl} declarations.  Otherwise, one would need to adjust the \lat{gb4e} package itself.

The \lat{leftflush} option puts example numbers to the left, but numbers 1-9 are not all the way to the left.  To force them to be, place the declaration \decla{lessthanten} in the document before the first example.  This will place examples 10 and above too far left, so use the declaration \decla{tenormore} to undo this effect.  

While we're at it, here is a similar list of abbreviations for ordinary list environments.  

 \begin{tabular}{>{\bfseries}l l l c}
  \mdseries declaration & command it replaces & purpose & name origin\\\hline
\decla{ben} & \comm{begin}{enumerate} & begin enumerate environment & begin enumerate \\
\decla{fen} & \comm{end}{enumerate} & end enumerate environment & finish enumerate \\
\\
\decla{bit} & \comm{begin}{itemize} & begin itemize environment & begin itemize \\
\decla{fit} & \comm{end}{itemize} & end itemize environment & finish itemize \\

\end{tabular}


 \subsection{The \lat{context} environment}
 
 The \lat{context} environment typesets the context used to elicit or set-up an example.  The typesetting can be changed globally.
 
 \begin{context}
 	Denny arrived at the restaurant, and sat at an empty table.  The moment he did so, a waiter approached and asked him: 
	\end{context}
 
 	\bex \ex[\#]{Would you like some more water?}
	\fex

\comm{begin}{context}  \\
	Denny arrived at the restaurant, and sat at an empty table.  The moment he did so, a waiter approached and asked him: \\
	\comm{end}{context}

	\decla{bex} \comopt{ex}{\decla{\#}}{Would you like some water?} \decla{fex}


\section{Phonology}

OT Tableaux can be made with a number of packages, each with their own macros for symbols.  To write phonological rules, however, you can use the following macros to simplify things.

\begin{tabular}{>{\bfseries}l l l c }
 \mdseries command & purpose & source & in print \\\hline
\commb{underlying} & the input to the rule  & \comm{underlying}{+back} & \underlying{+back} \\
\decla{becomes} & the arrow & \decla{becomes} & \becomes \\
\commb{spoken} & the output & \comm{spoken}{--back}   & \spoken{--back} \\
\decla{environ} & `in the environment of' slash & \decla{environ} & \environ \\ 
\decla{spot}  &   the exact spot of the change & \decla{spot} &\spot \\
\decla{syll} & syllable subscript  & [\decla{syll} ~~~ ]\decla{syll} & [\syll ~~~ ]\syll \\\hline 
\decla{fmleft} & feature matrix left bracket & \decla{fmleft} &  \\
\decla{fmright} & feature matrix right bracket & \decla{fmright} &   \\
 \comm{fmat}{\ldots}\cbl \ldots\cbr & feature matrix line & \parbox[c]{1.1in}{\comm{fmat}{+}\cbl{coronal}\cbr \\\comm{fmat}{-}\cbl{voiced}\cbr }  &  \fmleft \fmat{+}{coronal}\fmat{-}{voiced}\fmright \\ 
\end{tabular}

Combined, these get a source code like this, for a  rule fronting a back vowel between /i/ and any consonant: 

\comm{underlying}{+back}  \decla{becomes} \comm{spoken}{--back} \decla{environ}  i\decla{spot} C

The commands \decla{prule} and \decla{iparule} are macros combining the above macros.

About the \comm{prule}{\ldots}\cbl \ldots\cbr\cbl \ldots\cbr\ command: The first command is the underlying form, the second the spoken form, and the third the environment.  The \comm{iparule}{\ldots}\cbl \ldots\cbr\cbl \ldots\cbr\ does the same, but puts everything in the rule in IPA. The \decla{iparule} command requires the \lat{tipa} package, which you probably already use if you're typesetting phonology.

 
\hspace*{2em}\comm{prule}{+back}\cbl --back\cbr\cbl i\decla{spot} C\cbr  \hspace{1cm} \prule{+back}{--back}{i\spot C} \\
\hspace*{2em}\comm{iparule}{2}\cbl E\cbr\cbl i\decla{spot} \decla{*}C\cbr  \hspace{1.8cm} \iparule{2}{E}{i\spot \*C} \\
 
 
The \comm{fmat}{\ldots}\cbl \ldots\cbr command is for feature matrices.   Use \decla{fmleft} and \decla{fmright} for each bracket, and for each line in the feature matrix, use \comm{fmat} with its two arguments.  The first argument will be $+/-$, and the second will be the feature name. 
  
You can put feature matrices inside a phonological rule command as well. 


\section{Syntax}

For syntax trees, a tree package like \lat{qtree}, \lat{tikz-qtree}, or \lat{parsetree} suffices.  The following macros allow quick and regular typing of some common syntactic symbols, in better looking ways than are offered by ordinary distributions and packages.  

\hspace*{-1cm}\begin{tabular}{>{\bfseries}l l l c }
 \mdseries command & purpose & source & in print \\\hline
\commb{head}  & the head circle & \comm{head}{V} & \head{V} \\
\commb{xbar} & the bar in X-bar &   \comm{xbar}{Asp} & \xbar{Asp}~ \\ 
\decla{lv} & little v & \decla{lv} & \lv ~~\\
\commb{feat}  & syntactic feature in trees & \comm{feat}{fem} & \feat{{fem}} \\ 
\commb{textfeat}  & syntactic feature in text & \comm{textfeat}{fem} & \textfeat{{fem}} \\ 
\commb{dcopy} & deleted copy (strike-out)\footnotemark & \comm{dcopy}{the car} & \dcopy{the car} \\
\commb{mroot} & morpheme root & \comm{mroot}{car} & \mroot{car} \\
\commb{ufeat} & unvalued/uninterpretable feature in trees & \comm{ufeat}{T} & \ufeat{T} \\ 
\commb{unv} & unvalued/uninterpretable feature & \comm{feat}{\comm{unv}{T}} & \ufeat{T}   \\ 
\end{tabular}\footnotetext{Requires the \lat{ulem} package}

The \decla{unv} command should be used inside a \decla{feat} or \decla{textfeat} command, but of course doesn't have to be.  If you want to use an upright $\phi$ symbol ($\upphi$), use the \decla{upphi} declaration in math mode.

Use the \comm{featuresize}{$\langle$size$\rangle$} command to adjust the size of features in \commb{feat}.

The \commb{xbar} command places a bar over the entire head name.  For a prime symbol instead, you can use use the \decla{pri} declaration.

For bracket subscript labels, you can use the \comm{ix}{\ldots} or \comm{ux}{\ldots} commands. 

\begin{center}
	  \cbl [\cbr\comm{ux}{TP}   \comm{head}{T} [\comm{ux}{VP} \comm{head}{V}  [\comm{ux}{DP} \head{D} ] ] ]  \\
	 {[}\ux{TP} \head{T} [\ux{VP} \head{V} [\ux{DP}  \head{D} ] ] ]
\end{center}


 
\section{Semantics}

Formal semantics uses math mode more clearly than most areas of linguistics.  You can use the \commb{form} command to put anything into math mode. 
More recently, semanticists have been writing formulas in text, with mathematical symbols.  Using the \lat{text-semantics} option will convert these formulas from math mode to text mode.  Some symbols you will want to stay in math mode.  Putting them between \mathbit{\ldots} often creates errors. Instead, the \commb{ensuremath} command will protect them.  For short, you can use the \commb{f} command for any symbol you want to remain in math mode even as \commb{form} is redefined as text mode.

 Along with formal expressions, the  \commb{readas} command is used with denotations to write formal expressions out in metalanguage.
 
 \denol{every car} = \form{\lamd{Q}{et}.~ \all{y}[~ car(y) = 1 \to Q(y) = 1~]} \\
 \readas{the function from properties of entities to truth values such that for all \form{y}, if \form{y} is a car, then \form{Q(y) = 1}
}


\subsection{Operators and Symbols}

Operators all require math mode, and putting them in math mode makes source documents hard to read.  These macros simplify the writing of common operators, and make the source code more intuitive to read. 

\hspace*{-2em}\begin{tabular}{>{\bfseries}l l l c }
 \mdseries command & purpose & source & in print \\\hline
\comm{lam}{{\small\itshape variable}}		&  lambda operator  &  \comm{lam}{x}  & \lam{x} \\
\comm{lamd}{{\small\itshape var.}}\cbl {\small\itshape type}\cbr  & lambda operator with domain D\ix{type} &  \comm{lam}{P}\cbl s,t\cbr & \lamd{P}{s,t}\\
\comm{all}{{\small\itshape var.}} & universal quantifier & \comm{all}{x} & \all{x} \\
\comm{some}{{\small\itshape var.}} & existential quantifier & \comm{some}{x} & \some{x} \\
\comm{no}{{\small\itshape var.}} & negative quantifier & \comm{no}{x} & \no{x} \\
\comm{ddet}{{\small\itshape var.}} & iota-operator (definite determiner) & \comm{ddet}{x} & \ddet{x}\\
\decla{pri} & prime symbol in text or math mode & x\decla{pri} & x\pri \\
\end{tabular} 
%\EX 


 
 \subsection{Semantic types}
 
 The \commb{type} command is used for writing semantic types.  It places its argument in ordered pair brackets, in math mode.   It can be used inside the arguments of another \decla{type} command to get complex types.
 
 \begin{tabular}{l l}
 \comm{type}{e,t}   & \type{e,t} \\
 \comm{type}{e,\comm{type}{s,t}} & \type{e,\type{s,t}} \\
\comm{type}{\comm{type}{e,t},\comm{type}{\comm{type}{e,t},t}} & \type{\type{e,t},\type{\type{e,t},t}} \\
\end{tabular}

Since \decla{type} places its arguments in math mode, it can be used for ordinary ordered pairs as well.  For simple types, which don't require ordered pair brackets, simply place the type in math mode:  \mathbit{e}, \mathbit{t} $\Rightarrow$ $e$, $t$ 

The \lat{text-semantics} option will not put types in text mode. If you really want semantic types with upright letters, use the \commb{uptype} command wherever you'd use \commb{type}.

\subsection{Sets}

The \commb{set} and \commb{varset} commands are used to write sets.  The \decla{set} command is purely for making the source code more intuitive, since \decla{\{} is not exactly hard to type.  The \decla{varset} command (`variable set') writes an abstracted set.  \decla{varset} uses a vertical line for `such that'. For the older colon notation, use the \decla{cvarset} command. 

\begin{tabular}{l l}
\comm{set}{a, b, c, d}  & \set{a, b, c, d} \\
\comm{varset}{x \comm{f}{\decla{in}} D}\cbl x is happy\cbr  &  \varset{x \in D }{x ~is ~happy} \\
\comm{cvarset}{x}\cbl x is happy\cbr  &  \cvarset{x}{x ~is ~happy} \\
\end{tabular}

%Under the \lat{text-semantics} option: 
%
%\begin{tabular}{l l}
% \comm{set}{a, b, c, d}  & {\{ {a, b, c, d} \}} \\
%\comm{varset}{x \mathbit{\decla{in}}  D}\cbl x is happy\cbr  &  \{ {x $\in$ D} $|$ {x  is happy} \} \\
%\comm{cvarset}{x}\cbl x is happy\cbr  &  \{ {x} $:$ {x is happy} \} \\
%\end{tabular}

\subsection{Functions}

Use the command \commb{funcnot}\cbl \ldots \cbr\cbl \ldots \cbr\cbl \ldots \cbr\cbl \ldots \cbr , which allows quick writing of functions in an explicit functional notation (hence the name). The first argument is the variable representing the function; the second is the domain of the function, the third is the range, the fourth is the argument variable, and the fifth are the truth conditions.  

%\begin{minipage}{.45\textwidth} 
\comm{funcnot}{f}{\cbl D\cbr}{\cbl R\cbr}\cbl y\cbr\cbl 1 iff \comm{form}{y} is a bandit\cbr  \\

\parbox[t]{2in}{With no options : } \funcnot{f}{D}{R}{y}{1 iff \form{y} is a bandit}

\parbox[t]{2in}{With \lat{text-semantics} option : }  {  \renewcommand{\funcnot}[5]{%
 {#1} : \pbox[t]{2.0\textwidth}{%  1 the function
		  {#2} {\ensuremath{\to}} #3 \\% the buffoon % the domain
		\all{}{#4} \ensuremath{\in}  #2,  {#1(#4) =} #5% %object of domain
		}%
		}%%
		\funcnot{f}{D}{R}{y}{1 iff {y} is a bandit}
}

Functions can be embedded in others by putting the second function in the truth-conditions of the first
  
\comm{funcnot}{f}{\cbl D\comm{ix}{e}\cbr}{\cbl  D\comm{ix}{et}\cbr}\cbl x\cbr\cbl{\color{red}\%} \\
		\comm{funcnot}{g}{\cbl D\comm{ix}{e}\cbr}{\cbl  D\comm{ix}{t}\cbr}\cbl y\cbr\cbl 1 iff \comm{form}{y} is tall\cbr{\color{red}\%} \\
		\cbr{\color{red}\%} 

		\funcnot{f}%FUNCTION
			{D\ix{e}}%DOMAIN
			{D\ix{et}}%RANGE
			{x}%VARIABLE
			{\funcnot{g}%FUNCTION'
				{D\ix{e}}%DOMAIN'
				{D\ix{t}}%RANGE'
				{y}%VARIABLE'
				{1 iff \form{y} is tall}%TRUTH-CONDITIONS'
				}%TRUTH-CONDITIONS




To write a function in array format requires math mode and the array environment.  This is inconvenient, so the following macros simplify this.

\begin{enumerate} \item The \decla{fleft} (function left) declaration gives the left bracket.  
\item The \comm{func}{\ol{domain}}\cbl \ol{range}\cbr\ command is used for each line of the function.  
\item The \decla{fright} (function right) declaration gives the right bracket.
\end{enumerate}

To write the function \set{\uptype{a,1},\uptype{b,2}}: 

\begin{minipage}{.45\textwidth} 

\decla{fleft}{\color{KUCrimson}\%}  \\
\comm{func}{a}\cbl 1\cbr{\color{KUCrimson}\%}   \\
\comm{func}{b}\cbl 2\cbr{\color{KUCrimson}\%}  \\
\decla{fright}{\color{KUCrimson}\%}  
\end{minipage}
\begin{minipage}{.45\textwidth} 
\fleft 
\func{a}{1}
\func{b}{2}
\fright
\end{minipage}

These macros can be used recursively.

\begin{minipage}{.45\textwidth} 
\decla{fleft}{\color{KUCrimson}\%}  \\
\comm{func}{a}\cbl{\color{KUCrimson}\% range of a}   \\
  	\decla{fleft}~\comm{func}{c}\cbl 1\cbr {\color{KUCrimson}\%} \\
\hspace*{6ex}\comm{func}{d}\cbl 2\cbr{\color{KUCrimson}\%} \\
	\decla{fright}\cbr {\color{KUCrimson}\% end of range of a}  \\
\decla{fright}{\color{KUCrimson}\%}  
\end{minipage}
\begin{minipage}{.45\textwidth} 
\fleft 
\func{a}{
	\fleft\func{c}{1}
		\func{d}{2}
	\fright} 
 
\fright
\end{minipage}


\subsection{Scope brackets with \decla{scopebox}}

The \commb{scopebox} command places brackets ([ ]) around an expression to signal its scope.  This command allows the use of multi-line scope brackets, to make things easier to read.  If you put more than one scope box inside the largest one, you should use \commb{innerscopebox} for the inside ones.

Compare the following formulas, with simple brackets, and then with \commb{scopebox}.

\form{\all{x}[ ~dog(x) \&\ on(\textrm{the car})(x) \f{\to} \some{y}[ ~cat(y) ~\&\ \some{e}[ \textsc{perfective}(e) ~\&\ chase(x)(y)(e) ] ] ] }

\form{\all{\textrm{x}}\scopebox{dog(x) \&\ on(the car)(x) \f{\to}  ~~~~~~~~\\
				\some{\textrm{y}}\innerscopebox{cat(y) \&\ \\
					\some{e}\innerscopebox{ \m{perf}(e) \&\ chase(x)(y)(e) } }}}



Note:  These commands force their expression to be in text mode, because they rely on \commb{pbox}, which is part of the \lat{pbox} package.  \\
Note 2: Sometimes you may need to add spaces to the outside scopebox (with \raisebox{-3pt}{\~{}}  ) to make sure it's the widest.

\subsection{Denotation brackets}

 
Several commands involve double brackets for denotations (or interpretation functions).  These all require the \lat{stmaryrd} package, which is called by the \lat{ling-macros} package.

Many commands involve assignment modifications.  These modifications are already in math mode, so any use of {\color{green!60!black}{\$}} in them will lead to an error.  If you need to use math mode inside these, use \comm{ensuremath}{\ldots} or \comm{f}{\ldots}.

\hspace*{-5em}\begin{tabular}{>{\bfseries}l l l c}
 \mdseries command & purpose & source & in print  \\[2pt]\hline
\commb{den} & denotation brackets & \comm{den}{car} & \hspace{-1.1ex}\den{car}  \\% & req. \lat{stmaryrd} pkg. \\
\commb{dena}\cbl\ldots \cbr & denotation w/ assignment & \comm{dena}{car}\cbl g\cbr &  \dena{car}{g}  \\% 
\commb{denac}\cbl\ldots \cbr & d.b. w/ asst., context c & \comm{denac}{car}\cbl g\cbr & ~\denac{car}{g} \\%
\comm{denamod}{..}\cbl ..\cbr\cbl ..\cbr & {\small d.b. w/ modified assignment} & \comm{denamod}{car}\cbl g\cbr\cbl x\decla{to} 1\cbr &  \denamod{car}{g}{x \ensuremath{\to} 1} \\ %
 \comm{denacmod}{..}\cbl ..\cbr\cbl ..\cbr & {\small d.b. w/ modified asst, context c} & \comm{denacmod}{car}\cbl g\cbr\cbl x\decla{to} 1\cbr &  \denacmod{car}{g}{x \ensuremath{\to} 1} \\% 
\end{tabular}

\vspace{8pt}There is also a series of commands which are identical to those of the \decla{den} family, except that the text is already in the object language font (\decla{ol}).  These commands declutter the source code.  With \decla{ol} set to \decla{itshape}: 
 
 \hspace*{-5em}\begin{tabular}{>{\bfseries}l l l c}
 \mdseries command & purpose & source & in print  \\[2pt] \hline
 \commb{denol} & denotation brackets & \comm{denol}{car} & \hspace{-1.1ex}\denol{car}  \\% & req. \lat{stmaryrd} pkg. \\
\commb{denola}\cbl\ldots \cbr & denotation w/ assignment & \comm{denola}{car}\cbl g\cbr &  \denola{car}{g}  \\% 
\commb{denolac}\cbl\ldots \cbr & d.b. w/ asst., context c & \comm{denolac}{car}\cbl g\cbr & ~\denolac{car}{g} \\%
\comm{denolamod}{..}\cbl ..\cbr\cbl ..\cbr & {\small d.b. w/ modified assignment} & \comm{denolamod}{car}\cbl g\cbr\cbl x\decla{to} 1\cbr &  \denolamod{car}{g}{x \ensuremath{\to} 1} \\ %
 \comm{denolacmod}{..}\cbl ..\cbr\cbl ..\cbr & {\small d.b. w/ modified asst, context c} & \comm{denolacmod}{car}\cbl g\cbr\cbl x\decla{to} 1\cbr &  \denolacmod{car}{g}{x \ensuremath{\to} 1} \\% 
 
\end{tabular} 

  


\end{document}