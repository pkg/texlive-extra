%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%	ling-macros package
%
%%%%%%  %% ling-macros.sty
%%%%    %% version 2.2
%%%	%% 2016-10-12
%
%% Copyright 2016 Andrew McKenzie (andrew.mckenzie@ku.edu)
%
% %% not to be confused with lingmacros.sty, a part of tree-dvips.sty
%
%%%%  Thanks to Lydia Newkirk for the name suggestion.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Andrew McKenzie.
%
% This work consists of the files ling-macros.sty and ling-macros-doc.tex

% The linguistics package provides macros for typesetting formal linguistics.