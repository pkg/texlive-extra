numerica-plus: a package to iterate and find fixed points of
functions, to find zeros and extrema of functions and to 
calculate recurrence relations.

Andrew Parsloe (ajparsloe@gmail.com)

This work may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3c of this 
license or any later version; see
http://www.latex-project.org/lppl.txt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This is version 2.0.0 of numerica-plus, the first stand-alone
version. (Version 1 of numerica-plus was invoked from the
numerica package by means of a package option.) The packages 
numerica, l3kernel, l3packages, amsmath and mathtools are 
required. numerica-plus defines three commands: \nmcIterate to 
iterate and find fixed points of functions, \nmcSolve to find 
zeros and extrema of functions, and \nmcRecur to calculate the 
terms of recurrence relations. See numerica-plus.pdf for 
details on how to use the package.

Manifest
%%%%%%%%
README.txt             this document

numerica-plus.sty      LaTeX .sty file
numerica-plus.pdf      documentation for numerica-plus.sty
numerica-plus.tex      documentation source file

