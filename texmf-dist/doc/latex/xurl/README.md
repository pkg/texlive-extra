# README #
Package xurl loads package url by default and defines
possible url breaks for all alphanumerical characters
and = / . : * - ~ ' " 

All arguments which are valid for url can be used.
It will be passed to package url. xurl itself has only 
the optional argument "nobiblatex". For more information read
the documentation of package url.

This file is distributed under the terms of the LaTeX Project Public
License from CTAN archives in directory  macros/latex/base/lppl.txt.
Either version 1.3 or, at your option, any later version.

hvoss@tug.org