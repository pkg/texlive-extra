This package provides macros and an environment for easy worksheet creation:
	use the exercise environment for formating exercises in a simple, efficient design;
	make custom, automatically numbered worksheet titles in the same way as the \LaTeX\ title macros;
	provide course and author information with a scrlayer-scrpage based automated header;
	conforming to different Babel languages (At this time only English, French and German are supported.).

This package is guaranteed to work with pdflatex and xelatex, only depending on scrlayer-scrpage and Babel. It is recommended to use xelatex including fontspec package and setting sans font to "Helvetica Neue" or "Raleway".

Package worksheet by Benjamin Zöllner (c)2018. This work may be distributed and/or modified under the conditions of the LaTeX Project Public License (latest version).
