# SimplePlus Beamer Theme

A simple and clean beamer template.

**Website:**

https://github.com/PM25/SimplePlus-BeamerTheme

**Overleaf:**

https://www.overleaf.com/latex/templates/simpleplus-beamertheme/wfmfjhdcrdfx

**Author:**

Pin-Yen Huang (pyhuang97@gmail.com)

**Compiled sample document:**

beamertheme-simpleplus-sample.pdf

## License

This is free and unencumbered software released into the public domain.
For more information, please see the file `LICENSE` or refer to <http://unlicense.org>.