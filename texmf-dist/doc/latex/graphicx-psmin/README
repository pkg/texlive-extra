graphicx-psmin v1.2
-------------------

This package is an extension of the standard graphics bundle and
provides a way to include repeated postscript graphics (ps, eps)
only once in a postscript document. This provides a way to get
smaller postscript documents when having, for instance, a logo on
every page. This package only works when post-processed with dvips,
which should at least have version 5.95b! The difference for the pdf
file is minimal (as ghostscript already includes only a single copy
of graphics files). See for more information the pdf documentation
of this package.


Installation
------------

First check whether this package can be installed by the
distribution that you use, since that will simplify installation.
For manual installations, the structure and supposed location in a
TDS compliant TeX-tree is shown in the overview below. In words,
graphicx-psmin.sty should be available to LaTeX. Don't forget to
update your filename database after installing the files.

CTAN:/macros/latex/contrib/graphicx-psmin: official location
 |
 |- readme             : should not be installed
 |- graphicx-psmin.dtx : /source/latex/graphicx-psmin
 |- graphicx-psmin.pdf : /doc/latex/graphicx-psmin
 `- graphicx-psmin.sty : /tex/latex/graphicx-psmin

The package includes pregenerated run and doc files, but you can
reproduce them from the source if necessary. See the first lines of
graphicx-psmin.dtx for information how to do this.

See the documentation of your LaTeX distribution or the TeX
Frequently Asked Questions for more information on installing
graphicx-psmin into your LaTeX distribution
(http://www.tex.ac.uk/cgi-bin/texfaq2html?label=instpackages).


License
-------

Copyright (C) 2005-2020 Hendri Adriaens

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2003/12/01 or later.

This work has the LPPL maintenance status "maintained".

This Current Maintainer of this work is Hendri Adriaens.

This work consists of the file graphicx-psmin.dtx and derived file
graphicx-psmin.sty.

The following files constitute the graphicx-psmin bundle and must be
distributed as a whole: readme, graphicx-psmin.pdf, graphicx-psmin.sty
and graphicx-psmin.dtx.
