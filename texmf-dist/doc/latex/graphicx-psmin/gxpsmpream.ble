%%
%% This is file `gxpsmpream.ble',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% graphicx-psmin.dtx  (with options: `preamble')
%% 
%% ---------------------------------------
%% Copyright (C) 2005-2020 Hendri Adriaens
%% ---------------------------------------
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2003/12/01 or later.
%%
%% This work has the LPPL maintenance status "maintained".
%%
%% This Current Maintainer of this work is Hendri Adriaens.
%%
%% This work consists of the file graphicx-psmin.dtx and derived file
%% graphicx-psmin.sty.
%%
%% The following files constitute the graphicx-psmin bundle and must be
%% distributed as a whole: readme, graphicx-psmin.pdf, graphicx-psmin.sty
%% and graphicx-psmin.dtx.
%%
\usepackage{url}
\usepackage{graphicx-psmin}
\usepackage{fourier}
\usepackage{xcolor}
\usepackage[multiple]{footmisc}
\usepackage{pst-char}
\usepackage{listings}
\lstnewenvironment{command}{%
  \lstset{columns=flexible,frame=single,backgroundcolor=\color{blue!20},%
    xleftmargin=\fboxsep,xrightmargin=\fboxsep,escapeinside=`',gobble=1}}{}
\lstnewenvironment{example}{%
  \lstset{basicstyle=\footnotesize\ttfamily,columns=flexible,frame=single,%
    backgroundcolor=\color{yellow!20},xleftmargin=\fboxsep,%
    xrightmargin=\fboxsep,gobble=1}}{}
\def\mktitledecor{%
  \rput[tl]{90}(-6.5,-23.56){%
    \psline[linewidth=1pt](0,1.5)(\paperheight,1.5)%
    \rput[lB](.075\paperheight,.5){\pscharpath[linecolor=blue!50,%
      fillcolor=yellow!20,fillstyle=solid,linewidth=.5pt]%
      {\Huge\bfseries\sffamily graphicx-psmin}%
    }%
    \rput[rB](.925\paperheight,.5){\pscharpath[linecolor=blue!50,%
      fillcolor=yellow!20,fillstyle=solid,linewidth=.5pt]%
      {\Huge\bfseries Documentation}%
    }%
    \psline[linewidth=1pt](0,0)(\paperheight,0)%
  }%
}
\makeatletter
\def\tableofcontents{\@starttoc{toc}}
\renewenvironment{theglossary}{%
  \section*{Version history}%
  \GlossaryParms \let\item\@idxitem \ignorespaces
}{}%
\def\changes@#1#2#3{%
  \protected@edef\@tempa{%
    \noexpand\glossary{\textbf{#1}\hfill\emph{(#2)}%
    \levelchar
    \ifx\saved@macroname\@empty
      \space\actualchar\generalname
    \else
      \expandafter\@gobble\saved@macroname
      \actualchar\string\verb\quotechar*%
      \verbatimchar\saved@macroname\verbatimchar
    \fi
    :\levelchar #3}%
  }%
  \@tempa\endgroup\@esphack
}
\makeatother
\def\PrintChangesX{%
  \begingroup
    \let\efill\relax
    \PrintChanges
  \endgroup
}
\def\PrintIndexX{%
  \begingroup
    \setcounter{IndexColumns}{2}%
    \setlength\columnsep{18pt}%
    \setlength\columnseprule{.4pt}%
    \PrintIndex
  \endgroup
}
\def\pf#1{\textsf{#1}}
\DisableCrossrefs
\RecordChanges
\CodelineIndex
\endinput
%%
%% End of file `gxpsmpream.ble'.
