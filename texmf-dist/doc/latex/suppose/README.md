# the ``suppose`` package
## Andrew Lounsbury
### Licensed with LPPL 1.3c
### 1.2.2 2021/05/20

This package provides abbreviations of the word "Suppose" in various fonts and styles. \
See the documentation `suppose-doc.pdf` for a demonstration. 

## Change log
|**1.1 2021/03/26**                  |
|------------------------------------|
| Added options for setting one font |

|**1.2 2021/04/01**                                          |
|------------------------------------------------------------|
| Added `slant` option                                       |
| Fixed bug in `\PackageWarning` message for unknown options |
| Adjusted placement of the `\rule` for each font            |

|**1.2.1 2021/04/09**                                               |
|-------------------------------------------------------------------|
| Adjusted placement and thickness of the `\rule` for most commands |

|**1.2.2 2021/05/20**     |
|-------------------------|
| Improved implementation |