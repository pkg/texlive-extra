$Id: NEWS 458 2023-01-16 18:44:30Z karl $
This file records noteworthy changes.  This file is public domain.

-----------------
2.28 (2023-01-16):

ltugboat.cls:
- \tburl: new name for \tbsurl, should work in all cases.
          \def\tburl{\url} to keep using \url in the document body.
- \titleref: command to typeset a book (etc.) title (slanted + \frenchspacing).
- \tubdots: command for a normally-spaced ellipsis, not the squashed
  Unicode character provided by most fonts.
- \raggedleft: TUB's redefinition should not work with tabularray.

tugboat.bst: use \url instead of \href, to avoid requiring hyperref.

-----------------
2.27 (2022-05-19):
ltugboat.cls:
- \tbsurlfootnote, \tbhurlfootnote: typeset urls in ragged-right
  footnotes of their own.
- \tubsentencespace: force a sentence-ending space, e.g., after a
  post-sentence \cite.
- \tubjustifiedpar: undo ragged-right or other, restoring normal
  paragraph settings.
- \eTeX: use \boldsymbol if available for this logo.

-----------------
2.26 (2021-10-12):
ltugboat.cls:
- strip a leading http(s) from \tbsurl and \tbhurl.
- \Xe:increase negative kern.

tugboat.bst:
- allow editor field as well as author for @misc entries.
- make @software another alias for @misc.
- improve doi urls.

-----------------
2.25 (2021-06-27):
ltugboat.cls:
- new abbrevations: \RIT, \tbUTF.

\tbUTF expands to \acro{UTF}, and, if the next character is a -
(e.g., \tbUTF-8), don't allow breaking at it. (We don't define \UTF
since other packages already do.)

tugboat.bst:
- new entry type @ctan, following ctanbib (https://ctan.org/pkg/ctanbib).
- use \emph{...} instead of {\em ...}, so we get italic corrections.

-----------------
2.24 (2020-11-14):
ltugboat.cls:
- new hyphenation exceptions: Ja-pa-nese, Pak-i-stan.
- new abbrevations: \macOS, \OpTeX.

tugboat.bst:
- new field bookauthor for @incollection and @inproceedings.
- generalize truncation of author names with new code from
  Oren Patashnik, using integers max.names and trunc.num
  to control operation.
