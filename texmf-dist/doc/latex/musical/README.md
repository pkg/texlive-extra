The musical.sty package provides tools for typesetting musical theatre scripts, particularly ones that are still under development. It's designed to make it very easy to print and distribute scripts that comply with generally accepted script formatting standards while not getting in the way of frequent changes, as works under development tend to have. 

Created by Dave Howell (dh.musical@howell.seattle.wa.us), 
distributed under the usual LATEX Project Public License 1.3c.

