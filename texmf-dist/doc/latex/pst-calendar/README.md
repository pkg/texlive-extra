# Calendars #

Save the files pst-calendar.sty in a directory, which is part of your 
local TeX tree. 
Then do not forget to run texhash to update this tree.
For more information  see the documentation of your LATEX distribution 
on installing packages into your local TeX system or read the 
TeX Frequently Asked Questions.

PSTricks is PostScript Tricks, the documentation cannot be run
with pdftex, use the sequence latex->dvips->ps2pdf or xelatex
or lualatex with the package luapstricks.lua

pst-calendar is a PSTricks related package to print different calendars.

