## How to typeset

This document uses many packages and programs. If you want to typeset
it yourself, this is the list of stuff you'll have to install:

- Package: musicography.
<https://ctan.org/pkg/musicography>

- Package: leadsheets.
<https://ctan.org/pkg/leadsheets>

- Package: lilyglyphs.
<https://ctan.org/pkg/lilyglyphs>

- Font: Bravura.
<https://www.smufl.org/fonts>

- Package: guitar.
<https://ctan.org/pkg/guitar>

- Package: gtrcrd.
<https://ctan.org/pkg/gtrcrd>

- Package: songs.
<https://ctan.org/pkg/songs>

- Package: musixguit.
<https://ctan.org/pkg/musixguit>

- Package: songbook.
<https://ctan.org/pkg/songbook>

- Program: Chordii.
<https://www.vromans.org/projects/Chordii>

- Package: gchords.
<https://ctan.org/pkg/gchords>

- Package: guitarchordschemes.
<https://ctan.org/pkg/guitarchordschemes>

- Package: guitartabs.
<https://ctan.org/pkg/guitartabs>

- Page: MusiXTeX and Related Software.
<https://icking-music-archive.org/software/htdocs/htdocs.html>

- Package: MusiXTeX.
<https://ctan.org/pkg/musixtex>

- Package: M-Tx.
<https://ctan.org/pkg/m-tx>

- Package: Gregoriotex.
<https://ctan.org/pkg/gregoriotex>
http://gregorio-project.github.io/gregoriotex
<http://gregorio-project.github.io>

- Program: LilyPond.
<http://lilypond.org>

- Package: Lyluatex.
<https://ctan.org/pkg/lyluatex>

- Programs: abcm2ps, and2svg.
<http://moinejf.free.fr>

- Program: PMW, Philip’s Music Writer.
<http://people.ds.cam.ac.uk/ph10/pmw.html>

- Program: MUP.
<http://www.arkkra.com>

- Program: MuseScore.
<http://musescore.org>

- Package: Abc.
<https://ctan.org/pkg/abc>

- Program: abc2xml.ly.
<https://wim.vree.org/svgParse/abc2xml.html>

Then, just type "make" in this directory.

Cheers,
     Guido =8-)
