%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                  File: opteng.sty                       %
%                      VERSION: 1.1                       %
%                 Date: June 01, 2006                     %
%                                                         %
%                                                         %
%                 LaTeX style file for                    %
%          length check and submission of SPIE            %
%          Optical Engineering and OE Letters             %
%                                                         %
%                                                         %
% \documentclass[10pt,letterpaper,optengjnl]{article}     %
% or                                                      %
% \documentclass[10pt,letterpaper,optenglett]{article}    %
% or                                                      %
% \documentclass[12pt,letterpaper,optengsubmit]{article}  %
%                                                         %
% \usepackage{opteng}                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% Choose one of the following options:

%%\documentclass[10pt,letterpaper,optengjnl]{article}       % Activate for Optical Engineering

\documentclass[10pt,letterpaper,optenglett,fleqn]{article}      % Activate for OE Letters

%%\documentclass[12pt,letterpaper,optengsubmit]{article}    % Activate for Submission



\usepackage{opteng}
\usepackage[ps2pdf,
pdftitle={Manuscript submission and page length estimation for SPIE OE Letters and Optical Engineering journals},%
pdfauthor={Patrick Egan},%
pdfsubject={LaTeX document preparation.},%
pdfkeywords={LaTeX, OE Letters, page length
estimate, manuscript submission.},%
colorlinks=true,%
breaklinks=true,%
linkcolor=red,%
citecolor=green,%
final ]{hyperref}



% SPIE journals are published with Times (not the LaTeX computer modern default)
\usepackage{times}

%%\usepackage[tablesfirst,notablist,nomarkers]{endfloat}  % Submit with all figures at the back


% Activate for OE Letters
\fancyput(17.5cm,2.5cm){  \psframe[linestyle=none,fillstyle=solid,fillcolor=MyGray](-\paperwidth,-1.6)(\paperwidth,0.1)  \rput(-9.5,-0.75){\Large \bf \textsc{OE Letters}}  \rput[l](-18.6,-27){\footnotesize \textsf{Optical Engineering}}  \rput[r](-0.8,-27){\footnotesize \textsf{\today/Vol.~00(0)}}}%

% Activate for Optical Engineering
%%\fancyput(17.5cm,2.5cm){  \rput[l](-18.6,-1.25){\footnotesize \textsf{Optical Engineering 00(0), 000000 (\today)}}  \rput(-17.3,-27){\footnotesize \textsf{Optical Engineering}}  \rput(-2,-27){\footnotesize \textsf{\today/Vol.~00(0)}}}%


\begin{document}

%%\twocolumn[                                               % Activate for Optical Engineering

\title{Manuscript submission \\ and page length \\ estimation for
SPIE\\ \emph{OE Letters} and \emph{Optical \\ Engineering} journals}

%%\begin{minipage}{7cm}                                     % Activate for Optical Engineering

\author{Patrick Egan}

\address{University of Limerick\\ Castletroy\\ County Limerick\\ Ireland}

\email{patrick.egan@ul.ie}

\author{A.~N.~Other\SPIEmember}

\address{Research Group \\ Affiliation\\ Address}

\subjterm{\LaTeX, \emph{OE Letters}, page length estimate,
manuscript submission.}

\loginfo{Manuscript compiled \today}

\doi{10.0000/XXXX}


% Activate next three lines for Optical Engineering
%%\end{minipage}
%%\hfill%
%%\begin{minipage}{10cm}


\begin{abstract}
%
With this template, and associated style and \LaTeX\ packages, it is
possible to estimate the page length of manuscripts for submission
to the SPIE journals \emph{Optical Engineering} and \emph{OE
Letters}. With a strict three-page limit, this is particularly
important for the latter. This template gives simple instructions on
how to prepare the manuscript.
%
\end{abstract}


% Activate next three lines for Optical Engineering
%%\end{minipage}
%%\vskip 1cm
%%]%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Prerequisites}

The \texttt{opteng} style requires the following packages to be
installed on the system:

\begin{itemize}

\item \texttt{overcite} for SPIE style citations

\item \texttt{geometry} defines the page layout

\item \texttt{amsmath} is useful for displaying maths

\item \texttt{fancybox} is used to place the \textsc{OE Letters}
header

\item \texttt{pstricks} is used to create the \textsc{OE Letters}
header

\item \texttt{graphicx} is used for inserting eps figures

\end{itemize}

Note SPIE journals are set in times font, thus the package
\texttt{times} should be loaded in the preamble of the tex file.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

The preamble of the document is where differentiation between either
the \emph{Optical Engineering} (\texttt{optengjnl}) or the \emph{OE
Letters} (\texttt{optenglett}) format occurs. Furthermore there is
possibility to choose a format suitable for submission
(\texttt{optengsubmit}: 12pt, doubleline space, single column).

\begin{enumerate}

\item Choose either \texttt{optengjnl}, \texttt{optenglett}, or \texttt{optengsubmit} option in the
\verb+\documentclass+ definition.

\item Choose either \texttt{\fancyput} instruction. One will print the
gray background \textsc{OE Letters} heading. Both print rough
footers. For manuscript submission \textbf{no} \texttt{\fancyput}
command should be active.

\item For \emph{OE Letters} everything is set. Only for
\emph{Optical Engineering} should the \verb+\twocolumn+ and
\texttt{minipage} commands be activated.

\end{enumerate}

The \emph{OE Letters} format does not use sections or subsections.
Thus, the document should be started directly after the abstract and
subject terms, and no \verb+\section+ or \verb+\subsection+
environments should appear in the tex file.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Environments}

%%------------------------------------------------------------------
\subsection{Abstract}

SPIE journal abstracts are set in serif font, so put mathematics in
\verb+\mathsf+. The copyright is automatically generated, but the
user can specify the digital object identifier \verb+\doi+, subject
terms \verb+\subjterm+, and the submission/acceptance date
\verb+\loginfo+.


%%------------------------------------------------------------------
\subsection{Displayed equations}

Equations should fit into one column:
    \begin{equation}
    \label{eqn:app1-youngs_exp}%
    d_2 - d_1 = \pm n \pi,
    \end{equation}
or
    \begin{equation}
    \label{eqn:app1-light_fund}%
    \begin{split}
    E(z,t) & = E = \Re \bigl [ A \exp \left ( -j 2 \pi \nu t
    \right ) \bigr ]
    \\
    & = \dfrac{1}{2} \bigl [ A \exp \left ( -j 2 \pi \nu t
    \right ) + A^{\star} \exp \left ( j 2 \pi \nu t \right ) \bigr ],
    \end{split}
    \end{equation}
or even
    \begin{align}
    {\dot{E}_{x,y}} &=\frac{1}{2}\left( 1+j\alpha \right) \left( G_{x,y}
    - \gamma \right) E_{x,y}  \label{Eq1} \nonumber
    \\
    & \quad + \kappa E_{x,y}\left( t-\tau \right) \exp \left( -j
    \Omega_{x,y}\tau \right) \nonumber
    \\
    & \quad + (\beta _{sp}N)^{1/2} \xi _{x,y}.
    \end{align}

%%------------------------
\begin{figure}
\centerline{\includegraphics[width=\linewidth]{fig_1.eps}}
\caption{(Color online) Sample figure. Note \texttt{mathsf} should
be used to keep mathematics ($\mathsf{1, 2, 3}$) in serif font
style.} \label{fig:opteng-fig01}
\end{figure}
%%------------------------

Use standard LaTeX or AMSTeX environments. For equations that
\textit{must} span two columns, it is possible to use a float
environment, for example, \verb+\begin{figure*}...\end{figure*}+.
Such an environment will not interfere with figure or table
numbering (which is controlled by the caption), but it \textit{will}
cause equations to float, often with unwanted consequences.

%%------------------------
\begin{table}
\label{tab:opteng-tab01}  \centering
  \caption{Sample Table}\begin{tabular}{ccccc} \\ \hline
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
    TEST & TEST & TEST & TEST & TEST \\ \hline
    TEST & TEST & TEST & TEST & TEST \\
    TEST & TEST & TEST & TEST & TEST \\
    TEST & TEST & TEST & TEST & TEST \\ \hline
  \end{tabular}
\end{table}
%%------------------------

%%------------------------
\begin{table*}
\label{tab:opteng-tab02}  \centering
  \caption{Sample Table}\begin{tabular}{ccccccccc} \\ \hline
    % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
    TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST\\ \hline
    TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST\\
    TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST\\
    TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST & TEST\\ \hline
  \end{tabular}
\end{table*}
%%------------------------

%%------------------------------------------------------------------
\subsection{Figures and tables}

\textbf{Figures} should be set to one-column size ($\approx 8.3\,
\text{cm}$) whenever possible. This can be accomplished by setting
the graphic width equal to \verb+\linewidth+, for example, Figures
\ref{fig:opteng-fig01} and \ref{fig:opteng-fig02}. \textbf{Tables}
should also be set to one column whenever possible, as shown in
Table \ref{tab:opteng-tab01}. However, tables with more than five
columns will probably need to be set to two columns, like Table
\ref{tab:opteng-tab02}. For two-column layout, figures and tables
can be set across both columns with the alternate figure and table
environment commands \verb+\begin{figure*}...\end{figure*}+ instead
of \verb+\begin{figure}...\end{figure}+. Note that tables are
typeset and cannot be reduced in size like art, which may require
more space than in the submitted paper.

%%------------------------------------------------------------------
\subsection{\texttt{endfloat} package}

The \texttt{endfloat} package allows easy submission format. By
activating the package in the preamble, all figures and tables will
be put to the back of the document, and a list of figures will be
automatically generated.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{References and acknowledgments}

Reference callouts are formatted with the \texttt{overcite} package,
which produces superscript numerical reference callouts, for
example\cite{egan2006a}. Online callouts, for example, see
Ref.~\citeonline{egan2006b}, can be produced with the command
\verb+\citeonline{}+.

Before submitting, authors who use BibTeX should first run BibTeX,
then paste the contents of the output bbl file into the tex
manuscript file. Perhaps the \texttt{spiebib.bst} BibTeX style file
is the most suitable for an SPIE submission---in any case \emph{OE
Letters} references are significantly shortened by SPIE editors.

Both \textbf{Reference} and \textbf{Acknowledgments} are handled
with the \verb+\acksect+ environment. This gives the normal size
nonnumbered italic font, which SPIE journal use for these two final
sections.

\subsection{Biography}

The \emph{Optical Engineering} journal encourages short author
biographies. The can be facilitated as follows\footnote{from
\texttt{ieee.cls} by Gregory Plett and Istv\'{a}n Koll\'{a}r}

\scriptsize%
\begin{verbatim}
\begin{biography}[photograph.ps]{Patrick Egan}
received his BEng degree in electronic and computer engineering at
the University of Limerick, Ireland in 2003. He is currently
involved in doctoral research studies in a collaboration between the
University of Limerick and the European Commission Joint Research
Centre, Italy. In April 2006 he was awarded a postdoctoral research
associateship at the National Institute of Standards and Technology,
Gaithersburg, where he hope to commence research following
completion of his PhD. His research interests include optical
metrology, optoelectronics and signal processing.
\end{biography}
\end{verbatim}
\normalsize



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Remaining tasks}

%%------------------------------------------------------------------
\subsection{Figure captions and equations}

The figure and table captions should be flushed left. The equations
should also be flushed left and in serif font.


%%------------------------------------------------------------------
\subsection{OE Letters header font}

The \textsc{OE Letters} in the gray background header is not in the
correct font.


%%------------------------------------------------------------------
\subsection{Optical Engineering minipage}

The two \verb+minipage+ environments of the \emph{Optical
Engineering} option requires alignment. Currently the authors are
vertical center on the left column, while the abstract is vertical
top on the right. Both should be top vertical.




%%------------------------------------------------------------------
\subsection{Cover page: Author information}

The SPIE guidelines require a cover page with author information
(name, affiliation, email, phone number, fax number). The best
solution would be to automatically generate a table from the data
supplied to the \verb+\author+ and other input.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dummy section}

Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text.


%%------------------------------------------------------------------
\subsection{Dummy subsection}

Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text.

\subsubsection{Dummy subsubsection}

Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy
text. Dummy text. Dummy text. Dummy text. Dummy text. Dummy text.
Dummy text. Dummy text.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\acksect*{Acknowledgments}

The authors would like to acknowledge\ldots


%\pagebreak

% The bibliography can be generated using the following two lines:
%\bibliographystyle{spie}
%\bibliography{spiebib}

\begin{thebibliography}{99}

\bibitem{egan2006a}
P.~{Egan}, F.~{Lakestani}, M.~P. {Whelan}, and M.~J. {Connelly},
``Full-field optical coherence tomography with a complimentary
  metal-oxide semiconductor digital signal processor camera,'' \emph{Optical
  Engineering}, vol.~45, no.~1, p. 015601, Jan. 2006, available:
  \href{http://dx.doi.org/10.1117/1.2158968}{spiedl.aip.org}.

\end{thebibliography}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Biographies do not exist for OE Letters

\begin{biography}[fig_1]{Patrick Egan}
received his BEng degree in electronic and computer engineering at
the University of Limerick, Ireland in 2003. He is currently
involved in doctoral research studies in a collaboration between the
University of Limerick and the European Commission Joint Research
Centre, Italy. In April 2006 he was awarded a postdoctoral research
associateship at the National Institute of Standards and Technology,
Gaithersburg, MD USA, where he hope to commence research following
completion of his PhD. His research interests include optical
metrology, optoelectronics and signal processing.
\end{biography}


\end{document}
