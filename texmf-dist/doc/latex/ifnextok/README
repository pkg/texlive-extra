            README for the `ifnextok' package
       variant of \@ifnextchar not skipping spaces
                (C) Uwe Lueck 2011/06/02


`ifnextok' deals with the behavior of LaTeX's internal 
`\@ifnextchar' to skip blank spaces. This sometimes has surprising 
or for some users really unwanted effects, especially with 
brackets following `\\' when the user does not intend to specify 
an optional argument, rather wants that brackets are printed.
The package offers commands and options for modifying this 
behavior, maybe limited to certain parts of the document source.

A general method the package offers is 

    \usepackage[stdbreaks]{ifnextok}

so in "standard" circumstances `\\ [' prints a bracket in a 
new line. `\MakeNotSkipping<target>{<on-space>} modifies <target> 
so it executes <on-space> when followed by a space token.

KEYWORDS: macro programming, optional command arguments, 
          manual line breaks, humanities

RELATED PACKAGES: amsmath, mathtools

The package file `ifnextok.sty' and the documentation files
`ifnextok.pdf' and `ifnextok.tex' can be redistributed and/or 
modified under the terms of the LaTeX Project Public License; 
either version 1.3c of the License, or any later version, see

    http://www.latex-project.org/lppl.txt

We did our best to help you, but there is NO WARRANTY. 

The `ifnextok' package is author-maintained in the sense of 
this license.

The latest public version of the package is available at 

    http://mirror.ctan.org/macros/latex/contrib/ifnextok/

A TDS version `ifnextok.tds.zip' is available at

    http://mirror.ctan.org/install/macros/latex/contrib/

Please report bugs, problems, and suggestions via 

    http://www.contact-ednotes.sty.de.vu 


