  README.txt file for pm-isomath.sty  2021/08/24 v.1.2.00

 
  Distributable under the LaTeX Project Public License,
  version 1.3c or higher (your choice). The latest version of
  this license is at: http://www.latex-project.org/lppl.txt
  
  This work is "maintained"
  
  This work consists of this file pm-isomath.dtx, a README.txt file
  and the derived files:
      pm-isomath.sty, pm-isomath.pdf.
  
