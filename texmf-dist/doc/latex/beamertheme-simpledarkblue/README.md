# Simple Beamer Theme

A simple and clear beamer template.

**Website:**

https://github.com/PM25/SimpleDarkBlue-BeamerTheme

**Lead author:**

Pin-Yen Huang (pyhuang97@gmail.com)

**Compiled sample document:**

beamertheme-simpledarkblue-sample.pdf

## License

This is free and unencumbered software released into the public domain.
For more information, please see the file `LICENSE` or refer to <http://unlicense.org>.
