Minimal markup for documentation, text filtering
=================================================================
Release 0.43 synchronizes references to here, 
provides minor fixes, and enhances dialogues:

   `atari.cfg'  settings for conversion with:
  
   `atari.fdf'  from `morehype' exemplifies conversion with:

`copyfile.cfg'  initializes:

`copyfile.tex'  dialogue for compressing blank lines 
                (and text encoding conversion)

`fdtxttex.cfg'  initializes:

`fdtxttex.tex'  dialogue for .txt -> .tex conversion

`fddial0g.sty'  shared macros for the dialogues

`fifinddo.sty'  documentation fix \WriteResult -> \ResultFile

 `makedoc.cfg'  loads `color.sty'; provides \acro by `relsize.sty' 
                (with a funny protection for hyperref bookmarks); 
                uses \meta as semantical alias for \textit; 
                [new with release r.0.43a:]
                \MDkeywords passes keywords to \hypersetup 
                as well as to `MDabstract' environment

 `makedoc.sty'  documentation fix makedoc.cfg -> mdoccorr.cfg

`niceverb.sty'  \gt for >, \lt for <; 
                fixed noisy catcode changing in .aux 

  `README'      adjusted, enhanced

