Minimal markup for simple text (Wikipedia style) and documentation
==================================================================
New with release 0.42:

 `makedoc.sty'   \small starting \mdPackageCode spoiled all 
                 documentations 2009/2010! (fixed);
                 \ResetCodeLineNumbers fixed

`mdoccorr.cfg'  `<-' and `->' typeset arrows, `...' re-implemented
 
`fifinddo.sty'   new macros \IfFDpreviousInputEmpty ("compression" 
                 of empty lines), \BasicNormalCatcodes, \CopyFile* 
                 (compress), \CopyLine, \StartPrependingChain 
                 (per-line jobs), documentation modified

`niceverb.sty'   fixed `\ ' in auto mode; 
                 \ctanpkgref moves to `texlinks.sty'; 
                 additions in introduction

 `makedoc.cfg'   loads `hypertoc.sty' and `texlinks.sty'
 
-- dedicated to my sister for her birthday! 
  
  


