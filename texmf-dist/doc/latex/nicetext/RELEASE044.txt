Nice meta-verbatim, dialogues for .txt->.tex/encoding conversion
=================================================================
Release 0.44, 2011/09/14:

`niceverb.sty' v0.44 copes with design issue: on "\AddQuotes", 
                     "`...'" indeed surrounds inline TeX code 
                     with single quotation marks automatically
                     (besides meta-verbatim)

`fddial0g.sty' v0.2  all my ideas so far for dialogue programming 
                     tools carried out (handling "variables", 
                     displaying their values on a "screen", 
                     "\Spaces{15}" [e.g.] counts when expanding
                     w/o e-TeX); not super-comfortable, but fine

`fifinddo.sty' v0.43 new command useful in programming dialogues

dialogues/templates  adjusted

`u8atablg.fdf'       fifinddo rules for replacing UTF-8 chars
                     with TeX commands (and some extended ASCII)

(I use all of this for converting my Xpad/Tomboy notes into input 
 for my great Atari editor)

`mdoccheat.pdf'      was missing in previous release, back again
 
