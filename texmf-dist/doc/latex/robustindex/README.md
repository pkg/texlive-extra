Package robustindex.sty, Wilberd van der Kallen 2019.  
Date of last change of _anything_ in the bundle 2019/01/29  
 
 This package uses \pageref to ensure that the page numbers in the index are synchronous 
 with the manuscript, even when a third party changes the page numbers and fails to
 rerun makeindex.
 
 Some features of makeindex had to be disabled.
 
 [Home page of package](https://www.staff.science.uu.nl/~kalle101/stind.html)
 
 There is an option multind for many indexes with pagerefs.  
 It uses just one index file and relies on the old compile cycle
 (latex, makeindex, latex) in the standard TeX setup.
 
 Copyright 2005, 2017, 2018, 2019 Wilberd van der Kallen

 This package may be distributed under the conditions of the LaTeX Project Public
 License, either version 1.2 of this license or (at your option) any
 later version.  The latest version of this license is in
 [lpp.txt](http://www.latex-project.org/lppl.txt)
 and version 1.2 or later is part of all distributions of LaTeX
 version 1999/12/01 or later.
