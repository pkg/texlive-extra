% multisample.tex 2018/04/11
\documentclass{book}

\usepackage{makeidx}
\usepackage[multind]{robustindex}
\usepackage[plainpages=false,hyperindex=false,colorlinks=true]{hyperref}


\title {A file with several indexes}

\author{Wilberd van der Kallen\index{wilberd@Wilberd}}
\date{April 2018}

\makeindex    % obligatory with robustindex
%\altsort  % to get other sorting under option maltind in Version 2019/01/24 of robustindex .
% It places \index{\"U} after the alphabet, not before.
\begin{document}
%\renewcommand{\indexcapstyle}[1]{\par\vskip1em plus.5em minus.3em\textbf{\uppercase{#1}}}
\renewcommand\thepage{\roman{page}}
\index{z}

\setindex{b1}


\maketitle

\tableofcontents
\addcontentsline{toc}{chapter}{Contents}




\chapter*{Preface}

\addcontentsline{toc}{chapter}{Preface}
\markboth{Preface}{Preface}
This file illustrates some features of the \verb!robustindex! package with option \verb!multind!. This option
 supports multiple indexes. 
 
 Although this is not a book, we have chosen the book style for illustration.
Let us say we want to use four indexes. First we choose tags to identify them.
In this file we use the tags \verb|main|, \verb|bis|, \verb|a1|, \verb|b1|.
With the \verb!\setindex! command we activate an index.
Thus \verb!\setindex{main}! tells \LaTeX\ that the active index is now the index identified by the tag
\verb|main|, until the next \verb!\setindex! command. If a tag has not been encountered 
before, then a new index with that tag is created. The tag \verb|main| is always known and
the index with tag \verb|main|  becomes active at the \verb|\begin{document}|.
The \verb!\index! command and
the \verb|\printindex| commands write to/from the active index.\\

If you wish you may use \verb!\sindex[bis]{!\emph{entry}\verb!}! as shorthand for\\

\hspace{5em} \verb!\setindex{bis}\index{!\emph{entry}\verb!}!\\
 
and \verb!\sindex{!\emph{entry}\verb!}! as shorthand for\\

 \hspace{5em} \verb!\setindex{main}\index{!\emph{entry}\verb!}!.\\


To see how option \verb|multind| may be used, you may also compare the file
\verb|multisample.tex| with the output.

This preface has an \emph{indexed term}.\setindex{b1}\index{indexed term} (You see this in the source file only.)

\chapter{Introduction}\sloppy
\renewcommand\thepage{\arabic{page}}
\addtocounter{page}{-2}% Just to get overlap in roman and arabic page numbers.
This introduction has another indexed term 
\setindex{a1}
\index{other term!cited twice on same page}%
as well as the old indexed term.\setindex{b1}\index{indexed term}
The page number is again 5, but now in arabic.
If you index the same term twice on the same page, then one simply gets
a repeat in the index.\setindex{a1}\index{other term!cited twice on same page}
That is inherent in our method.
Indeed we had to disable the implicit page range formation of makeindex.

The index file {\it filename}.\texttt{ind} has become quite unreadable. Do not edit it.
Use the option \verb!multind! only if you are willing to keep the indexes standard. 
Note that if you open a page range you must close
it again with the same index active as when you opened the range.

We have hacked various mechanisms of makeindex. 
Subentries are still supported. We illustrate this in
\verb|multisample.tex| and appendix \ref{appendix} with alpha, beta and gamma.
This also illustrates the use of \verb|\gobblepageref|, a variant of \verb!\see!.
\setindex{a1}\index{alpha!see beta}
\index{alpha!see also gamma\gobblepageref}

After running \LaTeX\ on {\it filename}.\verb!tex! one must run makeindex on {\it
filename} to get the index entries in {\it filename}.\texttt{ind}.
Before this there may be error messages about labels or names.
Do not exclude any relevant files. For instance, do not use
\verb|\includeonly|.

The package \verb|hyperref| may break our code. Therefore
one must use the option
\verb|hyperindex=false|, as in
\verb|\usepackage[hyperindex=false]{hyperref}|.
Note that \verb|robustindex| already puts
hyperlinks in the index. Alternatively one may go against the advice of
\verb|hyperref| and put \verb|robustindex| later in the preamble.
In the file \verb|multisample.tex| we actually used
\verb|\usepackage[plainpages=false,hyperindex=false]{hyperref}|
because of the roman page numbers in the beginning.

\setindex{b1}\index{page range!risky@this $\vert$is$\vert$ complicated|(textbf}%
\index{page range|(textbf}%
\index{page range!risky@this $\vert$is$\vert$ complicated|textbf}%
\index{page range!risky@this $\vert$is$\vert$ complicated}%
\index{page range!risky@this $\vert$is$\vert$ complicated|textbf}%
%
\index{simpler!page range|(}%
\index{simplest page range|(}%

\appendix
\chapter{Structure of the \LaTeX\ file}\label{appendix}
\begin{verbatim}
\documentclass{book}

\usepackage{makeidx}
\usepackage[multind]{robustindex}
\usepackage[plainpages=false,hyperindex=false]{hyperref}
...
...\author{Wilberd van der Kallen\index{wilberd@Wilberd}} 
...
\makeindex    % obligatory with robustindex

\begin{document}
...
\tableofcontents
...\index{indexed term}                          % on page v
...
...\index{other term!cited twice on same page}   % on page 5
...\index{indexed term}                          % on page 5
...\index{other term!cited twice on same page}   % on page 5
...
\setindex{a1}\index{alpha!see beta}              % on page 5          
\index{alpha!see also gamma\gobblepageref}       % on page 5
...
\indexincontents %  before the index.
\setindex{main}
\printindex
...
\renewcommand{\indexcapstyle}[1]{\indexspace\textsc{#1}\par}
\renewcommand{\indexname}{Index with headings}
\setindex{main}
\printindex
\end{document}
\end{verbatim}

\index{as! t@is}

\index{tris}

\setindex{main}\index{alpha}

\setindex{a1}\index{gamma}

\setindex{bis}\index{bis}

\setindex{main}\index{delta!vardelta}


\setindex{main}\index{animal!than@fish}

\setindex{main}\index{animal!ruminant!cow}

\setindex{main}\index{time!here@now}


\setindex{b1}\index{page range!risky@this $\vert$is$\vert$ complicated|)textit}

\chapter{Purpose of \texttt{robustindex.sty}}\label{purpose}
Amazingly often a third party changes the page numbers without rerunning
makeindex. It is much nicer to have indexes that adapt themselves to all
changes that do not involve
\verb|\index{|\textit{entry}\verb|}| commands.

The package achieves this
by invoking the \verb|\pageref| mechanism on automatically generated labels
of the form \verb|ind.2|, \verb|ind.4|, \dots.

Only after adding/deleting/modifying an \verb|\index{|\textit{entry}\verb|}|
command, or after changing the order of \verb|\index{|\textit{entry}\verb|}|
commands, does one have to run makeindex. But then it is important that you
do run makeindex, even when no warning is issued. The robustness has its
limits.

With option \verb!multind! the package supports multiple indexes. But one still works with the standard compilation sequence\\
latex \emph{filename}.tex, makeindex \emph{filename}, latex \emph{filename}.tex.

\index{page range|)textit}%

\sindex[b1]{page range!risky@this $\vert$is$\vert$ complicated|texttt}

%
\setindex{b1}\index{page range!with risky label\label{range}}
% This label actually works and was used above in a \pageref{range} command.
%
\index{simpler!page range|)}%
\index{simplest page range|)}%


% If you want the index in the table of contents, you may do
%
% \clearpage
% \phantomsection
% \addcontentsline{toc}{chapter}{\indexname}
%
% We have a command for this:

\indexincontents % before \printindex




\renewcommand{\indexname}{Index}
\setindex{main}
\printindex

\renewcommand{\indexname}{Index bis}
\setindex{bis}
\printindex

\renewcommand{\indexname}{Index with tag a1}
\setindex{a1}
\printindex


\renewcommand{\indexname}{Index with tag b1}
\setindex{b1}
\printindex


\renewcommand{\indexcapstyle}[1]{\indexspace\textsc{#1}\par}
\renewcommand{\indexname}{Index with headings}
\setindex{main}
\printindex
\end{document}
