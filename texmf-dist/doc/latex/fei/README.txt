-----------------------------------------------------------------------------------------------------
fei --- Class for the creation of academic works under the typographic rules of FEI University Center
Author: Douglas De Rizzo Meneghetti
E-mail: douglasrizzo@fei.edu.br

Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
-----------------------------------------------------------------------------------------------------

fei is a class created by graduate students and LaTeX enthusiasts that allow students from FEI University Center to create their academic works, be it a monograph, master's thesis or PhD dissertation, under the typographic rules of the institution. The class makes it possible to create a full academic work, supporting functionalities such as cover, title page, catalog entry, dedication, summary, lists of figures, tables, algorithms, acronyms and symbols, multiple authors, index, references, appendices and attachments.

fei is loosely based in the Brazilian National Standards Organization (Associa^^c3^^a7^^c3^^a3o Brasileira de Normas T^^c3^^a9cnicas, ABNT) standards for the creation of academic works, such as NBR 14724:2011, NBR 6023:2018 and NBR 10520:2002.
In the manual, users will find detailed information regarding the class commands, environments and best practices to create an academic text of good quality. We also made available a few template files which students may use as a starting point for their texts.

# License
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt

# Latest releases and version control
CTAN is always the place to go to get the stable version of the class. To know the changes done to the class and get previous and developments versions, visit https://www.github.com/douglasrizzo/Classe-Latex-FEI/.
