The `eqnnumwarn` package 
------------------------

Sometimes an equation is too long that an equation number will be typeset below the equation itself, but yet not long enough to yield an "overfull `\hbox`" warning.  The `eqnnumwarn' package modifies the standard `amsmath` numbered equation environments to throw a warning whenever this occurs.

___

##### Licenscing
Copyright (c) 2017 by Jonathan Gleason <jgleason@math.berkeley.edu>

This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or (at your option) any later version.  The latest version of this license is in http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

This work consists of the file `eqnnumwarn.sty`, `eqnnumwarn.tex`, and `eqnnumwarn.pdf`.