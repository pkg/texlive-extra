Textpos: absolute positioning of text on the LaTeX page
=======================================================

Version 1.10.1, 2022 July 23

This package facilitates placing boxes at absolute positions on the
LaTeX page.  There are several reasons why this might be useful, but
the main one (or at least my motivating one) is to help produce a
large-format conference poster.

This package provides a single environment, plus a starred variant,
which contains the text (or graphics, or table, or whatever) which is
to be placed on the page, and which specifies where it is to be
placed.  The environment is accompanied by various configuration
commands.

Textpos has a canonical home page at <https://purl.org/nxg/dist/textpos>.
The CTAN URL for Textpos is <https://ctan.org/pkg/textpos>.
The source is hosted [online](https://heptapod.host/nxg/textpos).

For the change history, see textpos.html.


Installation
------------

Download or find the file `textpos.sty`.  Install `textpos.sty` somewhere
LaTeX will find it (see [the TeX FAQ](https://texfaq.org/FAQ-inst-wlcf)).

This package requires the services of Martin Schroeder's package
[everyshi][everyshi].  You will need to download this package from CTAN first.

Licence
-------

This software is copyright, 1999-2020, Norman Gray.
It is released under the terms of the [LaTeX Project Public License][lppl],
which is included in the file LICENCE.



Norman Gray  
<https://nxg.me.uk>

[lppl]: https://www.latex-project.org/lppl.txt
[everyshi]: https://www.ctan.org/pkg/everyshi
