# The `mluexercise` class

Template class for exercises/homework, maintained by the [Computer Science Student's Council](https://fachschaft.mathinf.uni-halle.de) at [Martin Luther University Halle-Wittenberg](https://uni-halle.de).

## Contact

We are happy to accept pull requests and feature requests on [GitHub](https://github.com/fsrmatheinfo/mluexercise).
Alternatively, you can contact us via email: [fachschaft@mathinf.uni-halle.de](mailto:fachschaft@mathinf.uni-halle.de)

## License

The `mluexercise` LaTeX class is distributed under the [MIT license](https://ctan.org/license/mit).
