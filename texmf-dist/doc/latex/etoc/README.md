<!-- -->

    Source:  etoc.dtx (1.1c)
    Author:  Jean-Francois Burnol
    Info:    Completely customisable TOCs
    License: LPPL 1.3c
    Copyright (C) 2012-2023 Jean-Francois Burnol.
    <jfbu at free dot fr>

ABSTRACT
========

The etoc package gives to the user complete control on how the entries
of the table of contents should be constituted from the *name*,
*number*, and *page number* of each sectioning unit. This goes via the
definition of *line styles* for each sectioning level used in the
document. The package provides its own custom line styles. Simpler
ones are given as examples in the documentation. The simplest usage
will be to take advantage of the layout facilities of packages dealing
with list environments.

Regarding the *global toc display*, etoc provides pre-defined styles
based on a multi-column format, with, optionally, a ruled title or
framed contents.

The `\tableofcontents` command may be used arbitrarily many times and
it has a variant `\localtableofcontents` which prints tables of
contents 'local' to the current surrounding document unit. An
extension of the `\label/\ref` syntax allows to reproduce (with
another layout) a local table of contents defined somewhere else in
the document.

Via *depth tags*, one gets an even finer control for each table of
contents of which sectioning units it should, or not, display.

The formatting inherited (and possibly customized by other packages)
from the document class will be used when in compatibility mode.

The assignment of levels to the sectioning units can be changed at any
time, and etoc can thus be used in a quite general manner to create
custom ''lists of'', additionally to the tables of contents related to
the document sectioning units. No auxiliary file is used additionally
to the standard `.toc` file.

INSTALLATION
============

To extract the package (.sty) and driver (.tex) files from etoc.dtx,
execute `etex etoc.dtx`.

It is also possible to execute latex or pdflatex directly on etoc.dtx.

To produce etoc.pdf one can run pdflatex directly on etoc.dtx or on
the file etoc.tex which was extracted from `etex etoc.dtx` step.

Options can be set in etoc.tex:

- scrartcl class options (paper size, font size, ...)
- with or without source code,
- with dvipdfmx or with latex+dvips or pdflatex.

Since release 1.08h pdflatex is the default in etoc.tex (prior it
was latex+dvipdfmx as it produces smaller PDFs) in order to allow
inclusion via the use of package `attachfile` of about 25 code
samples as file attachment annotations.

Installation:

    etoc.sty    -> TDS:tex/latex/etoc/etoc.sty
    etoc.dtx    -> TDS:source/latex/etoc/etoc.dtx
    etoc.pdf    -> TDS:doc/latex/etoc/etoc.pdf
    README.md   -> TDS:doc/latex/etoc/README.md

The other files may be discarded.

LICENSE
=======

This Work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, in its
version 1.3c. This version of this license is in

> <http://www.latex-project.org/lppl/lppl-1-3c.txt>

and the latest version of this license is in

> <http://www.latex-project.org/lppl.txt>

and version 1.3 or later is part of all distributions of
LaTeX version 2005/12/01 or later.

The Author of this Work is Jean-Francois Burnol <jfbu at free dot fr>

This Work consists of the main source file etoc.dtx and the derived files
etoc.sty, etoc.tex, etoc.pdf, etoc.dvi.

RECENT CHANGES
==============

1.1c \[2023/01/20\]
-------------------

Fix a brace removal bug in the construction of `\etocname`.  It
remained without visible effects in documents using `hyperref` and
default settings, thanks to the hyperlink wrapper, but e.g.
`\section{{\color{blue}Blue}}` in a document not using `hyperref`,
and not using etoc only in "compatibility mode", could cause a
color leak in the table of contents.

With the KOMA-script numberline toc feature, unnumbered entries in
TOCs typeset via etoc user-defined or package provided line styles
but using compatibility mode for the global display style were
(knowingly) considered to be numbered with an empty number.  They
are now considered by `\etocifnumbered` to be not numbered and the
empty `\etocnumber` will carry no hyperlink.

Fix a 1.1a regression in the context of KOMA-script unnumbered TOC
entries: `\etocthelinkedname` could lose its hyperlink.

Continue internal trimming of old code branches which became
un-needed after the 1.1a refactoring.  Add relatively decent
code comments to accompany the 1.1a-c refactoring.  Update
warning messages to use more consistently LaTeX's templates.

1.1b \[2023/01/15\]
-------------------

Documentation fix, 1.1a forgot to mention the following change:
`\etocthelinkedname`, `\etocthelinkednumber`, `\etocthelinkedpage`
are now always hyperlinks independently of linktoc status.

1.1a \[2023/01/14\]
-------------------

This version brings no new functionality, despite the number bump.
It implements a complete rewrite of old legacy core internals.
Formerly, etoc waited for
[hyperref](https://ctan.org/pkg/hyperref)
(if present) to have added hyperlinks via its patch to LaTeX's
`\contentsline`. etoc examined the arguments of `\l@section` and
other commands to extract hyperlinking information, if any.
With this release etoc decides earlier according to
[hyperref](https://ctan.org/pkg/hyperref)
linktoc status whether section names and page numbers
should be hyperlinked, and adds links itself via `\hyperlink`.
etoc is thus now immune to the details of how hyperref patches
the `\contentsline` command, which is not executed anymore.
Overall, the code is greatly simplified.

`\etoclink` now wraps its argument in an hyperlink even if
[hyperref](https://ctan.org/pkg/hyperref) is configured via
`linktoc=none`.  Formerly no hyperlink was added then.

Deprecation of `\etocsavedsectiontocline` and similarly named
commands.  They are not needed as `\l@section` et al. are with this
release left unmodified during the table of contents typesetting.

LaTeX kernel from `2020/10/01` or later is required (to allow
assuming the `\contentsline` entries in the TOC file always have
four arguments).

1.09i \[2022/11/21\]
--------------------

Fix bug showing when a document uses both `\etocchecksemptiness` and
`\etocsetlocaltop.toc`: the `start` and `finish` parts of some levels
were executed possibly causing extra printed output.

More hyperlinking in the implementation part of the documentation.

1.09h \[2022/11/20\]
--------------------

Documentation improvements.  In particular, attached code snippets
are now visible via their filenames in the page margins.  Also,
command names are doubly hyperlinked: first half links to the devoted
part of the user manual, second half links to the implementation part.

1.09g \[2022/11/17\]
--------------------

Compatibility hotfix with recent hyperref `7.00u` of `2022-11-13`.
Thanks to Denis Bitouzé for signaling the breakage to the author.

1.09f \[2022/08/30\]
--------------------

No more shipping of a German translation of the documentation, as
it was last updated in April 2015.

(etoc.pdf) User level commands hyperlink from their code source
definitions to their descriptions in the documentation part.  Macros
used in the code source hyperlink to where they first got defined
there.

Wrap the `\etocpartname` (from etoc's package provided toc line style)
together with the part number in a potential common hyperlink.

Try to sync the emulation of the global display style with KOMA-script
`v3.37` (in particular regarding the `noparskipfake` KOMA toc feature).

Improve documentation of some aspects under `memoir` class.

Remove the `\nonumberline` token, even though empty, from the meaning of
`\etocthename` (KOMA-script classes).

Add `\etocimmediatedepthtag.toc` to work around problems related to
`\include` (see user doc).  Thanks to Norman Ramsey who reported the problem
and proposed a work-around in July 2016.  Apologies for the somewhat longish
delay in incorporating it...

Also add `\etocimmediatesettocdepth.toc`.

Also add `\etocimmediatetoccontentsline` and its starred variant.

Also add `\etocimmediatesetlocaltop.toc`.

Fix an obscure bug (see source code comments) in the `\etocsetlocaltop.toc`
mechanism.
