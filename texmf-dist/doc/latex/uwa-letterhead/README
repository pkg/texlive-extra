This package generates the letterhead of the University of Western Australia.

It requires the UWA logo in PDF format, which is available in SVG format at
https://static-listing.weboffice.uwa.edu.au/visualid/core-rebrand/img/uwacrest/,
and uses the Arial (https://docs.microsoft.com/en-us/typography/font-list/arial)
and UWA Slab (https://www.brand.uwa.edu.au/) fonts by default.

Copyright 2019, 2021 Anthony Di Pietro

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Anthony Di Pietro.

This work consists of the files uwa-letterhead.dtx, uwa-letterhead.ins, and
uwa-letterhead-example.tex and the derived files uwa-letterhead.sty and
uwa-letterhead.pdf.
