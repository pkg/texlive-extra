% \draftfigure --%
%            modifying drafted figures
% Copyright (c) 2017 Lukas C. Bossert
%  
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
% !TEX program = lualatex
\documentclass[a4paper,
10pt,
english
]{ltxdoc}


\input{draftfigure-preamble.tex}
\newcommand\df{draftfigure\xspace}
\newcommand\dfstring{|draftfigure|\xspace}
\setkeys{Gin}{width=.5\linewidth}

\begin{document}
\AddRevision{2017-07-19}{Improvement of coding}{Lukas C. Bossert}
\AddRevision{2017-05-07}{Documentation completed}{Lukas C. Bossert}
\AddRevision{2017-05-06}{Initial release}{Lukas C. Bossert}


\title{\texttt{\df} -- \\ modifying drafted figures\footnote{%
For further information about the code visit \href{https://github.com/LukasCBossert/draftfigure}{https://github.com/LukasCBossert/draftfigure}
Comments and criticisms are welcome.}}
\author{Lukas C. Bossert\\{\small \href{mailto:lukas@digitales-altertum.de}{lukas@digitales-altertum.de}}}
\date{Version: \dfdate{} (\dfversion)} 
\maketitle

\begin{abstract}
\noindent This package allows you to customize the outcome of drafted figures.
\end{abstract}


\begin{multicols}{2}
\footnotesize\parskip=0mm \tableofcontents
\end{multicols}

\section{Installation}
\dfstring is part of the distributions MiK\TeX \footnote{Website: \url{http://www.miktex.org}.} 
and \TeX Live\footnote{Website: \url{http://www.tug.org/texlive}.}~-- thus, you
can easily install it using the respective package manager. 
If you would like to
install \dfstring manually, do the following:
Download the folder \dfstring with all relevant files from the CTAN-server\footnote{\url{https://www.ctan.org/pkg/\df}} and copy the content of the |zip|-file to the \texttt{\$LOCALTEXMF} directory of
 your system.\footnote{If you don't know what that is, have a look at
\url{http://www.tex.ac.uk/cgi-bin/texfaq2html?label=tds} or 
\url{http://mirror.ctan.org/tds/tds.html}.} 
Refresh your filename database.

Here is some additional information from the UK \TeX\ FAQ:
\begin{itemize}[nosep,after=\vspace{-\baselineskip} ]
  \item \href{%
    http://www.tex.ac.uk/cgi-bin/texfaq2html?label=install-where}{%
    Where to install packages}
  \item \href{%
    http://www.tex.ac.uk/cgi-bin/texfaq2html?label=inst-wlcf}{%
    Installing files \enquote{where \LaTeX /TeX\ can find them}}
  \item \href{%
    http://www.tex.ac.uk/cgi-bin/texfaq2html?label=privinst}{%
    \enquote{Private} installations of files}
\end{itemize}

%%introduction from biblatex-dw copied and applied. might to be rewritten.

\clearpage
\section{Status Quo}

Usually you see all of your images in your PDF:
\begin{examplebox}
\begin{figure}[H]
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
  \includegraphics{example-image-c}
  \caption{C}
\end{figure}
\end{examplebox}
Sometimes you do not need to see the images or want to omit them due to faster compiling or you do not have the right to publish them.


You can \enquote{switch off} the images by using the option |draft| either with 
|\documentclass[draft]|\marg{class} 
%\cs{documentclass}\oarg{draft}\marg{clss} 
or with 
|\usepackage[draft]{graphicx}|.
%\cs{usepackage}\oarg{draft}\marg{graphicx}


If you do not like the outcome with the filename and its path written with |\ttfamily| but still need the same size of the image in the text then use this package \dfstring to modify the outcome.
\clearpage

\section{Usage}
 \DescribeMacro{draftfigure}  The name of the package is  \dfstring which has to be activated in the preamble. 

\begin{code}
\usepackage[*@\meta{further options}@*]{draftfigure}
\end{code}
Without any further options you will now get white boxes instead of the images.
\begin{examplebox}
\begin{figure}[H]\setkeys{Gin}{draft=true}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
  \includegraphics{example-image-c}
  \caption{C}
\end{figure}
\end{examplebox}

\clearpage

\section{Options}
If you like the plain white boxes you don’t need to do anything else. But if you like to change that use one or more of the following options.

So let’s see some action.
\subsection{Preamble}
The following options are set in the preamble and considered globally for all figures.
\subsubsection{content}
\DescribeMacro{content}
With this option you can set an individual text which will be displayed in the white box.
\begin{code}
\usepackage[%
  content = {This figure is omitted due to missing copyrights.},%
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{draftfigure}{content = {This figure is omitted due to missing copyrights.}}
	\setkeys{Gin}{draft=true}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
\end{figure}
\end{examplebox}
\clearpage
\subsubsection{filename}
\DescribeMacro{filename}
Activating this option with |true| the filename will be displayed in the white box.
If you also have defined some text for |content| the filename will appear one line below of |content|.
\begin{code}
\usepackage[%
  filename,%
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{Gin}{draft=true}
	\setkeys{draftfigure}{filename}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
  \includegraphics{example-image-c}
  \caption{C}
\end{figure}
\end{examplebox}
\clearpage
\DescribeMacro{filename}
\DescribeMacro{content}
And with some text in |content| it will look like this:
\begin{code}
\usepackage[%
  filename,%
  content={no image available}
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{Gin}{draft=true}
	\setkeys{draftfigure}{filename,
	content={no image available}}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
  \includegraphics{example-image-c}
  \caption{C}
\end{figure}
\end{examplebox}
\clearpage
\subsubsection{style}
\DescribeMacro{style} This defines the font style of the text of |content| or |filename|. 
You can chose one of the following styles: 
\begin{multicols}{3}
\begin{itemize}
	\item[sf] |\sffamily|
	\item[tt] |\ttfamily|
	\item[rm] |\rmfamily|
	\item[normal] |\normalfont|
	\item[bf] |\bfseries|
	\item[it] |\itshape|
	\item[sc] |\scshape|
\end{itemize}
\end{multicols}
Default is |sf|.
\DescribeMacro{content}
\DescribeMacro{style}
\begin{code}
\usepackage[%
  content={hidden image},
  style = {bf},%
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{Gin}{draft=true}
	\setkeys{draftfigure}{style={bf},content={hidden image}}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
  \includegraphics{example-image-b}
  \caption{B}
  % \includegraphics{example-image-c}
 %  \caption{C}
\end{figure}
\end{examplebox}
\clearpage
\subsubsection{size}
\DescribeMacro{size}
You can also define the text size of |content| with predefined values: 
\begin{multicols}{2}
\begin{itemize}
	\item[tiny] |\tiny|
	\item[scriptsize] |\scriptsize|
	\item[small] |\small|
	\item[footnotesize] |\footnotesize|
	\item[normal] |\normalsize|
	\item[large] |\large|
	\item[Large] |\Large|
	\item[LARGE] |\LARGE|
	\item[huge] |\huge|
\end{itemize}
\end{multicols}
Default is |small|.
\DescribeMacro{content}
\DescribeMacro{size}
\begin{code}
\usepackage[%
  content={text instead of an image},
  size = {huge},%
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{Gin}{draft=true, width =.6\linewidth}
	\setkeys{draftfigure}{
  content={text instead of an image},
  size = {huge},%
	}
  \centering
  \includegraphics{example-image-a}
  \caption{A}
   \includegraphics{example-image-b}
   \caption{B}
  % \includegraphics{example-image-c}
 %  \caption{C}
\end{figure}
\end{examplebox}

\clearpage

\subsubsection{position}
\DescribeMacro{position}
You can also decide where your text from |content| has to be placed
\begin{multicols}{3}
\begin{itemize}
	\item[left] |\raggedright|
	\item[center] |\centering|
	\item[right] |\raggedleft|
\end{itemize}
\end{multicols}
Default is |left|.
\DescribeMacro{content}
\DescribeMacro{position}
\begin{code}
\usepackage[%
  content={positioning},
  position = {center},%
  ]{draftfigure}
\end{code}
\begin{examplebox}
\begin{figure}[H]
	\setkeys{Gin}{draft=true,width=.45\linewidth}
	\setkeys{draftfigure}{
 content={positioning},
 position = {center},%
 }
  \centering
  \includegraphics{example-image-a}
  \caption{A}
   \includegraphics{example-image-b}
   \caption{B}
   \includegraphics{example-image-c}
  \caption{C}
\end{figure}
\end{examplebox}

\clearpage
\subsubsection{allfiguresdraft}
\DescribeMacro{allfiguresdraft}
Enabling |allfiguresdraft| all figures in your text will be shown as a white box. This is the same as |\usepackage[draft]{graphicx}|.

\subsubsection{noframe}
\DescribeMacro{noframe}
The white boxes of the replaced figures are shown with no frame and no border, but still same sizes as the figure it represents.


\subsection{Individually possible options}

Since you can also set |draft| to individual images you can also pass some of the options of \dfstring to individual images.

\DescribeMacro{style}
\DescribeMacro{size}
\DescribeMacro{position}
\DescribeMacro{content}
\DescribeMacro{filename}These are |style|, |size|, |position|, |content|, |filename|.

In the preamble there is this setup
\begin{code}
\usepackage{draftfigure}
\end{code}
So no options are enabled globally.

To pass an option to an image you need to insert in the |figure|-environment the following:
\begin{code}
\setkeys{draftfigure}*@\marg{options}@*
\end{code}
or -- a little bit shorter:
\begin{code}
\setdf*@\marg{options}@*
\end{code}

The overview starts on the next page.
\clearpage 
\setkeys{Gin}{width=.4\linewidth}
\begin{codeexample}
\begin{figure}[H]
 \centering
 \setkeys{draftfigure}{content={white box}}
  \includegraphics[draft]{example-image-a}
  \caption{A}\label{A}
 \setkeys{draftfigure}{filename=true}
  \includegraphics[draft]{example-image-b}
  \caption{B}\label{B}
 \setkeys{draftfigure}{content={another white box}}
  \includegraphics[draft]{example-image-c}
  \caption{C}\label{C}
\end{figure}
\end{codeexample}
You probably noticed that I defined some text within |content| for \cref{A}.
This text is also shown for \cref{B} although I didn’t define it again --
the options you define with |\setkeys{draftfigure}|\marg{options}
are valid (in an environment) until they are refined 
as this happens for \cref{C}.

The same effect you see with the option |filename| which is enabled for \cref{B} and still valid for \cref{C}.

On the next page I show how to deactivate certain options.
\clearpage
\begin{codeexample}
\begin{figure}[H]
 \centering
 \setkeys{draftfigure}{content={white box}}
  \includegraphics[draft]{example-image-a}
  \caption{A}\label{A1}
 
 \setkeys{draftfigure}{filename=true,
 style={tt},position={right}}
  \includegraphics[draft]{example-image-b}
  \caption{B}\label{B1}

 \setkeys{draftfigure}{content={hidden image},
   style={rm},filename=false,position={center}}
  \includegraphics[draft]{example-image-c}
  \caption{C}\label{C1}
\end{figure}
\end{codeexample}
%Now you can see that for \cref{C1} I set |filename=false| so it isn’t shown in the white box.

\clearpage
\section{Things to work on}
There are still things which work not satisfactually:
\begin{itemize}
	\item  noframe for individual image
\item  combine different styles: sc and sf etc.
\item  color frame
\item  image instead of text in background
\end{itemize}
If anyone has some ideas please let me know: \href{mailto:lukas@digitales-altertum.de}{lukas@digitales-altertum.de}

\section{Changelog}
\PrintRevisions

\end{document}