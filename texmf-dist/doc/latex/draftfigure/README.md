draftfigure    
======= 

a package to replace figures with a white box and additional features
---
_2017/07/19 v0.2_

Copyright (c) 2017 Lukas C. Bossert

[info@biblatex-archaeologie.de](mailto:info@biblatex-archaeologie.de) | [www.digitales-altertum.de](http://www.digitales-altertum.de)

With this package you can control the outcome of a figure which is set to `draft` and modify the display with various options.

---

This package contains:

documentation:

- draftfigure.pdf
- draftfigure.tex

style-file:

- draftfigure.sty


This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in [http://www.latex-project.org/lppl.txt](http://www.latex-project.org/lppl.txt) and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

---
This work has the LPPL maintenance status _maintained_.
The current maintainer of this work is [Lukas C. Bossert](https://github.com/LukasCBossert).

