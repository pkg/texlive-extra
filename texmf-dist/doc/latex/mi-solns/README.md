The mi-solns Package
Author: D. P. Story
Dated: 2018/12/18

This package is designed to mark a solution environment of an exercise or 
quiz and insert it into the same or a different document. Solutions are 
ones created by either the exerquiz or eqexam package. 

All PDF creators are supported.

D. P. Story
www.acrotex.net
dpstory@uakron.edu
dpstory@acrotex.net
