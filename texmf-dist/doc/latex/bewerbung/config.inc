%%
%% This is file `config.inc',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% bewerbung.dtx  (with options: `config.inc,package')
%% 
%%  bewerbung.dtx
%%  Copyright 2022-01-17:16:26:37 -- Version 1.3 André Hilbig, mail@andrehilbig.de
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is André Hilbig, mail@andrehilbig.de.
%% 
%%  This work consists of the files bewerbung.dtx and bewerbung.ins and the derived files bewerbung-cv-casual.sty, bewerbung-cv-classic.sty, bewerbung-cv-oldstyle.sty, bewerbung-cv.sty, bewerbung.sty, argetabelle.cls, bewerbung.cls, neueBewerbung.sh, config.inc, titlepage.inc.
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%% zusätzliche
%% Farbdefinitionen:
%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
%%                                                                                                 +
%% Konfiguration der eigenen Daten                                                                 +
%%                                                                                                 +
\Name{Musterfrau}
\Vorname{Maja}
\Street{Musterstraße 9}
\Plz{45878}
\Stadt{Musterstadt}
\MeinBeruf{Dipl.-Pädagogin (Univ.)}
\EMail{mail@email.com}
\Tel{03131~465488}
\Mobile{01534~6324524353}
\Sta{deutsch}
\GebDatum{31.02.1990}
%%                                                                                                 +
%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
%%                                                                                                 +
%% +----Achtung--------------------------------------+
%% + ID meint (Zeilennummer-1) und nicht das Feld id +
%% + Bsp: Eintrag in Zeile 48: ID 47                 +
%% +                                                 +
%% + Außer der Klasse ahilbig-bewerbung wird die     +
%% + Option idPlain mitgegeben. Dann wird nach dem   +
%% + exakten Match im Feld id gesucht.               +
\ID{1}
%% +-------------------------------------------------+
\Anhang{Abiturzeugnis.\newline Diplomzeugnis.}{%
\item Abiturzeugnis
\item Diplomzeugnis
}
\endinput
%%
%% End of file `config.inc'.
