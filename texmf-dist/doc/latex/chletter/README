{chletter}[2010/10/10 v2.0 Swiss letter document class]

The chletter class is suited for typesetting letters with letterhead 
corresponding to the Swiss norm SN 010130.

The default letterhead fits into ISO 269 C5 & C6/5 windowed envelopes,
both right windowed and left windowed.

This class is mostly compatible with the standard LaTeX2e classes. It
is not limited to letters and may be used as a generic document class.

Its basic usage is very simple and user friendly. It is appropriate to
quickly typeset casual documents and letters.

However the class is highly configurable and may be used within complex
setups to provide automated letters composition.

The chletter code is very compact and highly optimized, dropping the
obsolete legacy (namely LaTeX 2.09) for the sake of efficiency.

Provided files:
chletter.dtx  code and documentation
chletter.ins  install script
chletter.pdf  documentation

Copyright 2010 Boris Oriet
The licence is LPPL 1.3c.