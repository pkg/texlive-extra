# inlinelabel

This package can assign equation numbers to inline equations. When Japanese is supported, you can switch to circled equation numbers.


## Documents

Documents for this package are available in English and Japanese.


## License

This package released under [the MIT license](https://ctan.org/license/mit).

(C) 2022 Yukoh KUSAKABE


## Revision History

+ Version 1.2.1 2022-07-08
  + Add the .tex source of the documentation.
  + Improve the documentation.
+ Version 1.2 2022-07-08
  + Add the \equationreset.
  + Improve the documentation.
+ Version 1.1 2022-07-07
  + Rewrite README.
  + Add the document (inlinelabel.pdf).
  + Add the [luacircled] option.
+ Version 1.0 2022-06-28
  + The first public version.


---

[Yukoh KUSAKABE](https://twitter.com/metaphysicainfo) (screen-name) at [metaphysica.info](https://www.metaphysica.info/)