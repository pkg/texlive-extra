The readarray package allows one to input formatted data into elements
of a 2-D or 3-D array (or a 1-D file-record array).  Subsequently, that
data may be recalled at will using an array-cell indexing nomenclature.
The data can be but need not be numerical in nature.  It can be, for
example, formatted text.  In this regard, the array stores the
unexpanded data from the file, if macros are present in the data.

As of V2.0, the separator by which data is parsed may be customized by
the user.  A totally new package syntax has also been introduced (the
prior syntax is retained as deprecated macros).  Various levels of bound
checking are now available to the user.

While the package can be used for any application where indexed data is
called for, the package proves particularly useful when elements of
multiple arrays must be recallable and dynamically combined at time of
compilation, rather than in advance.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in

  http://www.latex-project.org/lppl.txt

and version 1.3c or later is part of all distributions of LaTeX
version 2005/12/01 or later.

The Current Maintainer of this work is Steven B. Segletes.
