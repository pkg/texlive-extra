readm403.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998, 2001, 2002, 2004, 2005 by Shinsaku Fujita. 
All rights reserved. 
===========================================================================
readm402.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998, 2001, 2002, 2004 by Shinsaku Fujita. 
All rights reserved. 
===========================================================================
readm400.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998, 2001, 2002 by Shinsaku Fujita. 
All rights reserved. 
===========================================================================
readm300.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998, 2001, 2002 by Shinsaku Fujita. 
All rights reserved. 
===========================================================================
readm201.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998, 2001 by Shinsaku Fujita. All rights reserved. 
===========================================================================
(旧)readme2.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996, 1998 by Shinsaku Fujita. All rights reserved.  
===========================================================================
(旧)readme1.jpn 
Notes for XyMTeX (in Japanese)
Copyright (C) 1993, 1996 by Shinsaku Fujita. All rights reserved. 
===========================================================================
名  称: XyMTeX
登録名: xymtx403.lzh for drawing chem. structures
概  要: 化学構造式を描くためのマクロパッケージ
　　　   (LaTeX用のパッケージファイル類を含む)
鍵  語: LaTeX, 化学, 構造式
作  者: Shinsaku Fujita (藤田 眞作)
登録者: 藤田 眞作
最新版: 4.03
本  籍: http://imt.chem.kit.ac.jp/fujita/fujitas/fujita.html
覚  書: ドキュメントは、xymtx200.tex (xymtx200.dvi, xymtx200.pdf), 
xymtx300.tex (xymtx300.dvi,xymtx300.pdf), xymtx400.tex (xymtx400.pdf)
xymtx401.tex (xymtx401.pdf), およびxymtx402403.tex (xymtx402403.pdf)
===========================================================================
Name: XyMTeX
Description: Macro Package including LaTeX document-style options for 
             typesetting chemical structural formulas
Keywords: LaTeX, chemistry, structural formula
Author: Shinsaku Fujita
Supported: http://imt.chem.kit.ac.jp/fujita/fujitas/fujita.html
Latest Version: 4.03
Archives: http://imt.chem.kit.ac.jp/fujita/fujitas/fujita.html
Note: Documentation in xymtx200.tex (xymtx200.dvi, xymtx200.pdf), 
xymtx300.tex (xymtx300.dvi,xymtx300.pdf), xymtx400.tex (xymtx400.pdf)
xymtx401.tex (xymtx401.pdf), and xymtx402403.tex (xymtx402403.pdf)
===========================================================================

＜説明＞
　XyMTeXは、化学構造式を描くためののマクロパッケージです。 これは、
LaTeX2e用のパッケージファイル群から成り立っています。各ファイルには
化学構造式を描くためのコマンドのマクロコードが含まれています。各コマンドは、
広範囲の化合物の構造が描けるように、新しい構想のもとに作成したものです。
LaTeXのpicture環境を前提にして、その範囲内 (図形組版)で構造式が描けるように
なっています。したがって、構造式出力のプリンターは (プリンタードライバーさえ
あれば) 種類を選ばず、たとえば写植機でも出力が可能です。Version 4.00で
PostScript対応としました．XyMTeX Version4.03をPostScriptモードで使用した場合
には，PostScriptプリンターまたはそれにかわるものが必要です．

　Version 2.00用に約100ページのマニュアル (xymtx200.dvi) が付属しています。
この中に、Version 2.00で追加した機能とコマンドの書式 (仕様) と描画例を
多数記載しましたので、解凍後、まずどのようなことができるのかをご覧ください。

　Version 3.00用に約10ページのマニュアル (xymtx300.dvi) が付属しています。
この中に、Version 3.00で追加した機能とコマンドの書式 (仕様) と描画例を
多数記載しましたので、解凍後、まずどのようなことができるのかをご覧ください。

　chemist.styやmathchem.sty (下記拙著に付録として付いている
フロッピーディスクに収録)に含まれるコマンドを併用すれば、さらに
いろいろな反応スキームなどを描くことができるようになります。
XyMTeX(version1.01)から、chemist.styも同梱してあります。

　Version 4.00用に24ページのマニュアル(xymtx400.pdf)が付属しています．
PDFファイルですから，閲覧やプリントアウトは，Acrobat Readerなどを用いてください．
xymtx400.dviは，\specialのコードを含んでいますので，対応していないdvi-wareでは
読めません．

　Version 4.01用に33ページのマニュアル(xymtx401.pdf)が付属しています．
PDFファイルですから，閲覧やプリントアウトは，Acrobat Readerなどを用いてください．
xymtx401.dviは，\specialのコードを含んでいますので，対応していないdvi-wareでは
読めません．

　Version 4.02, 4.03用に45ページのマニュアル(xymtx402403.pdf)が付属しています．
PDFファイルですから，閲覧やプリントアウトは，Acrobat Readerなどを用いてください．
xymtx402403.dviは，\specialのコードを含んでいますので，対応していないdvi-wareでは
読めません．

基本的な使用法は、XyMTeX verion 4.03でも以前のバージョンでも同じです。
このため、付属のマニュアルでは、基本的な使用法を記載してありません。これらは、
次のレファレンスマニュアルを参照してください。

     「XyMTeX--Typesetting Chemical Structural Formulas」
      藤田眞作著、アジソン・ウェスレイ・ジャパン (1997) CD-DOM付

また次の拙著も参考にしてください．

　　「化学者・生化学者のためのLaTeX---パソコンによる論文作成の手引」
　　　藤田 眞作 著、東京化学同人 (1993) FD付

＜ダウンロード・解凍＞
(1) ファイル名xymtx403.lzhでダウンロードしてください
(2) TeXのメインディレクトリー内でlhaで解凍してください。
　　　a:\tex>lha x b:\xymtx403

詳しいインストールの方法は、xymtx403.docをご覧下さい。
