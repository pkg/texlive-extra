     *File List*
-----RELEASE.---   --  -- --   --     --
nicefilelist.RLS  2023/01/08  v0.9a  ver@@; option autolength
-----PACKAGE.---   --  -- --   --     --
nicefilelist.sty  2023/01/08  v0.9a  more file list alignment (UL)
------DOCSRC.---   --  -- --   --     --
nicefilelist.tex  2023/01/08   --    documenting nicefilelist.sty
    srcfiles.tex  2023/01/08   --    file infos -> SrcFILEs.txt
--------DEMO.---   --  -- --   --     --
    provonly.fd    --  -- --   --    no date, no version, but a lot of info,
                                     look how that is wrapped!
       wrong.prv   * NOT FOUND *
       empty.f     * NOT FOUND *
--------USED.---   --  -- --   --     --
    hardwrap.sty  2011/02/12  v0.2   Hard wrap messages
    myfilist.sty  2012/11/22  v0.71  \listfiles -- mine only (UL)
    readprov.sty  2012/11/22  v0.5   file infos without loading (UL)
    fifinddo.sty  2012/11/17  v0.61  filtering TeX(t) files by TeX (UL)
     makedoc.sty  2012/08/28  v0.52  TeX input from *.sty (UL)
    niceverb.sty  2015/11/21  v0.62  minimize doc markup (UL)
    texlinks.sty  2015/07/20  v0.83  TeX-related links (UL)
     makedoc.cfg  2013/03/25   --    documentation settings
    mdoccorr.cfg  2012/11/13   --    `makedoc' local typographical corrections
-not-so-much.---   --  -- --   --     --
   kvsetkeys.sty  2022-10-05  v1.19  Key value parser (HO)
     ***********

 List made at 2023/01/08, 00:00
 from script file srcfiles.tex