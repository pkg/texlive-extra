\documentclass[a4paper,article]{memoir}
\usepackage[T1]{fontenc}
\usepackage{amsmath,enumitem,showexpl}
%\usepackage[scaled]{beramono}
\title{The \textsf{interval} package}
\author{Lars Madsen\thanks{Email: daleif@math.au.dk, version: \INTVversion}\\ \small (on behalf of By the Danish \TeX\ collective)}
\setsecnumdepth{none}

%\setlength\overfullrule{5pt}

\usepackage{interval}

\begin{document}

\maketitle



\section{Motivation}
\label{sec:motivation}

In mathematics there are two syntax' when it comes to specifying open
and closed intervals.

The first use parantheses to mark an open end
\begin{align*}
  [a,b] \qquad (a,b] \qquad [a,b)\qquad (a,b),
\end{align*}
while the other use brackets throughout
\begin{align*}
  [a,b] \qquad ]a,b] \qquad [a,b[\qquad ]a,b[,
\end{align*}

The former poses no problem in \TeX, but the later does, as, e.g., a
closing bracket is being used in place of an opening fence, and thus
have the wrong category when it comes to spacing:
\begin{align*}
  ]-a,b[+c \qquad\text{versus}\qquad \mathopen{]}-a,b\mathclose{[}+c.
\end{align*}

One could use
\begin{verbatim}
\mathopen{]}-a,b\mathclose{[}+c
\end{verbatim}
to solve the problem, but then \cs{left}\dots\cs{right} can no longer
be used to auto scale the fences.




\section{The \cs{interval} command}
\label{sec:csinterval-command}

The following is the result of a discussion on the Danish \TeX\ Users
groups mailing list. Kudos to Martin Heller, for proposing the
original version using \textsf{pgfkeys}.

We provide a macro and a way to globally configure it
\begin{quote}
  \cs{interval}\oarg{options}\marg{start}\marg{end}\\
  \cs{intervalconfig}\marg{options}
\end{quote}
We note that the interval separator symbol is hidden inside the
\cs{interval} macro and can be changed using an option.

\subsection{Configuration options}
\label{sec:options}

\begin{description}[style=nextline,font=\sffamily\bfseries]
\item[separator symbol]
  symbol that separates the start and end of the interval. Default:
  \texttt{\{,\}}, note that as comma is the separating character in the
  options specification, the symbol is enclosed in braces, these are
  automatically removed.
\item[left open fence]
  Default: \texttt{]}
\item[left closed fence] 
  Default: \texttt{[}
\item[right open fence]
  Default: \texttt{[}
\item[right closed fence] 
  Default: \texttt{]}
\item[soft open fences] 
  This is just a fast way of saying
  \begin{quote}
    \ttfamily\obeylines
    left open fence=(,
    right open fence=)
  \end{quote}
\item[colorize]
  Default: \meta{empty}. When rewriting an existing document into
  using the \textsf{interval} package, it turns out to be \emph{very}
  handy to color the result of the \cs{interval} macro to keep track
  of which have been rewritten and which has not.
  This can be done using
\begin{lstlisting}[basicstyle=\ttfamily,columns=flexible]
  \usepackage{xcolor}
  \intervalconfig{ colorize=\color{red} }
\end{lstlisting}
It will colorize the entire interval including the fences.
  
\end{description}

\subsection{Usage options}

By default \cs{interval}\marg{start}\marg{end} will produce a closed
interval. Other types are provided via options:

\begin{description}[style=nextline,font=\sffamily\bfseries]
\item[open]
  an open interval
\item[open left]
  interval open on the left side
\item[open right]
  interval open on the right side
\item[scaled]
  auto scale interval fences
\item[scaled=\meta{scaler}]
  scale fences using \meta{scaler}, i.e.\ using \texttt{scaled=\cs{Big}}
\end{description}

\fancybreak{}

As some might be guessed, the \texttt{interval} package depends on the
\textsf{pgfkeys} package to handle its key-value configuration.


\subsection{Short hands}
\label{sec:shorthands}

For convenience the following short hands are provided as of version 0.4.
\begin{description}[style=nextline,font=\normalfont]
\item[\cs{ointerval}\oarg{options}\marg{start}\marg{end}]
  is short for \cs{interval}\texttt{[open,}\meta{options}\texttt{]}\marg{start}\marg{end}
\item[\cs{linterval}\oarg{options}\marg{start}\marg{end}]
  is short for \cs{interval}\texttt{[open left,}\meta{options}\texttt{]}\marg{start}\marg{end}
\item[\cs{rinterval}\oarg{options}\marg{start}\marg{end}]
  is short for \cs{interval}\texttt{[open right,}\meta{options}\texttt{]}\marg{start}\marg{end}
\end{description}

%\newpage

\section{Examples}
\label{sec:examples}

% used for showexpl
\lstset{
  explpreset={
    xleftmargin=0em,
    columns=fixed,
  },
  pos=r,
  width=-99pt,
  overhang=0pt,
  hsep=2\columnsep,
  vsep=\bigskipamount,
  rframe=x,
  basicstyle=\footnotesize\ttfamily,
}



\begin{LTXexample}[varwidth=true]
\begin{align*}
& A\in\interval{a}{b}              \\
& A\in\interval[open]{a}{b}        \\
& A\in\interval[open left]{a}{b}   \\      
& A\in\interval[open right,
  scaled]{a}{\frac{1}{2}b}=B       \\     
& A\in\interval[scaled=\big]{a}{b} \\
& A\in\ointerval[scaled]{%
  \tfrac{1}{3}}{\tfrac{1}{2}}
\end{align*}
\end{LTXexample}
And using soft open fences:
\begin{LTXexample}[varwidth=true]
\intervalconfig{
  soft open fences,
  separator symbol=;,
}
\begin{align*}
& A\in\interval{a}{b}              \\
& A\in\interval[open]{a}{b}        \\
& A\in\interval[open left]{a}{b}   \\      
& A\in\interval[open right,
  scaled]{a}{\frac{1}{2}b}=B       \\     
& A\in\interval[scaled=\big]{a}{b} \\
& A\in\rinterval{a}{b}
\end{align*}
\end{LTXexample}



\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
