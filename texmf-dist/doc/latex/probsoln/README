LaTeX Package : probsoln v3.05

Last Modified : 2017-07-10

Author        : Nicola L.C. Talbot

Package FAQ   :  http://www.dickimaw-books.com/faqs/probsolnfaq.html

Files         : probsoln.dtx   - documented source file
                probsoln.ins   - installation script

The package probsoln.sty is designed for lecturers who have 
to generate new problem sheets for their students on a 
regular basis (e.g. yearly) by randomly selecting a 
specified number of problems defined in another file.
This means that you can easily generate a new problem sheet 
that is different from the previous year, thus alieviating 
the temptation of students to seek out the previous year's 
students and checking out their answers.  The solutions to 
the problems can be defined along with the problem, making
it easy to generate the solution sheet from the same source 
code.

Required packages:

ifthen
amsmath
xkeyval

To extract the code do:

latex probsoln.ins

This will create the file probsoln.sty which should be placed
somewhere on your TeX path. It will also create several 
sample documents, sample*.tex, and sample databases used
by the sample documents. If you can't bring yourself to
read the manual, please try out these sample 
documents to give yourself an idea of what to do.

To extract the documentation do:

latex probsoln.dtx
latex probsoln.dtx

Comment out \OnlyDescription if you want the documented code.

If you experience difficulties, try reading the 
troubleshooting section of the manual. If that doesn't
help try http://www.dickimaw-books.com/faqs/probsolnfaq.html

This material is subject to the LaTeX Project Public License.
See http://www.ctan.org/license/lppl1.3 for
the details of that license.

http://www.dickimaw-books.com/
