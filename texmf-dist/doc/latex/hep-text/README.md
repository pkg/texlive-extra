

# The `hep-text` package

List and text extensions

## Introduction

The `hep-text` package extends `LaTeX` lists using the `enumitem package and provides some text macros.

The package can be loaded by `\usepackage{hep-text}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

