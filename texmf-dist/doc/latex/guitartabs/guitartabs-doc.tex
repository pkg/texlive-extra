%NAME: guitartabs-doc.tex
%
% Andrey Babushkin, 2018/05/01
% babusand@fit.cvut.cz
%
% The documentation source for the guitartabs class.
% 
% Released under the The LaTeX Project Public License 1.3c.

\documentclass{guitartabs}

\title{\LaTeX\ Guitar Tabs}
\author{Andrey Babushkin}
\date{June, 2018}

\usepackage{geometry}
\geometry{left=2.0cm, top=1.5cm, right=2.0cm, bottom=1.5cm, footskip=0cm}

\usepackage{listings}
\lstset{
	keepspaces=false,
	numbers=left,
	language=[LaTeX]TeX,
	basicstyle=\tt\scriptsize,
	keywordstyle=\color{blue},
	identifierstyle=\color{magenta},
	morekeywords={*,
				artistname,
				albumtitle,
				songname,
				maketabheader,
				note,
				notel,
				restwhole,
				resthalf,
				restquarter,
				resteighth,
				restsixteenth
				}
}

\usepackage{titling}
\renewcommand\maketitlehooka{\null\mbox{}\vfill}
\renewcommand\maketitlehookd{\vfill\null}

\begin{document}
	
	\begin{titlingpage}
		\maketitle
	\end{titlingpage}
	
	\pagebreak
	\tableofcontents
	\pagebreak
	
	\section{Introduction}
	
	Many guitarists in the world use instead of music sheets guitar tabs, which is a more convenient 
	way of learning songs. Music sheets show how a melody goes: the notes, tonality, mode, time signatures etc.
	Unfortunately, a guitar has one same note on different strings several times and the position of the left 
	hand plays its role. So, the guitar tabs were created where a different notation is used. Every line of a
	guitar tab shows a separate string. A number on a string shows which fret should be played on which moment. 
	Of course, the a time signature is still written on the beginning of the tab and vertical lines separate 
	a tab onto bars, just like on a music sheet. There are also tuning settings written for each string.
	
	This project describes a set of \LaTeX\space macros that allow a user to write such guitar tabs.
	
	\begin{tabline}{3}{6}{8}{E,A,D,G,B,E}
		\notel{1}{6}{6}{0}{8}
		\notel{2}{6}{3}{0}{8}
		\notel{3}{6}{2}{0}{8}
		\notel{4}{6}{1}{0}{8}
		\notel{5}{6}{2}{0}{8}
		\notel{6}{6}{3}{0}{8}
		
		\nextbar
		\notel{1}{12}{6}{0}{8}
		\notel{3}{12}{3}{0}{8}
		\notel{5}{12}{2}{0}{8}
		\notel{7}{12}{1}{0}{8}
		\notel{9}{12}{2}{0}{8}
		\notel{11}{12}{6}{3}{16}
		\notel{12}{12}{6}{2}{16}
		
		\nextbar
		\notel{1}{12}{6}{0}{8}
		\notel{3}{12}{3}{0}{8}
		\notel{5}{12}{2}{0}{8}
		\notel{7}{12}{1}{3}{8}
		\notel{9}{12}{3}{0}{16}
		\notel{10}{12}{1}{0}{16}
		\notel{11}{12}{2}{0}{16}
		\notel{12}{12}{3}{0}{16}
	\end{tabline}

	\section{Quick start}
	\subsection{Generating header}
	
	A new class called \textbf{\textit{guitartabs.cls}} was created, so, for using this class a command 
	\begin{lstlisting}
\documentclass{guitartabs}
	\end{lstlisting} 
	should be used on the beginning of a document.
	
	Every guitar tab has a title of a song, an album and a composer (resp. band) name. Before starting a 
	document one can use a set of macros to define these. 
	\begin{lstlisting}[firstnumber=2]
\artistname{Metallica}
\albumtitle{Metallica}
\songname{Nothing Else Matters}
	\end{lstlisting}
	
	To start writing a document one should begin with:
	\begin{lstlisting}[firstnumber=5]
\begin{document}
	\maketabheader % print the header
	
	The code goes here...
\end{document}
	\end{lstlisting}
	As you can see, the \lstinline{\maketabheader} is used to print a header of the document.
	
	\pagebreak
	\subsection{List of available commands}
	
	So, now we are ready to make some \LaTeX\ music! First of all, let me introduce a set of all available commands:
	\vspace{0.5cm}
	
	\begin{tabular}{ | p{4cm} | p{5cm} | p{7cm} | }
		\hline
		\textbf{Command} 
			& \textbf{Description} 
			& \textbf{Arguments} 
			\\ \hline
		\textbackslash artistname\{name\} 
			& Sets an artist name who wrote or performed a composition. 
			& \textit{name} - an artist name.
			\\ \hline
		\textbackslash albumtitle\{title\}
			& Sets an album title on which the song was published.
			& \textit{title} - an album title
			\\ \hline
		\textbackslash songname\{name\} 
			& Sets a song name. 
			& \textit{name} - a song name.
			\\ \hline
		\parbox[t]{4cm}{
			\textbackslash begin\{tabline\}
			\\ \hspace*{1cm}\{bars\}
			\\ \hspace*{1cm}\{tsupper\}
			\\ \hspace*{1cm}\{tslower\}
			\\ \hspace*{1cm}\{tuning\}
		}
			& Opens the environment and make \LaTeX\ ready for drawing tabs.
			& \parbox[t]{7cm}{
				\textit{bars} - a number of bars on a tab line.
				\\ \textit{tsupper} - upper time signature number.
				\\ \textit{tslower} - lower time signature number.
				\\ \textit{tuning} - a guitar tuning, starting from the 6th string. 
				\\ E.g. E,A,D,G,B,E
			}
			\\ \hline
		\textbackslash maketabheader
			& Prints a header.
			& 
			\\ \hline
		\parbox[t]{4cm}{
			\textbackslash note\{numerator\}
			\\ \hspace*{1cm}\{denominator\}
			\\ \hspace*{1cm}\{string\}
			\\ \hspace*{1cm}\{fret\}
		}
			& Draws one note.
			& \parbox[t]{7cm}{
				\textit{string} - a string.
				\\ \textit{fret} - a fret.
				\\ \textit{numerator}
				\\ \textit{denominator} - sets the position of a note relatively to the beginning of a bar. 
					E.g. values \{1\}\{2\} or \{8\}\{16\} print a note at the center of the bar.
			}
			\\ \hline
		\parbox[t]{4cm}{
			\textbackslash notel\{numerator\}
			\\ \hspace*{1cm}\{denominator\}
			\\ \hspace*{1cm}\{string\}
			\\ \hspace*{1cm}\{fret\}
			\\ \hspace*{1cm}\{length\}
		}
			& Draws one note with a note length beneath a tab line.
			& \parbox[t]{7cm}{
				\textit{string} - a string.
				\\ \textit{fret} - a fret.
				\\ \textit{numerator}
				\\ \textit{denominator} - sets the position of a note relatively to the beginning of a bar. 
				E.g. values \{1\}\{2\} or \{8\}\{16\} print a note at the center of the bar.
				\\ \textit{length} - 1 for a whole note, 2 for a half note etc. 
				\\ Available values for length are:
				\\ 1, 2, 2.5, 4, 4.5, 8, 8.5, 16, 16.5
			}
		\\ \hline
		\parbox[t]{4cm}{
			\textbackslash restwhole
			\\ \textbackslash resthalf
			\\ \textbackslash restquarter
			\\ \textbackslash resteighth
			\\ \textbackslash restsixteenth
			\\ \hspace*{1cm}\{numerator\}
			\\ \hspace*{1cm}\{denominator\}
		}
			& A set of functions that draw rest symbols (whole, half, quarter, eighth, sixteenth, eighth respectively) on a selected position. 
			& \parbox[t]{7cm}{
				\textit{numerator}
				\\ \textit{denominator} - sets the position of a rest relatively to the beginning of a bar. 
				E.g. values \{1\}\{2\} or \{8\}\{16\} print a rest at the center of the bar.
			}
			\\ \hline
		\textbackslash end\{tabline\}
			& Closes the environment and draws a tab line.
			& 
			\\ \hline
		\textbackslash nextbar 
			& Moves a position of the cursor to the next bar.
			& 
			\\ \hline
	\end{tabular}

	\vspace{0.5cm}
	The way how the position of a note is set was chosen with a reason. Sometimes some notes are played 
	out of the rhythm of a song. For example:
	
	 \begin{tabline}{1}{4}{4}{E,A,D,G,B,E}
	 	\notel{1}{2}{3}{12}{2}
	 	\notel{2}{2}{3}{12}{2}
	 	\notel{3}{4}{4}{12}{8}
	 \end{tabline}
 
 	So, here we have 3 notes, two of them fill the bar rhythmically but we have the 3rd note, 1/8, that goes before the second 1/2. So, such approach of setting the exact position of each note offers great flexibility.
 	
 	\pagebreak
 	\subsection{Example}
 	
 	So, let's have a look on the example from the introduction section:
 	
 	\begin{tabline}{3}{6}{8}{E,A,D,G,B,E}
 		
 		\notel{1}{6}{6}{0}{8}
 		\notel{2}{6}{3}{0}{8}
 		\notel{3}{6}{2}{0}{8}
 		\notel{4}{6}{1}{0}{8}
 		\notel{5}{6}{2}{0}{8}
 		\notel{6}{6}{3}{0}{8}
 		
 		\nextbar
 		\notel{1}{12}{6}{0}{8}
 		\notel{3}{12}{3}{0}{8}
 		\notel{5}{12}{2}{0}{8}
 		\notel{7}{12}{1}{0}{8}
 		\notel{9}{12}{2}{0}{8}
 		\notel{11}{12}{6}{3}{16}
 		\notel{12}{12}{6}{2}{16}
 		
 		\nextbar
 		\notel{1}{12}{6}{0}{8}
 		\notel{3}{12}{3}{0}{8}
 		\notel{5}{12}{2}{0}{8}
 		\notel{7}{12}{1}{3}{8}
 		\notel{9}{12}{3}{0}{16}
 		\notel{10}{12}{1}{0}{16}
 		\notel{11}{12}{2}{0}{16}
 		\notel{12}{12}{3}{0}{16}
 	\end{tabline}
 
 	Of course, the best way to learn something is to look at the code. Here is comes:
 	
 	\begin{lstlisting}[firstnumber=1]
 % we create a tab with 3 bars, with the time signature 6/8 and a standart guitar tuning (E, A, D, G, B, E)
 \begin{tabline}{3}{6}{8}{E,A,D,G,B,E}
 
	\notel{1}{6}{6}{0}{8} % 1st bar, position 1/6, 6th string, 0th fret, length 1/8
	\notel{2}{6}{3}{0}{8} % 1st bar, position 2/6, 3rd string, 0th fret, length 1/8
	\notel{3}{6}{2}{0}{8}
	\notel{4}{6}{1}{0}{8}
	\notel{5}{6}{2}{0}{8}
	\notel{6}{6}{3}{0}{8}
	
	\nextbar				% move the cursor to the next bar
	\notel{1}{12}{6}{0}{8}
	\notel{3}{12}{3}{0}{8}
	\notel{5}{12}{2}{0}{8}
	\notel{7}{12}{1}{0}{8}
	\notel{9}{12}{2}{0}{8}
	\notel{11}{12}{6}{3}{16} % 2nd bar, position 11/12, 6th string, 3rd fret, length 1/16
	\notel{12}{12}{6}{2}{16}
	
	\nextbar
	\notel{1}{12}{6}{0}{8}
	\notel{3}{12}{3}{0}{8}
	\notel{5}{12}{2}{0}{8}
	\notel{7}{12}{1}{3}{8}
	\notel{9}{12}{3}{0}{16}
	\notel{10}{12}{1}{0}{16}
	\notel{11}{12}{2}{0}{16}
	\notel{12}{12}{3}{0}{16}
	
\end{tabline}
 	\end{lstlisting}
 	
 	It is that simple. Of course, the position on the bar determines the length of each note, so, if
 	one does not need the length to be drawn, one can omit the last argument and use \lstinline{\note} 
 	instead of \lstinline{\notel}. Everything else stays the same.
 	
 	\begin{lstlisting}[firstnumber=1]
\begin{tabline}{3}{6}{8}{E,A,D,G,B,E}
	\note{1}{6}{6}{0} % we use \note instead of \notel and omit the last argument
	\note{2}{6}{3}{0} 
	\note{3}{6}{2}{0}
	\note{4}{6}{1}{0}
	\note{5}{6}{2}{0}
	\note{6}{6}{3}{0}
	
	\nextbar
	\note{1}{12}{6}{0}
	\note{3}{12}{3}{0}
	\note{5}{12}{2}{0}
	\note{7}{12}{1}{0}
	\note{9}{12}{2}{0}
	\note{11}{12}{6}{3} 
	\note{12}{12}{6}{2}
	
	\nextbar
	\note{1}{12}{6}{0}
	\note{3}{12}{3}{0}
	\note{5}{12}{2}{0}
	\note{7}{12}{1}{3}
	\note{9}{12}{3}{0}
	\note{10}{12}{1}{0}
	\note{11}{12}{2}{0}
	\note{12}{12}{3}{0}

\end{tabline}
	\end{lstlisting}

	This code results in this tab line:
	
	\begin{tabline}{3}{6}{8}{E,A,D,G,B,E}
		
		\note{1}{6}{6}{0} 
		\note{2}{6}{3}{0} 
		\note{3}{6}{2}{0}
		\note{4}{6}{1}{0}
		\note{5}{6}{2}{0}
		\note{6}{6}{3}{0}
		
		\nextbar
		\note{1}{12}{6}{0}
		\note{3}{12}{3}{0}
		\note{5}{12}{2}{0}
		\note{7}{12}{1}{0}
		\note{9}{12}{2}{0}
		\note{11}{12}{6}{3}
		\note{12}{12}{6}{2}
		
		\nextbar
		\note{1}{12}{6}{0}
		\note{3}{12}{3}{0}
		\note{5}{12}{2}{0}
		\note{7}{12}{1}{3}
		\note{9}{12}{3}{0}
		\note{10}{12}{1}{0}
		\note{11}{12}{2}{0}
		\note{12}{12}{3}{0}
		
	\end{tabline}
		
	One may notice that there are no small drawn anymore  underneath every note.
	
	\pagebreak
	
	\section{Conclusion}
	
	The goal of this work was to create flexible and simple enough system to draw guitar tabs. In my opinion, this goal was reached. The \textbf{guitartabs} class offers a simple interface for drawing such tabs, so, a user
	is only responsible for writing music and the class will make everything else including drawing, calculating
	positions on the bar, setting correct size etc. 
	
\end{document}