#OVERVIEW

Guitartabs is a simple LaTeX class that allows guitarists to create basic guitar tablatures using LaTeX. Create music and do not be bothered with macro programming.

##REQUIREMENTS

The environment is created using LaTeX2e and different packages. All of them are marked with \RequirePackage.

##What's inside

README.md 					- this README.
guitartabs.cls 				- a class with the environment.
nothingelsematters.tex 		- an example.
nothingelsematters.pdf 		- an example in PDF format.
guitartabs-doc.tex 			- documentation.
guitartabs-doc.pdf 			- documentation in PDF format.

##License

Released under the The LaTeX Project Public License 1.3c.

##Contacts

Andrey Babushkin
babusand@fit.cvut.cz