# `zx-calculus`: a library to typeset ZX-calculus in LaTeX

This library (based on the great TikZ and Tikz-cd packages) allows you to typeset ZX-calculus directly in LaTeX. It comes with a default (but highly customizable) style. For more information, see the documentation in [`zx-calculus.pdf`][1].

The project is hosted at https://github.com/leo-colisson/zx-calculus , feel free to report any bug there. It is licensed under MIT and its author is Léo Colisson.

 [1]: https://raw.githubusercontent.com/leo-colisson/zx-calculus/main/doc/zx-calculus.pdf
