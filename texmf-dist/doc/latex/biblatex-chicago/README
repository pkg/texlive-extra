IMPORTANT NOTE:

This is the package formerly known as biblatex-chicago-notes-df.  It
is designed for use with the latest version (3.18b) of biblatex.  The
package contains the 17th-edition Chicago style files, and I am also
maintaining the 16th-edition files for those for whom they remain a
necessity, though I have deprecated these older files and will remove
them in a future release.  I strongly encourage all users to move to
the 17th-edition styles, as they have received and will continue to
receive many new features and fixes as compared with the older styles.
If you have used the package before, then you should be sure to
consult the RELEASE file to find out what alterations you may need to
make to your .bib files and document preambles to bring them up to
date.  Most particularly please note that biber is now the required
backend for all the included styles (version 2.18 is designed for use
with the latest biblatex).

README (version 2.3a, 2022-11-17):

Biblatex-chicago contains three biblatex styles implementing the
specifications of the Chicago Manual of Style, 17th edition.  The
"notes & bibliography" style was formerly available in the package
biblatex-chicago-notes-df, and is intended primarily for writers in
the humanities.  The "author-date" style, generally favored by writers
in the sciences and social sciences, comes in two flavors.  Recent
editions of the Manual have brought the presentation of sources in
this style much more into line with the notes & bibliography
specification, especially regarding the formatting of titles.  If you
still require titles to be capitalized sentence style, and article
titles, for example, not to be enclosed in quotation marks, then you
may now use the traditional author-date style ("authordate-trad"),
which provides these features while in all other respects following
the 17th-edition specifications.

The package is under active development, but its feature set is
already fairly extensive.  If you have used the package before you
may, after perusing the RELEASE file, want to look at the changelog at
the end of biblatex-chicago.pdf, which contains cross-references to
more detailed explanations about how this update will affect parts of
your .bib file.  If you are just getting started, the best way to
learn the system is to read the Quickstart section in that same file,
then read either cms-notes-intro.pdf or cms-dates-intro.pdf, each of
which are fully cross-referenced and contain links to the complete
reference guide to both styles in sections 4 or 5 of
biblatex-chicago.pdf.  The package also contains annotated .bib files
(notes-test.bib and dates-test.bib) which each offer over 100 entries
demonstrating how to present a wide range of sources, nearly all of
them taken from the Chicago Manual of Style itself, so that you can
compare the output of your system (cms-notes-sample.tex,
cms-dates-sample.tex, cms-trad-sample.tex) or my system
(cms-notes-sample.pdf, cms-dates-sample.pdf, cms-trad-sample.pdf) with
the actual examples in the Manual.

I have, wherever possible, attempted to maintain backward
compatibility with the standard biblatex styles, but there are a
significant number of situations where I have been unable to do so,
which means that switching among various styles while using the same
bibliography database will not be as simple and painless as it could
be.  If you can see ways better to preserve this compatibility, or
indeed to improve the package in any way whatsoever, please let me
know, or better yet, send a patch.  If the styles don't behave as you
expect or if you discover a bug, I'd be very happy to hear about it,
but do please read biblatex-chicago.pdf first, as you may find that
your question has already been answered there.  If you do find a bug,
please send me the .bib entry, your LaTeX preamble, and your output
.log, so that I can try to reproduce it here.  My email address is at
the head of biblatex-chicago.pdf.

Installation:

If you want to place these files in your TeX directory tree instead of
in your working directory, I recommend the following, which should be
familiar from other packages:

 - The thirty-three files biblatex-chicago.sty, cmsendnotes.sty,
   chicago-notes.cbx, chicago-notes.bbx, chicago-authordate.cbx,
   chicago-authordate.bbx, chicago-authordate-trad.cbx,
   chicago-authordate-trad.bbx, chicago-dates-common.cbx,
   chicago-notes16.cbx, chicago-notes16.bbx, chicago-authordate16.cbx,
   chicago-authordate16.bbx, chicago-authordate-trad16.cbx,
   chicago-authordate-trad16.bbx, chicago-dates-common16.cbx, cms.dbx,
   cms-american.lbx, cms-brazilian.lbx, cms-british.lbx,
   cms-dutch.lbx, cms-finnish.lbx, cms-french.lbx, cms-german.lbx,
   cms-icelandic.lbx, cms-ngerman.lbx, cms-norsk.lbx,
   cms-norwegian.lbx, cms-nynorsk.lbx, cms-romanian.lbx,
   cms-spanish.lbx, cms-swedish.lbx, and cmsdocs.sty reside in the
   latex/ subdirectory.  The entire contents of this latex/ directory
   can go in <TEXMFLOCAL>/tex/latex/biblatex-contrib/biblatex-chicago,
   where <TEXMFLOCAL> is the root of your local TeX installation --
   for example, and depending on your system, /usr/share/texmf-local,
   /usr/local/texmf, or C:\Local TeX Files\.  You may need to create
   this directory first, and after copying the files there please
   remember to update your TeX file name database so that TeX can find
   them.

 - The twenty-one files biblatex-chicago.tex, biblatex-chicago.pdf,
   cms-notes-intro.tex, cms-notes-intro.pdf, cms-dates-intro.tex,
   cms-dates-intro.pdf, cms-trad-appendix.tex, cms-trad-appendix.pdf,
   cms-notes-sample.tex, cms-dates-sample.tex, cms-trad-sample.tex,
   cms-legal-sample.tex, cms-notes-sample.pdf, cms-dates.sample.pdf,
   cms-trad-sample.pdf, cms-legal-sample.pdf, cms-noteref-demo.tex,
   cms-noteref-demo.pdf, notes-test.bib, dates-test.bib, and
   legal-test.bib can all be found in the doc/ directory in the
   package archive.  You can place all of this in
   <TEXMFLOCAL>/doc/latex/biblatex-contrib/biblatex-chicago.

Changelog: See the RELEASE file, and also the end of
biblatex-chicago.pdf.

Copyright (c) 2008-2022 David Fussner.  This package is
author-maintained.  This work may be copied, distributed and/or
modified under the conditions of the LaTeX Project Public License,
either version 1.3 of this license or (at your option) any later
version.  The latest version of this license is in
http://www.latex-project.org/lppl.txt and version 1.3 or later is part
of all distributions of LaTeX version 2005/12/01 or later.  This
software is provided "as is," without warranty of any kind, either
expressed or implied, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose.
