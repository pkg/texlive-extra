# invoice-class
The `invoice-class` produces a standard US commercial invoice using data from a CSV file. Invoices can span multiple pages. The class is configurable for different shipping addresses.

The class is released under the LPPL 1.3 licence.


