                    This is the README file for namespc

                    Copyright (C) 2003 Alexander Dreyer



* Copyright
===========

This file is part of namespc.

This file may be distributed and/or modified under the conditions of
the LaTeX Project Public License, either version 1.2 of this license
or (at your option) any later version.  The latest version of this
license is in:

   http://www.latex-project.org/lppl.txt

and version 1.2 or later is part of all distributions of LaTeX version
1999/12/01 or later.

namespc consists of the following files:

- readme.txt (this file)
- namespc.dtx
- namespc.pdf

* Description
=============

 The \textsf{namespc} package adds rudimentary \emph{c++}-like namespace
 functionality to \LaTeX. It may be used to declare local \LaTeX{} commands,
 which can be made accessible in a later contexts without defining them 
 globally. 

* Changes
=========
+ added ::notation
  commands defined within the namespace preamble of a namespace (e.g. spcname) 
  may be accessed like follows:
  \::spcname::command{arg1}..{argn}::


* Installation
==============

  To install the package, please follow these steps:

- Run LaTeX on the file `namespc.dtx' to create the style file `namespc.sty'.
  and the doc file `namespc.dvi' simultanously.
- Install `namespc.sty' into a directory searched by TeX.
  

--
Alexander Dreyer <adreyer@web.de>
http://www.3r4u.de/