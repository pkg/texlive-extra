\documentclass{codedoc}

\usepackage{
    lmodern,
    tikz,
    graphicx,
    shortvrb,
    url,
    xspace
}

\usepackage[defaultlines=2,all]{nowidow}
\usepackage[small]{titlesec}
\usepackage[margin=1in]{geometry}

\MakeShortVerb{\|}
\newcommand{\option}[3]{\item [\texttt{#1}] (\meta{#2}; default: \texttt{#3}).}
\newcommand*{\pkgname}{\texttt{pdfreview}\xspace}

\author{Michael Palmer (\texttt{mpalmer@uwaterloo.ca})}
\title{The \pkgname package}
\date{v1.2 (\today)}

\usepackage{bookmark}
\hypersetup{colorlinks,linkcolor=blue}

% figure placement: Set more liberal parameters for combining
% text and figures on the same page.
\renewcommand{\topfraction}{0.9}
\renewcommand{\bottomfraction}{0.9}
\renewcommand{\floatpagefraction}{0.8}
\renewcommand{\textfraction}{0.1}

\usepackage{adjustmargins}

\begin{document}

\maketitle

\begin{abstract}
\noindent The \pkgname package lets you add comments in the page margins of PDF files, e.g.\ when reviewing manuscripts or grading reports. The PDF file to be annotated is included, one page at a time, as graphics, in a manner similar to the \texttt{pdfpages} package. Notes are placed in the margin next to the included graphics using a grid of guides. Alternatively, only numbers are placed in the page margins, and all the notes are collected into a numbered list at the end of the document. \par
Note that this package is \emph{not} intended for adding notes into the \LaTeX{} source of the document that is being reviewed; instead, the document undergoing review is already in PDF format and remains unchanged. Also note that this package does not produce the usual PDF `sticky notes' that must be opened in the viewer by clicking on them; instead, the notes are simply shown as text in the page margin.
\end{abstract}

\section{An example}

Let's assume a student submitted his essay as \mbox{\texttt{lorem ipsum.pdf}}. In order to review and annotate it, I create this wrapper \LaTeX{} document:

\begin{verbatim}
\documentclass[letterpaper,10pt]{article}
\usepackage[
  sourcedoc={lorem ipsum.pdf},    % declare the PDF document being reviewed
  grid=true,                      % draw guides for margin notes (set false when done)
  staggered=true,                 % avoid vertical overlap of adjacent notes
  bodywidth=0.66,                 % set width of source doc page as fraction of \textwidth
  twocolumn=true,                 % enable margin notes on both sides
  trim=2cm,                       % trim all page margins of source doc by this amount
]{pdfreview}

\begin{document}

\begin{page}{1}                   % mount the first page

\begin{leftnotes}                 % write notes into the left margin
\cnote{84}{It looks like Latin.}  % place note vertically centered on guide 84
\bnote{68}{Is this really Latin?} % align bottom of note to guide 68
\tnote{16}{No, not Latin}         % align top of note to guide 16
\end{leftnotes}                   % notes after this point go into the right margin

\tnote{48}{You really should be writing proper Latin}
\cnote{12}{Cite your references}
\end{page}

% \begin{page}{2}                 % mount the next page
% \note{...}{...} etc.

\end{document}
\end{verbatim}
\clearpage 

\noindent The first page of the output is shown in Figure 1. When you have added all your comments, you can set the package option \texttt{grid=false} in order to hide the guides in the page margins. That is already the gist of it; some more options and a few tips and tricks will be given below.

\begin{figure}
\centering
\includegraphics[clip,trim=0 1.7in 0 1.7in,width=\textwidth,page=1]{sample}
\caption{Frst page of the output produced by the sample listing above.}
\end{figure}

\section{Package options}

\subsection{Options for controlling the selection and formatting of the source document}

\begin{description}

\option{sourcedoc}{file name}{noname} Name of the PDF file to be reviewed. The file extension (\texttt{.pdf}) can be omitted. If the file name of your source document contains spaces, be sure to include it in braces.

\option{bodywidth}{number}{0.75} Width of the included source doc page, as fraction of \texttt{\textbackslash textwidth} of the wrapper document. The remainder of \texttt{\textbackslash textwidth} will be used for the margin notes. 

The default of 0.75 should be suitable in combination with \texttt{inline=true,twocolumn=false}, that is, with full note text in the right margin only (which is my own most common use case). Use smaller values with \texttt{inline=true,twocolumn=true} or larger ones with \texttt{inline=false}. 

\option{trim}{length(s)}{1cm} How much to trim off the margins of each page of the source document. This option is passed to the \texttt{\textbackslash adjustbox} macro from the eponymous package. Accordingly, you can use two lengths to specify separate trimmings for horizontal and vertical margins, and four lengths to specify separate values for the left, lower, right, and top margins (in this order).%
%
\footnote{No, this order doesn't resemble a clock, fuel gauge, or anything else that mere mortals might have encountered.} 

Note that multiple lengths values must be surrounded with braces, e.g.\ \texttt{trim=\textbraceleft 1cm 2cm\textbraceright} or \texttt{trim=\textbraceleft 1cm 2cm 0cm 1cm\textbraceright}. If you omit the braces, only the first length value will be used, and the others will be silently ignored. 

\end{description}

\subsection{Options for controlling the placement of notes}

\begin{description}

\option{twocolumn}{boolean}{false} If true, place included page in the middle and arrange for margin notes on both sides. If false, place included page on the left and have margin notes on the right hand side only. 

\option{inline}{boolean}{true} If true, print the note text into the page margin. If false, print only the number of each note into the page margin. In the latter case, the note text will be collected and printed out as an \texttt{enumerate}'d list at the end of the document. The items in this list and the corresponding numbers in the margin will be reciprocally hyperlinked.

\option{notenumbers}{boolean}{false} If true, start each note with its number. If option \texttt{inline} is false, the number is printed regardless of this option. 

\option{alignnotes}{t,b, or c}{c} Vertical alignment of a margin note declared with |\note|. Can be overridden by passing \texttt{t} or \texttt{b} as an optional argument (e.g.~|\note[b]{Go boil your bottoms}| or |\note[t]{I shall taunt you a second time}|). 

\option{notesep}{length}{3pt} Empty space between included page and margin notes.

\option{staggered}{boolean}{true} If true, vertical overlap of one margin note by a second is avoided by pushing the second note downward below the first one. See section \ref{sec-overlap} for additional discussion.

\option{maxscale}{number}{100} Number of vertical length units/guides for placing margin notes---the height of the source doc page will be divided into \texttt{maxscale} units. If not divisible by 4, you will get a few ugly surplus lines at the top. 

\end{description}

\subsection{Options for controlling the appearance and formatting of notes and helplines}

\begin{description}

\option{notesbg}{color name or `none'}{yellow} Background color to use for the margin notes. If `none', the background will be transparent. 

\option{notesframe}{color name or `none'}{black} Color to use for the box frame of the margin notes. If `none', no box is drawn. 

\option{important}{frame color name}{red} Color to use for frame around notes declared with style `important' (e.g.\ |\tnote[important]{Important note!}|). Note that \texttt{important} is just a TikZ style declared inside the package; you can override this style with |\tikzset| take control of font, background color etc. 

\option{grid}{boolean}{true} Draw numbered guides for margin notes. 

\option{gridcolor}{color name}{black!30} Color to use for help guides and their numbers. 

\option{fontsize}{string}{footnotesize} Font size to use for the margin notes (and also the numbers on the guides, if present). Use any of the usual font size commands, but without the backslash.
\end{description}


\subsection{Other options}
\label{sec-options-other}

\begin{description}

\option{withnotesonly}{boolean}{false} By default, \texttt{pdfreview} will output every page that was mounted with \texttt{\textbackslash{}begin\textbraceleft page\textbraceright\ \ldots\ \textbackslash{}end\textbraceleft page\textbraceright}, even if no notes were placed on the page. When you set \texttt{withnotesonly=true}, pages on which no notes were made will be omitted from the output. This can be useful to reduce file size.

Extra pages inserted with \texttt{\textbackslash{}begin\textbraceleft insertpage\textbraceright\ \ldots\ \textbackslash{}end\textbraceleft insertpage\textbraceright} will always be included in the output, regardless of whether or not they were left empty.


\option{scaletopage}{number}{1} Page number of source document to use for scaling all pages. It sometimes happens that the first page of a PDF document has different proportions from the subsequent ones; in this case, you can use this option to scale to the second page (\texttt{scaletopage=2}) or another one. 

\option{insertpagemargin}{length}{2.5cm} If you have comments of a general nature, or ones that don't fit comfortably into the page margin, you can put them on separate pages insert one or more empty pages using the \texttt{insertpage} environment (see below).

By default, \pkgname leaves only very narrow empty page margins beyond the margin notes. For inserted text pages, it is more appropriate to use a wider margin; you can adjust it with this option. 

\option{pageoffset}{number}{0} This option only affects the PDF bookmarks for each page of the current document; it is intended for synchronizing the bookmarks with the page numbers of the source document. 

By default, \pkgname simply numbers all included pages consecutively, starting with 1. If the source document has, say, 5 unnumbered (or Roman-numbered) pages preceding the proper page 1, you can declare this with \texttt{pageoffset=-5}; in this case, the first 5 pages will be given Roman numerals, and Arabic numbers will start (at 1) on the sixth physical page. Similarly, if the source document is missing some logical pages at the beginning, you can correct for this using a positive value for \texttt{pageoffset}.%
%
\footnote{Attentive students of Korean appliance makers' owner's manuals will recognize this pattern---the least useful features require the most long-winded explanations.}

\end{description}

\section{Macros}
\label{sec-macros}

\begin{description}

\item [{\texttt{\textbackslash note[\meta{alignment}]\marg{height}\marg{text}}}] 
      The optional \texttt{alignment} argument is one of \texttt{b}, \texttt{c}, or \texttt{t}. The \meta{height} argument names the guide to align to; \meta{text} is the content of the note. (The \meta{height} argument doesn't have to be an integral number. Also, it can fall outside of the displayed guide scale, within reason.
      
      Note that the entire text of each note will go inside a \texttt{TikZ} node. This means you cannot use empty lines within a note to insert paragraphs. If you do need paragraphs, use the |\par| macro instead.
      
\item [\texttt{\textbackslash bnote}\marg{height}\marg{text}] 
      Shorthand for \texttt{\textbackslash note[b]\marg{height}\marg{text}}. Analogous |\cnote| and |\tnote|. 

\item [\texttt{\textbackslash sourcedoc}\marg{file name}] Set the file name of the PDF file whose pages are to be included. More commonly, this is done using the \texttt{sourcedoc} package option (see above). However, this macro can be used to switch to another source document halfway through. 

\item [{\texttt{\textbackslash pagegrid[\meta{tikz settings}][\meta{steps}]}}] 

Occasionally, you may want to place a note right on top of the source page instead of in the margin. To this end, the \texttt{page} environment can be used like a \texttt{tikzpicture} environment (see section \ref{sec-environments}). This macro assists by drawing a grid of horizontal and vertical guides over the source doc pages. If you leave the \meta{tikz settings} empty, the lines will be drawn in the same style as the guides for the margin notes, but you can use e.g.\ \texttt{\textbackslash pagegrid[red,\,thick]} to make them stand out more. The \meta{steps} parameter controls the density of the grid. The number of guides in each dimension is given by 100/\meta{steps}; the default is 10. 

\end{description}

\begin{figure}
\centering
\includegraphics[clip,trim=0 1.7in 0 1.7in,width=\textwidth,page=2]{sample}
\caption{How to draw things on top of the included page, and how to muck up vertical spacing of the margin notes by misplacing a \texttt{leftnotes} environment.}
\label{fig-drawing}
\end{figure}

\section{Environments}
\label{sec-environments}

\begin{description}

\item [\texttt{\textbackslash begin\textbraceleft page\textbraceright [\meta{graphics options}]\marg{page number} \ldots\ \textbackslash end\textbraceleft page\textbraceright}] 

This environment loads the physical page \meta{page number} from the source document and sets up the containers for the margin notes. The formatting and scaling of the included page and of the margin notes is subject to the package options described earlier. 

The optional \meta{graphics options} are passed to the \texttt{\textbackslash adjustbox} macro from the  package of the same name. You could use this for example to override the global \texttt{trim} package option for this one page, like so: \texttt{trim=\textbraceleft0cm 1cm\textbraceright}. 

Another point to note is that the \texttt{page} environment can be used like a \texttt{tikzpicture} environment for drawing or placing nodes on top of the included source doc page. The dimension-less $x$ and $y$ units have been adjusted such that the point (0,\,0) is at the bottom left, and (100,\,100) is at the top right of the source doc page. That means you can say |\node[sticky] at (50,50) \{Sheesh!\}| to place some text smack-dab in the middle of the included page. (The \texttt{sticky} TikZ style will format the node text like the notes in the margin.) 
You can use the |\pagegrid| macro to draw a grid of guides over the included page for this purpose (see section \ref{sec-macros} and Figure \ref{fig-drawing}).

\item [\texttt{\textbackslash begin\textbraceleft leftnotes\textbraceright \ldots\ \textbackslash end\textbraceleft leftnotes\textbraceright}] 

This environment goes inside a \texttt{page} environment. In \texttt{twocolumn} mode, it will move the enclosed |\note| macros to the left page margin; with \texttt{twocolumn=false} it does nothing, that is, notes enclosed in \texttt{leftnotes} will also show up in the right margin. In this case, note that the \texttt{staggered=true} option will fail to prevent overlap between notes within and outside the \texttt{leftnotes} environment, respectively. 

There should be only one \texttt{leftnotes} environment within each \texttt{page} environment, and it should come either before or after all notes intended for the right margin. Otherwise, the mechanism for avoiding vertical overlap of margin notes that is activated by the package option \texttt{staggered=true} will not work properly.

\item [\texttt{\textbackslash begin\textbraceleft rightnotes\textbraceright \ldots\ \textbackslash end\textbraceleft rightnotes\textbraceright}] A no-op; it only exists to avoid tripping up users who might surmise its existence from that of the \texttt{leftnotes} environment.

\item [\texttt{\textbackslash begin\textbraceleft insertpage\textbraceright [\meta{heading}] \ldots\ \textbackslash end\textbraceleft insertpage\textbraceright}] 

Inserts one or more extra pages for longer comments. These are not subject to any special formatting, except for the page margin, which can be set using the \texttt{insertpagemargin} package option (see section \ref{sec-options-other}). Alternatively, you can set the margins manually using |\newgeometry| and |\restoregeometry| from the \texttt{geometry} package, which is loaded by \pkgname. (The optional argument \meta{heading} defaults to ``General comments''.)

\end{description}

\section{Practical recommendations}

\subsection{Page space usage}

Obviously, one needs to make some judicious choices in order to allow enough space for our margin comments yet also keep the included PDF pages legible. Use the \texttt{trim} package option to shave off empty margins from the included PDF pages. 

The \texttt{geometry} package will be loaded to divide up the page. By default, \pkgname leaves 0.25 cm empty margins on both sides. If you load the \texttt{geometry} package yourself, \pkgname will respect your settings and not impose its own margin width. In that case, note that the entire content---included PDF page and margin notes---will go inside |\textwidth|. Any margins you declare using the \texttt{geometry} package will remain entirely empty. Also note that any \texttt{geometry} settings can be declared in the preamble only; if you try to change the margins within the document using |\newgeometry|, things will unravel. 

\subsection{Vertical overlap of margin notes}
\label{sec-overlap}

Beginning with version 1.2, vertical overlap of margin notes can be prevented by using the package option \texttt{staggered=true}; this will cause any note that would overlap the preceding one to be pushed downwards (see Figure \ref{fig-drawing}). I myself use it all the time, and I have therefore made this the default.

If for some reason you don't want to use this convenience, then you will have to prevent vertical overlap manually. Where space is tight, it may help to use e.g.\ a |\bnote{60}{some note}| followed by a |\tnote{60}{another note}|; no space is wasted between both notes, yet at the same time they will not overlap.

The \texttt{bodywidth} package option also decides the horizontal and vertical space available for the margin notes. Should you decide half-way through to reduce the \texttt{bodywidth} parameter, this will create more horizontal space for the notes, but at the same time reduce the vertical space. This may occasionally create some new vertical overlap that would have to be fixed manually.

Another consideration is what font to use for your notes, and at what size. Times is a nice, space-saving font (in pdfLaTeX, you can load it with |\usepackage{mathptmx}|). You can adjust the font size using the corresponding package option (e.g.~\texttt{fontsize=scriptsize}).

\subsection{Writing less boiler plate code}

While the code to set up each page is pretty compact, it can still become tiresome to write for long source documents. The package includes a \texttt{bash} script named \texttt{pdfreview.sh}, which generates a document skeleton containing a \texttt{page} environment for each page of a source document: 

\begin{verbatim}
mpalmer@holzkopf:/data$ pdfreview.sh source.pdf > review.tex
\end{verbatim}

\noindent The script requires 1) \texttt{ghostscript}, 2) \texttt{bash} (obviously) and 3) that you place it somewhere into your shell's path. 

\subsection{File size}

Particularly when working with large source files, \pkgname can produce some startlingly large PDF output files. This problem is apparently related to the way |\includegraphics| works and has been described before (see \url{https://tex.stackexchange.com/questions/207527/}).
%
It can often be amended by post-processing the output PDF file with ghostscript, for example: 

\begin{verbatim}
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET \
   -dBATCH -sOutputFile=post-processed.pdf output.pdf
\end{verbatim}

\noindent The package contains a bash script named \texttt{pdfshrink.sh} that wraps the call to \texttt{ghostscript} and which you can use like 

\begin{verbatim}
mpalmer@holzkopf:/data$ pdfshrink.sh large.pdf
\end{verbatim}

\noindent An optional second argument controls the resolution of included bitmap graphics, with possible values (in order of decreasing resolution) \texttt{prepress}, \texttt{printer}, \texttt{default} (the default value), \texttt{ebook}, and \texttt{screen}. A  optional third argument can be used to set the output file name.

Again, you need to save the script in a directory listed in your shell's search path for this work. 

\subsection{Uncooperative input files}

Occasionally, I've run into source documents that failed to cooperate with the page-wise inclusion attempted by \pkgname, and/or with the \texttt{pdfreview.sh} code generation script, which internally uses ghostscript. I got around that by first printing the recalcitrant PDF input file to another PDF file---or, as long as ghostscript wasn't the problem, running through the  \texttt{pdfshrink.sh} script---and then using the resulting file as the source document instead. (This might also be useful on occasion when using the \texttt{pdfpages} package.)

\section{Implementation}

This package depends on several others that are all in TeXLive. Among these is the \texttt{ulem} package, which makes the |\sout{strike me out}| command available for a strike-through effect. The \texttt{geometry} package is used for dividing up the page; if you load it yourself, your settings will be respected, as long you declare them in the preamble (but don't use |\newgeometry| halfway through the document). The \texttt{adjustbox} package is used to scale and trim the included pages, and also to scale the containers for the margin notes. The \texttt{environ} package is used to implement the \texttt{page} environment. The \texttt{tikz} package is used for placing guides and margin notes. 
      
For further details, see the package file itself.

\section{Revision history}

\begin{description}
\item [May 2017] initial version (1.00)

\item [September 2017] corrections to documentation; added \texttt{withnotesonly} option (1.1)

\clearpage 

\item [February 2019] (version 1.2)

    \begin{itemize}
    \item added package options \texttt{staggered}, \texttt{important}, and \texttt{scaletopage}
    \item removed \texttt{stretch} option
    \item added |\pagegrid| macro
    \item updated documentation 
    \item updated auxiliary shell scripts to work with file names containing spaces, and to prevent loss of internal hyperlinks caused by ghostscript reprocessing
    \end{itemize}

\end{description}

\section{License}

All files in this package are subject to the LaTeX project public license (see \url{http://www.latex-project.org/lppl.txt}). 


\end{document}