semtex -- For stripped SemanTeX documents
--------------------------------------

The package semtex is a small LaTeX package that adds a collection
of simple macros for parentheses and bullets. It exists for
one purpose only: To be loaded by documents which were
originally typeset using the package SemanTeX, but which have
been stripped of SemanTeX markup using the package stripsemantex.
Therefore, unless your document is one of those,
simply **don't use this package**.

And even if your document *is* one of those, there is a good
chance you will not have to load it after all. In most cases,
you will be able to replace the macros it provides by macros
from other packages.

----------------------------------------------------------------
semtex -- For stripped SemanTeX documents
Maintained by Sebastian Ørsted
E-mail: sorsted@gmail.com
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
----------------------------------------------------------------

Copyright (C) 2020 by Sebastian Ørsted <sorsted@gmail.com>

The package is loaded via \usepackage{semantex}

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License (LPPL), either
version 1.3c of this license or (at your option) any later
version.  The latest version of this license is in the file:

http://www.latex-project.org/lppl.txt

This work is "maintained" (as per LPPL maintenance status) by
Sebastian Ørsted.