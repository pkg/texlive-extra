# Intelligent brackets - The ibrackets package


## Presentation

This small package provides a new definition of brackets [ and ] as active characters
to get correct blank spaces in mathematical mode when using for open intervals 
instead of parenthesis: ]-\infty, 0[ is equivalent to (-\infty, 0).


## Installation

- run LaTeX on ibrackets.ins, you obtain the file ibrackets.sty,
- if then you run pdfLaTeX on ibrackets.dtx you get the file ibrackets.pdf which is also in the archive,
- put the files ibrackets.sty and ibrackets.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
