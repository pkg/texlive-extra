|
---------:| ---------------------------------------------------------------
proofread:| Commands for inserting annotations
   Author:| Wybo Dekker
   E-mail:| wybo@dekkerdocumenten.nl
  License:| Released under the LaTeX Project Public License v1.3c or later
      See:| http://www.latex-project.org/lppl.txt

Short description:
The proofread package defines a few LaTeX commands that are useful
when you proofread a latex document. These allow you to easily highlight
text and add comments in the margin. Vim escape sequences are provided
for inserting these LaTeX commands in the source. The package is based
on code for a text highlighting command that was published by Antal
Spector-Zabusky in http://tex.stackexchange.com/questions/5959. The main file,
proofread.dtx, is self-extracting, so you can generate the style file by
compiling proofread.dtx with pdflatex.

