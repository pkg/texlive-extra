
 *File List*
-RELEASE.---    ----------
fileinfo.RLS    2012/11/30 r0.81a PDFs with fixed `makedoc.cfg'
-----USE.---    ----------
myfilist.sty    2012/11/22 v0.71 \listfiles -- mine only (UL)
readprov.sty    2012/11/22 v0.5 file infos without loading (UL)
-----DOC.---    ----------
myfilist.tex    2012/11/30 documenting myfilist.sty
readprov.tex    2012/11/22 documenting readprov.sty
----USED.---    ----------
 makedoc.cfg    2012/11/30 documentation settings
fifinddo.sty    2012/11/17 v0.61 filtering TeX(t) files by TeX (UL)
 makedoc.sty    2012/08/28 v0.52 TeX input from *.sty (UL)
niceverb.sty    2012/11/27 v0.51 minimize doc markup (UL)
  README.tex    2012/03/18 make README.pdf
fdatechk.tex    2012/11/13 filedate checks
  gather.tex    2012/11/22 collect file infos
 ***********

 List made at 2012/11/30, 16:41
 from script file gather.tex

