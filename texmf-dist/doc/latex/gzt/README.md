|
-------:| -----------------------------------------------------------------
    gzt:| Bundle of two classes and BibLaTeX styles for the French journal “La Gazette de la Société Mathématique de France”
 Author:| Denis Bitouzé
 E-mail:| denis.bitouze@univ-littoral.fr
License:| Released under the LaTeX Project Public License v1.3c or later
    See:| http://www.latex-project.org/lppl.txt

This bundle provides two classes and BibLaTeX styles for the French journal [“La
Gazette de la Société Mathématique de France”](https://smf.emath.fr/les-publications/la-gazette):

- `gzt` for the complete issues of the journal, aimed at the Gazette’s team,
- `gztarticle`, intended for authors who wish to publish an article in the
  Gazette. This class’s goals are to
  - faithfully reproduce the layout of the Gazette, thus enabling the authors to
    be able to work their document in actual conditions, and
  - provide a number of tools (commands and environments) to facilitate the
    drafting of documents, in particular those containing mathematical formulas.
