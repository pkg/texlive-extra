v0.2 - 2013-08-09
 - MAJOR CHANGE OF BIBLIOGRAPHY RULES: To be compatible with the major
   changes of the bibliography rules of the Institute for Linguistics
   Wuppertal that took place with the release of the new 5th edition
   of the brochure "Germanistik in Wuppertal", this style had to be
   changed in the following ways:
 - Change of rule dependency: Since the journal ZS is no longer
   recommended for cases of doubt by the brochure, only the Unified
   style sheet for linguistics (USSL) is being used for fallback rules.
   This reduces the checking of interdependent rule changes for future
   updates (before: own interpretation > BUW > ZS > USSL + Style sheet
   for Mouton journals, after: own interpretation > BUW > USSL).
 - Added an = before the series.
 - No comma anymore between series and number.
 - Series now at the very end of the entry.
 - Colon instead of comma before pages of journal (before: USSL rule,
   after: BUW rule).
 - Number of journal now divided from volume by colon instead of being
   in parathesis without space directy after volume (before: USSL rule,
   after: BUW rule).
 - Page now after publisher+location instead of before.
 - Now "URL:" before URL (before: USSL rule, after: BUW rule).
 - Changed both multinamedelim and finalnamedelim to a slash.
 - No forced comma after note anymore.
 - Edition as a superscripted number after the year if field edition is
   integer.
 - Edition as usual text after published+location if field edition is
   not integer.
 - Added option edsuper for deactivating superscripted edition. 
 - Updated documentation (see also for detailed explanation of how the
   BUW rules have been interpreted (German only).

v0.1 - 2012-04-17
 - Initial release
