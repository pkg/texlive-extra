# hvpygmentex.sty

Formatting source code of programming languages.

The package is based on pygmentex but provides an automatic
run from within the document itself, with the option "--shell-escape". 
It do not needs the additional action by the user to run the external 
program pygmentize to create the code snippets.

%% It may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.

hvoss@tug.org