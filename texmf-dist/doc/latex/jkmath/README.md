README for jkmath.sty

jkmath 0.1
Created by Jonas Kaerts
Updated 24th of March 2018

This material is subject to the LaTeX Project Public License.
See http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for 
the details of that license.

Inspired by the physics package on CTAN, this package defines some simple macros for mathematical notation which make the code more readable and/or allow flexibility in typesetting material.

The package is currently being developed as I gather examples of special notation fit for new macro's.
