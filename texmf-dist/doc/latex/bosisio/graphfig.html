<html>
<!--
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
|            Copyright(C) 1997-2010 by F. Bosisio             |
|                                                             |
| This program can be redistributed and/or modified under     |
| the terms of the LaTeX Project Public License, either       |
| version 1.3 of this license or (at your option) any later   |
| version. The latest version of this license is in           |
|   http://www.latex-project.org/lppl.txt                     |
| and version 1.3 or later is part of all LaTeX distributions |
| version 2005/12/01 or later.                                |
|                                                             |
| This work has the LPPL maintenance status `maintained'.     |
| The Current Maintainer of this work is F. Bosisio.          |
|                                                             |
| This work consists of files graphfig.dtx and graphfig.html  |
| and of the derived files graphfig.sty and graphfig.pdf.     |
|                                                             |
| E-mail:   fbosisio@bigfoot.com                              |
| CTAN location: macros/latex/contrib/bosisio/                |
+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
-->
<head>
  <title>Package graphfig</title>
</head>
<body>

<h1 align="CENTER">
Package <tt>graphfig</tt><br />
<small>
This is version 2.2, last revised 1997/12/15; documentation date 2005/04/09.
</small>
</h1>

<p align="CENTER">
<strong>
Author: <i>F. Bosisio</i><br />
<small>
E-mail: <a href="mailto:fbosisio@bigfoot.com">
	<tt>fbosisio@bigfoot.com</tt>
	</a><br />
CTAN location: <a href="http://mirror.ctan.org/macros/latex/contrib/bosisio">
	       <tt>macros/latex/contrib/bosisio</tt>
	       </a>
</small>
</strong>
</p>

<p align="LEFT"></p>

<!------------------------------------------------------------------------>
<p>

<h3>Abstract</h3>
<div>
Documentation for the package <tt>graphfig</tt>.
</div>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>Table of contents</h2>

<ul>
  <li><a href="#SECTION01">Introduction</a></li>
  <li><a href="#SECTION02">Required packages</a></li>
  <li><a href="#SECTION03">The options</a></li>
  <li><a href="#SECTION04">The <tt>Figure</tt> environment and its relatives</a>
    <ul>
      <li><a href="#SECTION04a">The <tt>graphfile</tt> command</a></li>
      <li><a href="#SECTION04b">Sub-figures</a></li>
    </ul></li>
</ul>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>
<a name="SECTION01">Introduction</a>
</h2>

<p>
This package provides some commands to make the use of graphic files
in LaTeX simpler.
</p>

<p>
It declares the ``<tt>Figure</tt>'' environment (capitalized!) and the two
commands ``<tt>\graphfile</tt>'' and ``<tt>\graphfile*</tt>''.
Combining this commands, it is possible to include graphic files in a
LaTeX document very simply.
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>
<a name="SECTION02">Required packages</a>
</h2>

<p>
This package uses the ``<tt>\includegraphics*</tt>'' command defined in
the standard <tt>graphics</tt> package.
Moreover, it uses the package <tt>subfigure</tt> when the
<a href="#SECTION03"><tt>subfigure</tt> option</a> is specified, and the
<tt>float</tt> package if the <a href="#SECTION03"><tt>AllowH</tt> option</a>
is used.
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>
<a name="SECTION03">The options</a>
</h2>

<p>
At now, two options are available: ``<tt>subfigure</tt>'' and
``<tt>AllowH</tt>''.
</p>

<p>
The ``<tt>subfigure</tt>'' option allow the use of sub-figures inside a
<a href="#SECTION04"><tt>Figure</tt> environment</a>, in order to place
multiple pictures in a single LaTeX figure (cfr. the <tt>subfigure</tt>
standard package).
</p>

<p>
The ``<tt>AllowH</tt>'' option allow the use of the ``<tt>H</tt>'' float
specifier for the <a href="#SECTION04"><tt>Figure</tt> environment</a>,
in order to place the figure exaclty where the command is placed
(cfr. the <tt>float</tt> standard package).
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<h2>
<a name="SECTION04">The <tt>Figure</tt> environment and its relatives</a>
</h2>

<p>
The ``<tt>Figure</tt>'' environment (capitalized!) is somewhat different from
the standard LaTeX ``<tt>figure</tt>'' environment: besides an optional
argument used to specify the placement parameters (which now defaults to
``<tt>[htbp]</tt>''), it has a mandatory agrument specifying the ``caption''
and another optional argument, used as a ``label'' for cross-referencies:
<pre>
		\begin{Figure}[&lt;htbpH&gt;]{&lt;caption&gt;}[&lt;label&gt;]
		            ...
		\end{Figure}
</pre>
</p>

<p>
The use of the ``<tt>H</tt>'' specifier (i.e. ``I want my float here!'') is
possible only if the <a href="#SECTION03">``<tt>AllowH</tt>'' option</a> has
been specified.
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<h3>
<a name="SECTION04a">The <tt>graphfile</tt> command</a>
</h3>

<p>
Inside the ``<tt>Figure</tt>'' environment, are available the commands:
<pre>
		\graphfile[&lt;width&gt;]{&lt;file&gt;}[&lt;sub-caption&gt;]
		\graphfile*[&lt;heigth&gt;]{&lt;file&gt;}[&lt;sub-caption&gt;]
</pre><br />
which are a simplified version of the ``<tt>\includegraphics*</tt>''
command (which is automatically included by this package; see the
<tt>graphics</tt> package for reference), since you don't have to worry
about scaling: the mandatory argument is the name of the graphic file to
include, whereas the first optional argument specifies the desired width
(in the non *-form) or heigth (in the *-form) of the figure as a fraction
of ``<tt>\linewidth</tt>'' (e.g. ``|50|'' means
``<tt>.50\linewidth</tt>'', i.e. half a line! ), so no unit of measure
(as ``<tt>cm</tt>'' or ``<tt>pt</tt>'') is required.
</p>

<p>
Moreover, since the ``*-form'' of ``<tt>\includegraphics</tt>'' is used,
the regions outside the bounding-box of postscript files are not drawn.
</p>

<p>
Another advantage of the combined use of the
<a href="#SECTION04">``<tt>Figure</tt>'' environment</a> and the
``<tt>\graphfile</tt>'' commands is that the picture is automatically
centered horizontally, so no ``<tt>\centering</tt>'' or similar
declartion is required.
<pre>
		\begin{Figure}[&lt;htbpH&gt;]{&lt;caption&gt;}[&lt;label&gt;]
		        \graphfile[&lt;width&gt;]{&lt;file&gt;}
		\end{Figure}
</pre>
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<h3>
<a name="SECTION04b">Sub-figures</a>
</h3>

<p>
If you want to include more than one file in a single figure, you may
specify <a href="#SECTION03">the ``<tt>subfigure</tt>'' option</a>, which
includes the <tt>subfigure</tt> package and provides the authomatic
placement of a sub-caption below each picture.
</p>

<p>
If the ``<tt>subfigure</tt>'' option is not specified, multiple graphs are
still allowed but no sub-caption will appear.
<pre>
		\graphfile[&lt;width_1&gt;]{&lt;file_1&gt;}[&lt;sub-caption_1&gt;]
			...
		\graphfile[&lt;width_N&gt;]{&lt;file_N&gt;}[&lt;sub-caption_N&gt;]
</pre><br />
This is a combined version of ``<tt>\subfigure</tt>'' and
``<tt>\includegraphics*</tt>'' (see the <tt>subfigure</tt> and
<tt>graphics</tt> packages for reference):
the last optional argument specify a ``caption'' for the sub-figure under
consideration, while the first two arguments work exactly like as described
above in the case of one only picture (indeed, you can use the last optional
argument even if the <tt>subfigure</tt> option was not specified, in which
case it is simply ignored).
</p>

<p>
Each individual caption is printed preceded by a bracketed letter, which is
the sub-figure counter and is printed even if no caption is specified.
Using only a series of ``<tt>\graphfile[...]{...}[...]</tt>'' commands
inside a ``<tt>Figure</tt>'' environment provides an equal spacing beetween
the pictures and around them, without the need for any extra command.
</p>

<p>
Finally, if a ``<tt>&lt;label&gt;</tt>'' was specified as the last optional
argument to the <a href="#SECTION04">``<tt>Figure</tt>'' environment</a>,
you can reference to each individual sub-figure by the labels
``<tt>&lt;label&gt;:a</tt>'', ``<tt>&lt;label&gt;:b</tt>'', and so on, without
the need for declaring them.
<pre>
	\begin{Figure}[&lt;htbpH&gt;]{&lt;caption&gt;}[&lt;label&gt;]
		\graphfile[&lt;width_1&gt;]{&lt;file_1&gt;}[&lt;sub-caption_1&gt;]
		\graphfile[&lt;width_2&gt;]{&lt;file_2&gt;}[&lt;sub-caption_2&gt;]
		       :
		       :
	\end{Figure}
</pre>
</p>

<hr /><br />
<!------------------------------------------------------------------------>

<address>
  <i>F. Bosisio</i><br />
  E-mail: <a href="mailto:fbosisio@bigfoot.com"><tt>fbosisio@bigfoot.com</tt></a>
</address>

<!------------------------------------------------------------------------>

</body>
</html>

