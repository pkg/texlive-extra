

# The `hep-math` package

Extended math macros

## Introduction

The `hep-math` package provides some additional features beyond the `mathtools` and `amsmath` packages.

To use the package place `\usepackage{hep-math}` in the preamble.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

