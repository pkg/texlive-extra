%%
%% This is file `hep-math-documentation.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% hep-math-implementation.dtx  (with options: `documentation')
%% This is a generated file.
%% Copyright (C) 2019-2020 by Jan Hajer
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. The latest version of this license is in:
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.

\ProvidesFile{hep-math-documentation.tex}[2022/11/01 v1.1 hep-math documentation]

\RequirePackage[l2tabu, orthodox]{nag}

\documentclass{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\AtBeginDocument{\MakeShortVerb{\"}}

\EnableCrossrefs
\CodelineIndex
\RecordChanges

\usepackage[parskip,oldstyle]{hep-paper}

\newenvironment{columns}[1][.5]{%
  \par\vspace{-\bigskipamount}%
  \begin{minipage}[t]{\linewidth}%
  \begin{minipage}[t]{#1\linewidth}%
  \def\column{%
    \end{minipage}%
    \begin{minipage}[t]{\linewidth-#1\linewidth}%
  }%
}{\end{minipage}\end{minipage}\par}

\bibliography{bibliography}

\usepackage{hologo}

\MacroIndent=1.5em
\setlength{\fboxsep}{1pt}
\AtBeginEnvironment{macrocode}{\renewcommand{\ttdefault}{clmt}}

\GetFileInfo{hep-math.sty}

\title{The \software{hep-math} package\thanks{This document corresponds to \software{hep-math}~\fileversion.}}
\subtitle{Extended math macros}
\author{Jan Hajer \email{jan.hajer@tecnico.ulisboa.pt}}
\date{\filedate}

\begin{document}

\newgeometry{vscale=.8, vmarginratio=3:4, includeheadfoot, left=11em, marginparwidth=4.6cm, marginparsep=3mm, right=7em}

\maketitle

\begin{abstract}
The \software{hep-math} package provides some additional features beyond the \software{mathtools} and \software{amsmath} packages.
\end{abstract}

To use the package place "\usepackage{hep-math}" in the preamble.

The \software{mathtools} \cite{mathtools} package is loaded, which in turn loads the \hologo{AmSLaTeX} \software{amsmath} \cite{amsmath} package.
Horizontal spacing in inline equations and page breaks in block equations are marginally adjusted.
\DescribeMacro{\left}
\DescribeMacro{\right}
Spacing around "\left" and "\right" is fixed with the \software{mleftright} package \cite{mleftright}.

\section{Macros}

\DescribeMacro{\mathdef}
The "\mathdef"\marg{name}\oarg{arguments}\marg{code} macro \prefix{re}{defines} macros only within math mode without changing the text mode definition.

\DescribeMacro{\i}
\DescribeMacro{\d}
The imaginary unit "\i" and the differential "\d" are defined using this functionality.

\DescribeMacro{\overline}
The "\overline" macro is adjusted to \overline{work also outside} of math mode using the \software{soulutf8} \cite{soulutf8} package.

\DescribeMacro{\oset}
\DescribeMacro{\overleft}
\DescribeMacro{\overright}
\DescribeMacro{\overleftright}
A better looking over left right arrow is defined \ie $\overleftright{\partial}$ using a new "\oset"\marg{over}\marg{math} functionality.

\DescribeMacro{\diag}
\DescribeMacro{\sgn}
Diagonal matrix "\diag", signum "\sgn", trace "\tr", "\Tr", and "\rank" operators are defined.

\DescribeMacro{\Re}
\DescribeMacro{\Im}
The real and imaginary projectors are redefined to look like ordinary operators.

\DescribeMacro{\sin}
\DescribeMacro{\cos}
\DescribeMacro{\tan}
"\cos" and "\tan" are adjusted to have the same height as "\sin".

\DescribeMacro{\accsc}
"\arccsc" and other inverse trigonometric functions are defined.

\subsection{Fractions and units}

\DescribeMacro{\unit}
\DescribeMacro{\inv}
The correct spacing for units is provided by the macro "\unit"\oarg{value}\marg{unit} from the \software{units} package \cite{units} which can also be used in text mode.
The macro "\inv"\oarg{power}\marg{text} allows to avoid math mode also for inverse units such as \unit[5]{\inv{fb}} typeset via "\unit[5]{\inv{fb}}".

\DescribeMacro{\nicefrac}
\DescribeMacro{\flatfrac}
\DescribeMacro{\textfrac}
The "\frac"\marg{number}\marg{number} macro is accompanied by "\nicefrac"\linebreak[1]\marg{number}\linebreak[1]\marg{number}, "\textfrac"\marg{number}\marg{number}, and "\flatfrac"\marg{number}\marg{number} leading to $\frac12$, $\nicefrac12$, \textfrac12, and $\flatfrac12$.
The "\textfrac" macro is mostly intended if a font with oldstyle numerals is used.

Some macros of the \software{physics} package \cite{physics} are reimplemented with a more conventional typesetting in mind.
Finer details about mathematical typesetting can be found in \cite{gregorio:2020}.

\subsection{Differentials and derivatives}

\DescribeMacro{\differential}
\DescribeMacro{\newderivative}
\DescribeMacro{\newpartialderivative}
The three macros "\differential"\marg{symbol}, "\newderivative""{\name}"\marg{symbol}, and "\newpartialderivative""{\name}"\marg{symbol} allow to define a differential with correct spacing, a derivative using this differential, and if necessary a partial derivative that can handle three dimensional derivatives.

\DescribeMacro{\d}
\DescribeMacro{\dv}
These macros are used for the usual differential and derivative, producing $\d x$ via "\d x" and
\begin{center}
\begin{tabular}{*4c}
"\dv[f]x" & "\dv*[f]x^n" & "\dv[f]x*^n" & "\dv*[f]x*^n" \\
$\dv[f]x$ & $\dv*[f]x^n$ & $\dv[f]x*^n$ & $\dv*[f]x*^n$ \\
"\dv xf" & "\dv*xf" & "\dv x*f" & "\dv*x*f" \\
$\dv xf$ & $\dv*xf$ & $\dv x*f$ & $\dv*x*f$
\end{tabular}
\end{center}
via "\dv*"\oarg{f}\marg{x}"*""^"\marg{n}.
Upright differential can be produced via "\renewcommand" "{\diffsymbol}""{\mathrm d}".

\DescribeMacro{\pd}
\DescribeMacro{\pdv}
Similarly a partial differential and derivative are defined that can be used according to "\pdv*"\oarg{f}\marg{x}"*""^"\marg{a}\oarg{y}"^"\marg{b}\oarg{z}"^"\marg{c}.
\begin{center}
\begin{tabular}{*4c}
"\pdv[f]x" & "\pdv[f]x[y]" & "\pdv[f]x^3" & "\pdv[f]x^2[y]" \\
$\pdv[f]x$ & $\pdv[f]x[y]$ & $\pdv[f]x^3$ & $\pdv[f]x^2[y]$ \\
"\pdv[f]x^2[y]^3" & "\pdv[f]x[y]^3" & "\pdv x[y]f"\\
$\pdv[f]x^2[y]^3$ & $\pdv[f]x[y]^3$ & $\pdv x[y]f$
\end{tabular}
\end{center}

\DescribeMacro{\var}
\DescribeMacro{\fdv}
Similarly a functional variation and functional derivative are defined.

\DescribeMacro{\cancel}
\DescribeMacro{\slashed}
The "\cancel"\marg{characters} macro from the \software{cancel} package \cite{cancel} and the "\slashed" \marg{character} macro from the \software{slashed} package \cite{slashed} allow to $\cancel{\text{cancel}}$ math and use the Dirac slash notation \ie $\slashed \pd$, respectively.

\subsection{Paired delimiters}

\DescribeMacro{\abs}
\DescribeMacro{\norm}
\begin{center}
\begin{tabular}{*8r}
"\abs x" & "\norm x" & "\norm[2]x" & "\norm*[2]x" \\
$\abs x$ & $\norm x$ & $\norm[2]x$ & $\norm*[2]x$
\end{tabular}
\end{center}

\DescribeMacro{\eval}
\DescribeMacro{\order}
\begin{center}
\begin{tabular}{*8r}
"\order x" & "\eval x_0^\infty" & "\eval* x_0^\infty" \\
$\order x$ & $\eval x_0^\infty$ & $\eval* x_0^\infty$
\end{tabular}
\end{center}

\DescribeMacro{\newpair}
\DescribeMacro{\comm}
\DescribeMacro{\acomm}
The "\newpair"\marg{name}\marg{left delim} \marg{right delim}"_"\marg{subscript}"^"\marg{superscript} macro is defined and used for the definition of \prefix{anti}{commutators} and Poisson brackets.
\begin{center}
\begin{tabular}{*8r}
"\pb xy" & "\comm xy" & "\acomm xy" \\
$\pb xy$ & $\comm xy$ & $\acomm xy$
\end{tabular}
\end{center}
They can easily be redefined using \eg "\newpair\comm\lbrack\rbrack_-".

\DescribeMacro{\bra}
\DescribeMacro{\ket}
\DescribeMacro{\braket}
\DescribeMacro{\ketbra}
\DescribeMacro{\mel}
\DescribeMacro{\ev}
\DescribeMacro{\vev}
Macros for the bra-ket notation are introduced.
\begin{center}
\begin{tabular}{*4c}
"\bra x" & "\ket x" & "\braket xy" & "\ketbra xy" \\
$\bra x$ & $\ket x$ & $\braket xy$ & $\ketbra xy$ \\
"\mel xyz" & "\ev x" & "\ev[\Omega] x" & "\vev x" \\
$\mel xyz$ & $\ev x$ & $\ev[\Omega] x$ & $\vev x$
\end{tabular}
\end{center}

\DescribeMacro{\column}
\DescribeMacro{\row}
Macros for row and column vectors are introduced together with a symbol for transpose vectors.
\begin{center}
\begin{tabular}{*4c}
"\column{x,y,z}" & "\row{x,y,z}^\trans" \\
$\column{x,y,z}$ & $\row{x,y,z}^\trans$ \\
\end{tabular}
\end{center}

\section{Environments}

\DescribeMacro{eqnarray}
The "eqnarray" environment is depreciated, the "split", "multline", "align", "multlined", "aligned", "alignedat", and "cases" environments of the \software{amsmath} and \software{mathtools} packages should be used instead.

\DescribeMacro{equation}
Use the "equation" environment for short equations.
\begin{columns}
\begin{verbatim}
\begin{equation}
  left = right \ .
\end{equation}
\end{verbatim}
\column
\begin{equation}
\framebox[2em]{left\strut} = \framebox[7em]{right\strut} \ .
\end{equation}
\end{columns}

\DescribeMacro{multline}
Use the "multline" environment for longer equations.
\begin{columns}
\begin{verbatim}
\begin{multline}
  left = right 1 \\
  + right 2 \ .
\end{multline}
\end{verbatim}
\column
\begin{multline}
\framebox[2em]{left\strut} = \framebox[7em]{right 1\strut} \\
\framebox[7em]{+ right 2\strut} \ .
\end{multline}
\end{columns}

\DescribeMacro{split}
Use the "split" sub environment for equations in which multiple equal signs should be aligned.
\begin{columns}
\begin{verbatim}
\begin{equation} \begin{split}
  left &= right 1 \\
  &= right 2 \ .
\end{split} \end{equation}
\end{verbatim}
\column
\begin{equation}
\begin{split}
\framebox[2em]{left\strut} &= \framebox[7em]{right 1\strut} \\
&= \framebox[7em]{right 2\strut} \ .
\end{split}
\end{equation}
\end{columns}

\DescribeMacro{align}
Use the "align" environment for the vertical alignment and horizontal distribution of multiple equations.
\begin{columns}
\begin{verbatim}
\begin{subequations} \begin{align}
  left &= right \ , &
  left &= right \ , \\
  left &= right \ , &
  left &= right \ .
\end{align} \end{subequations}
\end{verbatim}
\column
\begin{subequations}
\begin{align}
\framebox[2em]{left\strut} &= \framebox[3em]{right\strut} \ , &
\framebox[2em]{left\strut} &= \framebox[3em]{right\strut} \ , \\
\framebox[2em]{left\strut} &= \framebox[3em]{right\strut} \ , &
\framebox[2em]{left\strut} &= \framebox[3em]{right\strut} \ .
\end{align}
\end{subequations}
\end{columns}

\DescribeMacro{aligned}
Use the "aligned" environment within a "equation" environment if the aligned equations should be labeled with a single equation number.

\DescribeMacro{multlined}
Use the "multlined" environment if either "split" or "align" contain very long lines.
\begin{columns}
\begin{verbatim}
\begin{equation} \begin{split}
  left &= right 1 \\ &=
  \begin{multlined}[t]
     right 2 \\ + right 3 \ .
  \end{multlined}
\end{split} \end{equation}
\end{verbatim}
\column
\begin{equation}
\begin{split}
\framebox[2em]{left\strut} &= \framebox[7em]{right 1\strut} \\ &=
 \begin{multlined}[t]
  \framebox[7em]{right 2\strut} \\
  \framebox[7em]{+ right 3\strut} \ .
\end{multlined}
\end{split}
\end{equation}
\end{columns}

\DescribeMacro{alignat}
Use the "alignat" environment together with the "\mathllap" macro for the alignment of multiple equations with vastly different lengths.
\begin{columns}
\begin{verbatim}
\begin{subequations}
\begin{alignat}{2}
  left &= long right && \ , \\
  le. 2 &= ri. 2 \ , &
  \mathllap{le. 3 = ri. 3} & \ .
\end{alignat}
\end{subequations}
\end{verbatim}
\column
\begin{subequations}
\begin{alignat}{2}
\framebox[2em]{left\strut} &=
\framebox[11em]{long right\strut} && \ , \\
\framebox[2em]{le.\ 2\strut}
&= \framebox[2.5em]{ri.\ 2\strut} \ , &
\mathllap{\framebox[2em]{le.\ 3\strut}
= \framebox[2.5em]{ri.\ 3\strut}} & \ .
\end{alignat}
\end{subequations}
\end{columns}

As a rule of thumb if you have to use "\notag", "\nonumber", or perform manual spacing via "\quad" you are probably using the wrong environment.

\printbibliography

\end{document}

\endinput
%%
%% End of file `hep-math-documentation.tex'.
