%%
%% thuaslogos.tex - documentation of logos for THUAS - Dutch version
%%
%% Logos for The Hague University of Applied Sciences (THUAS)
%% by Jesse op den Brouw
%%
%% Copyright (c)2019, Jesse E. J. op den Brouw
%%

%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX 
%% version 2003/12/01 or later.

%% This work consists of the files thuaslogos.sty,
%% thuaslogos.tex

%% This software is provided 'as is', without warranty of any kind,
%% either expressed or implied, including, but not limited to, the
%% implied warranties of merchantability and fitness for a
%% particular purpose.

%% Jesse op den Brouw
%% Department of Electrical Engineering
%% The Hague University of Applied Sciences
%% Rotterdamseweg 137, 2628 AL, Delft
%% Netherlands
%% J.E.J.opdenBrouw@hhs.nl

%% The newest version of this documentclass should always be available
%% from BitBucket: https://bitbucket.org/jesseopdenbrouw/thuaslogos

%% Version 1.1
%%

\documentclass[a4paper,12pt]{article}

\usepackage{thuaslogos}
\usepackage{parskip}
\usepackage[dutch]{babel}
\usepackage{float}
\usepackage{multicol}
\usepackage{tocloft}
\renewcommand\cftsecleader{\cftdotfill{\cftdotsep}}
%\renewcommand\cftsecafterpnum{\vspace*{-1ex}}
\usepackage{tikz}
\usetikzlibrary{scopes,fadings}
\usepackage[hyphens]{url}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{charter}
\usepackage[left=1.2in,right=1.2in,top=1in,bottom=1.5in,footskip=0.4in]{geometry}


\author{Jesse op den Brouw\thanks{De Haagse Hogeschool, \texttt{J.E.J.opdenBrouw@hhs.nl}}}
\title{THUAS logo's\\[2ex] \large Logo's van De Haagse Hogeschool / The Hague University of Applied Sciences in het Nederlands en Engels v\fileversion}
\date{\today}

\begin{document}
\maketitle

\begin{multicols}{2}[\setlength{\columnsep}{30pt}\section*{\contentsname}]
\makeatletter
\@starttoc{toc}%
\makeatother
\end{multicols}
\clearpage

\section{Introductie}
De package \verb|thuaslogos| biedt een aantal logo's van De Haagse Hogeschool
(Engels: The Hague University of Applied Sciences) in de kleuren zwart, wit, grijs
en groen. De kleuren grijs en groen zijn specifiek voor de HHS/THUAS.

De package wordt geladen met de regel:

\verb|  \usepackage{thuaslogos}|

De package heeft geen opties. Op dit moment zijn de logo's van de HHS
en de faculteiten TIS, BFM, ITD, MO, SWE, BRV (PLS) en GVS (HNS) ge\"{\i}mplementeerd.
Daarnaast zijn nog de logo's van Let's Change beschikbaar.


\section{Waar te vinden}
De laatste versie wordt gepubliceerd op:

\url{https://bitbucket.org/jesseopdenbrouw/thuaslogos}


\section{De gebruikte kleuren}
De Haagse Hogeschool gebruikt in principe twee primaire kleuren: grijs en groen.

Grijs is Pantone kleur 432.
Dit staat gelijk aan CYMK (67,45,27,70) en RGB (34,51,67). Let op: de CYMK- en RGB-kleuren zijn anders dan gevonden kunnen worden bij Pantone.

Groen is Pantone kleur 2305. Dit staat gelijk aan CYMK (25,0,100,32) en RGB (158,167,0).

Als grijs en groen niet mogelijk zijn, kan zwart gebruikt worden. Daarnaast gebruikt
De Haagse Hogeschool nog een aantal secundaire kleuren. Zie hiervoor hoofdstuk~\ref{anderekleur}.

Als de package \verb|xcolor| geladen is, worden twee kleuren aangemaakt. Dit zijn
de kleuren \verb|thuasgreen| en \verb|thuasgrey| met de volgende definitie:

\begin{verbatim}
\definecolor{thuasgreen}{RGB}{158,167,0}
\definecolor{thuasgrey}{RGB}{34,51,67}
\end{verbatim}


\section{Logo's HHS Nederlands}
De logo's zijn afgebeeld op hun natuurlijke grootte. De omranding geeft de
grenzen van de logo's aan. Zij zijn geen onderdeel van de logo's.

\begin{verbatim}
\thuaslogodutchblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchblack}
\caption{HHS logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchgrey}
\caption{HHS logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchgreen}
\caption{HHS logo Nederlands groen.}
\end{figure}


\section{Logo's THUAS Engels}
De logo's zijn afgebeeld op hun natuurlijke grootte. De omranding geeft de
grenzen van de logo's aan. Zij zijn geen onderdeel van de logo's.

\begin{verbatim}
\thuaslogoenglishblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishblack}
\caption{THUAS logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishgrey}
\caption{THUAS logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishgreen}
\caption{THUAS logo Engels groen.}
\end{figure}


\section{Logo's TIS Nederlands}
De logo's van de faculteit TIS zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblacktis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblacktis}}
\caption{TIS logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreytis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreytis}}
\caption{TIS logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreentis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreentis}}
\caption{TIS logo Nederlands groen.}
\end{figure}


\section{Logo's TIS Engels}
De logo's van de faculteit TIS zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblacktis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblacktis}}
\caption{TIS logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreytis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreytis}}
\caption{TIS logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreentis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreentis}}
\caption{TIS logo Engels groen.}
\end{figure}


\section{Logo's BFM Nederlands}
De logo's van de faculteit BFM zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackbfm}}
\caption{BFM logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreybfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreybfm}}
\caption{BFM logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenbfm}}
\caption{BFM logo Nederlands groen.}
\end{figure}


\section{Logo's BFM Engels}
De logo's van de faculteit BFM zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackbfm}}
\caption{BFM logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreybfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreybfm}}
\caption{BFM logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenbfm}}
\caption{BFM logo Engels groen.}
\end{figure}


\section{Logo's ITD Nederlands}
De logo's van de faculteit ITD zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackitd}}
\caption{ITD logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreyitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreyitd}}
\caption{ITD logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenitd}}
\caption{ITD logo Nederlands groen.}
\end{figure}


\section{Logo's ITD Engels}
De logo's van de faculteit ITD zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackitd}}
\caption{ITD logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreyitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyitd}}
\caption{ITD logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenitd}}
\caption{ITD logo Engels groen.}
\end{figure}


\section{Logo's MO Nederlands}
De logo's van de faculteit MO zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackmo}}
\caption{MO logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreymo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreymo}}
\caption{MO logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenmo}}
\caption{MO logo Nederlands groen.}
\end{figure}


\section{Logo's MO Engels}
De logo's van de faculteit MO zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackmo}}
\caption{MO logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreymo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreymo}}
\caption{MO logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenmo}}
\caption{MO logo Engels groen.}
\end{figure}


\section{Logo's SWE Nederlands}
De logo's van de faculteit SWE zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackswe}}
\caption{SWE logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreyswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreyswe}}
\caption{SWE logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenswe}}
\caption{SWE logo Nederlands groen.}
\end{figure}


\section{Logo's SWE Engels}
De logo's van de faculteit SWE zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackswe}}
\caption{SWE logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreymswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyswe}}
\caption{SWE logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenswe}}
\caption{SWE logo Engels groen.}
\end{figure}


\section{Logo's BRV Nederlands}
De logo's van de faculteit BRV zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackbrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackbrv}}
\caption{BRV logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreybrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreybrv}}
\caption{BRV logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenbrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenbrv}}
\caption{BRV logo Nederlands groen.}
\end{figure}


\section{Logo's PLS (BRV) Engels}
De logo's van de faculteit PLS (Ned: BRV) zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackpls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackpls}}
\caption{PLS logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreympls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreypls}}
\caption{PLS logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenpls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenpls}}
\caption{PLS logo Engels groen.}
\end{figure}


\section{Logo's GVS Nederlands}
De logo's van de faculteit GVS zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogodutchblackgvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackgvs}}
\caption{GVS logo Nederlands zwart.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreygvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreygvs}}
\caption{GVS logo Nederlands grijs.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreengvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreengvs}}
\caption{GVS logo Nederlands groen.}
\end{figure}


\section{Logo's HNS (GVS) Engels}
De logo's van de faculteit HNS (Ned: GVS) zijn afgebeeld op de helft van hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoenglishblackhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackhns}}
\caption{HNS logo Engels zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreyhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyhns}}
\caption{HNS logo Engels grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenhns}}
\caption{HNS logo Engels groen.}
\end{figure}


\section{Logo's Let's change}
Deze logo's zijn voor algemeen gebruik en zijn afgebeeld op hun natuurlijke grootte.
De omranding geeft de grenzen van de logo's aan. Zij zijn geen onderdeel
van de logo's.

\begin{verbatim}
\thuaslogoletschangeblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeblack}
\caption{Logo Let's change zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangegrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangegrey}
\caption{Logo Let's change grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangegreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangegreen}
\caption{Logo Let's change groen.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframeblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframeblack}
\caption{Logo Let's change kader zwart.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframegrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframegrey}
\caption{Logo Let's change kader grijs.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframegreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframegreen}
\caption{Logo Let's change kader groen.}
\end{figure}


\section{Logo's schalen}
De logo's kunnen op gewenste breedte of hoogste worden afgedrukt. Dat kan met
het commando \verb|\resizebox|:

\begin{verbatim}
\resizebox{4cm}{!}{\thuaslogodutchgrey}
\end{verbatim}

Het uitroepteken in het tweede argument geeft aan dat de aspect ratio behouden
blijft.

\begin{figure}[H]
\centering
\resizebox{4cm}{!}{\thuaslogodutchgrey}
\caption{Logo afgedrukt op een breedte van 4 cm.}
\end{figure}

Met het commando \verb|\scalebox| kan een logo geschaald worden ten opzichte
van de originele grootte:

\begin{verbatim}
\scalebox{0.7071}{\thuaslogodutchgrey}
\end{verbatim}

\begin{figure}[H]
\centering
\scalebox{0.5}{\thuaslogodutchgrey}
\caption{Logo geschaald op de helft van het origineel.}
\end{figure}


\section{Ti\emph{k}Z}
De logo's kunnen in een \verb|tikzpicture| environment gebruikt worden. Daarbij
kan een schaalfactor (en meer) worden opgegeven:

\begin{verbatim}
\begin{tikzpicture}[scale=0.5,opacity=0.7071]
\newbox\mybox
\node[above,yslant=0.05] at(0,0) {\global\setbox\mybox=
                        \hbox{\thuaslogoenglishgreen}\copy\mybox};
\node[above,yscale=-1,opacity=0.7071,scope fading=south,fading angle=15,
                               yslant=0.05] at(0,0) {\copy\mybox};
\draw[blue,thick,opacity=0.9] (current bounding box.east) -- 
                                        (current bounding box.west);
\end{tikzpicture}
\end{verbatim}

\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=0.5,opacity=0.7071]
\newbox\mybox
\node[above,yslant=0.05] at(0,0){\global\setbox\mybox=\hbox{\thuaslogoenglishgreen}\copy\mybox};
\node[above,yscale=-1,opacity=0.7071,scope fading=south,fading angle=15,yslant=0.05] at(0,0) {\copy\mybox};
\draw[blue,thick,opacity=0.9] (current bounding box.east) -- (current bounding box.west);
\end{tikzpicture}
\caption{Logo geschaald op 50\% en een \textsl{slant} van 5\%.}
\end{figure}


\section{Logo's met een andere kleur afdrukken}
\label{anderekleur}
Logo's met een andere kleur afdrukken kan door de kleur \verb|thuaslogos@oolor|
aan te passen en gebruik te maken van de interne logo's. De onderstaande kleuren
zijn de zogenoemde secundaire kleuren die door De Haagse Hogeschool gebruikt worden:

\begin{verbatim}
\makeatletter
\definecolor{thuaslogos@color}{RGB}{0,178,205}%
\thuaslogo@logoenglishnocolor \qquad \thuaslogo@logodutchnocolor
\definecolor{thuaslogos@color}{RGB}{202,67,60}%
\thuaslogo@logoenglishnocolortis \qquad \thuaslogo@logodutchnocolortis
\definecolor{thuaslogos@color}{RGB}{255,186,0}%
\thuaslogo@logoenglishnocolorbfm \qquad \thuaslogo@logodutchnocolorbfm
\definecolor{thuaslogos@color}{RGB}{142,152,6}%
\thuaslogo@logoenglishnocoloritd \qquad \thuaslogo@logodutchnocoloritd
\definecolor{thuaslogos@color}{RGB}{168,173,0}%
\thuaslogo@logoenglishnocolormo \qquad \thuaslogo@logodutchnocolormo
\definecolor{thuaslogos@color}{RGB}{59,69,89}%
\thuaslogo@logoenglishnocolorswe \qquad \thuaslogo@logodutchnocolorswe
\definecolor{thuaslogos@color}{RGB}{78,91,118}%
\thuaslogo@logoenglishnocolorpls \qquad \thuaslogo@logodutchnocolorbrv
\makeatother
\end{verbatim}

\begin{figure}[H]
\centering
\makeatletter
\definecolor{thuaslogos@color}{RGB}{0,178,205}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolor \qquad \thuaslogo@logodutchnocolor}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{202,67,60}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolortis \qquad \thuaslogo@logodutchnocolortis}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{255,186,0}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorbfm \qquad \thuaslogo@logodutchnocolorbfm}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{142,152,6}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocoloritd \qquad \thuaslogo@logodutchnocoloritd}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{168,173,0}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolormo \qquad \thuaslogo@logodutchnocolormo}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{59,69,89}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorswe \qquad \thuaslogo@logodutchnocolorswe}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{78,91,118}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorpls \qquad \thuaslogo@logodutchnocolorbrv}
\makeatother
\caption{Logo's afgedrukt in een andere kleur.}
\end{figure}


Het is ook mogelijk om de logo's in het wit af te drukken, bijvoorbeeld t.o.v.
een achtergrondkleur:

\begin{verbatim}
% thuasgreen defined by package if xcolor is loaded
%\definecolor{thuasgreen}{RGB}{158,167,0}
\begin{mdframed}[hidealllines=true,backgroundcolor=thuasgreen]
\thuaslogoenglishwhite \qquad thuaslogodutchwhite
\end{mdframed}
\end{verbatim}

\begin{figure}[H]
\centering
\begin{mdframed}[hidealllines=true,backgroundcolor=thuasgreen]
\centering
\resizebox{0.9\textwidth}{!}{\thuaslogoenglishwhite \qquad \thuaslogodutchwhite}
\end{mdframed}
\caption{Logo's afgedrukt in wit.}
\end{figure}


\section{Genereren van logo's, rendersnelheid}
De logo's zijn verstrekt door de dienst Communicatie en Marketing. De logo's zijn van \verb|eps|-formaat
met de tool \verb|eps2pfg|\footnote{Zie \url{https://sourceforge.net/projects/eps2pgf/}} omgezet
naar \verb|pgf| commando's. De package laadt dan ook de \verb|pgf| package.

Het is ook mogelijk om de \verb|eps|-bestanden in InkScape in te lezen en ze dan te exporteren naar
Ti\emph{k}Z-formaat. De bestandsomvang is dan kleiner maar wel moet Ti\emph{k}Z geladen worden.
Hoewel de package uit meer dan 12500 regels bestaat, is renderen met PGF vele malen sneller dan met
Ti\emph{k}Z. Alle Ti\emph{k}Z-commando's moeten immers omgezet worden in PGF-commando's en dat kost
veel tijd.


\section{Alle macro's op een rij}
\begin{verbatim}
\thuaslogodutchblack
\thuaslogodutchblackbfm
\thuaslogodutchblackbrv
\thuaslogodutchblackgvs
\thuaslogodutchblackitd
\thuaslogodutchblackmo
\thuaslogodutchblackswe
\thuaslogodutchblacktis
\thuaslogodutchgreen
\thuaslogodutchgreenbfm
\thuaslogodutchgreenbrv
\thuaslogodutchgreengvs
\thuaslogodutchgreenitd
\thuaslogodutchgreenmo
\thuaslogodutchgreenswe
\thuaslogodutchgreentis
\thuaslogodutchgrey
\thuaslogodutchgreybfm
\thuaslogodutchgreybrv
\thuaslogodutchgreygvs
\thuaslogodutchgreyitd
\thuaslogodutchgreymo
\thuaslogodutchgreyswe
\thuaslogodutchgreytis
\thuaslogodutchwhite
\thuaslogodutchwhitebfm
\thuaslogodutchwhitebrv
\thuaslogodutchwhitegvs
\thuaslogodutchwhiteitd
\thuaslogodutchwhitemo
\thuaslogodutchwhiteswe
\thuaslogodutchwhitetis
\thuaslogoenglishblack
\thuaslogoenglishblackbfm
\thuaslogoenglishblackhns
\thuaslogoenglishblackitd
\thuaslogoenglishblackmo
\thuaslogoenglishblackpls
\thuaslogoenglishblackswe
\thuaslogoenglishblacktis
\thuaslogoenglishgreen
\thuaslogoenglishgreenbfm
\thuaslogoenglishgreenhns
\thuaslogoenglishgreenitd
\thuaslogoenglishgreenmo
\thuaslogoenglishgreenpls
\thuaslogoenglishgreenswe
\thuaslogoenglishgreentis
\thuaslogoenglishgrey
\thuaslogoenglishgreybfm
\thuaslogoenglishgreyhns
\thuaslogoenglishgreyitd
\thuaslogoenglishgreymo
\thuaslogoenglishgreypls
\thuaslogoenglishgreyswe
\thuaslogoenglishgreytis
\thuaslogoenglishwhite
\thuaslogoenglishwhitebfm
\thuaslogoenglishwhitehns
\thuaslogoenglishwhiteitd
\thuaslogoenglishwhitemo
\thuaslogoenglishwhitepls
\thuaslogoenglishwhiteswe
\thuaslogoenglishwhitetis
\thuaslogoletschangeblack
\thuaslogoletschangeframeblack
\thuaslogoletschangeframegreen
\thuaslogoletschangeframegrey
\thuaslogoletschangegreen
\thuaslogoletschangegrey
\end{verbatim}


\begin{multicols}{2}[\setlength{\columnsep}{30pt}\section{Changelog}]
\begin{itemize}
\item[v1.2] [2019/06/07]\\ Engelse documentatie.
\item[v1.1] [2019/01/09]\\ Logo's van BFM, ITD, MO, SWE, BRV (PLS) en GVS (HNS). Logo's Let's change toegevoegd. Kleuren van logo's aangepast naar de huisstijl. Logo's zijn \verb|tikzpicture|-aware. Logo's in wit.
\item[v1.0] [2019/01/01]\\ Initi\"ele uitgave. Logo's van HHS/THUAS en van TIS.
\end{itemize}
\end{multicols}



\end{document}