This version (2013/02/02 v2.01a) has a housekeeping correction. The
error message

   ! Missing = inserted for \ifnum. 

was provoked by an incorrect date format for \ProvidesClass. 

Old version of nrc.dtx: 2012/11/2 v2.01
New version of nrc.dtx: 2013/02/02 v2.01a

Current version of nrc.ins: unchanged from 2002/11/19

Current version of userguide: unchanged 18 NOV 2002

Christina Thiele
For CSP, 2 February 2013

======================================================================
======================================================================


         THE NOTES BELOW ARE UNCHANGED FROM NOV 2012
               -----------------------------



THIS IS A SLIGHTLY MODIFIED VERSION of the original authors.txt file
for the NRC's macros, last updated Nov 2002. -- Ch. Thiele

-----------------

  "In September 2010, NRC Research Press (formerly part of the
   National Research Council of Canada) transitioned from NRC and the
   Federal Government of Canada into an independent not-for-profit
   organization operating under the name Canadian Science Publishing."

                     [from http://www.nrcresearchpress.com/page/about]


This file (00-2012-authors.txt) has been minimally adjusted to reflect
some administrative changes since 2002.

1. All CSP journals now use the 2-column formal of nrc2.cls. The
   .dtx still generates both nrc1 (1-column) and nrc2 (2-column)
   class files -- authors should ONLY be using nrc2.cls. 

   
   The existing README file can still serve for details on how to
   generate nrc2.cls via the nrc.ins + nrc.dtx files. The existing
   userguide is also still valid and should be downloaded in an
   appropriate format.


      AUTHORS ARE STRONGLY ADVISED TO REPLACE THEIR FORMER
        VERSION OF THE CLASS FILE WITH THIS NEW ONE. 

    THE USERGUIDE SHOULD ALSO BE DOWNLOADED AND PRINTED OUT.


The updated nrc.dtx file will generate a new nrc2.cls file which will
adjust the footer text, to reflect the organisational change:

  old: (Copyright <year> NRC Canada)

  new: (Published by NRC Research Press)


In order for author-generated files to be posted to the web, the
updated class file (nrc2.cls) MUST be generated and MUST replace the
old version.

The .dtx file has the following ID line (old and new):

  old ID line: ]% $Id: nrc.dtx,v 2.141 2002/11/19 17:45:07 rf Exp rf $

  new ID line: ]% $Id: nrc.dtx,v 2.142 2012/10/31 17:45:07 ct $


NO OTHER CHANGES HAVE BEEN MADE AT THIS TIME. 

Ch. Thiele
2 NOV 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


   The notes below focus on author-specific elements
   in the userguide, and how to avoid common pitfalls.


1. UPDATING AND GENERATING NEW CLASS FILES

   Use the README file to generate the necessary class files and
   instructions on where they should be stored on your machine. 

   Check CTAN periodically for updates:

      http://mirror.ctan.org/macros/latex/contrib/nrc/ 


2. DOCUMENTATION
  
   The Userguide has two main components: information on creating and
   coding a file (sections 1 through 5), and additional information
   for in-house staff (from section 6 to the end). 


3. CLASS OPTIONS

   There are no new options. However, there is a simplification of
   options for authors.

   The publicly available class files automatically activate
   author-specific codes, and de-activate in-house codes (the log file
   will provide information/warnings if the latter are accessed). It
   is no longer necessary to explicitly insert author-specific codes
   such as

                genTeX     usecmfonts     type1rest

   See section 2.1 for details.


4. ADDITIONAL PACKAGES

   Experience has shown that the following packages (all available
   from CTAN) are consistently used and provide the additional
   functionality authors (and the NRC Research Press) most often
   require:

      graphicx     rotating      
      amsmath      amssymb     bm
      cite         babel

   As well, tables benefit from the additional options available in
   the array package. 


   Any other packages which authors find helpful MUST accompany their
   submissions. This is particularly important where submissions are
   using old versions of packages --- production staff only have
   access to new versions on CTAN; should they have to acquire the
   additional (and latest) packages themselves, these may not always
   work in the same way as an older version. Of course, authors are
   encouraged to use current versions of all additional packages. 


   Additional author-specific macros should be gathered together in
   the preamble area, after all other packages have been loaded. See
   section 3.2 for details. 


5. MACROS

   a. New macros: 
        - \IDnote                        (Section 4.1)
        - \PACS and \PACS* (for Physics) (Section 4.2)

   b. Revised macros: 
        - \correspond                    (Section 4.1)
        - headings now to 5 levels       (Section 4.3)

   c. Retired macros: 
        - \AddressNote                   (Section 4.1)


Christina Thiele
For CSP, 2 November 2012

