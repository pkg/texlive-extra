efbox
=====

`efbox` is a LaTeX package that defines the `\efbox` command, which
creates a box just wide enough to hold the text created by its argument,
and optionally puts a frame around the outside of the box, and it allows
setting the box background color. It uses key-value pairs for that.

License
-------

Copyright 2011 - 2014 José Romildo Malaquias

This material is subject to the
[LaTeX Project Public License](http://www.ctan.org/license/lppl1.3).

Installation
------------

To install the distribution:

- run `latex efbox.ins`
- move `efbox.sty` to locations where LaTeX will find it
- run `pdflatex efbox.dtx` to generate the documentation file
