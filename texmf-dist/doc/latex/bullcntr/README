The bullcntr package defines the command "\bullcntr", which can be thought
of as an analogue of the "\fnsymbol" command: like the latter, it displays
the value of a counter lying between 1 and 9, but uses, for the purpose, a
regular pattern of bullets.  Approximately, these nine patterns of bullets
are laid down as follows:


                                *
  *            *   *
                              *   *
  1              2              3



  *            *   *           * *
*   *            *            *   *
  *            *   *           * *
  4              5              6



 * *           * * *          * * *
* * *           * *           * * *
 * *           * * *          * * *
  7              8              9


An ancillary package is included along with the bullcntr package: the
bullenum package.  This is a "wrapper" that calls bullcntr and, in
addition, defines a new environment, called "bullenum", that lets you
create enumeration lists numbered via the "\bullcntr" command.

I wrote this package as an amusing exercise during a boring afternoon in
which I had nothing better to do!

The bullcntr package may be distributed and/or modified under the
conditions of the LaTeX Project Public License, available at
    http://www.latex-project.org/lppl.txt
See the files `00readme.txt' and `manifest.txt' for further details.

For more information, read the following files:

00readme.txt        --  start by reading this file
bullcntr-man.pdf    --  general information on the bullcntr package (PDF)

Other files that make up the distribution are:

manifest.txt        --  legal stuff
bullcntr.dtx        --  main source file
bullcntr.ins        --  installation script
bullcntr-man.tex    --  LaTeX source of `bullcntr-man.pdf'
bullcntr-sam.tex    --  sample code that uses the bullcntr package
bullenum-sam.tex    --  sample code that uses the bullenum package

See the file `manifest.txt' for a more precise specification of what files
constitutes a LPPL-compliant distribution.
