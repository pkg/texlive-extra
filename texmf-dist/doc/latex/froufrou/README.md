# froufrou - visual section separators (a.k.a. anonymous sections) for LaTeX

This package provides fancy separators, which are visual cues that
indicate a change of subject or context without actually starting
a new chapter or section.

Code etc: <https://gitlab.com/lago/froufrou>

Copyright 2020-2021 Nelson Lago <lago@ime.usp.br>

This work may be distributed and/or modified under the conditions of the
LaTeX Project Public License, either version 1.3c of this license or (at
your option) any later version.
