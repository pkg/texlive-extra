Nath is a LaTeX (both 2e and 2.09) style to separate presentation and content
in mathematical typography. The style delivers a particular context-dependent
presentation on the basis of a rather coarse context-independent notation.
Although essentially backward compatible with LaTeX, Nath aims at producing
traditional math typography. Its name comes from ``NAtural maTH notation'' --
see M.M., Natural TeX notation in mathematics, in: Proc. Conf. EuroTeX 2001,
Kerkrade, 23--27 September 2001 <www.ntg.nl/eurotex/marvan-3.pdf>.

Nath is a free software distributed under the terms of the GNU General Public
License <www.gnu.org/copyleft/gpl.html>.

To install Nath, put the nath.sty file into the TeX input directory.
A LaTeX 2.09 document may start like

   \documentstyle[nath]{article}

a LaTeX 2e document, like

   \documentclass{article}
   \usepackage{nath}

Nath does not introduce any new fonts.

Nath helps to prevent wasting human work on something that can be done by
computer.
In particular, delimiters adapt their size to the material enclosed,
(rendering \left and \right almost obsolete), no matter how many \\'s
intervene.
Depending on the context, the command \frac produces either built-up or
case or solidus fractions, with parentheses added whenever required for
preservation of the mathematical meaning.

Nath is provided as it is; only bug reports and serious discussion should go
to <M.Marvan@math.slu.cz>.
On average, LaTeX runs about three times slower with Nath than
without it, depending on the complexity of math formulas.

The new release dated 11 February 2003 brings mainly several bug fixes
and introduces a new bug (sorry).
The new bug corrected 21 March 2003.








