The LaTeX metalogox package
Automatically adjusts the TeX logo and related, depending on the font.

v1.01
Copyright 2019, 2023 Brian Dunn — www.BDTechConcepts.com
LaTeX Project Public License, version 1.3

The metalogox package extends the metalogo package to automatically adjust
the appearance of the logos TEX, LATEX, LATEX2e, XELATEX, and LuaLATEX,
depending on the font detected or the option given to metalogox.

Most of the serif and sans fonts listed at The LATEX Font Catalogue
are supported.
