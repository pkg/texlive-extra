﻿%%%%%%%%% gradstudentresume %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% Written By: Anagha Kumar %%%%%%%%%%%%
%%%%%% License: The LATEX Project Public License (lppl1.3) %%%%%%
%%%%%% Version: 2015/11/12 %%%%%%%%%%%%%%%


%I tried to write a generic resume template for graduate students. Naturally, I tried to make it as flexible as %possible since I cannot possibly come up with every section and subsection imaginable. Regardless, this should %serve as a helpful template for those graduate students writing an academic CV. I have uploaded a generic CV %as a demonstration of how this document class works. 

%The purpose of this README file is to explain how this document class was constructed so that other authors %can use this as a template to design their own document classes. 

% The example.tex provides an example of how this document class can be operationalized. 


%I have named this class gradstudentresume. Please save it to the same folder that your .tex file is located in
%and then compile. 

%% The .cls file contains many comments to help guide the user. 