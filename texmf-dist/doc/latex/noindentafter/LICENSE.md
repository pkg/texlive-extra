
 noindentafter &ndash; purposefully prevention of paragraph indentation
----------------------------------------------------------------------------

 Copyright (C) Michiel Helvensteijn, 2014-2021<br>
 Copyright (C) Falk Hanisch <hanisch.latex@outlook.com>, 2021-2021

----------------------------------------------------------------------------

 This work may be distributed and/or modified under the conditions of the
 LaTeX Project Public License, version 1.3c of the license. The latest
 version of this license is in<br>
 &nbsp;&nbsp; http://www.latex-project.org/lppl.txt<br>
 and version 1.3c or later is part of all distributions of
 LaTeX version 2008-05-04 or later.
 
 This work has the LPPL maintenance status "author-maintained".
 
 The current maintainer and author of this work is Falk Hanisch.

----------------------------------------------------------------------------
