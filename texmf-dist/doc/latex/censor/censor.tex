\documentclass{article}
\usepackage{verbatimbox,xcolor,mathtools}
\usepackage{censor}

\parindent 0in
\parskip 1em

\begin{document}

\vspace*{0in}
\begin{center}
\LARGE The \textsf{censor} Package\\
\small \rule{0in}{1em}Tools for Producing Redacted Documents\\
\large \rule{0in}{2em} Steven B. Segletes\\
SSegletes@verizon.net\\
\rule{0em}{2em}\censorversiondate\\
v\censorversionnumber
\end{center}

\section{Introduction}

The \textsf{censor} package allows a convenient redaction/censor
capability to be employed, for those who need to protect restricted
information, as well as for those who are forced to work in a less
efficient environment when dealing with restricted information.

Let us assume you have a document for internal use, containing some
restricted (\textit{i.e.}, private, sensitive, proprietary or
classified) information.  To give a very short (fictitious) example:

{\addtolength{\leftskip}{2.3em}
RESTRICTED SOURCE: \hrulefill

\verb|The Liberty missile, with charge diameter (CD) of 80~mm, |\\
\verb|revealed a penetration capability of 1.30 1.19, and |\\
\verb|1.37~CD in three recent tests into armor steel.|

RESTRICTED OUTPUT: \hrulefill

The Liberty missile, with charge diameter (CD) of 80~mm, revealed a
penetration capability of 1.30 1.19, and 1.37~CD in three recent tests
into armor steel.

\hrulefill

}

Censor/redaction capabilities are desirable to sanitize the information.
This would accomplish two things:  allow for wider distribution of the
output and/or allow a less sensitive output to be stored locally with
fewer storage controls.

There are two modes in which the censor package can be used: (i) when
the source (.tex file) remains restricted/secure; and (ii) when the source
(.tex file) is public/unsecure.

\clearpage
\section{Mode I: Restricted/Secure Source (.tex file)}

In this mode, you create the source in a restricted/secure environment,
but would like to produce output that can be more widely distributed, or
stored in a less restricted location.  With the addition of
\verb|\usepackage{censor}| to your document preamble, the \verb|\censor|
command becomes accessible to block out key identifiers:

{\addtolength{\leftskip}{2.3em}
CENSORED RESTRICTED SOURCE: \hrulefill

\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability of 1.30|\\
\verb|1.19, and 1.37~CD in three recent tests into armor steel.|

CENSORED NO-LONGER-RESTRICTED OUTPUT: \hrulefill

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability of 1.30 1.19, and
1.37~CD in three recent tests into armor steel.

\hrulefill

}

The censored version of the output is (presumably) less sensitive in its
content, and my be stored in or distributed to a less sensitive
environment (\textit{e.g.}, as a hardcopy).  The censored words are not part
of the output document (hardcopy or electronic), even though the space
allocated for the redaction is identical to the original word being
redacted.

The document may also be printed out in its restricted (uncensored)
form, merely with the addition of the \verb|\StopCensoring| command at
the beginning of the document:

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill

\verb|\StopCensoring|\\
\verb| |\\
\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability of 1.30,|\\
\verb|1.19, and 1.37~CD in three recent tests into armor steel.|

UNCENSORED RESTRICTED OUTPUT: \hrulefill

\StopCensoring

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability of 1.30, 1.19, and
1.37~CD in three recent tests into armor steel.

\hrulefill

}

As of version 3.0, the ability to censor ``boxed'' objects like images
and/or tabular environments is made possible with the \verb|\censorbox|
command.  As an example, one could use the following source:

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill

\verb|The \censor{Liberty} missile, with charge diameter (CD) of|\\
\verb|\censor{80}~mm, revealed a penetration capability into armor|\\
\verb|steel, as detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. \censor{Liberty} Missile Test Data}\\|\\
\verb|\censorbox{%|\\
\verb|  \small\begin{tabular}{cc}|\\
\verb|  Standoff & Penetration \\|\\
\verb|  (CD) & (CD) \\|\\
\verb|  \hline|\\
\verb|  5.0 & 1.30 \\|\\
\verb|  6.0 & 1.19 \\|\\
\verb|  6.8 & 1.37\\|\\
\verb|  \end{tabular}%|\\
\verb|}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

\hrulefill

}

in order to turn, what would otherwise be the following restricted
output into a censored, unrestricted table.

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED OUTPUT: \hrulefill

The Liberty missile, with charge diameter (CD) of
80~mm, revealed a penetration capability into armor steel, as detailed
in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. Liberty Missile Test Data}\\
\small\begin{tabular}{cc}
Standoff & Penetration \\
(CD) & (CD) \\
\hline
 5.0 & 1.30 \\
 6.0 & 1.19 \\
 6.8 & 1.37\\
\end{tabular}
\end{center}
\end{table}

\clearpage CENSORED UNRESTRICTED OUTPUT: \hrulefill

The \censor{Liberty} missile, with charge diameter (CD) of
\censor{80}~mm, revealed a penetration capability into armor steel, as
detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. \censor{Liberty} Missile Test Data}\\
\censorbox{%
  \small\begin{tabular}{cc}
  Standoff & Penetration \\
  (CD) & (CD) \\
  \hline
  5.0 & 1.30 \\
  6.0 & 1.19 \\
  6.8 & 1.37\\
  \end{tabular}%
}
\end{center}
\end{table}
\par
\hrulefill

}


As of version 2.0, there is provided a block-censor
  capability, for redacting larger blocks of text.  
This new command is \verb|\blackout|, and is used in the form 
  \verb|\blackout{Block of text}|.  This nice thing about this command
  is that it can stretch across line, paragraph, and page boundaries 
  conveniently.
For example,

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill


\verb|\blackout{|\\
\verb|The Liberty missile, with charge diameter (CD) of 80~mm,|\\
\verb|revealed a penetration capability of 1.30, 1.19, and 1.37~CD|\\
\verb|in three recent tests into armor steel.}|\\
\verb| |\\
\verb|New paragraph.}|

CENSORED UNRESTRICTED OUTPUT: \hrulefill

\blackout{%
The Liberty missile, with charge diameter (CD) of 
80~mm, revealed a penetration capability of 1.30, 1.19, and 
1.37~CD in three recent tests into armor steel. xxx

New paragraph.
}

\hrulefill

}

Prior to V4.0, \verb|\blackout| did not conveniently work across 
  environment boundaries, such as math or tabular.  
To blackout the whole tabular environment, one could use
  the \verb|\censorbox| command as has been shown.
Presently, however, there is another option---to censor 
  cell-by-cell using a shorthand notation.

The \verb|\blackout| macro now allows uncensored content to be \textit{escaped}
  from its argument by delimiting it between \verb+|...|+ bars (note: the
  closing escape bar must be in the same grouping level).
Thus, the censoring directive {\ttfamily\detokenize
  {\blackout{This |is a| test}}}
  renders as \blackout{This |is a| test}.

This escaping of text becomes essential if one wishes to execute
  environments within a \verb|\blackout| argument.  
Unless the environment name is escaped, it will break the command,
  since the environment name itself would be replaced with censoring
  commands.
As of v4.2, tokens of catcode 4 (the \verb|&| token) are intercepted
  and automatically passed through to the output stream, which can 
  be of use in censoring \verb|tabular| expressions.
Here shows how a partial and full censor could be accomplished, with
  the use of appropriate text-escaping delimiters, instead of a
  \verb|\censorbox|:
  
\begingroup
\begin{verbatim}
\blackout{
|\begin{tabular}{|c|c|}
\hline
Title 1  & Title 2 \\
\hline
a & |bbb\\
cc| & dd\\
\hline
\end{tabular}|
|\begin{tabular}{|c|c|}|
\hline
Title 1  & Title 2 \\
\hline
 a & bbb \\
 cc & dd\\
\hline
|\end{tabular}|
}
\end{verbatim}

\blackout{
|\begin{tabular}{|c|c|}
\hline
Title 1  & Title 2 \\
\hline
a & |bbb\\
cc| & dd\\
\hline
\end{tabular}|
|\begin{tabular}{|c|c|}|
\hline
Title 1  & Title 2 \\
\hline
 a & bbb \\
 cc & dd\\
\hline
|\end{tabular}|
}

\StopCensoring

For comparison, the uncensored tabulars are:

\blackout{
|\begin{tabular}{|c|c|}
\hline
Title 1  & Title 2 \\
\hline
|a| & |bbb|\\
|cc| & dd\\
\hline
\end{tabular}|
|\begin{tabular}{|c|c|}|
\hline
Title 1  & Title 2 \\
\hline
a&bbb\\
cc&dd\\
\hline
|\end{tabular}|
}

\endgroup


\noindent**v4.0 CHANGE**:
The restriction on \verb|\blackout| \textit{not} ending on glue
  is no longer applicable.

Finally, the argument to \verb|\blackout| can, in limited situations,
  process macros; However,
  the macros are not expanded by default.
See section~\ref{S:A} for details on what is possible with respect to
  the censoring of macro content with this package.


\section{Mode II: Public/Unsecure Source (.tex file)}

This mode is envisioned for users who must bear a large level of
inconvenience to work in a restricted/secure environment (generally
meaning in a location physically removed from one's desk).  Its use is
envisioned to allow a large percentage of a document to be created in an
public/unsecure environment, with only the restricted details being
completed in a restricted/secure environment.

After including the \verb|\usepackage{censor}| command in the document
preamble, the \verb|\censor*| and \verb|\censorbox*| commands becomes
accessible.  The way this author envisions its use is as follows:

{\addtolength{\leftskip}{2.3em}
CENSORED UNRESTRICTED SOURCE: \hrulefill

\verb|% KEYWORD IDENTIFIERS:|\\
\verb|\def\Missile{\censor*{7}}|\\
\verb|\def\Size{\censor*{2}}|\\
\verb|\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}|\\
\verb||\\
\verb|The \Missile{} missile, with charge diameter (CD) of|\\
\verb|\Size~mm, revealed a penetration capability ranging from|\\
\verb|1.19--1.37~CD in three recent tests into armor steel, as|\\
\verb|detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. \Missile{} Missile Test Data}\\|\\
\verb|\TableOne{}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

CENSORED UNRESTRICTED OUTPUT: \hrulefill

\def\Missile{\censor*{7}}
\def\Size{\censor*{2}}
\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}

The {\Missile} missile, with charge diameter (CD) of {\Size}~mm, revealed a
penetration capability ranging from 1.19--1.37~CD in three recent tests
into armor steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

In this way, the source (.tex file) contains no restricted information,
and may thus be prepared in an unrestricted environment.  The argument
to the \verb|\censor*| command is a number representing the approximate
width of the redaction (in ex's).  In the case of the \verb|\censorbox*|
command, it has three mandatory arguments and one optional argument
(note that the unstarred form of the \verb|\censorbox| command has not
three, but one, mandatory arguments).  The mandatory arguments to
\verb|\censorbox*| are box width (in ex's), box height (in multiples of
\verb|\baselineskip|) and finally the depth below the baseline where the
bottom of the box should be (in multiples of \verb|\baselineskip|).  The
optional argument can include commands that you want in force for the
\verb|\censorbox| command, most typically fontsize commands, which will
affect the size of the an ex and \verb|\baselineskip|.

Because the redaction width is only approximate, it is possible that the
  censored and uncensored originals might have differing text
  justification.  
In the text, empty braces may be added after the
  keyword identifier macros so as to produce the proper interaction 
  with the subsequent whitespace and punctuation.

Because the source (.tex) file contains no restricted information, the
use of \verb|\StopCensoring| cannot (in and of itself) produce the
restricted document.  Rather it will produce the following:

{\addtolength{\leftskip}{2.3em}
UNCENSORED UNRESTRICTED OUTPUT: \hrulefill

\StopCensoring

\def\Missile{\censor*{7}}
\def\Size{\censor*{2}}
\def\TableOne{\censorbox*[\small]{26}{5}{3.5}}

The {\Missile} missile, with charge diameter (CD) of {\Size}~mm, revealed a
penetration capability ranging from 1.19--1.37~CD in three recent tests
into armor steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

In order to produce the uncensored, restricted output, the unrestricted
source file must be moved into the restricted/secure environment and be
subject to a small amount of additional editing.  Once in the restricted
environment, the source (.tex file) may be edited such that the censored
keyword identifiers are filled in with their restricted values and, in
the process, changing the \verb|\censor*| and \verb|\censorbox*|
commands to \verb|\censor| and \verb|\censorbox|, respectively.  This
and the addition of the \verb|\StopCensoring| command to the file will
produce the uncensored, restricted result:

{\addtolength{\leftskip}{2.3em}
UNCENSORED NO-LONGER-UNRESTRICTED SOURCE: \hrulefill

\verb|\StopCensoring|\\
\verb|\def\Missile{\censor{Liberty}}|\\
\verb|\def\Size{\censor{80}}|\\
\verb|\def\TableOne{\censorbox{%|\\
\verb|  \begin{tabular}{cc}|\\
\verb|  Standoff & Penetration \\|\\
\verb|  (CD) & (CD) \\|\\
\verb|  \hline|\\
\verb|  5.0 & 1.30 \\|\\
\verb|  6.0 & 1.19 \\|\\
\verb|  6.8 & 1.37\\|\\
\verb|  \end{tabular}%|\\
\verb|}}|\\
\verb||\\
\verb|The {\Missile} missile, with charge diameter (CD) of|\\
\verb|{\Size}~mm, revealed a penetration capability ranging|\\
\verb|from 1.19--1.37~CD in three recent tests into armor|\\
\verb|steel, as detailed in Table 1.|\\
\verb||\\
\verb|\begin{table}[ht]|\\
\verb|\begin{center}|\\
\verb|\textbf{Table 1. {\Missile} Missile Test Data}\\|\\
\verb|{\TableOne}|\\
\verb|\end{center}|\\
\verb|\end{table}|\\

UNCENSORED RESTRICTED OUTPUT: \hrulefill

\StopCensoring

\def\Missile{\censor{Liberty}}
\def\Size{\censor{80}}
\def\TableOne{\censorbox{%
  \begin{tabular}{cc}
  Standoff & Penetration \\
  (CD) & (CD) \\
  \hline
  5.0 & 1.30 \\
  6.0 & 1.19 \\
  6.8 & 1.37\\
  \end{tabular}%
}}

The {\Missile} missile, with charge diameter (CD) of
{\Size}~mm, revealed a penetration capability ranging
from 1.19--1.37~CD in three recent tests into armor
steel, as detailed in Table 1.

\begin{table}[ht]
\begin{center}
\textbf{Table 1. {\Missile} Missile Test Data}\\
{\TableOne}
\end{center}
\end{table}

\hrulefill

}

The only changes required of the document were at the very beginning,
among the keyword identifiers.  The primary text of the source document
remained unaltered.  Note also that the censored, unrestricted output
may also be obtained from the no-longer-unrestricted source by removing
the \verb|\StopCensoring| command at the beginning of the file.

It is not envisioned that the \verb|\blackout|
command, discussed in the prior section, is used in this mode, where
the source file is in an unsecure environment.

\section{Contiguous Blackout}

The author received some negative feedback concerning the appearance
  resulting from the \verb|\blackout| command.  Generally, people did not
  like the fact that spaces were not blacked out.  
To this end, the \verb|\xblackout| command is introduced in V3.10, which 
  \textit{cosmetically} blacks out the text more completely
  (note, however, that underneath the more complete blackout are still
  the word-length blackouts that otherwise compose \verb|\blackout|).

As of v4.2 of this package, 
  additional blacking-out provided by the \verb|\xblackout| macro 
  does not make use of any tunable parameters, but merely employs
  the \verb|\fontdimen| parameters already provided by each font.
%  is controlled
%  by one resettable length which laps additional blacking atop the
%  result of \verb|\blackout|.  
%This length is defined in the macro \verb|\spacelap|, with
%  a default value of \spacelap.

%During \verb|\xblackout|, a
%  right- and left-lapped blacking of dimension \verb|\spacelap| is
%  applied before and after each space character, respectively.

The blacking from \verb|\xblackout| is intended to be less ``choppy''
than that of \verb|\blackout|.  However, it too has some drawbacks.
Importantly, because spaces are now subject to blackout, any
  space that would otherwise be ignored by \verb|\ignorespaces| or
  \verb|\ignorespacesafterend| will not be ignored during 
  the \verb|\xblackout|.
These will include bounding spaces in \verb|tabular| cells as well
as trailing spaces after the \verb|\end| of environments.

With these caveats,
it may still be found preferable to the \verb|\blackout| macro, as 
  shown in the example below.

{\addtolength{\leftskip}{2.3em}
UNCENSORED RESTRICTED SOURCE: \hrulefill


\verb|\|\textit{x}\verb|blackout{|\\
\verb|The Liberty missile, with charge diameter (CD) of 80~mm,|\\
\verb|revealed a penetration capability of 1.30, 1.19, and 1.37~CD|\\
\verb|in recent testing here.  The tests were into armor steel.}|\\
\verb| |\\
\verb|New paragraph} with open text.|

UNCENSORED RESTRICTED OUTPUT: \hrulefill

{
The Liberty missile, with charge diameter (CD) of 
80~mm, revealed a penetration capability of 1.30, 1.19, and 
1.37~CD in recent testing here.  The tests were into armor steel.

New paragraph} with open text.

CENSORED UNRESTRICTED OUTPUT (\verb|\blackout{}|): \hrulefill

\blackout{
The Liberty missile, with charge diameter (CD) of 
80~mm, revealed a penetration capability of 1.30, 1.19, and 
1.37~CD in recent testing here.  The tests were into armor steel.

New paragraph} with open text.

CENSORED UNRESTRICTED OUTPUT (\verb|\xblackout{}|): \hrulefill

\xblackout{
The Liberty missile, with charge diameter (CD) of 
80~mm, revealed a penetration capability of 1.30, 1.19, and 
1.37~CD in recent testing here.  The tests were into armor steel.

New paragraph} with open text.

\hrulefill

}

\section{Can I Censor Control Sequences?\label{S:A}}

In general, the \textsf{censor} package is designed to operate on
  literal text.
This is especially true if you are using the \verb|\censor| macro, which
  is designed for use on a single word, rather than extended text.
However, it is also clear that an extended text passage may contain 
  various macro markup, for example, italic phrases.

The \verb|\blackout| and \verb|\xblackout| macros can at least digest
  control sequences in the input-text stream.
How the sequences are handled may require some user intervention.

Certain macro that are known to expand directly into typeset characters
  are handled automatically.
These include \verb|\$|, \verb|\&|, \verb|\#|, \verb|\%|, \verb|\_|, 
  \verb|\o|, \verb|\O|, \verb|\oe|, \verb|\OE|, \verb|\aa|, \verb|\AA|, 
  \verb|\ae|, \verb|\AE|, \verb|\l|, and \verb|\L|.
Other macros, that are part of the \LaTeX{} accents, are likewise
  handled seamlessly, including \verb|\`|,  \verb|\=|, \verb|\'|,
  \verb|\.|, \verb|\^|, \verb|\"|, \verb|\u|, \verb|\d|, \verb|\v|,
  \verb|\b|, \verb|\H|, \verb|\t|, \verb|\~|, and \verb|\c|.

For macros outside of these special cases, the default behavior is
  to echo the macro into the output stream in a literal
  (unexpanded) form.

Consider the following line of \LaTeX{} code:

\verb|\blackout{This is \textit{italic text}}|\\
\blackout{|This is \textit{italic text}|\\This is \textit{italic text}}

With censoring disabled, it would normally render as in the 2nd line.  
When enabled, it renders as the 3rd line.
The \verb|\textit| is still part of the blackout rendering, but the 
  \textit{input} to \verb|\textit| has been censored and the
  censor rule does not change its presented shape under the influence
  of \verb|\textit|.

Some macros, however, expand to literal text.
Because macros are, by default, carried literally into the output, they
  will only expand into text \textit{after} the censoring has been 
  applied.
Consider the following line of \LaTeX{} code, in which the macro
  \verb|\today| is not rendered until after censoring is complete:

\verb|\blackout{Today's date is \today.}|\\
\blackout{|Today's date is \today.|\\Today's date is \today.}

If the macro is expandable (and not all are), the \verb|\blackout| 
  and \verb|\xblackout| macros are set up to intercept callouts to
  the primitive \verb|\expanded| and act upon them prior to censoring.
Therefore, with one small change to the above example, the problem
  can be remedied:

\verb|\blackout{Today's date is \expanded{\today}.}|\\
\blackout{|Today's date is \expanded{\today}.|\\%
  Today's date is \expanded{\today}.}

If a control sequence is not expandable, then there is no way to
  censor its \textit{output}.
Therefore, you have two options: you can remove it (and its arguments)
  from the blackout altogether, or else you can leave it (and its arguments)
  in the blackout, but ``escape'' them between $|$ escape character
  delimiters.
In either case, however, the output of the unexpandable macro will be 
  visible to the reader, as in the example below, where the macro 
  \verb|\ref| and its argument are escaped from the blackout.

%\settcEscapechar{|}
\verb+\blackout{In Section |\ref{S:A}|, we discuss censored macros}+\\
\StopCensoring
\blackout{In Section |\ref{S:A}|, we discuss censored macros}\\
\RestartCensoring
\blackout{In Section |\ref{S:A}|, we discuss censored macros}

\section{Can I Censor Math?}

As of v4.2, the package makes an attempt to facilitate the
  censoring of mathematical expressions, when given as part of the
  argument to \verb|\blackout| and \verb|\xblackout|.  
Namely, tokens with catcodes 3, 7 and 8 (typically, 
  \verb|$|, \verb|^|, and \verb|_|) are intercepted and passed
  through to the output stream without censoring
(Note: the same is true of catcode 4, the \verb|&| token, which can 
  be of use in censoring \verb|tabular| as well as 
  \verb|amsmath| expressions.)

Because macros are, likewise, passed through by default without
  censoring, this means the expression 
  ``\verb|the equation $\frac{E}{m}=c^{2}$,|'' will be rendered under 
  \verb|\blackout| as \blackout{the equation $\frac{E}{m}=c^{2}$,}.
There are several points that \textbf{\textit{must}} be noted in
  a case such as this.  First, the arguments to super/sub-scripts 
  must be embraced, even if a single character in length.
Secondly, censored math scripts will not be reduced to script size, but
  will be censored using the height and depth of the pre-defined
  censor rule (this can
  affect the vertical spacing of lines).
Finally, customary math spacing associated with operators, 
  punctuation, etc., may be lost, depending on the expression. 

As of v4.2, \verb|\blackout| will, by default, automatically 
  detect and censor macros of math-mode Greek letters, such as 
  \verb|\alpha|, \verb|\beta|, etc.
If one, however, wishes to not waste compilation time searching
  for such instances, one may disable the feature with the switch
  \verb|\censormathgreekfalse|.
  

\section{Censor-Rule Marks}

The censor (blacking) mark is a \verb|\rule| which has a depth
  \(-\)0.3ex (\textit{i.e.}, below the baseline), and a total height 
  of 2.1ex, by default.  
The characteristics of that rule may be changed by setting
  the following two lengths.

\itshape
\verb|   \censorruledepth=|length\\
\verb|   \censorruleheight=|length
\upshape

When using \verb|\blackout| and \verb|\xblackout|, periods are 
  now censored by default.
If, however, you wish the prior approach of not censoring periods,
  you may redefine \verb|\censordot| to something else, for example,

\verb|   \renewcommand\censordot{.}|

to make them visible in the censored document.

\section{Usage Summary}

The complete set of commands available to the \textsf{censor} package,
to bring about text redaction, are:\\
{~\\
\verb|   \censor{|\it text\tt\}\\
\verb|   \censor*{|\it width multiplier\tt\}\\
\verb|   \censorbox[|\it pre-commands\tt]\{\it object box\tt\}\\
\verb|   \censorbox*[|\it pre-commands\tt]\{\it width mult.\tt\}\{\it 
height mult.\tt\}\{\it depth mult.\tt\}\\
\verb|   \blackout{|\it extended text passage\tt\}\\
\verb|   \xblackout{|\it extended text passage\tt\}\\
\verb|   \StopCensoring|\\
\verb|   \RestartCensoring|\\
\verb|   \censorruledepth=|\it length\\
\verb|   \censorruleheight=|length\\
%\verb|   \renewcommand\spacelap{|\it length expression\tt\}\\
\verb|   \renewcommand\censordot{|\it token or instruction\tt\}\\
\verb|   \censormathgreektrue| \rm(default) or \verb|\censormathgreekfalse|\\
~\\}
The star ({\tt*}) version of the commands is envisioned when the source
document is being created in an unsecure environment, whereas, the
unstarred version is when the document source may be created in a secure
environment.
In the star ({\tt*}) commands, width multipliers are given in ex's,
whereas height and depth multipliers are given in multiples of
\verb|\baselineskip|.  The depth indicates the distance below the
baseline where the bottom of a boxed object is to be placed (this
command has a more direct effect for inline use, whereas use within
environments is often overridden by the environment).  The use of
pre-commands will typically be fontsize commands, so that measurements
of ex's and \verb|\baselineskip| are done in a relevant fontsize.

Censoring may be dynamically turned off and on in a document with the
use of the \verb|\StopCensoring| and \verb|\RestartCensoring| commands,
respectively.  The default is censoring `on.'  

\subsection{\textit{Caveat Emptor}}

Here is some added information that users of the package should know.

\begin{itemize}

\item As of package version v4.0, the token-cycling pseudo-environments of the
  \textsf{tokcycle} package were used to process the tokens of 
  the \verb|\blackout| and \verb|\xblackout| macros.
While the syntax of these macros is unchanged, one has the additional
  option of invoking them using the pseudo-environment syntax of
  \textsf{tokcycle} as

\verb|\blackoutenv Text to blackout\endblackoutenv|:\\
\blackoutenv Text to blackout\endblackoutenv{} and\\
\verb|\xblackoutenv Text to blackout\endxblackoutenv|:\\
\xblackoutenv Text to blackout\endxblackoutenv.

\item Because \verb|\blackout| and \verb|\xblackout| use 
  tokcycle-package environments to perform their actions, the
  macro \verb|\settcEscapechar| can be used to reset the escape 
  character to something other than $|$, in accordance with 
  the documented \textsf{tokcycle}-package commands.


\item It is preferable not to apply \verb|\censor| to whitespace, or text
justification could be adversely affected.  If one wishes to censor a
multi-word phrase, such as ``Little Bo Peep,'' it is recommended to do
it as follows:\\
\verb|   \xblackout{Little Bo Peep}|\\
If such a phrase is to be used repeatedly through a document, 
it is most convenient to place it as a macro:\\
\verb|   \newcommand\CenName{\xblackout{Little Bo Peep}}|\\
such that subsequent reference is done indirectly:\\
\verb|   We examine the life of \CenName{} in this report,|\\
\verb|   assured that \CenName{} line breaks.|\\
\newcommand\CenName{\xblackout{Little Bo Peep}}
\verb|   |We examine the life of \CenName{} in this report, assured that 
  \CenName{} line breaks.

\item Censoring can still affect text justification, because 
  censored words are never hyphenated across line
  boundaries, whereas the original text may have been.
For this reason, both \verb|\blackout| and \verb|\xblackout|
  operate locally in \verb|\sloppy| mode, to minimize instances
  of overfull \verb|\hbox|es.
(Note however, that effects of \verb|\sloppy| will take hold only
  for blacked-out text that closes out a paragraph.)
If necessary, hyphenation points (\verb|\-|) or 
  \verb|\allowbreak| may be added to words subject to blackout 
  or xblackout, to facilitate manual hyphenation.

\item Users have asked for color support in the package.
I do not wish to hardwire color into the package---however,
  if the \textsf{xcolor} package is loaded,
  the following preamble definition will force censoring to be
  in the selected color (in this example, red):

\verb|   \def\censorcolor{red}|\\
\verb|   \let\svcensorrule\censorrule|\\
\verb|   \renewcommand\censorrule[1]{%|\\
\verb|      \textcolor{\censorcolor}{\svcensorrule{#1}}}|

Let's see if it works:

{\let\svcensorrule\censorrule
\renewcommand\censorrule[1]{\textcolor{red}{\svcensorrule{#1}}}
\addtolength\leftskip{2em}
Is this blacked out text censored in red?\\
\blackout{Is this blacked out text censored in red?}\par}

This technique can be used at the user's discretion to provide color
  censoring.
The value of \verb|\censorcolor| can be redefined on the fly to change
  the color of the censoring.

\end{itemize}

\clearpage
\section{Source Code}

The source code for \textsf{censor} is included below:

\verbfilenobox[\small]{censor.sty}

\end{document}
