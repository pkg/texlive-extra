normalcolor
Copyright (c) Markus Kohm, 2016
----------------------------------------------------------------------------
This work may be distributed and/or modified under the conditions of
the LaTeX Project Public License, version 1.3c of the license.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3c or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status "maintained".

The Current Maintainer and author of this work is Markus Kohm.

The work consists of the file `normalcolor.dtx` only.
----------------------------------------------------------------------------
SHORT DESCRIPTION

The package simple provides a command `\setnormalcolor` with the same
syntax as command `\color' either of package `color' or package `xcolor'.
But `\setnormalcolor' will not change the current colour but the normal
colour.  So using `\normalcolor' (implicit or explicit) afterwards will
change the current colour to the colour given by the previous
`\setnormalcolor'.
----------------------------------------------------------------------------
PACKAGE GENERATION

Simple way:

To use the simple way you need the original `makefile' from the source
distribution of `normalcolor' and `GNU make'.  Then you can use:

    make all

to generate all files of `normalcolor'.

Manual way:

If you cannot use make you can extract all the files using:

    tex normalcolor.dtx

Note that you have to use `tex' not any LaTeX format!

This will generate files `normalcolor.sty' and `README.txt'. Rename the
generated `README.txt' to `README'.

To make the documentation use:

    pdflatex normalcolor.dtx
    pdflatex normalcolor.dtx
    mkindex normalcolor
    pdflatex normalcolor.dtx
    pdflatex normalcolor.dtx

This will result in the manual file `normalcolor.pdf'.
----------------------------------------------------------------------------
INSTALLATION

Simple way:

To use the simple way you need the original `makefile' from the source
distribution of `normalcolor' and `GNU make' and a TeX-Live-compatible
`kpsewhich'.  Then you can use:

    make install

to install normalcolor into your personal TEXMF tree.  If you want to use
the local TEXMF tree instead of the personal one, you may use:

    make INSTALLLOCAL=true install

If you want to use another TEXMF tree, you may use

    make INSTALLROOT=<path to the TEXMF tree> install

Manual way:

Copy `normalcolor.sty' to `tex/latex/normalcolor/' inside the TDS tree you
want to install `normalcolor'.

Copy `normalcolor.pdf' and `README.txt' as `README' to
`doc/latex/normalcolor/' inside the TDS tree you want to install
`normalcolor'.

Maybe you have to call `texhash' for the TDS tree you have installed
`normalcolor'.
----------------------------------------------------------------------------
USAGE

See `normalcolor.pdf'.
----------------------------------------------------------------------------
