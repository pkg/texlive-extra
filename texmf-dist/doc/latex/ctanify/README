              +---------------------------------------+
              |                CTANIFY                |
              |                                       |
              |  Prepare a package for upload to CTAN |
              |                                       |
              | By Scott Pakin, scott+ctify@pakin.org |
              +---------------------------------------+


Description
===========

ctanify is intended for developers of LaTeX packages who want to
distribute their packages via the Comprehensive TeX Archive Network
(CTAN).  Given a list of filenames, ctanify creates a tarball (a
.tar.gz file) with the files laid out in CTAN's preferred structure.
The tarball additionally contains a ZIP (.zip) file with copies of all
files laid out in the standard TeX Directory Structure (TDS), which
facilitates inclusion of the package in the TeX Live distribution of
TeX.

ctanify has been tested primarily on Linux but with reports from users
that it also works on OS X and can work on Windows under Cygwin.
Volunteers willing to help port ctanify to other platforms are
especially welcome.


File manifest
=============

README                          This file
ctanify                         The ctanify Perl script itself
ctanify.1                       ctanify documentation (Unix man-page format)
ctanify.pdf                     ctanify documentation (PDF format)


Copyright and License
=====================

Copyright (C) 2017 Scott Pakin

This work may be distributed and/or modified under the conditions of
the LaTeX Project Public License, either version 1.3c of this license
or (at your option) any later version.  The latest version of this
license is in

                http://www.latex-project.org/lppl.txt

and version 1.3c or later is part of all distributions of LaTeX version
2008/05/04 or later.
