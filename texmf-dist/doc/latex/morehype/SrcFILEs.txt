
 *File List*
-RELEASE.---   --  -- --   --     ----
morehype.RLS  2015/11/09  r0.83  texlinks v0.83 CTANology
-----USE.---   --  -- --   --     ----
    blog.sty  2013/01/21  v0.81a simple fast HTML (UL)
 blogdot.sty  2013/01/22  v0.41b HTML presentations (UL)
 blogdot.cfg  2011/10/21   --    local blogdot.sty config
blogexec.sty  2012/12/20  v0.21  assignments with blog.sty (UL)
blogligs.sty  2012/11/29  v0.2   pervasive blog ligatures (UL)
hypertoc.sty  2011/01/23  v0.1   pretty TOC links (UL)
lnavicol.sty  2011/10/13   --    left navigation column with blog.sty
markblog.sty  2012/11/29  v0.2   wiki markup with blog.sty (UL)
texlinks.sty  2015/07/20  v0.83  TeX-related links (UL)
atari_ht.fdf  2012/10/05   --    Atari umlauts for blog.sty
 texblog.fdf  2013/01/19   --    extra blog settings
-----DOC.SRC   --  -- --   --     ----
    blog.tex  2015/05/11   --    documenting blog.sty
blogexec.tex  2014/04/09   --    documenting blogexec.sty
hellowor.tex  2012/11/30   --    hello world source
hypertoc.tex  2011/01/27   --    documenting hypertoc.sty
markblog.tex  2012/11/29   --    extended blog markup
texlinks.tex  2015/05/24   --    documenting texlinks.sty
wiki_mwe.tex  2012/12/20   --    texlinks wiki MWE
-----DOC.FMT   --  -- --   --     ----
  README.tex  2012/03/18   --    make README.pdf
fdatechk.tex  2012/12/20   --    morehype filedate checks
mkhellow.tex  2012/11/30   --    blog demo
srcfiles.tex  2012/12/20   --    file infos -> SrcFILEs.txt
NICETEXT.---   --  -- --   --     ----
fifinddo.sty  2012/11/17  v0.61  filtering TeX(t) files by TeX (UL)
 makedoc.sty  2012/08/28  v0.52  TeX input from *.sty (UL)
niceverb.sty  2012/11/27  v0.51  minimize doc markup (UL)
 makedoc.cfg  2014/08/01   --    documentation settings
mdoccorr.cfg  2012/11/13   --    `makedoc' local typographical corrections
 ***********

 List made at 2015/11/09, 10:50
 from script file srcfiles.tex

