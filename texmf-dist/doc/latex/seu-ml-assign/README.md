# SEU-ML-Assign LaTeX Template
LaTeX Template for Southeast University Machine Learning Assignment

> - GitHub Project Site: https://tvj.one/ml-tex
> - CTAN: https://ctan.org/pkg/seu-ml-assign
> - Website: https://seu-ml-assign.github.io

## Class and Options
This project provides the `seu-ml-assign` class.

| Option | Explanation | Default |
| - | - | :-: |
| `solution` | Write solutions (for students). | • |
| `problem` | Write problem sets (for instructors). ||
| `oneside` | One-sided document. | • |
| `twoside` | Two-sided document. ||
| `9pt` | Set font size as 9 points. ||
| `10pt` | Set font size as 10 points. | • |
| `11pt` | Set font size as 11 points. ||
| `12pt` | Set font size as 12 points. ||

## Commands and Usage
Please refer to the [documentation](http://mirrors.ctan.org/macros/latex/contrib/seu-ml-assign/seu-ml-assign-doc.pdf).

The main higher level commands provided in this template are `\problem[<points>]{title}` and `\subproblem{title}`.

## Publication
This project has been contained in [**CTAN**](https://ctan.org/pkg/seu-ml-assign), and so is in **TeX Live**.

## Issues
Please go to [issues](https://github.com/Teddy-van-Jerry/SEU-ML-Assign_LaTeX_Template/issues) to report them if any.

## License
This project is licensed under the [MIT License](https://github.com/Teddy-van-Jerry/SEU-ML-Assign_LaTeX_Template/blob/master/LICENSE).
