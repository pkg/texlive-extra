numerica-tables: a package to create tables of function values.

Andrew Parsloe (ajparsloe@gmail.com)

This work may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3c of this 
license or any later version; see
http://www.latex-project.org/lppl.txt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This is version 2.0.0 of numerica-tables (but the first stand-alone
version). The packages numerica, booktabs, l3kernel, l3packages,
amsmath and mathtools are required. numerica-tables allows the 
creation of possibly multi-column tables of mathematical function
values in a wide variety of formats. See numerica-tables.pdf for 
details on how to use the package.

Manifest
%%%%%%%%
README.txt             this document

numerica-tables.sty    LaTeX .sty file
numerica-tables.pdf    documentation for numerica-tables.sty
numerica-tables.tex    documentation source file

        
