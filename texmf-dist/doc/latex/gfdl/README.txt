-------------------------------------------------------------
Package:      gfdl
Version:      v0.1 (January 1, 2023)
Author:       निरंजन
Description:  For using GFDL in LaTeX.
Repository:   https://puszcza.gnu.org.ua/projects/gfdl-tex
License:      GPLv3+, GFDLv1.3+
-------------------------------------------------------------
