----------------------------------------------------------------
    The bubblesort package
    ====================

    This package sorts a list of TeX items {item 1}...{item k}
    in "increasing" order where "increasing" is determined by a
    comparator macro. By default it sorts real numbers with the
    usual meaning of "increasing" but some other examples are 
    discussed in the documentation. A second macro is included 
    which sorts one list and applies the same permutation to a 
    second list. 


E-mail: taylor.2@nd.edu
Released under the LaTeX Project Public License v1.3c or later
See http://www.latex-project.org/lppl.txt
----------------------------------------------------------------

