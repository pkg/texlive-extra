`abraces` 2022/11/06 v2.1
----------------------------------------

A package that provides a character key-driven interface to 
supplement new constructions of the traditional `\overbrace` 
and `\underbrace` pairs in an asymmetric or arbitrary way.

Copyright (c) 2021 Werner Grundlingh

This work may be distributed and/or modified under the 
conditions of the LaTeX Project Public License, either version 1.3 
of this license or (at your option) any later version. 
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.