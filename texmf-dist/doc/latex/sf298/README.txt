sf298 package
-------------

Authors
-------
Steven Douglas Cochran
SVC HS Information Technology,  M200 Scaife Hall
3550 Terrace Street
Pittsburgh, PA 15261, USA
email: sdc18@pitt.edu
(Original Author, versions 1.1 & 1.2)

and

Peter Andrew Rochford
Acorn Science & Innovation, Inc.
1616 Anderson Road, Suite 213
McLean, VA 22102, USA
email: prochford@acornsi.com
(Author, version 1.3)

Purpose
-------
The sf298 package provides for creating a filled-in copy of the standard form 
298 (Rev. 8/98), ``Report Documentation Page''. This form is used in announcing 
and cataloging reports submitted as deliverables on contracts with the U.S. 
Government.  It is important that the information on the sf298 page be 
consistent with the rest of the report, particularly the cover and title page. 
Instructions for filling in each block of the form are given in the documentation
file (sf298.pdf) distributed with the package. The macro \MakeRptDocPage causes 
the page to be printed. If the "twoside" option is specified in the 
documentclass, then it is printed as a separate page with a blank back. The 
macro \GeneralInstructions causes the page of general instructions that 
accompanies the form to be printed. This is typically inserted after the 
sf298 form.

License
-------
Copyright (C) 2000,2004 Steven Douglas Cochran.

The copyright remains with the above author.

The sf298 package may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.2 
of this license or (at your option) any later version. The latest
version of this license is in:
    http://www.latex-project.org/lppl.txt
and version 1.2 or later is part of all distributions of LaTeX 
version 1999/12/01 or later.

The sf298 package is distributed in the hope that it will be
useful, but `as is', WITHOUT WARRANTY OF ANY KIND, either expressed 
or implied, including, but not limited to, the implied warranties of
MERCHANTABILITY and FITNESS FOR A PARTICULAR PURPOSE.  See the LaTeX
Project Public License for more details.
