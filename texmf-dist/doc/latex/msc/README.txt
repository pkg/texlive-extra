MSC Macro Package
-----------------
Version 2.00
May 13, 2022
----------------------------------------------------------------------
The MSC macro package is a LaTeX2e package to draw MSC diagrams. See
user manual (manual.pdf) for more information.
----------------------------------------------------------------------
Contact address:
Reynaldo Gil Pons
Université du Luxembourg 
Maison du Nombre
6 Av. de la Fonte
L-4364 Esch-sur-Alzette
Email: reynaldo.gilpons@uni.lu
Website: http://satoss.uni.lu/mscpackage/
