%% MSC Macro Package
%% msc.sty
%%
%% Copyright 2022 V. Bos, T. van Deursen, and S. Mauw
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Reynaldo Gil Pons.
%
% This program consists of the files 
%   msc.sty
%   manual.tex
%   manual_macros.tex
%   biblio.bib
%   README.txt
%   COPYRIGHT.txt
