%%
%% This is file `mathfont_symbol_list.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% mathfont_code.dtx  (with options: `chars')
%% 
%% This file is from version 2.2a of the free and open-source
%% LaTeX package "mathfont," released December 2022, to be used
%% with the XeTeX or LuaTeX engines. (As of version 2.0, LuaTeX
%% is recommended.)
%% 
%% Copyright 2018-2022 by Conrad Kosowsky
%% 
%% This Work may be used, distributed, and modified under the
%% terms of the LaTeX Public Project License, version 1.3c or
%% any later version. The most recent version of this license
%% is available online at
%% 
%%           https://www.latex-project.org/lppl/.
%% 
%% This Work has the LPPL status "maintained," and the current
%% maintainer is the package author, Conrad Kosowsky. He can
%% be reached at kosowsky.latex@gmail.com.
%% 
%% PLEASE KNOW THAT THIS FREE SOFTWARE IS PROVIDED WITHOUT
%% ANY WARRANTY. SPECIFICALLY, THE "NO WARRANTY" SECTION OF
%% THE LATEX PROJECT PUBLIC LICENSE STATES THE FOLLOWING:
%% 
%% THERE IS NO WARRANTY FOR THE WORK. EXCEPT WHEN OTHERWISE
%% STATED IN WRITING, THE COPYRIGHT HOLDER PROVIDES THE WORK
%% `AS IS’, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
%% OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
%% PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE
%% OF THE WORK IS WITH YOU. SHOULD THE WORK PROVE DEFECTIVE,
%% YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR, OR
%% CORRECTION.
%% 
%% IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED
%% TO IN WRITING WILL THE COPYRIGHT HOLDER, OR ANY AUTHOR
%% NAMED IN THE COMPONENTS OF THE WORK, OR ANY OTHER PARTY
%% WHO MAY DISTRIBUTE AND/OR MODIFY THE WORK AS PERMITTED
%% ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL,
%% SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT
%% OF ANY USE OF THE WORK OR OUT OF INABILITY TO USE THE WORK
%% (INCLUDING, BUT NOT LIMITED TO, LOSS OF DATA, DATA BEING
%% RENDERED INACCURATE, OR LOSSES SUSTAINED BY ANYONE AS A
%% RESULT OF ANY FAILURE OF THE WORK TO OPERATE WITH ANY
%% OTHER PROGRAMS), EVEN IF THE COPYRIGHT HOLDER OR SAID
%% AUTHOR OR SAID OTHER PARTY HAS BEEN ADVISED OF THE
%% POSSIBILITY OF SUCH DAMAGES.
%% 
%% For more information, see the LaTeX Project Public License.
%% Derivative works based on this package may come with their
%% own license or terms of use, and the package author is not
%% responsible for any third-party software.
%% 
%% For more information, see mathfont_code.dtx. Happy TeXing!
%% 
\documentclass[12pt,twoside]{article}
\makeatletter
\usepackage[margin=72.27pt]{geometry}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{mathfont}
\mathfont[agreeklower=upright,agreekupper]{Crimson}
\mathfont[hebrew]{Coelacanth}
\mathfont[\M@keys]{STIXGeneral}
\usepackage{shortvrb,doc}
\MakeShortVerb{|}
\raggedcolumns
\parskip=0pt
\smallskipamount=2pt plus 1pt minus 1pt
\multicolsep=0pt
\premulticols=0pt
\fboxrule=1pt
\CharmLine{8747 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\CharmLine{8748 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\CharmLine{8749 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\CharmLine{8750 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\CharmLine{8751 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\CharmLine{8752 1200 1700 * * * * * * * * * * * * * *
  * * * * * * * * * * * * * * * * * * * * }
\begin{document}

\def\documentname{Symbol List}
\def\showabstract{1}
\input mathfont_heading.tex

The \textsf{mathfont} package provides tools to access several hundred characters for math typesetting, and this document lists these symbols along with the control sequences to access them. To get access to the symbols from a section of this document, call |\mathfont| with the keyword-option for that section and the name of a font that contains those symbols. The package does not define any math symbols until you call |\mathfont| or |\setfont| (or load \textsf{mathfont} with a font name as package option), and if you see a symbol or control sequence here that is not part of standard \LaTeX, you will not be able to access it until you call |\mathfont| on the corresponding keyword. Further, \textsf{mathfont} does not come with or load any fonts by itself, so you are responsible for the fonts. Not all fonts contain all math symbols, so choose your font wisely!\footnote{Besides letters and digits, most unicode text fonts should contain diacritics, delimiters, and the basic math characters in the keyword |symbols|. Text fonts will often contain square root and basic operator symbols, but they may not be suitable for math typesetting. Greek characters are hit or miss, and it is unusual for English text fonts to contain Cyrillic, Hebrew, ancient Greek, arrows, letterlike characters, or any extended (keywords |extbigops| and |extsymbols|) set of symbols. After you load \textsf{mathfont}, \TeX\ will print a message to the terminal if you try to typeset a missing character from some font.} This document shows ancient Greek in Crimson, Hebrew in Coelacanth, and all other math characters in STIXGeneral.

\begin{figure}[t]
\centerline{\bfseries Table 1: Characters Defined by Multiple Keywords\strut}
\begin{tabularx}\textwidth{XXX}\toprule
Character & Keywords & Default Font\\\midrule
\vrb\increment & \texttt{greekupper} & \texttt{symbols}\\
 & \texttt{symbols} & \\\midrule
\vrb\nabla & \texttt{greekupper} & \texttt{(ext)symbols}\\
 & \texttt{symbols} with Lua\TeX & \\
 & \texttt{extsymbols} with \XeTeX & \\\midrule
\expandafter\vrb\string| & \texttt{symbols} & \texttt{symbols} normally\\
 & \texttt{delimiters} with Lua\TeX & \texttt{delimiters} after |\left|, \hfil\penalty0|\right|, etc.\\\midrule
\vrb\simeq\ and \vrb\cong & \texttt{symbols} & \texttt{extsymbols} \\
 & \texttt{extsymbols} & \\\bottomrule
\end{tabularx}
\end{figure}

As of version 2.0, \textsf{mathfont} artificially adds resizable delimiters and big operator characters to text fonts when you complile with Lua\LaTeX. In this case, square root symbols will automatically resize, big operators will appear larger in |\displaystyle|, and you can use |\left|, |\right|, and |\big|, etc.\ with characters from the keyword |delimiters|. If you use \XeTeX, \textsf{mathfont} will not create large variants of characters, and your unicode math symbols will all be the same size as they appear in the font. In this case, you may be best off sticking with the Computer Modern defaults for resizable characters provided they don't clash with the rest of your document. Throughout this document, anything labeled ``Lua\TeX\ only'' means that \textsf{mathfont} provides this functionality only if you enable Lua-based font adjustments. If you load \textsf{mathfont} with the |no-adjust| option, you will not be able to access these features the same as if you compile with \XeTeX.

A few characters appear multiple times in this list. When that happens, it means that \textsf{mathfont} defines the control sequence for multiple keywords. If you call |\mathfont| for only one of those keywords, your symbol will appear in the font associated with that keyword. If you call |\mathfont| on multiple keywords, the package uses the font associated with the default keyword/font for that character. Table~1 lists the default keyword for each command that appears multiple times in this document. If you need unicode encoding slot numbers for character metric adjustments, each symbol corresponds to its standard unicode encoding value, with the exception of the fake angle brackets. When you typeset with Lua\TeX, \textsf{mathfont} artificially adds |\fakelangle|, |\fakerangle|, |\fakellangle|, and |\fakerrangle| to the font in encoding slots 1,044,508--1,044,511 respectively.

\bigskip\centerline{\vrule height 0.5pt width 2.5in}\medskip\smallskip

\catcode`\|=12

\blockheader{lower}{Lower-Case Latin}

\begin{multicols}{3}
\charexample a
\charexample b
\charexample c
\charexample d
\charexample e
\charexample f
\charexample g
\charexample h
\charexample i
\charexample j
\charexample k
\charexample l
\charexample m
\charexample n
\charexample o
\charexample p
\charexample q
\charexample r
\charexample s
\charexample t
\charexample u
\charexample v
\charexample w
\charexample x
\charexample y
\charexample z
\charexample\hbar
\charexample\imath
\charexample\jmath
\end{multicols}

\blockheader{upper}{Upper-Case Latin}

\begin{multicols}{3}
\charexample A
\charexample B
\charexample C
\charexample D
\charexample E
\charexample F
\charexample G
\charexample H
\charexample I
\charexample J
\charexample K
\charexample L
\charexample M
\charexample N
\charexample O
\charexample P
\charexample Q
\charexample R
\charexample S
\charexample T
\charexample U
\charexample V
\charexample W
\charexample X
\charexample Y
\charexample Z
\end{multicols}

\blockheader{diacritics}{Accent}

\begin{multicols}{3}
\accentexample\acute
\accentexample\aacute
\accentexample\dot
\accentexample\ddot
\accentexample\grave
\accentexample\breve
\accentexample\hat
\accentexample\check
\accentexample\bar
\accentexample\mathring
\accentexample\tilde
\end{multicols}

\blockheader{digits}{Arabic Numeral}

\begin{multicols}{3}
\charexample0
\charexample1
\charexample2
\charexample3
\charexample4
\charexample5
\charexample6
\charexample7
\charexample8
\charexample9
\end{multicols}

\blockheader{greekupper}{Upper-Case Greek}

\begin{multicols}{3}
\charexample\Alpha
\charexample\Beta
\charexample\Gamma
\charexample\Delta
\charexample\Epsilon
\charexample\Zeta
\charexample\Eta
\charexample\Theta
\charexample\Iota
\charexample\Kappa
\charexample\Lambda
\charexample\Mu
\charexample\Nu
\charexample\Xi
\charexample\Omicron
\charexample\Pi
\charexample\Rho
\charexample\Sigma
\charexample\Tau
\charexample\Upsilon
\charexample\Phi
\charexample\Chi
\charexample\Psi
\charexample\Omega
\charexample\varTheta
\charexample\increment
\charexample\nabla
\end{multicols}

\blockheader{greeklower}{Lower-Case Greek}

\begin{multicols}{3}
\charexample\alpha
\charexample\beta
\charexample\gamma
\charexample\delta
\charexample\epsilon
\charexample\zeta
\charexample\eta
\charexample\theta
\charexample\iota
\charexample\kappa
\charexample\lambda
\charexample\mu
\charexample\nu
\charexample\xi
\charexample\omicron
\charexample\pi
\charexample\rho
\charexample\sigma
\charexample\tau
\charexample\upsilon
\charexample\phi
\charexample\chi
\charexample\psi
\charexample\omega
\charexample\varbeta
\charexample\varepsilon
\charexample\varkappa
\charexample\vartheta
\charexample\varrho
\charexample\varsigma
\charexample\varphi
\end{multicols}

\blockheader{agreekupper}{Upper-Case Ancient Greek}

\begin{multicols}{3}
\charexample\Heta
\charexample\Sampi
\charexample\Digamma
\charexample\Koppa
\charexample\Stigma
\charexample\Sho
\charexample\San
\charexample\varSampi
\charexample\varDigamma
\charexample\varKoppa
\end{multicols}

\blockheader{agreeklower}{Lower-Case Ancient Greek}

\begin{multicols}{3}
\charexample\heta
\charexample\sampi
\charexample\digamma
\charexample\koppa
\charexample\stigma
\charexample\sho
\charexample\san
\charexample\varsampi
\charexample\vardigamma
\charexample\varkoppa
\end{multicols}

\blockheader{cyrillicupper}{Upper-Case Cyrillic}

\begin{multicols}{3}
\charexample\cyrA
\charexample\cyrBe
\charexample\cyrVe
\charexample\cyrGhe
\charexample\cyrDe
\charexample\cyrIe
\charexample\cyrZhe
\charexample\cyrZe
\charexample\cyrI
\charexample\cyrKa
\charexample\cyrEl
\charexample\cyrEm
\charexample\cyrEn
\charexample\cyrO
\charexample\cyrPe
\charexample\cyrEr
\charexample\cyrEs
\charexample\cyrTe
\charexample\cyrU
\charexample\cyrEf
\charexample\cyrHa
\charexample\cyrTse
\charexample\cyrChe
\charexample\cyrSha
\charexample\cyrShcha
\charexample\cyrHard
\charexample\cyrYeru
\charexample\cyrSoft
\charexample\cyrE
\charexample\cyrYu
\charexample\cyrYa
\charexample\cyrvarI
\end{multicols}

\blockheader{cyrilliclower}{Lower-Case Cyrillic}

\begin{multicols}{3}
\charexample\cyra
\charexample\cyrbe
\charexample\cyrve
\charexample\cyrghe
\charexample\cyrde
\charexample\cyrie
\charexample\cyrzhe
\charexample\cyrze
\charexample\cyri
\charexample\cyrka
\charexample\cyrel
\charexample\cyrem
\charexample\cyren
\charexample\cyro
\charexample\cyrpe
\charexample\cyrer
\charexample\cyres
\charexample\cyrte
\charexample\cyru
\charexample\cyref
\charexample\cyrha
\charexample\cyrtse
\charexample\cyrche
\charexample\cyrsha
\charexample\cyrshcha
\charexample\cyrhard
\charexample\cyryeru
\charexample\cyrsoft
\charexample\cyre
\charexample\cyryu
\charexample\cyrya
\charexample\cyrvari
\end{multicols}

\blockheader{hebrew}{Hebrew}

\begin{multicols}{3}
\charexample\aleph
\charexample\beth
\charexample\gimel
\charexample\daleth
\charexample\he
\charexample\vav
\charexample\zayin
\charexample\het
\charexample\tet
\charexample\yod
\charexample\kaf
\charexample\lamed
\charexample\mem
\charexample\nun
\charexample\samekh
\charexample\ayin
\charexample\pe
\charexample\tsadi
\charexample\qof
\charexample\resh
\charexample\shin
\charexample\tav
\charexample\varkaf
\charexample\varmem
\charexample\varnun
\charexample\varpe
\charexample\vartsadi
\end{multicols}

\medskip
\def\temp{; shown in \texttt{\vrb\big}, etc.\ sizes}
\blockheader{delimiters\aftergroup\temp}{Delimiter}
\medskip
\begin{multicols}{3}
\delimexample(
\delimexample)
\delimexample[
\delimexample]
\delimexample{\{\ifmmode\else\space(Lua\TeX\ only)\fi}
\delimexample{\}\ifmmode\else\space(Lua\TeX\ only)\fi}
\delimexample{|\,\ifmmode\else\space(Lua\TeX\ only)\fi}
\delimexample\lguil
\delimexample\rguil
\delimexample\llguil
\delimexample\rrguil
\luadelimexample\fakelangle
\luadelimexample\fakerangle
\luadelimexample\fakellangle
\luadelimexample\fakerrangle
\hbox to \hsize{\hbox to 0.8in{$\leftbrace$\hfil}\vrb\leftbrace\hfil}
\hbox to \hsize{\hbox to 0.8in{$\rightbrace$\hfil}\vrb\rightbrace\hfil}
\end{multicols}
\medskip

\blockheader{radical}{Square Root}
\smallskip
\begin{multicols}{2}
\charexample\surd
\def\temp{ (Lua\TeX\ only)\hfill}
\charexample{\sqrt{\ifmmode\mkern 12mu\vphantom{1}\else\temp\fi}}
\end{multicols}

\blockheader{bigops}{Big Operator}
\smallskip
\begin{multicols}{2}
\operatorexample\sum
\operatorexample\prod
\operatorexample\intop
\end{multicols}
\medskip

\blockheader{extbigops}{Extended Big Operators}
\smallskip
\begin{multicols}{3}
\operatorexample\coprod
\operatorexample\bigvee
\operatorexample\bigwedge
\operatorexample\bigcap
\operatorexample\bigcup
\operatorexample\bigoplus
\operatorexample\bigotimes
\operatorexample\bigodot
\operatorexample\bigsqcap
\operatorexample\bigsqcup
\operatorexample\iint
\operatorexample\iiint
\operatorexample\oint
\operatorexample\oiint
\operatorexample\oiiint
\end{multicols}

\medskip

\blockheader{symbols}{Basic Math}
\begin{multicols}{3}
\charexample.
\charexample @
\let\par\relax
\charexample\#
\charexample\$
\charexample\%
\charexample\&
\charexample\P
\charexample\S
\charexample\pounds
\charexample|
\charexample\neg
\charexample\infty
\charexample\partial
\charexample\mathbackslash
\charexample\degree
\charexample\increment
\def\temp{ (Lua\TeX only)}
\charexample{\nabla\ifmmode\else\temp\fi}
\charexample'
\charexample\prime
\charexample"
\charexample+
\charexample-
\charexample*
\charexample\times
\charexample/
\charexample\fractionslash
\charexample\div
\charexample\pm
\charexample\bullet
\charexample\dag
\charexample\ddag
\charexample\cdot
\charexample\setminus
\charexample=
\charexample<
\charexample>
\charexample\leq
\charexample\geq
\charexample\sim
\charexample\approx
\charexample\simeq
\charexample\equiv
\charexample\cong
\charexample\mid
\charexample\parallel
\charexample!
\charexample?
\charexample{,\ifmmode\else\phantom{\texttt{comma}} (as \rlap{\vrb\mathpunct)}\fi}
\charexample{\comma\ifmmode\else\space(as \vrb\mathord)\fi}
\charexample{:\ifmmode\else\phantom{\texttt{colon}} (as \vrb\mathrel)\fi}
\charexample{\colon\ifmmode\else\space(as \vrb\mathord)\fi}
\charexample;
\charexample\mathellipsis
\end{multicols}
\blockheader{symbols}{Lua\TeX-only (!) Operator}
\begin{multicols}{3}
\operatorexample\bigat
\operatorexample\bighash
\operatorexample\bigdollar
\operatorexample\bigpercent
\operatorexample\bigand
\operatorexample\bigplus
\operatorexample\bigp
\operatorexample\bigq
\operatorexample\bigS
\operatorexample\bigtimes
\operatorexample\bigdiv
\end{multicols}

\blockheader{extsymbols}{Extended Math}
\begin{multicols}{3}
\charexample\wp
\charexample\Re
\charexample\Im
\charexample\ell
\charexample\forall
\charexample\exists
\charexample\emptyset
\charexample{\nabla\ifmmode\else\space(\XeTeX)\fi}
\charexample\in
\charexample\ni
\charexample\mp
\charexample\angle
\charexample\top
\charexample\bot
\charexample\vdash
\charexample\dashv
\charexample\flat
\charexample\natural
\charexample\sharp
\charexample\bclubsuit
\charexample\clubsuit
\charexample\bdiamondsuit
\charexample\bheartsuit
\charexample\bspadesuit
\charexample\spadesuit
\charexample\wclubsuit
\charexample\wdiamondsuit
\charexample\diamondsuit
\charexample\wheartsuit
\charexample\heartsuit
\charexample\wspadesuit
\charexample\wedge
\charexample\vee
\charexample\cap
\charexample\cup
\charexample\sqcap
\charexample\sqcup
\charexample\amalg
\charexample\wr
\charexample\ast
\charexample\star
\charexample\diamond
\charexample\varcdot
\charexample\varsetminus
\charexample\oplus
\charexample\otimes
\charexample\ominus
\charexample\odiv
\charexample\oslash
\charexample\odot
\charexample\sqplus
\charexample\sqtimes
\charexample\sqminus
\charexample\sqdot
\charexample\in
\charexample\ni
\charexample\subset
\charexample\supset
\charexample\subseteq
\charexample\supseteq
\charexample\sqsubset
\charexample\sqsupset
\charexample\sqsubseteq
\charexample\sqsupseteq
\charexample\triangleleft
\charexample\triangleright
\charexample\trianglelefteq
\charexample\trianglerighteq
\charexample\propto
\charexample\bowtie
\charexample\hourglass
\charexample\therefore
\charexample\because
\charexample\ratio
\charexample\proportion
\charexample\ll
\charexample\gg
\charexample\lll
\charexample\ggg
\charexample\leqq
\charexample\geqq
\charexample\lapprox
\charexample\gapprox
\charexample\simeq
\charexample\eqsim
\charexample\simeqq
\charexample\cong
\charexample\approxeq
\charexample\sssim
\charexample\seq
\charexample\doteq
\charexample\coloneq
\charexample\eqcolon
\charexample\ringeq
\charexample\arceq
\charexample\wedgeeq
\charexample\veeeq
\charexample\stareq
\charexample\triangleeq
\charexample\defeq
\charexample\qeq
\charexample\lsim
\charexample\gsim
\charexample\prec
\charexample\succ
\charexample\preceq
\charexample\succeq
\charexample\preceqq
\charexample\succeqq
\charexample\precsim
\charexample\succsim
\charexample\precapprox
\charexample\succapprox
\charexample\precprec
\charexample\succsucc
\charexample\asymp
\charexample\nin
\charexample\nni
\charexample\nsubset
\charexample\nsupset
\charexample\nsubseteq
\charexample\nsupseteq
\charexample\subsetneq
\charexample\supsetneq
\charexample\nsqsubseteq
\charexample\nsqsupseteq
\charexample\sqsubsetneq
\charexample\sqsupsetneq
\charexample\neq
\charexample\nl
\charexample\ng
\charexample\nleq
\charexample\ngeq
\charexample\lneq
\charexample\gneq
\charexample\lneqq
\charexample\gneqq
\charexample\ntriangleleft
\charexample\ntriangleright
\charexample\ntrianglelefteq
\charexample\ntrianglerighteq
\charexample\nsim
\charexample\napprox
\charexample\nsimeq
\charexample\nsimeqq
\charexample\simneqq
\charexample\nlsim
\charexample\ngsim
\charexample\lnsim
\charexample\gnsim
\charexample\lnapprox
\charexample\gnapprox
\charexample\nprec
\charexample\nsucc
\charexample\npreceq
\charexample\nsucceq
\charexample\precneq
\charexample\succneq
\charexample\precneqq
\charexample\succneqq
\charexample\precnsim
\charexample\succnsim
\charexample\precnapprox
\charexample\succnapprox
\charexample\nequiv
\end{multicols}

\blockheader{arrows}{Arrow}
\begin{multicols}{2}
\charexample\rightarrow
\charexample\to
\charexample\nrightarrow
\charexample\Rightarrow
\charexample\nRightarrow
\charexample\Rrightarrow
\charexample\longrightarrow
\charexample\Longrightarrow
\charexample\rightbararrow
\charexample\mapsto
\charexample\Rightbararrow
\charexample\longrightbararrow
\charexample\longmapsto
\charexample\Longrightbararrow
\charexample\hookrightarrow
\charexample\rightdasharrow
\charexample\rightharpoonup
\charexample\rightharpoondown
\charexample\rightarrowtail
\charexample\rightoplusarrow
\charexample\rightwavearrow
\charexample\rightsquigarrow
\charexample\longrightsquigarrow
\charexample\looparrowright
\charexample\curvearrowright
\charexample\circlearrowright
\charexample\twoheadrightarrow
\charexample\rightarrowtobar
\charexample\rightwhitearrow
\charexample\rightrightarrows
\charexample\rightrightrightarrows
\charexample\leftarrow
\charexample\from
\charexample\nleftarrow
\charexample\Leftarrow
\charexample\nLeftarrow
\charexample\Lleftarrow
\charexample\longleftarrow
\charexample\Longleftarrow
\charexample\leftbararrow
\charexample\mapsfrom
\charexample\Leftbararrow
\charexample\longleftbararrow
\charexample\longmapsfrom
\charexample\Longleftbararrow
\charexample\hookleftarrow
\charexample\leftdasharrow
\charexample\leftharpoonup
\charexample\leftharpoondown
\charexample\leftarrowtail
\charexample\leftoplusarrow
\charexample\leftwavearrow
\charexample\leftsquigarrow
\charexample\longleftsquigarrow
\charexample\looparrowleft
\charexample\curvearrowleft
\charexample\circlearrowleft
\charexample\twoheadleftarrow
\charexample\leftarrowtobar
\charexample\leftwhitearrow
\charexample\leftleftarrows
\charexample\leftleftleftarrows
\charexample\leftrightarrow
\charexample\Leftrightarrow
\charexample\nLeftrightarrow
\charexample\longleftrightarrow
\charexample\Longleftrightarrow
\charexample\leftrightwavearrow
\charexample\leftrightarrows
\charexample\leftrightharpoons
\charexample\leftrightarrowstobar
\charexample\rightleftarrows
\charexample\rightleftharpoons
\charexample\uparrow
\charexample\Uparrow
\charexample\Uuparrow
\charexample\upbararrow
\charexample\updasharrow
\charexample\upharpoonleft
\charexample\upharpoonright
\charexample\twoheaduparrow
\charexample\uparrowtobar
\charexample\upwhitearrow
\charexample\upwhitebararrow
\charexample\upuparrows
\charexample\downarrow
\charexample\Downarrow
\charexample\Ddownarrow
\charexample\downbararrow
\charexample\downdasharrow
\charexample\zigzagarrow
\charexample\lightningboltarrow
\charexample\downharpoonleft
\charexample\downharpoonright
\charexample\twoheaddownarrow
\charexample\downarrowtobar
\charexample\downwhitearrow
\charexample\downdownarrows
\charexample\updownarrow
\charexample\Updownarrow
\charexample\updownarrows
\charexample\downuparrows
\charexample\updownharpoons
\charexample\downupharpoons
\charexample\nearrow
\charexample\Nearrow
\charexample\nwarrow
\charexample\Nwarrow
\charexample\searrow
\charexample\Searrow
\charexample\swarrow
\charexample\Swarrow
\charexample\nwsearrow
\charexample\neswarrow
\charexample\lcirclearrow
\charexample\rcirclearrow
\end{multicols}

\blockheader{bb}{Blackboard Bold}

\letterlikechars\mathbb
\hbox to 10em{\printchars\digits}

\blockheader{cal}{Caligraphic}

\letterlikechars\mathcal

\blockheader{frak}{Fraktur}

\letterlikechars\mathfrak

\blockheader{bcal}{Bold Calligraphic}

\letterlikechars\mathbcal

\blockheader{bfrak}{Bold Fraktur}

\letterlikechars\mathbfrak

\end{document}
\endinput
%%
%% End of file `mathfont_symbol_list.tex'.
