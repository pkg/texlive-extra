# hyperbar [![CTAN](https://img.shields.io/badge/CTAN-hyperbar-blue.svg?style=flat-square)](https://ctan.org/pkg/hyperbar)

> This package allows to add interactive Barcode fields to PDF forms

To install, you can run `tex hyperbar.dtx` and copy the generated file `hyperbar.sty` to a directory in the search path of your TeX installation.
For quick evaluation, you can also rename `hyperbar.dtx` to `hyperbar.sty` and use that file directly.

This package is released under the LaTeX Project Public License v1.3c or any later version, see http://www.latex-project.org/lppl.txt for the full text of the license.
