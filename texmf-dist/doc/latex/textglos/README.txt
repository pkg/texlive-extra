%%
%% This is file `README.txt',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% textglos.dtx  (with options: `readme')
%% This is a generated file.
%% 
%% ----------------------------------------------------------------
%% textglos --- A package to typeset inline linguistic examples.
%% E-mail: natalie.a.weber@gmail.com
%% Released under the LaTeX Project Public License v1.3c or later
%% See http://www.latex-project.org/lppl.txt
%% ----------------------------------------------------------------
%% 
%% 
%% Copyright 2013 by Natalie Weber
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status `maintained'.
%% 
%% The Current Maintainer of this work is Natalie Weber.
%% 
%% This work consists of the files textglos.dtx and textglos.ins
%% and the derived files textglos.sty and textglos.pdf.
%% 

The \textglos{} package provides a set of macros for in-line linguistic examples (as opposed to interlinear glossing, set apart from the main text). It prevents hyphenated examples from breaking across lines and consistently formats phonemic examples, orthographic examples, and more.

\endinput
%%
%% End of file `README.txt'.
