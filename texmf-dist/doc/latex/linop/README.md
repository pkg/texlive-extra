%% linop.sty
%% Copyright 2016 Johannes Weytjens
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Johannes Weytjens
%
% This work consists of the file linop.sty

Linop
-----
Version: 0.1
Last revision: June 6, 2016
Created by: Johannes Weytjens
Email: johannes.weytjens@nietsissimpel.org

Linop is a small package that aims to provide two simple commands and many options to easily write linear operators, as they appear in many-body physics, quantum theory and linear algebra, in any of the ways they commonly appear.
