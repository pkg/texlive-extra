
LaTeX labelschanged package v1.0   README.txt

Locates labels which cause endless “may have changed” warnings.


Files included are:

labelschanged.pdf: The documentation.
labelschanged.dtx: The documented source code.
labelschanged.ins: The documentation driver.

Derived by compiling labelschanged.ins:
labelschanged.sty: The labelschanged package.

License:
This work is based on a public Internet post by David Carlisle,
is packaged with permission, and is in the public domain.

Maintainer: Brian Dunn
Homepage: http://BDTechConcepts.com
Email: bd@BDTechConcepts.com
