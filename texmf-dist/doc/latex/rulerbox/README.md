rulerbox
=======

`rulerbox` is a LaTeX package for drawing rulers around a box. This might be useful when showing the absolute size of something in electronic documents, or designating the relative scale in printed materials.

Contributing
------------

This package is developed and maintained by Yuchang Yang < yang.yc.allium@gmail.com >.

Discussions and questions are welcome.

Copyright and Licence
---------------------

    Copyright (C) 2019 by Yuchang Yang < yang.yc.allium@gmail.com >
    ----------------------------------------------------------------------

    This work may be distributed and/or modified under the
    conditions of the LaTeX Project Public License, either
    version 1.3c of this license or (at your option) any later
    version. This version of this license is in
       http://www.latex-project.org/lppl/lppl-1-3c.txt
    and the latest version of this license is in
       http://www.latex-project.org/lppl.txt
    and version 1.3 or later is part of all distributions of
    LaTeX version 2005/12/01 or later.

    This work has the LPPL maintenance status `maintained'.

    The Current Maintainer of this work is Yuchang Yang.

    This work consists of the files rulerbox.sty,
                                    rulerbox.tex,
                                    rulerbox.pdf, and
                                    README.md.
