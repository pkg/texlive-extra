This is the distribution directory for quiz2socrative, a LaTeX package for
preparing multiple choice, true/false and short answer questions.
The main purpouse is to offer a tool to easily insert rather complicated 
mathematical stuff in socrative quizzes (see https://socrative.com).

This is quiz2socrative version 1.0, October 2019. 
quiz2socrative is distributed under the LaTeX Project Public License 1.3c.


*** Source files:

quiz2socrative.sty	The style file
quiz2socrative-IT.tex	The documentation file
sample-quiz2socrative-socrativeQuiz+standalone.tex  An example of a set of questions ready to prepare a socrative quiz.
sample-quiz2socrative-pdfQuiz.tex   An example of a traditional pen and paper quiz.


*** Pdf files:

quiz2socrative-IT.pdf The documentation (italian only)
sample-quiz2socrative-socrativeQuiz+standalone.pdf  An example of a set of questions ready to prepare a socrative quiz.
sample-quiz2socrative-pdfQuiz.pdf   An example of a traditional pen and paper quiz.


Paolo Lella (paolo.lella@polimi.it)