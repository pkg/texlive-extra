Vocal Tract Latex package
---------------------------
by Dr. Dimitrios Ververidis

The manuscript related to this package can be found on PRACTex journal 2012/1
URL: http://dw.tug.org/pracjourn/2012-1/ververidis/ververidis.pdf

Please cite as: 

 "Dimitrios Ververidis, Daniel Schneider, and Joachim Koehler, "The vocal tract LATEX package," PracTeX journal, no 1, 2012."

Package tested for Miktex 2.8 and 2.9 and Matlab 7.5 under Windows 
 
 
 
The software is licensed as FREE under APACHE 2.0 licence:
http://www.apache.org/licenses/LICENSE-2.0.html

 
 
Using the package: 
--------------------

Examples

1st Example - Simple Figure from Latex compiling
-------------------------------------------------

         Create a figure in PS or PDF by compiling with latex the file:
 
                vtLatex_FigureDemo.tex


2nd Example - Animation from Latex compiling
---------------------------------------------

        Create an animation in PDF by compiling with latex the file:

               vtLatex_AnimationDemo.tex


3rd Example - Simple Figure or Animation compiling from Matlab
--------------------------------------------------------------

        Create a simple Figure or Animation by compiling from Matlab the function
		
		       vtMainVisual.m   
        
		as 
		>> vtMainVisual(VocalTractVecToTime, SWPDForBMP)
		
		where 
		
		1. "VocalTractVecToTime" is the CELL of vocal set up through time. See the codebook of german phonemes in the VocalTract.sty.
		
		For Example
		
			VocalTractVecToTime = ...                           
			{[ 0.5, -2,    1 ,     -2 ,    1,    -1  ,  0,   0,    0,    0],   'i'      ;   
			 [ 0.5, -2,    1 ,     -2 ,    0,     2  ,  0,   0,    0,    0],   'y(\"u)' ; 
			 [-1  , -1,    1 ,     -2 ,    1,    -0.5,  0,   0,    0,    0],   'e'      };
 
        2. "SWPDForBMP" is the string either 'bmp' or 'pdf' to define the output format.
		
			In case of 'bmp' the irfan view programm should be in current path.
			From IrfanView two files are needed to be in current directory
			1. "i_view32.exe" and 
			2. "/plugins/Postscript.dll"
			Free download IrfanView from Irfan Skiljan, E-Mail: irfanview@gmx.net, http://www.irfanview.com
						
			
4th Example - Animation compiling from Matlab with phonemes as an input
------------------------------------------------------------------------        
		
		Create a PDF by compiling from Matlab
		e.g. 
	   
        >> vtQueryVisual('zats') 
        
		
		

      
