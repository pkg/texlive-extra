# beilstein

    beilstein -- Support for submissions to the ``Beilstein Journal
    of Nanotechnology'' published by the Beilstein-Institut
    zur Foerderung der Chemischen Wissenschaften
    Version:     2.1
    E-mail:      journals-support@beilstein-institut.de
    License:     Released under the LaTeX Project Public License v1.3c or later
    See          http://www.latex-project.org/lppl.txt

The Beilstein bundle provides a LaTeX class file and a BibTeX
style file in accordance with the requirements of submissions to
the [Beilstein Journal of Nanotechnology]. Although the
files can be used for any kind of document, they have only been
designed and tested to be suitable for submission to the [Beilstein Journal of Nanotechnology].


Stable versions are uploaded to CTAN (https://www.ctan.org/pkg/beilstein). From there they usually
make their way into TeX Live and MiKTeX rather quickly.
In addition you will find the most recent version at https://www.beilstein-journals.org/bjnano/templates.
The most recent documentation is available at BJNANO_Technical_Handbook.pdf within the doc directory.
`texdoc beilstein` should give you this file. It includes a short description how to use the template and
also provides trouble shooting hints.

Please see [CHANGELOG.md](CHANGELOG.md) for a version history

  [Beilstein Journal of Nanotechnology]: https://www.beilstein-journals.org/bjnano/
