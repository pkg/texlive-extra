README.txt for the Canadian Journal of Economics “cje” package
2018/02/22 v1.06

INFORMATION
===========

“cje” is the LaTeX class for the Canadian Journal of Economics (economics.ca/cje/en/), published by Wiley (onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-5982).

“cje” is based on the standard LaTeX "article" class.

COPYRIGHT
=========

(c) 2017 Canadian Economics Association

LICENSE
=======

This work can be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or any later version. The latest version of this license is available at latex-project.org/lppl.txt. Version 1.3, or later, is part of all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status ``maintained.’’

This work comprises the following 10 files:

README.txt (this file)     
cjeguide.tex (Instructions for authors, written in CJE style)
cjeguide.pdf (Instructions for authors, written in CJE style)
cjetemplate.tex (Template for CJE style, containing only code)      
cje.cls (CJE class file)
cjebibstyle.bst (CJE bibliography style file)          
cjenatbib.sty (CJE style file for citations) 
cjeupmath.sty (CJE style file for non-italic Greek characters)    
ageingbib.bib (bib file called in by cjeGuide2018.tex)
canadian-flag.pdf (figure file called in by cjeGuide.tex)


=========

The following standard files are required and available at ctan.org if you don’t already have them:

amsbsy.sty (style file called in by cjeupmath.sty)        
amsfonts.sty (style file called in by amssymb.sty)
amsgen.sty (style file called in by cjeupmath.sty)         
amssymb.sty (accesses AMS fonts msam and msbm)
amsthdoc.pdf (documentation for amsthm.sty)      
amsthm.sty (for typesetting theorems, proofs, etc.)             
ednmath0.sty (style file called in by lineno.sty)      
edtable.sty (style file called in by lineno.sty)              
graphicx.sty (graphics style file)     
hyperref.sty (to generate hypertext links)   
lineno.sty (style file required for [review] and [proof] options)  
lmodern.sty (Default font family for CJE) 
tabularx.sty (style file to manipulate columns in tables)
upquote.sty (to generate upright quote marks in verbatim)            
url.sty (for formatting hypertext links, etc.)
vplref.sty (style file called in by lineno.sty)

If you’re missing any other standard files you need, you can download them from ctan.org.


CURRENT MAINTAINER
==================

Feedback, suggestions, questions or pleas for help can be emailed to: 

Kim Nesbitt
cje-copyeditor@outlook.com

Ottawa, Ontario, Canada
Tel: +1 613-290-0834





