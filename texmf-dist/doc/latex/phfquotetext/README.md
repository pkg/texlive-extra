# The phfquotetext package

Quote verbatim text without white space formatting.

Provides an environment for displaying block text with special characters, such
as verbatim quotes from a referee report which may contain pseudo-(La)TeX code.
This behaves like a `verbatim` environment, except that it displays its content
as normal paragraph content, ignoring any white space preformatting.


# Documentation

Run 'make sty' to generate the style file and 'make pdf' to generate the package
documentation. Run 'make' or 'make help' for more info.

