econometrics ~~~~~~~~~~~~~~~~~~~

A package that defines some commands that simplify mathematic
notation in economic and econometrics writing. The commands are
related to the notation of
- vectors, matrices, sets, calligraphic and roman letters;
- statistical distributions;
- constants and symbols;
- matrix operators and statistical operators.

The package is based on "Notation in Econometrics: a proposal
for a standard" by Karim Abadir and Jan R. Magnus,
The Econometrics Journal (2002), 5, 76-90.

____________________________________________
Erik Kole, http://people.few.eur.nl/kole 
Copyright (c) 2016 by Erik Kole.
All Rights Reserved.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status "maintained".

The Current Maintainer of this work is Erik Kole.

This work consists of the files econometrics.sty, econometrics.tex and
econometrics.pdf

This version: 1.0, January 15, 2016
