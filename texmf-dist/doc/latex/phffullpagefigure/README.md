# The phffullpagefigure package

Typeset figures which fill up a full page of a document.

Provide a figure environment which provides the figure content on its own page,
with the corresponding caption reading for example "Figure 3 (on next page):
<caption>".


# Documentation

Run 'make sty' to generate the style file and 'make pdf' to generate the package
documentation. Run 'make' or 'make help' for more info.

