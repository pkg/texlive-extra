%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 3 -1 12 851 
%%HiResBoundingBox: 3.56172 -0.2 11.4813 850.59502 
%%Creator: MetaPost 1.504
%%CreationDate: 2011.07.24:0826
%%Pages: 1
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0.502 0.502 setrgbcolor
newpath 7.51953 0 moveto
11.2813 3.54297 lineto
7.51953 7.08594 lineto
3.76172 3.54297 lineto
7.51953 0 lineto
 closepath fill
 0 0 0 setrgbcolor 0 0.4 dtransform truncate idtransform setlinewidth pop [] 0 setdash 0 setlinecap
 1 setlinejoin 10 setmiterlimit
newpath 7.51953 0 moveto
11.2813 3.54297 lineto
7.51953 7.08594 lineto
3.76172 3.54297 lineto
7.51953 0 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 7.08594 moveto
11.2813 10.6289 lineto
7.51953 14.1719 lineto
3.76172 10.6289 lineto
7.51953 7.08594 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 7.08594 moveto
11.2813 10.6289 lineto
7.51953 14.1719 lineto
3.76172 10.6289 lineto
7.51953 7.08594 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 14.1719 moveto
11.2813 17.7148 lineto
7.51953 21.2617 lineto
3.76172 17.7148 lineto
7.51953 14.1719 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 14.1719 moveto
11.2813 17.7148 lineto
7.51953 21.2617 lineto
3.76172 17.7148 lineto
7.51953 14.1719 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 21.2617 moveto
11.2813 24.8047 lineto
7.51953 28.3477 lineto
3.76172 24.8047 lineto
7.51953 21.2617 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 21.2617 moveto
11.2813 24.8047 lineto
7.51953 28.3477 lineto
3.76172 24.8047 lineto
7.51953 21.2617 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 28.3477 moveto
11.2813 31.8906 lineto
7.51953 35.4336 lineto
3.76172 31.8906 lineto
7.51953 28.3477 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 28.3477 moveto
11.2813 31.8906 lineto
7.51953 35.4336 lineto
3.76172 31.8906 lineto
7.51953 28.3477 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 35.4336 moveto
11.2813 38.97661 lineto
7.51953 42.5195 lineto
3.76172 38.97661 lineto
7.51953 35.4336 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 35.4336 moveto
11.2813 38.97661 lineto
7.51953 42.5195 lineto
3.76172 38.97661 lineto
7.51953 35.4336 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 42.5195 moveto
11.2813 46.0625 lineto
7.51953 49.6055 lineto
3.76172 46.0625 lineto
7.51953 42.5195 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 42.5195 moveto
11.2813 46.0625 lineto
7.51953 49.6055 lineto
3.76172 46.0625 lineto
7.51953 42.5195 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 49.6055 moveto
11.2813 53.14839 lineto
7.51953 56.6914 lineto
3.76172 53.14839 lineto
7.51953 49.6055 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 49.6055 moveto
11.2813 53.14839 lineto
7.51953 56.6914 lineto
3.76172 53.14839 lineto
7.51953 49.6055 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 56.6914 moveto
11.2813 60.2344 lineto
7.51953 63.7813 lineto
3.76172 60.2344 lineto
7.51953 56.6914 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 56.6914 moveto
11.2813 60.2344 lineto
7.51953 63.7813 lineto
3.76172 60.2344 lineto
7.51953 56.6914 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 63.7813 moveto
11.2813 67.3242 lineto
7.51953 70.8672 lineto
3.76172 67.3242 lineto
7.51953 63.7813 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 63.7813 moveto
11.2813 67.3242 lineto
7.51953 70.8672 lineto
3.76172 67.3242 lineto
7.51953 63.7813 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 70.8672 moveto
11.2813 74.4102 lineto
7.51953 77.9531 lineto
3.76172 74.4102 lineto
7.51953 70.8672 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 70.8672 moveto
11.2813 74.4102 lineto
7.51953 77.9531 lineto
3.76172 74.4102 lineto
7.51953 70.8672 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 77.9531 moveto
11.2813 81.4961 lineto
7.51953 85.03911 lineto
3.76172 81.4961 lineto
7.51953 77.9531 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 77.9531 moveto
11.2813 81.4961 lineto
7.51953 85.03911 lineto
3.76172 81.4961 lineto
7.51953 77.9531 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 85.03911 moveto
11.2813 88.582 lineto
7.51953 92.125 lineto
3.76172 88.582 lineto
7.51953 85.03911 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 85.03911 moveto
11.2813 88.582 lineto
7.51953 92.125 lineto
3.76172 88.582 lineto
7.51953 85.03911 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 92.125 moveto
11.2813 95.668 lineto
7.51953 99.21089 lineto
3.76172 95.668 lineto
7.51953 92.125 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 92.125 moveto
11.2813 95.668 lineto
7.51953 99.21089 lineto
3.76172 95.668 lineto
7.51953 92.125 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 99.21089 moveto
11.2813 102.758 lineto
7.51953 106.30101 lineto
3.76172 102.758 lineto
7.51953 99.21089 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 99.21089 moveto
11.2813 102.758 lineto
7.51953 106.30101 lineto
3.76172 102.758 lineto
7.51953 99.21089 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 106.30101 moveto
11.2813 109.84401 lineto
7.51953 113.387 lineto
3.76172 109.84401 lineto
7.51953 106.30101 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 106.30101 moveto
11.2813 109.84401 lineto
7.51953 113.387 lineto
3.76172 109.84401 lineto
7.51953 106.30101 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 113.387 moveto
11.2813 116.93 lineto
7.51953 120.473 lineto
3.76172 116.93 lineto
7.51953 113.387 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 113.387 moveto
11.2813 116.93 lineto
7.51953 120.473 lineto
3.76172 116.93 lineto
7.51953 113.387 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 120.473 moveto
11.2813 124.016 lineto
7.51953 127.55899 lineto
3.76172 124.016 lineto
7.51953 120.473 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 120.473 moveto
11.2813 124.016 lineto
7.51953 127.55899 lineto
3.76172 124.016 lineto
7.51953 120.473 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 127.55899 moveto
11.2813 131.102 lineto
7.51953 134.645 lineto
3.76172 131.102 lineto
7.51953 127.55899 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 127.55899 moveto
11.2813 131.102 lineto
7.51953 134.645 lineto
3.76172 131.102 lineto
7.51953 127.55899 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 134.645 moveto
11.2813 138.188 lineto
7.51953 141.73 lineto
3.76172 138.188 lineto
7.51953 134.645 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 134.645 moveto
11.2813 138.188 lineto
7.51953 141.73 lineto
3.76172 138.188 lineto
7.51953 134.645 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 141.73 moveto
11.2813 145.277 lineto
7.51953 148.82 lineto
3.76172 145.277 lineto
7.51953 141.73 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 141.73 moveto
11.2813 145.277 lineto
7.51953 148.82 lineto
3.76172 145.277 lineto
7.51953 141.73 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 148.82 moveto
11.2813 152.363 lineto
7.51953 155.906 lineto
3.76172 152.363 lineto
7.51953 148.82 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 148.82 moveto
11.2813 152.363 lineto
7.51953 155.906 lineto
3.76172 152.363 lineto
7.51953 148.82 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 155.906 moveto
11.2813 159.449 lineto
7.51953 162.992 lineto
3.76172 159.449 lineto
7.51953 155.906 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 155.906 moveto
11.2813 159.449 lineto
7.51953 162.992 lineto
3.76172 159.449 lineto
7.51953 155.906 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 162.992 moveto
11.2813 166.535 lineto
7.51953 170.078 lineto
3.76172 166.535 lineto
7.51953 162.992 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 162.992 moveto
11.2813 166.535 lineto
7.51953 170.078 lineto
3.76172 166.535 lineto
7.51953 162.992 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 170.078 moveto
11.2813 173.621 lineto
7.51953 177.164 lineto
3.76172 173.621 lineto
7.51953 170.078 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 170.078 moveto
11.2813 173.621 lineto
7.51953 177.164 lineto
3.76172 173.621 lineto
7.51953 170.078 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 177.164 moveto
11.2813 180.707 lineto
7.51953 184.254 lineto
3.76172 180.707 lineto
7.51953 177.164 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 177.164 moveto
11.2813 180.707 lineto
7.51953 184.254 lineto
3.76172 180.707 lineto
7.51953 177.164 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 184.254 moveto
11.2813 187.797 lineto
7.51953 191.34 lineto
3.76172 187.797 lineto
7.51953 184.254 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 184.254 moveto
11.2813 187.797 lineto
7.51953 191.34 lineto
3.76172 187.797 lineto
7.51953 184.254 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 191.34 moveto
11.2813 194.883 lineto
7.51953 198.426 lineto
3.76172 194.883 lineto
7.51953 191.34 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 191.34 moveto
11.2813 194.883 lineto
7.51953 198.426 lineto
3.76172 194.883 lineto
7.51953 191.34 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 198.426 moveto
11.2813 201.969 lineto
7.51953 205.512 lineto
3.76172 201.969 lineto
7.51953 198.426 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 198.426 moveto
11.2813 201.969 lineto
7.51953 205.512 lineto
3.76172 201.969 lineto
7.51953 198.426 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 205.512 moveto
11.2813 209.055 lineto
7.51953 212.598 lineto
3.76172 209.055 lineto
7.51953 205.512 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 205.512 moveto
11.2813 209.055 lineto
7.51953 212.598 lineto
3.76172 209.055 lineto
7.51953 205.512 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 212.598 moveto
11.2813 216.141 lineto
7.51953 219.684 lineto
3.76172 216.141 lineto
7.51953 212.598 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 212.598 moveto
11.2813 216.141 lineto
7.51953 219.684 lineto
3.76172 216.141 lineto
7.51953 212.598 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 219.684 moveto
11.2813 223.227 lineto
7.51953 226.773 lineto
3.76172 223.227 lineto
7.51953 219.684 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 219.684 moveto
11.2813 223.227 lineto
7.51953 226.773 lineto
3.76172 223.227 lineto
7.51953 219.684 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 226.773 moveto
11.2813 230.316 lineto
7.51953 233.859 lineto
3.76172 230.316 lineto
7.51953 226.773 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 226.773 moveto
11.2813 230.316 lineto
7.51953 233.859 lineto
3.76172 230.316 lineto
7.51953 226.773 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 233.859 moveto
11.2813 237.402 lineto
7.51953 240.945 lineto
3.76172 237.402 lineto
7.51953 233.859 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 233.859 moveto
11.2813 237.402 lineto
7.51953 240.945 lineto
3.76172 237.402 lineto
7.51953 233.859 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 240.945 moveto
11.2813 244.488 lineto
7.51953 248.031 lineto
3.76172 244.488 lineto
7.51953 240.945 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 240.945 moveto
11.2813 244.488 lineto
7.51953 248.031 lineto
3.76172 244.488 lineto
7.51953 240.945 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 248.031 moveto
11.2813 251.574 lineto
7.51953 255.117 lineto
3.76172 251.574 lineto
7.51953 248.031 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 248.031 moveto
11.2813 251.574 lineto
7.51953 255.117 lineto
3.76172 251.574 lineto
7.51953 248.031 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 255.117 moveto
11.2813 258.66 lineto
7.51953 262.203 lineto
3.76172 258.66 lineto
7.51953 255.117 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 255.117 moveto
11.2813 258.66 lineto
7.51953 262.203 lineto
3.76172 258.66 lineto
7.51953 255.117 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 262.203 moveto
11.2813 265.75 lineto
7.51953 269.293 lineto
3.76172 265.75 lineto
7.51953 262.203 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 262.203 moveto
11.2813 265.75 lineto
7.51953 269.293 lineto
3.76172 265.75 lineto
7.51953 262.203 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 269.293 moveto
11.2813 272.836 lineto
7.51953 276.379 lineto
3.76172 272.836 lineto
7.51953 269.293 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 269.293 moveto
11.2813 272.836 lineto
7.51953 276.379 lineto
3.76172 272.836 lineto
7.51953 269.293 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 276.379 moveto
11.2813 279.922 lineto
7.51953 283.465 lineto
3.76172 279.922 lineto
7.51953 276.379 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 276.379 moveto
11.2813 279.922 lineto
7.51953 283.465 lineto
3.76172 279.922 lineto
7.51953 276.379 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 283.465 moveto
11.2813 287.008 lineto
7.51953 290.551 lineto
3.76172 287.008 lineto
7.51953 283.465 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 283.465 moveto
11.2813 287.008 lineto
7.51953 290.551 lineto
3.76172 287.008 lineto
7.51953 283.465 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 290.551 moveto
11.2813 294.094 lineto
7.51953 297.637 lineto
3.76172 294.094 lineto
7.51953 290.551 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 290.551 moveto
11.2813 294.094 lineto
7.51953 297.637 lineto
3.76172 294.094 lineto
7.51953 290.551 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 297.637 moveto
11.2813 301.18 lineto
7.51953 304.72299 lineto
3.76172 301.18 lineto
7.51953 297.637 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 297.637 moveto
11.2813 301.18 lineto
7.51953 304.72299 lineto
3.76172 301.18 lineto
7.51953 297.637 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 304.72299 moveto
11.2813 308.26999 lineto
7.51953 311.81299 lineto
3.76172 308.26999 lineto
7.51953 304.72299 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 304.72299 moveto
11.2813 308.26999 lineto
7.51953 311.81299 lineto
3.76172 308.26999 lineto
7.51953 304.72299 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 311.81299 moveto
11.2813 315.35501 lineto
7.51953 318.89801 lineto
3.76172 315.35501 lineto
7.51953 311.81299 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 311.81299 moveto
11.2813 315.35501 lineto
7.51953 318.89801 lineto
3.76172 315.35501 lineto
7.51953 311.81299 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 318.89801 moveto
11.2813 322.44101 lineto
7.51953 325.98401 lineto
3.76172 322.44101 lineto
7.51953 318.89801 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 318.89801 moveto
11.2813 322.44101 lineto
7.51953 325.98401 lineto
3.76172 322.44101 lineto
7.51953 318.89801 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 325.98401 moveto
11.2813 329.52701 lineto
7.51953 333.07 lineto
3.76172 329.52701 lineto
7.51953 325.98401 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 325.98401 moveto
11.2813 329.52701 lineto
7.51953 333.07 lineto
3.76172 329.52701 lineto
7.51953 325.98401 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 333.07 moveto
11.2813 336.613 lineto
7.51953 340.156 lineto
3.76172 336.613 lineto
7.51953 333.07 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 333.07 moveto
11.2813 336.613 lineto
7.51953 340.156 lineto
3.76172 336.613 lineto
7.51953 333.07 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 340.156 moveto
11.2813 343.699 lineto
7.51953 347.242 lineto
3.76172 343.699 lineto
7.51953 340.156 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 340.156 moveto
11.2813 343.699 lineto
7.51953 347.242 lineto
3.76172 343.699 lineto
7.51953 340.156 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 347.242 moveto
11.2813 350.789 lineto
7.51953 354.332 lineto
3.76172 350.789 lineto
7.51953 347.242 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 347.242 moveto
11.2813 350.789 lineto
7.51953 354.332 lineto
3.76172 350.789 lineto
7.51953 347.242 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 354.332 moveto
11.2813 357.875 lineto
7.51953 361.418 lineto
3.76172 357.875 lineto
7.51953 354.332 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 354.332 moveto
11.2813 357.875 lineto
7.51953 361.418 lineto
3.76172 357.875 lineto
7.51953 354.332 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 361.418 moveto
11.2813 364.961 lineto
7.51953 368.504 lineto
3.76172 364.961 lineto
7.51953 361.418 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 361.418 moveto
11.2813 364.961 lineto
7.51953 368.504 lineto
3.76172 364.961 lineto
7.51953 361.418 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 368.504 moveto
11.2813 372.047 lineto
7.51953 375.59 lineto
3.76172 372.047 lineto
7.51953 368.504 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 368.504 moveto
11.2813 372.047 lineto
7.51953 375.59 lineto
3.76172 372.047 lineto
7.51953 368.504 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 375.59 moveto
11.2813 379.133 lineto
7.51953 382.676 lineto
3.76172 379.133 lineto
7.51953 375.59 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 375.59 moveto
11.2813 379.133 lineto
7.51953 382.676 lineto
3.76172 379.133 lineto
7.51953 375.59 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 382.676 moveto
11.2813 386.219 lineto
7.51953 389.76599 lineto
3.76172 386.219 lineto
7.51953 382.676 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 382.676 moveto
11.2813 386.219 lineto
7.51953 389.76599 lineto
3.76172 386.219 lineto
7.51953 382.676 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 389.76599 moveto
11.2813 393.30899 lineto
7.51953 396.85199 lineto
3.76172 393.30899 lineto
7.51953 389.76599 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 389.76599 moveto
11.2813 393.30899 lineto
7.51953 396.85199 lineto
3.76172 393.30899 lineto
7.51953 389.76599 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 396.85199 moveto
11.2813 400.39499 lineto
7.51953 403.93799 lineto
3.76172 400.39499 lineto
7.51953 396.85199 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 396.85199 moveto
11.2813 400.39499 lineto
7.51953 403.93799 lineto
3.76172 400.39499 lineto
7.51953 396.85199 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 403.93799 moveto
11.2813 407.48001 lineto
7.51953 411.02301 lineto
3.76172 407.48001 lineto
7.51953 403.93799 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 403.93799 moveto
11.2813 407.48001 lineto
7.51953 411.02301 lineto
3.76172 407.48001 lineto
7.51953 403.93799 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 411.02301 moveto
11.2813 414.56601 lineto
7.51953 418.10901 lineto
3.76172 414.56601 lineto
7.51953 411.02301 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 411.02301 moveto
11.2813 414.56601 lineto
7.51953 418.10901 lineto
3.76172 414.56601 lineto
7.51953 411.02301 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 418.10901 moveto
11.2813 421.65201 lineto
7.51953 425.195 lineto
3.76172 421.65201 lineto
7.51953 418.10901 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 418.10901 moveto
11.2813 421.65201 lineto
7.51953 425.195 lineto
3.76172 421.65201 lineto
7.51953 418.10901 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 425.195 moveto
11.2813 428.738 lineto
7.51953 432.285 lineto
3.76172 428.738 lineto
7.51953 425.195 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 425.195 moveto
11.2813 428.738 lineto
7.51953 432.285 lineto
3.76172 428.738 lineto
7.51953 425.195 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 432.285 moveto
11.2813 435.828 lineto
7.51953 439.371 lineto
3.76172 435.828 lineto
7.51953 432.285 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 432.285 moveto
11.2813 435.828 lineto
7.51953 439.371 lineto
3.76172 435.828 lineto
7.51953 432.285 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 439.371 moveto
11.2813 442.914 lineto
7.51953 446.457 lineto
3.76172 442.914 lineto
7.51953 439.371 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 439.371 moveto
11.2813 442.914 lineto
7.51953 446.457 lineto
3.76172 442.914 lineto
7.51953 439.371 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 446.457 moveto
11.2813 450 lineto
7.51953 453.543 lineto
3.76172 450 lineto
7.51953 446.457 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 446.457 moveto
11.2813 450 lineto
7.51953 453.543 lineto
3.76172 450 lineto
7.51953 446.457 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 453.543 moveto
11.2813 457.086 lineto
7.51953 460.629 lineto
3.76172 457.086 lineto
7.51953 453.543 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 453.543 moveto
11.2813 457.086 lineto
7.51953 460.629 lineto
3.76172 457.086 lineto
7.51953 453.543 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 460.629 moveto
11.2813 464.172 lineto
7.51953 467.715 lineto
3.76172 464.172 lineto
7.51953 460.629 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 460.629 moveto
11.2813 464.172 lineto
7.51953 467.715 lineto
3.76172 464.172 lineto
7.51953 460.629 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 467.715 moveto
11.2813 471.262 lineto
7.51953 474.805 lineto
3.76172 471.262 lineto
7.51953 467.715 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 467.715 moveto
11.2813 471.262 lineto
7.51953 474.805 lineto
3.76172 471.262 lineto
7.51953 467.715 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 474.805 moveto
11.2813 478.34799 lineto
7.51953 481.89099 lineto
3.76172 478.34799 lineto
7.51953 474.805 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 474.805 moveto
11.2813 478.34799 lineto
7.51953 481.89099 lineto
3.76172 478.34799 lineto
7.51953 474.805 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 481.89099 moveto
11.2813 485.43399 lineto
7.51953 488.97699 lineto
3.76172 485.43399 lineto
7.51953 481.89099 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 481.89099 moveto
11.2813 485.43399 lineto
7.51953 488.97699 lineto
3.76172 485.43399 lineto
7.51953 481.89099 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 488.97699 moveto
11.2813 492.51999 lineto
7.51953 496.06299 lineto
3.76172 492.51999 lineto
7.51953 488.97699 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 488.97699 moveto
11.2813 492.51999 lineto
7.51953 496.06299 lineto
3.76172 492.51999 lineto
7.51953 488.97699 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 496.06299 moveto
11.2813 499.60501 lineto
7.51953 503.14801 lineto
3.76172 499.60501 lineto
7.51953 496.06299 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 496.06299 moveto
11.2813 499.60501 lineto
7.51953 503.14801 lineto
3.76172 499.60501 lineto
7.51953 496.06299 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 503.14801 moveto
11.2813 506.69101 lineto
7.51953 510.23401 lineto
3.76172 506.69101 lineto
7.51953 503.14801 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 503.14801 moveto
11.2813 506.69101 lineto
7.51953 510.23401 lineto
3.76172 506.69101 lineto
7.51953 503.14801 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 510.23401 moveto
11.2813 513.781 lineto
7.51953 517.32397 lineto
3.76172 513.781 lineto
7.51953 510.23401 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 510.23401 moveto
11.2813 513.781 lineto
7.51953 517.32397 lineto
3.76172 513.781 lineto
7.51953 510.23401 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 517.32397 moveto
11.2813 520.867 lineto
7.51953 524.40997 lineto
3.76172 520.867 lineto
7.51953 517.32397 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 517.32397 moveto
11.2813 520.867 lineto
7.51953 524.40997 lineto
3.76172 520.867 lineto
7.51953 517.32397 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 524.40997 moveto
11.2813 527.953 lineto
7.51953 531.49597 lineto
3.76172 527.953 lineto
7.51953 524.40997 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 524.40997 moveto
11.2813 527.953 lineto
7.51953 531.49597 lineto
3.76172 527.953 lineto
7.51953 524.40997 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 531.49597 moveto
11.2813 535.039 lineto
7.51953 538.58197 lineto
3.76172 535.039 lineto
7.51953 531.49597 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 531.49597 moveto
11.2813 535.039 lineto
7.51953 538.58197 lineto
3.76172 535.039 lineto
7.51953 531.49597 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 538.58197 moveto
11.2813 542.125 lineto
7.51953 545.66803 lineto
3.76172 542.125 lineto
7.51953 538.58197 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 538.58197 moveto
11.2813 542.125 lineto
7.51953 545.66803 lineto
3.76172 542.125 lineto
7.51953 538.58197 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 545.66803 moveto
11.2813 549.211 lineto
7.51953 552.758 lineto
3.76172 549.211 lineto
7.51953 545.66803 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 545.66803 moveto
11.2813 549.211 lineto
7.51953 552.758 lineto
3.76172 549.211 lineto
7.51953 545.66803 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 552.758 moveto
11.2813 556.30103 lineto
7.51953 559.844 lineto
3.76172 556.30103 lineto
7.51953 552.758 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 552.758 moveto
11.2813 556.30103 lineto
7.51953 559.844 lineto
3.76172 556.30103 lineto
7.51953 552.758 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 559.844 moveto
11.2813 563.38702 lineto
7.51953 566.93 lineto
3.76172 563.38702 lineto
7.51953 559.844 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 559.844 moveto
11.2813 563.38702 lineto
7.51953 566.93 lineto
3.76172 563.38702 lineto
7.51953 559.844 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 566.93 moveto
11.2813 570.47302 lineto
7.51953 574.01599 lineto
3.76172 570.47302 lineto
7.51953 566.93 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 566.93 moveto
11.2813 570.47302 lineto
7.51953 574.01599 lineto
3.76172 570.47302 lineto
7.51953 566.93 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 574.01599 moveto
11.2813 577.55902 lineto
7.51953 581.10199 lineto
3.76172 577.55902 lineto
7.51953 574.01599 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 574.01599 moveto
11.2813 577.55902 lineto
7.51953 581.10199 lineto
3.76172 577.55902 lineto
7.51953 574.01599 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 581.10199 moveto
11.2813 584.64502 lineto
7.51953 588.18799 lineto
3.76172 584.64502 lineto
7.51953 581.10199 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 581.10199 moveto
11.2813 584.64502 lineto
7.51953 588.18799 lineto
3.76172 584.64502 lineto
7.51953 581.10199 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 588.18799 moveto
11.2813 591.72998 lineto
7.51953 595.27698 lineto
3.76172 591.72998 lineto
7.51953 588.18799 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 588.18799 moveto
11.2813 591.72998 lineto
7.51953 595.27698 lineto
3.76172 591.72998 lineto
7.51953 588.18799 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 595.27698 moveto
11.2813 598.82 lineto
7.51953 602.36298 lineto
3.76172 598.82 lineto
7.51953 595.27698 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 595.27698 moveto
11.2813 598.82 lineto
7.51953 602.36298 lineto
3.76172 598.82 lineto
7.51953 595.27698 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 602.36298 moveto
11.2813 605.906 lineto
7.51953 609.44897 lineto
3.76172 605.906 lineto
7.51953 602.36298 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 602.36298 moveto
11.2813 605.906 lineto
7.51953 609.44897 lineto
3.76172 605.906 lineto
7.51953 602.36298 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 609.44897 moveto
11.2813 612.992 lineto
7.51953 616.53497 lineto
3.76172 612.992 lineto
7.51953 609.44897 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 609.44897 moveto
11.2813 612.992 lineto
7.51953 616.53497 lineto
3.76172 612.992 lineto
7.51953 609.44897 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 616.53497 moveto
11.2813 620.078 lineto
7.51953 623.62097 lineto
3.76172 620.078 lineto
7.51953 616.53497 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 616.53497 moveto
11.2813 620.078 lineto
7.51953 623.62097 lineto
3.76172 620.078 lineto
7.51953 616.53497 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 623.62097 moveto
11.2813 627.164 lineto
7.51953 630.70697 lineto
3.76172 627.164 lineto
7.51953 623.62097 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 623.62097 moveto
11.2813 627.164 lineto
7.51953 630.70697 lineto
3.76172 627.164 lineto
7.51953 623.62097 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 630.70697 moveto
11.2813 634.25403 lineto
7.51953 637.797 lineto
3.76172 634.25403 lineto
7.51953 630.70697 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 630.70697 moveto
11.2813 634.25403 lineto
7.51953 637.797 lineto
3.76172 634.25403 lineto
7.51953 630.70697 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 637.797 moveto
11.2813 641.34003 lineto
7.51953 644.883 lineto
3.76172 641.34003 lineto
7.51953 637.797 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 637.797 moveto
11.2813 641.34003 lineto
7.51953 644.883 lineto
3.76172 641.34003 lineto
7.51953 637.797 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 644.883 moveto
11.2813 648.42603 lineto
7.51953 651.969 lineto
3.76172 648.42603 lineto
7.51953 644.883 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 644.883 moveto
11.2813 648.42603 lineto
7.51953 651.969 lineto
3.76172 648.42603 lineto
7.51953 644.883 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 651.969 moveto
11.2813 655.51202 lineto
7.51953 659.055 lineto
3.76172 655.51202 lineto
7.51953 651.969 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 651.969 moveto
11.2813 655.51202 lineto
7.51953 659.055 lineto
3.76172 655.51202 lineto
7.51953 651.969 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 659.055 moveto
11.2813 662.59802 lineto
7.51953 666.14099 lineto
3.76172 662.59802 lineto
7.51953 659.055 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 659.055 moveto
11.2813 662.59802 lineto
7.51953 666.14099 lineto
3.76172 662.59802 lineto
7.51953 659.055 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 666.14099 moveto
11.2813 669.68402 lineto
7.51953 673.22699 lineto
3.76172 669.68402 lineto
7.51953 666.14099 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 666.14099 moveto
11.2813 669.68402 lineto
7.51953 673.22699 lineto
3.76172 669.68402 lineto
7.51953 666.14099 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 673.22699 moveto
11.2813 676.77301 lineto
7.51953 680.31598 lineto
3.76172 676.77301 lineto
7.51953 673.22699 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 673.22699 moveto
11.2813 676.77301 lineto
7.51953 680.31598 lineto
3.76172 676.77301 lineto
7.51953 673.22699 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 680.31598 moveto
11.2813 683.85901 lineto
7.51953 687.40198 lineto
3.76172 683.85901 lineto
7.51953 680.31598 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 680.31598 moveto
11.2813 683.85901 lineto
7.51953 687.40198 lineto
3.76172 683.85901 lineto
7.51953 680.31598 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 687.40198 moveto
11.2813 690.945 lineto
7.51953 694.48798 lineto
3.76172 690.945 lineto
7.51953 687.40198 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 687.40198 moveto
11.2813 690.945 lineto
7.51953 694.48798 lineto
3.76172 690.945 lineto
7.51953 687.40198 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 694.48798 moveto
11.2813 698.031 lineto
7.51953 701.57397 lineto
3.76172 698.031 lineto
7.51953 694.48798 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 694.48798 moveto
11.2813 698.031 lineto
7.51953 701.57397 lineto
3.76172 698.031 lineto
7.51953 694.48798 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 701.57397 moveto
11.2813 705.117 lineto
7.51953 708.65997 lineto
3.76172 705.117 lineto
7.51953 701.57397 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 701.57397 moveto
11.2813 705.117 lineto
7.51953 708.65997 lineto
3.76172 705.117 lineto
7.51953 701.57397 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 708.65997 moveto
11.2813 712.203 lineto
7.51953 715.75 lineto
3.76172 712.203 lineto
7.51953 708.65997 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 708.65997 moveto
11.2813 712.203 lineto
7.51953 715.75 lineto
3.76172 712.203 lineto
7.51953 708.65997 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 715.75 moveto
11.2813 719.29303 lineto
7.51953 722.836 lineto
3.76172 719.29303 lineto
7.51953 715.75 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 715.75 moveto
11.2813 719.29303 lineto
7.51953 722.836 lineto
3.76172 719.29303 lineto
7.51953 715.75 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 722.836 moveto
11.2813 726.37903 lineto
7.51953 729.922 lineto
3.76172 726.37903 lineto
7.51953 722.836 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 722.836 moveto
11.2813 726.37903 lineto
7.51953 729.922 lineto
3.76172 726.37903 lineto
7.51953 722.836 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 729.922 moveto
11.2813 733.46503 lineto
7.51953 737.008 lineto
3.76172 733.46503 lineto
7.51953 729.922 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 729.922 moveto
11.2813 733.46503 lineto
7.51953 737.008 lineto
3.76172 733.46503 lineto
7.51953 729.922 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 737.008 moveto
11.2813 740.55103 lineto
7.51953 744.094 lineto
3.76172 740.55103 lineto
7.51953 737.008 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 737.008 moveto
11.2813 740.55103 lineto
7.51953 744.094 lineto
3.76172 740.55103 lineto
7.51953 737.008 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 744.094 moveto
11.2813 747.63702 lineto
7.51953 751.18 lineto
3.76172 747.63702 lineto
7.51953 744.094 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 744.094 moveto
11.2813 747.63702 lineto
7.51953 751.18 lineto
3.76172 747.63702 lineto
7.51953 744.094 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 751.18 moveto
11.2813 754.72302 lineto
7.51953 758.27002 lineto
3.76172 754.72302 lineto
7.51953 751.18 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 751.18 moveto
11.2813 754.72302 lineto
7.51953 758.27002 lineto
3.76172 754.72302 lineto
7.51953 751.18 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 758.27002 moveto
11.2813 761.81299 lineto
7.51953 765.35498 lineto
3.76172 761.81299 lineto
7.51953 758.27002 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 758.27002 moveto
11.2813 761.81299 lineto
7.51953 765.35498 lineto
3.76172 761.81299 lineto
7.51953 758.27002 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 765.35498 moveto
11.2813 768.89801 lineto
7.51953 772.44098 lineto
3.76172 768.89801 lineto
7.51953 765.35498 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 765.35498 moveto
11.2813 768.89801 lineto
7.51953 772.44098 lineto
3.76172 768.89801 lineto
7.51953 765.35498 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 772.44098 moveto
11.2813 775.98401 lineto
7.51953 779.52698 lineto
3.76172 775.98401 lineto
7.51953 772.44098 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 772.44098 moveto
11.2813 775.98401 lineto
7.51953 779.52698 lineto
3.76172 775.98401 lineto
7.51953 772.44098 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 779.52698 moveto
11.2813 783.07 lineto
7.51953 786.61298 lineto
3.76172 783.07 lineto
7.51953 779.52698 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 779.52698 moveto
11.2813 783.07 lineto
7.51953 786.61298 lineto
3.76172 783.07 lineto
7.51953 779.52698 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 786.61298 moveto
11.2813 790.156 lineto
7.51953 793.69897 lineto
3.76172 790.156 lineto
7.51953 786.61298 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 786.61298 moveto
11.2813 790.156 lineto
7.51953 793.69897 lineto
3.76172 790.156 lineto
7.51953 786.61298 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 793.69897 moveto
11.2813 797.24597 lineto
7.51953 800.789 lineto
3.76172 797.24597 lineto
7.51953 793.69897 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 793.69897 moveto
11.2813 797.24597 lineto
7.51953 800.789 lineto
3.76172 797.24597 lineto
7.51953 793.69897 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 800.789 moveto
11.2813 804.33197 lineto
7.51953 807.875 lineto
3.76172 804.33197 lineto
7.51953 800.789 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 800.789 moveto
11.2813 804.33197 lineto
7.51953 807.875 lineto
3.76172 804.33197 lineto
7.51953 800.789 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 807.875 moveto
11.2813 811.41803 lineto
7.51953 814.961 lineto
3.76172 811.41803 lineto
7.51953 807.875 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 807.875 moveto
11.2813 811.41803 lineto
7.51953 814.961 lineto
3.76172 811.41803 lineto
7.51953 807.875 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 814.961 moveto
11.2813 818.50403 lineto
7.51953 822.047 lineto
3.76172 818.50403 lineto
7.51953 814.961 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 814.961 moveto
11.2813 818.50403 lineto
7.51953 822.047 lineto
3.76172 818.50403 lineto
7.51953 814.961 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 822.047 moveto
11.2813 825.59003 lineto
7.51953 829.133 lineto
3.76172 825.59003 lineto
7.51953 822.047 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 822.047 moveto
11.2813 825.59003 lineto
7.51953 829.133 lineto
3.76172 825.59003 lineto
7.51953 822.047 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 829.133 moveto
11.2813 832.67603 lineto
7.51953 836.219 lineto
3.76172 832.67603 lineto
7.51953 829.133 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 829.133 moveto
11.2813 832.67603 lineto
7.51953 836.219 lineto
3.76172 832.67603 lineto
7.51953 829.133 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 836.219 moveto
11.2813 839.76599 lineto
7.51953 843.30902 lineto
3.76172 839.76599 lineto
7.51953 836.219 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 836.219 moveto
11.2813 839.76599 lineto
7.51953 843.30902 lineto
3.76172 839.76599 lineto
7.51953 836.219 lineto stroke
 0 0.502 0.502 setrgbcolor
newpath 7.51953 843.30902 moveto
11.2813 846.85199 lineto
7.51953 850.39502 lineto
3.76172 846.85199 lineto
7.51953 843.30902 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 7.51953 843.30902 moveto
11.2813 846.85199 lineto
7.51953 850.39502 lineto
3.76172 846.85199 lineto
7.51953 843.30902 lineto stroke
showpage
%%EOF
