Package manyind.sty, Wilberd van der Kallen 2019.  
Date of last change of _anything_ in the bundle 2019/01/29  
 
 This package provides support for many indexes, leaving all the bookkeeping to LaTeX and
 makeindex. No extra programs or files are needed. One runs latex and makeindex as if
 there is just one index. In the main file one puts commands like \setindex{main} to
 steer the flow.
 
 Some features of makeindex may no longer work.
 
 [Home page of package](https://www.staff.science.uu.nl/~kalle101/mind.html)
 

 Copyright 2019 Wilberd van der Kallen

 This package may be distributed under the conditions of the  
 LaTeX Project Public License, either version 1.2 of this license or  
 (at your option) any later version.  The latest version of this license is in  
 [lpp.txt](http://www.latex-project.org/lppl.txt)
 and version 1.2 or later is part of all distributions of LaTeX
 version 1999/12/01 or later.
