#### License

The package color-edits may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or (at your option) any later version. The latest version of this license is in
(http://www.latex-project.org/lppl.txt),
and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

#### Short Package Overview

The package provides a fairly light-weight solution for annotating LaTeX source code with color to show additions/changes, replacements, deletions, and comments. This is particularly useful when a document is being edited by multiple authors. Two package options allow the quick suppression of all colorful edits and comments, and showing text whose deletion was proposed.

Copyright (C) 2020 by David Kempe
