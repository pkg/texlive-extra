This work is distributed under the LaTeX Project Public License, version 1.3 or later. The license is contained in the file license.txt.
The preamble of the documentation may alternatively, at your choice, be reused under the terms of the WTFPL as published by Sam Hocevar (http://www.wtfpl.net/about/).
This work consists of the following files:

easyfloats-file-list.txt

easyfloats.ins
easyfloats.dtx
easyfloats.sty
README.md
DEPENDS.txt

doc/easyfloats.tex
doc/easyfloats.pdf
doc/easyfloats.bib
doc/links.tex
doc/undescribed-keywords.tex
doc/content/bug-reports-and-contributions.tex
doc/content/documentation.tex
doc/content/examples.tex
doc/content/installation.tex
doc/content/license.tex
doc/content/motivation.tex
doc/content/other-packages.tex
doc/content/names.tex
doc/content/titlepage.tex
doc/content/used-packages.tex
doc/preamble/bibliography.tex
doc/preamble/description-links.tex
doc/preamble/examplecode.tex
doc/preamble/keydoc.tex
doc/preamble/link.tex
doc/preamble/macros.tex
doc/preamble/markdown.tex
doc/preamble/pdfstring.tex
doc/lexer/latex_atletter.py

There are automated tests to test this work at
https://gitlab.com/erzo/latex-easyfloats/-/tree/master/test
These tests are licensed under the WTFPL http://www.wtfpl.net/about/.
