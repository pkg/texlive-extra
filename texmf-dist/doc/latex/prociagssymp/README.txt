
% This file is README.txt for the "procIAGssymp" package.
% Current version : [2022/05/05]
% Author: Battista Benciolini

The package "procIAGssymp" provides (re-)definitions of some LaTeX 
commands that can be useful for the preparation of a paper with 
the style of the proceedings of symposia sponsored by the 
International Association of Geodesy (IAG).

I presume that "official" more refined and  
up-to-date "sty" and "cls" file are now available 
from the publisher. This package is therefore not as useful 
as it was at the time of it's first publication, 
but it is pehaps still conveniet for some user.

The distribution includes :

README.txt 		this file
procIAGssymp.sty 	the package
TestprocIAGssymp.tex 	external documentation, example, suggestions
TestprocIAGssymp.pdf 	external documentation, example, suggestions

=====================   END  of  README  file ======================

