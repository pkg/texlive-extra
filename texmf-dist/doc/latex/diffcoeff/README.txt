diffcoeff: a package to ease the writing of a variety
of differential coefficients 

Andrew Parsloe (ajparsloe@gmail.com)

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either 
version 1.3c of this license or (at your option) any later 
version. The latest version of this license is in
  http://www.latex-project.org/lppl.txt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Version 5.1 of diffcoeff adds a version-conflict message if the 
now redundant ISO package option of earlier versions is used, 
and tweaks documentation. Version 5.0 of diffcoeff introduced 
many interface incompatibilities with version 4, which is still 
available through the trailing optional argument in the 
\usepackage statement, e.g., \usepackage{diffcoeff}[=v4] 
Version 5 requires xtemplate from the l3packages bundle and 
the mleftright package. 

Manifest
%%%%%%%%
Changed files:
README.txt            this document
diffcoeff.sty         LaTeX .sty file (current version, v5.1)
diffcoeff.pdf         documentation for current version, v5.1
diffcoeff.tex         LaTeX source of documentation for v5.1

Unchanged files
diffcoeff5.def         definition file for v5.1
diffcoeff4.sty         LaTeX .sty file, v4.2
diffcoeff4.pdf         documentation for v4.2
diffcoeff4.tex         LaTeX source of documentation for v4.2
diffcoeff-doc.def      definition file for v4.2

