# The spacingtricks package


## Presentation

This package provides macros for dealing with some spacing issues e.g.
 centering a single line, making a variable strut, indenting a block, typesetting a compact list,
 placing two boxes side by side with vertical adjustment. 


## Installation

- run LaTeX on spacingtricks.ins, you get the file spacingtricks.sty,
- if then you run LaTeX + dvips + ps2pdf on spacingtricks.dtx you get the file spacingtricks.pdf which is also in the archive,
- put the files spacingtricks.sty and spacingtricks.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
