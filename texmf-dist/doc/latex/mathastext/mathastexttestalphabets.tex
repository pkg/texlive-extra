\def\testfileincipit{Test file to accompany \texttt{mathastext}
version \texttt{1.3y} of \texttt{2022/11/04}}
%%----------------------------------------------------------------
%% Package: mathastext
%% Info:    Use the text font in math mode (JFB)
%% Version: 1.3y 2022/11/04
%% License: LPPL 1.3c (http://www.latex-project.org/lppl/lppl-1-3c.txt)
%% Copyright (C) 2011-2019, 2022 Jean-Francois Burnol <jfbu at free dot fr>
%% Examples of use of mathastext:
%%        http://jf.burnol.free.fr/mathastext.html
%%        http://jf.burnol.free.fr/showcase.html
%%----------------------------------------------------------------
%% This file `mathastexttestalphabets.tex' is for testing the extended
%% scope of the math alphabet commands with package `mathastext', via
%% the package command \MTnonlettersobeymathxx.
%%
%% Compile either with latex, pdflatex, lualatex (pdf output) or xelatex.
%%
%% See `mathastext.dtx' for the copyright and conditions of distribution or
%% modification.
%%
\documentclass{article}
\usepackage[hscale=0.66, vscale=0.72]{geometry}
\usepackage{amsmath}
\usepackage{mathtools}\mathtoolsset{centercolon}
%% WARNING THIS IS INCOMPATIBLE WITH BABEL+FRENCHB
%% BUT WITH MATHASTEXT LOADED THIS IS CORRECTED
%% *IF* \MTnonlettersobeymathxx IS MADE USE OF.
\usepackage{iftex}
\ifXeTeX
\expandafter\def\expandafter\testfileincipit\expandafter
 {\testfileincipit\ (compiled with \XeLaTeX)}
\usepackage[no-math]{fontspec}
\setmainfont[ExternalLocation,
             Mapping=tex-text,
             BoldFont=texgyretermes-bold,
             ItalicFont=texgyretermes-italic,
             BoldItalicFont=texgyretermes-bolditalic]{texgyretermes-regular}
\setmonofont[ExternalLocation,
             Mapping=tex-text]{texgyrecursor-regular}
\setsansfont[ExternalLocation,
             Mapping=tex-text]{texgyreheros-regular}
\else
\ifLuaTeX % for pdf output
\expandafter\def\expandafter\testfileincipit\expandafter
 {\testfileincipit\ (compiled with \LuaLaTeX)}
\usepackage[no-math]{fontspec}
\setmainfont[Ligatures=TeX]{TeX Gyre Termes}
\setmonofont[Ligatures=TeX]{TeX Gyre Cursor}
\setsansfont[Ligatures=TeX]{TeX Gyre Heros}
\else
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{tgtermes}
\usepackage{tgcursor}
\usepackage{tgheros}
\fi\fi
\usepackage{metalogo} % must be loaded /after/ [no-math]fontspec
\usepackage[french]{babel}
\usepackage[italic,asterisk]{mathastext}
\MTlettershape{n}\Mathastext[upright]
\MTnonlettersobeymathxx
\MTexplicitbracesobeymathxx
\delimitershortfall-1pt
\usepackage{color}
\begin{document}
\testfileincipit

\begin{center}
  \bfseries Two features of \texttt{mathastext}:\\
extended scope of the math alphabets and added italic corrections\par
\end{center}

The package makes
${!}\,{?}\,{,}\,{:}\,{;}\,{+}\,{-}\,{=}\,{(}\,{)}\,{[}\,{]}\,{<}\,{>}\,{\{}\,{\}}$,
the asterisk $*$, and $.\,/\,\vert\,\backslash\,{\#}\,{\$}\,{\%}\,{\&}$ obey the
math alphabet commands (this is the maximal list, some characters may have been
excluded by the corresponding package options). For the characters listed first
the mechanism involves a `mathematical activation'.

As this process may create incompatibilities, it will be put into action
for
${!}\,{?}\,{,}\,{:}\,{;}\,{+}\,{-}\,{=}\,{(}\,{)}\,{[}\,{]}\,{<}\,{>}$
only if the user makes use of the package command
\verb|\MTnonlettersobeymathxx| (and the braces necessitate
\verb|\MTexplicitbracesobeymathxx|).

It could be that one such character has been made `active' in the entire
document by some other package, typically a language definition file for the
\verb|babel| system. Here for example we have used \verb|babel| with the
\verb|french| option, which makes the high punctuation characters !?:; active
throughout the document (extra spacing is put in front of the character when
used in text; no change in math but perhaps for other languages and characters
this could happen, it is up to the language definition file to decide).

When \verb|mathastext| detects that a character it wants to `mathematically
activate' is already `active', it does not go further except if it seems that
the activation was done by Babel. If the activation was done by Babel, then
\verb|mathastext| replaces the expansion of the active character in math mode by
what is necessary to achieve its goal. It does not additionally mathematically
activate the character; rather it makes sure that the character is \emph{not}
mathematically active. In the present document the colon was made mathematically
active by \verb|mathtools| but this was already canceled in the preamble by
\verb|mathastext| as it was loaded later. And it is better so, because the
combination \verb|babel| (with option \verb|frenchb|) +\verb|mathtools| (with
\verb|centercolon|) makes \verb|$:$| create an infinite loop!

But even if someone had mathematically activated the colon after the preamble,
or after the loading of \verb|mathastext|, this would be canceled again
automatically for each inline or displayed mathematical formula (if the user
does \verb|\MTnonlettersobeymathxx|).

The conclusion with \verb|\MTnonlettersobeymathxx| is: if some package has tried
to make the character mathematically active, this will be overruled by
\verb|mathastext|; if some package has made the character globally active, then
the package wins except if it is Babel, as \verb|mathastext| may in the latter
case safely modify the action in math mode (paying attention to the fact that
the character should be usable in \verb|\label| and \verb|\ref| in and outside
of math mode).

The displayed equations next illustrate the extended scope of the math alphabets
which now apply to $=$, $-$, $($, $)$, $[$, $]$ (but not to the large delimiters
of course). Furthermore, for testing purposes the equations were labeled using
such characters, for example the last one has label \verb|eq=7|, to check that
the mathematical activation of $=$ does not cause problems with
\verb|\label/\ref|.

\def\testformula{\quad\Biggl\lbrace\biggl(\left(\left[[\sin(a) + \cos(b) - \log(c) =
\sec(d)]\right]\right)\biggr)\Biggr\rbrace}
\begin{equation}\testformula\label{eq:1}\end{equation}
\begin{equation}\mathnormalbold{mathnormalbold:\testformula}\label{eq;2}\end{equation}
\begin{equation}\mathrm{mathrm:\testformula}\label{eq?3}\end{equation}
\begin{equation}\mathbf{mathbf:\testformula}\label{eq!4}\end{equation}
\begin{equation}\mathit{mathit:\testformula}\label{eq(5}\end{equation}
\begin{equation}\mathtt{mathtt:\testformula}\label{eq)6}\end{equation}
\begin{equation}\mathsf{mathsf:\testformula}\label{eq=7}\end{equation}

Equations above are numbered \ref{eq:1}, \ref{eq;2}, \ref{eq?3}, \ref{eq!4}, and
$\ref{eq(5}$, $\ref{eq)6}$, and $\ref{eq=7}$.

\def\testline#1{$#1$&$\mathnormalbold{#1}$&$\mathrm{#1}$&$\mathbf{#1}$&$\mathit{#1}$&$\mathtt{#1}$&$\mathsf{#1}$}

\centerline{\begin{tabular}{ccccccc}
\testline{a!b}\\
\testline{a?b}\\
\testline{a,b}\\
\testline{a;b}\\
\testline{a:b}\\
\testline{a:=b}\\
\testline{a\vcentcolon= b}\\
\testline{a\colon b}\\
\testline{a.b}\\
\testline{a-b}\\
\testline{a+b}\\
\testline{a=b}\\
\testline{a<b}\\
\testline{a>b}\\
\testline{<x,y>}\\
\testline{\mathopen{<} x,y\mathclose{>}}\\
\testline{\left< x,y\right>}\\
\testline{a/b}\\
\testline{a\backslash b}\\
\testline{a\setminus b}\\
\testline{a|b}\\
\testline{a\mid b}\\
\testline{(a,b)}\\
\testline{[a,b]}\\
\testline{\{a,b\}}
\end{tabular}}

The question mark has been made active by \verb|babel+frenchb|.
\verb|mathastext| has imposed in math mode its ways (now \verb|$\mathbf{???}$|
gives $\mathbf{???}$). As the extra spacing is added by \verb|frenchb| only in
text, we had to use the math alphabet to check that indeed \verb|mathastext|
overruled Babel.

To double-check we will now make \string? mathematically active:
\verb|\mathcode`?="8000|\mathcode`?="8000. This is a sure cause for disaster
normally with Babel (don't do this at home without \verb|mathastext|!). But here
with \verb|$?$| no bad surprise (infinite loop!) awaits us: just $?$.

Let's take some other character, for example the opening parenthesis, and make
it catcode active:
\verb|\catcode`(=\active \def ({X}|.
Let's try the input
\verb|( and $($|.
\begingroup
\catcode`(=\active \gdef ({X}
This gives ( and $($. We see that \verb|mathastext| does not attempt to modify
the definition of the active character, as this activation was not done via the
\verb|babel| services. \catcode`\(=12 \mathcode`(="8000 We now revert the
parenthesis to catcode other (but maintain \verb|\def ({X}| as definition of its
active version), and then make it mathematically active using the command
\verb|\mathcode`(="8000|. If we try \verb|$((($| we see that the parenthesis is
not converted into an $X$: $((($. The mathematically active character was
overruled by \verb|mathastext|.

Issuing \verb|\MTnonlettersdonotobeymathxx|\MTnonlettersdonotobeymathxx\ we do
get the $X$'s from the input \verb|$((($|: $((($
\endgroup
This shows that \verb|mathastext| now does not modify in math mode the
non-letter \verb|(|.

\MTversion{upright}\MTnonlettersobeymathxx
We defined in the preamble of the document a \verb|mathastext|-enhanced
math version (named \verb|upright|) having the Latin letters upright in
math mode. Let's switch to
it: \newline
\hbox to\linewidth{\hss\verb|\MTversion{upright}|\hss}

With a font which is neither italic nor slanted, \verb|mathastext| automatically
inserts italic corrections for better positioning of the subscript:
\verb|$f_i^i$| gives
$f_i^i$.
After \verb|\MTnoicinmath| which turns off this feature\MTnoicinmath{}, the same
input gives $f_i^i$, which is different.
\footnote{last time I tried, this only worked with PDF\LaTeX{}, not with
  \LuaLaTeX{} or \XeTeX{}.}

Again with italic corrections on (\verb|\MTicinmath|)\MTicinmath{}
\verb|$f_{abc}^{def}$| gives $f_{abc}^{def}$, and here is another one:
$f^{f_{abc}^{def}}_u$. Without italic corrections\MTnoicinmath: $f_{abc}^{def}$,
and respectively $f^{f_{abc}^{def}}_u$.

\MTicinmath

\verb|mathastext| does not add these italic corrections inside arguments of
math alphabets, as this would prevent the formation of ligatures:
$\mathnormal{ff}$, $\mathrm{ff}$, $\mathit{ff}$, $\mathbf{ff}$, $\mathtt{ff}$
(no ligature in teletype) and $\mathsf{ff}$.\footnote{\llap{\textcolor{magenta}{\bfseries Changed!\kern3em}}Prior to 1.3i,
  italic corrections were added to the \string\mathnormal\ arguments.}

\centerline{\begin{tabular}{ccccccc}
\testline{a!b}\\
\testline{a?b}\\
\testline{a,b}\\
\testline{a;b}\\
\testline{a:b}\\
\testline{a:=b}\\
\testline{a\vcentcolon= b}\\
\testline{a\colon b}\\
\testline{a.b}\\
\testline{a-b}\\
\testline{a+b}\\
\testline{a=b}\\
\testline{a<b}\\
\testline{a>b}\\
\testline{<x,y>}\\
\testline{\mathopen{<} x,y\mathclose{>}}\\
\testline{\left< x,y\right>}\\
\testline{a/b}\\
\testline{a\backslash b}\\
\testline{a\setminus b}\\
\testline{a|b}\\
\testline{a\mid b}\\
\testline{(a,b)}\\
\testline{[a,b]}\\
\testline{\{a,b\}}
\end{tabular}}

\end{document}
\endinput
%%
%% End of file `mathastexttestalphabets.tex'.
