Canoniclayout v.1.0

Canoniclayout is a small module that allows to design a canonic layout 
based of the great circle inscribed within the page and tangent to the 
horizontal sides of the text block rectangle. The margins reflect the 
trimmed page shape ratio, therefore the page block principal diagonal 
coincides with the corresponding page diagonal; this layout is especially 
good for ISO shapes but can be used with many other traditional book 
page~shapes. 

This work is released under the Latex Project Pubblic Licence v.1.3c. The 
LPPL is distributed with any  TeX system distribution and can be found 
also in any CTAN archive.

Claudio Beccari 2022
Author maintained
claudio dot beccari at gmail dot com
