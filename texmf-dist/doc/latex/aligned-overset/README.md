This package allows the base character of `\underset` or `\overset` to be used as the alignment position for the amsmath aligned math environments.

To install, you can run `tex align-overset.dtx` and copy the generated file `aligned-overset.sty` to a directory in the search path of your TeX installation.
For quick evaluation, you can also rename `align-overset.dtx` to `align-overset.sty` and use that file directly.
