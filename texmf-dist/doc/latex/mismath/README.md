# mismath - Miscellaneous mathematical macros


## Presentation

The package provides some mathematical macros to typeset:
- mathematical constants e, i, pi in upright shape (automatically) as recommended by ISO 80000-2,
- vectors with beautiful arrow and adjusted norm,
- some standard operator names,
- improved spacings in mathematical formulas,
- systems of equations and small matrices,
- displaymath in double columns for long calculations.


## Installation

- run LaTeX on mismath.ins, you obtain the file mismath.sty,
- if then you run pdfLaTeX on mismath.dtx you get the file mismath.pdf which is also in the archive,
- put the files mismath.sty and mismath.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
