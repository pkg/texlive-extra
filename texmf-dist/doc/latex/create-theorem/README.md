<!-- Copyright (C) 2021-2022 by Jinwen XU -->

# `create-theorem` - Initializing theorem-like environments with multilingual support

The package `create-theorem` provides the commands `\NameTheorem`, `\CreateTheorem` and `\SetTheorem` for naming, initializing and configuring theorem-like environments. All of these commands have key-value based interface and are especially useful in multi-language documents, allowing the easy declaration of theorem-like environments that can automatically adapt to the language settings.

*For more information, please refer to its documentation.*

# License

This work is released under the LaTeX Project Public License, v1.3c or later.
