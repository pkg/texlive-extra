<!-- Time-stamp: <2020-07-29 14:23:06 administrateur> -->
<!-- Création : 2020-07-28T17:22:37+0200 -->

# clefval

Authors: Yvon Henel, aka Le TeXnicien de surface
([contact](le.texnicien.de.surface@yvon-henel.fr))
and Josselin Noirel.

## LICENCE

This material is subject to the LaTeX Project Public License, version
1.3c or later.

See https://www.latex-project.org/lppl.txt
for the details of that license.

© 2020 Yvon Henel (Le TeXnicien de surface)

## Description of the Package

`clefval.sty` provides two macros  `\TheKey` and `\TheValue` to define
then use pairs of key/value and gives a semblance of hash.

### Examples provided

  * **example.pdf** from <kbd>example.tex</kbd>, English version of the following.
  * **exemple.pdf** from <kbd>exemple.tex</kbd> (French): 8bit encoded
    legacy file using package `engpron` together with `clefval`.
  * **example-utf8.pdf** from <kbd>example-utf8.tex</kbd>  with <kbd>pdflatex</kbd>. The
    source can also be processed with <kbd>lualatex</kbd> and <kbd>xelatex</kbd>. The
    then created pdf-s will contain an additionnal text for <kbd>pdflatex</kbd>
    cannot by default process a key written in cyrillic alphabet.

## History

Version 0.1 — 2020-07-27 — adapted to utf-8 encoded source

Version 0   — 2004-05-22 — first public version


