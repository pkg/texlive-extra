%%
%% This is file `README.txt',
%%
%%   Copyright (C)  2020-2022 Claudio Beccari  all rights reserved.
%%   License information appended
%% 

File README.txt for package swfigure
        [2022-04-24 v.0.9.20 Managing large and spread wide figures]
This bundle contains  the swfigure.dtx, swfigure-examples.tex, 
and README.txt files plus eight fake figures with extension .jpg
or .pdf and a screenshot with extension .pdf.

This file README.txt contains the licence, the status, and some 
general information. Save the README.txt and swfigure.dtx file 
in a .../source/latex/swfigure folder either in the main TeX 
system tree or in your personal tree.
Save the swfigure-examples.tex, the derived swfigures-examples.pdf, 
and the various .jpg and .pdf files in folder .../doc/latex/swfigure.

Process the self extracting swfigure.dtx file with pdfLaTeX; 
repeat at least twice in order to get all references correct; 
you obtain the swfigure.pdf and the swfigure.sty files. 
After extraction move the .sty file to the .../tex/latex/swfigure 
folder, and move the .pdf file to the .../doc/latex/swfigure folder. 
From the original folder .../source/latex/swfigure move to the bin 
all files except the .dtx and the .txt ones.

The author disclaims any liability from every possible inconvenience 
deriving from the use of this software; you use it at your own risk.

%% 
%% Distributable under the LaTeX Project Public License,
%% version 1.3c or higher (your choice). The latest version of
%% this license is at: http://www.latex-project.org/lppl.txt
%% 
%% This work is "maintained"
%% 
%% This work consists of files swfigure.dtx, swfigure-examples.tex 
%% and README.txt, and the derived files swfigure.sty, swfigure.pdf
%% subfigure-exaples.pdf.
%%
%% The other .jpg and .pdf files contained in this bundle are just 
%% fake figures and a screenshot that anybody can replicate, copy 
%% or modify at will. 
%% 
