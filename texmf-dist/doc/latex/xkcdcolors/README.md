## Package `xkcdcolors` for LaTeX

In the year 2010, Randall Munroe on [posted a really fun and nice article](https://blog.xkcd.com/2010/05/03/color-survey-results/)  on the iconic web of [xkcd](https://xkcd.com/). 
He did a very curious experiment: showing colors to a lot of people and asking to name each one.

Afterward, he processed the data and sorted the names for each color by popularity --- that means, how many people gave the same name to the same color (no guidance here! Read the post linked above for details).

He obtained a nice list of around 950 color names. It's not at all surprising that a lot of people forged `CloudyBlue`, but some color is really surprising, like for example `BabyPoopGreen` or `DullPink`...

### LICENSE

This package use Randall Munroe's file [rgb.txt](https://xkcd.com/color/rgb.txt) which is distributed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/). You can use that license or [LPPL 1.3c](https://www.latex-project.org/lppl/lppl-1-3c/)
