edichokey
=======

`edichokey` is a LaTeX package for typesetting dichotomous identification key in 
indented style. It can be considered as an extended version of package `dichokey`, 
as `edichokey` is more capable of dealing with complex keys.

Contributing
------------

This package is authored and maintained by Yuchang Yang < yang.yc.allium@gmail.com >.

Discussions and questions are welcome.

Copyright and Licence
---------------------

    Copyright (C) 2017--2020 Yuchang Yang < yang.yc.allium@gmail.com >
    ----------------------------------------------------------------------
    
    This work may be distributed and/or modified under the
    conditions of the LaTeX Project Public License, either version 1.3c
    of this license or (at your option) any later version.
    The latest version of this license is in
      http://www.latex-project.org/lppl.txt
    and version 1.3c or later is part of all distributions of LaTeX
    version 2005/12/01 or later.

    This work has the LPPL maintenance status `maintained'.

    The Current Maintainer of this work is Yuchang Yang.

    This work consists of:
      - the style file: [edichokey.sty];
      - the manual files: [edichokey-doc-en.tex, edichokey-doc-en.pdf, README.md];
      - the example files: [edichokey-ex.tex, edichokey-ex.pdf].
