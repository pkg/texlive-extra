% fnspe 1.2a
% This material is subject to the LaTeX Project Public License.
% See https://www.latex-project.org/lppl/ for the details of that license.
%
% Useful commands for FNSPE students 
%
% Created by Michal Zeman
% Updated on September 20, 2017
% Uploaded on September 20, 2017

\documentclass{article}
\usepackage[arrowtensor,oldcomplex]{fnspe}
\usepackage{hyperref}
\begin{document}
	\section*{Options}
	
	In this package there are four categories of options (examples and differences will be shown further)
	\begin{enumerate}
		\item for interval notation
			\begin{itemize}
				\item \texttt{isointerval} for using standardized format of interval described in \textbf{ISO 31-11}
				\item \texttt{isoointerval} for using standardized alternative format of interval described in \textbf{ISO 31-11}
				\item \texttt{fnspeinterval} for using special notation used at FNSPE CTU in Prague
			\end{itemize}
		\item for tensor notation (now for vectors and matrices)
			\begin{itemize}
				\item \texttt{isotensor} for using standardized format of tensor 
				\item \texttt{undertensor} for using underline notation of tensor 
				\item \texttt{arrowtensor} for using arrow notation of tensor
			\end{itemize}
		\item for complex notation (real and complex part)
			\begin{itemize}
				\item \texttt{isocomplex} for using standardized format of complex and real part 
				\item \texttt{oldcomplex} for using old \LaTeX{} default format of complex and real part 
			\end{itemize}
		\item for definition notation
		\begin{itemize}
			\item \texttt{deftext} for definition using \textit{def.} over the equal
			\item \texttt{defcolon} for definition using the colon with equal
		\end{itemize}
	\end{enumerate}

	\section*{Macros}
		\subsection*{Interval}
		Let $a$ and $b$ be real numbers.
		
		\subsubsection*{Closed interval}
		Using of macro
		\begin{lstlisting}
			\ci{a}{b}
		\end{lstlisting}
		as \textbf{c}losed \textbf{i}nterval.
		
		\begin{itemize}
			\item \texttt{isointerval}
				\begin{equation*}
					\left[ a , b \right]
				\end{equation*}
			\item \texttt{isoointerval} (same as for \texttt{isointerval})
				\begin{equation*}
					\left[ a , b \right]
				\end{equation*}
			\item \texttt{fnspeinterval}
				\begin{equation*}
					\left< a , b \right>
				\end{equation*}
		\end{itemize}
	
		\subsubsection*{Opened interval}
		Using of macro
		\begin{lstlisting}
			\oi{a}{b}
		\end{lstlisting}
		as \textbf{o}pened \textbf{i}nterval.
		
		\begin{itemize}
			\item \texttt{isointerval}
			\begin{equation*}
			\left] a , b \right[
			\end{equation*}
			\item \texttt{isoointerval}
			\begin{equation*}
			\left( a , b \right)
			\end{equation*}
			\item \texttt{fnspeinterval} (same as for \texttt{isoointerval})
			\begin{equation*}
			\left( a , b \right)
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Right closed interval}
		Using of macro
		\begin{lstlisting}
			\rci{a}{b}
		\end{lstlisting}
		as \textbf{r}ight \textbf{c}losed \textbf{i}nterval.
		
		\begin{itemize}
			\item \texttt{isointerval}
			\begin{equation*}
			\left] a , b \right]
			\end{equation*}
			\item \texttt{isoointerval}
			\begin{equation*}
			\left( a , b \right]
			\end{equation*}
			\item \texttt{fnspeinterval}
			\begin{equation*}
			\left( a , b \right>
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Left closed interval}
		Using of macro
		\begin{lstlisting}
			\lci{a}{b}
		\end{lstlisting}
		as \textbf{l}eft \textbf{c}losed \textbf{i}nterval.
		
		\begin{itemize}
			\item \texttt{isointerval}
			\begin{equation*}
			\left[ a , b \right[
			\end{equation*}
			\item \texttt{isoointerval} (same as for \texttt{isointerval})
			\begin{equation*}
			\left[ a , b \right)
			\end{equation*}
			\item \texttt{fnspeinterval}
			\begin{equation*}
			\left< a , b \right)
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Using in text}
		All these macros can be used directly in text (thanks to the command \textit{ensuremath}). Therefore one can use this syntax
		\begin{lstlisting}
			Let $x$ be in \ci{a}{b}
		\end{lstlisting}
		which casts: Let $x$ be in \ci{a}{b}.
	
	\subsection*{Tensor}
		Let $x$ be vector and $A$ be matrix.
	
		\subsubsection*{Vector}
		Using of macro
		\begin{lstlisting}
			\vec{x}
		\end{lstlisting}
		as \textbf{vec}tor.
		
		\begin{itemize}
			\item \texttt{isotensor} - small letter with italic boldface
			\begin{equation*}
				\bm{\mathrm{x}}
			\end{equation*}
			\item \texttt{undertensor}
			\begin{equation*}
				\underline{x}
			\end{equation*}
			\item \texttt{arrowtensor}
			\begin{equation*}
				\vec x
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Matrix}
		Using of macro
		\begin{lstlisting}
		\mat{x}
		\end{lstlisting}
		as \textbf{mat}rix.
		
		\begin{itemize}
			\item \texttt{isotensor} - capital letter with italic boldface
			\begin{equation*}
				\bm{\mathrm{A}}
			\end{equation*}
			\item \texttt{undertensor}
			\begin{equation*}
				\underline{\underline{A}}
			\end{equation*}
			\item \texttt{arrowtensor}
			\begin{equation*}
				\mat{A}
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Using in text}
		All these macros can be used directly in text (thanks to the command \textit{ensuremath}). Therefore one can use this syntax
		\begin{lstlisting}
		Let \vec{x} be real.
		\end{lstlisting}
		which casts: Let \vec{x} be real.
	
	\subsection*{Macro for set}
		
		\subsubsection*{Set of natural numbers from 1 to $n$}
			Using of macro
			\begin{lstlisting}
				\allset{n}
			\end{lstlisting}
			as \textbf{all} natural number up to $n$ \textbf{set} leads to
			\begin{equation*}
				\allset{n}.
			\end{equation*}
		
		\subsubsection*{Set of natural numbers from 0 to $n$}
			Using of macro
			\begin{lstlisting}
				\allsetzero{n}
			\end{lstlisting}
			as \textbf{all} natural number up to $n$ \textbf{set} with \textbf{zero} leads to
			\begin{equation*}
				\allsetzero{n}.
			\end{equation*}
			
	\subsection*{Differentiability class}
		
		\subsubsection*{Just symbol}
			Using of macro
			\begin{lstlisting}
				\cclass
			\end{lstlisting}
			as \textbf{C} \textbf{class} leads to
			\begin{equation*}
				\cclass.
			\end{equation*}
			
		\subsubsection*{C infinity}
			Using of macro
			\begin{lstlisting}
				\ccinf
			\end{lstlisting}
			as \textbf{C} \textbf{class} of \textbf{inf}inity leads to
			\begin{equation*}
				\ccinf.
			\end{equation*}
			
		\subsubsection*{C of order $d$}
			Using of macro
			\begin{lstlisting}
				\ccof{d}
			\end{lstlisting}
			as \textbf{C} \textbf{class} \textbf{of} order leads to
			\begin{equation*}
				\ccof{d}.
			\end{equation*}
	
	\subsection*{Complex}
		Let $z \in \compn$.
	
		\subsubsection*{Real part}
		Using of macro
		\begin{lstlisting}
			\Re{x}
		\end{lstlisting}
		as \textbf{Re}al.
		
		\begin{itemize}
			\item \texttt{oldcomplex}
			\begin{equation*}
				\Re\left\{z\right\}
			\end{equation*}
			\item \texttt{isocomplex}
			\begin{equation*}
				\mathrm{Re}\; z
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Imaginary part}
		Using of macro
		\begin{lstlisting}
		\Im{x}
		\end{lstlisting}
		as \textbf{Im}aginary.
		
		\begin{itemize}
			\item \texttt{oldcomplex}
			\begin{equation*}
				\Im\left\{z\right\}
			\end{equation*}
			\item \texttt{isocomplex}
			\begin{equation*}
				\mathrm{Im}\; z
			\end{equation*}
		\end{itemize}
	
		\subsubsection*{Using in text}
		All these macros can be used directly in text (thanks to the command \textit{ensuremath}). Therefore one can use this syntax
		\begin{lstlisting}
		Let $x$ equal to \Re{z}.
		\end{lstlisting}
		which casts: Let $x$ equal to $\mathrm{Re}\;z$.
	
	\subsection*{Subscript}
	Subscript text with two or more characters should be written in roman style (not italic as default). One can use prefix ! which makes the word after it in roman style. Using of macro
	\begin{lstlisting}
		A_{!unique}
	\end{lstlisting}
	which leads to
	\begin{equation*}
		A_{!unigue}
	\end{equation*}
	instead of classic
	\begin{equation*}
		A_{unique}
	\end{equation*}
	
	\subsection*{Floor and ceiling functions}
		\subsubsection*{Floor function}
		Macro
		\begin{lstlisting}
		\floor{x}
		\end{lstlisting}
		as \textbf{floor} function leads to
		\begin{equation*}
		\floor{x}
		\end{equation*}
		
		\subsubsection*{Ceil function}
		Macro
		\begin{lstlisting}
		\ceil{x}
		\end{lstlisting}
		as \textbf{ceil} function leads to
		\begin{equation*}
		\ceil{x}
		\end{equation*}
		
	\subsection*{Definition operator}
	There are two ways to set a definition operator. First with \textit{text} and the second with \textit{colon}.
		
		\subsubsection*{Text definition}
		Macro
		\begin{lstlisting}
		x \df a
		\end{lstlisting}
		\begin{itemize}
			\item \texttt{deftext}
			\begin{equation*}
			x \stackrel{\mathclap{\normalfont{\mbox{\tiny def.}}}}{=} a
			\end{equation*}
			\item \texttt{defcolon}
			\begin{equation*}
				x \df a
			\end{equation*}
		\end{itemize}
		
	
	\subsection*{Special sets of numbers}
	
		\subsubsection*{Natural number}
		Macro
		\begin{lstlisting}
			\natun
		\end{lstlisting}
		as \textbf{natu}ral \textbf{n}umber leads to
		\begin{equation*}
			\natun
		\end{equation*}
		
		\subsubsection*{Natural number with zero included}
		Macro
		\begin{lstlisting}
		\nnzero
		\end{lstlisting}
		as \textbf{n}atural \textbf{n}umber \textbf{zero} leads to
		\begin{equation*}
		\nnzero
		\end{equation*}
		
		\subsubsection*{Integers}
		Macro
		\begin{lstlisting}
			\inte
		\end{lstlisting}
		as \textbf{inte}regers leads to
		\begin{equation*}
			\inte
		\end{equation*}
		
		\subsubsection*{Rational number}
		Macro
		\begin{lstlisting}
			\ratin
		\end{lstlisting}
		as \textbf{rati}onal \textbf{n}umber leads to
		\begin{equation*}
			\ratin
		\end{equation*}
		
		\subsubsection*{Real number}
		Macro
		\begin{lstlisting}
			\realn
		\end{lstlisting}
		as \textbf{real} \textbf{n}umber leads to
		\begin{equation*}
			\realn
		\end{equation*}
		
		\subsubsection*{Complex number}
		Macro
		\begin{lstlisting}
			\compn
		\end{lstlisting}
		as \textbf{comp}ex \textbf{n}umber leads to
		\begin{equation*}
			\compn
		\end{equation*}
	
		\subsubsection*{Using in text}
		All these macros can be used directly in text (thanks to the command \textit{ensuremath}). Therefore one can use this syntax
		\begin{lstlisting}
		Let $n$ be in \natun
		\end{lstlisting}
		which casts: Let $n$ be in \natun.
		
	\subsection*{Derivative}
		It is derived from \textit{physics} package. The manual is \href{http://mirrors.ctan.org/macros/latex/contrib/physics/physics.pdf}{here}.
		
	\subsection*{Operator}
	Partially derived from \textit{physics} package.
	
		\subsubsection*{Gradient}
		Macro
		\begin{lstlisting}
		\grad
		\end{lstlisting}
		as \textbf{grad}ient leads to
		\begin{equation*}
			\grad
		\end{equation*}
	
		\subsubsection*{Divergence}
		Macro
		\begin{lstlisting}
		\div
		\end{lstlisting}
		as \textbf{div}ergence leads to
		\begin{equation*}
			\div
		\end{equation*}
	
		\noindent Derived from \textit{physics} package, the original meaning of this command as a maths symbol for dividing has alias
		\begin{lstlisting}
			\divisionsymbol
		\end{lstlisting}
		which cast
		\begin{equation*}
			\divisionsymbol
		\end{equation*}
	
		\subsubsection*{Rotation}
		In English literature as \textbf{curl} operator has macro 
		\begin{lstlisting}
			\rot
		\end{lstlisting}
		as \textbf{rot}ation and leads to
		\begin{equation*}
			\rot
		\end{equation*}
	
		\noindent One can also use \textit{physics} package command
		\begin{lstlisting}
			\curl
		\end{lstlisting}
		
		\subsubsection*{Laplacian}
		Macro
		\begin{lstlisting}
			\lapl
		\end{lstlisting}
		as \textbf{lapl}acian leads to
		\begin{equation*}
			\lapl
		\end{equation*}
	
		\noindent One can also use \textit{physics} package notation
		\begin{equation*}
			\laplacian
		\end{equation*}
		which is cast by macro
		\begin{lstlisting}
			\laplacian
		\end{lstlisting}
		
	\section*{Degree}
		Macro
		\begin{lstlisting}
			\degree
		\end{lstlisting}
		as \textbf{degree} leads to \degree. Can be used without math mode.
		
	\section*{Physics unit}
		\subsubsection*{Variable unit}
		Macro
		\begin{lstlisting}
		\varun{m}{kg}
		\end{lstlisting}
		as \textbf{var}iable \textbf{un}it leads to 
		\begin{equation*}
			\varun{m}{kg}
		\end{equation*}
	
		\noindent This macro can be used directly in text (thanks to the \textit{ensure} function). Therefore one can use
		\begin{lstlisting}
			where \varun{m}{kg} is the mass.
		\end{lstlisting}
		which casts: where \varun{m}{kg} is the mass.
		
		\subsubsection*{Unit}
		Macro
		\begin{lstlisting}
		m\unit{kg}
		\end{lstlisting}
		as \textbf{unit} leads to 
		\begin{equation*}
			m\unit{kg}
		\end{equation*}
		This macro looks as
		\begin{lstlisting}
		\;\mathrm{kg}
		\end{lstlisting}
		the space before the roman characters is very important in science publications.
	
	\subsection*{Expected value}
	Macro
	\begin{lstlisting}
	\expv{x}
	\end{lstlisting}
	as \textbf{exp}ected \textbf{v}alue leads to 
	\begin{equation*}
		\expv{x}
	\end{equation*}
	
	\section*{Shortcuts}
		\subsubsection*{One half}
		Macro
		\begin{lstlisting}
			\hlf
		\end{lstlisting}
		as \textbf{h}a\textbf{lf} leads to 
		\begin{equation*}
			\hlf
		\end{equation*}
	
		\subsubsection*{One over }
		Macro
		\begin{lstlisting}
			\oover{x}
		\end{lstlisting}
		as \textbf{o}ne \textbf{over} leads to 
		\begin{equation*}
			\oover{x}
		\end{equation*} 
	
	\section*{Spaces}
		\subsubsection*{Horizontal space}
		Macro
		\begin{lstlisting}
		\hem[width]
		\end{lstlisting}
		as \textbf{h}space\{\textbf{em}\} leads to horizontal space of specific width (multiples of em). Special case is 1em
		\begin{lstlisting}
			\mathrm{text}\hem\mathrm{text}
		\end{lstlisting}
		which leads to
		\begin{equation*}
			\mathrm{text}\hem\mathrm{text}
		\end{equation*}
		or shortcut form space with 2em width
		\begin{lstlisting}
			\mathrm{text}\htem\mathrm{text}
		\end{lstlisting}
		which casts
		\begin{equation*}
			\mathrm{text}\htem\mathrm{text}
		\end{equation*}
	
		\subsection*{Implies with em spaces}
		Macro
		\begin{lstlisting}
			\impem
		\end{lstlisting}
		as \textbf{implies} with \textbf{em} spaces leads to
		\begin{equation*}
			\mathrm{text}\impem\mathrm{text}
		\end{equation*}
\end{document}