The endnotes-hy package
Author: D. P. Story 
Dated: 2020-04-08

This is a short package to support the creation of hypertext 
links in support of the endnotes package. The package modifies 
the syntax of the \endnote command: 
\endnote*[<num>]{<text>}\label{<name>}. When the *-option is 
used, no endnote mark is created, but the endnote itself is 
written. The \label command appears at the end of the \endnote 
and its arguments, rather than within the argument of the <text> 
argument. 

Now, I simply must get back to my retirement.

D. P. Story
www.acrotex.net
dpstory@uakron.edu
dpstory@acrotex.net
