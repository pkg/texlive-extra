# bible
LaTeX bibletext package

Copyright &copy; 2016 Camil Staps. Licensed under MIT (see the LICENSE file).

This is a LaTex package that allows you to insert Bible texts in a document by specifying references. Compile `bibletext.tex` to get the documentation.
