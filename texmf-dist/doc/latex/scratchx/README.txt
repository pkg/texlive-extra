%———————————————————————————————————————————————————————————————————————————————————%

The ScratchX package is used in order to include every kind of Scratch programs in LaTeX documents.
Particularly useful for Math Teachers ans IT specialists.


Le paquet ScratchX est utilisé pour écrire n’importe quel type de programme Scratch dans des documents LaTeX.
Notamment très utile pour les professeurs de Mathématiques et les informaticiens.

%———————————————————————————————————————————————————————————————————————————————————%

Author / auteur : Thibault Ralet
Version : 27 juillet 2017 (version 1.1)
E-mail : Thibault.Ralet@ac-clermont.fr
Licence : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt

%———————————————————————————————————————————————————————————————————————————————————%

Please read the user’s guide (Explanations_ScratchX.pdf) for further information. 

Lire le fichier d’aide (Explications_ScratchX.pdf) pour plus d’informations.

%———————————————————————————————————————————————————————————————————————————————————%