\documentclass{ltxdockit}[2010/09/26]
\usepackage[utf8]{luainputenc}
\usepackage[ngerman]{babel}
\usepackage{csquotes}
\usepackage{btxdockit}

%%% Verwende biber als backend und den hier definierten Zitierstil
\usepackage[%
  backend   = biber,%
  style     = german-legal-book,%
]{biblatex}
\addbibresource{biblatex-german-legal.bib}

\AtBeginToc{\setcounter{tocdepth}{3}}
\AtEndToc{\setcounter{tocdepth}{5}}

\titlepage{%
  title={Das \sty{biblatex-german-legal}-Paket},
  subtitle={biblatex-Zitierstile für die Rechtswissenschaften in Deutschland},
  url={https://git.linta.de/?p=~brodo/biblatex-german-legal.git;a=summary},
  author={Dominik Brodowski},
  email={dominik.brodowski@uni-saarland.de},
  revision={002},
  date={2020-11-15}}

\hypersetup{%
  pdftitle={Das \sty{biblatex-german-legal}-Paket},
  pdfsubject={biblatex-Zitierstile für die Rechtswissenschaften in Deutschland},
  pdfauthor={Dominik Brodowski},
  pdfkeywords={latex, pdflatex, lualatex, jura, rechtswissenschaften},
  citecolor=black,
}

\newcommand{\biblatex}{bib\LaTeX}

\begin{document}

\printtitlepage
\tableofcontents

\section{Einführung}

Wie nützlich es ist, \LaTeX{} bzw. seine Varianten für das Setzen von Texten und \biblatex{} für die Automatisierung von Zitaten, Referenzen bzw. Fundstellen zu nutzen, sei an dieser Stelle nicht wiederholt.\footnote{Ein Plädoyer liefert beispielsweise das ">Nachbarpaket"< \url{https://www.ctan.org/pkg/biblatex-jura2} in seiner Dokumentation.} Statt dessen möchte ich nur auf eines von mutmaßlich vielen Beispielen für juristische Monographien verweisen,\footnote{Namentlich \cite{Brodowski:2016}.} die vollständig in \LaTeX{} gesetzt und dessen Referenzen mit \biblatex{} verwaltet wurden.

\subsection{Über das Paket \sty{biblatex-german-legal}}

Allerdings sind die juristischen Zitierstile speziell. Sie unterscheiden sich maßgeblich von den in den Naturwissenschaften, vielen Geisteswissenschaften und auch in ausländischen Rechtsordnungen verwendeten Zitierstilen. Das hier vorgelegte Paket enthält aktuell einen auf \textbf{Monographien in den deutschen Rechtswissenschaften} ausgerichteten Zitierstil namens \sty{german-legal-book}, wie ich ihn selbst verwende; eine Erweiterung auf Zitierstile für \textbf{Aufsätze} ist geplant. Dank \biblatex{} und \LaTeX{} ist ein wesentliches Kriterium automatisch gewährleistet: die Einheitlichkeit des Zitierstils. Andere Fragen sind Geschmackssache. Dabei habe ich mich ganz an meinen eigenen Vorstellungen orientiert, die sich gewiss von anderen Geschmacksrichtungen unterscheiden mögen. Für derartige Variationen lässt sich dieses Paket aber weiterentwicklen und/oder modifizieren.

\subsection{Lizenz}

\copyright{} 2020 Dominik Brodowski \url{dominik.brodowski@uni-saarland.de} u.a. ">Permission is granted to copy, distribute and\slash or modify this software under the terms of the \lppl, version 1.3c or any later version.\footnote{\url{http://www.latex-project.org/lppl/}.}"<

\subsection{Mitarbeit}

Mitarbeit an diesem Paket ist sehr gerne gesehen! Schicken Sie mir hierzu bitte Änderungsvorschläge (idealerweise in Code) an die angegebene E-Mail-Adresse mit Ihrem Einverständnis, dies -- auch in abgewandelter Form -- in eine zukünftige Veröffentlichung dieses Pakets zu intergrieren. Hierfür eignet sich als Kurform das sogenannte ">Developer Certificate of
Origin"<.\footnote{\url{https://developercertificate.org/}.}

\section{Verwendung}

Um dieses Paket zu verwenden, sollte folgender Code-Schnipsel im Kopf des Dokuments eingebunden werden:

\begin{ltxcode}
\usepackage[backend=biber,style=german-legal-book]{biblatex}
\end{ltxcode}

\noindent{}%
und sodann eine oder mehrere Dateien angegeben werden, in denen sich die Literaturangaben befinden, z.B.

\begin{ltxcode}
\addbibresource{quellen-buecher.bib}
\addbibresource{quellen-aufsaetze.bib}
\addbibresource{quellen-kommentare.bib}
\end{ltxcode}

\subsection{Optionen}

Folgende Aufrufoption ist implementiert:

\begin{optionlist}

\boolitem[true]{edsuper}

Diese Option steuert, ob in den Zitierungen im Manuskript bei Büchern und Kommentaren die Auflagen\textit{nummer} mit abgedruckt wird.

\end{optionlist}

\subsection{Eintragstypen}

\sty{german-legal-book} kann mit Zitationen von Aufsätzen,\footnote{Dies gilt sowohl für Beiträge in Zeitschriften, die nach Jahren zitiert werden, z.B. \cite[107]{Radbruch_SJZ_1946_105}, als auch in solchen, die nach Bänden zitiert werden, z.B. \cite[295 \psqq]{NaginPogarsky_JQC_20_295}.} Monographien,\footcite[4]{Brodowski:2016} Beiträgen in Sammelbänden\footnote{\cite[211]{Vogel_Beck:2011}; auch mit Zitierung nach Randnummern: \cite[3 \psqq]{Moestl_HdbStR:179:2010}.} sowie Festschriften\footcite[408 \psq]{Koriath_FS_Jung}, Kommentaren\footnote{Dies gilt sowohl für Einzelautorenkommentare, z.B. \cite[\S\,248c Rn.~1 \psqq{}]{Fischer66}, für einbändige Kommentare, z.B. \cite[Bosch][§\,248c Rn.~1 \psq{}]{SchoenkeSchroeder30}, als auch für mehrbändige \mbox{(Groß-)}""Kommentare, z.B. \cite[Lüderssen/Jahn][§\,140 Rn.~1]{LR26}.} und Online-Quellen\footnote{Exemplarisch \cite{Mordkommission:2015}.} umgehen. Die seiten- oder randnummerngenaue Fundstelle wird jeweils in eckigen Klammern mit angegeben, erforderlichenfalls mit Verweis auf die folgende oder die folgenden Seiten:

\begin{ltxcode}
\cite[295 \psqq]{NaginPogarsky_JQC_20_295}
\cite[408 \psq]{Koriath_FS_Jung}
\end{ltxcode}

Im Einzelnen:

\subsubsection{Aufsätze}

Für Zeitschriften, die nach Jahren zitiert werden, verwende man \bibtype{article}, für Zeitschriften, die nach Bänden zitiert werden, verwende man \bibtype{periodical} und gebe den Band im Feld \bibfield{volume} an. Erforderlich sind zudem Angaben zum Verfasser (\bibfield{author}), zum Erscheinungsjahr (\bibfield{year}), zum Beitragstitel (\bibfield{title}) und zu den Seiten, auf denen der Beitrag zu finden ist (\bibfield{pages}). Weitere Felder (z.B. \bibfield{subtitle} oder \bibfield{issue}) können angegeben werden, haben aber teils nur Auswirkungen auf die Darstellung im Literaturverzeichnis. Die hier als Beispiele herangezogenen Aufsätze sind wie folgt deklariert:

\begin{ltxcode}
@article{Radbruch_SJZ_1946_105,
author = {Gustav Radbruch},
year = {1946},
journal = {SJZ},
pages = {105--108},
title = {Gesetzliches Recht und übergesetzliches Recht},
}

@periodical{NaginPogarsky_JQC_20_295,
journal = {Journal of Quantitative Criminology},
year = {2004},
title = {Time and Punishment: Delayed Consequences and Criminal Behavior},
volume = {20},
pages = {295--317},
author = {Daniel S. Nagin and Greg Pogarsky},
}
\end{ltxcode}

\subsubsection{Bücher}

Bücher (\bibtype{book}) haben als Mindestangaben einen Autor (\bibfield{author}) \textit{und/oder} einen Herausgeber (\bibfield{editor}), einen Titel (\bibfield{title}), ein Erscheinungsjahr (\bibfield{year}) und einen Erscheinungsort (\bibfield{address}). Optional sind z.B. Untertitel (\bibfield{subtitle}), Kurztitel für die Ausgabe in den Fußnoten (\bibfield{shorttitle}) oder Auflagenangaben (\bibfield{edition}). In der Bibliographie-Datei empfiehlt es sich, auch Festschriften und Sammelbände als Bücher zu deklarieren. Das ergibt für die hier genutzten Beispiele folgende (Min\-\mbox{dest-)}Deklarationen:

\begin{ltxcode}
@book{Beck:2011,
editor = {Susanne Beck and Christoph Burchard and Bijan Fateh-Moghadam},
title = {Strafrechtsvergleichung als Problem und Lösung},
address = {Baden-Baden},
year = {2011},
}

@book{FS_Jung,
editor = {Heinz Müller-Dietz and Egon Müller and Karl-Ludwig Kunz and%
   Henning Radtke and Guido Britz and Carsten Momsen and Heinz Koriath},
year = {2007},
address = {Baden-Baden},
title = {Festschrift für Heike Jung zum 65. Geburtstag am 23. April 2007},
shorttitle = {FS Jung},
}

@book{Brodowski:2016,
author = {Dominik Brodowski},
title = {Verdeckte technische Überwachungsmaßnahmen im Polizei- und%
   Strafverfahrensrecht},
subtitle = {Zur rechtsstaatlichen und rechtspraktischen Notwendigkeit%
   eines einheitlichen operativen Ermittlungsrechts},
shorttitle = {Verdeckte technische Überwachungsmaßnahmen},
address = {Tübingen},
year = {2016},
}
\end{ltxcode}

\subsubsection{Beiträge in Sammelbänden}

Hat man den Sammelband als \bibtype{book} deklariert, so lassen sich einzelne Beiträge in diesem Sammelband als \bibtype{inbook} knapp unter Angabe des Autors (\bibfield{author}), des Beitragstitels (\bibfield{title}), der Seitenspanne (\bibtype{pages}) sowie unter Angabe eines Querverweises (\bibfield{crossref}) deklarieren:

\begin{ltxcode}
@inbook{Vogel_Beck:2011,
author = {Joachim Vogel},
title = {Diskussionsbemerkungen: Instrumentelle Strafrechtsvergleichung},
pages = {205--212},
crossref = {Beck:2011},
}
\end{ltxcode}

Wird bei einem Beitrag als \bibfield{pagniation} der Wert ">section"< angegeben, so erfolgt die Zitierung nach Randnummern:

\begin{ltxcode}
@inbook{Moestl_HdbStR:179:2010,
author = {Markus Möstl},
title = {Grundrechtliche Garantien im Strafverfahren},
pages = {§ 179},
pagination = {section},
crossref = {HdbStR:VIII},
}
\end{ltxcode}

\subsubsection{Beiträge in Festschriften}

Gleiches gilt für Festschriften, wobei hier der Eintragstyp \bibtype{incollection} zu verwenden ist und in der Deklaration des Buches die Kurzbezeichnung (\bibfield{shorttitle}) deklariert werden muss:

\begin{ltxcode}
@incollection{Koriath_FS_Jung,
author = {Heinz Koriath},
title = {Fahrlässigkeit und Schuld},
pages = {397--409},
crossref = {FS_Jung},
}
\end{ltxcode}

\subsubsection{Kommentare}

Einzelautorenkommentare werden wie Bücher (\bibtype{book}) behandelt, alle übrigen (ein- und mehrbändigen) Kommentare mit \bibtype{commentary} deklariert. Dann ist die Kurzbezeichnung des Kommentars als \bibfield{shorthand} anzugeben. Verwendet man \bibtype{commentary}, ist der Bearbeiter bei der Zitation in eckingen Klammern mit anzugeben:

\begin{ltxcode}
\cite[Lüderssen/Jahn][§\,140 Rn.~1]{LR26}
\end{ltxcode}

In der Literaturangabe können auch Begründer*innen (Begr.), Fortführer*innen (Fortgef.) und Kommentator*innen (Komm.) angegeben werden, wie nachfolgend am Beispiel des Schönke-Schröder-Kommentars gezeigt werden möge:

\begin{ltxcode}
@commentary{LR26,
title = {Löwe-Rosenberg},
subtitle = {Die Strafprozeßordnung und das Gerichtsverfassungsgesetz},
shorthand = {LR},
edition = {26},
date = {2006/2014},
editor = {Volker Erb and Robert Esser and Ulrich Franke and%
    Kirsten Graalmann-Scheerer and Hans Hilger and Alexander Ignor},
}

@commentary{SchoenkeSchroeder30,
shorthand = {Schönke/Schröder},
editor = {Adolf Schönke},
editortype = {founder},
editora = {Horst Schröder},
editoratype = {continuator},
editorb = {Albin Eser and Walter Perron and Detlev Sternberg-Lieben%
    and Jörg Eisele and Bernd Hecker and Jörg Kinzig and Nikolaus Bosch%
    and Frank Schuster and Bettina Weißer and Ulrike Schittenhelm},
editorbtype = {commentator},
edition = {30},
address = {München},
title = {Strafgesetzbuch},
year = {2019},
}

@book{Fischer66,
author = {Thomas Fischer},
edition = {66},
address = {München},
year = {2019},
title = {Strafgesetzbuch mit Nebengesetzen},
shorttitle = {StGB},
}
\end{ltxcode}

\subsubsection{Online-Quellen}

Für Online-Quellen verwende man den \bibtype{online}-Eintragstyp und gebe sowohl die URL (\bibfield{url}) als auch das Abrufdatum (\bibfield{urldate}) an:

\begin{ltxcode}
@online{Mordkommission:2015,
author = {{Expertengruppe zur Reform der Tötungsdelikte}},
title = {Abschlussbericht der Expertengruppe zur Reform der Tötungsdelikte%
    (§§\,211 -- 213, 57a StGB)},
subtitle = {Dem Bundesminister der Justiz und für Verbraucherschutz Heiko%
    Maas im Juni 2015 vorgelegt},
year = {2015},
url = {https://www.bmjv.de/SharedDocs/Downloads/DE/News/Artikel/Abschluss%
    bericht_Experten_Toetungsdelikte.pdf?__blob=publicationFile&v=2},
urldate = {2020-01-01},
shorttitle = {Abschlussbericht},
}
\end{ltxcode}

\section{Literaturverzeichnis}

\printbibliography[heading=none]{}

%%% Beispieleinträge für das Literaturverzeichnis
\begin{filecontents*}{biblatex-german-legal.bib}

@book{Brodowski:2016,
author = {Dominik Brodowski},
title = {Verdeckte technische Überwachungsmaßnahmen im Polizei- und Strafverfahrensrecht},
subtitle = {Zur rechtsstaatlichen und rechtspraktischen Notwendigkeit eines einheitlichen operativen Ermittlungsrechts},
shorttitle = {Verdeckte technische Überwachungsmaßnahmen},
address = {Tübingen},
publisher = {Mohr Siebeck},
series = {Tübinger Rechtswissenschaftliche Abhandlungen},
number = {119},
year = {2016},
isbn = {978-3-16-154302-9},
size = {XXXIII, 649},
comment = {Diss. Tübingen 2015},
note = {Zugl.: Tübingen, Univ., Diss., 2015},
}

@article{Radbruch_SJZ_1946_105,
author = {Gustav Radbruch},
year = {1946},
journal = {SJZ},
pages = {105--108},
title = {Gesetzliches Recht und übergesetzliches Recht},
}

@periodical{NaginPogarsky_JQC_20_295,
journal = {Journal of Quantitative Criminology},
year = {2004},
title = {Time and Punishment: Delayed Consequences and Criminal Behavior},
volume = {20},
pages = {295--317},
author = {Daniel S. Nagin and Greg Pogarsky},
}

@book{Beck:2011,
editor = {Susanne Beck and Christoph Burchard and Bijan Fateh-Moghadam},
title = {Strafrechtsvergleichung als Problem und Lösung},
address = {Baden-Baden},
year = {2011},
}

@inbook{Vogel_Beck:2011,
author = {Joachim Vogel},
title = {Diskussionsbemerkungen: Instrumentelle Strafrechtsvergleichung},
pages = {205--212},
crossref = {Beck:2011},
}

@book{FS_Jung,
editor = {Heinz Müller-Dietz and Egon Müller and Karl-Ludwig Kunz and Henning Radtke and Guido Britz and Carsten Momsen and Heinz Koriath},
year = {2007},
address = {Baden-Baden},
title = {Festschrift für Heike Jung zum 65. Geburtstag am 23. April 2007},
shorttitle = {FS Jung},
}

@incollection{Koriath_FS_Jung,
author = {Heinz Koriath},
title = {Fahrlässigkeit und Schuld},
pages = {397--409},
crossref = {FS_Jung},
}

@book{Fischer66,
author = {Thomas Fischer},
edition = {66},
address = {München},
year = {2019},
title = {Strafgesetzbuch mit Nebengesetzen},
shorttitle = {StGB},
}

@commentary{LR26,
title = {Löwe-Rosenberg},
subtitle = {Die Strafprozeßordnung und das Gerichtsverfassungsgesetz},
shorthand = {LR},
edition = {26},
date = {2006/2014},
editor = {Volker Erb and Robert Esser and Ulrich Franke and Kirsten Graalmann-Scheerer and Hans Hilger and Alexander Ignor},
}

@commentary{SchoenkeSchroeder30,
shorthand = {Schönke/Schröder},
editor = {Adolf Schönke},
editortype = {founder},
editora = {Horst Schröder},
editoratype = {continuator},
editorb = {Albin Eser and Walter Perron and Detlev Sternberg-Lieben and Jörg Eisele and Bernd Hecker and Jörg Kinzig and Nikolaus Bosch and Frank Schuster and Bettina Weißer and Ulrike Schittenhelm},
editorbtype = {commentator},
edition = {30},
address = {München},
title = {Strafgesetzbuch},
year = {2019},
}

@online{Mordkommission:2015,
author = {{Expertengruppe zur Reform der Tötungsdelikte}},
title = {Abschlussbericht der Expertengruppe zur Reform der Tötungsdelikte (§§\,211 -- 213, 57a StGB)},
subtitle = {dem Bundesminister der Justiz  und für Verbraucherschutz Heiko Maas im Juni 2015 vorgelegt},
year = {2015},
url = {https://www.bmjv.de/SharedDocs/Downloads/DE/News/Artikel/Abschlussbericht_Experten_Toetungsdelikte.pdf?__blob=publicationFile&v=2},
urldate = {2020-01-01},
shorttitle = {Abschlussbericht},
}

@book{HdbStR:VIII,
title = {Handbuch des Staatsrechts der Bundesrepublik Deutschland},
subtitle = {Band VIII. Grundrechte},
editor = {Josef Isensee and Paul Kirchhof},
address = {Heidelberg},
year = {2010},
edition = {3},
}

@inbook{Moestl_HdbStR:179:2010,
author = {Markus Möstl},
title = {Grundrechtliche Garantien im Strafverfahren},
pages = {§ 179},
pagination = {section},
crossref = {HdbStR:VIII},
}

\end{filecontents*}

\end{document}
