biblatex-german-legal - comprehensive citation style for German legal texts
===========================================================================

The `biblatex-german-legal` aims to provide citation style (for footnotes and
bibliographies) for German legal texts. Currently it is focused on citations
in books, but may be extended for journal articles in the future.

The source code is available in a git repository accessible at
git://git.linta.de/~brodo/biblatex-german-legal.git , which may be browsed at
https://git.linta.de/?p=~brodo/biblatex-german-legal.git;a=summary . If you
click on 'snapshot' there, you can download a tar.gz containing the contents
of the repository. 

The rationale behind this package is as follows: The package I have relied
upon previously ('biblatex-juradiss') has become outdated. The alternatives,
in particular 'biblatex-jura2', do not offer (yet) the flexibility I prefer,
and (though this is a matter of subjective taste) partly follow stylistic
choices I do not share. Parts of this code are inspired, however, by these
two packages. Though this package is not a Derived Work of these other
packages within the meaning of the LPPL, here are, in line with section 5
lit. d sublit. ii of the LPPL, references to these packages:

- biblatex-juradiss: https://www.ctan.org/pkg/biblatex-juradiss
- biblatex-jura2:    https://www.ctan.org/pkg/biblatex-jura2


## Licence

This work may be distributed and/or modified under the conditions of the
LaTeX Project Public License (LPPL), either version 1.3c of this license or
(at your option) any later version. The latest version of this license is at
<http://www.latex-project.org/lppl.txt> and version 1.3 or later is part of
all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status 'author-maintained'.

The Current Maintainer of the work is Dominik Brodowski
<dominik.brodowski@uni-saarland.de>. Patches, bug reports, and critique are
welcome.

