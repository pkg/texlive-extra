%% documentation for anonymous-acm.sty  
%% version 1.0 09-May-2020   
%% Maintained by Brett A. Becker brett.becker@ucd.ie

% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either 
% version 1.3 of this license or any later version.
% The latest version of this license is in
% http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.

% This program is distributed in the hope that it will be useful 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

% requires acmart.cls

\documentclass[sigconf, balance=false]{acmart}

\setcopyright{rightsretained}

\usepackage[false]{anonymous-acm}

\title{The anonymous-acm Package for the ACM \LaTeX~ Master Article Template}
\subtitle{Version 1.0: May 09, 2020}

\authoranon{
\author{Brett A. Becker}
\affiliation{
    \institution{University College Dublin}
    \city{Dublin}
    \country{Ireland}
    }
\email{brett.becker@ucd.ie}

\author{Jiahao Xu}
\affiliation{
    \institution{Beijing-Dublin International College}
    \city{Beijing}
    \country{China}
    }

\author{Zixiang Xu}
\affiliation{
    \institution{Beijing-Dublin International College}
    \city{Beijing}
    \country{China}
    }

\author{Xinchi Feng}
\affiliation{
    \institution{Beijing-Dublin International College}
    \city{Beijing}
    \country{China}
    }

\author{Zhaorui Wang}
\affiliation{
    \institution{Beijing-Dublin International College}
    \city{Beijing}
    \country{China}
    }
}

\begin{document}

\begin{abstract}
Academics often need to submit anonymous versions of their papers for peer-review. This often requires anonymization which at some future date needs to be reversed. However de-anonymizing an anonymized paper can be laborious and error-prone. This \LaTeX~ package allows anonymization options to be specified at the time of writing for authors using acmart.cls, the official Association of Computing Machinery (ACM) master article template. Anonymization or deanonymization is carried out by simply changing one option and recompiling.
\end{abstract}

\maketitle

%\tableofcontents

\section{Introduction}
The anonymous-acm package allows for the easy anonymization and deanonymization of academic papers. The required information for both the anonymous and non-anonymous versions of the paper are specified at the time of writing. It is made to be used with the official Association of Computing Machinery (ACM) master article template for \LaTeX~ as described in Section \ref{sec:reqs}. This pdf was produced using acmart. acmart has an anonymous mode which allows for the anonymization of author information, the optional acknowledgments section, and also allows arbitrary text to be hidden (without being replaced by anything such as placeholder text). This package provides additional functionality including placeholder text, anonymous links and anonymous citations/references as described in Section~\ref{sec:usage}.

\subsection{Demonstration}
A live demonstration of this package is available on Overleaf at:
\begin{itemize}
    \item \href{https://www.overleaf.com/read/bmqdgdxkfwys}{https://www.overleaf.com/read/bmqdgdxkfwys}
\end{itemize}
\noindent (Overleaf account required). 

\subsection{Installation}
The anonymous-acm package can be found at:

\begin{itemize}
    \item \url{https://ctan.org/pkg/anonymous-acm}
\end{itemize}
\noindent Simply include anonymous-acm.sty in the preamble of your .tex file.

\subsection{Requirements}
\label{sec:reqs}
The anonymous-acm package is designed to work with \textbf{acmart}, the official Association of Computing Machinery (ACM) master article template for \LaTeX. This file was prepared with acmart. It is available here:
\begin{itemize}
\item \url{https://www.acm.org/publications/proceedings-template} 
\end{itemize}

\noindent or here: 
\begin{itemize}
    \item \url{https://www.ctan.org/pkg/acmart}. 
\end{itemize}
This package was tested with acmart version 1.69. The users' guide, implementation and GitHub repo for this class are available here at the link above.  

The anonymous text and link features described in Sections \ref{sec:text} and \ref{sec:links} will work with or without acmart. The anonymization of references and citations described in Section \ref{sec:refs} may or may not - it was tested with the ACM reference format only. The anonymization of authors and acknowledgments described in Sections \ref{sec:authors} and \ref{sec:acks} will most likely only work with acmart.   
 
This package also uses the hyperref package which is already used by acmart, so there is probably no need to use it directly, but we include it here for completeness:

\begin{itemize}
    \item \url{http://www.ctan.org/pkg/hyperref}
\end{itemize}

\subsection{License}
This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or any later version. The latest version of this license is in  http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

\section{Usage}
\label{sec:usage}

The following command should be placed in the preamble of the .tex document you wish to make anonymous. 
\begin{verbatim}
    \usepackage[option]{anonymous} 
\end{verbatim}

\noindent \texttt{option} can be \texttt{true} or \texttt{false}. True turns anonymous mode on, and false turns anonymous mode off. 

\subsection{Author Anonymity}
\label{sec:authors}
The following command is used to anonymize author information. 

\begin{verbatim}
    \authoranon{authors information}
\end{verbatim}

\noindent It should be used to enclose the normal author entries as in the example below. 

\begin{verbatim}
    \authoranon{
        \author{Brett A. Becker}
        \affiliation{
        \institution{University College Dublin}
        \city{Dublin}
        \country{Ireland}}
        \email{brett.becker@ucd.ie}
        ...
        %other authors' information here
    }
\end{verbatim}

\noindent When anonymous mode is on, the author listing will appear as follows:

\begin{center}
    \textbf{Anonymous Author(s)}
\end{center}
\vspace*{3mm}
\noindent Additionally, author information will be removed from the `ACM Reference Format' and the `bibstrip' (in this document at the bottom of the first column on page 1). It will also be removed from running headers that appear on pages other than the title page. 

\subsection{Anonymous Arbitrary Text}
\label{sec:text}
The following command replaces arbitrary text with a default placeholder or a placeholder of the user's choice.

\begin{verbatim}
    \textanon{text}{placeholder}
\end{verbatim}

\noindent where \texttt{text} is the text that should be replaced. If the second argument is empty, \texttt{text} is replaced with the default placeholder: <text removed for peer review>. If \texttt{placeholder} is non-empty, that is used to replace \texttt{text}. 
\ \\

\noindent \textbf{Example 1}

\begin{verbatim}
    In 2020 at \textanon{University College Dublin}{}
    we conducted a randomized controlled trial with
    128 students enrolled in the BSc in Biology.
\end{verbatim}

\noindent When anonymous mode is on, the above will appear as follows:
\begin{quote}
    In 2020 at <text removed for peer review> we conducted a randomized controlled trial with 128 students enrolled in the BSc in Biology. 
\end{quote}
When anonymous mode is off, it will appear as expected:
\begin{quote}
    In 2020 at University College Dublin we conducted a randomized controlled trial with 128 students enrolled in the BSc in Biology. 
\end{quote}
\ \\
\noindent \textbf{Example 2}

\begin{verbatim}
    In 2020 at \textanon{University College Dublin}
    {Anonymous University} we conducted a randomized
    controlled trial with 128 students enrolled in the
    BSc in Biology.
\end{verbatim}

\noindent When anonymous mode is on, the above will appear as follows:
\begin{quote}
    In 2020 at Anonymous University we conducted a randomized controlled trial with 128 students enrolled in the BSc in Biology. 
\end{quote}
When anonymous mode is off, it will appear as expected:
\begin{quote}
    In 2020 at University College Dublin we conducted a randomized controlled trial with 128 students enrolled in the BSc in Biology. 
\end{quote}

\subsection{Anonymous Links}
\label{sec:links}
The following commands allow links to be hidden with some flexibility: 
\begin{verbatim}
    \linkanon{}{}
    \textlinkanon{}{}
\end{verbatim}

\noindent Here is an example:

\begin{verbatim}
    The url for Google is:
    \linkanon{https://www.google.com}{www.google.com}
    
    \textlinkanon{https://www.ryanair.com}{Click here} 
    to go to Ryanair.
\end{verbatim}

\noindent When anonymous mode is on, links are deactivated and the above appears as follows:
\begin{quote}
    The url for Google is: <anonymous link>
\end{quote}    
\begin{quote}
    Click here to go to Ryanair. 
\end{quote}

\noindent When anonymous mode is off, links are active, and the above appears as follows: 
\begin{quote}
    The url for Google is: \href{www.google.com}{www.google.com}
\end{quote}    
\begin{quote}        
    \href{www.ryanair.com}{Click here} to go to Ryanair.
\end{quote}

\noindent \texttt{\textbackslash linkanon} can also be used with one argument as follows:

\begin{verbatim}
    \linkanon{www.google.com}
\end{verbatim}

\noindent In that case the link text and the link itself are both set to be the argument. 

\subsection{Anonymous References \& Citations}
\label{sec:refs}
The following command is used to make citations anonymous. 

\begin{verbatim}
    \citeanon[option]{bibentry} 
\end{verbatim}

\noindent When anonymous mode is on, citations will appear as [anonymous] and the corresponding reference will not appear in the reference list. If there are other non-anonymous citations in your paper, they may appear renumbered depending on the location of the temporarily anonymized reference in the ordering of references.. 

When anonymous mode is off, citations appear as normal. For example, \texttt{\textbackslash citeanon\{bibentry\}} might appear as [4] depending on your bibfile and bibliography style. \texttt{\textbackslash citeanon[p.1]\{bibentry\}} might appear as [4, p.1]. Additionally, the corresponding reference will appear in the references list. 

\subsection{Anonymous Acknowledgments}
\label{sec:acks}
When anonymous mode is on the Acknowledgments Section is automatically hidden (header and body). When anonymous mode is off it appears as normal. It is necessary to use the ``acks'' environment provided by acmart:

\begin{verbatim}
    \begin{acks}
        Acknowledgments go here. 
    \end{acks}
\end{verbatim}

\end{document}