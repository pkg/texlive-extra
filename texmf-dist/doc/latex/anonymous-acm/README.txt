The anonymous-acm package for acmart.cls

The anonymous-acm package allows for the easy anonymization and deanonymization of academic papers that use the official Association of Computing Machinery (ACM) master article template, acmart.cls. The required information for both the anonymous and non-anonymous versions are specified at the time of writing. Anonymization or deanonymization is carried out by simply changing one option and recompiling. 

Maintained by Brett A. Becker, brett.becker@ucd.ie

This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or any later version. The latest version of this license is in  http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

This program is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.