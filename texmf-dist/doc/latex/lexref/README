lexref 2015/01/11 v1.1 alpha
Copyright (c) Adrien Vion. adrien[dot]vion3[at]gmail[dot]com.
--------------------------------------------------------
This LaTeX package is aimed at continental lawyers (especially swiss and german). It provides macros to make references to legal provisions conveniently and uniformly. It also allows the user to automatically add cited Acts to a nomenclature list and to build specific indexes for each cited Act.

IMPORTANT NOTICE for users who were using version 1.0.1a: the \DeclareLex macro has changed and now requires four arguments instead of three. Please modify your .tex documents accordingly (see documentation).

Please read the package documentation for further information.

This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3 of this license or any later version.
The latest version of this license is in http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Adrien Vion.

This work consists of the file lexref.sty and the related information files.

Please note that this work is experimental (still alpha version) and that retrocompatibility may not be ensured.