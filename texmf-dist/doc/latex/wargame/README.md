A package to make Hex'n'Counter wargames in LaTeX
=================================================

Version 0.3.2
-------------

This package can help make classic [Hex'n'Counter
wargames](https://en.wikipedia.org/wiki/Wargame) using LaTeX. The
package provide tools for generating

-   Hex maps and boards
-   Counters (called *chits* since TeX have already taken *counters*)
    for units, markers, and so on
-   Counter sheets
-   Order of Battle charts
-   Illustrations in the rules using the defined maps and counters

The result will often be a PDF (or set of PDFs) that contain everything
one will need for a game (rules, charts, boards, counter sheets).

-   The package uses [NATO App6](#nato-app6 "Section below") symbology
    for units.
-   The package uses [TikZ](https://github.com/pgf-tikz/pgf) for most
    things.
-   The package support exporting the game to a
    [VASSAL](#vassal-support%20'Section%20below') module

Sources
-------

The sources of the package are kept at
[GitLab](https://gitlab.com/wargames_tex/wargame_tex)

Download from GitLab
--------------------

-   [Zip file of package and support
    files](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist)
-   [Browse content of
    package](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/browse?job=dist)
-   [Documentation](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/wargame.pdf?job=dist)
-   [Tutorial](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/tutorial.pdf?job=dist)
    (and associated VASSAL
    [module](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/Game.vmod?job=dist))
-   [Table of
    symbols](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/symbols.pdf?job=dist)
-   [Compatibility](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/compat.pdf?job=dist)

To install, get the ([zip
file](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist))
and unzip into your TeX tree, for example

    mkdir -p ~/texmf
    (cd ~/texmf && unzip ../artifacts.zip)

on Un\*x like systems.

If you clone from GitLab
(`git clone https://gitlab.com/wargames_tex/wargame_tex.git`) to get the
sources, then you can do

    cd wargame
    make install 

to install in `~/texmf`. If you prefer another destination, say
`/usr/local/share/texmf`, do

    make install DESTDIR=/usr/local/share/texmf

Download from CTAN
------------------

The package is available from
[CTAN](https://ctan.org/tex-archive/macros/latex/contrib/wargame) in the
directory `/macros/latex/contrib/wargame`. As of this writing, the
package has not hit any of the major distributions (e.g.,
[TeXLive](https://tug.org/texlive/index.html)).

### From TDS zip archive

You can get the *TDS* [zip
file](https://mirrors.ctan.org/install/macros/latex/contrib/wargame.tds.zip)
and unpack that into your desired destination, e.g., `~/texmf`

    unzip wargame.tds.zip -d ~/texmf

### From sources

If you get the source (`wargame.ins`, all `.dtx`,`.py`, and `Makefile`)
files, then do

    make install 

to install into `~/texmf`. If you prefer to install somewhere else, say
`/usr/local/share/texmf`, then do

    make install DESTDIR=/usr/local/share/texmf 

If you do not have `make` (or `gmake` on MacOSX), or because you are on
some system which does not have that tool (e.g., Windows), then do (the
equivalent of)

    latex wargame.ins
    pdflatex wargame.beach
    pdflatex wargame.city
    pdflatex wargame.light_woods
    pdflatex wargame.mountains
    pdflatex wargame.rough
    pdflatex wargame.swamp
    pdflatex wargame.town
    pdflatex wargame.village
    pdflatex wargame.woods

(You need to use `pdflatex`, `xelatex`, or `lualatex` - plain `latex`
with DVI output will not work)

Then copy the relevant files to your TeX tree (e.g., `~/texmf/`) as

    mkdir ~/texmf/tex/latex/wargame
    cp tikzlibrary*.tex         ~/texmf/tex/latex/wargame/
    cp wargame.sty              ~/texmf/tex/latex/wargame/
    cp wgexport.cls             ~/texmf/tex/latex/wargame/
    cp wargame.*.pdf            ~/texmf/tex/latex/wargame/
    cp wgexport.py              ~/texmf/tex/latex/wargame/
    cp wgsvg2tikz.py            ~/texmf/tex/latex/wargame/

To generate the documentation, after having done the above, do

    pdflatex wargame.dtx
    makeindex -s gind -o wargame.ind wargame.idx
    pdflatex wargame.dtx
    pdflatex wargame.dtx
    pdflatex symbols.tex
    pdflatex compat.tex
    pdflatex compat.tex

(You need to use `pdflatex`, `xelatex`, or `lualatex` - plain `latex`
with DVI output will not work)

You can install these into your TeX tree with (e.g., `~/texmf/`)

    mkdir -p ~/texmf/doc/latex/wargame/
    cp wargame.pdf  ~/texmf/doc/latex/wargame/
    cp symbols.py   ~/texmf/doc/latex/wargame/
    cp compat.py    ~/texmf/doc/latex/wargame/

If you want to generate the tutorial document, do

    cd tutorial 
    make 

or

    cd tutorial
    pdflatex game
    pdflatex game 

If you also want to make the tutorial VASSAL module, do (also in
`tutorial`), do

    pdflatex export.tex 
    ../wgexport.py export.pdf export.json -o Game.vmod \
        -d "Example module from LaTeX PnP game" \
        -t "LaTeX wargame tutorial" -v 0.1 \
        -p patch.py
        

Note, you need `pdfinfo` and `pdftocairo` from Poppler, and Python with
the module `PIL` for this. On Debian based systems, do

    sudo apt install poppler-utils python3-pil

Tutorial
--------

See the [tutorial](tutorial/README.md "Link works on GitLab only") page
for more
([here](https://ctan.org/tex-archive/macros/latex/contrib/wargame/doc/tutorial/)
if you browse from CTAN).

Examples
--------

Below are some print'n'play board wargames made with this package. These
are not original games but rather revamps of existing games. All credits
goes to the original authors of these games.

-   WWII
    -   Eastern Front
        -   [Battle for Moscow](https://gitlab.com/wargames_tex/bfm_tex)
            (classic introductory game)
    -   Western Front
        -   [D-Day](https://gitlab.com/wargames_tex/dday_tex) (Avalon
            Hill classic)
        -   [Paul Koenig's Market
            Garden](https://gitlab.com/wargames_tex/pkmg_tex)
        -   [Paul Koenig's
            D-Day](https://gitlab.com/wargames_tex/pkdday_tex)
    -   Pacific theatre
        -   [First
            Blood](https://gitlab.com/wargames_tex/firstblood_tex)
            (Avalon Hill International Kriegspiel Association classic)
    -   Africa
        -   [Afrika
            Korps](https://gitlab.com/wargames_tex/afrikakorps_tex)
            (Avalon Hill classic)
-   Modern/Speculative
    -   [Strike Force One](https://gitlab.com/wargames_tex/sfo_tex)
    -   [Kriegspiel](https://gitlab.com/wargames_tex/kriegspiel_tex)
        (Avalon Hill classic)

VASSAL support
--------------

The packages has the script [`wgexport.py`](utils/wgexport.py) to
generate a draft [VASSAL](https://vassalengine.org) module from the
defined charts, boards, counters, OOBs, and so on. More about this is
given in the documentation.

The script will generate a first draft of a module, which then can be
edited in the VASSAL editor for the final thing.

Alternatively, one can provide a Python script to do more on the
generated module, such as defining starting positions, fixing grid
layouts, and so on. Using such a patch script, one can get an (almost)
final module. This means, that even if one makes changes to the original
content, it is easy to remake the module without much need for the
VASSAL editor.

An example of this is given in the [Battle for
Moscow](https://gitlab.com/wargames_tex/bfm_tex) project, and of course
in the [tutorial](tutorial/ "Link works on GitLab only")

NATO App6
---------

The package supports

-   All air, equipment, installation, land, sea surface, sub surface,
    and space command symbols, including amplifiers and modifiers
-   All friendly, hostile, neutral, and unknown faction frame. Undecided
    faction frames can be made by specifying `dashed` line styles.
-   Some, but very few, other kinds of symbology.

Here are some references for more information on the NATO App6 symbology
standard.

-   The [Wikipedia
    page](https://en.wikipedia.org/wiki/NATO_Joint_Military_Symbology)
    on NATO Joint Military Symbology
-   The [NATO
    page](https://nso.nato.int/nso/nsdd/main/standards/ap-details/1912/EN).
    If this does not work for you, then try to go to the [standards
    page](https://nso.nato.int/nso/nsdd/main/standards) and put in and
    write in `SYMBOLOGY` in the *Document Title Contains Words* field
    and press *Start*.
-   Other LaTeX package for making NATO symbology
    [XMilSymb](https://github.com/ralphieraccoon/MilSymb)

Copyright and license
---------------------

(c) 2022 Christian Holm Christensen

This work is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License (CC-BY-SA-4.0). To view a copy of this
license, visit [CC](http://creativecommons.org/licenses/by-sa/4.0/) or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042,
USA.

### Some comments on copyright

*Caveat*: I am not a lawyer.

Note that this license applies to the `wargame` package as such. What
you generate using the package is *not* required to be licensed
according to the *ShareAlike* clause of CC-BY-SA-4.0. That is, you can
license your materials *any way you want*.

However, that copyright rights on games is not as prohibitive as you may
think (see [this thread on
BGG](https://boardgamegeek.com/thread/493249/)). What you *can*
copyright is original, artistic expression. That is, the copyrighted
work must not be a copy of something else (originality) and must be some
form of expression. One *cannot* copyright ideas, only their expression
in so far as it is *artistic* (i.e., not a trivial expression that
anyone knowledgeable within the field can do with rudimentary effort).

So you *can not* copyright your game mechanics, for example, only how
you described them. You *can not* copyright a title (but you may be able
to claim trademark on it). You *can* copyright the wording of the rules,
the graphics that you use, and so on.

This also means, that you are essentially free to make your own version
of previously published game, *as long as*

-   you do not copy existing text
-   you do not copy existing graphics
-   you respect any kind of trademark claims

However, it is advisable to contact the copyright holders of the
previously published game to avoid
[SNAFU](https://en.wikipedia.org/wiki/SNAFU). If in doubt, seek
professional help.
