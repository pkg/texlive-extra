# Tutorial on how to use the `wargame` package 

This directory contains a tutorial on how to use the `wargame` package
to make a hex'n'counter (Print'n'Play) wargame. 

It also contains information on how to make a VASSAL module from a
game. 

## The PDF and VASSAL module 
- 
  [PDF](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/tutorial.pdf?job=dist)
- [VASSAL
  module](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/doc/latex/wargame/Game.vmod?job=dist) 

## Content 

- [`game.tex`](game.tex)     The tutorial and game 
- [`game.sty`](game.sty)     Package defining most stuff for the game 
- [`export.tex`](export.tex) Export graphics to VASSAL 
- [`patch.py`](patch.py)     Patch VASSAL module 

## How to build 

To build the tutorial, simple do

	pdflatex game
	pdflatex game 
	
Alternatively, if you use some sort of LaTeX Integrated Development
Environment (IDE), load [`game.tex`](game.tex) into your IDE and run
LaTeX from there. 

To build the VASSAL module, simply do 

	pdflatex export.tex 
	`kpsewhich wgexport.py` export.pdf export.json -o Game.vmod \
		-d "Example module from LaTeX PnP game" \
		-t "LaTeX wargame tutorial" -v 0.1 \
		-p patch.py


