def patch(build,data,vmod,verbose=False):
    # We need to import the export module :-)
    import wgexport as wg

    # Get the game and last free GPID
    game = wg.get_game(build)
    gpid = int(game.getAttribute('nextPieceSlotId'))

    # Get all boards 
    maps = wg.get_maps(build)
    board = maps['Board']

    mkeys = wg.get_masskey(board,'name')
    wg.set_node_attr(mkeys['Eliminate'],icon='eliminate-icon.png')
    wg.set_node_attr(mkeys['Flip'],icon='flip-icon.png')
    
    # Get main map
    pool = maps['DeadMap']
    wg.set_node_attr(pool,icon='pool-icon.png')

    mkeys = wg.get_masskey(pool,'name')
    wg.set_node_attr(mkeys['Restore'],icon='restore-icon.png')
    
    # Get the OOB map
    oob = wg.get_chartwindows(game)['OOBs']
    wg.set_node_attr(oob,icon='oob-icon.png')
    
    

   
    
