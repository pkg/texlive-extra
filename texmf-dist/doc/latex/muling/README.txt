-----------------------------------------------------------
Class:        muling
Author:       निरंजन
Version:      0.3  (16 December, 2021)
Description:  A class file for the Department of
              Linguistics, University of Mumbai
Repository:   https://git.gnu.org.ua/muling.git
Bug tracker:  https://puszcza.gnu.org.ua/bugs/?group=muling
License:      GPLv3+, GFDLv1.3+
-----------------------------------------------------------
