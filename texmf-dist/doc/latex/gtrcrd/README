Package name: gtrcrd (Guitar chords)
Author: Riccardo Bresciani - gtrcrd@tex.nopkoguo.it

Copyright 2012-2013 Riccardo Bresciani

Description: This package is suitable to add chords to lyrics.
The main advantages of using this package is that chords are
written before the text they refer to and LaTeX takes care of
typesetting them above the lyrics in the right place: thanks to
this we avoid the task of correctly aligning chords above the
lyrics, which is tedious in the case of a monospaced font and
absolutely non-trivial in the case of variable-width fonts.

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either
version 1.3c of this license or (at your option) any
later version.
The latest version of the license is in
   http://www.latex-project.org/lppl.txt
and version 1.3c or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work consists of the files: README, gtrcrd.sty,
gtrcrd-doc.tex, gtrcrd-doc.pdf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2013/12/24: v1.1
- added \chordsbelow
- added \neolatin

2012/01/15: v1.0