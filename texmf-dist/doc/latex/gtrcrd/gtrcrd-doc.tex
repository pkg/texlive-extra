\documentclass[green,a4paper,oneside,openany,noparindent,noparskip,article,nomatter]{bookest}

\usepackage{gtrcrd}
\usepackage{hyperref}

\setleftmark{}
\setrightmark{}

%pdfLaTeX
\hypersetup{
pdftitle={gtrcrd},
pdfsubject={A LaTeX package which is suitable to add chords to lyrics.},
pdfauthor={Riccardo Bresciani},
pdfkeywords={gtrcrd, LaTeX, chords, lyrics, TeX},
pdfstartview=FitV,
%colorlinks
}


% Aliases
\newcommand{\ie}{\emph{i.e.} }

\author{Riccardo Bresciani}
\title{gtrcrd\hfill{\small\href{http://tex.nopkoguo.it/gtrcrd}{http://tex.nopkoguo.it/gtrcrd}}}
\newcommand{\belowLine}{{\normalsize Version 1.1 --- \today}}

\makeatletter
\renewcommand{\maketitle}{\thispagestyle{plain}{\Huge\textbf{\colorB\@title}\Large\\\vspace{2ex}
\@author{\colorA\hrule}\vspace{1ex}\belowLine\\\vspace{2ex}}}
\makeatother

  \geometry{
     hmargin=2.6cm,
     vmargin={3.5cm,3cm},
     bindingoffset=0mm,
     columnsep=20pt	
  }
  
  \pagestyle{plain}
  
\begin{document}

\maketitle

\begin{abstract}{Brief description of the package:}
This package is suitable to add chords to lyrics. The main advantages of using this package is that chords are written before the text they refer to and \LaTeX\ takes care of typesetting them above the lyrics in the right place: thanks to this we avoid the task of correctly aligning chords above the lyrics, which is tedious in the case of a monospaced font and absolutely non-trivial in the case of variable-width fonts.
\end{abstract}

\artsection{Usage}
\artsubsection{Chords}

The following commands typeset a chord above a given text:

\begin{center}
\ttfamily \textbackslash\A A \quad\textbackslash\B B \quad\textbackslash\C C \quad\textbackslash\D D \quad\textbackslash\E E \quad\textbackslash\F F \quad\textbackslash\G G

\textbackslash\Ab Ab \quad\textbackslash\Bb Bb \quad\textbackslash\Cb Cb \quad\textbackslash\Db Db \quad\textbackslash\Eb Eb \quad\textbackslash\Fb Fb \quad\textbackslash\Gb Gb

\textbackslash\As As \quad\textbackslash\Bs Bs \quad\textbackslash\Cs Cs \quad\textbackslash\Ds Ds \quad\textbackslash\Es Es \quad\textbackslash\Fs Fs \quad\textbackslash\Gs Gs

\textbackslash\Am Am \quad\textbackslash\Bm Bm \quad\textbackslash\Cm Cm \quad\textbackslash\Dm Dm \quad\textbackslash\Em Em \quad\textbackslash\Fm Fm \quad\textbackslash\Gm Gm

\textbackslash\Abm Abm \quad\textbackslash\Bbm Bbm \quad\textbackslash\Cbm Cbm \quad\textbackslash\Dbm Dbm \quad\textbackslash\Ebm Ebm \quad\textbackslash\Fbm Fbm \quad\textbackslash\Gbm Gbm

\textbackslash\Asm Asm \quad\textbackslash\Bsm Bsm \quad\textbackslash\Csm Csm \quad\textbackslash\Dsm Dsm \quad\textbackslash\Esm Esm \quad\textbackslash\Fsm Fsm \quad\textbackslash\Gsm Gsm
\end{center}

For example \texttt{\textbackslash Bb\{text\}} produces the following: \Bb{text}.

\ppar
Each command takes an optional argument, in order to allow users to typeset variations of the above chords.

For example \texttt{\textbackslash G[7sus4]\{text\}} produces the following: \G[7sus4]{text}.

\artsubsubsection{An Example: London Bridge is Falling Down}

\begin{tabular}{p{6cm}l}
\G London bridge is falling down &\texttt{\textbackslash G London bridge is falling down}\\

\D[7] Falling down, \G falling down. &\texttt{\textbackslash D[7] Falling down, \textbackslash G falling down.}\\

\G London bridge is falling down, &\texttt{\textbackslash G London bridge is falling down,}\\

\D my fair \G lady. &\texttt{\textbackslash D my fair \textbackslash G lady.}
\end{tabular}

\artsubsection{Different notations}\label{notations}

By default chords are typeset in English notation above the text. The default behaviour can be altered via the following commands:

\begin{itemize}
	\item \texttt{\textbackslash chordsbelow} switches to typesetting chords below the text --- for example \texttt{\textbackslash Cs\{text\}} outputs {\chordsbelow \Cs{text}} instead of \Cs{text};
	\item \texttt{\textbackslash neolatin} switches to the neo-latin notation (\emph{i.e.} Do-Re-Mi-Fa-Sol-La-Si)  --- for example \texttt{\textbackslash As\{text\}} outputs {\neolatin \As{text}} instead of \As{text}.
\end{itemize}



\artsubsection{Operations on chords}\label{opts}

It is sometimes useful to transpose a song by a certain number of half-tones. This package provides the following commands to do this:

\begin{center}\ttfamily
\begin{tabular}{p{4cm}p{4cm}p{4cm}}

\textbackslash transposeOneUp &\textbackslash transposeTwoUp &\textbackslash transposeThreeUp\\

\textbackslash transposeFourUp &\textbackslash transposeFiveUp &\textbackslash transposeSixUp\\

\textbackslash transposeOneDown &\textbackslash transposeTwoDown &\textbackslash transposeThreeDown\\

\textbackslash transposeFourDown &\textbackslash transposeFiveDown &\textbackslash transposeSixDown
\end{tabular}
\end{center}

Things can be brought back to normal via the command \texttt{\textbackslash notranspose}.

\ppar
Another possibility is to convert all chords with a $\flat$ to the corresponding chords with a $\sharp$ or vice-versa: this is achieved with the commands \texttt{\textbackslash sharponly} and \texttt{\textbackslash flatonly}.

\ppar
Finally it is possible to avoid all occurrences of E$\sharp$, F$\flat$, B$\sharp$ and C$\flat$ and replace them with F, E, C and B respectively by using the command \texttt{\textbackslash normalize}.

\artsubsection{Package Options}
All commands from \S\S\ref{notations},\ref{opts} can be run at the beginning of the document by using the desired ones as package options --- the initial backslash has to be omitted of course!

\artsubsection{Changing the Way Things Look}
The line spacing in presence of a chord is controlled by the length \texttt{\textbackslash crdheight} --- the default setting is \texttt{3ex}.

\ppar
The font of the chord is controlled by the command \texttt{\textbackslash crdfont} --- the default setting is \texttt{\textbackslash footnotesize \textbackslash sffamily}.

\ppar
All chords are specific instances of the command \texttt{\textbackslash CHORD}, which takes two mandatory arguments and an optional one: the first argument is concatenated to the optional argument (respectively the chord and its variation) and is typeset above the text given as second argument.

It is possible to change the way a chord is typeset by redefining this command --- and this does not compromise the functionality of the commands in \S\ref{opts} as long as the meaning of the arguments of \texttt{\textbackslash CHORD} is not changed.

\artsection{Contacts}
If you have comments or want to report any bug, please send a mail to \textsl{\href{mailto:gtrcrd@tex.nopkoguo.net}{gtrcrd@tex.nopkoguo.it}}.


\end{document}