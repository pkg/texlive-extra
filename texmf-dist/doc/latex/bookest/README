La classe bookest e' un'estensione della classe standard book, classe alla
quale si appoggia e che viene caricata con le opzioni di default.

Le estensioni fornite dalla classe riguardano:
1. colori;
2. layout del documento;
3. testatine e pie' di pagina;
4. layout della copertina;
5. ...

     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The bookest class in an extension of the standard book class, which it relies
on and that is loaded with the default options.

The extensions provided by the class involve:
1. colors;
2. document layout;
3. headings and footers;
4. title page layout;
5. ...

     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Version 1.1 - 2008/03/20
# Added "logo-toplcr" and "logo-toplcr-nofooter" options
# Added "article" option and "\notinarticle" command
# Added "\art*" aliases for sectioning commands
# Added "noepigraph" option and "\noepigraph" command
# Added "nomatter" option and "\nomatter" command
# "pdfcolmk" is not a required package anymore

Version 1.0.4 - 2007/05/22
# Added "abstract" environment

Version 1.0.3 - 2007/05/13
# Unused options are passed to the book class

Version 1.0.2 - 2007/05/12
# Added "\shipouttext" and "\shipoutimage" commands

Version 1.0.1 - 2007/04/27
# Added "logo-topc" and "logo-topc-nofooter" options

Version 1.0 - 2007/04/12
# First public release

     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%                                                                 %%
     %% Package name: bookest                                           %%
     %% Author: Riccardo Bresciani - bresciani(at)sssup.it              %%
     %% Web: http://tex.boris-web.net/bookest                           %%
     %%                                                                 %%
     %% Description: This package provides an extension to the          %%
     %%              book class                                         %%
     %%                                                                 %%
     %% Copyright 2007-2008 Riccardo Bresciani                          %%
     %%                                                                 %%
     %% This work may be distributed and/or modified under the          %%
     %% conditions of the LaTeX Project Public License, either          %%
     %% version 1.3c of this license or (at your option) any            %%
     %% later version.                                                  %%
     %% The latest version of the license is in                         %%
     %%    http://www.latex-project.org/lppl.txt                        %%
     %% and version 1.3c or later is part of all distributions of LaTeX %%
     %% version 2005/12/01 or later.                                    %%
     %%                                                                 %%
     %% This work has the LPPL maintenance status "author-maintained".  %%
     %%                                                                 %%
     %% This work consists of the files: README, bookest.cls,           %%
     %% bookestdoc-it.tex, bookestdoc-it.pdf, bookestdoc-en.tex,        %%
     %% bookestdoc-en.pdf                                               %%
     %%                                                                 %%
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
