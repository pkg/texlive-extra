# The exesheet class and package - Typesetting exercise or exam sheets


## Presentation

The exesheet package is used for typesetting exercise or exam sheets. 
In addition, the exesheet class loads the schooldocs package.

The package provides:
- macros to mark out exercises and subparts,
- specific settings for enumeration lists, 
- environments for questions and answers, with conditional display,
- macros for displaying a marking scheme with detailed comments in margins.


## Installation

- run LaTeX on exesheet.ins, you obtain the files exesheet.cls and exesheet.sty,
- if then you run LaTeX + dvips + ps2pdf on exesheet.dtx you get the file exesheet.pdf which is also in the archive,
- put the files exesheet.cls, exesheet.sty and exesheet.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
