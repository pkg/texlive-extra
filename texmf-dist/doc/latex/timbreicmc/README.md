# Package timbreicmc #

Typeset documents with
[ICMC/USP São Carlos-SP, Brazil](http://www.icmc.usp.br) watermarks.

Author: Miguel V. S. Frasson
[mvsfrasson@gmail.com](mailto:mvsfrasson@gmail.com)

This material is subject to the
[LaTeX Project Public License 1.3c](https://ctan.org/license/lppl1.3)

## Simple usage ##

For basic watermark of the institute, just include
``` TeX
\usepackage{timbreicmc}
```
For watermarks of the departments (`scc`, `sma`, `sme`, `ssc`) and
sectors of ICMC (`ccex`, `ci`, `cg`, `comunic`, `congreg`, `cpg`,
`cpq`, `cqp`, `cta`, `dir`, `sbab`, `scapinst`, `sti`),
include one of these acronyms in lowercase as package option, like
``` TeX
\usepackage[sme]{timbreicmc}
```

## Documentation ##

Full documentation on [timbreicmc.pdf], with ICMC watermark, in
Portuguese only, sorry :-)
