A comprehensive package for adding University of Copenhagen or
faculty logo to your front page. For use by student or staff at
University of Copenhagen / Københavns Universitet.

Use like this with inputs:
\usepackage[LANGUAGE,FACULTY]{ku-template}

Inputs:
Languages:
da, en

Faculties:
ku, jur, teo, sun, hum, sam, sci