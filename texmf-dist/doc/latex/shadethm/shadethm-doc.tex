\documentclass[pagesize=auto, fontsize=12pt, DIV=10]{scrartcl}

\usepackage{fixltx2e}
\usepackage{etex}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{microtype}
\usepackage{hyperref}
  \hypersetup{
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=blue,          % color of internal links (change box color with linkbordercolor)
    citecolor=blue,        % color of links to bibliography
    filecolor=blue,      % color of file links
    urlcolor=blue           % color of external links
  }

\newcommand*{\mail}[1]{\href{mailto:#1}{\texttt{#1}}}
\newcommand*{\pkg}[1]{\textsf{#1}}
\newcommand*{\cs}[1]{\texttt{\textbackslash#1}}
\makeatletter
\newcommand*{\cmd}[1]{\cs{\expandafter\@gobble\string#1}}
\makeatother
\newcommand*{\env}[1]{\texttt{#1}}
\newcommand*{\opt}[1]{\texttt{#1}}
\newcommand*{\meta}[1]{\textlangle\textsl{#1}\textrangle}

\renewcommand*{\labelenumi}{(\theenumi)}

\addtokomafont{title}{\rmfamily}

\title{The \pkg{shadethm} package}
\subtitle{ Shaded theorem environments in \LaTeX,\\with \cmd{\newshadetheorem} in addition to \cmd{\newtheorem}}
\author{Jim Hef{}feron\thanks{\mail{jhefferon@smcvt.edu}}}
\date{2020/01/08}


\begin{document}

\maketitle

\section{This package is obsolete}
New projects should not use this package.
There are a number of newer and more powerful packages that are available.

This material only remains available for people supporting existing
projects. 


\section{Usage}

Used as a \LaTeXe\ style, with \verb|\usepackage{shadethm}|, it allows declarations
of the form \verb|\newshadetheorem{theorem}{Theorem}|, so that
%
\begin{verbatim}
\begin{theorem}  $e=mc^2$  \end{theorem}
\end{verbatim}
%
will produce the usual theorem, only placed in a shaded box.


\minisec{Options.}

Two come in this file:
%
\begin{description}
\item[\opt{bodymargin} (default)]
  The body of the shaded text lines up with the
  margins.  Just as though you called \verb|newtheorem|, then colored later.
  Shading falls outside the margin.

\item[\opt{shademargin}]
  The shading lines up with the margins, so the body of
  shaded text comes out a little thinner.
\end{description}
%
If you call it with an option not on that list then it will look for a
file \texttt{\meta{Option\-Name}.sth} and take the parameters from there.  See the included 
files \texttt{shadein.sth} and \texttt{colored.sth}, the first of which gives theorems that are
(like \LaTeX\ quotations) indented left and  right and the second of which 
gives color shading.  


\section{Remarks}

\begin{enumerate}
\item You can of course have non-shaded environments, also.  The \cmd{\newtheorem} 
  command still works.

\item Numbering within: An prior version of this style required that you
  number within like this:
  %
\begin{verbatim}
\newshadetheorem{thm}{Theorem}
\newshadetheorem{note}[shadethm]{Note} % No longer needed!
\end{verbatim}
  %
  but this one drops that requirement.  If you didn't use the old version,
  forget about it.

% \item Uses the \pkg{graphics} package.  Look for it in the \href{http://ctan.org/}{CTAN} \TeX-archive nearest
%   you, for instance at \href{http://tug.ctan.org/}{\texttt{tug.ctan.org}}.

\item Works with \pkg{amsthm.sty}.

\item Notice that if you say
  %
\begin{verbatim}
\newshadetheorem{theorem}{Theorem}
\end{verbatim}
  %
  and then
  %
\begin{verbatim}
\newtheorem{theorem}{Theorem}
\end{verbatim}
  %
  then you will get an error message since
  the first allows you to say\\\verb|\begin{theorem} ...|, as does the second.
\end{enumerate}


\section{Installation}

Put this where your \TeX\ looks for \texttt{.sty} files. 
There are some parameters you could set the default for (try them as is, 
first).
%
\begingroup
\newcommand*{\desc}[1]{{\usekomafont{descriptionlabel}#1}\quad}%
\renewcommand*{\theenumi}{\roman{enumi}}%
\leftmargini = 1.5 \leftmargini
\begin{enumerate}
\item \label{item:i}\desc{\texttt{shadethmcolor}}
  The shading color of the background.  See the 
  documentation for the \pkg{color} package, but with a `gray' model, I~find .97
  looks good out of my printer, while a darker shade like~.92 is needed 
  to make it copy well.  (Black is~0, white is~1.)

\item \desc{\texttt{shaderulecolor}}
  The shading color of the border of the shaded box.  
  See (\ref{item:i}).  If \cmd{\shadeboxrule} is set to 0\,pt then this won't print anyway.

\item \desc{\cmd{\shadeboxrule}}
  The width of the border around the shading.  Set it to
  0\,pt (not just 0) to make it disappear.

\item \desc{\cmd{\shadeboxsep}}
  The length by which the shade box surrounds the text.

\item \desc{\cmd{\shadesetinsideminipage}}
  The theorems are set inside a \env{minipage} to
  get line breaks, but some things, like paragraph indents, are messed 
  with there, so I~have a hook to reset them.  Probably you can just
  leave this alone.

\item The default option.  Change the \verb|\ExecuteOptions{...}| right before
  the \cmd{\ProcessOptions}.
\end{enumerate}
\endgroup


\minisec{Comment:}

If you get an error about `undefined color model' then you need to create
the file \texttt{color.cfg} in the same place as \texttt{color.sty}, and put in it the line
%
\begin{verbatim}
\ExecuteOptions{dvips}
\end{verbatim}
%
(or~whatever is the name of your driver).  See the
documentation for the \pkg{graphics} package.  I~needed this in the distant past
under N\TeX.


\section{Bugs}

Note that this material is obsolete.
The bug listed will not be fixed.

\begin{enumerate}
\item Page breaks are a problem since it sets the theorem before it is shaded.
  The theorem is put into a \env{minipage} in order to have the right line breaks
  put in, but then can't be broken across pages.
\end{enumerate}


\section{License}

Copyright 1999-2020 Jim Hefferon \mail{jhefferon@smcvt.edu}.

This program can redistributed and/or modified under the terms
of the \LaTeX\ Project Public License Distributed from CTAN
archives in directory \href{http://ctan.org/macros/latex/base/lppl.txt}{\nolinkurl{macros/latex/base/lppl.txt}}; either
version~1 of the License, or (at your option) any later version.

\clearpage


\section{History}

\begin{description}
\item[\texttt{20I08} jh \mail{jhefferon@smcvt.edu}] Declare package obsolete to keep people from mistakenly starting new projects with it.
\item[\texttt{99XI23}  jh] Redid the parsing to stick to \cmd{\newcommand}-style calls.  Needed
  to put in some oppresive \cmd{\expandafter} strings.
\item[\texttt{99VII09} jh \mail{tex@joshua.smcvt.edu}] Fixed the number within situation.  LPPL 
  added.
\item[\texttt{97I07} jh] Changed the interior of the \cmd{\DeclareOptions} to \cmd{\AtBeginDocument}
  for cases where the \cmd{\textwidth} is changed.
\item[\texttt{95VI19} jh] Adapted for \LaTeXe.  Now uses the \pkg{graphics} package, and all
  the neat \LaTeXe\ package stuff.  Still haven't rewritten the parsing code to
  use the new optional arguments to \cmd{\newcommand}, though.  Ah well.
\item[\texttt{94VII11} jim hefferon \mail{hefferon@smcvax.smcvt.edu}.]  First written (after lots
  of sporadic tries, over months; I couldn't get any of the extant shading 
  macros to work with theorems) from a hint in TvZ's \texttt{Fancybox} writeup. 
  It's all so easy once you know how.
\end{description}


\end{document}
