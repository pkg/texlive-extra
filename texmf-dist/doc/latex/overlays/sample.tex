\documentclass[fontsize=9pt,landscape,parskip=half]{scrartcl}
\usepackage{scrlayer-scrpage}
\usepackage[paperwidth=128mm,
            paperheight=96mm,
            margin=8mm,
            footskip=6mm]{geometry}
\usepackage{overlays}
\usepackage[breaklinks,hidelinks]{hyperref}
\usepackage{doc}
\usepackage{alltt}
\lofoot{\hspace{-4mm}%
        \hyperlink{titlepage}{Andreas Nolda \textbar{}
                              The \textrm{overlays} package} \textbar{}
        \hyperlink{page.\thepage}{\thepage}/%
        \pageref{lastpage}}
\cofoot{}
\renewcommand{\familydefault}{\sfdefault}
\definecolor{alert}{rgb}{0.7,0.15,0.35}
\definecolor{structure}{rgb}{0.1,0.25,0.5}
\setkomafont{pagefoot}{\color{structure}\normalfont\bfseries\scriptsize}
\addtokomafont{sectioning}{\color{structure}}
\frenchspacing
\sloppy

\begin{document}
\pagestyle{scrheadings}
\raggedright

\thispagestyle{empty}
\label{titlepage}
{\color{structure}%
{\Large \textbf{The \textrm{overlays} package} \\
\textit{A sample presentation}} \\[\baselineskip]
Andreas Nolda \\
\url{https://andreas.nolda.org}}\vfill
{\small Version 2.12 \\
23 February 2021}
\clearpage

\begin{fragileoverlays}{6}
\subsubsection*{Overlays}
The \textrm{overlays} package allows to write presentations with incremental
slides. It does not presuppose any specific document class. Rather, it is a
lightweight alternative to full-fledged presentation classes like
\href{http://www.ctan.org/pkg/beamer}{\textrm{beamer}}.

\visible{2-}{Every single state of a incremental slide will called an
\emph{overlay} of that slide.}

\visible{3-}{For incremental slides, the following environment is provided:}
\begin{quote}
\begin{alltt}
\visible{3-}{\alert{3}{\textbackslash{}begin\{overlays\}\{\meta{total overlay number}\}
\alert{4-}{\meta{slide content}}
\textbackslash{}end\{overlays\}}}
\end{alltt}
\end{quote}

\visible{4-}{In the slide content, the following commands can be used in order
to specify the content of the overlays:
\only{4}{\texttt{\textbackslash{}alert}.}%
\only{5}{\texttt{\textbackslash{}visible}.}%
\only{6}{\texttt{\textbackslash{}only}.}}
\end{fragileoverlays}
\clearpage

% Slide with verbatim content.

\begin{fragileoverlays}{6}
\subsubsection*{Highlightning}
For highlightning some content, the \textrm{overlays} package provides
following command:
\begin{quote}
\begin{alltt}
\alert{1}{\textbackslash{}alert\{\alert{2-5}{\meta{overlay specification}}\}\{\meta{content}\}}
\end{alltt}
\end{quote}

\visible{2-}{Overlay specifications are either
\alert{3}{single numbers},
\alert{4}{sequences of numbers}, or
\alert{5}{ranges of numbers}.
\visible{3-}{For example:
\begin{itemize}
\item{} \alert{3}{\texttt{1}}
\visible{4-}{\item{} \alert{4}{\texttt{1,4}}}
\visible{5-}{\item{} \alert{5}{\texttt{1-4}}
\item{} \alert{5}{\texttt{1-}}}
\end{itemize}}}

\visible{6}{The \texttt{alert} color is red by default and can be changed to,
say, \alert{6}{magenta} as follows:
\begin{quote}
\begin{alltt}
\textbackslash{}definecolor\{alert\}\{rgb\}\{\alert{6}{0.7,0.15,0.35}\}
\end{alltt}
\end{quote}}
\end{fragileoverlays}
\clearpage

\begin{fragileoverlays}{5}
\subsubsection*{Visibility}
The visibility of content is specified by means of the following commands:
\begin{quote}
\begin{alltt}
\alert{1-3}{\textbackslash{}visible\{\meta{overlay specification}\}\{\meta{content}\}}
\end{alltt}
\end{quote}
\begin{quote}
\begin{alltt}
\alert{1,4-}{\textbackslash{}only\{\meta{overlay specification}\}\{\meta{content}\}}
\end{alltt}
\end{quote}

\only{2,3}{The \texttt{\textbackslash{}visible} command uncovers its content on
the overlays which are specified in the overlay specification.}

\only{3}{On unspecified overlays, the content is hidden, but still takes up
space. Technically speaking, it is rendered in the \texttt{background} color,
which, by default, is white.}%
\only{4-}{
The \texttt{\textbackslash{}only} command also uncovers its content on the
overlays specified in the overlay specification.

The content is absent from unspecified overlays and does not take up space
there.}

\only{5}{This is particularly useful for alternating content.}
\end{fragileoverlays}
\clearpage

\begin{fragileoverlays}{3}
\subsubsection*{Verbatim content}
For incremental slides with verbatim content, the following environment should
be used instead of the \texttt{overlays} environment:
\begin{quote}
\begin{alltt}
\alert{1}{\textbackslash{}begin\{fragileoverlays\}\{\meta{total overlay number}\}
\meta{slide content}
\textbackslash{}end\{fragileoverlays\}}
\end{alltt}
\end{quote}

\visible{2-}{Note that the \alert{2,3}{\texttt{\textbackslash{}alert}},
\alert{2,3}{\texttt{\textbackslash{}visible}}, and
\alert{2,3}{\texttt{\textbackslash{}only}} commands themselves must not contain
verbatim commands or environments.\visible{3}{ They may be used in the content
of the \texttt{alltt} environment of the
\href{http://www.ctan.org/pkg/alltt}{\textrm{alltt} package}, though.}}
\end{fragileoverlays}
\clearpage

\begin{overlays}{3}
\subsubsection*{Caveats}
The package expects that the slide content in the \alert{1}{\texttt{overlays}}
environment fits on a single page. This can be ensured by means of
\texttt{\textbackslash{}clearpage} commands before or after the environment.

\visible{2-}{The \texttt{page} and \texttt{equation} counters are not
incremented between overlays. Other counters can be saved between overlays, too,
by means of the command
\alert{2}{\texttt{\textbackslash{savecounterbetweenoverlays\{\meta{counter name}\}}}}.}

\visible{3-}{Series of first-level lists specified by the \texttt{series} and
\texttt{resume} keys of the \textrm{enumitem} package can be saved between
overlays by means of the command
\alert{3}{\texttt{\textbackslash{saveseriesbetweenoverlays\{\meta{series
name}\}}}}, provided that the series name matches the basename of the list
counter (i.e. the counter name without the final \texttt{i}).}
\end{overlays}
\clearpage

\begin{overlays}{4}
\subsubsection*{Further caveats}
\visible{1-}{In the \texttt{tabular} environment, the
\alert{1}{\texttt{\textbackslash{}alert}},
\alert{2}{\texttt{\textbackslash{}visible}} and
\alert{3}{\texttt{\textbackslash{}only}} commands have to be put into braces:
\begin{quote}
\begin{tabular}[t]{ll}
outside \texttt{tabular} & inside \texttt{tabular} \\\hline
{\alert{1}{\texttt{\textbackslash{}alert\{\ldots\}\{\ldots\}}}} &
{\alert{1}{\texttt{\{\textbackslash{}alert\{\ldots\}\{\ldots\}\}}}} \\
{\visible{2-}{{\alert{2}{\texttt{\textbackslash{}visible\{\ldots\}\{\ldots\}}}}}} &
{\visible{2-}{{\alert{2}{\texttt{\{\textbackslash{}visible\{\ldots\}\{\ldots\}\}}}}}} \\
{\visible{3-}{{\alert{3}{\texttt{\textbackslash{}only\{\ldots\}\{\ldots\}}}}}} &
{\visible{3-}{{\alert{3}{\texttt{\{\textbackslash{}only\{\ldots\}\{\ldots\}\}}}}}}
\end{tabular}
\end{quote}}
\visible{4}{In addition, make sure not to include the cell delimiter
\texttt{\&} or the row delimiter \texttt{\textbackslash{}\textbackslash{}} into
the content of these commands.}
\end{overlays}
\clearpage

\begin{overlays}{3}
\subsubsection*{Non-interactive versions}
Overlay processing can be switched off by means of the command
\alert{1}{\texttt{\textbackslash{}overlaysoff}}. If used in the preamble, this
command typesets only the last overlay of each incremental slide in the
presentation.

\visible{2-}{Similarly, the command
\alert{2}{\texttt{\textbackslash{}alertsoff}} removes highlights in
\texttt{alert} color.}

\visible{3}{Both commands may be useful for non-interactive versions of a
presentation, such as a presentation provided as a download.}
\end{overlays}
\clearpage

\begin{overlays}{3}
\subsubsection*{Credits}
The code of the \textrm{overlays} package is inspired by Matthias Meister's
\href{http://www.ctan.org/pkg/present}{\textrm{present} package}.

\visible{2-}{In addition, it uses an algorithm by Martin Scharrer for testing
numbers in numerical ranges (cf. \url{http://tex.stackexchange.com/q/19000}).}

\visible{3}{The code for saving counters between overlays as well as the code
for overlays with verbatim content is taken from the
\href{http://www.ctan.org/pkg/texpower}{\textrm{texpower} package}, which in
turn is based on Till Tantau's
\href{http://www.ctan.org/pkg/beamer}{\textrm{beamer} package}.}
\end{overlays}
\label{lastpage}
\end{document}
