2021-02-23:
 * "\alertsoff" command for switching alerts off
 * bump version to 2.12

2021-02-22:
 * "\overlaysoff" command for typesetting the last overlay only
 * bump version to 2.11

2018-01-15:
 * bug fix for "tabular"-style environments

2017-12-21:
 * support the "graphics" package in addition to the "graphicx" package
 * bump version to 2.10

2017-12-20:
 * hide included images in the argument of the "\visible" command
 * bump version to 2.9

2017-11-19:
 * "\psvisible" and "\psalert" commands for PSTricks-only effects
 * bump version to 2.8

2017-03-08:
 * don't ignore spaces in the arguments of the "\visible" and "\alert" commands
 * bump version to 2.7

2017-03-05:
 * blend all colors in the content of the "\visible" command with the background
   color on non-specified overlays
 * require the "xcolor" package instead of the "color" package
 * bump version to 2.6

2017-03-04:
 * "\saveseriesbetweenoverlays" command for enumitem series not to be
   incremented between overlays (currently restricted to the first level of
   cloned or uncloned lists with a series name matching the basename of the
   list counter)
 * rename "\savebetweenoverlays" to "\savecountersbetweenoverlays" (preserving
   "\savebetweenoverlays" as a shortcut for backward compatibility)
 * bump version to 2.5

2016-10-24:
 * if PSTricks is loaded, set linecolor to background or alert color
 * bump version to 2.4

2016-10-04:
 * ensure side-effects (such as incremented counters) of hidden content of the
   "\only" command
 * bump version to 2.3

2016-10-02:
 * "\savebetweenoverlays" command for counters not to be incremented between
   overlays
 * bump version to 2.2

2016-10-01:
 * don't increment the "equation" counter between overlays
 * bump version to 2.1

2016-09-30:
 * "fragileoverlays" environment, providing partial support for verbatim content
 * bump version to 2.0

2016-09-28:
 * initial release
