\documentclass[a4paper]{ltxdoc}
\usepackage[breaklinks,hidelinks]{hyperref}
\usepackage{polyglossia}
\setdefaultlanguage{english}
\setlength{\leftmargini}{\parindent}
\frenchspacing
\sloppy

\begin{document}
\title{\textsf{overlays} -- incremental slides}
\author{Andreas Nolda}
\date{2021/02/23 (v.\,2.12)}
\maketitle

\noindent The \textsf{overlay} package allows to write presentations with
incremental slides. It does not presuppose any specific document class. Rather,
it is a lightweight alternative to full-fledged presentation classes like
\textsf{beamer}.

In the following, every single state of a incremental slide will be called an
\emph{overlay} of that slide.

\section{User interface}

\DescribeMacro{overlays}
\DescribeMacro{fragileoverlays}
For incremental slides, the following environments are provided:
\begin{quote}
|\begin{overlays}{|\meta{total overlay number}|}| \\
\meta{slide content} \\
|\end{overlays}|
\end{quote}
\begin{quote}
|\begin{fragileoverlays}{|\meta{total overlay number}|}| \\
\meta{slide content} \\
|\end{fragileoverlays}|
\end{quote}
The |fragileoverlays| environment should be used for incremental slides with
verbatim content.

Note that the slide content should fit on a single page.

\DescribeMacro{\alert}
\DescribeMacro{\visible}
\DescribeMacro{\only}
In the slide content, the following commands can be used in order
to specify the content of the overlays:
\begin{quote}
|\alert{|\meta{overlay specification}|}{|\meta{content}|}|
\end{quote}
\begin{quote}
|\visible{|\meta{overlay specification}|}{|\meta{content}|}|
\end{quote}
\begin{quote}
|\only{|\meta{overlay specification}|}{|\meta{content}|}|
\end{quote} Overlay specifications are either single numbers like |1|, sequences
of numbers like |1,4|, or ranges of numbers like |1-4| or |1-|.

The |\alert| command highlights its content on the specified overlays with the
|alert| color, which may be redefined by means of the |\definecolor| command.

The |\visible| command uncovers its content on the overlays which are specified
in the overlay specification. On unspecified overlays, the content still takes
up space, being rendered in the |background| color, which, by default, is white.

The content of the |\only| command is also uncovered on the overlays specified
in the overlay specification, but is absent from unspecified overlays and does
not take up space there.

\pagebreak
\DescribeMacro{\savecountersbetweenoverlays}
The |page| and |equation| counters are not incremented between overlays. Other
counters can be saved between overlays, too, by means of the following command:
\begin{quote}
|\savecountersbetweenoverlays{|\meta{counter name}|}|
\end{quote}
\DescribeMacro{\savebetweenoverlays}
The |\savebetweenoverlays| command is provided as a shortcut for backward
compatibility.

\hyphenation{enum-item}
\DescribeMacro{\saveseriesbetweenoverlays}
Series of first-level lists specified by the |series| and |resume| keys of the
\textsf{enumitem} package can be saved between overlays by means of the
following command:
\begin{quote}
|\saveseriesbetweenoverlays{|\meta{series name}|}|
\end{quote}
See below for known limitations regarding the series name.

The |\savecountersbetweenoverlays| and |\saveseriesbetweenoverlays| commands
are typically used in the preamble.

\DescribeMacro{\overlaysoff}
\DescribeMacro{\alertsoff}
Use the |\overlaysoff| command in the preamble in order to switch overlay
processing off, effectively typesetting only the last overlay of each
incremental slide. In addition, highlights in |alert| color can be switched off
by means of the |\alertsoff| command.

\section{Known limitations}

The |\alert|, |\visible|, and |\only| commands must not contain verbatim
commands or environments. They may be used in the content of the |alltt|
environment, though.

The |\saveseriesbetweenoverlays| command only supports first-level lists. In
addition, it requires that the series name matches the basename of the list
counter (i.e. the counter name without the final |i|). In the case of the
|enumerate| list, the series name thus should be |enum|; in the case of lists
cloned by means of the |\newlist| command, the series name should be identical
to the list name.

\section{Dependencies}

The \textsf{overlays} package requires the following packages: \textsf{xcolor},
\textsf{environ}, and \textsf{pgffor} (part of \textsf{pgf}).

The \textsf{overlays} package enables support for the \textsf{graphics} and
\textsf{graphicx} packages if one of them is loaded before the \textsf{overlays}
package.

\DescribeMacro{\psalert}
\DescribeMacro{\psvisible}
Similarly, the \textsf{overlays} package enables support for the
\textsf{pstricks} package if the latter is loaded before the former. In
particular, the commands |\psalert| and |\psvisible| are provided for
highlighting or uncovering lines only.

\section{Credits}

The code of the \textsf{overlays} package is inspired by Matthias Meister's
\textsf{present} package.

In addition, it uses an algorithm by Martin Scharrer for testing numbers in
numerical ranges (cf. \url{http://tex.stackexchange.com/q/19000}).

The code for saving counters between overlays as well as the code for overlays
with verbatim content is taken from the \textsf{texpower} package, which in turn
is based on Till Tantau's \textsf{beamer} package.
\end{document}
