# Identkey

## Usage

    \begin{key}
      \lead Flowers zygomorphic \ident{Senna}
      \lead Flowers actinomorphic \goto{2}

      \lead Corolla inconspicuous, flowers arranged in dense head or spikes
        \ident{Acacia}
      \lead Corolla conspicuous, flowers not arranged in dense heads or spikes
        \ident{Gleditsia}
    \end{key}
