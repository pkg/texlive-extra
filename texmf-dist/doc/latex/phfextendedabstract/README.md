# The phfextendedabstract package

Typeset extended abstracts for conferences, such as often encountered in quantum
information theory.


# Documentation

Run `make sty` to generate the style file, `make pdf` to generate the package
documentation, and `make install` to install the package in your local texmf
tree. Run 'make' or 'make help' for more info.


# Author and License

(C) 2021 Philippe Faist, philippe.faist@bluewin.ch

License: [LaTeX project public license](http://www.ctan.org/license/lppl1.3),
version 1.3 or above
