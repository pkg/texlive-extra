File README.txt for package euclideangeometry
        [2021-10-04 v.0.2.1 Extension package for curve2e]


This work is "maintained"

This work consists of files:
1) euclideangeometry.dtx, and the derived files
      euclideangeometry.sty  and euclideangeometry.pdf,
2) euclideangeometry-man.tex and the derived file
      euclideangeometry-man.pdf,
3) README.txt

euclideangeometry.dtx is the documented TeX source file of package
euclideangeometry.sty obtained by running pdflatex on euclideangeometry.dtx
euclideangeometry.pdf obtained by running pdflatex on euclideangeometry.dtx

README.txt, this file, contains general information.

euclideangeometry-man.tex and euclideangeometry-man.pdf are
the source file and the readable document containing the
end-user manual.

In other words euclideangeometry.pdf is oriented towards the
developers and euclideangeometry-man.pdf to the end-users.

Claudio Beccari

claudio dot beccari at gmail dot com
