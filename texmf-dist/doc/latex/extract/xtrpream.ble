%%
%% This is file `xtrpream.ble',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% extract.dtx  (with options: `preamble')
%% 
%% ---------------------------------------
%% Copyright (C) 2004-2019 Hendri Adriaens
%% ---------------------------------------
%%
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2003/12/01 or later.
%%
%% This work has the LPPL maintenance status "maintained".
%%
%% This Current Maintainer of this work is Hendri Adriaens.
%%
%% This work consists of the file extract.dtx and the derived files
%% extract.sty, xtrex1.tex, xtrex2.tex, xtrex3.tex, xtrex4.tex,
%% xtrex5.tex, xtrex6.tex and xtrex7.tex.
%%
%% The following files constitute the extract package and must be
%% distributed as a whole: readme, extract.dtx, extract.pdf and
%% extract.sty.
%%
\usepackage{url}
\usepackage{extract}
\usepackage{xcolor}
\usepackage{fourier}
\usepackage{varioref}
\usepackage{pst-text}
\def\reftextafter{on page~\thevpagerefnum}
\def\reftextbefore{on page~\thevpagerefnum}
\def\reftextfaceafter{on page~\thevpagerefnum}
\def\reftextfacebefore{on page~\thevpagerefnum}
\usepackage{listings}
\lstnewenvironment{command}{%
  \lstset{columns=flexible,frame=single,backgroundcolor=\color{blue!20},%
    xleftmargin=\fboxsep,xrightmargin=\fboxsep,escapeinside=`',gobble=1}}{}
\lstnewenvironment{example}{%
  \lstset{basicstyle=\footnotesize\ttfamily,columns=flexible,frame=single,%
    backgroundcolor=\color{yellow!20},xleftmargin=\fboxsep,%
    xrightmargin=\fboxsep,gobble=1}}{}
\def\mktitledecor{%
  \rput[tl]{90}(-6.5,-22.56){%
    \psline[linewidth=1pt](0,1.5)(\paperheight,1.5)%
    \rput[lB](.075\paperheight,.5){\pscharpath[linecolor=blue!50,%
      fillcolor=yellow!20,fillstyle=solid,linewidth=.5pt]%
      {\Huge\bfseries\sffamily extract}%
    }%
    \rput[rB](.925\paperheight,.5){\pscharpath[linecolor=blue!50,%
      fillcolor=yellow!20,fillstyle=solid,linewidth=.5pt]%
      {\Huge\bfseries Documentation}%
    }%
    \psline[linewidth=1pt](0,0)(\paperheight,0)%
  }%
}
\usepackage{float}
\newfloat{Listing}{htb}{loe}
\makeatletter
\def\tableofcontents{%
  \begin{multicols}{2}%
  [\section*{Contents}%
  \setlength{\columnseprule}{.4pt}%
  \setlength{\columnsep}{18pt}]%
  \@starttoc{toc}%
  \end{multicols}%
}
\def\changes@#1#2#3{%
  \protected@edef\@tempa{%
    \noexpand\glossary{\textbf{#1}\hfill\emph{(#2)}%
      \levelchar
      \ifx\saved@macroname\@empty
        \space\actualchar\generalname
      \else
        \expandafter\@gobble\saved@macroname
        \actualchar\string\verb\quotechar*%
        \verbatimchar\saved@macroname\verbatimchar
      \fi
      :\levelchar #3}%
    }%
  \@tempa\endgroup\@esphack
}
\def\DescribeMacros{\leavevmode\@bsphack
  \begingroup\MakePrivateLetters\Describe@Macros}
\def\Describe@Macros#1{\endgroup\strut
  \marginpar{\raggedleft
  \def\@tempa{#1}\count@\z@
  \@for\@tempa:=\@tempa\do{%
    \ifnum\count@>\z@\\\fi\advance\count@\@ne
    \MacroFont\expandafter\string\@tempa
    \expandafter\SpecialUsageIndex\expandafter{\@tempa}%
  }}%
  \@esphack\ignorespaces
}
\def\DescribeOption#1{\leavevmode\@bsphack
              \marginpar{\raggedleft\PrintDescribeOption{#1}}%
              \SpecialOptionIndex{#1}\@esphack\ignorespaces}
\def\PrintDescribeOption#1{\strut\emph{option}\\\MacroFont #1\ }
\def\SpecialOptionIndex#1{\@bsphack
    \index{#1\actualchar{\protect\ttfamily#1}
           (option)\encapchar usage}%
    \index{options:\levelchar#1\actualchar{\protect\ttfamily#1}\encapchar
           usage}\@esphack}
\def\DescribeOptions#1{\leavevmode\@bsphack
  \marginpar{\raggedleft\strut\emph{options}%
  \@for\@tempa:=#1\do{%
    \\\strut\MacroFont\@tempa\SpecialOptionIndex\@tempa
  }}\@esphack\ignorespaces}
\def\DescribeEnv#1{\leavevmode\@bsphack
              \marginpar{\raggedleft\PrintDescribeEnv{#1}}%
              \SpecialEnvIndex{#1}\@esphack\ignorespaces}
\def\PrintDescribeEnv#1{\strut\emph{environment}\\\MacroFont #1\ }
\def\SpecialEnvIndex#1{\@bsphack
    \index{#1\actualchar{\protect\ttfamily#1}
           (environment)\encapchar usage}%
    \index{environments:\levelchar#1\actualchar{\protect\ttfamily#1}\encapchar
           usage}\@esphack}
\def\SpecialMainEnvIndex#1{\@bsphack\special@index{%
                                      #1\actualchar
                                      {\string\ttfamily\space#1}
                                      \encapchar main}%
    \special@index{environments:\levelchar#1\actualchar{%
                   \string\ttfamily\space#1}\encapchar
           main}\@esphack}
\def\DescribeEnvs#1{\leavevmode\@bsphack
  \marginpar{\raggedleft\strut\emph{environments}%
  \@for\@tempa:=#1\do{%
    \\\strut\MacroFont\@tempa\SpecialEnvIndex\@tempa
  }}\@esphack\ignorespaces}
\renewenvironment{theglossary}{%
  \glossary@prologue
  \GlossaryParms \let\item\@idxitem \ignorespaces
  }{}%
\makeatother
\def\PrintChangesX{%
  \begingroup
    \let\efill\relax
    \PrintChanges
  \endgroup
}
\def\PrintIndexX{%
  \begingroup
    \setcounter{IndexColumns}{2}
    \setlength{\columnsep}{18pt}%
    \setlength{\columnseprule}{.4pt}%
    \PrintIndex
  \endgroup
}
\def\pf#1{\textsf{#1}}
\def\descriptionlabel{\hspace\labelsep\normalfont}
\EnableCrossrefs
\RecordChanges
\GlossaryPrologue{\section*{Version history}}
\CodelineIndex
\endinput
%%
%% End of file `xtrpream.ble'.
