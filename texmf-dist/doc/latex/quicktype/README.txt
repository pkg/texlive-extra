quicktype

v.0.1 (28th September 2016)

License: lppl1.3

Package Author: Xandru Mifsud

Intended for the quick typesetting of basic documents using LaTeX using shortcuts to existing commands and specific commands for quick formatting.

In order to use the package, \usepackage{quicktype} is required in the preamble, and quicktype.sty must be present in the necessary directory.

For further information, see Quicktype.pdf

Regards,
Xandru