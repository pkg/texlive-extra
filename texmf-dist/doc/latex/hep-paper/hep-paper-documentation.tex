%%
%% This is file `hep-paper-documentation.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% hep-paper-implementation.dtx  (with options: `documentation')
%% This is a generated file.
%% Copyright (C) 2019-2022 by Jan Hajer
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. The latest version of this license is in:
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
\ProvidesFile{hep-paper-documentation.tex}[2022/11/01 v2.1 HEP-Paper documentation]
\RequirePackage[l2tabu, orthodox]{nag}
\documentclass{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\AtBeginDocument{\MakeShortVerb{\"}}

\EnableCrossrefs
\CodelineIndex
\RecordChanges

\usepackage[parskip]{hep-paper}

\bibliography{bibliography}

\acronym{PDF}{portable document format}
\acronym{LM}{latin modern}

\usepackage{hologo}

\MacroIndent=1.5em
\AtBeginEnvironment{macrocode}{\renewcommand{\ttdefault}{clmt}}

\GetFileInfo{hep-paper.sty}

\title{The \software{hep-paper} package\thanks{This document corresponds to \software{hep-paper}~\fileversion.}}
\subtitle{Publications in high energy physics}
\author{Jan Hajer \email{jan.hajer@tecnico.ulisboa.pt}}
\date{\filedate}

\begin{document}

\maketitle

\begin{abstract}
The \software{hep-paper} package aims to provide a single style file containing most configurations and macros necessary to write appealing publications in High Energy Physics.
Instead of reinventing the wheel by introducing newly created macros \software{hep-paper} preferably loads third party packages.
\end{abstract}

\tableofcontents\clearpage

\newgeometry{vscale=.8, vmarginratio=3:4, includeheadfoot, left=11em, marginparwidth=4.6cm, marginparsep=3mm, right=7em}

\section{Introduction}

For usual publications it is enough to load additionally to the "article" class without optional arguments only the \software{hep-paper} package \cite{hep-paper}.
\begin{verbatim}
\documentclass{article}
\usepackage{hep-paper}
\end{verbatim}
The most notable changes after loading the \software{hep-paper} package is the change of some \hologo{LaTeX} defaults.
The paper and font sizes are set to A4 and \unit[11]{pt}, respectively.
Additionally, the paper geometry is adjusted using the \software{geometry} package \cite{geometry}.
Furthermore, the font is changed to \LM using the \software{hep-font} package \cite{hep-font}.
Finally, \PDF hyperlinks are implemented with the \software{hyperref} package \cite{hyperref}.

\subsection{Options}

\DescribeMacro{paper}
The "paper="\meta{format} option loads the specified paper format.
The possible \meta{formats} are:
"a0", "a1", "a2", "a3", "a4", "a5", "a6",
"b0", "b1", "b2", "b3", "b4", "b5", "b6",
"c0", "c1", "c2", "c3", "c4", "c5", "c6",
"ansia", "ansib", "ansic", "ansid", "ansie",
"letter", "executive", "legal".
The default is "a4".

\DescribeMacro{font}
The "font="\meta{size} option loads the specified font size.
The possible \meta{sizes} are:
"8pt", "9pt", "10pt", "11pt", "12pt", "14pt", "17pt", "20pt".
The default is \unit[11]{pt}.

\DescribeMacro{lang}
The "lang"=\meta{name} option switches the document language.
The default is "british".

\DescribeMacro{sansserif}
The "sansserif" option switches the document including math to sans serif font shape.

\DescribeMacro{oldstyle}
The "oldstyle" option activates the use of oldstyle text- (\texto{123}) in favour of lining- (\textl{123}) figures in text mode.

\DescribeMacro{parskip}
The "parskip" option changes how paragraphs are separated from each other using the \software{parskip} package \cite{parskip}.
The \hologo{LaTeX} default is separation via indentation the "parskip" option switches to separation via vertical space.
\footnote{Although the "parskip" option is used for this document, it is recommended only for very few document types such as technical manuals or answers to referees.}

\DescribeMacro{symbols}
The "symbols"=\meta{family} is passed to the \software{hep-math-font} package \cite{hep-math-font} and sets the family of the symbol fonts.
"symbols=false" deactivates loading any additional symbol fonts.

\subsubsection{Deactivation}

The \software{hep-paper} package loads few bigger packages which have a large impact on the document.
The deactivation options can prevent such and other adjustments.

\DescribeMacro{defaults}
The "defaults" option prevents the adjustment of the page geometry and the font size set by the document class.

\DescribeMacro{title}
The "title=false" option deactivates the title page adjustments.

\DescribeMacro{bibliography}
The "bibliography"=\meta{key} option prevents the automatic loading of the \software{hep-bibliography} package \cite{hep-bibliography} if \meta{key}="false".

\DescribeMacro{glossaries}
The "glossaries=false" option deactivates acronyms and the use of the \software{hep-acronym} package \cite{hep-acronym}.

\DescribeMacro{references}
The "references=false" option prevents the \software{cleveref} package \cite{cleveref} from being loaded and deactivates further redefinitions of reference macros.

\subsubsection{Compatibility}

The compatibility options activate the compatibility mode for certain classes and packages used for publications in high energy physics.
They are mostly suitable combinations of options described in the previous section.
If \software{hep-paper} is able to detect the presence of such a class or package, \ie if it is loaded before the \software{hep-paper} package, the compatibility mode is activated automatically.

\DescribeMacro{beamer}
The "beamer" option activates the \software{beamer} \cite{beamer} compatibility mode.

\DescribeMacro{jhep}
The "jhep" option activates the \software{JHEP} \cite{jhep} compatibility mode.

\DescribeMacro{jcap}
The "jcap" option activates the \software{JCAP} \cite{jcap} compatibility mode.

\DescribeMacro{revtex}
The "revtex" option activates the REV\hologo{TeX} \cite{revtex} compatibility mode.

\DescribeMacro{pos}
The "pos" option activates the \software{PoS} compatibility mode.

\DescribeMacro{springer}
The "springer" option activates the compatibility mode the "svjour" class \cite{svjour}.

\subsubsection{Reactivation}

The \software{hep-paper} package deactivates unrecommended macros, which can be reactivated manually.

\DescribeMacro{manualplacement}
The "manualplacement" option reactivates manual float placement.

\DescribeMacro{eqnarray}
The "eqnarray" option reactivates the depreciated "eqnarray" environment.

\section{Macros and environments}

\DescribeMacro{twocolumn}
\DescribeMacro{abstract*}
If the global "twocolumn" option is present the page geometry is changed to cover almost the entire page.
Additionally the "abstract*" environment is defined that generates a one column abstract and takes care of placing the title information.

\subsection{Title page} \label{sec:title}

\DescribeMacro{\series}
The "\series"\marg{series} macro is defined using the \software{hep-title} package \cite{hep-title}.

\DescribeMacro{\title}
The \PDF meta information is set according to the "\title"\marg{text} and "\author"\marg{text} information.

\DescribeMacro{\subtitle}
The "\subtitle"\marg{subtitle} macro is defined.

\DescribeMacro{\editor}
\DescribeMacro{\author}
\DescribeMacro{\affiliation}
\DescribeMacro{\email}
The following lines add \eg two authors with different affiliations
\begin{verbatim}
\author[1]{Author one \email{email one}}
\affiliation[1]{Affiliation one}
\author[2]{Author two \email{email two}}
\affiliation[1,2]{Affiliation two}
\end{verbatim}

\DescribeMacro{\preprint}
The "\preprint"\marg{numer} macro places a pre-print number in the upper right corner of the title page.

\DescribeEnv{abstract}
The "abstract" environment is adjusted to not start with an indentation.

\DescribeMacro{\titlefont}
\DescribeMacro{\subtitlefont}
\DescribeMacro{\authorfont}
\DescribeMacro{\affiliationfont}
\DescribeMacro{\preprintfont}
Various title font macros are defined, allowing to change the appearance of the "\maketitle" output.

\subsection{Text}

\DescribeMacro{inlinelist}
\DescribeMacro{enumdescript}
The "inlinelist" and "enumdescript" environments are defined.

\DescribeMacro{\textsc}
A bold versions \textbf{\textsc{Small Caps}} and a sans serif version of \textsf{\textsc{Small Caps}} is provided.

\DescribeMacro{\underline}
\DescribeMacro{\overline}
The "\underline" macro is redefined to allow line-breaks.
The "\overline" macro is extended to also \overline{overline} text outside of math environments.

\DescribeMacro{\useparskip}
\DescribeMacro{\useparindent}
If the "parskip" option is activated the "\useparindent" macro switches to the usual parindent mode, while the "\useparskip" macro switches to the parskip mode.

\subsubsection{References and footnotes}

\DescribeMacro{\cref}
References are extended with the \software{cleveref} package \cite{cleveref}, which allows to \eg just type "\cref"\marg{key}  in order to write \enquote{figure 1}.
Furthermore, the \software{cleveref} package allows to reference multiple objects within one "\cref"\marg{key1,key2}.

\DescribeMacro{\cite}
Citations are adjusted to not start on a new line in order to avoid the repeated use of "~\cite"\marg{key}.

\DescribeMacro{\ref}
\DescribeMacro{\eqref}
\DescribeMacro{\subref}
References are also adjusted to not start on a new line.

\DescribeMacro{\footnote}
Footnotes are adjusted to swallow white space before the footnote mark and at the beginning of the footnote text.

\subsubsection{Acronyms} \label{sec:acronyms}

The \software{hep-acronym} package \cite{hep-acronym} is loaded.
\DescribeMacro{\acronym}
\DescribeMacro{\shortacronym}
\DescribeMacro{\longacronym}
The "\acronym"\meta{*}\oarg{typeset abbreviation} \marg{abbreviation}\meta{*}\marg{definition}\oarg{plural definition} macro generates the singular "\"\meta{abbreviation} and plural "\"\meta{abbreviation}"s" macros.
The first star prevents the addition of an \enquote{s} to the abbreviation plural.
The second star restores the \hologo{TeX} default of swallowing subsequent white space.
The long form is only shown at the first appearance of these macros, later appearances generate the abbreviation with a hyperlink to the long form.
The long form is never used in math mode.
Capitalization at the beginning of paragraphs and sentences is (mostly) ensured.
The "\shortacronym" and "\longacronym" macros are drop-in replacements of the "\acronym" macro showing only the short or long form of their acronym.

\subsection{Math}

The \software{hep-math} \cite{hep-math} and \software{hep-math-font} \cite{hep-math-font} packages are loaded.
\DescribeMacro{\mathbf}
Bold math, via "\mathbf" is improved, \ie ($ A  b  \Gamma \delta \mathbf A \mathbf b \mathbf \Gamma \mathbf \delta$).
Macros switching to "bfseries" such as "\section"\marg{text} are ensured to also typeset math in bold.
\DescribeMacro{\text}
The "\text"\marg{text} macro makes it possible to write text within math mode, \ie ($ \text A  \text b  \text \Gamma \text \delta \text{\textbf A} \text{\textbf b} \text{\textbf \Gamma} \text{\textbf \delta}$).
\DescribeMacro{\mathsf}
The math sans serif alphabet is redefined to be italic sans serif if the main text is serif and italic serif if the main text is sans serif, \ie ($\mathsf A \mathsf b \mathsf \Gamma \mathsf \delta \mathbf{\mathsf A} \mathbf{\mathsf b} \mathbf{\mathsf \Gamma} \mathbf{\mathsf \delta}$).
\DescribeMacro{\mathscr}
The "\mathcal" font \ie ($\mathcal{ABCD}$) is accompanied by the "\mathscr" font \ie ($\mathscr{ABCD}$).
\DescribeMacro{\mathbb}
The "\mathbb" font is adjusted depending on the "sansserif" option \ie ($\mathbb{Ah1}$).
\DescribeMacro{\mathfrak}
Finally, the "\mathfrak" font is also available \ie ($\mathfrak{AaBb12}$).

\DescribeMacro{\nicefrac}
\DescribeMacro{\flatfrac}
\DescribeMacro{\textfrac}
The "\frac"\marg{number}\marg{number} macro is accompanied by "\nicefrac"\linebreak[1]\marg{number}\linebreak[1]\marg{number}, "\textfrac"\marg{number}\marg{number}, and "\flatfrac"\marg{number}\marg{number} leading to $\frac12$, $\nicefrac12$, \textfrac12, and $\flatfrac12$.
\DescribeMacro{\diag}
\DescribeMacro{\sgn}
Diagonal matrix "\diag" and signum "\sgn" operators are defined.

\DescribeMacro{\mathdef}
The "\mathdef"\marg{name}\oarg{arguments}\marg{code} macro \prefix{re}{defines} macros only within math mode without changing the text mode definition.

\DescribeMacro{\i}
\DescribeMacro{\d}
The imaginary unit $\i$ and the differential $\d$ are defined using this functionality.

\DescribeMacro{\numberwithin}
For longer paper it can be useful to re-number the equation in accordance with the section numbering "\numberwithin{equation}{section}".
\DescribeMacro{subequations}
In order to further reduce the size the of equation counter it can be useful to wrap "align" environments with multiple rows in a "subequations" environment.

\DescribeMacro{\unit}
\DescribeMacro{\inv}
The correct spacing for units, \cf \cref{eq:greek}, is provided by the macro "\unit"\oarg{value} \marg{unit} which can also be used in text mode.
The macro "\inv"\oarg{power}\marg{text} allows to avoid math mode also for inverse units such as \unit[5]{\inv{fb}} typeset via "\unit[5]{\inv{fb}}".

Greek letters are adjusted to always be italic and upright in math and text mode, respectively, using the \software{hep-math-font} \cite{hep-math-font} package.
This allows differentiations like
\begin{align} \label{eq:greek}
\sigma &= \unit[5]{fb} \ , & &\mbox{at \unit[5]{\sigma} C.L.} \ , & \mu &= \unit[5]{cm} \ , & l &= \unit[5]{\mu m} \ .
\end{align}
Additionally, Greek letters can also be directly typed using Unicode.

\DescribeMacro{\ev}
\DescribeMacro{\pdv}
\DescribeMacro{\comm}
\DescribeMacro{\order}
The \software{hep-math} package \cite{hep-math} provides additional macros such as
\begin{align}
&\ev{\phi} \ ,
&&\pdv[f]{x}[y]^2 \ ,
&&\comm{A}{B} \ ,
&&\order{x^2} \ ,
&&\eval{x}_0^\infty \ ,
&&\det(M)\ .
\end{align}

\DescribeMacro{\cancel}
\DescribeMacro{\slashed}
The "\cancel"\marg{characters} macro and the "\slashed" \marg{character} macro allow to $\cancel{\text{cancel}}$ math and use the Dirac slash notation \ie $\slashed \partial$, respectively.

\DescribeMacro{\overleftright}
A better looking over left right arrow is defined \ie $\overleftright{\partial}$.

\subsection{Floats}

\DescribeEnv{figure}
\DescribeEnv{table}
Automatic float placement is adjusted to place a single float at the top of pages and to reduce the number of float pages, using the \software{hep-float} package \cite{hep-float}.
The most useful float placement is usually archived by placing the float \emph{in front} of the paragraph it is referenced in first.

\DescribeEnv{panels}
\DescribeMacro{\panel}
The "panels" environment provides sub-floats and takes as mandatory argument either the number of sub-floats (default~2) or the width of the first sub-float as fraction of the "\linewidth".
Within the "\begin{panels}"\oarg{vertical alignment}\marg{width} environment the "\panel" macro initiates a new sub-float.
In the case that the width of the first sub-float has been given as an optional argument to the "panels" environment the "\panel"\marg{width} macro takes the width of the next sub-float as mandatory argument.

\DescribeMacro{\graphic}
\DescribeMacro{\graphics}
The "\graphic"\oarg{width}\marg{figure} macro is defined, which is a wrapper for the "\includegraphics"\marg{figure} macro and takes the figure width as fraction of the "\linewidth" as optional argument (default~1).
If the graphics are located in a sub-folder its path can be indicated by "\graphics"\marg{subfolder}.

\subsection{Bibliography} \label{sec:bibliography}

\DescribeMacro{\bibliography}
\DescribeMacro{\printbibliography}
The \software{biblatex} package \cite{biblatex} is loaded for bibliography management.
The user has to add the line "\bibliography"\marg{my.bib} to the preamble of the document and "\printbibliography" at the end of the document.
The bibliography is generated by \software{Biber} \cite{biber}.
\software{biblatex} is extended by the \software{hep-bibliography} package \cite{hep-bibliography} to be able to cope with the "collaboration" and "reportNumber" fields provided by \online{https://inspirehep.net}{inspirehep.net} and a bug in the volume number is fixed.
Additionally, the PubMed IDs are recognized and \online{https://ctan.org}{ctan.org}, \online{https://github.com}{github.com}, \online{https://gitlab.com}{gitlab.com}, \online{https://bitbucket.org}{bitbucket.org}, \online{https://www.launchpad.net}{launchpad.net}, \online{https://sourceforge.net}{sourceforge.net}, and \online{https://hepforge.org}{hepforge.org} are valid "eprinttype"s.
\DescribeMacro{erratum}
Errata can be included using the "related" feature.
\begin{verbatim}
\article{key1,
  ...,
  relatedtype="erratum",
  related="key2",
}
\article{key2,
  ...,
}
\end{verbatim}

\section{Conclusion}

The \software{hep-paper} package provides a matching selection of preloaded packages and additional macros enabling the user to focus on the content instead of the layout by reducing the amount of manual tasks.
The majority of the loaded packages are fairly lightweight, the others can be deactivated with package options.

\DescribeMacro{arxiv-collector}
\nolinkurl{arxiv.org} \cite{arxiv} requires the setup dependent "bbl" files instead of the original "bib" files, which causes trouble if the local \hologo{LaTeX} version differs from the one used by arXiv.
The \software{arxiv-collector} python script \cite{arxiv-collector} alleviates this problem by collecting all files necessary for publication on arXiv (including figures).

\printbibliography

\end{document}

\endinput
%%
%% End of file `hep-paper-documentation.tex'.
