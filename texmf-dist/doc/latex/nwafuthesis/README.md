The nwafuthesis Class
LaTeX thesis template for Northwest A&F University, China, v1.11

Overview
This template supports doctoral or master dissertion and undergraduate thesis in Chinese. With the help of modern LaTeX3 technology, nwafuthesis aims to create a simple interface, a normative format, as well as a hackable class for the users.

At present, nwafuthesis only supports XeTeX and LuaTEX engines. nwafuthesis only allows UTF-8 encoding.

The documentation can be found in nwafuthesis.pdf (in Chinese).

Gitee re­pos­i­tory: https://gitee.com/nwafu_nan/nwafuthesis-l3.

Installation
To install nwafuthesis, you can use one of the following methods:

If you are running TeX Live or MikTeX, the simplest way is to use that distribution's package manager.

Get the source from Gitee:
  git clone git@gitee.com:nwafu_nan/nwafuthesis-l3.git
Run build-win.bat (on Windows) or build.sh (on Linux) to generate the templates file. This file would be found in a same folder. You can enjoy nwafuthesis here at once.

For permanent installation, you may move the following files into a directory where LaTeX will find them (e.g. TEXMF/tex/LaTeX/nwafuthesis/):

nwafuthesis.cls
Do not forget to update the filename database afterwards.

Contributing
Issues and pull requests are always welcome.

License
This work may be distributed and/or modified under the conditions of the LaTeX Project Public License, either version 1.3c of this license or (at your option) any later version.

Copyright (C) 2017–2022 by Nan Geng <nangeng@nwafu.edu.cn>.
