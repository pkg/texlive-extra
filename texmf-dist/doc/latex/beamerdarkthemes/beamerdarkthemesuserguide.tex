% Copyright 2014-2018 by Damien Thiriet
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
% http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is Damien Thiriet
%
% This work consists of the files beamerdarkthemesuserguide.tex,
% beamerdarkthemesuserguide.pdf, beamercolorthemecormorant.sty,
% beamercolorthemefrigatebird.sty, beamercolorthememagpie.sty,
% dahut.jpg, img_5630.jpg, example.tex, frigatebirdexampletree.pdf,
% frigatebirdexampledefault.pdf, frigatebirdexamplesidebar.pdf,
% frigatebirdexampleinfolines.pdf, cormorantexampledefault.pdf,
% cormorantexamplesidebar.pdf, cormorantexampleinfolines.pdf,
% cormorantexampletree.pdf, magpieexampledefault.pdf,
% magpieexamplesidebar.pdf, magpieexampleinfolines.pdf,
% magpieexampletree.pdf, makeexamples.sh
%
% img_5630.jpg was taken by Damien Thiriet in the nice Dolina Pięciu
% Stawów near Zakopane, Poland. img_5630.jpg, dahut.jpg and
% example.tex are licenced CC-BY version 4.0 or later.
% You will find a copy of this licence in
% http://creativecommons.org/licenses/by/4.0/legalcode
%
% makeexamples.sh is based upon beamerthemesmakeexample.sh, a file that
% could be found in beamer official doc in 2013 (scripts are nowadays
% part of beamer Makefile).
% 
% beamercolorthemeseahorse.sty was used as a canvas 
% when coding beamercolorthememagpie.sty first version.
%
% Package version 0.5.1


\documentclass[12pt]{article}
\usepackage[a4paper,vmargin={1cm,1.5cm},inner=2cm,outer=2cm]{geometry}           
\usepackage{polyglossia}
\usepackage{pgf}
\usepackage{hyperref}

\setdefaultlanguage{english}

\setmainfont{Latin Modern Roman}
\setsansfont{Latin Modern Sans}
\setmonofont{Latin Modern Mono}
%\setmainfont{Linux Libertine O}
%\setsansfont{Linux Biolinum O}
%\setmonofont{Linux Libertine Mono O}

\title{Beamer dark Themes}
\author{Damien Thiriet \\ \url{damien.thiriet@uj.edu.pl}}

%\date{}

\begin{document}
\maketitle
\tableofcontents

\section{Installation}

\subsection{TeX{} distribution package manager}

Just download the package \ttfamily darkbeamerthemes \rmfamily  with your \TeX{} distribution (\TeX{} Live, MiK\TeX{}, Mac\TeX{}, pro\TeX{}t…).
\TeX{} Live users may use the graphical interface or write in a console
\begin{verbatim}
 tlmgr install darkbeamerthemes
\end{verbatim}
An introduction to Mac\TeX{} package manager can be found at \url{http://code.google.com/p/mactlmgr/wiki/GettingStarted}, a more complete documentation should be found in Mac\TeX{} help menu.
MiK\TeX{} manual is available online: \url{http://www.miktex.org/help}.
As far as I understand, pro\TeX{}t uses MiK\TeX{} package manager.

\subsection{Manual download}

Sometime you cannot use your \TeX{} distribution packaging tools. You may just
do not know what a \TeX{} distribution is; maybe you don't have rights to use its admin tools; some GNU/linux (and *BSD?) distributions package \TeX{} Live without its administration tools; etc. 

Download \ttfamily beamercolorthemecormorant.sty, beamercolorthemefrigatebird.sty \rmfamily{} and \rmfamily{}  beamercolorthememagpie.sty. \rmfamily Under GNU/Linux, you'd better put them in a subdirectory of \TeX{} Live personal directory, \verb+$HOME/texmf+ by defaults (\verb+%USERPROFILE%\texmf+ under Windows). 
Create the subdirectory \verb+$HOME/texmf/tex/latex/+ and move those three files there. They will now be found during compilation, whatever directory you are writing from.

An alternative is to download the file you need directly in your presentation file. This is much less convenient if you intend to use these themes in some other presentation, located in another directory. Do not forget then to copy/symlink those files or to point them using the \verb+../+ notation.

Then insert the following line in your preamble:
\begin{verbatim}
 \usecolortheme{cormorant}
\end{verbatim}

Cormorant should be of course replaced by \ttfamily frigatebird \rmfamily or \ttfamily{}magpie\rmfamily{}, depending on your choice.
If you chose any global theme, you should mention the theme first, then your color theme.

\begin{verbatim}
  \usetheme{Bergen}
  \usecolortheme{cormorant}
\end{verbatim}

\section{Dark themes: what for?}

Before choosing any dark theme, you should be aware of its side effects.

\begin{quotation}
        Inverse video (bright text on dark background) can be a problem during presentations in bright environments since only a small percentage of the presentation area is light up by the beamer. Inverse video is harder to reproduce on printouts and on transparencies.
\end{quotation}

\begin{flushright}
  \emph{Beamer Class User Guide} 3.3, p.~38. 
\end{flushright}

On the other hand, several years of hard experience with light backgrounds used in bright classroom convinced me that a really dark theme might be useful: 
\begin{itemize}
    \item when there is no screen but a not-so-white wall 
    \item when there is a screen, but your classroom is quite sunny (on the other hand, students would be more likely to fall asleep in a dark classroom…) 
    \item when your presentation includes lots of photos, images, paintings… Art Historians often use dark background to emphasize the paintings they are about to comment. Paintings and photos are more visible on a black background than on a white one.
\end{itemize}

These themes were tested with all beamer themes. In my opinion, mixing them with shadowed themes, such as the very popular Warsaw, should be avoided. White shadows do not look too fancy on black background IMHO. \emph{De gustibus}…


\section{Names}

As requested by Till Tantau in \ttfamily{}beamer\rmfamily{} documentation, my color themes are named after birds.

\begin{description}
\item[cormorant]
A black and green theme, the first one I worked with. First named magpie, since the first black and green bird I could find on the web was an American magpie. 
Tested during one year, sadly without asking students for callback. Its items were hardly readable when projector darkened the output. I decided finally to mix green with white rather than black. 

\item[magpie]
A black/blue theme. Named after European magpies.

\item[frigatebird]
A black/red theme. Inspired by frigate bird's male. Rather for those who do not associate to closely red with beamer's alert boxes. 
\end{description}

\section{Caveats}

\verb+\alert{}+ is not easy to combine with dark backgrounds. Red is not that readable on a dark background. Orange is more readable, but the hierarchy between \verb+\alert{}+ and \verb+\structure{}+ is not obvious at all.

Version 0.5 reassigned therefore basic colors: yellow to \ttfamily{}alert\rmfamily{} foregrounds and orange to \ttfamily{}structure\rmfamily{} foregrounds. I bet people might find yellow/black contrast more important than the orange/black one. Dark red backgrounds are still used for alertblocks however.

\section{Examples}

Surprisingly, Euclid's presentation does not include any photography. I designed those themes to end up with describing photos students could hardly see. So let's take an example of my own, with a picture, last but not least in French: this is the part of the doc that will surely be properly written.
Some sentences are too long, though.

Examples are basically issued from an \ttfamily{}example.tex\rmfamily{} file with 4 slides designed with as many  environments or macros as possible: 
\begin{itemize}
   \item slide 2: \verb+frametitle+, \verb+block+, \verb+exampleblock+, \verb+description+
   \item slide 3: \verb+framesubtitle+, \verb+includegraphics+ 
   \item slide 4: \verb+structure+, \verb+alert+
\end{itemize}
You can browse the whole file \ttfamily{}example.tex\rmfamily{} in the package sources.

Examples are displayed on pages \pageref{fig:cormorant-default}–\pageref{fig:frigatebird-tree}. Figure~\ref{fig:cormorant-default} to \ref{fig:frigatebird-default} display the whole document without any outer nor inner theme. Figure~\ref{fig:cormorant-sidebar} to \ref{fig:frigatebird-tree} display slide 2 and 3 with outer themes variations.  

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=1]{cormorantexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=2]{cormorantexampledefault.pdf} 

   \pgfimage[width=.45\linewidth,page=3]{cormorantexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=4]{cormorantexampledefault.pdf}
   \caption{cormorant color theme}
   \label{fig:cormorant-default}
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=1]{magpieexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=2]{magpieexampledefault.pdf} 

   \pgfimage[width=.45\linewidth,page=3]{magpieexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=4]{magpieexampledefault.pdf}
   \caption{magpie color theme}
   \label{fig:magpie-default}
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=1]{frigatebirdexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=2]{frigatebirdexampledefault.pdf} 

   \pgfimage[width=.45\linewidth,page=3]{frigatebirdexampledefault.pdf}
   \pgfimage[width=.45\linewidth,page=4]{frigatebirdexampledefault.pdf}
   \caption{frigatebird color theme}
   \label{fig:frigatebird-default}
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{cormorantexamplesidebar.pdf}
   \pgfimage[width=.45\linewidth,page=3]{cormorantexamplesidebar.pdf}
   \caption{cormorant color theme, sidebar outer theme}  
   \label{fig:cormorant-sidebar}
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{cormorantexampleinfolines.pdf}
   \pgfimage[width=.45\linewidth,page=3]{cormorantexampleinfolines.pdf}
   \caption{cormorant color theme, infolines outer theme}  
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{cormorantexampletree.pdf}
   \pgfimage[width=.45\linewidth,page=3]{cormorantexampletree.pdf}
   \caption{cormorant color theme, tree outer theme}  
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{magpieexamplesidebar.pdf}
   \pgfimage[width=.45\linewidth,page=3]{magpieexamplesidebar.pdf}
   \caption{magpie color theme with sidebar outer theme}  
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{magpieexampleinfolines.pdf}
   \pgfimage[width=.45\linewidth,page=3]{magpieexampleinfolines.pdf}
   \caption{magpie color theme with infolines outer theme}  
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{magpieexampletree.pdf}
   \pgfimage[width=.45\linewidth,page=3]{magpieexampletree.pdf}
   \caption{magpie color theme with tree outer theme}  
   \label{fig:magpie-tree}
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{frigatebirdexamplesidebar.pdf}
   \pgfimage[width=.45\linewidth,page=3]{frigatebirdexamplesidebar.pdf}
   \caption{frigatebird color theme, sidebar outer theme}  
\end{figure}

\begin{figure}[p]
   \pgfimage[width=.45\linewidth,page=2]{frigatebirdexampleinfolines.pdf}
   \pgfimage[width=.45\linewidth,page=3]{frigatebirdexampleinfolines.pdf}
   \caption{frigatebird color theme, infolines outer theme}  
\end{figure}

\begin{figure}[h!]
   \pgfimage[width=.45\linewidth,page=2]{frigatebirdexampletree.pdf}
   \pgfimage[width=.45\linewidth,page=3]{frigatebirdexampletree.pdf}
   \caption{frigatebird color theme, tree outer theme}  
   \label{fig:frigatebird-tree}
\end{figure}

\newpage

\section{History}

\subsection*{v. 0.5.1}


\begin{itemize}
   \item fixed a bug: whitish colors when covered (Riccardo Maffei)
\end{itemize}

\subsection*{v. 0.5}

Color changes may be considered as bug fixes. I realized only few colors do render well on a dark background; red and green are not one of them.
\begin{itemize}
   \item \verb+\alert{}+ switched to previous \verb+\structure{}+ yellow output.
   \item \verb+\structure{}+ gives now an orange output.
   \item \ttfamily{}item\rmfamily{} color is now using \ttfamily{}structure.fg\rmfamily{} color.
   \item Palettes redesigned in a more consistent way.
   \item New colors: \ttfamily{} section in sidebar shaded\rmfamily{},\ttfamily{} subsection in sidebar shaded\rmfamily{},\ttfamily{} section in head/foot shaded\rmfamily{},\ttfamily{} subsection in head/foot shaded\rmfamily{}.
   \item \ttfamily{}local structure\rmfamily{} was deleted.
   \item Documentation improvements: links to auxiliary files, new caveats section, switch to Latin Modern Font, history divided in subsections.
   \item README now lists package files.
\end{itemize}

\subsection*{v. 0.4.2}
\begin{itemize}
   \item Improvements of frigatebird: brighter description items (the former ones were hardly readable on a black background), alert colour based on purple.
   \item Typos fixed in documentation, new history section.
\end{itemize}
This version was not uploaded on CTAN because I lacked time to do it.

\subsection*{v. 0.4.1}
First release on CTAN.
\begin{itemize}
   \item add package version on *sty, *tex files. 
   \item *jpg and example.tex licenced CC-BY. 
   \item Add changelog to README.
\end{itemize}

\subsection*{v. 0.4}

\begin{itemize}
   \item Rename magpie to cormorant; 
   \item New themes: magpie and frigatebird.
   \item New files: \ttfamily{}beamerdarkthemesuserguide.tex\rmfamily{}, \ttfamily{}example.tex\rmfamily{} and their derivative, \ttfamily{}img\_5630.jpg\rmfamily{} and \ttfamily{}dahut.jpg\rmfamily{}.

\end{itemize}

\subsection*{v. 0.3}
\begin{itemize}
   \item Change main green from green!50!black to green!50!white to avoid unreadable description items on dark rendering.
   \item New file: \ttfamily{}makeexample.sh\rmfamily{}, adapted from \ttfamily{}beamer\rmfamily{} package for testing purpose.
\end{itemize}

\subsection*{v. 0.2}
\begin{itemize}
   \item Cracow theme dropped; 
   \item oneline outertheme dropped;
   \item Add sidebar colors to \ttfamily{}beamercolorthememagpie.sty\rmfamily{}.
\end{itemize}

\subsection*{v. 0.1}
\begin{itemize}
   \item New colortheme \ttfamily{}magpie\rmfamily{}. 
   \item New theme \ttfamily{}Cracow\rmfamily{}.
   \item New outer theme \ttfamily{}oneline\rmfamily{}.
\end{itemize}



\end{document}
