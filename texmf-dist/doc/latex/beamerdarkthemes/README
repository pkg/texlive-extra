DESCRIPTION 

Beamerdarkthemes is a bundle of three dark beamer color themes.
They mix one dominant colour with a black background: green for
cormorant colour theme, red for frigatebird and blue for magpie.
You will find some examples in the documentation file 
beamerdarkthemesuserguide.pdf.

Package version: 0.5.1, 2020-05-10

AUTHOR

Damien Thiriet

contact: damienXthirietX77@laposteXnet (change X with .)

DOCUMENTATION

Documentation files are beamerdarkthemesuserguide.tex and beamerdarkthemesuserguide.pdf 

FILES

This work consists of the files beamerdarkthemesuserguide.tex,
beamerdarkthemesuserguide.pdf, beamercolorthemecormorant.sty,
beamercolorthemefrigatebird.sty, beamercolorthememagpie.sty,
dahut.jpg, img_5630.jpg, example.tex, frigatebirdexampletree.pdf,
frigatebirdexampledefault.pdf, frigatebirdexamplesidebar.pdf,
frigatebirdexampleinfolines.pdf, cormorantexampledefault.pdf,
cormorantexamplesidebar.pdf, cormorantexampleinfolines.pdf,
cormorantexampletree.pdf, magpieexampledefault.pdf,
magpieexamplesidebar.pdf, magpieexampleinfolines.pdf,
magpieexampletree.pdf, makeexamples.sh, ccby.png

img_5630.jpg was taken by Damien Thiriet in the nice Dolina 
Pięciu Stawów near Zakopane, Poland. 

LICENCES

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Damien Thiriet

img_5630.jpg and dahut.jpg and example.tex are licenced 
CC-BY version 4.0 or later.
You will find a copy of this licence in
http://creativecommons.org/licenses/by/4.0/legalcode

makeexamples.sh is based upon beamerthemesmakeexample.sh, 
a file that could be found in beamer official doc in 2013 
(scripts are nowadays part of beamer Makefile)

beamercolorthemeseahorse.sty was used as a canvas 
when coding beamercolorthememagpie.sty first version.  
