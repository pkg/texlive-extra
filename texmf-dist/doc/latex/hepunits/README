hepunits --- a set of useful units for use in high energy physics
by Andy Buckley <andy@insectnation.org>
-----------------------------------------------------------

hepunits is a LaTeX package built on the siunitx package which adds a collection
of useful HEP units to the existing siunitx set.  These include a refined
definition of the \electronvolt energy unit (accounting for missing kerns),
convenience derived units \eV, \MeV, \GeV, \TeV, their associated derived
momentum and mass units \MeVc, \MeVcsq and so on (also with kern corrections),
and convenience-prefixed cross-section and luminosity units.

Additionally, some units are provided (for now) for pure convenience via the
[sicmds] package option. Please provide feedback if you'd like to see more
HEP-specific units added.

This material is subject to the LaTeX Project Public License.
See http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html
for the details of that license.

Author: Andy Buckley <andy@insectnation.org>
