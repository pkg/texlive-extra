2020-04-10  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 2.0.0.

	* Update to use siunitx.

	* Refine eV and eV/c kerns, with bold font-series specifialisation.

	* Change 'overc' and 'overcsq' macros to e.g. \MeVc and \MeVcsq.

	* Removing 'inverse eV' units.

	* Make the units non-freestanding by default, and exclude
	non-essential convenience prefixed macros for SI units unless
	requsted via a package option.

2009-01-13  Andy Buckley  <andy@insectnation.org>

	* Added cross-section units of millibarns and upwards for e.g. LHC
	total cross-sections.

2007-09-27  Andy Buckley  <andy@insectnation.org>

	* Ready for the momentous 1.1.1 release.

	* Removed "full screen" param from documentation PDF
	configuration, since this is actually very annoying.

	* Added Makefile to build distribution tarball.

	* Fixed definition of \GeVovercsq, which was missing the second
	power of c. Thanks to Jeroen Hegeman for the bug report.

2005-12-12  Andy Buckley  <andy@insectnation.org>

	* Added noprefixcmds option due to residual guilt about
	contaminating everyone's command namespace.

	* Made basic manual.

	* Removed "derived" from default SIunits options.

	* Added SIunits options passing.

	* Started ChangeLog.
