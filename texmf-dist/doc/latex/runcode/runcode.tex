% LaTeX Package: runcode 2023/01/18 v1.8
% 
% Copyright (C) 2020-2022 by Haim Bar and HaiYing Wang
% 
% This file may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3c of this license or (at your option) any later
% version.  The latest version of this license is in:
% 
% http://www.latex-project.org/lppl.txt
% 
% and version 1.3c or later is part of all distributions of
% LaTeX version 2005/12/01 or later.

\documentclass{ltxdoc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\author{Haim Bar and HaiYing Wang \\haim.bar@uconn.edu, haiying.wang@uconn.edu}
\date{\today}
\title{The \textbf{runcode} package}
\begin{document}

\maketitle
\begin{abstract}

\texttt{runcode} is a \LaTeX{} package that executes programming source codes (including
all command line tools) from \LaTeX{}, and embeds the results in the resulting pdf
file. Many programming languages can be easily used and any command-line
executable can be invoked when preparing the pdf file from a tex file. \texttt{runcode}
is also available on \href{https://ctan.org/pkg/runcode}{CTAN}.

It is recommended to use this package in the server mode together with the
\href{https://www.python.org/}{Python} \href{https://pypi.org/project/talk2stat/}{talk2stat} package. Currently, the server mode supports \href{https://julialang.org/}{Julia}, \href{https://www.mathworks.com/products/matlab.html}{MatLab},
\href{https://www.python.org/}{Python}, and \href{https://www.r-project.org/}{R}. More languages will be added.

For more details and usage examples and troubleshooting, refer to the
package’s github repository, at \url{https://github.com/Ossifragus/runcode}.

\end{abstract}

\section{Installation}
\label{sec:org1037dff}
You can simply put the runcode.sty file in the \LaTeX{} project folder.

The server mode requires the
\href{https://pypi.org/project/talk2stat/}{talk2stat} package. To install
it from the command line, use:

\begin{verbatim}
pip3 install talk2stat
\end{verbatim}

\textbf{Note}: \texttt{runcode} requires to enable the \texttt{shell-escape} option when
compiling a \LaTeX{} document.


\section{Usage}
\label{sec:org325e4fc}
\subsection{Load the package:}
\label{sec:orgbccebff}
\begin{minted}[]{latex}
\usepackage[options]{runcode}
\end{minted}

Available options are:

\begin{itemize}
\item \texttt{cache}: use cached results.

\item \texttt{fvextra}: use the \href{https://ctan.org/pkg/fvextra}{fvextra} package to show code.

\item \texttt{julia}: start server for \href{https://julialang.org/}{Julia} (requires \href{https://pypi.org/project/talk2stat/}{talk2stat}).

\item \texttt{listings}: use the \href{https://ctan.org/pkg/listings?lang=en}{listings} package to show code.

\item \texttt{matlab}: start server for \href{https://www.mathworks.com/products/matlab.html}{MatLab} (requires \href{https://pypi.org/project/talk2stat/}{talk2stat}).

\item \texttt{minted}: use the \href{https://ctan.org/pkg/minted}{minted} package to show code (requires the \href{https://pygments.org/}{pygments} package).
This is the default option.

\item \texttt{nominted}: use the \href{https://ctan.org/pkg/fvextra}{fvextra} package
instead of the \href{https://ctan.org/pkg/minted}{minted} package to show
code (this does not require the \href{https://pygments.org/}{pygments}
package, but it does not provide syntax highlights).

\item \texttt{nohup}: use the \texttt{nohup} command when starting a server. When using
the server-mode, some editors terminate all child processes after
\LaTeX{} compiling such as Emacs with Auctex. This option set the
variable notnohup to be false, and the server will not be terminated
by the parent process. \textbf{This option has to be declared before
declaring any language}, e.g., \texttt{[nohup, R]} works but \texttt{[R, nohup]}
does not work.

\item \texttt{python}: start server for \href{https://www.python.org/}{Python}
(requires \href{https://pypi.org/project/talk2stat/}{talk2stat}).

\item \texttt{run}: run source code.

\item \texttt{R}: start server for \href{https://www.r-project.org/}{R} (requires
\href{https://pypi.org/project/talk2stat/}{talk2stat}).

\item \texttt{stopserver}: stop the server(s) when the pdf compilation is done.
\end{itemize}

\textbf{Note}: If \href{https://ctan.org/pkg/minted}{minted} is used, the style of
the code block is controlled through the minted package,
\href{https://github.com/Ossifragus/runcode/blob/master/examples/MontyHall/MontyHall.tex\#L3-L4}{e.g.:}

\begin{minted}[]{latex}
\setminted[julia]{linenos, frame=single, bgcolor=bg, breaklines=true}
\setminted[R]{linenos, frame=single, bgcolor=lightgray, breaklines=true}
\end{minted}

Similarly, \href{https://ctan.org/pkg/fvextra}{fvextra} and \href{https://ctan.org/pkg/listings?lang=en}{listings} packages can be customized through the \texttt{\textbackslash{}lstset}
and \texttt{\textbackslash{}fvset} commands, respectively, e.g.: 

\begin{minted}[]{latex}
\lstset{basicstyle=\large, frame=single}
\fvset{fontsize=\small, linenos=true, frame=single}
\end{minted}

The outputs from executing codes are displayed in
\href{https://ctan.org/pkg/tcolorbox?lang=en}{tcolorbox}, so the style can
be customized with \texttt{\textbackslash{}tcbset},
\href{https://github.com/Ossifragus/runcode/blob/master/examples/MontyHall/MontyHall.tex\#L5}{e.g.:}

\begin{minted}[]{latex}
\tcbset{breakable,colback=red!5!white,colframe=red!75!black}
\end{minted}

\subsection{Basic commands:}
\label{sec:org88340c8}
\begin{itemize}
\item \texttt{\textbackslash{}runExtCode\{Arg1\}\{Arg2\}\{Arg3\}[Arg4]} runs an external code.

\begin{itemize}
\item \texttt{Arg1} is the executable program.
\item \texttt{Arg2} is the source file name.
\item \texttt{Arg3} is the output file name (with an empty value, the counter
\texttt{codeOutput} is used).
\item \texttt{Arg4} controls whether to run the code. \texttt{Arg4} is optional with
three possible values: if skipped or with empty value, the value of
the global Boolean variable \texttt{runcode} is used; if the value is set
to \texttt{run}, the code will be executed; if set to \texttt{cache} (or anything
else), use cached results (see more about the cache below).
\end{itemize}

\item \texttt{\textbackslash{}showCode\{Arg1\}\{Arg2\}[Arg3][Arg4]} shows the source code, using
\href{https://ctan.org/pkg/minted}{minted} (requires \href{https://pygments.org/}{pygments}), \href{https://ctan.org/pkg/fvextra}{fvextra}, or \href{https://ctan.org/pkg/listings?lang=en}{listings}.

\begin{itemize}
\item \texttt{Arg1} is the programming language.
\item \texttt{Arg2} is the source file name.
\item \texttt{Arg3} is the first line to show (optional with a default value 1).
\item \texttt{Arg4} is the last line to show (optional with a default value of
the last line).
\end{itemize}

\item \texttt{\textbackslash{}includeOutput\{Arg1\}[Arg2]} is used to embed the output from executed
code.

\begin{itemize}
\item \texttt{Arg1} is the output file name, and it needs to have the same value
as that of \texttt{Arg3} in \texttt{\textbackslash{}runExtCode}. If an empty value is given to
\texttt{Arg1}, the counter \texttt{codeOutput} is used.
\item \texttt{Arg2} is optional and it controls the type of output with a default
value \texttt{vbox}
\begin{itemize}
\item \texttt{vbox} (or skipped) = verbatim in a box.
\item \texttt{tex} = pure latex.
\item \texttt{inline} = embed result in text.
\end{itemize}
\end{itemize}

\item \texttt{\textbackslash{}inln\{Arg1\}\{Arg2\}[Arg3]} is designed for simple calculations; it runs
one command (or a short batch) and displays the output within the
text.

\begin{itemize}
\item \texttt{Arg1} is the executable program or programming language.
\item \texttt{Arg2} is the source code.
\item \texttt{Arg3} is the output type.
\begin{itemize}
\item \texttt{inline} (or skipped or with empty value) = embed result in text.
\item \texttt{vbox} = verbatim in a box.
\end{itemize}
\end{itemize}
\end{itemize}

\subsection{Language specific shortcuts:}
\label{sec:org591e87e}
\href{https://julialang.org/}{Julia}

\begin{itemize}
\item \texttt{\textbackslash{}runJulia[Arg1]\{Arg2\}\{Arg3\}[Arg4]} runs an external
\href{https://julialang.org/}{Julia} code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses
\href{https://pypi.org/project/talk2stat/}{talk2stat}'s
\href{https://julialang.org/}{Julia} server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the
basic command \texttt{\textbackslash{}runExtCode}.
\end{itemize}
\item \texttt{\textbackslash{}inlnJulia[Arg1]\{Arg2\}[Arg3]} runs \href{https://julialang.org/}{Julia}
source code (\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the \href{https://julialang.org/}{Julia}
server by default.
\item \texttt{Arg2} is the \href{https://julialang.org/}{Julia} source code to run.
If the \href{https://julialang.org/}{Julia} source code is wrapped
between "\texttt{```}" on both sides (as in the markdown grammar), then it
will be implemented directly; otherwise the code will be written to
a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \texttt{\textbackslash{}inln}.
\end{itemize}
\end{itemize}

\href{https://www.mathworks.com/products/matlab.html}{MatLab}

\begin{itemize}
\item \texttt{\textbackslash{}runMatLab[Arg1]\{Arg2\}\{Arg3\}[Arg4]} runs an external
\href{https://www.mathworks.com/products/matlab.html}{MatLab} code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses
\href{https://pypi.org/project/talk2stat/}{talk2stat}'s
\href{https://www.mathworks.com/products/matlab.html}{MatLab} server by
default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the
basic command \texttt{\textbackslash{}runExtCode}.
\end{itemize}
\item \texttt{\textbackslash{}inlnMatLab[Arg1]\{Arg2\}[Arg3]} runs
\href{https://www.mathworks.com/products/matlab.html}{MatLab} source code
(\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the
\href{https://www.mathworks.com/products/matlab.html}{MatLab} server by
default.
\item \texttt{Arg2} is the
\href{https://www.mathworks.com/products/matlab.html}{MatLab} source
code to run. If the
\href{https://www.mathworks.com/products/matlab.html}{MatLab} source
code is wrapped between "```" on both sides (as in the markdown
grammar), then it will be implemented directly; otherwise the code
will be written to a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \texttt{\textbackslash{}inln}.
\end{itemize}
\end{itemize}

\href{https://www.python.org/}{Python}

\begin{itemize}
\item \texttt{\textbackslash{}runPython[Arg1]\{Arg2\}\{Arg3\}[Arg4]} runs an external
\href{https://www.python.org/}{Python} code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses
\href{https://pypi.org/project/talk2stat/}{talk2stat}'s
\href{https://julialang.org/}{Julia} server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the
basic command \texttt{\textbackslash{}runExtCode}.
\end{itemize}
\item \texttt{\textbackslash{}inlnPython[Arg1]\{Arg2\}[Arg3]} runs
\href{https://www.python.org/}{Python} source code (\texttt{Arg2}) and displays
the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the \href{https://www.python.org/}{Python}
server by default.
\item \texttt{Arg2} is the \href{https://julialang.org/}{Julia} source code to run.
If the \href{https://www.python.org/}{Python} source code is wrapped
between "```" on both sides (as in the markdown grammar), then it
will be implemented directly; otherwise the code will be written to
a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \texttt{\textbackslash{}inln}.
\end{itemize}
\item \texttt{\textbackslash{}runPythonBatch[Arg1][Arg2]} runs an external
\href{https://www.python.org/}{Python} code file in batch mode (without a
server running). Python (at least currently), unlike the other
languages we use, does not have an option to save and restore a
session, which means that once a Python session ends, the working
environement (variable, functions) is deleted. In order to allow a
batch-mode in Python, we implemented such capability. It requires the
\href{https://pypi.org/project/dill/}{dill} module, which has to be
installed via \texttt{pip3 install dill}.
\begin{itemize}
\item \texttt{Arg1} is the \href{https://www.python.org/}{Python} source file name,
\item \texttt{Arg2} is the output file name.
\end{itemize}
\end{itemize}

\href{https://www.r-project.org/}{R}

\begin{itemize}
\item \texttt{\textbackslash{}runR[Arg1]\{Arg2\}\{Arg3\}[Arg4]} runs an external
\href{https://www.r-project.org/}{R} code file.
\begin{itemize}
\item \texttt{Arg1} is optional and uses
\href{https://pypi.org/project/talk2stat/}{talk2stat}'s
\href{https://www.r-project.org/}{R} server by default.
\item \texttt{Arg2}, \texttt{Arg3}, and \texttt{Arg4} have the same effects as those of the
basic command \texttt{\textbackslash{}runExtCode}.
\end{itemize}
\item \texttt{\textbackslash{}inlnR[Arg1]\{Arg2\}[Arg3]} runs \href{https://www.r-project.org/}{R}
source code (\texttt{Arg2}) and displays the output in line.
\begin{itemize}
\item \texttt{Arg1} is optional and uses the \href{https://www.r-project.org/}{R}
server by default.
\item \texttt{Arg2} is the \href{https://www.r-project.org/}{R} source code to run.
If the \href{https://www.r-project.org/}{R} source code is wrapped
between "```" on both sides (as in the markdown grammar), then it
will be implemented directly; otherwise the code will be written to
a file on the disk and then be called.
\item \texttt{Arg3} has the same effect as that of the basic command \texttt{\textbackslash{}inln}.
\end{itemize}
\end{itemize}


\section{Revisions}
\label{sec:orgf884e2e}
\begin{itemize}
\item v1.8, January 18, 2023, add support to \href{https://ctan.org/pkg/listings?lang=en}{listings.}
\item v1.7, August 20, 2022: changed the tmp/ folder to generated/ in order to
conform with CTAN suggestions; renamed the troubleshooting file.
\item v1.6, August 10, 2022: stop only configured/running servers; a new
reducedspace option - some document classes put more space after the code box;
changed the default timeout of servers to 60 seconds; expanded the
troubleshooting document. New examples are now available on GitHub, including
how to collaborate with people who use Overleaf.
\item v1.5, July 23, 2022: Removed the utf8x option when loading inputenc due to a
conflict with hyperref.
\item v1.4, July 18, 2022: Fixed a bug in the cache mode.
\item v1.3, May 14, 2022: Removed the hard-coded minted options.
\item v1.2, May 3, 2022: Added python options (server and batch).
\item v1.1, April 17, 2021: Added a nohup option; improved error handling (missing
code files, zero bytes in output files.)
\end{itemize}

\section{Contributing}
\label{sec:org0dce33b}
We welcome your contributions to this package by opening issues on
GitHub and/or making a pull request. We also appreciate more example
documents written using \texttt{runcode}.


\textbf{Citing} \texttt{runcode}:
Haim Bar and HaiYing Wang (2021). \href{https://jds-online.org/journal/JDS/article/103/info}{Reproducible Science with \LaTeX{}},
\emph{Journal of Data Science} 2021; 19, no. 1, 111-125, DOI 10.6339/21-JDS998
\end{document}
