This package automatically compute headlength for fancyhdr package.
For using this package just substitute
```tex
\usepackage{fancyhdr}
```
with
```tex
\usepackage{autofancyhdr}
```
This package is under LaTeX Project Public 1.3c license.
You can contact me by emailing to mojtaba.baghban@gmail.com