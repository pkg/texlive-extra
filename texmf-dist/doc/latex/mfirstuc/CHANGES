mfirstuc change log:

v2.08 (2022-10-14):

  * Reimplemented \makefirstuc in LaTeX3. This change supports
    UTF-8 with pdflatex.

    Rollback to v2.07 provided:

    \usepackage{mfirstuc}[=v2.07]

  * New command \MFUsentencecase

    This just uses \text_titlecase_first:n

    Principle features of \MFUsentencecase vs \makefirstuc are
    listed in Table 1 of the manual.

  * \makefirstuc has been rewritten to internally use
    \MFUsentencecase (via \glsmakefirstuc), but it still parses 
    its argument as in previous versions.

  * New command \MFUexcl{<cs>}

    This adds a command to the case-changing exclusion list. 
    Both \MFUsentencecase and \makefirstuc will skip the 
    identified command and its argument, and apply sentence-casing 
    to the content that follows.

    For example, glossaries.sty v4.50 implements \MFUexcl{\glsadd}

  * New command \MFUblocker{<cs>}

    This adds a command to the blocker list. This is used by
    \makefirstuc but not \MFUsentencecase. If the command is encountered 
    no case-change will be applied. This is designed for commands
    that automatically implement a case-change that may have been nested.

    For example, glossaries.sty v4.50 implements \MFUblocker{\Gls}

  * New command \MFUaddmap{<cs1>}{<cs2>}

    Adds a mapping. This is used by \makefirstuc but not
    \MFUsentencecase. If the first command <cs1> is encountered 
    it will be replaced with the second <cs2>. No case change 
    will be applied. (The case-change, if appropriate,
    should be dealt with by the replacement command.)

    For example, glossaries.sty v4.50 implements \MFUaddmap{\gls}{\Gls}

    Order of precedence: map, blocker, exclusion

  * New commands \MFUsaveatend and \MFUsave

v2.07 (2021-10-15):

  * new commands:

    - \MFUcapwordfirstuc
    - \MFUwordbreak
    - \MFUskippunc

v2.06 (2017-11-14):

  * \makefirstuc: added check for nested commands.

v2.05 (2017-11-10):

  * bug fix in \@mfu@applytofirst (#1 must be grouped)

v2.04 (2016-07-31):

  * bug fixes in \capitalisefmtwords and \@gls@split

  * added starred form of \capitalisefmtwords

v2.03 (2016-07-29):

  * Fixed test for \protect at the start of \makefirstuc

  * Add new conditional \ifMFUhyphen and new command
    \MFUcapword

  * New commands \capitalisefmtwords, \xcapitalisefmtwords
    and \ecapitalisefmtwords (like \capitalisewords etc
    but allows for text-block formatting commands within
    the argument).

v2.02 (2015-12-17):

  * Fixed bug in \capitalisewords
    http://www.dickimaw-books.com/cgi-bin/bugtracker.cgi?action=view&key=104

v2.01 (2015-12-08):

  * Added check for initial \protect in \makefirstuc

v2.0 (2015-09-09):

  * Split from glossaries bundle.

  * Fixed bug that occurred when the first two tokens of 
    \makefirstuc are both control sequences.

v1.10:

  * added \ecapitalisewords, \emakefirstuc

v1.09:

  * Added \MFUnocap

  * New package: mfirstuc-english.sty

v1.08:

  * Made \makefirstuc and \capitalisewords robust

v1.07:

  * Added \mfirstucMakeUppercase to allow the user to switch from
      \MakeUppercase to another case changing command.

v1.06:

  * added \capitalisewords, \xcapitalisewords

v1.05:

  * added \glsmakefirstuc (replaces \@gls@makefirstuc) to make
    it easier to customise.

v1.04: 

  * fixed bug occurring when initial control sequence
    in the argument of \makefirstuc has more than one 
    argument.

v1.02:

   * Fixed bug that transfered grouping, (e.g 
     \makefirstuc{\emph{abc}xyz} moved the xyz into the \emph)

   * If group following initial command is empty, \MakeUppercase
     is applied to the command, e.g. \makefirstuc{\ae{}bc} now
     produces \MakeUppercase\ae{}bc.

v1.01:

   * Added \xmakefirstuc (expands argument before applying \makefirstuc)

v1.0: 

   * Initial version

