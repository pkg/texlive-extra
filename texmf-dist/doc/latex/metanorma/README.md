# metanorma

This work is sponsored by Ribose Inc. (<https://www.ribose.com>).

This work is maintained by Ribose Inc. (<open.source@ribose.com>).
 
This work is licensed under MIT License.

---

This package includes a LaTeX document class, a `latexml` script and a `latexmlpost` stylesheet which allow you to write a standard LaTeX document and transcode it into Metanorma's `ADOC` format.

---

Copyright (c) 2019-2020 Ribose Inc.