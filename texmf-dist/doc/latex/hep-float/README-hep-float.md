

# The `hep-float` package

Convenience package for float placement

## Introduction

The `hep-float` package redefines some `LaTeX` float placement defaults and defines convenience wrappers for floats.

The `hep-float` package can be loaded with `\usepackage{hep-float}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

