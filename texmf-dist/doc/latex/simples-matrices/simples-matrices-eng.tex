%%
%% This is file `simples-matrices-eng.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% simples-matrices.dtx  (with options: `doc,ENG')
%% 
%% Do not distribute this file without also distributing the
%% source files specified above.
%% 
%% Do not distribute a modified version of this file.
%% 
%% File: simples-matrices.dtx
%% Copyright (C) 2022 Yvon Henel aka Le TeXnicien de surface
%%
%% It may be distributed and/or modified under the conditions of the
%% LaTeX Project Public License (LPPL), either version 1.3c of this
%% license or (at your option) any later version.  The latest version
%% of this license is in the file
%%
%%    http://www.latex-project.org/lppl.txt
%%
\RequirePackage{expl3}[2020/01/12]
\GetIdInfo$Id: simples-matrices.dtx 1.0.1 2022-07-03 TdS $
  {}
\documentclass[full]{l3doc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french,main=english]{babel}
\usepackage{xparse}
\usepackage{simples-matrices}
\newcommand\BOP{\discretionary{}{}{}}
\usepackage{xspace}
\usepackage{csquotes}
\usepackage{fancyvrb,fancyvrb-ex}
\usepackage{listings}
\usepackage{mathtools}
\usepackage{delarray}
\usepackage{nicematrix}

\providecommand\darg[3]{ \texttt{#1} \meta{#2} \texttt{#3} }

\colorlet{keyword20}{cyan} %!75!black
\colorlet{keyword30}{teal}
\colorlet{keycolor}{green!50!black}
\colorlet{dollarcolor}{violet}
\colorlet{cmdmuette}{blue}
\colorlet{cmdcausante}{red}

\lstset{frame=single,%
  language=[LaTeX]{TeX},%
  showspaces=false,%
  breaklines=true,%
  breakatwhitespace=true,%
  basicstyle=\ttfamily\bfseries,%
  alsoletter={*-$},%
  texcsstyle=*[2]\color{cmdcausante},%
  texcs=[2]{matrice,declarermatrice*,lamatrice,matid,matnulle,MatriceInterieur,LaMatriceInterieur},%
  texcsstyle=*[3]\color{cmdmuette},%
  texcs=[3]{declarermatrice,simplesmatricessetup},%
  keywordstyle=[20]\color{keyword20},%
  keywordstyle=[30]\color{keyword30},%
  keywordstyle=[40]\color{keycolor},%
  keywordstyle=[50]\color{dollarcolor},%
  keywords=[20]{tabular,array,matrix,matrix*,NiceArray},%
  keywords=[30]{x,S,T,I,J,C,O,D},%
  keywords=[40]{out-of-box,prefix,envir,typeord,argopt},%
  keywords=[50]{$},%
  escapeinside={\%*@}{@*)}%
}

\newcommand{\MacroPres}[2]{\textcolor{#1}{\textbf{\cs{#2}}}}
\newcommand{\csmute}{\MacroPres{cmdmuette}}
\newcommand{\csprint}{\MacroPres{cmdcausante}}
\newcommand{\ObjetPres}[2]{\textcolor{#1}{\textbf{\texttt{#2}}}}
\newcommand{\keyname}{\ObjetPres{keycolor}}
\newcommand{\envir}{\ObjetPres{keyword20}}
\newcommand{\TypeMat}{\ObjetPres{keyword30}}

\newcommand{\UnExemple}{%
  \par \medskip \par \noindent
  \begin{minipage}[c]{0.66\linewidth}
    \lstinputlisting[gobble=0, frame=single, numbers=left, numberstyle={\small}]{code00.tex}
  \end{minipage}
  \hspace{\stretch{1}}
  \begin{minipage}[c]{0.32\linewidth}
    \small
    \input{code00}
  \end{minipage}
  \par \medbreak}

\newcommand*{\vliste}{liste\xspace}

 \newcommand{\VoirEx}[1]{(see example on page~\pageref{#1})}

\begin{document}
\title{\pkg{simples-matrices} user guide\thanks{This file describes
    version~\ExplFileVersion, last revised~\ExplFileDate.\\
    \emph{In memory of my maternal grandmother} Adrienne \textsc{Binaut}
    (1908-03-23 -- 1997-06-08).}  }
\author{Yvon Henel\thanks{E-mail:
    \href{mailto:le.texnicien.de.surface@yvon-henel.fr}
    {le.texnicien.de.surface@yvon-henel.fr}}}
\maketitle
\noindent\hrulefill

\begin{abstract}
A package to write matrices which are defined with a list of comma separated
coefficients read by row.

A macro enables the definition of a named matrix, another enables the writing of
a named matrix. This package provides also some shortcuts for identity matrices
and null matrices.

The name of this package and of its macros are French based for there are
already too many macros using the word ``matrix'' itself. The French
``\foreignlanguage{french}{simples matrices}'' means ``simple
matrices''. \emph{Just a letter apart!}
\end{abstract}

\noindent\hrulefill

 \begin{otherlanguage}{french}
\begin{abstract}
Une extension pour écrire des matrices en donnant les coefficients par ligne
sous la forme d'une liste de valeurs séparées par des virgules.

Une macro permet de définir des matrices nommées et une autre permet d'écrire
les matrices nommées. L'extension fournit également quelques raccourcis pour les
matrices identité et les matrices nulles.

La documentation française pour l'utilisateur de l'extension
\pkg{simples-matrices} est disponible sous le nom de \texttt{simples-matrices-fra}.
\end{abstract}
\end{otherlanguage}

\noindent\hrulefill

\tableofcontents{}

\vspace{3\baselineskip}

The package \pkg{simples-matrices} requires \pkg{xparse} and \pkg{l3keys2e} used
to define macros and manage key-options. It loads \pkg{amsmath} as well for
ensuring a correct presentation of matrices.

\textbf{Beware}: you have to provide the suitable mathematical environment to
use the macros which print a matrix. Only the unstarred version of
\csmute{declarermatrice} may be used outside math-mode.


\section{The Macros}
\label{sec:macros}

\pkg{simples-matrices} offers nine document macros.


In the main text (syntaxes and examples), the name of a macro of this package is
printed in red if the macro produces some text in the document, otherwise it is
printed in blue.

\subsection{Main Macros}
\label{sec:main}

The six main document macros are the following:

\begin{function}{\matrice}
  \begin{syntax}
    \csprint{matrice}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\oarg{type}\marg{clist of coefficients}
  \end{syntax}
  where \meta{prefix} has the same meaning as the key \keyname{prefix} (see page~\pageref{key-prefix});
  \meta{clist of key-value pairs} ---optional and void by default--- can be used to
  redefined the \envir{matrix}-like environment; \meta{type} is a string of character
  the usage of which is explained later ---see \ref{sec:matrix-type}--- and
  \meta{clist of coefficients} is a clist of the coefficients of the matrix
  given by row order.

  The French \emph{matrice} means ``matrix''.
\end{function}

 With
\lstinline+$\matrice{1, 2, 3, 4}$+
 we obtain
\(\matrice{1, 2, 3, 4}\).

 With
\lstinline+$\matrice(b)[3]{1, 2, 3, 4, 5, 6}$+
 we obtain
\(\matrice(b)[3]{1, 2, 3, 4, 5, 6}\).

\begin{function}{\declarermatrice}
  \begin{syntax}
    \csmute{declarermatrice}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\marg{matrix name}\oarg{type}\marg{clist of coefficients}
  \end{syntax}
\end{function}

\begin{function}{\declarermatrice*}
  \begin{syntax}
    \csprint{declarermatrice*}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\marg{matrix name}\oarg{type}\marg{clist of coefficients}
  \end{syntax}
  where \meta{matrix name} is the name of the matrix to be used afterwards. The
  other arguments, optional or mandatory, have the same meaning than above. The
  starred version defines and prints the matrix. The unstarred version only
  defines it.

  The definition is global but one can redefine an existing named matrix with
  the same function. \textbf{No check is done} to ensure that one is not
  redefining a previously defined matrix.

  With the macro \csmute{declarermatrice}, the first two optional arguments have
  no effect for they are not take into account to define the matrix \VoirEx{sec:declar}.

  The French \emph{déclarer une matrice} means ``declare a matrix''.
\end{function}


\begin{function}{\lamatrice}
  \begin{syntax}
    \csprint{lamatrice}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\marg{matrix name}
  \end{syntax}
  prints the previously defined matrix the name of which is given by
  \meta{matrix name}. The two optional arguments have the meaning as before \VoirEx{sec:declar}.

  The French \emph{la matrice} means ``the matrix''.
\end{function}

\begin{function}{\MatriceInterieur}
  \begin{syntax}
    \csprint{MatriceInterieur}
  \end{syntax}
  gives the inner part of the last printed ---via \csprint{matrice} or
  \csprint{declarermatrice*}--- or defined ---via \csmute{declarermatrice}---
  matrix. It can be used inside an \envir{array}-like environment
  \VoirEx{sec:matint}.

  The French should be \emph{\foreignlanguage{french}{intérieur de la matrice}}
  which means ``inside of the matrix''. What is implicit is the adjective
  ``anonymous''. For named matrix see below.
\end{function}

\begin{function}{\LaMatriceInterieur}
  \begin{syntax}
    \csprint{LaMatriceInterieur}\marg{matrix name}
  \end{syntax}
  gives the inner part of the matrix with name \meta{matrix name}. It can be
  used inside an \envir{array}-like environment \VoirEx{sec:matint}.

  Again false French but the parallel with \csprint{lamatrice} should suggest that,
  now, the matrix has a name and we have to use it.
\end{function}


\subsection{Setting the Keys}
\label{sec:setting-keys}

One can change the values of the keys of \pkg{simples-matrices} with
\begin{function}{\simplesmatricessetup}
  \begin{syntax}
    \csmute{simplesmatricessetup}\marg{clist of key-value pairs}
  \end{syntax}
  where \meta{clist of key-value pairs} is the usual clist of key-value pairs
  setting one or many of the three keys of the package as presented on
  page~\pageref{sec:keys}.

  To stick to established convention the name of this macro is created from the
  name of the package (reduced to \TeX{} letters) followed by \emph{setup}. I
  apologize for that strange linguistic mixture.
\end{function}

\newpage{}
\subsection{Shortcuts}
\label{sec:shortcuts}

There are also two \emph{shortcut} macros:

\begin{function}{\matid}
  \begin{syntax}
    \csprint{matid}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\oarg{coefficient}\marg{number of columns}
  \end{syntax}
  which write the identity matrix --- by default --- with \meta{number of
    columns} columns. If \meta{coefficient} is given, we obtain a diagonal
  matrix with all its coefficients equal to \meta{coefficient}.

  The two first optional arguments have the same functionality as in the
  preceding macros.

  \emph{matid} stands for \emph{\foreignlanguage{French}{matrice identité}} which means
  ``identity matrix''.
\end{function}

\begin{function}{\matnulle}
  \begin{syntax}
    \csprint{matnulle}\parg{prefix}\darg{\string<}{clist of key-value pairs}{\string>}\oarg{coefficient}\marg{number of columns}
  \end{syntax}
  writes the null matrix with \meta{number of columns} columns ---by default---
  or a matrix containing with \meta{number of columns} columns and all
  coefficients equal to \meta{coefficient}.

  The two first optional arguments have the same functionality as in the
  preceding macros.

  \emph{matnulle} stands for \emph{\foreignlanguage{French}{matrice nulle}} which means
  ``null matrix''.
\end{function}

\begin{VerbatimOut}[gobble=0]{code00.tex}
$\matid{3}$\qquad $\matid[9]{3}$
\par \medskip
$\matnulle{3}$\qquad $\matnulle[2]{3}$
\end{VerbatimOut}
\UnExemple{}

\section{The Package Options}
\label{sec:keys}

The package uses key-value options. There are four keys:
\keyname{envir},
\keyname{prefix},
\keyname{typeord}
and
\keyname{argopt}.

\begin{description}
\item[\keyname{envir} (\textit{\texttt{string}})] the main and last part of the
  name of the environment used to print the matrix. Its initial value is
  \texttt{matrix}.\label{key-envir}

\item[\keyname{prefix} (\textit{\texttt{string}})] a string which is prefixed to
  \texttt{envir} to obtain the complete name of the environment. Its initial
  value is \texttt{p}. Therefore, by default, the environment used to print the
  matrix is \envir{pmatrix} as defined by \pkg{amsmath}.\label{key-prefix}

\item[\keyname{typeord} (\textit{\texttt{string}})] a string which is the
  ordinary ---i.~e. default--- value of the \meta{type} optional argument. The
  initial value of the key is \TypeMat{C}.\label{key-typeord}

\item[\keyname{argopt} (\textit{\texttt{token list}})] for French
  \foreignlanguage{french}{\emph{argument optionnel}} (optional argument). That
  key is initially void. See page~\pageref{sec:delarray} for usage.\label{key-argopt}
\end{description}

Moreover, an other key is available which is not an option of the package:
\keyname{out-of-box} which is a metakey.


 With
\lstinline|\simplesmatricessetup{out-of-box}|
 we obtain the same effect as with
\lstinline|\simplesmatricessetup{prefix=p,|\BOP\lstinline| envir=matrix,|%
  \BOP\lstinline| argopt=,|\BOP\lstinline| typeord=C}|.

\newpage{}
\section{Examples}
\label{sec:examples}
\subsection{Without any special package}
\label{sec:amsmath}

That is with only \pkg{amsmath} loaded as done by this package.

\subsubsection{Declaration and Usage}
\label{sec:declar}

\begin{VerbatimOut}[gobble=0]{code00.tex}
\declarermatrice{A}{1, 2, 300, 400}
$\declarermatrice*{B}{a, b, c, d}$
\quad $\lamatrice{A}$ \par \bigskip
$\lamatrice(V){A}$
\quad $\lamatrice(b){B}$
\end{VerbatimOut}
\UnExemple{}

On the first line of this example, the matrix~\texttt{A} is defined but not
printed. Then, on the second line, the matrix~\texttt{B} is defined and
printed. We can afterwards call the~\texttt{A} or \texttt{B}-matrix with
\csprint{lamatrice}.

\subsubsection{The ``\texttt{type}'' argument}
\label{sec:matrix-type}

The last but one --- optional --- argument of macros \csprint{matrice},
\csmute{declarermatrice} and \csprint{declarermatrice*}
defaults to \TypeMat{O}. In that case the value of the key
\keyname{typeord} is used to determine the \emph{type} of the matrix input,
see~\pageref{sec:matrix-type-key}.

Its value can be a number, in which case it is the number of columns, or a
string ---presently a one letter string--- among: \TypeMat{C}, \TypeMat{D},
\TypeMat{I}, \TypeMat{J}, \TypeMat{S}, \TypeMat{T} and \TypeMat{x}.


\medskip{}

 \TypeMat{C} means square matrix for French ``\texttt{C}arré'' means ``square''.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice{1, 2, 3, 4, 5, 6, 7, 8, 9}
 \quad
 \matrice[C]{1, 2, 3, 4, 5, 6, 7, 8, 9}
\]
\end{VerbatimOut}
\UnExemple{}

 \TypeMat{D} means \textbf{diagonal} matrix.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice{1, 2, 3, 4}\quad
 \matrice[D]{1, 2, 3, 4}
\]
\end{VerbatimOut}
\UnExemple{}

\TypeMat{I} means lower triangular matrix. French ``\texttt{I}nférieur'' means
``lower''; \TypeMat{J} means lower triangular matrix with zeros on the diagonal.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice[I]{1, 2, 3}\quad
 \matrice[J]{1, 2, 3}
\]
\end{VerbatimOut}
\UnExemple{}

\TypeMat{S} means upper triangular matrix. French ``\texttt{S}upérieur'' means
``upper''; \TypeMat{T} means upper triangular matrix with zeros on the diagonal.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice[S]{1, 2, 3}\quad
 \matrice[T]{1, 2, 3}
\]
\end{VerbatimOut}
\UnExemple{}

\TypeMat{x} means \texttt{xcas}\footnote{Free computer algebra system, see here:
\url{https://www-fourier.univ-grenoble-alpes.fr/~parisse/giac.html}.}. With that value, one can copy-paste from
\texttt{xcas} into the \LaTeX{} document.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice[x]{[[7,1,3],[1,0,3],[5,1,2]]}
\]
\end{VerbatimOut}
\UnExemple{}

\pagebreak[3]
A number sets up the number of columns of the matrix.
\begin{VerbatimOut}[gobble=0]{code00.tex}
\[
 \matrice[2]{1, 2, 3, 4, 5, 6}
 \quad
 \matrice[3]{1, 2, 3, 4, 5, 6}
\]
\end{VerbatimOut}
\UnExemple{}

\subsubsection{Using the ``\keyname{typeord}'' Key}
\label{sec:matrix-type-key}

In this document, the key \keyname{typeord} has the initial value of~\TypeMat{C}.


\medskip{}

\begin{VerbatimOut}[gobble=0]{code00.tex}
\simplesmatricessetup{typeord=3}
$\matrice{1, 2, 3, 4, 5, 6}$\quad
\simplesmatricessetup{typeord=2}
$\matrice{1, 2, 3, 4, 5, 6}$\quad
\simplesmatricessetup{typeord=C}
$\matrice{1, 2, 3, 4}$
\end{VerbatimOut}
\UnExemple{}

The preceding code is but a mere illustration. In order to declare a type for
one matrix it's more convenient to use the optional argument. However if one
wants to write a lesson about upper triangular matrices\dots{}

\subsubsection{Using the \csprint{(La)MatriceInterieur} Macros}
\label{sec:matint}

\begin{VerbatimOut}[gobble=0]{code00.tex}
\declarermatrice{B}[4]{
 \hline ein, zwei, drei, vier,
 \hline unos, dos, tres, cuatro,
 \hline unu, du, tri, kvar}%
\declarermatrice{A}[3]{
 one, two, three,
 un, deux, trois,
 uno, due, tre}%
\begin{tabular}{*{3}{l}}
  \MatriceInterieur %*@\label{anonyme}@*)
\end{tabular}
\par \bigskip *** \par \bigskip
\begin{tabular}{*{3}{l}}
  \LaMatriceInterieur{A} %*@\label{parnom}@*)
\end{tabular}
\par \bigskip
\begin{tabular}{|*{4}{l|}}
  \LaMatriceInterieur{B}\hline
\end{tabular}
\end{VerbatimOut}
\UnExemple{}

On lines~\ref{anonyme} and~\ref{parnom} we use the same \emph{internal} because
the \texttt{A}-matrix is the last defined before line~\ref{anonyme}.

\subsection{Changing the Look}
\label{sec:changelook}

We can load other packages which deal with matrices such as \pkg{mathtools},
\pkg{delarray} or \pkg{nicematrix}. In that case we can change the look of our
matrices thanks to the option-key \keyname{envir}, \keyname{prefix} and
\keyname{argopt}.

This document loads the three packages \pkg{mathtools}, \pkg{delarray} and
\pkg{nicematrix} in order to give the following examples.



\subsubsection{With \pkg{mathtools}}
\label{sec:mathtools}

As already stated above there are two ways to use the option-keys: through
\csmute{simples}\BOP\ObjetPres{cmdmuette}{matrices}\BOP\ObjetPres{cmdmuette}{setup}
or through the optional argument \darg{\string<}{clist of pairs of
  key-value}{\string>}. I show both.

\begin{VerbatimOut}[gobble=0]{code00.tex}
\simplesmatricessetup{
  envir=matrix*, prefix=p, argopt=[r]}
$\matrice{1, 2, 30, 40}$ \quad
\simplesmatricessetup{out-of-box}
$\matrice{1, 2, 30, 40}$
\par \medskip
$\matrice(b)<envir=matrix*,argopt=[l]> {1, 2, 30, 40}$
\end{VerbatimOut}
\UnExemple{}

\subsubsection{With \pkg{delarray}}
\label{sec:delarray}


\begin{VerbatimOut}[gobble=0]{code00.tex}
\simplesmatricessetup{
  envir=array, prefix=,
  argopt=[c][{l r}\}}
$\matrice{1, 2, 30, 40}$ \quad
$\matid()<envir=array,
  argopt={[c][{@{}c c c@{}}\}}>{3}$
\end{VerbatimOut}
\UnExemple{}
\simplesmatricessetup{out-of-box}

\subsubsection{With \pkg{nicematrix}}
\label{sec:nicematrix}

\begin{VerbatimOut}[gobble=0]{code00.tex}
$\matrice<prefix=b, envir=NiceArray,
argopt={ {cc||rl} }>{1, 2, 3, 4, 5, 6,
  7, 8, 9, 10, 11, 12, 13, 14, 15, 16}$
\end{VerbatimOut}
\UnExemple{}

In that case we need to double the curly braces around the column descriptors of
the \envir{array} environment because the first level is stripped off by
\LaTeX{} when valuating the key.

\medskip{}

In the following example, for the second matrix, it is not necessary to surround
the value of \keyname{argopt} with an extra pair of curly braces (see
line~\ref{sansaccol}) ---even if it is not dangerous (see
line~\ref{avecaccol})--- for the presence of \texttt{[margin]} prevents \LaTeX{}
to strip off the curly braces.

\begin{VerbatimOut}[gobble=0]{code00.tex}
$\matrice<prefix=b, envir=NiceArray,
argopt={{l c r}}>{1, 2, 3, 40, 50, 60,
  700, 800, 900}$
\par \medskip
$\matrice<prefix=b, envir=NiceArray,
argopt={l c r}[margin]>{1, 2, 3, %*@\label{sansaccol}@*)
  40, 50, 60, 700, 800, 900}$
\par \medskip
$\matrice<prefix=b, envir=NiceArray,
argopt={{l c r}[margin]}>{1, 2, 3, %*@\label{avecaccol}@*)
  40, 50, 60, 700, 800, 900}$
\end{VerbatimOut}
\UnExemple{}

 \section{Thanks}
Many thanks to Denis \textsc{Bitouzé} for his remarks about the initial state of
this documentation. Thanks to him, some colours have appeared on these pages.

\vspace{\stretch{2}}

\noindent\hspace*{0.2\textwidth}\hrulefill\hspace*{0.2\textwidth}
\begin{center}
  \textsl{Le TeXnicien de Surface scripsit.}
\end{center}
\noindent\hspace*{0.2\textwidth}\hrulefill\hspace*{0.2\textwidth}

\vspace*{\stretch{2}}

\end{document}
\endinput
%%
%% End of file `simples-matrices-eng.tex'.
