<!-- Time-stamp: <2022-07-03 11:22:16 Yvon Henel (TdS)> -->

# simples-matrices

Author: Yvon Henel, aka Le TeXnicien de surface
([contact](le.texnicien.de.surface@yvon-henel.fr))

## LICENCE

This material is subject to the LaTeX Project Public License. 
See http://www.ctan.org/license/lppl1.3c 
for the details of that license.

© 2022 Yvon Henel (Le TeXnicien de surface)

## Description of the Package

This package provides macros to define and write matrices the
coefficients of which are given, row by row, in a list of values
separated by commas.

## History

* Version 1.0 — 2022-06-19 — First public version

* Version 1.0.1 — 2022-07-03 — Some improvements in the documentation.
