<!-- Time-stamp: <2022-07-03 11:22:22 Yvon Henel (TdS)> -->

# simples-matrices

Auteur: Yvon Henel, alias _Le TeXnicien de surface_
([contact](le.texnicien.de.surface@yvon-henel.fr))

## LICENCE

Le contenu de cette archive est placé sous la « LaTeX Project Public License ».
Voir http://www.ctan.org/license/lppl1.3c 
pour la licence.

© 2022 Yvon Henel (Le TeXnicien de surface)

## Description de l’extension

Cette extension fournit des commandes pour définir et écrire des
matrices en donnant leurs coefficients, par ligne, dans une liste de
valeurs séparées par des virgules.

## Historique

* Version 1.0 — 2022-06-19 — Première version publique

* Version 1.0.1 — 2022-07-03 — Quelques améliorations dans la documentation.
