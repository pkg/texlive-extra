# README #

Writing a TeX length with \the writes the value and
the unit without a space. Package isopt provides
a macro \ISO which inserts a user defined space
between number and  unit.


This program can be redistributed and/or modified under the terms
of the LaTeX Project Public License Distributed from CTAN archives
in directory macros/latex/base/lppl.txt.
