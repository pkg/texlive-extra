%!TEX encoding = UTF-8 Unicode 
%!TEX TS-program = lualatex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Esempio di composizione con XeLaTeX o LuaLatex
\documentclass[%
corpo=12pt,
twoside,
tipotesi=magistrale,
]{toptesi}\errorcontextlines=100
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Impostazione dei font e delle lingue                                               %
\usepackage{fontspec}                                                                %
\setmainfont[Ligatures=TeX]{TeX Gyre Termes}% font Times                             %
\setsansfont[Ligatures=TeX, Scale=MatchLowercase]{TeX Gyre Heros}% font Helvetica    %
\setmonofont{UM Typewriter}% font typewriter un po' più scuri del font Courier       %
\usepackage{amsmath} % per la matematica estesa                                      %
\usepackage[math-style=ISO]{unicode-math}% per la matematica UNICODE
                     % specify math-style=ISO for ISO compliant math
                     % specify math-style=TeX for TeX standard style
\setmathfont{XITS Math}[partial=upright]% font matematici UNICODE che
%                                          van bene con i Times

\setotherlanguage[variant=ancient]{greek}% questa variante col patch
%  nel file gloss-greek.ldf funziona anche con LuaLaTeX; senza patch
% funziona solo con XeLaTeX e parzialmente con pdfLaTeX
%
\newfontfamily{\greekfont}{GFS Didot}                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{pict2e}% per togliere ogni limitazione all'ambiente picture

%%%%%%%%% da caricare per ultimo e solo se non e' gia' stato caricato
\expandafter\csname @ifpackageloaded\endcsname{hyperref}{}{\usepackage{hyperref}}

\hypersetup{%
    pdfpagemode={UseOutlines},
    bookmarksopen,
    pdfstartview={FitH},
    colorlinks,
    linkcolor={blue},
    citecolor={red},
    urlcolor={blue}
  }
%%%%%%%%%% fine impostazioni di hyperref
  
\begin{document}


\newtheorem{osservazione}{Osservazione}% Standard LaTeX


%\setbindingcorrection{3mm}% impostazione per la cucitura

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{ThesisTitlePage}*
\ateneo{Università degli Studi di Einstenia}% nome generico dell'Universita'
\nomeateneo{Weiss Turm}% eventuale nome proprio dell'universita'
\logosede{logouno}% logo dell'universita'
\FacoltaDi{Facoltà di\space}% Etichetta della facolta'
\facolta[III]{Matematica, Fisica\\e Scienze Naturali}% facolta'
\Materia{Remote sensing}
\titolo{La pressione barometrica di~Giove}% 
\sottotitolo{Metodo dei satelliti medicei}% 
\corsodilaurea{Astronomia Applicata}% per la laurea
\candidato{Galileo \textsc{Galilei}}% per tutti i percorsi
\secondocandidato{Evangelista \textsc{Torricelli}}% 
\relatore{prof.\ Albert Einstein}% per la laurea e/o il dottorato
\secondorelatore{dipl.~ing.~Werner von Braun}% per la laurea 
\tutoreaziendale{dott.\ ing.\ Giovanni Giacosa}
\NomeTutoreAziendale{Supervisore aziendale\\Centro Ricerche FIAT}

\sedutadilaurea{\textsc{Anno~accademico} 1615-1616}% per la laurea magistrale
%\annoaccademico{1615-1616}% solo con l'opzione classica
%\annoaccademico{2007}% idem
%\ciclodidottorato{XV}% per il dottorato
%
%\chapterbib %solo per sperimentare; e' preferibile comporre una sola bibliografia
%\AdvisorName{Supervisors}
\retrofrontespizio{Questo testo è sottoposto alla Creative Common Licence

    \vspace{2\baselineskip}

    Stampato il \today
}
\end{ThesisTitlePage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\sommario% compone il sommario

La pressione barometrica di Giove viene misurata
mediante un metodo originale  messo a punto dai candidati, che si basa
sul rilevamento telescopico della pressione.

\ringraziamenti% compone i ringraziamenti che di solito sono inutili

I candidati ringraziano vivamente il Granduca di Toscana per i mezzi
messi loro a disposizione, ed il signor Von Braun, assistente del
prof.~Albert Einstein, per le informazioni riservate che egli ha
gentilmente fornito loro, e per le utili discussioni che hanno permesso ai candidati di evitare di riscoprire l'acqua calda.

\tablespagetrue\figurespagetrue % usare solo se queste liste sono necessarie
\indici% stampa i vari indici

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Altro esperimento da non ripetere MAI <------- !
% Qui lo si fa per mostrare come viene composto  il testo greco
% inserito direttamente in greco nel file sorgente
\begin{citazioni}
    \textit{testo testo testo\\testo testo testo}

    [\textsc{G.\ Leopardi}, Operette Morali]

    \begin{greek}
    ἀλλὰ πάντα ὁ κέραυνος δ' ὀιακίζει
    \end{greek}

    [\textsc{Eraclito}, fr.\ D-K 134]
\end{citazioni}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\mainmatter % ridondante ma non guasta

\chapter{Introduzione generale}

\section{Principi generali}
Il problema della determinazione della pressione barometrica dell'atmosfera di
Giove non ha ricevuto finora una soluzione soddisfacente, per l'elementare
motivo che il pianeta suddetto si trova ad una distanza tale che i mezzi attuali
non consentono di eseguire una misura \emph{in loco}.

Conoscendo però con grande precisione le orbite dei satelliti principali di
Giove, e segnatamente le orbite dei satelliti medicei, è possibile eseguire
delle misure indirette, che fanno ricorso alla nota formula \cite{gal}:
\[
\Phi = K\frac{\Xi^2 +\Psi\ped{max}}{1+\gei\Omega}
\]
dove le varie grandezze hanno i seguenti significati:
\begin{enumerate}
\item
$\Phi$ angolo di rivoluzione del satellite in radianti se $K=1$, in gradi se
$K=180/\pi$;
\item
$\Xi$ eccentricità dell'orbita del satellite; questa è una grandezza priva
di dimensioni;
\item
$\Psi\ped{max}$ rapporto fra il semiasse maggiore ed il semiasse minore
dell'orbita del satellite, nelle condizioni di massima eccentricità;
poiché le dimensioni di ciascun semiasse sono $[l]=\unit{km}$, la grandezza
$\Psi\ped{max}$ è adimensionata;
\item
$\Omega$ velocità istantanea di rotazione; si ricorda che è $[\Omega]=%
\unit{rad}\unit{s}^{-1}$;
\item bisogna ancora ricordarsi che $10^{-6}\unit{m}$ equivalgono a
1\unit{\micro m}.
\end{enumerate}
%

Le grandezze in gioco sono evidenziate nella figura \ref{fig1}.
\begin{figure}[ht]\centering
\setlength{\unitlength}{0.01\textwidth}
\begin{picture}(40,30)(30,0)
\put(50,15){\scalebox{1}[0.7]{\circle{20}}}
\put(48.5,15){\circle*{1}}
\put(30,0){\framebox(40,30){}}
\end{picture}
\caption{Orbita del generico satellite; si noti l'eccentricità dell'orbita rispetto al pianeta.\label{fig1}}
\end{figure}

Per misurare le grandezze che compaiono in questa formula è necessario
ricorrere ad un pirometro con una resistenza di 120\unit{M\ohm}, altrimenti gli
errori di misura sono troppo grandi, ed i risultati completamente falsati.

\unless\ifPDFTeX
Ci si ricordi anche di espressioni del tipo
\begin{equation}
\nabla \symbf{E}= - \frac{\partial \symbf{B}}{\partial t}
\label{equ:isomath}
\end{equation}
compilabili solo con LuaLaTeX o XeLaTeX oppure anche con pdfLaTeX pur di caricare i pacchetti giusti e i font giusti per la matematica conforme con le norme ISO.

Nell'espressione~(\ref{equ:isomath}) il simbolo di derivata parziale discende dall'impostazione ``math-style=iso'' nella specifica del font matematico UNICODE usato; i simboli inclinati neri sono ottenuti con il comando \verb|\symbf| che agisce nello stesso modo sia sui simboli latini sia su quelli greci, a differenza di quanto succede con \LaTeX\ in condizioni normali. 

Per impostare ed usare correttamente i font matematici UNICODE è bene consultare la documentazione leggendo il file \texttt{unicode-math.pdf} già presente nella propria installazione aggiornata e completa del sistema \TeX.
\fi

\section{I satelliti medicei}
I satelliti medicei, come noto, sono quattro ed hanno dei periodi di rivoluzione
attorno al pianeta Giove che vanno dai sette giorni alle tre settimane.

Essi furono per la prima volta osservati da uno dei candidati mentre
sperimentava l'efficacia del tubo occhiale che aveva appena inventato
rielaborando una idea sentita di seconda mano da un viaggiatore appena arrivato
dai Paesi Bassi.
\chapter{Il barometro}
\section{Generalità}
\begin{interlinea}{0.87} Il barometro, come dice il nome, serve per
misurare la pesantezza; più precisamente la pesantezza dell'aria
riferita all'unità di superficie. Questo capoverso è composto con un fattore di interlinea pari a 0,87 e come si vede le righe sono troppo dense e si leggono male. Oltretutto la distanza effettiva delle successive linee di base di righe adiacenti non è costante e dipende dalla presenza di lettere con ascendenti e discendenti.
\end{interlinea}

\begin{interlinea}{2} Studiando il fenomeno fisico si può concludere
che in un dato punto grava il peso della colonna d'aria che lo
sovrasta, e che tale colonna è tanto più grave quanto maggiore
è la superficie della sua base; il rapporto fra il peso e la base
della colonna si chiama pressione e si misura in once toscane al cubito
quadrato, \cite{tor1}; nel Ducato di Savoia la misura in once al piede
quadrato è quasi uguale, perché colà usano un piede molto
grande, che è simile al nostro cubito. Questo capoverso è composto a spaziatura doppia (fattore di interlinea pari a 2) ed è semplicemente penoso.
\end{interlinea}

\subsection{Forma del barometro}

\begin{interlinea}{1.5}
Il barometro consta di un tubo di vetro chiuso ad una estremità e
ripieno di mercurio, capovolto su di un vaso anch'esso ripieno di
mercurio; mediante un'asta graduata si può misurare la distanza fra
il menisco del mercurio dentro il tubo e la superficie del mercurio
dentro il vaso; tale distanza è normalmente di 10 pollici toscani,
\cite{tor1,tor2}, ma la misura può variare se si usano dei pollici
diversi; è noto infatti che gl'huomini sogliono avere mani di
diverse grandezze, talché anche li pollici non sono egualmente
lunghi. Questo capoverso è composto con un fattore di interlinea pari a 1,5; l'effetto è brutto, ma per delle bozze da stampare su carta e da correggere a mano può bastare.
\end{interlinea}
\section{Del mercurio}
Il mercurio è un a sostanza che si presenta come un liquido, ma ha il colore
del metallo. Esso è pesantissimo, tanto che un bicchiere, che se fosse pieno
d'acqua, sarebbe assai leggero, quando invece fosse ripieno di mercurio,
sarebbe tanto pesante che con entrambe le mani esso necessiterebbe di essere
levato in suso.

Esso mercurio non trovasi in natura nello stato nel quale è d'uopo che sia
per la costruzione dei barometri, almeno non trovasi così abbondante come
sarebbe necessario.

\setcounter{footnote}{25}

Il Monte Amiata, che è locato nel territorio del Ducato%
\footnote{Naturalmente stiamo parlando del Granducato di Toscana.}
del nostro Eccellentissimo et Illustrissimo Signore Granduca di Toscana\footnote{Cosimo IV de' Medici.}, è uno dei luoghi della terra dove può rinvenirsi in gran copia un sale rosso, che nomasi \emph{cinabro}, dal quale con artifizi alchemici, si estrae il mercurio nella forma e nella consistenza che occorre per la costruzione del barometro terrestre.

La densità del mercurio è molto alta e varia con la temperatura come
può desumersi dalla tabella \ref{t:1}.


Il mercurio gode di una sorprendente qualità e proprietà, cioè che esso diventa tanto solido da potersene fare una testa di martello et infiggere chiodi aguzzi nel legname.
\begin{table}[htp]      % crea un floating body col nome Tabella nella
                        % didascalia
\centering              % comando necessario per centrare la tabella
\begin{tabular}%        % inizio vero e proprio della tabella
{rrrrrr}                % parametri di incolonnamento
\hline\hline            % filetti orizzontali sopra la tabella
                        % intestazione della tabella
\multicolumn{3}{c}{\rule{0pt}{2.5ex}Temperatura} % \rule serve per lasciare
& \multicolumn{3}{c}{Densit\`a} \\  % un po' di spazio sopra le parole
    &\multicolumn1c{\unit{\gradi C}} & & 
    &\multicolumn1c{$\unit{t/m^3}$} &  \\
\hline%              % Filetto orizzontale per separare l'intestazione
\hspace*{1.3em}& 0  &  & & 13,8 &  \\   % I numeri sono incolonnati  
              & 10  &  & & 13,6 &  \\   % a destra; le colonne vuote
              & 50  &  & & 13,5 &  \\   % servono per centrare le colonne
              &100  &  & & 13,3 &  \\   % numeriche sotto le intestazioni
\hline \hline                           % Filetti di fine tabella
\end{tabular}
\caption[Densit\`a del mercurio]{Densit\`a del mercurio. Si può fare molto meglio usando il pacchetto \textsf{booktabs}.}  % didascalia
\label{t:1}                                           % label
\end{table}

%\selectlanguage{italian}

\begin{osservazione}\normalfont
Questa proprietà si manifesta quando esso è estremamente freddo, come quando lo si immerge nella salamoia di sale e ghiaccio che usano li siciliani per confetionare i sorbetti, dei quali sono insuperabili artisti.
\end{osservazione}

Per nostra fortuna, questo grande freddo, che necessita per la confetione de li sorbetti, molto raramente, se non mai, viene a formarsi nelle terre del Granduca Eccellentissimo, sicché non vi ha tema che il barometro di mercurio possa essere ruinato dal grande gelo e non indichi la pressione giusta, come invece deve sempre fare uno strumento di misura, quale è quello che è descritto costì.




\begin{thebibliography}{9}
\bibitem{gal} G.~Galilei, {\em Nuovi studii sugli astri medicei}, Manunzio,
        Venetia, 1612.
\bibitem{tor1} E.~Torricelli, in ``La pressione barometrica'', {\em Strumenti
        Moderni}, Il Porcellino, Firenze, 1606.
\bibitem{tor2} E.~Torricelli e A.~Vasari, in ``Delle misure'', {\em Atti Nuovo
        Cimento}, vol.~III, n.~2 (feb. 1607), p.~27--31.
\end{thebibliography}




\end{document}
