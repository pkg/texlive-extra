# The statistics package

The `statistics` package can compute and typeset statistics like
frequency tables, cumulative distribution functions (increasing or decreasing,
in frequency or absolute count domain), from the counts of individual values,
or ranges, or even the raw value list with repetitions.

It can also compute and draw a bar diagram in case of individual values, or,
when the data repartition is known from ranges, an histogram or the continuous
cumulative distribution function.

You can ask `statistics` to display no result, selective results or all
of them. Similarly `statistics` can draw only some parts of the graphs.
Every part of the generated tables or graphics is customizable.


(C) Copyright 2014-2019 RIVAUD Julien

This package may be distributed and/or modified under the conditions of the
General Public License (GPL), either version 3 of this license or (at your
option) any later version.
