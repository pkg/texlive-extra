 gamebook package for LaTeX

 Copyright (C) 2011 by Andr\'e Miede, http://www.miede.de
 
 This file may be distributed and/or modified under the
 conditions of the LaTeX Project Public License, either version 1.3
 of this license or (at your option) any later version.
 The latest version of this license is in:

    http://www.latex-project.org/lppl.txt

 and version 1.3 or later is part of all distributions of LaTeX 
 version 2005/12/01 or later.
 
 This package provides the means in order to layout gamebooks with \LaTeX. 
 If you do not know what a gamebook is, just have a look at the informative 
 Wikipedia article \url{http://en.wikipedia.org/wiki/Gamebook} or check 
 out Demian's very good overview website, where you can find tons of information 
 about gamebooks: \url{http://www.gamebooks.org}.

 A simple gamebook example is included with this package, teaching you how to 
 use the package in no time.

 If you created a gamebook using this package, please drop me an e-mail 
 (ideally, send an PDF of the gamebook along). Thanks in advance.