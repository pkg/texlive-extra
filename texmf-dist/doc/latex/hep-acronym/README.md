
# The `hep-acronym` package

An acronym extension for glossaries

## Introduction

The `hep-acronym` package provides an acronym macro based on the `glossaries` package.
It can be loaded using `\usepackage{hep-acronym}`.

## Author

Jan Hajer

## License

This file may be distributed and/or modified under the conditions of the `LaTeX` Project Public License, either version 1.3c of this license or (at your option) any later version.
The latest version of this license is in `http://www.latex-project.org/lppl.txt` and version 1.3c or later is part of all distributions of LaTeX version 2005/12/01 or later.

