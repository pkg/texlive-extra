LaTeX Package : glossaries-extra v1.50 (2022-11-08)

Author        : Nicola Talbot
		https://www.dickimaw-books.com/contact

LICENCE

This material is subject to the LaTeX Project Public License. 
See http://www.ctan.org/license/lppl1.3 
for the details of that license.

Copyright 2015-2022 Nicola Talbot

DESCRIPTION

This package provides improvements and extra features to the 
glossaries package. Some of the default glossaries.sty behaviour 
is changed by glossaries-extra.sty. See the user manual
glossaries-extra-manual.pdf for further details.

Requires the glossaries package (and, naturally, all packages 
required by glossaries.sty).

The file example-glossaries-xr.tex contains dummy
entries with cross-references that may be used for creating
minimal working examples for testing the glossaries-extra
package. (The base glossaries package provides additional files,
but this one needs glossaries-extra.sty.)

SEE ALSO:

https://ctan.org/pkg/glossaries
https://ctan.org/pkg/bib2gls
