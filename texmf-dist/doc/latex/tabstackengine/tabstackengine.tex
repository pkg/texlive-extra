\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[TABcline]{tabstackengine}
\usepackage{verbatimbox}
\usepackage{xcolor}
 \parindent 0in\parskip 1em \newlength\dotscale


% FOR CLOSE-BOXING ARGUMENTS \fboxsep=0pt\fboxrule=.6pt
\def\rl{\rule[-.3pt]{2ex}{.6pt}} \def\tst{\textsf{tabstackengine}}
\def\ste{\textsf{stackengine}}   \def\loi{\textsf{listofitems}}
\def\bs{\texttt{\char'134}}      \let\vb\verb
\reversemarginpar \marginparwidth 1.6in
\newcommand\margtt[1]{\marginpar{\hfill\ttfamily#1}}
\newcommand\margcmd[1]{\marginpar{\hfill\ttfamily\char'134#1}}
\newcommand\cmd[1]{\texttt{\char'134#1}}

\catcode`>=\active %
\catcode`<=\active %
\catcode`!=\active %
\def\vbspecial#1>{\textit{\rmfamily#1}}
\def\vbdelim{\catcode`<=\active \catcode`>=\active \catcode`!=\active \def<{\vbspecial}\def>{}\def!{\stringrms}}
\catcode`!=12 %
\catcode`>=12 %
\catcode`<=12 %
\def\endvbdelim{\catcode`>=12 \catcode`<=12 }

\def\stringrms#1{%
  \ifx1#1\relax\textrm{(provided by \ste)}\fi%
  \ifx2#1\relax\textrm{(only with \texttt{TABcline} package option)}\fi%
}
\begin{document}

\begin{center} \LARGE The {\tst} Package\\ \rule{0em}{.7em}\small
Front-end to the {\ste} package, allowing tabbed stacking\\ \rule{0em}{2.7em}\large
Steven B. Segletes\\ steven.b.segletes.civ@mail.mil\\
\rule{0em}{1.7em}\today\\ \tabstackengineversionnumber \end{center}

\tableofcontents

\section{Introduction}

The {\tst} package provides a front end to the {\ste} package that
allows for the use of tabbing characters within the stacking arguments.
\textbf{Familiarity with the syntax of the {\ste} package is assumed.}
When invoked, {\tst} loads the {\ste} package and initializes it in
such a way that the end-of-line (EOL)
character in certain stacking arguments will be taken, by default, 
as \vb|\\|, rather
than a space (which is the default EOL separator in \ste).
The EOL separator can be changed using \ste{}'s \cmd{setstackEOL} macro.

With \tst, command variations are introduced to allow several variants
of tabbing within the macro arguments. The default tabbing character is
the ampersand (\vb|&|); however, the tabbing character can be reset to
other tokens using the \vb|\setstackTAB| macro.

\begin{sloppypar}
In most cases (where it makes sense), a {\ste} macro name may be
prepended with the word \texttt{tabbed}, \texttt{align}, or
\texttt{tabular} to create a new {\tst} macro that allows for tabbed
arguments.
\end{sloppypar}

\section{Modes and Styles of \tst}

\begin{sloppypar}
Like the \ste{} package which provides the modes \vb|\stackText| and
  \vb|\stackMath|, the \tst{} package provides the modes
  \vb|\TABstackText| and  \vb|\TABstackMath|.\margcmd{TABstackText}%
  \margcmd{TABstackMath}  
However, the \tst{} package honors the underlying mode of \ste, and
  so if either \vb|\stackMath| or \vb|\TABstackMath| are set, all
  TABstacking arguments will be processed in math mode.%
  \footnote{The one exception here is if \ste{} macros are embedded (nested)
  inside \tst{} macro arguments.  In this case, the embedded \ste{} macro will
  only respond to the \ste{} mode, and not the \tst{} mode.}
So what are the differences?\vspace{-\parskip}
\begin{itemize}
\item \vb|\TABstackMath| and \vb|\TABstackText| are local settings, whereas
  \vb|\stackMath| and \vb|\stackText| are global settings.
\item As of version 2.00, 
  \tst{} provides the means to add additional styles to a stack, 
  associated with \vb|\TABstackMath| and \vb|\TABstackText|.
  In particular,
  the macros \vb|\TABstackMathstyle| and \vb|\TABstackTextstyle|%
  \margcmd{TABstackMathstyle}\margcmd{TABstackTextstyle}
  can be used to add custom styles to stacks.  For example, 
  \vb|\TABstackMathstyle{\displaystyle}| will cause all stacked items
  processed in TABstack-math mode to be set in display style.  Likewise
  \vb|\TABstackTextstyle{\scriptsize}| will cause all stacked items
  processed in TABstack-text mode to be set in script size.
  Styles (for both math and text modes) can be cleared with the command
  \vb|\clearTABstyle|\margcmd{clearTABstyle}.
\end{itemize}
\end{sloppypar}


\section{Tabbing Variations within \tst}

The {\tst} package syntax allows three types of tabbing variation
denoted by the words \texttt{tabbed}, \texttt{align}, and
\texttt{tabular} in the macro name itself. In the case of
\texttt{tabbed} macros, the tabbed columns all share the same alignment,
as dictated by the \vb|\stackalignment| setting or perhaps provided as
an optional argument in some macro forms.

In the case of \texttt{align} macros, the alignment in columns is
alternately specified as right, then left, \textit{etc.}, in the manner
of the \texttt{align} environment of the \textsf{amsmath} package.

Finally, in the case of \texttt{tabular} macros, an extra argument is
passed to the macro that specifies the left-center-right alignment for
each individual column, in the manner of \vb|{lccr}|.

\section{Column Spacing within \tst}

Intercolumn space can be introduced to {\tst} output in one of two ways.
\margcmd{fixTABwidth}First, there is a macro setting to force all columns 
  to be the same width (namely, the width of the widest entry in the stack), 
  using the syntax
\vb|\fixTABwidth{T| \textit{or} \vb|F}|.
The default is \texttt{F}.
When set true, additional column space will be introduced to all but the widest
  column of a stack, so as to make all columns of a width equal to that
  of the widest column.

Secondly,\margcmd{setstacktabbedgap} 
each of the tabbing variations has the means to introduce a
fixed amount of space between columns.  By default, the \texttt{tabbed}
stacking macros add no space (\texttt{0pt}) between adjacent columns, 
but this value can be reset with the macro 
\vb|\setstacktabbedgap{|\textit{length}\vb|}|.

In\margcmd{setstackaligngap}  
the case of the \texttt{align} stacking macros, there is never any 
  gap introduced after the right-aligned (odd-numbered) columns.  
However, the default gap introduced after the left-aligned (even-numbered) 
  columns is, by default, \texttt{1em} (the same gap as \cmd{quad}).  
It can be reset with the macro \vb|\setstackaligngap{|\textit{length}\vb|}|.

For\margcmd{setstacktabulargap} 
the \texttt{tabular} stacks, the default intercolumn gap is the
value of \cmd{tabcolsep}.  The default value may be reset with the macro
\vb|\setstacktabulargap{|\textit{length}\vb|}|.

Note that these \cmd{setstack...gap} macros are for setting horizontal
gaps between columns of a stack.  They should not be confused with
the \cmd{setstackgap} macro of {\ste} that sets the vertical gap
for long and short stacks.

\section{Horizontal Rules}\label{s:hr}

As of version 2.10, \tst{} provides the facility to lay down horizontal rules 
  that fill up the TABstack cell width.
There are two ways that this may be accomplished.
First, the macro \vb|\TABrule[<|\textit{vertical shift}\vb|>]|%
\margcmd{TABrule}
  may be specified as the entry in a cell, in order to place a horizontal 
  rule, of thickness \vb|\fboxrule| across the width of the cell.%
\footnote{The width of the \cmd{TABrule} will actually be the width of
  the widest data in that column.  This has implications when the
  \cmd{fixTABwidth} switch is set true, in that some columns will
   be typeset wider than their widest data.}
The optional argument specifies the vertical shift of the rule.
The default vertical shift, initially 0pt, may also be specified in the 
  length \cmd{TABruleshift}.\margcmd{TABruleshift}

So, for example,

\vb|\tabbedstackon{a&bb&CCC&dddd}{&\TABrule&&\TABrule}|

 will yield this result:
  \tabbedstackon{a&bb&CCC&dddd}{&\TABrule&&\TABrule}.
Note that the vertical spacing around the \vb|\TABrule| will conform
  to the stackengine rules set by \vb|\setstackgap|, for both ``short'' and
  ``long'' stacks.
In essence, the \vb|\TABrule|s literally constitute their own row in the stack.

A \vb|\TABrule| may also constitute a cell that contains actual [non-rule]
  data elsewhere on the row.
So, for example, 

\vb|\tabbedLongstack{aa&\TABrule\\\TABrule&bbb}| 

produces the result \tabbedLongstack{aa&\TABrule\\\TABrule&bbb}.

If a row of the TABstack is to be composed solely of rules, then these rules 
  can also be achieved in a second way.

\subsection{The \texttt{TABcline} Package Option}\label{s:rtx}

There is also introduced in version 2.10, a package option \vb|TABcline|,
  invoked in the standard way via \vb|\usepackage[TABcline]{tabstackengine}|.
This option reduces the efficiency of the package slightly, but provides
  two macros that can enhance convenience.
The two macros that are activated with this package option are \vb|\TABcline{}|%
\margcmd{TABcline}
  and \vb|\relaxTABsyntax|.
The macro 

\vb|\TABcline{<|\textit{col1}\vb|>,<|\textit{col2}\vb|>,<|%
  \textit{col3--col4}\vb|>,...}|

provides a shorthand notation for creating a row of \vb|\TABrule|s. 
For example, in a five-column TABstack, the shorthand

\vb|\TABcline{1,3-4}|

is equivalent to (assuming column separator as \vb|&|, spaces 
inserted for clarity)

\vb|\TABrule & & \TABrule & \TABrule &|

Furthermore, if the \vb|\TABcline| occurs prior to the last row of the 
  TABstack, an end-of-line (EOL) token (\textit{e.g.}, \vb|\\|) is also
  suffixed to the replacement.
Thus,

\vb|\tabbedShortstack{\TABcline{2-3}\TABcline{1,3-4}a&bb&CCC&dddd}|

produces \tabbedShortstack{\TABcline{2-3}\TABcline{1,3-4}a&bb&CCC&dddd}.

When the \texttt{TABcline} package option is selected,
  the \vb|\relaxTABsyntax|%
\margcmd{relaxTABsyntax}
  switch may also be invoked to address an issue of 
  bad syntax.
Some users have a tendency to tack a trailing stack-EOL to the end of 
  an input argument, in the manner of \vb|\tabbedLongstack{a&bb\\cc&d\\}|.
This rightfully provokes a \loi{} invalid-index error in \tst{}, because 
  the syntax implies the creation of a third row of a 2-column TABstack, 
  for which no column separators have been specified.

However, some may find the error message difficult to understand, especially
  because similar syntax is benign in the \texttt{tabular} environment and
  non-fatal in the \texttt{align} family of environments.
If the switch \vb|\relaxTABsyntax| is in force, the compilation will instead 
  succeed, with the trailing row of the TABstack left blank, in the manner
  of {\relaxTABsyntax\tabbedLongstack{a&bb\\cc&d\\}}.
Such syntax is still considered bad form, but by compiling without error,
  the visual result may make the source of the problem more obvious to the 
  user.
 
\clearpage
\section{Command Summary}

Below are the new TABstack making commands introduced by \tst.  
In the syntax shown below, when there are multiple commands delimited by 
  braces,  any one of the commands within the brace may be selected.

\vbdelim
\ttfamily\footnotesize
\(\left.\Centerstack[r]{%
  \textcolor{red}{\cmd tabbed}\\\\
  \textcolor{teal}{\cmd align}\\\\
  \textcolor{blue}{\cmd tabular}
}\right\}%
\left\{
\Centerstack[c]{Shortstack\\Shortunderstack\\Longstack\\
  Longunderstack\\Centerstack\\Vectorstack}
\right\}%
\left\{\Centerstack[c]{
  \textcolor{red}{[<alignment>]}\\\\
  \\\\
  \textcolor{blue}{\{<column alignments>\}}
}\right\}\)\{<tabbed EOL-separated string>\}

\(\displaystyle\$\left\{\Centerstack[r]{\cmd\\ \cmd paren\\\cmd brace\\\cmd bracket\\\cmd vert}\right\}\)%
\textcolor{red}{Matrix}stack[<alignment>]\{%
  <tabbed EOL-separated string>\}\$

\(\left.\Centerstack[r]{
  \textcolor{red}{\cmd tabbed}\\\\
  \textcolor{teal}{\cmd align}\\\\
  \textcolor{blue}{\cmd tabular}}\right\}%
\left\{
\Centerstack[c]{stackon\\stackunder\\stackanchor}
\right\}%
\mathtt{[<stack gap>]}\left\{\Centerstack[l]{
\\\\
\\\\
\textcolor{blue}{\{col.\ alignments\}}
}\right\}\)\{<tabbed anchor>\}\{\rlap{<tabbed~argument>\}}

\cmd setstack\(\left\{\Centerstack{
  \textcolor{red}{tabbed}\\\\
  \textcolor{teal}{align}\\\\
  \textcolor{blue}{tabular}
}\right\}\)gap\{<length>\}
\hspace{3em}\textrm{Initial Defaults:}\(\left\{\Centerstack{
  \textcolor{red}{0pt}\\\\
  \textcolor{teal}{1em}\\\\
  \textcolor{blue}{\cmd tabcolsep}
}\right\}\)

\rmfamily\normalsize

The ``\textit{tabbed EOL separated string}'' can contain not only regular 
  \LaTeX{} content, but
  it may also contain \cmd{TABrule}s and/or \cmd{TABcline}s as discussed 
  in section~\ref{s:hr}.
The macro\margcmd{ensureTABstackMath}\vspace{-8pt}
\begin{verbnobox}[\footnotesize\vbdelim]
\ensureTABstackMath{<commands involving TABstacks>}
\end{verbnobox}
\vspace{-8pt}
will force any \tst{} stacks within its argument to be 
  processed in math mode, even
  if the prevailing mode is otherwise \vb|\TABstackText|.
The package also provides a set of declarations that can be used to define the
  manner in which subsequent TABstacks will be processed:\vspace{-8pt}
\begin{verbnobox}[\footnotesize\vbdelim]
\fixTABwidth{T <or> F}
\TABstackMath
\TABstackText
\TABstackMathstyle{<directive>}
\TABstackTextstyle{<directive>}
\clearTABstyle
\setstackEOL{<end-of-line character>}      !1
\setstackTAB{<tabbing character>}
\TABunaryLeft    (\TABbinaryRight)
\TABunaryRight   (\TABbinaryLeft)
\TABbinary
\relaxTABsyntax                   !2
\end{verbnobox}
The following macros can be used for parsing tabbed data outside of a
  TABstack and also provide various stack metrics for the most recently 
  parsed \tst{} data.
\vspace{-6pt}
\begin{verbnobox}[\footnotesize\vbdelim]
\readTABstack{<tabbed EOL-separated string>}
\TABcellRaw[<row>,<column>]
\TABcell{<row>}{<column>}
\TABcellBox[<alignment>]{<row>}{<column>}
\getTABcelltoks[<row>,<column>]     \the\TABcelltoks
\TABcells{<row> or blank}
\TABstrut{<row>}
\TABwd{<column>}
\TABht{<row>}
\TABdp{<row>}
\end{verbnobox}
\vspace{-12pt}

\subsection{Command Examples}

Below we give examples of the various types of commands made available through
the \tst{} package.

\subsection*{Tabbed End-of-Line (EOL)-delimited Stacks}

Here, the optional argument \vb|[l]| defines the alignment of \textit{all} the
columns as ``left.''  The default alignment is \vb|[c]|.

\vb|\TABstackTextstyle{\scshape}|\\
\vb|\tabbedShortunderstack[l]{This& Is &The\\Time & Of&Man's\\ |\\
\vb|  Great&Dis&content}|

{\small\TABstackTextstyle{\scshape}%
  \tabbedShortunderstack[l]{This& Is &The\\Time & Of&Man's\\ 
  Great&Dis&content}}

Note that spaces around the arguments are absorbed and discarded.  Furthermore, the
text style has been set to \vb|\scshape|.

\subsection*{Align End-of-Line (EOL)-delimited Stacks}

In an \texttt{align}-stack, the column alignments will always be \vb|rlrl...|
The gap following the left-aligned columns is set by \vb|\setstackaligngap|.

\vb|$\ensurestackMath{Z:\left\{\alignCenterstack{%|\\
\vb|  y=&mx+b,&0=&Ax+By+C \\ y_1=&W_1,&y_2=&W_2}\right.}$|

{\small$\ensurestackMath{Z:\left\{\alignCenterstack{%
        y=&mx+b,&0=&Ax+By+C \\ y_1=&W_1,&y_2=&W_2}\right.}$}

\subsection*{Tabular End-of-Line (EOL)-delimited Stacks}

In a \texttt{tabular}-stack, the alignment of each column is specified in a separate
leading argument.

\vb|\stackText\tabularLongstack{rllc}{%|\\
\vb|  9)& $y_1=mx+b$ &linear&*\\10)& $y_2=e^x$ &exponential&[23]}|

{\small\stackText\tabularLongstack{rllc}{9)& $y_1=mx+b$ &linear&*\\10)& $y_2=e^x$ &exponential&[23]}}

\subsection*{Matrix Stacks}

The \texttt{Matrix}-stacks are tabbed variants of \ste's \texttt{Vector}-stacks.

\vb|\setstacktabbedgap{1.5ex}|\\
\vb|$I = \bracketMatrixstack{1&0&0\\0&1&0\\0&0&1}$|

{\small\setstacktabbedgap{1.5ex} $I = \bracketMatrixstack{1&0&0\\0&1&0\\0&0&1}$}

\subsection*{Tabbed Stack}

This variant of a \texttt{tabbed}-stack stacks exactly two items.  
The optional argument is a stacking gap,
as in the syntax of the \ste{} package.

\vb|\setstacktabbedgap{1ex}|\\
\vb|\tabbedstackon[4pt]{Jack&drove&the car&home.}{SN&V&DO&IO}|

{\small\stackText\setstacktabbedgap{1ex}\tabbedstackon[4pt]{Jack&drove&the car&home.}{SN&V&DO&IO}}

\subsection*{Align Stack}

This is for stacking two items with \texttt{rlrl...} alignment pattern.

\vb|\TABstackMath\setstackaligngap{3em}|\\
\vb|\alignstackunder[8pt]{y=&mx+b,&0=&Ax+By+C}{y_1=&W_1,&y_2=&W_2}|

{\small\TABstackMath\setstackaligngap{3em}
\alignstackunder[8pt]{y=&mx+b,&0=&Ax+By+C}{y_1=&W_1,&y_2=&W_2}}

\subsection*{Tabular Stack}

This is for stacking items with specifiable alignment pattern.

\vb|\TABbinary\TABstackMath\setstackgap{S}{0pt}\fboxrule=1pt\relax|\\
\vb|\tabularShortstack{lcr}{1 + 2(4-3) &=& 6 - 6/2\\|\\
\vb|  \TABcline{1-3}\kern6em & \triangle & \kern6em}|


{\small\TABbinary\TABstackMath\setstackgap{S}{0pt}\fboxrule=1pt\relax
\tabularShortstack{lcr}{1 + 2(4-3) &=& 6 - 6/2\\
  \TABcline{1-3}\kern6em & \triangle & \kern6em}}

Note the use of \vb|\TABbinary|, which applies a group to the beginning and
  end of each cell, in the event a binary treatment of leading/trailing
  operators is desired. 
So, in this case, a cell containing \vb|=| will be set as \vb|{}={}|.
In the absence of that declaration, the cell containing
the equal sign would have to have been explicitly defined as \vb|={}| 
  (in accordance with the \vb|\TABunaryLeft| default setting of the package).  
Negative vertical stacking gap, while not used here, is a perfectly 
  acceptable syntax and can be used to achieve overlap, if desired.

\subsection*{Fixed Tab Width (equal width columns, based on largest)}

With the \vb|\fixTABwidth| mode set\margcmd{fixTABwidth}, the stack 
  will have fixed-width columns, based on the overall widest entry.
Compare with versus without fixed width for the following TABstack.

\vb|\setstacktabbedgap{1ex}\fixTABwidth{T}%|\\
\vb|$\left(\tabbedCenterstack[r]{1&34&544\\4324329&0&8\\89&123&1}\right)$|\\
\vb|versus \fixTABwidth{F}%|\\
\vb|$\left(\tabbedCenterstack[r]{1&34&544\\4324329&0&8\\89&123&1}\right)$|

{\small\setstacktabbedgap{1ex}\fixTABwidth{T}
$\left(\tabbedCenterstack[r]{1&34&544\\4324329&0&8\\89&123&1}\right)$
versus \fixTABwidth{F}%
$\left(\tabbedCenterstack[r]{1&34&544\\4324329&0&8\\89&123&1}\right)$}

\subsection*{Setting the Stack Tabbing Character}

\begin{sloppypar}
By default, for the parsing of columns within a given row, this package employs 
  the \vb|&| character to delimit the columns.  
This value can be changed via \vb|\setstackTAB{|<tabbing character>\vb|}|,%
  \margcmd{setstackTAB} 
  where the argument is the newly desired tabbing token.  
It can be any of various tokens\footnote{%
Since \tst{} uses the \loi{} package for parsing rows and columns, 
  see the \loi{} package documentation for limitations on the tokens that
  can be used as a valid parsing separator.}
, including  a space token, if one wishes to 
  parse a space-separated list of columns.  
\end{sloppypar}

\subsection*{TABstacks Inside the \texttt{tabular} or \texttt{align} 
  Environments}

When invoking a TABstack inside another tabbed environment, such as
  \vb|tabular|, \vb|align|, or other similar environments, one 
  \textbf{no longer}\footnote{%
  As of version 2.10, the problem of properly scoping TABstack argument
  was resolved with the use of ``brace hacks'' described on p.\,385 of the 
  \TeX book, and suggested to the author by Prof. Enrico Gregorio.
}
  needs to group the TABstacks in their own braces \vb|{}|:
\begin{verbnobox}[\footnotesize\vbdelim]
\ensureTABstackMath{%
\begin{tabular}{c|c}
Left Eqn. & Right Eqn.\\
\hline
\tabularCenterstack{lr}{a_1 & 12\\c & 1234} & \
\tabularCenterstack{rl}{a_1 & 12\\c & 1234}\\
\hline
\end{tabular}
}
\end{verbnobox}
{%
\ensureTABstackMath{%
\begin{tabular}{c|c}
Left Eqn. & Right Eqn.\\
\hline
\tabularCenterstack{lr}{a_1 & 12\\c & 1234} & 
\tabularCenterstack{rl}{a_1 & 12\\c & 1234}\\
\hline
\end{tabular}
}

\subsection*{Math Relations/Operators at Cell Left/Right Extrema}

There are two things to keep in mind regarding TABstacked content.  
First, a TABstack cell has no precise understanding up what content precedes 
  it in the cell to the immediate left, nor what content follows it in the 
  cell to the immediate right.  
It does, however know the overall height/depth of the content across the whole 
  row and creates a vertical ``strut'' of that height and depth, which must,
  in some way,  be applied to every cell in the row.  

This vertical strut can be applied to the cell immediately prior to or 
  immediately following the cell content, as we shall see.  
However, such an action will have an effect on math operators and relations 
  found at the leading or trailing ends of the cell content.  

Math operators and relations can be categorized as unary or binary; some
  may be both, depending on their usage context, such as the minus sign.  
When used as $a-b$, the relation is binary, because it connects $a$ and $b$ 
  in a mathematical operation.  
Note how space appears both before and after the minus sign.  
Alternatively, when used as $-\pi$, the minus sign operates only upon what 
  follows, in this case $\pi$, to produce a negative.  
Note how no space is introduced between the minus sign and $\pi$.
This is the minus used as a unary operator.

Because a TABstack cell has no intimate knowledge of the adjacent cell content, 
  it is up to the user to employ his tabbing separators in a way that produces
  the desired result.  
By default, \tst{} will place the strut after the cell content.  
This means that any trailing math operator in a cell will present itself in its 
  binary form (regardless of what comes in the cell to the right), because the 
  strut will appear as trailing data against which the operator can 
  be set.  
Similarly, any leading math operator will present itself as unary (regardless of 
  what content appears in the cell to the left).

\stackMath
Thus, under the default setting \vb|\tabbedLongstack{y =&-mx +& b}| will 
  present as {\tabbedLongstack{y =&-mx +& b}}, by default, with the trailing
  equal and plus signs as binary, and the leading minus sign as unary.  
The package can reverse the default with the following declarative modes: 
  \vb|\TABunaryRight|\margcmd{TABunaryRight} (identical to 
  \vb|\TABbinaryLeft|)\margcmd{TABbinaryLeft}; alternately,
  one may use \vb|\TABbinary|\margcmd{TABbinary}, which will 
  present both leading \textit{and} trailing 
  operators in their binary form.  
The default can be restored with \vb|\TABunaryLeft| (identical to 
  \vb|\TABbinaryRight|).\margcmd{TABunaryLeft}\margcmd{TABbinaryRight}

Without changing any of the package strut modes, an operator, such as minus, 
  can always be forced into its unary mode by enclosing it in braces: \vb|{-}|.  
Likewise, it can be forced into its binary mode by placing empty braces on 
  both sides of it: \vb|{}-{}|.

\subsection*{The Parsing Macros \texttt{\textbackslash readTABstack}, <etc.>}

As of version 2.00 of the \tst{} package, the parsing functions of the package 
  were delegated to the very powerful \loi{} package. 
As such {\bfseries the \vb|\readTABrow| macro is no longer supported}.
For typical parsing functionality, therefore, please consult the 
  documentation to the \loi{} package and its \vb|\readlist| macro.
I commend it to your inspection and use for a variety of parsing tasks. 

However, there may still be a need to access the various stacking related data
  in either a recently composed TABstack, or even one that is yet to be typeset.
When a TABstack is constructed by the \tst{} package, a call is made to the routine
  \vb|\readTABstack|\margcmd{readTABstack}, in order to parse the data.
This macro may be independently called by the user to read TABstack data
  without producing a constructed TABstack, by passing it the same tabbed,
  EOL-separated data that would otherwise be used to construct a stack.%
  \footnote{Alternately, TABstacking data can be read without generating a
  TABstack by using the \texttt{\cmd renewcommand\cmd quietstack\{T\}} 
  setting of \ste{} to suppress the stack output, without suppressing its 
  construction.}
If the routine is not called independently by the user, data from the most 
  recent TABstacking operation is still available for interrogation.

Take the example

\vb|\TABstackMath\TABstackMathstyle{\displaystyle}\setstackgap{S}{5pt}%|\\
\vb|\alignShortstack{\frac{A}{Q}x=&B\\ C= &\frac{Dx}{2}\\E=&F}|

which presents as

\TABunaryLeft
\TABstackMath\TABstackMathstyle{\displaystyle}\setstackgap{S}{5pt}%
  \alignShortstack{\frac{A}{Q}x=&B\\ C= &\frac{Dx}{2}\\E=&F}

Let us say we were interested in information about the cell in the 1st
  column of the 2nd row.  
I can obtain its dimensions as the column-1 width \vb|\TABwd{1}|%
  \margcmd{TABwd}, as well as the row-2 height and depth 
  \vb|\TABht{2}|\margcmd{TABht} and \vb|\TABdp{2}|\margcmd{TABdp}.
Note that these macros provide dimensions of the TABstack \textit{cell}, which 
  in this case is larger than the mere ``\TABcell{2}{1}'' content.
Those dimensions are as follows, followed by a \vb|\rule| depicting the total 
  size of the cell:

Width: \TABwd{1},\hfill Height/Depth: \TABht{2}/\TABdp{2},\hfill Rule: 
  \rule[-\TABdp{2}]{\TABwd{1}}{\dimexpr\TABdp{2}+\TABht{2}\relax}

One can also obtain information about what is in the cell.
Here, use the macro \vb|\TABcellRaw[2,1]|\margcmd{TABcellRaw}, which will
  expand to the tokens employed in the stack definition (shown here in an
  \vb|\fbox| to show that the leading/trailing spaces have been discarded):

\fboxsep=1pt
\fbox{\TABcellRaw[2,1]}

If one would like to see the cell data presented in the prevailing
  (\textsf{tab})\ste{} mode and style%
\footnote{Note that both \cmd{TABcell} and, as described later,
  \cmd{TABcellBox} present in the \textit{prevailing} TABstack mode and
  style.
  While a recent use of \cmd{ensureTABstackMath} will be remembered,
  intervening declarations of \cmd{TABstackMath}, \cmd{TABstackText} and
  their associated styles will change the \textit{prevailing} mode
  and style in which subsequent \cmd{TABcell} and \cmd{TABcellBox}
  are processed.}%
  , the macro 
  \vb|\TABcell{2}{1}|\margcmd{TABcell} may be used (again shown in
  an \vb|\fbox|):
 
\fbox{\TABcell{2}{1}}

Note, however, that the \vb|\TABcell| still does not account for three things: 
\vspace{-\parskip}\begin{itemize}
  \item it is not strutted to reflect the height of the full row content; 
  \item it does not reflect the full column width (nor the alignment within
     the column); and
  \item it does not provide any of the empty group treatments that would
     otherwise make leading/trailing math operators perform in a binary fashion.
\end{itemize}\vspace{-\parskip}
A strut of the given row height may be obtained with  \vb|\TABstrut{2}|%
  \margcmd{TABstrut}:

\fboxsep=0pt
\fbox{\TABstrut{2}} $\quad\leftarrow$the strut is boxed here to show 
  its vertical extent

However, to obtain the fully rendered cell, \textit{as it appears within the
  actual TABstack}, one needs \vb|\TABcellBox{2}{1}|\margcmd{TABcellBox},
  shown (in an \vb|\fbox|) as

\fbox{\TABcellBox{2}{1}}\quad% ACCOUNTS FOR \TABunaryLeft, etc.

Since the \vb|\readTABstack| macro, itself, neither knows nor determines the
  eventual cell alignment of a future stack, the actual \texttt{lcr} 
  alignment of a \vb|\TABcellBox| will only be known when applied to
  a previously constructed stack.
Therefore, if \vb|\TABcellBox| is called following an independent 
  invocation of \vb|\readTABstack|, center alignment
  of the cell content will be provided, by default, which can be overridden
  with the optional argument to \vb|\TABcellBox|.

Note that the height/depth of the \vb|\TABcellBox| reflects the height and
  depth of the row content of the TABstack.  
For short stacks, the specified gap between rows is \textit{in addition} 
  to these strutted boxes.
For long stacks, the inter-row spacing is independent of the box height and
  depth.
However, even for long stacks, the height of the top row and the depth of 
  the bottom row of a stack still affect the overall dimensions of the stack.

\begin{sloppypar}
If one wishes to recover the \textit{actual tokens} that were employed in a 
  given TABstack cell (rather that just something that will \textit{expand}
  to those tokens), that can be accomplished in one of two ways.
The macro \vb|\TABcellRaw[,]| can be expanded twice in the manner of
\end{sloppypar}

\vb|\detokenize\expandafter\expandafter\expandafter{\TABcellRaw[2,2]}|

\detokenize\expandafter\expandafter\expandafter{\TABcellRaw[2,2]}

Alternately, the macro \vb|\getTABcelltoks[,]|\margcmd{getTABcelltoks} will
  produce a token list named  \vb|\TABcelltoks|\margcmd{the\cmd TABcelltoks} that 
  contains the cell's tokens:

\vb|\getTABcelltoks[2,2]\detokenize\expandafter{\the\TABcelltoks}|

\getTABcelltoks[2,2]\detokenize\expandafter{\the\TABcelltoks}
\TABstackText

In summary then, \tst{} cell content can be accessed in a number of ways:\\
\renewcommand\arraystretch{1.2}
\begin{tabular}{@{}l@{~--~}p{3.3in}@{}}
\vb|\TABcellRaw[,]| & expands into the tokens of the cell\\
\vb|\TABcell{}{}| & presents the cell content in the prevailing mode (text
  or math) and style set by \ste{} and \tst{}\\
\vb|\TABcellBox{}{}| & presents the cell content, in the prevailing mode
  and style, strutted to the proper row height/depth, set in a box of the
  proper cell width, flanked by the appropriate \vb|{}| groups defined by
  \tst's unary and/or binary declarations,
  and (when knowable) set in the proper \texttt{lcr} alignment\\
\vb|\getTABcelltoks[,]| & creates a token list register \vb|\TABcelltoks| 
  that contains the actual tokens employed in the cell, accessible by
   way of \vb|\the\TABcelltoks|
\end{tabular}

\section*{TABstack Array Dimensions}

Consider the example

\vb|\setstacktabbedgap{.5em}|\\
\vb|\tabbedLongstack{a & b & c & d\\ e & f & g & h\\ i & j & k & l}|

which produces

\setstacktabbedgap{.5em}
\tabbedLongstack{a & b & c & d\\ e & f & g & h & H\\ i & j & k & l}

The macros \vb|\TABwd|, \vb|\TABht|, and \vb|\TABdp| were presented as the
  means to get the physical dimensions of various rows and columns of a TABstack.
But what if the information sought is the number of rows and columns?

The macro \vb|\TABcells{}|\margcmd{TABcells} performs the function. 
When passed a blank argument, it returns the number of rows of the most
  recently constructed TABstack (or \vb|\readTABstack|).

$\textrm{Rows} = \vb|\TABcells{}| = \TABcells{}$

On the other hand, pass it a row number for its arguments and it will tell you
how many columns below to that row

$\textrm{Columns} = \vb|\TABcells{1}| = \TABcells{1}$

Note that \tst{} uses the number of columns provided in row 1 to determine the
  dimensions of the subsequent TABstack.
If the 2nd or 3rd rows of the above stack were [accidentally] defined with 5 
  columns of data, the 5th column of data would be ignored during the TABstack
  construction, since the 1st row only has 4 columns.  
However, in that case, \vb|\TABcells{2}| would still, in fact, yield 
  \TABcells{2}.

\section{Absent Features/Tricky Syntax}

\textbf{1. Nothing Equivalent to} \vb`|`

This is not a bug, but rather a notation of a missing feature.  Currently
vertical lines may \textbf{not} be added to a tabular stack 
  with the use of \vb`|` elements in the column specifier.

\textbf{2. Trailing Row Separator/Empty Items Are Not Ignored (by Default)}

The \loi{} package used to parse TABstack input, does not, by default,
  ignore empty items.
This can cause parsing errors, if not understood properly. 
Take, for example, the well formed TABstack invocation,

\vb|\tabularLongstack{rc}{11&12\\21&22\\31&32}|.

Adding a trailing \vb|\\| row separator to the input, as in:

\vb|\tabularLongstack{rc}{11&12\\21&22\\31&32\\}|,

however, breaks the parsing because 2 columns of data are expected
  following the final \vb|\\| (even though such syntax is benign
  in, for example, the \texttt{tabular} environment).
This syntax can be immediately made acceptable by
  invoking the \loi{} declaration \cmd{ignoreemptyitems}, in which case
  the final [empty] row is discarded.
However, that approach can introduce a new set of problems, because 
  it will then ignore actual blank input that was intended, as in the
  case of this example, in which table cell (2,2) is intentionally
  left blank:

\vb|\tabularLongstack{rc}{11&12\\21&\\31&32\\}|.

To make this latter case work, when empty items are ignored, an
   empty group would need be explicitly inserted:

\vb|\tabularLongstack{rc}{11&12\\21&{}\\31&32\\}|.

As of V2.1, the case of the trailing row separator can also be addressed 
  in a 2nd way, if the \texttt{TABcline} package option has been
  declared.
In that case, the switch \cmd{relaxTABsyntax} will automatically
  add the necessary column separators to \tst{} macro input, in 
  order to avoid compilation error for the case of a trailing row
  separator.
See section~\ref{s:rtx} for more details.

These problems arising from syntax can be wholly avoided, 
  without the need to resort to
  \cmd{ignoreemptyitems} or \cmd{relaxTABsyntax}, if care is used in the
  construction of the TABstack input.

\endvbdelim
\section*{Acknowledgments}

I would like to thank Christian Tellechea for his development of the 
  \loi{} package (which was directly inspired by my deficient
  \textsf{getargs} package).
The macros provided by Christian were directly implemented for version 2.00
  of the \tst{} package.

I would also thank the user ``Werner'' at \textsf{tex.stackexchange.com} for
helping me to understand some of the details of the \textsf{etoolbox} package:

\vb|http://tex.stackexchange.com/questions/140372/|\\
\vb|loop-multi-contingency-using-etoolbox|

Professor Enrico Gregorio is a constant source of knowledge and assistance
  for which I am very grateful.

\section{Code Listing}

\verbfilenobox[\footnotesize]{tabstackengine.sty}
\end{document}



