% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode
% arara: pdflatex: { synctex: true }
%% cfr-initials.tex
%% Copyright 2015 Clea F. Rees
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Clea F. Rees.
%
% This work consists of all files listed in manifest.txt.
\listfiles
\documentclass[11pt,british,a4paper]{article}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp,cfr-lm}
\usepackage{fancyhdr,pageslts}
\usepackage{longtable,verbatim}
\usepackage{lettrine}
\usepackage{booktabs,url}
\usepackage{microtype}
\usepackage[headheight=14pt,vscale=.71]{geometry}	% use 14pt for 11pt text, 15pt for 12pt text
\usepackage{parskip}
\usepackage{Acorn, AnnSton, ArtNouv, ArtNouvc, Carrickc, Eichenla, Eileen, EileenBl, Elzevier, GotIn, GoudyIn, Kinigcap, Konanur, Kramer, MorrisIn, Nouveaud, Romantik, Rothdn, Royal, Sanremo, Starburst, Typocaps, Zallman}

\title{cfr-initials}
\author{Clea F.\ Rees\footnote{reesc21 <at> cardiff <dot> ac <dot> uk}}
\def\dyddiad{2015--04--06}
\def\fyversion{Version 1.01}
\date{\fyversion\ --- \dyddiad}
\pagestyle{fancy}
\fancyhf[lh]{\itshape \fyversion}
\fancyhf[rh]{\itshape \dyddiad}
\fancyhf[ch]{\itshape cfr-initials}
\fancyhf[lf]{}
\fancyhf[rf]{}
\fancyhf[cf]{\itshape --- \thepage~of~\lastpageref*{LastPage} ---}

\makeatletter
\newcommand\lettrinetest[1]{% text is from the kantlipsum package
  \setcounter{DefaultLines}{3}%
  \renewcommand{\rmdefault}{fnc}%
  \gdef\fontlist{#1}%
  \@for \xx:=\fontlist \do {%
	% 	\lettrinetest{\xx}
	\renewcommand\LettrineFontHook{\xx}%
	\lettrine{A}{s} any dedicated reader can clearly see, the Ideal of practical reason is a representation of, as far as I know, the things in themselves; as I have shown elsewhere, the phenomena should only be
	used as a canon for our understanding.}}
\makeatother

\begin{document}
\maketitle\thispagestyle{empty}
\pdfinfo{%
  /Creator		(TeX)
  /Producer		(pdfTeX)
  /Author		(Clea F.\ Rees)
  /Title		(cfr-initials)
  /Subject		(TeX)
  /Keywords		(TeX,LaTeX,font,fonts,tex,latex,cfr-initials,initials,Clea,Rees)}
\pdfcatalog{%
  /URL			()
  /PageMode	/UseOutlines}	% other values: /UseNone, /UseOutlines, /UseThumbs, /FullScreen
%[openaction <actionspec>]
\pagenumbering{arabic}
\newcommand*{\lpack}[1]{\textsf{#1}}

\begin{abstract}
  \hspace*{-\parindent}\lpack{cfr-initials} is a set of 23 tiny packages designed to make it easier to use the decorative and ornamental initials provided by \lpack{initials} in \LaTeX.
  \lpack{initials} provides 23 such fonts in type 1 format, together with the support files required to use them.
  They cannot be used straightforwardly in \LaTeX, however, because the lack of package files providing ready-to-use commands is complicated by the non-standard naming of the font definition files.
  \lpack{cfr-initials} is designed to make good that deficit.
\end{abstract}

\section{Requirements}\label{sec:require}

The next sentence but one states the blindingly obvious.
Sine the blindingly obvious is extremely easy to miss, the previous sentence draws crucial attention to the next sentence in the hopes that readers will avoid the attendant pitfalls.
In addition to the usual suspects (\LaTeX\ etc.), the packages provided by \path{cfr-initials} require \lpack{initials}.

In addition, the documentation requires:
\begin{itemize}
  \item \lpack{babel}
  \item \lpack{microtype}
  \item \lpack{geometry}
  \item \lpack{booktabs}
  \item \lpack{fancyhdr}
  \item \lpack{pageslts}
  \item \lpack{longtable}
  \item \lpack{verbatim}
  \item \lpack{url}
  \item \lpack{lettrine}
\end{itemize}

\section{Using the fonts}\label{sec:usage}

To access the fonts, you simply load the relevant package in your preamble.
For example:
\begin{verbatim}
  \usepackage{Zallman}
\end{verbatim}

Each package provides two new commands.
The first is a font \emph{switch}.
It switches to the relevant set of initials until the end of the group, or until another command switches to a different family.
You will rarely wish to use these commands directly but they are useful in the definitions of macros.
For example, these are the commands you will need if using the fonts with the \lpack{lettrine} package.
(See section \ref{sec:lettrine} for examples.)

The second takes a single, mandatory argument and typesets that argument in the appropriate font.

\begin{longtable}{llllll}
  \toprule
  \bfseries Family	& \bfseries Package	&	\bfseries Switch	&	\bfseries Command	& \verb|ABC| & \verb|abc|	\\\midrule\endhead
  \bottomrule\endfoot
  Acorn & \lpack{Acorn} & \verb|\Acornfamily| & \verb|\acorn{}| & \acorn{ABC} & \acorn{abc} \\
  AnnSton & \lpack{AnnSton} & \verb|\AnnStonfamily| & \verb|\astone{}| & \astone{ABC} & \astone{abc} \\
  ArtNouv & \lpack{ArtNouv} & \verb|\ArtNouvfamily| & \verb|\artnouv{}| & \artnouv{ABC} & --- \\
  ArtNouvc & \lpack{ArtNouvc} & \verb|\ArtNouvcfamily| & \verb|\artnouvc{}| & \artnouvc{ABC} & \artnouvc{abc} \\
  Carrickc & \lpack{Carrickc} & \verb|\Carrickcfamily| & \verb|\carr{}| & \carr{ABC} & \carr{abc} \\
  Eichenla & \lpack{Eichenla} & \verb|\Eichenlafamily| & \verb|\eichen{}| & \eichen{ABC} & \eichen{abc} \\
  Eileen & \lpack{Eileen} & \verb|\Eileenfamily| & \verb|\eileen{}| & \eileen{ABC} & \eileen{abc} \\
  EileenBl & \lpack{EileenBl} & \verb|\EileenBlfamily| & \verb|\eileenbl{}| & \eileenbl{ABC} & \eileenbl{abc} \\
  Elzevier & \lpack{Elzevier} & \verb|\Elzevier| & \verb|\elz{}| & \elz{ABC} & \elz{abc} \\
  GotIn & \lpack{GotIn} & \verb|\GotInfamily| & \verb|\gotin{}| & \gotin{ABC} & --- \\
  GoudyIn & \lpack{GoudyIn} & \verb|\GoudyInfamily| & \verb|\goudyin{}| & \goudyin{ABC} & --- \\
  Kinigcap & \lpack{Kinigcap} & \verb|\Kinigcapfamily| & \verb|\kinig{}| & \kinig{ABC} & \kinig{abc} \\
  Konanur & \lpack{Konanur} & \verb|\Konanurfamily| & \verb|\konanur{}| & \konanur{ABC} & \konanur{abc} \\
  Kramer & \lpack{Kramer} & \verb|\Kramerfamily| & \verb|\kramer{}| & \kramer{ABC} & \kramer{abc} \\
  MorrisIn & \lpack{MorrisIn} & \verb|\MorrisInfamily| & \verb|\morrisin{}| & \morrisin{ABC} & \morrisin{abc} \\
  Nouveaud & \lpack{Nouveaud} & \verb|\Nouveaudfamily| & \verb|\nouvd{}| & \nouvd{ABC} & \nouvd{abc} \\
  Romantik & \lpack{Romantik} & \verb|\Romantik| & \verb|\romantik{}| & \romantik{ABC} & \romantik{abc} \\
  Rothdn & \lpack{Rothdn} & \verb|\Rothdnfamily| & \verb|\roth{}| & \roth{ABC} & \roth{abc} \\
  Royal & \lpack{Royal} & \verb|\Royal| & \verb|\royal{}| & \royal{ABC} & ---\\
  Sanremo & \lpack{Sanremo} & \verb|\Sanremofamily| & \verb|\sanremo{}| & \sanremo{ABC} & \sanremo{abc} \\
  Starburst & \lpack{Starburst} & \verb|\Starburstfamily| & \verb|\starburst{}| & \starburst{ABC} & \starburst{abc} \\
  Typocaps & \lpack{Typocaps} & \verb|\Typocapsfamily| & \verb|\typocap{}| & \typocap{ABC} & \typocap{abc} \\
  Zallman & \lpack{Zallman} & \verb|\Zallmanfamily| & \verb|\zall{}| & \zall{ABC} & \zall{abc} \\
\end{longtable}
\clearpage

\section{Sample lettrines}\label{sec:lettrine}

\lettrinetest{\Royal,\Romantik,\ArtNouvfamily,\EileenBlfamily,\Zallmanfamily,\Konanurfamily,\Starburstfamily,\Typocapsfamily,\ArtNouvcfamily,\Kinigcapfamily,\Kramerfamily,\GotInfamily}

\end{document}

