# hideanswer

This package can generate documents with and without answers from a single file by toggling a switch. However, it can only be used to create documents to be printed on paper.


## Documents

Documents for this package are available in English and Japanese.


## License

This package released under [the MIT license](https://ctan.org/license/mit).

(C) 2022 Yukoh KUSAKABE


## Revision History

+ Version 1.1 2022-07-09
  + Rewrite README.
  + Add the document (hideanswer.pdf).
+ Version 1.0 2022-06-28
  + The first public version.


---

[Yukoh KUSAKABE](https://twitter.com/metaphysicainfo) (screen-name) at [metaphysica.info](https://www.metaphysica.info/)