=== Description of the Class ===

The ffslides ("freeform slides") class is intended to make 
it easier to place various types of content freely on the 
page, and therefore easier to design documents with a strong 
visual component: presentations, posters, research or lecture
notes, and so on.  

The goal of the class is to be less rigid and less complex 
than some of the popular presentation-making options.  It is
essentially a small set of macros added to the article class. 
A well-organized template file is included, and the 
documentation is itself an extensive example of the class's 
capabilities.


=== Author Information ===

Mark A. Wolters
ffslides at mwolters dot com


=== License information ===

Copyright 2015 Mark A. Wolters

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or any later version.
The latest version of this license is in
   http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is M. A. Wolters.

This work consists of the following files:
  README.txt
  ffslides.cls
  ffslides-doc.pdf
  header-example.txt
  ffslides-doc.tex
  ffslides-template.tex
  tiny_example_2.pdf
  tiny_example_1.pdf
  figure.pdf
  bground-example.txt
  footer-example.txt
  