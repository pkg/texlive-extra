gu.sty - Gruppe-Untergruppe-Stammbäume setzen

(Note for English speaking users: see further down for an English version of
this summary.)

Das Paket gu.sty erleichtert das Setzen von einfachen kristallographischen
Gruppe-Untergruppe-Schemata im Bärnighausen-Formalismus und/oder
Lagenfortführungstabellen. Die einzelnen Elemente und Abstände werden
innerhalb einer neuen Umgebung namens "stammbaum" mittels vorgegebener
Kommandos definiert. Anschließend werden alle Abstände automatisch berechnet
und der Stammbaum und/oder die Tabellen ausgegeben. Momentan werden maximal
zwei Abstiegsschritte unterstützt, eine Erweiterung ist aber relativ
problemlos möglich. Für Einzelheiten und Dokumentation siehe gudoc.pdf.
Dieses Paket unterliegt der LaTeX Project Public License, siehe
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html für nähere
Details. Copyright (C) 2006, 2007 Stefan Lange


The aim of the package gu.sty is to simplify typesetting of simple
crystallographic group-subgroup-schemes in the Bärnighausen formalism. It
defines a new environment "stammbaum", wherein all elements of the scheme
are defined. Afterwards all necessary dimensions are calculated and the
scheme is drawn. Currently two steps of symmetry reduction are supported.
Further documentation is currently only available in German in the file
gudoc.pdf.

This material is subject to the LaTeX Project Public License. See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for the
details of that license.

Copyright (C) 2006, 2007 Stefan Lange