# Changelog

## [Unreleased](https://github.com/gusbrs/zref-vario/compare/v0.1.7...HEAD)

## [v0.1.7](https://github.com/gusbrs/zref-vario/compare/v0.1.6...v0.1.7) (2023-01-03)

### Changed
- Improved Italian localization (see issue
  [#1](https://github.com/gusbrs/zref-vario/issues/1)).

## [v0.1.6](https://github.com/gusbrs/zref-vario/compare/v0.1.5...v0.1.6) (2022-12-29)

### Added
- Hyperlinking for the nearby page references and for the page part of paired
  references.
- Localization for Italian.

## [v0.1.5](https://github.com/gusbrs/zref-vario/compare/v0.1.4...v0.1.5) (2022-07-08)

### Added
- `\zvsetup` for package options settings.
- `pageprop` option to set the reference property used for page comparisons.
  This option lifts `varioref`'s restriction of depending on the arabic page
  numbering to distinguish between nearby and far away references.  See the
  user manual for details.

## [v0.1.4](https://github.com/gusbrs/zref-vario/compare/v0.1.3-alpha...v0.1.4) (2022-04-24)

### Changed
- Improved loading of dependencies.

## [v0.1.3-alpha](https://github.com/gusbrs/zref-vario/compare/v0.1.2-alpha...v0.1.3-alpha) (2022-02-11)

### Changed
- (Internal) Option variables setting was reviewed, ensuring proper scope is
  in use and that they are always properly declared.  `expl3` debugging
  enabled in regression tests.

## [v0.1.2-alpha](https://github.com/gusbrs/zref-vario/compare/v0.1.1-alpha...v0.1.2-alpha) (2022-02-08)

### Added
- Integration with `zref-check` and new corresponding option `vcheck`.

## [v0.1.1-alpha](https://github.com/gusbrs/zref-vario/compare/v0.1.0-alpha...v0.1.1-alpha) (2022-02-01)

### Changed
- Improved loading setup of required packages.

## [v0.1.0-alpha](https://github.com/gusbrs/zref-vario/releases/tag/v0.1.0-alpha) (2022-01-31)

### Added
- Initial release.
