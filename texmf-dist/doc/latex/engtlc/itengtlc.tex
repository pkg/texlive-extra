\documentclass[11pt,a4paper,openany]{book}
\setlength{\headheight}{14pt}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[italian]{babel}
\usepackage{amsmath, amssymb}
\usepackage{tabularx}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{colortbl}
\usepackage{xcolor}
\usepackage{textcomp}
\usepackage{microtype}
\lstset{language=[LaTeX]TeX,%
        basicstyle=\footnotesize\ttfamily}
\usepackage{engtlc}
\usepackage[pdfpagelabels,plainpages=false,colorlinks=true,hyperindex=true]{hyperref}

\setlength{\headheight}{14pt}

% Comandi per la documentazione
\newcommand*{\meta}[1]{{\normalfont\textlangle\textit{#1}\textrangle}}
\newcommand*{\marg}[1]{\texttt{\{\meta{#1}\}}}
\newcommand*{\cs}[1]{\texttt{\char92#1}}

\begin{document}

\frontmatter
\begin{titlepage}
\title{\begin{huge}\textbf{Il pacchetto \textsf{Engtlc}}\end{huge}\\[.5cm]
       \begin{large}Versione 3.2\end{large}}
\author{\begin{LARGE}Claudio Fiandrino \end{LARGE}}
\maketitle
\end{titlepage}
\hypersetup{urlcolor=blue}
\hypersetup{linkcolor=blue}
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{%
 \markboth{\MakeUppercase{%
 \chaptername}\ \thechapter.%
 \ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}}
\fancyhead[RO,LE]{\thepage}
\fancyhead[RE]{\leftmark}
\fancyhead[LO]{\rightmark}
\fancyfoot{}
\tableofcontents

\mainmatter
\chapter{Introduzione}
Questo pacchetto è stato realizzato in quattro periodi differenti: nel primo, si sono raggruppati i comandi fondamentali come unità di misura e simboli generali; nel secondo, sono state aggiunte alcune unità di misura e simboli come impedenze ed ammettenze.

Nella terza versione, in cui Alessio Sanna ha collaborato con preziosi suggerimenti, sono state inserite le unità di misura in byte, chip e i simboli per campi elettrico e mangetico. Ultimata in data 18/12/2009, è stata pubblicata in data 13/01/2010.

A causa della mia inesperienza nella programmazione \LaTeX{}, questa edizione era costellata di piccoli e grandi errori. Ringrazio Enrico Gregorio che mi ha cortesemente inviato un elenco delle modifiche da apportare per rendere migliore il pacchetto. Inoltre, sono stati aggiunti nuovi comandi come il simbolo di segnale analitico, di integrale, delta, i simboli delle lunghezze d'onda e di potenza.

Questa quarta versione, grazie al fondamentale aiuto di Claudio Beccari, rispetta le norme ISO e introduce comandi \emph{alias} in inglese, come spiegato nella sezione~\ref{sec:simboli}. Inoltre nuovi comandi sono stati introdotti: simboli di probabilità, comandi per definire segnali nel dominio temporale, nel dominio delle frequenze, sequenze a tempo discreto e sequenze nel dominio della trasformata z.

Le finalità per cui \textsf{engtlc} è stato creato sono molto semplici: serve a tutti coloro che lavorano, studiano in ambienti riguardanti ambiti elettronici e di telecomunicazionisti; infatti engtlc è l'acronimo delle parole Engineering Telecommunications. 

In che cosa aiuta?
Serve a velocizzare la scrittura in ambiente LTEX; per esperienza personale ho avuto modo di notare quanto poco comodo possa essere il codice in casi in cui si debba ripetere molte volte alcune espressioni e magari occorra cambiare di poco rispetto a prima il codice.

Se si deve indicare l’espressione della probabilità della variabile $x$, il codice \LaTeX\ da scrivere risulta essere:
\begin{tabbing}
\hspace{7cm}\=\kill
\begin{lstlisting}
$\mathcal{P}(x)$
\end{lstlisting}  \>   $\mathcal{P}(x)$
\end{tabbing} 
Nel caso in cui qualche riga successiva si deve indicare invece la probabilità dell’evento A occorrerebbe nuovamente digitare:

\begin{tabbing}
\hspace{7cm}\=\kill
\begin{lstlisting}
$\mathcal{P}(\text{A})$ 
\end{lstlisting} \>   $\mathcal{P}(\text{A})$
\end{tabbing} 
oppure copiare il codice precedente e cambiare solo l’argomento.

Con \textsf{engtlc} è più semplice; occorre semplicemente scrivere:
\begin{flushleft}
\texttt{\$\cs{prob}\marg{argomento}\$}
\end{flushleft}
dove l'\meta{argomento} è ciò che volete inserire, come $x$ o A.

I seguenti capitoli sono così strutturati:
\begin{itemize}
\item[$\star$] le procedure di installazione del pacchetto e le sue dipendenze sono spiegate nel capitolo~\ref{chap:installazione};
\item[$\star$] i comandi specifici introdotti da \textsf{engtlc} sono illustrati nel capitolo~\ref{chap:comandi}.
\end{itemize}


\chapter{Come installare \textsf{engtlc}}
\label{chap:installazione}
Se non trovate il pacchetto già installato nella vostra distribuzione \TeX{}, è possibile effettuare il download dal mio sito internet  \url{http://claudiofiandrino.altervista.org} nella sezione ``latex projects''; in alternativa, è anche possibile scaricare il pacchetto dal sito ufficiale dei pacchetti \LaTeX{} \url{http://tug.ctan.org/tex-archive/macros/latex/contrib/engtlc}.

Dopo aver scaricato il file \emph{.zip}, estraete il contenuto in una cartella di lavoro temporanea e copiate i file nella vostra home: \texttt{~/texmf/tex/latex/engtlc/} (se non avete un albero personale \texttt{texmf} nella vostra home, createlo). Si ricorda di effettuare il refresh del database della vostra distribuzione dopo queste operazioni.

La cartella home viene indicata con: \texttt{\textasciitilde}; questo simbolo è il modo standard con cui si fa riferimento alla home nei sistemi Linux. Per quanto riguarda i sistemi Mac, la cartella \texttt{texmf} dell'albero personale dovrebbe essere in \texttt{\textasciitilde/Library}; sui sistemi Windows, a partire da Vista, il concetto di ``home'' non è ben chiaro mentre, per le versioni precedenti, dovreste trovare la cartella \texttt{texmf} in \texttt{C:\char92Documents and Settings\char92\meta{nome-utente}}.

Si ricordi che questo pacchetto richiede come dipendenze alcuni pacchetti esterni come  \textsf{textcomp}, \textsf{amsmath}, \textsf{amssymb}. Non vengono automaticamente caricati per evitare conflitti fra le possibili configurazioni che l'utente può specificare nel preambolo. Pertanto, la responsabilità di caricarli è esclusivamente dell'utilizzatore. Siccome questi pacchetti sono generalmente richiamati per altri scopi, non dovrebbero esserci problemi. In ogni caso, se non vengono caricati \textsf{amsmath} and \textsf{amssymb}, si riceverà un errore in quanto alcuni comandi richiedono l'uso di alcune funzionalità di questi pacchetti. Al contrario, se nel vostro preambolo non caricate \textsf{textcomp}, riceverete un warning: i comandi per cui è necessario possono funzionare ugualmente, ma con meno efficacia.

\chapter{I comandi introdotti da \textsf{engtlc}}\label{chap:comandi}
Esaminiamo ora quali sono le potenzialità \textsf{engtlc}.

Nella prima sezione si prenderanno in considerazione i comandi per scrivere le unità di misura mentre nella seconda i simboli generali.

\section{Unità di misura}
Credo che la possibilità di introdurre comandi per le unità di misura in maniera veloce e coerente sia veramente utile. Ovviamente anche altri pacchetti permettono di fare la stessa cosa, ma con comandi più lunghi. Ad esempio, con \textsf{siunuitx} se occorre scrivere ``kbit/s'', bisogna digitare:
\begin{tabbing}
\hspace{7cm}\= \kill
Code \> Visualization \\ 
\begin{lstlisting}
\si{\kibi\bit\per\second}
\end{lstlisting}  \> \kbits
\end{tabbing} 
Per questo ho deciso di creare comandi più corti capaci di sostituire costrutti più lunghi come quello mostrato.

Le unità di misura di \textsf{Engtlc} devono essere utilizzate in ambiente matematico, quindi in ambienti come \texttt{\$ \$}, \texttt{\cs{}[} \texttt{\cs]} o all'interno di ambienti di tipo \emph{equation}. 

Si presti attenzione: se nel testo si inserisce un'unità di misura e ci si dimentica lo spazio dopo il comando, si ha un errore dovuto al fatto che il comando \emph{assorbe} lo spazio seguente. Infatti: 
\begin{tabbing}
\hspace{4cm}\= \kill
Code \> Visualization\\
\begin{lstlisting}
8 \cm and 
\end{lstlisting} \> 8 \cm and
\end{tabbing} 
\noindent
In questo modo, invece,  il risultato è corretto:
\begin{tabbing}
\hspace{4cm}\= \kill
Code \> Visualization\\
\begin{lstlisting}
8 \cm{} and 
\end{lstlisting} \> 8 \cm{} and
\end{tabbing} 

\subsubsection{Unità di misura temporali}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
ore & \cs{ho} & \ho \\
secondi & \cs{s} & \s \\
millisecondi & \cs{ms} & $\ms$ \\
microsecondi & \cs{us} & $\us$ \\
nanosecondi & \cs{ns} & $\ns$\\
picosecondi & \cs{ps} & $\ps$\\
\bottomrule
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Unità di misura spaziali}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
micrometri & \cs{um} & $\um$ \\
millimetri & \cs{mm} & $\mm$ \\
centimetri & \cs{cm} & $\cm$ \\
decimetri & \cs{dm} & $\dm$\\
metri & \cs{m} & $\m$\\
kilometri & \cs{km} & $\km$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di corrente}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
microampere & \cs{uA} & $\uA$ \\
milliampere & \cs{mA} & $\mA$ \\
ampere & \cs{A} & $\A$ \\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di tensione}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
microvolt & \cs{uV} & $\uV$ \\
millivolt & \cs{mV} & $\mV$ \\
volt & \cs{V} & $\V$\\
megavolt & \cs{MV} & $\MV$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di resistenza}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
milliohm & \cs{mohm} & $\mohm$ \\
ohm & \cs{ohm} & $\ohm$\\
kilohm & \cs{kohm} & $\kohm$\\
megaohm & \cs{Mohm} & $\Mohm$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di conduttanza}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
picosiemens & \cs{pSi} & $\pSi$\\
nanosiemens & \cs{nSi} & $\nSi$ \\
microsiemens &\cs{uSi} & $\uSi$ \\
millisiemens & \cs{mSi} & $\mSi$ \\
siemens & \cs{Si} & $\Si$\\
kilosiemens & \cs{kSi} & $\kSi$\\
megasiemens & \cs{MSi} & $\MSi$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di capacità} 
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
femtofarad & \cs{fFa} & $\fFa$ \\
picofarad & \cs{pFa} & $\pFa$\\
nanofarad & \cs{nFa} & $\nFa$ \\
microfarad & \cs{uFa} & $\uFa$\\
millifarad & \cs{mFa} & $\mFa$ \\
farad & \cs{Fa} & $\Fa$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di induttanza}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
femtohenry & \cs{fHe} & $\fHe$ \\
picohenry & \cs{pHe} & $\pHe$\\
nanohenry & \cs{nHe} & $\nHe$ \\
microhenry & \cs{uHe} & $\uHe$\\
millihenry & \cs{mHe} & $\mHe$ \\
henry & \cs{He} & $\He$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure in dB}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
dB & \cs{dB} & $\dB$ \\
dBm & \cs{dBm} & $\dBm$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di potenza}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
microwatt & \cs{uW} & $\uW$\\
milliwatt & \cs{mW} & $\mW$ \\
watt & \cs{W} & $\W$\\
kilowatt & \cs{kW} & $\kW$\\
megawatt & \cs{MW} & $\MW$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di frequenza}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
hertz & \cs{Hz} & $\Hz$\\
kilohertz & \cs{kHz} & $\kHz$ \\
megahertz & \cs{MHz} & $\MHz$\\
gigaahertz& \cs{GHz} & $\GHz$\\
terahertz & \cs{THz} & $\THz$\\
\bottomrule
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Misure di Bit, byte e chip secondo lo standard ISO}
\begin{center}
\begin{tabular}{lll}
\toprule
Unità & Codice equivalente & Visualizzazione\\
\midrule
bit & \cs{bit} & $\bit$\\
kibibit & \cs{kbit} & $\kbit$ \\
mebibit & \cs{Mbit} & $\Mbit$\\
byte & \cs{Byte} & $\Byte$\\
kibibyte & \cs{kByte} & $\kByte$ \\
mebibyte & \cs{MByte} & $\MByte$\\
gibibyte & \cs{GByte}	&	$\GByte$	\\
tebibyte & \cs{TByte}  & $\TByte$ \\
bit per second & \cs{bits} & $\bits$\\
kibibit per second & \cs{kbits} & $\kbits$ \\
mebibit per second & \cs{Mbits} & $\Mbits$\\
byte per second & \cs{Bytes} & $\Bytes$\\
kibibyte per second & \cs{kBytes} & $\kBytes$ \\
mebibyte per second & \cs{MBytes} & $\MBytes$\\
gibibyte per second & \cs{GBytes}	& $\GBytes$\\
tebibyte per second & \cs{TBytes}	& $\TBytes$\\
chip per second & \cs{chips} & $\chips$\\
kibichip per second & \cs{kchips} & $\kchips$ \\
mebichip per second & \cs{Mchips} & $\Mchips$\\
chip su bit per second & \cs{chipsubit} & $\chipsubit$\\
\bottomrule
\end{tabular}
\end{center}
\section{Simboli}\label{sec:simboli}
In questa sezione sono riportati una serie di simboli utili in diversi ambiti.

Tutti questi comandi devono essere usati in ambiente matematico. Alcuni hanno un nome sia in inglese che in italiano e negli esempi si riportano entrambi: nella prima riga la versione inglese mentre nella seconda in italiano. Se l'esempio è caratterizzato da una sola riga, il comando non presenta differenze fra italiano ed inglese.

\subsection{Simboli generali}

\subsubsection{Fine esercizio}
Il comando di fine esercizio inserisce un quadrato nero allineato a destra.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{exerend} & $\blacksquare$\\
\cs{finees} & $\blacksquare$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Comando di ``implicazione'' con spaziatura}
Questo comando è molto simile a \cs{implies}, ma prima e dopo il simbolo viene inserita una spaziatura che l'utente può scegliere grazie all'argomento opzionale e la spaziatura inserita per default è di 0.5\,\cm{}; è possibile cambiare tale definizione assegnando al registro \cs{Implspace} un valore differente; ad esempio:
\begin{verbatim}
\setlength{\Implspace}{3mm}
\end{verbatim}
La sintassi del comando è:
\begin{flushleft}
\cs{Spimplies}\texttt{[\meta{spazio opzionale in unità di \normalfont\cs{Implspace}}]}\\
\cs{frecciadex}\texttt{[\meta{spazio opzionale in unità di \normalfont\cs{Implspace}}]}
\end{flushleft}
Nella seguente tabella, ricordando che \cs{Spimplies} e \cs{frecciadex} sono sinomini, nella prima linea l'argomento opzionale è stato utilizzato, mentre nella seconda no; in questo modo è possibile apprezzare la differenza fra i due casi, fermo restando che il registro \cs{Implspace} non è stato modificato.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\texttt{A\cs{Spimplies[0.3]} B} & $A\Spimplies[0.3]B$	\\
\texttt{A\cs{frecciadex} B} 		& $A\frecciadex B$		\\
\bottomrule
\end{tabular}
\end{center}
Si noti come inserendo nell'argomento opzionale la spaziatura default, si ottenga lo stesso risultato:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\texttt{A\cs{Spimplies} B} & $A\Spimplies B$	\\
\texttt{A\cs{frecciadex[0.5]} B} 		& $A\frecciadex[0.5] B$		\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Comando di ``implicazione'' verticale}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Downimplies} & $\Downarrow$\\
\cs{frecciadown} & $\Downarrow$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Varianza del rumore bianco}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{noisevar} & \noisevar\\[1ex]
\cs{varianzarumore} & \varianzarumore\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Trasformata di Fourier}
Comando per la trasformata di Fourier di $x$.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{fourier\{x\}} & \fourier{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Trasformata inversa di Fourier}	
Il comando per la trasformata inversa opera in maniera analoga del precedente.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{invfourier\{x\}} & \invfourier{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Parte reale}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{bfRe\{x\}} & \partereale{x}\\
\cs{partereale\{x\}} & \partereale{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Parte immaginaria}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{bfIm\{x\}} & \parteimm{x}\\
\cs{parteimm\{x\}} & \parteimm{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Quantità di informazione}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Info\{x\}} & \Info{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Segnali nei differenti domini}
I quattro comandi seguenti definiscono segnali, usando come convenzione lettere minuscole per segnali nel dominio del tempo e sequenze discrete mentre i segnali nel dominio delle frequenze e della trasformata $z$ assumono automaticamente la lettera maiuscola.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{signt\{x\}} & \signt{x}\\
\cs{signf\{g\}} & \signf{g}\\
\cs{signn\{h\}} & \signn{h}\\
\cs{signz\{k\}} & \signz{k}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Segnale analitico}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{analytic\{x\}} & \analytic{x}\\
\cs{analitic\{x\}} & \analitic{x}\\
\bottomrule
\end{tabular}
\end{center}


\subsubsection{Versori}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{unitvec\{x\}} & \versore{x}\\
\cs{versore\{x\}} & \versore{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Vettori}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{vector\{x\}}  & \vettore{x}\\
\cs{vettore\{x\}}  & \vettore{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Coseno con frequenza specifica}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{cosine\{f\_0\}} & \coseno{f_0}\\
\cs{coseno\{f\_0\}} & \coseno{f_0}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Seno con frequenza specifica}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{sine\{f\_0\}} & \seno{f_0}\\
\cs{seno\{f\_0\}} & \seno{f_0}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Energia}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{energy\{m\}} & \energia{m}\\
\cs{energia\{m\}} & \energia{m}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Modulo}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Abs\{x\}} & \modulo{x}\\
\cs{modulo\{x\}} & \modulo{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Exponential with ISO compliant natural base}
Le norme ISO richiedono che la base di un logaritmo naturale e dell'esponenziale utilizzino il tondo.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{rmexp\{x\}} & \rmexp{x}\\
\cs{ex\{x\}} & \ex{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Unità immaginaria secondo lo standard ISO}
In modo simile al comando precedente, le norme ISO impongono che l'unità immaginaria sia scritta in tondo; in ingegneria elettronica e delle telecomunicazioni si usa la lattera ``j'' per evitare confusione con il simbolo di corrente~$i$; il tondo è usato per distinguere l'unità immaginaria con il simbolo di densità di corrente~$j$.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{iu\cs{omega}} & $\iu\omega$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Modulo con esponente}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{AbsPow\{x\}\{2\}} & \moduloexp{x}{2}\\
\cs{moduloexp\{x\}\{2\}} & \moduloexp{x}{2}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Funzione valutata per un certo valore della variabile indipendente}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{for\{f(x)\}\{x\_0\}} & \for{f(x)}{x_0}\\
\bottomrule
\end{tabular}
\end{center}


\subsubsection{Un rapporto in dB}
Questo comando è un'applicazione particolare del comando \cs{for}.
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{indB\{\cs{dfrac}\{C\}\{I\}\}}  & $\indB{\dfrac{C}{I}}$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Massimo}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Max\{x\}} & \massimo{x}\\
\cs{massimo\{x\}} & \massimo{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Minimo}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Min\{x\}} & \minimo{x}\\
\cs{minimo\{x\}} & \minimo{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Velocità della luce}
\begin{center}
\begin{tabular}{cc}
\toprule
Code & Visualization\\
\midrule
\cs{clight} & $\valc$\\
\cs{valc} & $\valc$\\
\bottomrule
\end{tabular}
\end{center}
\subsubsection{Logaritmo con una base specifica}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Log\{2\}\{x\}} & \loga{2}{x}\\
\cs{loga\{2\}\{x\}} & \loga{2}{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Integrale}
Questo comando definisce un'integrale su tutto l'asse reale, da $-\infty$ a $+\infty$:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{infint\{x\cs{diff} x\}} & \intinf{x\diff x}\\[1ex]
\cs{intinf\{x\cs{diff} x\}} & \intinf{x\diff x}\\
\bottomrule
\end{tabular}
\end{center}
Si noti l'uso del differenziale con il comando \cs{diff} in accordo con le norme ISO che impongono di scrivere il simbolo in tondo con un'opportuna spaziatura a destra e a sinistra.

\subsubsection{Delta di Dirac}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{deltain\{x\}} & \deltain{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsection{Coefficienti di riflessione}
I simboli descritti sono quelli utilizzati nei corsi di campi elettromagnetici; esistono due tipi di comandi: quelli generici e quelli specifici dove è possibile specificare il punto di misurazione.

\subsubsection{Coefficienti di riflessione generici}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Vgamma} & \gammatens\\
\cs{gammatens} & \gammatens\\
\cs{Cgamma} & \gammacorr\\
\cs{gammacorr} & \gammacorr\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Coefficienti di riflessione specifici}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{Vgammain\{A\}} & \gammatensin{A}\\
\cs{gammatensin\{A\}} & \gammatensin{A}\\
\cs{Cgammain\{A\}} & \gammacorrin{A}\\
\cs{gammacorrin\{A\}} & \gammacorrin{A}\\
\bottomrule
\end{tabular}
\end{center}
Poichè il coefficiente di riflessione in tensione è più usato rispetto a quello in corrente, esiste un comando più corto per indicarlo:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{gammain\{A\}} & \gammain{A}\\
\bottomrule
\end{tabular}
\end{center}
\subsubsection{Il coefficiente di riflessione di Kurokawa}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{gammak} & \gammak\\
\bottomrule
\end{tabular}
\end{center}

\subsection{Esempi}
Ecco alcuni esempi che illustrano l'utilità di \textsf{engtlc}:
\begin{enumerate}
\item \begin{tabbing}
\hspace{7cm}\=\kill
Codice \> Visualizzazione\\
\cs{moduloexp\{\cs{gammak}\}\{2\}} \> \moduloexp{\gammak}{2}
\end{tabbing}
\item \begin{tabbing}
\hspace{9cm}\=\kill
Codice \> Visualizzazione\\
\cs{partereale\{\cs{fourier}\{\cs{moduloexp}\{x\}\{2\}\}\}} \> $\partereale{\fourier{\AbsPow{x}{2}}}$
\end{tabbing}
Il codice standard per scrivere le espressioni precedenti sarebbe:
\begin{lstlisting}
$\textbf{Re}\left\lbrace \mathcal{F}\left\lbrace
\left\vert x \right\vert^{2} \right\rbrace \right\rbrace$
\end{lstlisting}
\end{enumerate}

\subsection{Simboli di lunghezze d'onda}
Esistono tre tipi di comandi differenti:

\subsubsection{Nel vuoto}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{lbvt} & $\lbvt$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{In guida - materiale dielettrico}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{lbg} & $\lbg$\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{In guida - vuota}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{lbgvt} & $\lbgvt$\\
\bottomrule
\end{tabular}
\end{center}

\subsection{Simboli di impedenza e ammettenza}
Con \textsf{engtlc} è possibile scrivere ogni tipo di impedenze e ammettenze.

\subsubsection{Impedenze e ammettenze generiche}
Per esprimere un’impedenza o un’ammettenza calcolata nel punto A di una linea si utilizzano i comandi:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{z\{A\}} & \z{A}\\
\cs{y\{A\}} & \y{A}\\
\bottomrule
\end{tabular}
\end{center}
La caratterizzazione di impedenze e ammettenze normalizzate in un punto A, avviene con i comandi:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{znorm\{A\}} & \znorm{A}\\
\cs{ynorm\{A\}} & \ynorm{A}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Impedenze e ammettenze caratteristiche}
Per inserire questi simboli si utilizza:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{zinf} & \zinf\\
\cs{yinf} & \yinf\\
\bottomrule
\end{tabular}
\end{center}
In una guida ci sono diversi modi che hanno impedenze o ammettenze caratteristiche diverse: per caratterizzarli univocamente esiste un comando apposito; per esempio, definire una seconda impedenza o ammettenza caratteristica con label~2, si usi:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{zinf[2]}	& \zinf[2]\\
\cs{zinfn\{2\}} & \zinfn{2}\\
\cs{yinf[2]}	& \yinf[2]\\
\cs{yinfn\{2\}} & \yinfn{2}\\
\bottomrule
\end{tabular}
\end{center}
Per evitare confusione, si presti attenzione quando si inserisce una label usando i comandi \cs{zinf} o \cs{yinf}:  è necessario fare ricorso all'argomento opzionale oppure al comando per definire impedenze e ammettenze caratteristiche con label (\verb!n! finale):
\begin{tabbing}
\hspace{7cm}\=\kill
Impedenza con argomento opzionale	\> Visualizzazione	\\
\cs{zinf[2]}						\> \zinf[2]			\\
Impedenza con label 			\> Visualizzazione 	\\ 
\cs{zinfn\{2\}} 					\>  \zinfn{2}		\\
Impedenza generica				\> Visualizzazione 	\\ 
\cs{zinf\{2\}} 						\>  \zinf{2}
\end{tabbing}
L'ultimo comando non è sbagliato secondo la sintassi \LaTeX{}, ma probabilmente non è il risultato che ci si aspetta \dots

Impedenze e ammettenze nel vuoto sono caratterizzate attraverso:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{zvt} & \zvt\\
\cs{yvt} & \yvt\\
\bottomrule
\end{tabular}
\end{center}

\subsection{Esempi ulteriori}

\noindent Con \textsf{engtlc} la potenza disponibile si può scrivere in questo modo:
\begin{verbatim}
\[\potdisp=\frac{\moduloexp{V}{2}}{\partereale{4\cdot\z{G}}}\]
\end{verbatim}
con cui si ottiene:
\[ \availpow=\frac{\AbsPow{V}{2}}{4\cdot \bfRe{\z{G}}} \]
Senza i comandi di \textsf{engtlc} sarebbe necessario un codice più lungo:
\begin{verbatim}
\[P_{\mathrm{disp}}=\frac{\left| V \right|^2}%
{ 4\cdot\textbf{Re}\left\{ Z_\mathrm{G}\right\}}\]
\end{verbatim}

\subsection{Simboli di potenza}
In questa sezione si riportano i comandi per inserire simboli di potenza; possono essere usati indifferentemente in modo matematico o meno.

\subsubsection{Potenza in un punto}
Per caratterizzare la potenza in un punto specifico, ad esempio su una porta o una sezione di guida A, si usi:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{powerin\{A\}} & \powerin{A}\\
\cs{potin\{A\}} & \potin{A}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Potenza disponibile}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{availpow} & \potdisp\\
\cs{potdisp} & \potdisp\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Potenza di alimentazione di un generatore di segnali}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{potDC} & \potCC\\
\cs{potCC} & \potDC\\
\bottomrule
\end{tabular}
\end{center}
Il vecchio nome del comando \cs{potalim} è stato mantenuto per ragioni di compatibilità.

\subsubsection{Potenza irradiata}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{irrpow} & \potirr\\
\cs{potirr} & \potirr\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Potenza dissipata}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{disspow} & \potdiss\\
\cs{potdiss} & \potdiss\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Potenza incidente}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{incpow} & \potinc\\
\cs{potinc} & \potinc\\
\bottomrule
\end{tabular}
\end{center}

Si presti attenzione che, in questa versione rispetto alle precedenti, questi simboli, a meno del primo \cs{powerin\{A\}}, hanno un argomento opzionale: può venire usato per indicare esplicitamente in quale punto è stata effettuata la misura. Per descrivere, ad esempio, la potenza dissipata per un dispositivo in trasmissione, si utilizzi \cs{potdiss[tx]} che darà come risultato $\potdiss[tx]$. Ovviamente, se l'argomento è racchiuso da \{ \} invece di [ ] si otterrà un risultato differente. Infatti, \cs{potirr\{rx\}} diventa $\potirr{rx}$.

\subsection{Simboli di campi elettrico e magnetico}
Per quanto riguarda i campi elettrico e magnetico, i comandi \textsf{engtlc} sono alquanto generici in quanto ogni istituto e docente usa convenzioni diverse; per dispense informali, i vettori sono spesso rappresentati con un trattino sottolineato. Per indicare \vettore{A}, ad esempio, si utilizza $\underline{A}$. Si distinguono:
\begin{itemize}
\item[$\star$] campi elettrico e magnetico in funzione di un vettore posizione \vector{r} e del tempo;
\item[$\star$] fasori di campi elettrico e magnetico in funzione del vettore posizione~\vector{r}.
\end{itemize}
Siccome \textsf{engtlc} adotta la convenzione sopra citata, il vettore posizione \vettore{r} è indicato con $\underline{r}$.
\begin{center}
\begin{tabular}{lc@{\qquad}lr}
\toprule
\multicolumn2{c@{\qquad}}{Campi in funzione del tempo}&
                             \multicolumn2c{Fasori}		\\
%\midrule
Codice 		& Visualizzazione	& Codice 				& \makebox[3em][r]{Visualizzazione}	\\
\midrule
\cs{Efield} & \campoe		& \cs{phasorEfield}	& \campoefas	\\
\cs{campoe} & \campoe		& \cs{campoefas} 	& \campoefas	\\
\cs{Hfield} & \campoh		& \cs{phasorHfield}	& \campohfas	\\
\cs{campoh} & \campoh		& \cs{campohfas} 	& \campohfas	\\
\bottomrule
\end{tabular}
\end{center}

Si presti attenzione: i simboli di energia \cs{energy\marg{argomento}} e campo elettrico \cs{campoe} si distinguono in quanto il secondo è un vettore e non presenta pedici.

\subsection{Simboli di probabilità}

\subsubsection{Probabilità}
La probabilità di un evento A, o di una variabile $x$, si caratterizza come:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{prob\{\cs{text}\{A\}\}} & \prob{\text{A}}\\
\cs{prob\{x\}} & \prob{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Valor medio}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{expval\{x\}} & \expval{x}\\
\cs{valatt\{x\}} & \valatt{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Varianza}
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{var\{x\}} & \var{x}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Probabilità congiunta}
Grazie a questo comando, è possibile inserire una virgola che separa adeguatamente le due variabili stocastiche:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{comma} & \prob{x\comma y}\\
\bottomrule
\end{tabular}
\end{center}

\subsubsection{Probabilità condizionata}
In modo simile al comando precedente, si inserisce un filetto verticale per rappresentare il simbolo di condizione fra due variabili stocastiche:
\begin{center}
\begin{tabular}{cc}
\toprule
Codice & Visualizzazione\\
\midrule
\cs{given} & \prob{x\given y}\\
\cs{dato} & \prob{x\dato y}\\
\bottomrule
\end{tabular}
\end{center}

\section*{Ringraziamenti}
Voglio ringraziare il prof.~Enrico Gregorio, che molto gentilmente mi ha inviato una lista completa di errori e correzioni da apportare nella terza versione.

Un ringraziamento speciale è per il prof.~Claudio Beccari per i numerosi suggerimenti e l'adattamento del pacchetto in conformità con le norme ISO.

\chapter{Licenza LPPL}
\textsf{engtlc} è distribuito con Licenza LPPL:  \LaTeX\, Project Public Licence.
\begin{footnotesize}
\begin{verbatim}
%% engtlc.sty
%% Copyright 2010-2012 Claudio Fiandrino
%
%  This work may be distributed and/or modified under the
%  conditions of the LaTeX Project Public License, either version 1.3
%  of this license or (at your option) any later version.
%  The latest version of this license is in
%  http://www.latex-project.org/lppl.txt
%  and version 1.3 or later is part of all distributions of LaTeX
%  version 2005/12/01 or later.
%
%  This work has the LPPL maintenance status `maintained'.
% 
%  The Current Maintainer of this work is Fiandrino Claudio.
%
%  This work consists of the file engtlc.sty.
\end{verbatim}
\end{footnotesize}
\end{document}
