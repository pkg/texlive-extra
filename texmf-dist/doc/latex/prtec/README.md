  #prtec -- A template for PRTEC conference papers#
 
  Version 1.06 dated 2019/08/20.
  
  ####Overview####
  This class provides a LaTeX template to format extended abstracts for the 
  Pacific Rim Thermal Engineering Conference (PRTEC), which is sponsored 
  by ASTFE, JSME, and KSME.
  
  Files in this distribution are:

      README.md           --  this file
      prtec.cls           --  the class file
      prtec.bst           --  bibtex style for PRTEC conference format
      prtec-template.tex  --  a latex template/example for this class
      prtec-template.pdf  --  documentation/sample abstract
      prtec-sample.bib    --  a sample bibliography file
      *
      sample-figure.pdf   -- figure for the sample abstract

  The .tex and .cls files are commented and should be self-explanatory.

  
  

  This template has the following features:

- matches PRTEC's font specifications and layout

- matches PRTEC's citation formats
	
- uses the newtxmath and newtxtext font packages, enabling many math and text features

- supports use of pdf bookmarks and the hyperref package

- supports bold math and footnotes in section headings without errors from hyperref
	

  ####Author####
  
  John H. Lienhard V
  
  Department of Mechanical Engineering
          
  Massachusetts Institute of Technology
          
  Cambridge, MA 02139-4307 USA

 ---
 
 ####Version History####
 
 v1.06 -- correct nofoot option to fully clear header
 
 v1.05 -- edits to .bst to better support urls; minor edit to \\entry

 v1.04 -- edit documentation
 
 v1.03 -- automate use of bold math in section and subsection headings; use key value for nodefaultsups
 
 ---
 
 ####License####

 Copyright (c) 2019 John H. Lienhard

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 associated documentation files (the "Software"), to deal in the Software without restriction, 
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 subject to the following two conditions:

 The above copyright notice and this permission notice shall be included in all copies or 
 substantial portions of the Software.

 The software is provided "as is", without warranty of any kind, express or implied, including but 
 not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. 
 In no event shall the authors or copyright holders be liable for any claim, damages or other liability, 
 whether in an action of contract, tort or otherwise, arising from, out of or in connection with the 
 software or the use or other dealings in the software.
