# authoraftertitle

This very simple package provides a simple way of having the autor, title and date availble after the titlepage.

## Usage

Use `\MyAuthor`, `\MyTitle` and `\MyData` as on the title page in your document, even after the titlepage.
The original definitions of \verb+\author+, \verb+\title+ and \verb+\date+ can be accessed using \verb+\Originalauthor+, \verb+\Originaltitle+ and \verb+\Originaldate+.


## Author
Matthias Bilger <matthias@bilger.info>
