\def\libfolder#1{../lib/#1}
\newif\ifinidedoc\inidedocfalse
\newif\ifinfulldoc\inidedocfalse

\input{stex-docheader}
\infulldoctrue

\ExplSyntaxOn
\def\OmitDocInput#1{
\clist_gput_right:Nn \g_docinput_clist
  { #1 }
}
\ExplSyntaxOff

\begin{document}
	\title{
		The {\stex{3}} Package Collection
		\thanks{Version {\fileversion} (last revised {\filedate})}
 	}
	\author{Michael Kohlhase, Dennis Müller\\
		FAU Erlangen-Nürnberg\\
		\url{http://kwarc.info/}
	}
	\pagenumbering{roman}
	\maketitle
	
	\input{stex-abstract}\bigskip

	This is the full documentation of \sTeX. It
	consists of four parts:
	\begin{itemize}
		\item \autoref{part:manual} is a general manual
			for the \sTeX package and associated software.
			It is primarily directed at end-users who
			want to use \sTeX to author semantically
			enriched documents.
		\item \autoref{part:documentation} 
			documents the macros provided
			by the \sTeX package. It is primarily directed
			at package authors who want to build on
			\sTeX, but can also serve as a reference manual 
			for end-users.
		\item \autoref{part:extensions} documents additional
			packages that build on \sTeX, primarily its
			module system. These are not part of the
			\sTeX package itself, but useful additions
			enabled by \sTeX package functionality.
		\item \autoref{part:implementation} is the
			detailled documentation of the \sTeX package
			implementation.
	\end{itemize}
	
	\makeatletter
		\renewcommand\part{%
    		\clearpage
  			\thispagestyle{plain}%
  			\@tempswafalse
  			\null\vfil
  			\secdef\@part\@spart%
  		}
		\newcounter{chapter}
		\numberwithin{section}{chapter}
		\renewcommand\thechapter{\@arabic\c@chapter}
		\renewcommand\thesection{\thechapter.\@arabic\c@section}
		\newcommand*\chaptermark[1]{}
		\setcounter{secnumdepth}{2}
		\newcommand\@chapapp{\chaptername}
		%\newcommand\chaptername{Chapter}
  		\def\ps@headings{%
    		\let\@oddfoot\@empty
    		\def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    		\let\@mkboth\markboth
    		\def\chaptermark##1{%
      			\markright{\MakeUppercase{%
        			\ifnum \c@secnumdepth >\m@ne
            			\@chapapp\ \thechapter. \ %
        			\fi
        		##1}}%
        	}%
        }
		\newcommand\chapter{\clearpage
			\thispagestyle{plain}%
			\global\@topnum\z@
			\@afterindentfalse
			\secdef\@chapter\@schapter%
		}
		\def\@chapter[#1]#2{\refstepcounter{chapter}%
			\typeout{\@chapapp\space\thechapter.}%
			\addcontentsline{toc}{chapter}%
				{\protect\numberline{\thechapter}#1}%
			\chaptermark{#1}%
			\addtocontents{lof}{\protect\addvspace{10\p@}}%
			\addtocontents{lot}{\protect\addvspace{10\p@}}%
			\@makechapterhead{#2}%
			\@afterheading%
		}
		\def\@makechapterhead#1{%
			\vspace*{50\p@}%
			{\parindent \z@ \raggedright \normalfont
				\huge\bfseries \@chapapp\space \thechapter
				\par\nobreak
				\vskip 20\p@
				\interlinepenalty\@M
				\Huge \bfseries #1\par\nobreak
				\vskip 40\p@
			}%
		}
\newcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \addpenalty{-\@highpenalty}%
    \vskip 1.0em \@plus\p@
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil
      \nobreak\hb@xt@\@pnumwidth{\hss #2%
                                 \kern-\p@\kern\p@}\par
      \penalty\@highpenalty
    \endgroup
  \fi}
\renewcommand*\l@section{\@dottedtocline{1}{1.5em}{2.8em}}
\renewcommand*\l@subsection{\@dottedtocline{2}{3.8em}{3.2em}}
\renewcommand*\l@subsubsection{\@dottedtocline{3}{7.0em}{4.1em}}
\def\partname{Part}
\def\toclevel@part{-1}
\def\maketitle{\chapter{\@title}}
\let\thanks\@gobble
\let\DelayPrintIndex\PrintIndex
\let\PrintIndex\@empty
\providecommand*{\hexnum}[1]{\text{\texttt{\char`\"}#1}}
\makeatother

\ExplSyntaxOn
\int_set:Nn \l_document_structure_section_level_int {1}
\ExplSyntaxOff

\clearpage

{%
  \def\\{:}% fix "newlines" in the ToC
  \tableofcontents
}

\clearpage
\pagenumbering{arabic}

\part{Manual}\label{part:manual}

%\let\oldsubsubsection\subsubsection
%\let\subsubsection\section
%\let\subsection\section
%\let\section\chapter

\input{stex-manual}

%\let\section\subsection
%\let\subsection\subsubsection
%\let\subsubsection\oldsubsubsection

\part{Documentation}\label{part:documentation}

\ExplSyntaxOn
\int_set:Nn \l_document_structure_section_level_int {2}
\ExplSyntaxOff


%\errmessage{\expandafter\meaning\csname comment \endcsname}
\DisableImplementation


\DocInput{../source/stex/stex-basics.dtx}
\DocInput{../source/stex/stex-mathhub.dtx}
\DocInput{../source/stex/stex-references.dtx}
\DocInput{../source/stex/stex-modules.dtx}
\DocInput{../source/stex/stex-inheritance.dtx}
\DocInput{../source/stex/stex-symbols.dtx}
\DocInput{../source/stex/stex-terms.dtx}
\DocInput{../source/stex/stex-features.dtx}
\DocInput{../source/stex/stex-statements.dtx}
\DocInput{../source/stex/stex-proofs.dtx}

\OmitDocInput{../source/stex/stex-others.dtx}
\DocInput{../source/stex/stex-metatheory.dtx}

\part{Extensions}\label{part:extensions}\slabel{part:extends}

\DocInput{../source/extensions/tikzinput.dtx}
\DocInput{../source/extensions/document-structure.dtx}
\DocInput{../source/extensions/notesslides.dtx}
\DocInput{../source/extensions/problem.dtx}
\DocInput{../source/extensions/hwexam.dtx}


\part{Implementation}\label{part:implementation}

\def\maketitle{}
\let\subsubsection\section
\let\subsection\section
\let\section\chapter

\EnableImplementation
\DisableDocumentation

\DocInputAgain

\begin{sfragment}{References}\ednote{we need an un-numbered version {sfragment*}}
  \printbibliography[heading=none]
\end{sfragment}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
