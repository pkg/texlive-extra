\begin{sfragment}[id=sec:localmh]{The Local MathHub-Directory}
    \stexcode"\usemodule", \stexcode"\importmodule", 
    \stexcode"\inputref" etc. allow for 
    including content modularly without having to specify absolute
    paths, which would differ between users and machines. Instead,
    \sTeX uses \emph{archives} that determine the global
    namespaces for symbols and statements and make it possible
    for \sTeX to find content referenced via such URIs.

    All \sTeX archives need to exist in the local |MathHub|-directory.
    \sTeX knows where this folder is via one of four means:

    \begin{enumerate}
    \item If the \sTeX package is loaded with the option |mathhub=/path/to/mathhub|, then
      \sTeX will consider |/path/to/mathhub| as the local |MathHub|-directory.
    \item If the |mathhub| package option is \emph{not} set, but the macro |\mathhub|
      exists when the \sTeX-package is loaded, then this macro is assumed to point to the
      local |MathHub|-directory; i.e.
      \stexcode"\def\mathhub{/path/to/mathhub}\usepackage{stex}" will set the
      |MathHub|-directory as |path/to/mathhub|.
    \item Otherwise, \sTeX will attempt to retrieve the system variable |MATHHUB|,
      assuming it will point to the local |MathHub|-directory. Since this variant needs
      setting up only \emph{once} and is machine-specific (rather than defined in tex
      code), it is compatible with collaborating and sharing tex content, and hence
      recommended.
    \item Finally, if all else fails, \sTeX will look for a file
      |~/.stex/mathhub.path|. If this file exists, \sTeX will assume that it contains the
      path to the local |MathHub|-directory. This method is recommended on systems where
      it is difficult to set environment variables.
    \end{enumerate}
\end{sfragment}

\begin{sfragment}{The Structure of \sTeX Archives}
    An \sTeX archive |group/name| is stored in the
    directory |/path/to/mathhub/group/name|; e.g. assuming your
    local |MathHub|-directory is set as |/user/foo/MathHub|, then
    in order for the |smglom/calculus|-archive to be found by the
    \sTeX system, it needs to be in |/user/foo/MathHub/smglom/calculus|.

    Each such archive needs two subdirectories:
    \begin{itemize}
        \item |/source| -- this is where all your tex files go.
        \item |/META-INF| -- a directory containing a single file
            |MANIFEST.MF|, the content of which we will consider shortly
    \end{itemize}
    An additional |lib|-directory is optional, and is where \sTeX will
    look for files included via \stexcode"\libinput".

    Additionally a \emph{group} of archives |group/name| may have
    an additional archive |group/meta-inf|. If this |meta-inf|-archive
    has a |/lib|-subdirectory, it too will be searched by \stexcode"\libinput"
    from all tex files in any archive in the |group/*|-group.

    \paragraph{} We recommend the following additional directory structure in the
    |source|-folder of an \sTeX archive:
    \begin{itemize}
        \item |/source/mod/| -- individual \sTeX modules, containing
            symbol declarations, notations, and 
            \stexcode"\begin{sparagraph}[type=symdoc,for=...]"
            environments for ``encyclopaedic'' symbol documentations
            \iffalse\end{sparagraph}\fi
        \item |/source/def/| -- definitions
        \item |/source/ex/| -- examples
        \item |/source/thm/| -- theorems, lemmata and proofs; preferably
            proofs in separate files to allow for multiple proofs for the
            same statement
        \item |/source/snip/| -- individual text snippets such as remarks,
            explanations etc.
        \item |/source/frag/| -- individual document fragments,
            ideally only \stexcode"\inputref"ing snippets, definitions,
            examples etc. in some desirable order
        \item |/source/tikz/| -- tikz images, as individual |.tex|-files
        \item |/source/PIC/| -- image files.
    \end{itemize}

\end{sfragment}

\begin{sfragment}{MANIFEST.MF-Files}
  The |MANIFEST.MF| in the |META-INF|-directory consists of key-value-pairs, informing
  \sTeX (and associated software) of various properties of an archive. For example, the
  |MANIFEST.MF| of the |smglom/calculus|-archive looks like this:

    \begin{framed}
        \begin{verbatim}
    id: smglom/calculus
    source-base: http://mathhub.info/smglom/calculus
    narration-base: http://mathhub.info/smglom/calculus
    dependencies: smglom/arithmetics,smglom/sets,smglom/topology,
                smglom/mv,smglom/linear-algebra,smglom/algebra
    responsible: Michael.Kohlhase@FAU.de
    title: Elementary Calculus
    teaser: Terminology for the mathematical study of change. 
    description: desc.html
        \end{verbatim}
    \end{framed}

    Many of these are in fact ignored by \sTeX, but some are important:
    \begin{itemize}
        \item[|id|:] The name of the archive, including its group (e.g. |smglom/calculus|),
        \item[|source-base|] or
        \item[|ns|:] The namespace from which all symbol and module URIs
            in this repository are formed, see (\textcolor{red}{TODO}),
        \item[|narration-base:|] The namespace from which all document
            URIs in this repository are formed, see (\textcolor{red}{TODO}),
        \item[|url-base|:] The URL that is formed as a basis for \emph{external references},
            see (\textcolor{red}{TODO}),
        \item[|dependencies|:] All archives that this archive depends on. \sTeX ignores
            this field, but \mmt can pick up on them to resolve dependencies,
            e.g. for |lmh install|.  
    \end{itemize}

\end{sfragment}

\begin{sfragment}{Using Files in \sTeX Archives Directly}
    Several macros provided by \sTeX allow for directly including
    files in repositories. These are:
    \begin{function}{\mhinput}
        \stexcode"\mhinput[Some/Archive]{some/file}" directly
        inputs the file |some/file| in the |source|-folder of
        |Some/Archive|.
    \end{function}
    \begin{function}{\inputref}
      \stexcode"\inputref[Some/Archive]{some/file}" behaves like \stexcode"\mhinput", but
      wraps the input in a |\begingroup ... \endgroup|. When converting to |xhtml|, the
      file is not input at all, and instead an |html|-annotation is inserted that
      references the file, e.g. for lazy loading. 

      In the majority of practical cases \stexcode"\inputref" is likely to be preferred
      over \stexcode"\mhinput" because it leads to less duplication in the generated
      |xhtml|.
    \end{function}
    \begin{function}{\ifinput}
        Both \stexcode"\mhinput" and \stexcode"\inputref"
        set \stexcode"\ifinput" to ``true'' during input. This allows
        for selectively including e.g. bibliographies only if the
        current file is not being currently included in a larger document.
    \end{function}
    \begin{function}{\addmhbibresource}
      \stexcode"\addmhbibresource[Some/Archive]{some/file}" searches for a file like
      \stexcode"\mhinput" does, but calls |\addbibresource| to the result and looks for
      the file in the archive root directory directly, rather than the |source|
      directory. Typical invocations are
      \begin{itemize}
      \item |\addmhbibresource{lib/refs.bib}|, which specifies a bibliography in the |lib|
        folder in the local archive or
      \item |\addmhbibresource[HW/meta-inf]{lib/refs.bib}| in another.
      \end{itemize}
    \end{function}
    \begin{function}{\libinput}
        \stexcode"\libinput{some/file}" 
        searches for a file |some/file| in
        \begin{itemize}
            \item the |lib|-directory of the current archive, and
            \item the |lib|-directory of a |meta-inf|-archive in
                (any of) the archive groups containing the current archive
        \end{itemize}
        and include all found files in reverse order; 
        e.g. \stexcode"\libinput{preamble}" in a |.tex|-file in
        |smglom/calculus| will \emph{first} input |.../smglom/meta-inf/lib/preamble.tex|
        and then |../smglom/calculus/lib/preamble.tex|. 

        \stexcode|\libinput| will throw an error if \emph{no} candidate for |some/file|
        is found.
    \end{function}
    \begin{function}{\libusepackage}
      \stexcode"\libusepackage[package-options]{some/file}" searches for a file
      |some/file.sty| in the same way that \stexcode"\libinput" does, but will
      call\\
      |\usepackage[package-options]{path/to/some/file}| instead of |\input|.

      \stexcode|\libusepackage| throws an error if not \emph{exactly one} candidate for
      |some/file| is found.
    \end{function}

    \begin{remark}
        A good practice is to have individual \sTeX fragments
        follow basically this document frame:
        \begin{latexcode}[gobble=12]
            \documentclass{stex}
            \libinput{preamble}
            \begin{document}
                ... 
                \ifinputref \else \libinput{postamble} \fi
            \end{document}
        \end{latexcode}
        Then the |preamble.tex| files can take care of loading the generally required
        packages, setting presentation customizations etc. (per archive or archive group
        or both), and |postamble.tex| can e.g. print the bibliography, index etc.

        \stexcode|\libusepackage| is particularly useful in |preamble.tex| when we want to
        use custom packages that are not part of {\TeX}Live. In this case we commit the
        respective packages in one of the |lib| folders and use \stexcode|\libusepackage|
        to load them.
    \end{remark}
\end{sfragment}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../stex-manual"
%%% End:

%%% LocalWords:  mathhub symdoc,for lmh subdirs arithmetics,smglom sets,smglom mv,smglom
%%% LocalWords:  linear-algebra,smglom
