If we take features like \stexcode"\inputref" and \stexcode"\mhinput" 
(and the \stexcode"sfragment"-environment, see 
\sref[file=stex-document-structure]{sec:ds:intro}[in=../stex-manual,title={the \sTeX{}3 manual}]) seriously,
and build large documents modularly from individually compiling documents for sections, chapters
and so on, cross-referencing becomes an interesting problem.

Say, we have a document |main.tex|, which \stexcode"\inputref"s a section
|section1.tex|, which references a definition with label |some_definition| in
|section2.tex| (subsequently also inputted in |main.tex|).
Then the numbering of the definition will depend on the \emph{document context}
in which the document fragment |section2.tex| occurs - in |section2.tex| itself
(as a standalone document), it might be \emph{Definition 1}, in |main.tex| it might
be \emph{Definition 3.1}, and in |section1.tex|, the definition \emph{does not even occur}, so
it needs to be referenced by some other text.

What we would want in that instance is an equivalent of \stexcode"\autoref", that takes
the document context into account to yield something like \emph{Definition 1}, \emph{Definition 3.1}
or ``\emph{Definition 1 in the section on Foo}'' respectively.

The \stexcode"\sref" command attempts to do precisely that. Unlike plain \stexcode"\ref", \stexcode"\autoref"
etc., \stexcode"\sref" refers to not just a \emph{label}, but instead a pair consisting of a \emph{label}
and the \emph{document} in whose context we want to refer to it. Conversely, every \emph{document} 
(i.e. standalone compilable |.tex|-file) keeps track of the ``names'' (\emph{Definition 3.1} etc.) for every
label as determined in the context of the document, and stores them in a dedicated file |\jobname.sref|.
Additionally, every document has a ``\emph{reference name}'' (e.g. ``\emph{the section on Foo}'').
This allows us to refer to ``label $x$ in document $D$'' to yield ``\emph{Definition 1 in the section on Foo}''.
And of course, \sTeX can decide based on the current document to either refer to the label by its
``full name'' or directly as e.g. \emph{Definition 3.1} depending on whether the label occurs in
the current document anyway (and link to it accordingly).

\paragraph{} For that to work, we need to supply (up to) three pieces
of information:
\begin{itemize}
    \item The \emph{label} of the reference target (e.g. |some_definition|),
    \item (optionally) the \emph{file}/document containing the reference target
        (e.g. |section2|).
        This is not strictly necessary, but allows for additional disambiguation
        between possibly duplicate labels across files, and
    \item (optionally) the document context, in which we want to
        refer to the reference target (e.g. |main|).
\end{itemize}
Additionally, the document in which we want to reference a label needs
a title for external references.


\begin{function}{\sref}
    \begin{syntax} \cs{sref}|[archive=|\meta{archive1}|,file=|\meta{file}|]|%
        \Arg{label}|[archive=|\meta{archive2}|,in=|\meta{document-context}|,title=|\meta{title}|]| \end{syntax}
    This command references \meta{label} (declared in \meta{file} in \meta{archive1}).
    If the object (section, figure, etc.) with that label occurs ultimately
    in the same document, \cs{sref} will ignore the second set of optional arguments
    and simply defer to \cs{autoref} if that command exists, or \cs{ref} if the \pkg{hyperref}
    package is not included.

    If the referenced object does \emph{not} occur in the current document however,
    \cs{sref} will refer to it by the object's name as it occurs in
    the file \meta{document-context} in \meta{archive2}.

For example, the reference to the \stexcode"sfragment"-environment above
will appear as ``subsection 7.2.1 (Introduction) in the \sTeX{}3 manual''
if you are reading this in the package documentation for |stex-references|
directly, but as a linked ``subsection 7.2.1'' in the full documentation
or manual. This is achieved using 

\stexcode"\sref[file=stex-document-structure]{sec:ds:intro}[in=../stex-manual,title={the \sTeX{}3 manual}]".

\end{function}

For a further example, the following:
\begin{center}
    \sref[file=../stex-doc]{part:extends}[in=../stex-doc,title={the full \sTeX{}3 documentation}]
\end{center}
will say ``Part III'' (and link accordingly) in the full documentation,
and ``Part III (Extensions) in the full \sTeX{}3 documentation''
everywhere else. This is achieved using

\stexcode"\sref[file=../stex-doc]{part:extends}[in=../stex-doc,title={the full \sTeX{}3 documentation}]".

\begin{function}{\extref}
    \begin{syntax} \cs{sref}|[archive=|\meta{archive1}|,file=|\meta{file}|]|%
        \Arg{label}|{archive=|\meta{archive2}|,in=|\meta{document-context}|,title=|\meta{title}|}| \end{syntax}
    The \cs{extref}-command behaves exactly like \cs{sref}, but takes
    \emph{required} the document context argument and will always use it
    for generating the document text, regardless of whether the label
    occurs in the current document.
\end{function}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../stex-manual"
%%% End:
