\begin{smodule}{SymbolsInText}
    \symdef{set}{\comp{\texttt{Set}}}

    Given a symbol declaration \stexcode"\symdecl{symbolname}",
    we obtain a semantic macro \stexcode"\symbolname".
    We can use this semantic macro in math mode to use its notation(s),
    and we can use \stexcode"\symbolname!" 
    in math mode to use its operator notation(s).
    What else can we do?
    
\begin{sfragment}{\texttt{\textbackslash symref} and its variants}

    \begin{function}{\symref,\symname}
        We have already seen \stexcode"\symname" and
        \stexcode"\symref", the latter being the more general.

        \stexcode"\symref{<symbolname>}{<code>}" marks-up |<code>|
        as referencing |<symbolname>|. Since quite often, the |<code>|
        should be (a variant of) the name of the symbol anyway,
        we also have \stexcode"\symname{<symbolname>}".
    \end{function}

    Note that \stexcode"\symname" uses the \emph{name}
    of a symbol, not its macroname. More precisely, 
    \stexcode"\symname" will insert the name of the symbol
    with ``|-|'' replaced by spaces.
    If a symbol does not have
    an explicit |name=| given, the two are equal -- but
    for \stexcode"\symname" it often makes sense to make the
    two explicitly distinct. For example:
    \stexexample{%
\symdef{Nat}[
    name=natural-number,
    type=\set
]{\comp{\mathbb{N}}}

A \symname{Nat} is...
    }

    \stexcode"\symname" takes two additional optional
    arguments, |pre=| and |post=| that get prepended or appended
    respectively to the symbol name.

    \begin{function}{\Symname}
        Additionally, \stexcode"\Symname" behaves exactly
        like \stexcode"\symname", but will capitalize the first
        letter of the name:
    \end{function}
    \stexexample{%
\Symname[post=s]{Nat} are...
    }

    \begin{dangerbox}
        This is as good a place as any other to explain how
        \sTeX resolves a string |symbolname| to an actual symbol.

        If \stexcode"\symbolname" is a semantic macro, then
        \sTeX has no trouble resolving |symbolname| to the full 
        URI of the symbol that is being invoked.

        However, especially in \stexcode"\symname" (or if a symbol
        was introduced using \stexcode"\symdecl*" without
        generating a semantic macro), we might
        prefer to use the \emph{name} of a symbol directly for
        readability -- e.g. we would want to write
        \stexcode"A \symname{natural-number} is..." rather than
        \stexcode"A \symname{Nat} is...". \sTeX attempts to handle
        this case thusly:

        If |string| does \emph{not} correspond to a semantic
        macro \stexcode"\string" and does \emph{not}
        contain a |?|, then \sTeX checks
        all symbols currently in scope until it finds one,
        whose name is |string|. If |string| is of the
        form |pre?name|, \sTeX first looks through all modules
        currently in scope, whose full URI ends with |pre|,
        and then looks for a symbol with name |name| in those.
         This allows
        for disambiguating more precisely, e.g. by
        saying \stexcode"\symname{Integers?addition}"
        or \stexcode"\symname{RealNumbers?addition}" in the
        case where several |addition|s are in scope.
    \end{dangerbox}
\end{sfragment}

\symdef{addition}[op=+,prec=100,args=2]{#1 \comp+ #2}
\symdef{multiplication}[op=\cdot,prec=50,args=a]{#1}{##1 \comp\cdot ##2}

\begin{sfragment}{Marking Up Text and On-the-Fly Notations}
    We can also use semantic macros outside of text mode though,
    which allows us to annotate arbitrary text fragments.

    Let us assume again, that we have 
    \stexcode"\symdef{addition}[args=2]{#1 \comp+ #2}". Then we
    can do
    \stexexample{%
\addition{\comp{The sum of} \arg{$\svar{n}$} \comp{ and }\arg{$\svar{m}$}}
is...
    }
    ...which marks up the text fragment as representing
    an \emph{application} of the |addition|-symbol to two
    argument $\svar{n}$ and $\svar{m}$.

    \begin{mmtbox}
        As expected, the above example is translated to \omdoc/\mmt
        as an |OMA| with |<OMS name="...?addition"/>| as head and
        |<OMV name="n"/>| and |<OMV name="m"/>| as arguments.
    \end{mmtbox}

    \begin{dangerbox}
      Note the difference in treating ``arguments'' between math mode and text mode. In
      math mode the (in this case two) tokens/groups following the \stexcode|\addition|
      macro are treated as arguments to the addition function, whereas in text mode the
      group following \stexcode|\addition| is taken to be the ad-hoc presentation. We
      drill in on this now. 
    \end{dangerbox}
    
    \begin{function}{\arg}
        In text mode, every semantic macro takes exactly one
        argument, namely the text-fragment to be annotated.
        The \stexcode"\arg" command is only valid within the
        argument to a semantic macro and marks up the 
        \emph{individual arguments} for the symbol.
    \end{function}

    We can also use semantic macros in text mode to invoke
    an operator itself instead of its application, with the
    usual syntax using |!|:
    \stexexample{%
\addition!{Addition} is...
    }

    Indeed, \stexcode"\symbolname!{<code>}" is exactly equivalent to
    \stexcode"\symref{symbolname}{<code>}" (the latter is in fact implemented in terms of
    the former).

    \stexcode"\arg" also allows us to switch the order of arguments
    around and ``hide'' arguments: For example, \stexcode"\arg[3]{<code>}"
    signifies that |<code>| represents the \emph{third}
    argument to the current operator, and \stexcode"\arg*[i]{<code>}"
    signifies that |<code>| represents the $i$th argument, but it
    should not produce any output (it is exported in the |xhtml|
    however, so that \mmt and other systems can pick up on it).\ednote{MK: I do not
      understand why we have to/want to give the second arg*; I think this must be
      elaborated on.}
    \stexexample{%
\addition{\comp{adding}
    \arg[2]{$\svar{k}$} 
    \arg*{$\addition{\svar{n}}{\svar{m}}$}} yields...
    }
    Note that since the second \stexcode"\arg" has no explicit argument
    number, it automatically represents the first not-yet-given
    argument -- i.e. in this case the first one.\ednote{MK: I do not understand this at
      all. }

    \paragraph{} The same syntax can be used in math mod as well.  This allows us to
    spontaneously introduce new notations on the fly. We can activate it using the starred
    variants of semantic macros:

    \stexexample{%
Given $\addition{\svar{n}}{\svar{m}}$, then 
$\addition*{
    \arg*{\addition{\svar{n}}{\svar{m}}}
    \comp{+}
    \arg{\svar{k}}
}$  yields...
    }

\end{sfragment}
\end{smodule}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../stex-manual"
%%% End:

%  LocalWords:  prec cdot,prec 50,args th
