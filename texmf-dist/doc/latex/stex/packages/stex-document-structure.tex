\begin{sfragment}[id=sec:ds:intro]{Introduction}
The \pkg{document-structure} package supplies an infrastructure for writing {\omdoc} documents in {\LaTeX}.
This includes a simple structure sharing mechanism for \sTeX that allows to to move from
a copy-and-paste document development model to a copy-and-reference model, which
conserves space and simplifies document management. The augmented structure can be used
by MKM systems for added-value services, either directly from the \sTeX sources, or
after translation.


 The \pkg{document-structure} package supplies macros and environments that allow to label document
 fragments and to reference them later in the same document or in other documents. In
 essence, this enhances the document-as-trees model to
 documents-as-directed-acyclic-graphs (DAG) model. This structure can be used by MKM
 systems for added-value services, either directly from the \sTeX sources, or after
 translation. Currently, trans-document referencing provided by this package can only be
 used in the \sTeX collection.

 DAG models of documents allow to replace the ``Copy and Paste'' in the source document
 with a label-and-reference model where document are shared in the document source and the
 formatter does the copying during document formatting/presentation.
\end{sfragment}

\begin{sfragment}{Package Options}
The \pkg{document-structure} package accepts the following options:
\begin{center}
  \begin{tabular}{|l|p{10cm}|}\hline
    \texttt{class=\meta{name}} & load \meta{name}|.cls| instead of |article.cls|\\\hline 
    \texttt{topsect=\meta{sect}} & The top-level sectioning level; the default for
    \meta{sect} is \texttt{section}\\\hline 
  \end{tabular}
\end{center}
\end{sfragment}

\begin{sfragment}{Document Fragments}
\begin{environment}{sfragment}
  The structure of the document is given by nested |sfragment| environments. In the
  {\LaTeX} route, the |sfragment| environment is flexibly mapped to sectioning commands,
  inducing the proper sectioning level from the nesting of |sfragment|
  environments. Correspondingly, the |sfragment| environment takes an optional key/value
  argument for metadata followed by a regular argument for the (section) title of the
  sfragment. The optional metadata argument has the keys |id| for an identifier,
  |creators| and |contributors| for the Dublin Core metadata~\cite{DCMI:dmt03}. The option
  |short| allows to give a short title for the generated section. If the title contains
  semantic macros, we need to give the |loadmodules| key (it needs no value). For instance
  we would have
\begin{latexcode}
\begin{smodule}{foo}
  \symdef{bar}{B^a_r}
   ...
   \begin{sfragment}[id=sec.barderiv,loadmodules]
     {Introducing $\protect\bar$ Derivations}
\end{latexcode}

\sTeX automatically computes the sectioning level, from the nesting of |sfragment|
environments.
\end{environment}

But sometimes, we want to skip levels (e.g. to use a |\subsection*| as an introduction for
a chapter).

\begin{environment}{blindfragment}
  Therefore the \pkg{document-structure} package provides a variant |blindfragment| that
  does not produce markup, but increments the sectioning level and logically groups
  document parts that belong together, but where traditional document markup relies on
  convention rather than explicit markup. The |blindfragment| environment is useful
  e.g. for creating frontmatter at the correct level. The example below shows a typical
  setup for the outer document structure of a book with parts and chapters.
  
\begin{latexcode}
\begin{document}
\begin{blindfragment}
\begin{blindfragment}
\begin{frontmatter}
\maketitle\newpage
\begin{sfragment}{Preface}
... <<preface>> ...
\end{sfragment}
\clearpage\setcounter{tocdepth}{4}\tableofcontents\clearpage
\end{frontmatter}
\end{blindfragment}
... <<introductory remarks>> ...
\end{blindfragment}
\begin{sfragment}{Introduction}
... <<intro>> ...
\end{sfragment}
... <<more chapters>> ... 
\bibliographystyle{alpha}\bibliography{kwarc}
\end{document}
\end{latexcode}

Here we use two levels of |blindfragment|:
\begin{itemize}
\item The outer one groups the introductory parts of the book (which we assume to have a
  sectioning hierarchy topping at the part level). This |blindfragment| makes sure that
  the introductory remarks become a ``chapter'' instead of a ``part''.
\item The inner one groups the frontmatter\footnote{We shied away from redefining the
    |frontmatter| to induce a blindfragment, but this may be the ``right'' way to go in
    the future.} and makes the preface of the book a section-level construct. The
  |frontmatter| environment also suppresses numbering as is traditional for prefaces.
\end{itemize}
\end{environment}

\begin{function}{\skipfragment}
  The |\skipfragment| ``skips an |sfragment|'', i.e. it just steps the respective sectioning
  counter. This macro is useful, when we want to keep two documents in sync structurally,
  so that section numbers match up: Any section that is left out in one becomes a
  |\skipfragment|.
\end{function}

\begin{function}{\currentsectionlevel,\CurrentSectionLevel}
  The |\currentsectionlevel| macro supplies the name of the current sectioning level,
  e.g. ``chapter'', or ``subsection''. |\CurrentSectionLevel| is the capitalized
  variant. They are useful to write something like ``In this |\currentsectionlevel|, we
  will\ldots'' in an |sfragment| environment, where we do not know which sectioning level we
  will end up.
\end{function}
\end{sfragment}  

\begin{sfragment}{Ending Documents Prematurely}
\begin{function}{\prematurestop,\afterprematurestop}
  For prematurely stopping the formatting of a document, \sTeX provides the
  |\prematurestop| macro. It can be used everywhere in a document and ignores all input
  after that -- backing out of the |sfragment| environments as needed. After that -- and
  before the implicit |\end{document}| it calls the internal |\afterprematurestop|, which
  can be customized to do additional cleanup or e.g. print the bibliography.

  |\prematurestop| is useful when one has a driver file, e.g. for a course taught multiple
  years and wants to generate course notes up to the current point in the lecture. Instead
  of commenting out the remaining parts, one can just move the |\prematurestop| macro.
  This is especially useful, if we need the rest of the file for processing, e.g. to
  generate a theory graph of the whole course with the already-covered parts marked up as
  an overview over the progress; see |import_graph.py| from the |lmhtools|
  utilities~\cite{lmhtools:github:on}.
\end{function}

Text fragments and modules can be made more re-usable by the use of global variables. For
instance, the admin section of a course can be made course-independent (and therefore
re-usable) by using variables (actually token registers) |courseAcronym| and |courseTitle|
instead of the text itself. The variables can then be set in the \sTeX preamble of the
course notes file.
\end{sfragment}

\begin{sfragment}{Global Document Variables}
  To make document fragments more reusable, we sometimes want to make the content depend
  on the context. We use \defemph{document variables} for that.

\begin{function}{\setSGvar,\useSGvar}
  |\setSGvar{|\meta{vname}|}{|\meta{text}|}| to set the global variable \meta{vname} to
  \meta{text} and |\useSGvar{|\meta{vname}|}| to reference it.
\end{function}
  
\begin{function}{\ifSGvar}
  With|\ifSGvar| we can test for the contents of a global variable: the macro call
  |\ifSGvar{|\meta{vname}|}{|\meta{val}|}{|\meta{ctext}|}| tests the content of the global
  variable \meta{vname}, only if (after expansion) it is equal to \meta{val}, the
  conditional text \meta{ctext} is formatted.
\end{function}
\end{sfragment}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../stex-manual"
%%% End:

%  LocalWords:  article.cls topsect DCMI:dmt03 loadmodules lmhtools
%  LocalWords:  prematurestop afterprematurestop import_graph.py STRlabel STRcopy vname
%  LocalWords:  STRsemantics setSGvar ifSGvar ctext
