  \begin{sfragment}{A First \sTeX Document}
    Having set everything up, we can write a first
    \sTeX document. As an example, we will use the
    |smglom/calculus| and |smglom/arithmetics| archives, 
    which should be present in the designated |MathHub|-folder,
    and write a small fragment defining the \emph{geometric series}:

   % \textcolor{red}{TODO: use some sTeX-archive instead of smglom,
   % use a convergence-notion that includes the limit,
   % mark-up the theorem properly}

    \begin{framed}\begin{latexcode}[gobble=8]
        \documentclass{article}
        \usepackage{stex,xcolor,stexthm}

        \begin{document}
        \begin{smodule}{GeometricSeries}
            \importmodule[smglom/calculus]{series}
            \importmodule[smglom/arithmetics]{realarith}

            \symdef{geometricSeries}[name=geometric-series]{\comp{S}}

            \begin{sdefinition}[for=geometricSeries]
                The \definame{geometricSeries} is the \symname{series}
                \[\defeq{\geometricSeries}{\definiens{
                    \infinitesum{\svar{n}}{1}{
                        \realdivide[frac]{1}{
                            \realpower{2}{\svar{n}}
                    }}
                }}.\]
            \end{sdefinition}

            \begin{sassertion}[name=geometricSeriesConverges,type=theorem]
            The \symname{geometricSeries} \symname{converges} towards $1$.
            \end{sassertion}
        \end{smodule}
        \end{document}
    \end{latexcode}\end{framed}

    Compiling this document with |pdflatex| should yield
    the output

    \begin{mdframed}
        \noindent\textbf{Definition 0.1. }\ The 
        \pdftooltip{\textcolor{blue}{\textbf{geometric series}}}{URI: file://your/file/name/here?GeometricSeries?geometric-series}
        is the 
        \pdftooltip{\textcolor{blue}{series}}{URI: http://mathhub.info/smglom/calculus?series?series}
        \[
        \pdftooltip{\textcolor{blue}S}{URI: file://your/file/name/here?GeometricSeries?geometric-series}
        \pdftooltip{\textcolor{blue}{:=}}{URI: http://mathhub.info/smglom/mv?defeq?definitional-equation}
        \mathop{\pdftooltip{\textcolor{blue}{\sum}}{URI: http://mathhub.info/smglom/calculus?series?infinitesum}
        }_{
            \pdftooltip{\textcolor{gray}{n}}{Variable var://n}=1
        }^{
          \pdftooltip{\textcolor{blue}\infty}{URI: http://mathhub.info/smglom/calculus?series?infinitesum}
        } \frac{1}{2^{\pdftooltip{\textcolor{gray}{n}}{Variable var://n}}}
        .\]
        \noindent\textbf{Theorem 0.2. }\ The 
        \pdftooltip{\textcolor{blue}{geometric series}}{URI: file://your/file/name/here?GeometricSeries?geometric-series}
        \pdftooltip{\textcolor{blue}{converges}}{URI: http://mathhub.info/smglom/calculus?sequenceConvergence?converges} towards $1$.
    \end{mdframed}

    Move your cursor over the various highlighted parts of the document -- depending on
    your pdf viewer, this should yield some interesting (but possibly for now cryptic)
    information.

    \begin{sparagraph}[type=remark]
      Note that all of the highlighting, tooltips, coloring and the environment headers
      come from \pkg{stexthm} -- by default, the amount of additional packages loaded
      is kept to a minimum and all the presentations can be customized,
      see \sref{sec.customhighlight}.
    \end{sparagraph}

    Let's investigate this document in detail to understand the respective parts of the
    \sTeX markup infrastructure:\bigskip

    \begin{environment}{smodule}
      \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=8]
          \begin{smodule}{GeometricSeries}
          ...
          \end{smodule}
      \end{latexcode}
      First, we open a new \emph{module} called |GeometricSeries|.  The main purpose of
      the |smodule| environment is to group the contents and associate it with a
      \emph{globally unique} identifier (URI), which is computed from the name
      |GeometricSeries| and the document context.

      (Depending on your pdf viewer), the URI should pop up in a tooltip if you hover over
      the word \pdftooltip{\textcolor{blue}{\textbf{geometric series}}}{URI:
        file://your/file/name/here?GeometricSeries?geometric-series}.
    \end{environment}\bigskip

    \begin{function}{\importmodule}
      \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=8]
          \importmodule[smglom/calculus]{series}
          \importmodule[smglom/arithmetics]{realarith}
      \end{latexcode}
      Next, we \emph{import} two modules -- |series| from the \sTeX archive
      |smglom/calculus|, and |realarith| from the \sTeX archive |smglom/arithmetics|. If
      we investigate these archives, we find the files |series.en.tex| and
      |realarith.en.tex| (respectively) in their respective |source|-folders, which
      contain the statements \stexcode"\begin{smodule}{series}" and
        \stexcode"\begin{smodule}{realarith}" (respectively).
          \iffalse\end{smodule}\end{smodule}\fi

      The \stexcode"\importmodule"-statements make all \stex symbols and associated
      semantic macros (e.g. \stexcode"\infinitesum", \stexcode"\realdivide",
      \stexcode"\realpower") in the imported module available to the current module
      |GeometricSeries|. The module |GeometricSeries| ``exports'' all of these symbols to
      all modules imports it via an \stexcode"\importmodule{GeometricSeries}"
      instruction. Additionally it exports the local symbol \stexcode"\geometricSeries".
    \end{function}

    \begin{function}{\usemodule}
      If we only want to \emph{use} the content of some module |Foo|,
      e.g. in remarks or examples, but none
      of the symbols in our current module actually \emph{depend} on
      the content of |Foo|, we can use \stexcode"\usemodule" instead -- like
      \stexcode"\importmodule", this will make the module content available,
      but will \emph{not} export it to other modules.
    \end{function}\bigskip

    \begin{function}{\symdef}
      \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=6]
        \symdef{GeometricSeries}[name=geometric-series]{\comp{S}}
      \end{latexcode}
      Next, we introduce a new \emph{symbol} with name
      |geometric-series| and assign it the semantic macro
      \stexcode"\geometricSeries".
      \stexcode"\symdef" also immediately assigns this symbol a \emph{notation},
      namely $S$.
    \end{function}

    \begin{function}{\comp}
      The macro \stexcode"\comp" marks the $S$ in the notation as a
      \emph{notational component}, as opposed to e.g. arguments
      to \stexcode"\geometricSeries".
      It is the notational components that get highlighted
      and associated with the corresponding symbol (i.e. in this
      case |geometricSeries|). Since \stexcode"\geometricSeries" takes
      no arguments, we can wrap the whole notation in a \stexcode"\comp".
    \end{function}\bigskip

    \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=8]
        \begin{sdefinition}[for=geometricSeries]
        ...
        \end{sdefinition}
        \begin{sassertion}[name=geometricSeriesConverges,type=theorem]
        ...
        \end{sassertion}
    \end{latexcode}
    What follows are two \sTeX-\emph{statements} (e.g. definitions,
    theorems, examples, proofs, ...). These are semantically marked-up
    variants of the usual environments, which take additional optional
    arguments (e.g. |for=|, |type=|, |name=|). Since many \LaTeX\xspace templates
    predefine environments like |definition| or |theorem| with
    different syntax, we use \stexcode"sdefinition", 
    \stexcode"sassertion", \stexcode"sexample"
    etc. instead. You can customize these environments to e.g.
    simply wrap around some predefined |theorem|-environment.
    That way, we can still use \stexcode"sassertion" to provide semantic
    information, while being fully compatible with (and using
    the document presentation of) predefined environments.

    In our case, the \pkg{stexthm}-package patches
    e.g. \stexcode"\begin{sassertion}[type=theorem]" to use
    a |theorem|-environment defined (as usual) using the \pkg{amsthm} package.
    \bigskip \iffalse \end{sassertion}\fi

    \begin{function}{\symname}
      \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=6]
        ... is the \symname{?series}
      \end{latexcode}
      The \stexcode"\symname"-command prints the name of a symbol,
      highlights it (based on customizable settings)
      and associates the text printed with the corresponding
      symbol.
      
      Note that the argument of \stexcode"\symref" can be 
      an imported symbol
      (here the |series| symbol is imported from the |series| module). \sTeX tries to
      determine the full symbol URI from the argument. If there are name clashes in or
      with the imported symbols, the name of the exporting module can be prepended to the
      symbol name before the |?| character.

      If you hover over the word
      \pdftooltip{\textcolor{blue}{series}}{URI: http://mathhub.info/smglom/calculus?series?series}
      in the pdf output, you should see a tooltip showing the full URI
      of the symbol used.
    \end{function}
    \begin{function}{\symref}
      The \stexcode"\symname"-command is a special case of the more general
      \stexcode"\symref"-command, which allows customizing the precise text associated
      with a symbol. \stexcode"\symref" takes two arguments: the first ist the symbol
      name (or macro name), and the second a variant verbalization of the symbol, e.g. an inflection
      variant, a different language or a synonym. In our example
      \stexcode"\symname{?series}" abbreviates \stexcode|\symref{?series}{series}|.
      
    \end{function}
    \begin{function}{\definame,\definiendum}
      \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=6]
        The \definame{geometricSeries} ...
      \end{latexcode}
      The \stexcode"sdefinition"-environment provides two additional
      macros, \stexcode"\definame" and \stexcode"\definiendum" which behave
      similarly to \stexcode"\symname" and \stexcode"\symref", but explicitly mark
      the symbols as \emph{being defined} in this environment,
      to allow for special highlighting.
    \end{function}\bigskip

    \begin{latexcode}[numbers=none,aboveskip=0pt,belowskip=0pt,gobble=8]
        \[\defeq{\geometricSeries}{\definiens{
            \infinitesum{\svar{n}}{1}{
                \realdivide[frac]{1}{
                    \realpower{2}{\svar{n}}
            }}
        }}.\]
    \end{latexcode}
    The next snippet -- set in a math environment -- uses
    several semantic macros imported from (or recursively via) 
    |series| and |realarithmetics|, such as \stexcode"\defeq", 
    \stexcode"\infinitesum",
    etc. In math mode, using a semantic macro inserts its (default)
    definition. A semantic macro can have several notations -- in
    that case, we can explicitly choose a specific notation by
    providing its identifier as an optional argument; e.g.
    \stexcode"\realdivide[frac]{a}{b}" will use the explicit notation named |frac|
    of the semantic macro \stexcode"\realdivide", which yields $\frac ab$
    instead of $a/b$.
    \begin{function}{\svar}
      The \stexcode"\svar{n}" command marks up the |n| as a variable
      with name |n| and notation |n|.
    \end{function}
    \begin{function}{\definiens}
      The \stexcode"sdefinition"-environment additionally provides the
      \stexcode"\definiens"-command, which allows for explicitly
      marking up its argument as the \emph{definiens} of the
      symbol currently being defined.
    \end{function}

    \begin{sfragment}{\omdoc/xhtml Conversion}
      So, if we run |pdflatex| on our document, then \sTeX yields pretty colors and
      tooltips\footnote{...and hyperlinks for symbols, and indices, and allows reusing
        document fragments modularly, and...}.  But \sTeX becomes a lot more powerful if
      we additionally convert our document to |xhtml| while preserving all the \sTeX
      markup in the result.

      \textcolor{red}{TODO VSCode Plugin}

      Using \rustex \cite{RusTeX:on}, we can convert the document to |xhtml|
      using the command |rustex -i /path/to/file.tex -o /path/to/outfile.xhtml|.
      Investigating the resulting file, we notice additional semantic
      information resulting from our usage of semantic macros,
      \stexcode"\symref" etc. Below is the (abbreviated) snippet inside
      our \stexcode"\definiens" block:

\begin{lstlisting}[escapechar=!,
morekeywords={property,resource,stex:comp,stex:arg,stex:OMA,stex:OMV}]
<mrow resource="" property="stex:definiens">
 <mrow resource="...?series?infinitesum" property="stex:OMBIND">
  <munderover displaystyle="true">
   <mo resource="...?series?infinitesum" property="stex:comp">!$\Sigma$!</mo>
   <mrow>
    <mrow resource="1" property="stex:arg">
     <mi resource="var://n" property="stex:OMV">n</mi>
    </mrow>
    <mo resource="...?series?infinitesum" property="stex:comp">=</mo>
    <mi resource="2" property="stex:arg">1</mi>
   </mrow>
   <mi resource="...?series?infinitesum" property="stex:comp">!$\infty$!</mi>
  </munderover>
  <mrow resource="3" property="stex:arg">
   <mfrac resource="...?realarith?division#frac#" property="stex:OMA">
    <mi resource="1" property="stex:arg">1</mi>
    <mrow resource="2" property="stex:arg">
     <msup resource="...realarith?exponentiation" property="stex:OMA">
      <mi resource="1" property="stex:arg">2</mi>
      <mrow resource="2" property="stex:arg">
       <mi resource="var://n" property="stex:OMV">n</mi>
      </mrow>
     </msup>
    </mrow>
   </mfrac>
  </mrow>
 </mrow>
</mrow>
      \end{lstlisting}
      ...containing all the semantic information. The \mmt system
      can extract from this the following \openmath snippet:

      \begin{lstlisting}[escapechar=!]
<OMBIND>
  <OMID name="...?series?infinitesum"/>
  <OMV name="n"/>
  <OMLIT name="1"/>
  <OMA>
    <OMS name="...?realarith?division"/>
    <OMLIT name="1"/>
    <OMA>
      <OMS name="...realarith?exponentiation"/>
      <OMLIT name="2"/>
      <OMV name="n"/>
    </OMA>
  </OMA>
</OMBIND>
      \end{lstlisting}
      ...giving us the full semantics of the snippet, allowing for
      a plurality of knowledge management services -- in particular
      when serving the |xhtml|.

      \begin{remark}
          Note that the |html| when opened in a browser will
          look slightly different than the |pdf| when it comes
          to highlighting semantic content -- that is because
          naturally |html| allows for much more powerful
          features than |pdf| does. Consequently, the |html|
          is intended to be served by a system like \mmt,
          which can pick up on the semantic information and
          offer much more powerful highlighting, linking
          and similar features, and being customizable by
          \emph{readers} rather than being prescribed by an author.

          Additionally, not all browsers (most notably Chrome)
          support \mathml natively, and might require
          additional external JavaScript libraries such as
          MathJax to render mathematical formulas properly.
      \end{remark}
    \end{sfragment}


    \begin{sfragment}{\mmt/\omdoc Conversion}
      Another way to convert our document to \emph{actual}
      \mmt/\omdoc is to put it in an \sTeX \textbf{archive} 
      (see \sref{sec.stexarchives}) and have \mmt take care of 
      everything.

      Assuming the above file is |source/demo.tex| in
      an \sTeX archive |MyTest|, you can run \mmt and
      do
      |build MyTest stex-omdoc demo.tex| to convert the
      document to both |xhtml| (which you will find in
      |xhtml/demo.xhtml| in the archive) and formal
      \mmt/\omdoc, which you can subsequently view in
      the \mmt browser (see \url{https://uniformal.github.io//doc/applications/server.html#the-mmt-web-site}
      for details).
    \end{sfragment}
\end{sfragment}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "stex-manual"
%%% End:

%  LocalWords:  coloring sec.customhighlight realarith infinitesum realarithmetics
