% Manual for the syllogism package
% (c) Nicolas Vaughan 2008 (nivaca@gmail.com)
% 25/10/2008

\documentclass[letterpaper,
              10pt,
              ]{article}

\usepackage[T1]{fontenc}
\usepackage[english]{babel}

\usepackage[
   colorlinks%
   ,plainpages=false% This forces a unique identification of pages.
   ,hypertexnames=true% This is necessary to have exact link on Index page.
   ,naturalnames
   ,hyperindex
]{hyperref}

%--------------------------------------------------------------------------------
\usepackage[sc,osf]{mathpazo}          % Palatino font
\usepackage[tracking=true,%
  final,%
  babel=true,%
  verbose=true,
  tracking=smallcaps]{microtype}
%----------------------------------------------------------------------------------

\usepackage{makeidx}
\usepackage{url}
\usepackage{xspace}
\usepackage{xcolor}
\usepackage{syllogism}

% . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

\newcommand{\syllversion}{release 1.2}
\newcommand{\syllp}{\texttt{\color{sred}syllogism}\xspace}
\colorlet{sblue}{blue!60!black}
\colorlet{sred}{red!60!black}
\colorlet{comcol}{green!45!black}
\newcommand{\comm}[1]{\texttt{\color{comcol}\textbackslash{}#1}\xspace} % commands
\newcommand{\commnb}[1]{\texttt{\color{comcol}#1}\xspace} % command without backslash
\newcommand\maincind[1]{\index{main commands!#1@\texttt{$\protect\backslash$#1}|}}
  % main commands index
\newcommand\addcind[1]{\index{additional commands!#1@\texttt{$\protect\backslash$#1}|}}
  % additional commands index
\newcommand\optind[1]{\index{package options!#1@\texttt{$\protect\backslash$#1}|}}
  % package options index
% . . . . .. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

\title{\vspace*{-1cm}%
  \textsc{The} \syllp \textsc{package} \\%
  {\large\syllversion}%
  }
\author{(c) 2007--2008 \textsc{Nicolas Vaughan}\\ \\
  Report bugs and submit questions to:\\
  \texttt{nivaca@gmail.com}
  }
\date{Oct. 29, 2008}

\makeindex
\begin{document}% =============================================================
\setlength{\parindent}{0em}
\maketitle
\tableofcontents

\section{Introduction}
The \syllp package provides a simple way for neatly typesetting syllogisms and syllogistic-like arguments, composed of two premisses and a conclusion. It is fully configurable, allowing the user to tweak the various distances, line widths, and other options.

\section{Changelog}
\begin{description}
  \item[v.\,1.2 (25/10/2008)] Changed the typesetting engine of \comm{syllog}.
  \item[] Fixed a problem with \comm{syllogTAC}. (Thanks to Carlos Cortissoz for noticing this.)
  \item[v.\,1.1 (02/06/2008)] Added a syllogism counter (\comm{c:syl}) and two new commands, \comm{syllogC} and \comm{syllogTAC}.
\end{description}

\section{How to use it~\label{S:howto}}
You must set
\comm{usepackage[\emph{options}]\{syllogism\}} before the
\comm{begin\{document\}} command. The available options
are described in section~\S \ref{S:options}.

To install \syllp, just place it anywhere \LaTeX can find it (e.g., \url{/texmf-local/tex/latex/syllogism}).

\subsection{\comm{syllog}~\label{SS:syllog}~\maincind{syllog}~\index{main commands}}
The main command provided by the package is \comm{syllog}, whose syntax is:

\bigskip

  \comm{syllog[\emph{pre-text}]\{\emph{first premiss}\}\{\emph{second premiss}\}\{\emph{conclusion}\},
    }

\bigskip

in which the optional parameter \emph{pre-text} is the text typeset to the left of the syllogism; \emph{first premiss}, \emph{second premiss} and \emph{conclusion} correspond respectively to the the text of the first and second premisses and the conclusion.

For example, the following command

\begin{quote}
  \comm{syllog\{Every man except Socrates is musician\}}\%\\
    \quad\commnb{\{Socrates is a man\}}\%\\
    \quad\commnb{\{Socrates is not a musician\}},
\end{quote}

will produce this output:

\syllog{Every man except Socrates is musician}{Socrates is a man}{Socrates is not a musician}

If you choose to use the optional parameter, as in:

\begin{quote}
  \comm{syllog[(S1)]\{Every man except Socrates is musician\}}\%\\
    \quad\commnb{\{Socrates is a man\}}\%\\
    \quad\commnb{\{Socrates is not a musician\}},
\end{quote}

you should get the following result:

\syllog[(S1)]{Every man except Socrates is musician}{Socrates is a man}{Socrates is not a musician}

\subsection{\comm{syllogC}~\label{SS:syllogC}~\maincind{syllogC}}
The package also provides the command \comm{syllogC} which includes an automatic numeration~\index{automatic numeration} feature. The syntax for this command is similar to the previous one, but does not take the optional parameter.%
  \footnote{The~\label{N:slabel}~\index{automatic numeration!redefining the label} label for \comm{syllogC} is defined internally through the following instruction: \texttt{\textbackslash{}newcommand\{\textbackslash{}syl\}\{(S\textbackslash{}arabic\{c:syl\})\textbackslash{}xspace\}}. It may be thus redefined as usual in \LaTeX. For example, to remove the `S' in the label, use the following: \texttt{\textbackslash{}renewcommand\{\textbackslash{}syl\}\{(\textbackslash{}arabic\{c:syl\})\textbackslash{}xspace\}}.
  } %

\bigskip

  \comm{syllogC\{\emph{first premiss}\}\{\emph{second premiss}\}\{\emph{conclusion}\},
    }

\bigskip

For example, the following code

\begin{quote}
  \comm{syllogC\{Every man except Socrates is musician\}}\%\\
    \quad\commnb{\{Socrates is a man\}}\%\\
    \quad\commnb{\{Socrates is not a musician\}},
\end{quote}

will produce this output (provided that it has not been called elsewhere before):

\syllogC{Every man except Socrates is musician}{Socrates is a man}{Socrates is not a musician}

The counter associated with \comm{syllogC} is \comm{c:syl}, which may be reset or stepped with in the usual ways.

\subsection{\comm{syllogTA}~\label{SS:syllogTA}~\maincind{syllogTA}}
The command \comm{syllogTA} works almost exactly as \comm{syllog}, the only difference being that it adds some text to the second premiss and the conclusion. Its syntax is:

\bigskip

  \comm{syllogTA[\emph{pre-text}]\{\emph{first premiss}\}\{\emph{second premiss}\}\{\emph{conclusion}\},
    }

\bigskip

As for today, \syllp supports three languages for this task: English, Spanish and Latin. The default language is English. (The language may be changed through a package option, see \S \ref{S:options} below). The added text for each language is shown in Table \ref{T:languages}, below.

\begin{table}[h]
  \begin{center}
    \begin{tabular}{lcc}
       & \textsc{second premiss} & \textsc{conclusion} \\
      \textsc{english} & \emph{but} & \emph{Therefore} \\
      \textsc{spanish} & \emph{pero} & \emph{Por lo tanto,} \\
      \textsc{latin1} & \emph{sed} & \emph{Ergo} \\
      \textsc{latin1} & \emph{sed} & \emph{Igitur} \\
    \end{tabular}
    \caption{Language variants}\label{T:languages}%
  \end{center}
\end{table}

For example, the following code:

\begin{quote}
  \comm{syllogTA\{Every man except Socrates is musician\}}\%\\
    \quad\comm{\{Socrates is a man\}}\%\\
    \quad\comm{\{Socrates is not a musician\}},
\end{quote}

will be typeset thus:

\syllogTA{Every man except Socrates is musician}{Socrates is a man}{Socrates is not a musician}

Please note that the text may be defined through several commands provided also by \syllp (see below \S \ref{SS:Tcomm}).

\subsection{\comm{syllogTAC}~\label{SS:syllogTAC}~\maincind{syllogTAC}}
The command \comm{syllogTAC}, which adds an automatic counter to \comm{syllogTA}~\maincind{syllogTA}.%
  \footnote{See note \ref{N:slabel}, above.
  } %
Its syntax is:

\bigskip

  \comm{syllogTAC\{\emph{first premiss}\}\{\emph{second premiss}\}\{\emph{conclusion}\},
    }


\section{Package options~\label{S:options}}
The package options~\index{package options} for \syllp are four, corresponding the language of the added text (used only with the commands \comm{syllogTA} and \comm{syllogTAC}):

\begin{description}
  \item{\texttt{english}}~\optind{english}
  \item{\texttt{spanish}}~\optind{spanish}
  \item{\texttt{latin1}}~\optind{latin1}
  \item{\texttt{latin2}}~\optind{latin2}
\end{description}

The option \texttt{english} is the default one. As stated previously, the text content selected by these options may be changed by one or more of several commands provided in the package (see below \S \ref{SS:Tcomm}).

\section{Additional commands~\label{S:addcomm}~\index{additional commands}}
\subsection{Length-related commands~\label{SS:Lcomm}}
\begin{description}
  \item[\comm{setpresyl}] Command~\addcind{setpresyl} for changing the horizontal space to the right of the syllogism (def=2em).
  \item[\comm{setsyllabel}] Command~\addcind{setsyllabel} for changing the horizonal space between the label and the syllogism (def=1em).
  \item[\comm{setsylrh}] Command~\addcind{setsylrh} for changing the `width' of the rule (def=.3pt).
  \item[\comm{setsylsepa}] Command~\addcind{setsylsepa} for changing the vertical space before the syllogism (def=\texttt{\textbackslash{}baselineskip}).
  \item[\comm{setsylsepb}] Command~\addcind{setsylsepb} for changing the vertical space after the syllogism (def=\texttt{\textbackslash{}baselineskip}).
  \item[\comm{setsylsep}] Command~\addcind{setsylsep} for changing the vertical space \emph{both} before \emph{and} after the syllogism to the same value.
\end{description}

\subsection{Text-related commands~\label{SS:Tcomm}}
These commands change the default text used with \comm{syllogTA} and \comm{syllogTAC}. The defaults are set by the package options (see above \S \ref{S:options}).

\begin{description}
  \item[\comm{setsylsed}] Command~\addcind{setsylsed} for changing the text that begins the second premiss.
  \item[\comm{setsylergo}] Command~\addcind{setsylergo} for changing the text that begins the conclusion.
  \item[\comm{setsylpuncpa}] Command~\addcind{setsylpuncpa} for changing the punctuation after the first premiss (def=;).
  \item[\comm{setsylpuncpb}] Command~\addcind{setsylpuncpb} for changing the punctuation after the second premiss (def=.).
  \item[\comm{setsylpuncc}] Command~\addcind{setsylpuncc} for changing the punctuation after the conclusion (def=.).
  \item[\comm{setsylergosign}] Command~\addcind{setsylergosign} for changing the sign that goes after the conclusion text (def=$\therefore$).
\end{description}

\section{Future features~\label{S:ffeatures}}
Future features depend on user feedback and requests. However, support for more languages and automatic language selection (through \texttt{babel}) is envisaged.
\clearpage
\printindex
\end{document}% ============================================================= 