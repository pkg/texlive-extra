This work consists of the following files:

diadia.pdf
diadia.dtx
diadia-example.pdf
README
makefile

Files packaged in diadia.dtx:

diadia.sty
diadia.cfg
diadia-fallback.trsl
diadia-english.trsl
diadia-german.trsl
diadia.dat
201502.dat
201503.dat
201504.dat
hba1c.dat
diadia-example.tex
manifest.txt
