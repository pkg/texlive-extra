% $Id: biblist.gde,v 1.1 1991/10/14 14:09:50 schrod Exp schrod $
%----------------------------------------------------------------------

%
% $Log: biblist.gde,v $
% Revision 1.1  1991/10/14  14:09:50  schrod
% Initial revision
%



\subsubsection{The {\tt biblist\/} Style Option}
\label{sty:biblist}

The |biblist| option is appropriate to create a typeset listing of a
(possibly large) \BibTeX{} input file. With such large files --
especially, if the cite keys are long -- the needed string space is
often exceeded. Often a Big\TeX{} is available to circumstance this
problem, but with this style option each \TeX{} will do it.

You have to prepare a \LaTeX{} document which uses the |article|
style and the |biblist| style option. You may add almost all other
style options, as you wish, e.g., |twoside|, |german| (or other
language style options), |a4|, etc. This style option must be used
with a ragged bottom; this has the effect, that it cannot be used with
|twocolumn| or |multicol|.

You must issue a |\bibliography| tag which names all \BibTeX{}
databases which you want to print. You may issue a
|\bibliographystyle| tag to specify how \BibTeX{} will process its
databases. (There is a default bibliography style named |biblist|,
see~\ref{bst:biblist}.) You may issue |\nocite| commands if you want
to print only selected entries from the databases.

A ``bug'' you may encounter is that |\cite| tags within \BibTeX{}
entries will not be processed. Instead the cite key itself will be
printed. Note that this is not a bug, this is a feature! You have to
use |\nocite| tags for {\it all\/} entries that shall be included in
the listing. If you do not give any |\nocite| tag at all, a listing
with all entries is created.




\endinput
