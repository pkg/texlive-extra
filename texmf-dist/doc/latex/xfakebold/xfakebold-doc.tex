% $Id: xfakebold-doc.tex 11 2020-06-24 06:56:41Z herbert $
\listfiles
\documentclass[english]{article}
\usepackage{dejavu-otf}
\usepackage{xfp,dtk-logos}
\ExplSyntaxOn
\let\PI\c_pi_fp
\ExplSyntaxOff
\usepackage{xfakebold}
\usepackage{graphicx}
%\pdfcompresslevel=0
\usepackage{babel}
\usepackage[a4paper,tmargin=1cm,bmargin=1.5cm,includeheadfoot]{geometry}
\usepackage{listings}
\title{\texttt{xfakebold}, v. 0.08\\ using bold characters with\\ \texttt{pdflatex}, \texttt{lualatex} or \texttt{xelatex}}
\author{Herbert Voß}

\lstset{basicstyle=\ttfamily\small,language={[LaTeX]TeX},frame=lrtb}
\begin{document}

\maketitle

\tableofcontents

\vspace{1cm}
\begin{abstract}
The package fakes a vector font with outline characters by the text render of PDF.
It writes directly into the pdf output with \verb|\pdfliteral| and  uses the colorstack package \texttt{pdfrender} to allow
boldness with a pagebreak inside the bold part.
%(pdflatex ) or \verb|\pdfextension| \verb|literal| 
%(lualatex) or \verb|\special| (xelatex). 
The package defines two macros which can be used in text and in math mode. However, for the text
mode one should use the bold version of the text font which should be available in most cases. This is different to the
math mode where only some free math fonts provide a bold version.
\end{abstract}

\vfill
\small Thanks to: Heiko Oberdiek; Scott Pakin, Will Robertson; Yusuke Terada;
%\meaning\setBold

%\meaning\unsetBold

\newpage

\section{\TeX-engines}
With \XeLaTeX\ you do not really need this package, because you can use the optional argument \texttt{AutoFakeBold} from
package \texttt{fontspec} or \texttt{unicode-math} which does the same internally. For Lua\TeX\ the option \texttt{AutoFakeBold} is only supported
by \texttt{unicode-math} for math typesetting. For pdf\LaTeX\ this package can be used for text and math.


\section{How does it work?}
PDF knows different text render modes for outline fonts.

\bigskip
\ifxetex\else% Only for the test run with xelatex

\def\OFF{\pdfrender{TextRenderingMode=Fill,LineWidth=0}}
\def\Verb|#1|{\texttt{\{#1\}}}

\makebox[3cm][l]{Mode 0 \Verb| 0 Tr 0 w |} \tabular{l}\pdfrender{TextRenderingMode=Fill}\scalebox{10}{OTTO} OTTO\OFF\endtabular

\makebox[3cm][l]{Mode 1 \Verb| 1 Tr 0 w |} \tabular{l}\pdfrender{TextRenderingMode=Stroke,LineWidth=0}\scalebox{10}{OTTO} OTTO\OFF\endtabular

\makebox[3cm][l]{Mode 1 \Verb| 1 Tr 0.3 w |} \tabular{l}\pdfrender{TextRenderingMode=Stroke,LineWidth=0.3}\scalebox{10}{OTTO} OTTO\OFF\endtabular

\makebox[3cm][l]{Mode 2 \Verb| 2 Tr 0 w |} \tabular{l}\pdfrender{TextRenderingMode=FillStroke,LineWidth=0}\scalebox{10}{OTTO} OTTO\OFF\endtabular

\makebox[3cm][l]{Mode 2 \Verb| 2 Tr 0.3 w |} \tabular{l}\pdfrender{TextRenderingMode=FillStroke,LineWidth=0.3}\scalebox{10}{OTTO} OTTO\OFF\endtabular
\fi

\bigskip
In mode 0 the character is filled but without drawing its outline which can be seen when printing in mode 1, where
the linewidth of the outline is the smallest one which the system allows. Setting the linewidth to 0.3\,bp, which is
nearly the same as 0.3\,pt, the linewidth of the outline increases. In mode 2 the character is printed with filling \emph{and}
drawing the outline, which is mode 0 and 1 together. The reason why the character is bold by default.  Increasing the linewidth 
makes it more bold.


\section{Optional package argument}
The only package option is \verb|bold| which is preset by 0.3, which is the linewidth of
the outlines of the characters.

\begin{lstlisting}
\usepackage[bold=0.6]{xfakebold}
\end{lstlisting}

makes the characters more bold.

\section{The macros}

\begin{lstlisting}
\setBold[<optional value>]
\unsetBold
\end{lstlisting}

Without using the optional argument the default setting is used.




\section{The example code}

The following examples use the value for $\pi$, defined in \LaTeX3 as \verb|\c_pi_fp|. To get rid of
the L3-syntax we define a new variable:

\begin{lstlisting}
\ExplSyntaxOn
\let\PI\c_pi_fp
\ExplSyntaxOff
\end{lstlisting}

\subsection{Default setting}
\begin{lstlisting}
\documentclass{article}
\usepackage{xfakebold,xfp}
\begin{document}
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\unsetBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\end{document}
\end{lstlisting}

\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\unsetBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$


\section{Loading the package with  another value}

\begin{lstlisting}
\documentclass{article}
\usepackage[bold=1]{xfakebold}
\usepackage{xfp}
\begin{document}
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\unsetBold\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\end{document}
\end{lstlisting}

\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\ifxetex\special{pdf:literal direct 2 Tr 1 w }%
\else\pdfrender{TextRenderingMode=FillStroke,LineWidth=1}\fi
\noindent 
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\ifxetex\special{pdf:literal direct 0 Tr 0 w }%
\else\pdfrender{TextRenderingMode=Fill,LineWidth=0}\fi
\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\section{Using the optional argument of the macro}

\begin{lstlisting}
\documentclass{article}
\usepackage{xfakebold,xfp}
\begin{document}
\setBold[0.01]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold[0.2]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold[0.6]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\unsetBold

\setBold\noindent% Using the default value
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\unsetBold
\end{document}
\end{lstlisting}

\setBold[0.01]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold[0.2]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$

\setBold[0.6]\noindent
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\unsetBold

\setBold\noindent% Using the default value
An example: 
$\pi^{\pi}=\fpeval{\PI^\PI}$ and
$\displaystyle\int\limits_1^{\infty}\frac1{x^2}\symup dx=1$
\unsetBold
\end{document}