(Version indicator: 2019 Nov 01)

The halloweenmath package originated from a question asked for enjoyment
on TeX-LaTeX Stack Exchange <http://tex.stackexchange.com> by the user
cfr (see <http://tex.stackexchange.com/q/336768/69818>); it defines a
handful of commands for typesetting mathematical symbols of various
kinds, ranging from "large" operators to extensible arrow-like relations
and growing arrow-like math accents, that all draw from the classic
Halloween-related iconography (pumpkins, witches, ghosts, cats, and so
on) while being, at the same time, seamlessly integrated within the rest
of the mathematics produced by (AmS-)LaTeX.

The halloweenmath package may be distributed and/or modified under
the conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.

For more information, read the following files:

00readme.txt            --  start by reading this file
halloweenmath-doc.pdf   --  overview of the halloweenmath package (PDF)
halloweenmath-man.pdf   --  user's manual for this same package (PDF)

Other files that make up the distribution are:

manifest.txt        --  legal stuff
halloweenmath.dtx   --  main source file
halloweenmath.ins   --  installation script

See the file `manifest.txt' for a more precise specification of what
files constitutes a LPPL-compliant distribution.
