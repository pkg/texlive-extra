biblatex-jura2 - Citations for the German Legal Profession
==========================================================

The `biblatex-jura2` bundle provides a citation style for the German legal profession. 

## Licence

This work may be distributed and/or modified under the conditions of the LaTeX Project Public License (LPPL), either version 1.3c of this license or (at your option) any later
version. The latest version of this license is at <http://www.latex-project.org/lppl.txt> and version 1.3 or later is part of all distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status 'maintained'.

The Current Maintainer of the work is Christoph <biblatex-jura2@gmx.net>. Please report bugs via email. Suggestions for improvements and feature request are also welcome.