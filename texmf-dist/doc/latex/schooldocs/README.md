# The schooldocs package - Various layout styles for school documents


## Presentation

The purpose of this package is to provide several layout styles for school documents.
It is usefull for exercices sheets, exams, course materials.
The package sets the page geometry (dimensions of text and margins)
and the title typesetting;
the various styles define the header, footer and title formatting, 
Many features are freely configurable.


## Installation

- run LaTeX on schooldocs.ins, you obtain the file schoodocs.sty,
- if then you run pdfLaTeX on schooldocs.dtx you get the file schooldocs.pdf which is also in the archive,
- put the files schooldocs.sty and schooldocs.pdf in your TeX Directory Structure.


## Author

Antoine Missier 

Email: antoine.missier@ac-toulouse.fr


## License

Released under the LaTeX Project Public License v1.3 or later. 
See http://www.latex-project.org/lppl.txt
