The nChairX-Style Package
============================

Overview
--------

The `nchairx` package has been developed by members of the chair for 
mathematical physics at the University of Wuerzburg as a collection
of macros and predefined environments for quickly creating nice
mathematical documents.

Documentation
-------------

The primary documentation for `nchairx` is in provided as `nchairx.pdf`.
Additionally, the file `chairxmath.pdf`contains the documentation only 
for the math macros included in the `chairxmath` package.


Reporting Bugs
--------------

If you wish to report a problem or bug in the `nchairx` bundle
please contact us via marvin.dippell@uni-wuerzburg.de.




License
-------

The contents of this bundle are distributed under the [LaTeX Project
Public License](https://www.latex-project.org/lppl/lppl-1-3c/),
version 1.3c.
