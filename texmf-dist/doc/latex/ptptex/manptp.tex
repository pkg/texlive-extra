%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   template.tex for PTPTeX.cls <ver.0.91>  %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[seceq]{ptptex}

%\documentclass[letter]{ptptex}
%\documentclass[seceq,supplement]{ptptex}
%\documentclass[seceq,addenda]{ptptex}
%\documentclass[seceq,errata]{ptptex}
%\documentclass[seceq,preprint]{ptptex}
%\usepackage{graphicx}

\usepackage{wrapft}

%%%%% Personal Macros %%%%%%%%%%%%%%%%%%%

\def\BS{\ttfamily \symbol{"5C}}        %backslash
\def\itPTP{{\slshape Progress of Theoretical Physics}}        %backslash
\def\ttmac#1{{\ttfamily  \BS #1}}
\def\boxmac#1{\fbox{\ttmac{#1}}}
\def\boxenv#1{\fbox{\ttfamily  #1}}
\def\asp{.3em} 
\def\bsp{.3em}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pubinfo{Vol.~120, No.~5, November 2008}%Editorial Office will fill
%\setcounter{page}{}                 %Editorial Office use                  
%\def\ptype{p}                       %Editorial Office use
%\def\ptpsubject{}                   %Editorial Office use   
%\def\pageinfo{X-X}                  %Editorial Office use  
%------------------------------------------------------------
%\nofigureboxrule%to eliminate the rule of \figurebox
%\notypesetlogo  %comment in if to eliminate PTPTeX logo
%\subfontMBF     %use if you have not enough fonts when using mbf.sty
%---- When [preprint] you can put preprint number at top right corner.
%\preprintnumber[3cm]{%<-- [..]: optional width of preprint # column.
%KUNS-1325\\PTPTeX ver.0.9\\ August, 1997}
%-------------------------------------------

\markboth{%     %running head for even-page (authors' name)
S.-I.~Tomonaga and H.~Yukawa
}{%             %running head for odd-page (`short' title)
Instruction for Making \LaTeX\ Compuscripts Using \protect\PTPTeX}

\title{%        %You can use \\ for explicit line-break.
Instruction for Making \LaTeX\ Compuscripts Using \PTPTeX
}
\subtitle{\LaTeXe\ Version}    %Use this when you want a subtitle.

\author{%       %Use \scshape  for the family name.
Shin-Ichiro \textsc{Tomonaga}$^{1,}$\footnote{A friend of Schwinger,
because they both have `swing' in their names.} 
and Hideki \textsc{Yukawa}$^{2,}$\footnote{A friend of Fermi and
Bose. E-mail: yukawa@yukawa.kyoto-u.ac.jp}
}

\inst{%         %Affiliation, neglected when [addenda] or [errata].
$^1$Physics Department, Tokyo Bunrika University, Tokyo 113-1234, Japan\\
$^2$Yukawa Institute for Theoretical Physics, Kyoto University,\\
Kyoto 606-8502, Japan
}

%\publishedin{%     %Write this ONLY in cases of [addenda] and [errata].
%Prog.~Theor.~Phys.\ {\bfseries  XX} (19YY), page}

\recdate{%      %Editorial Office will fill in this.
April 1, 2004; revised November 20, 2008}

\abst{%         %This abstract is neglected when [addenda] or [errata].
This is a manual for making \LaTeX\ compuscripts for 
\itPTP\, using the \PTPTeX\ class file ``ptptex.cls.'' In particular,
we explain some useful options, macros and environments that are
specially prepared with ptptex.cls.
The source file for this manual itself is designed to provide a template 
that can be used for writing compuscripts.
}

%\PTPindex{123, 456}   %Input the subject index(es) of your paper, 
                       %neglected when [supplement], [addenda] or [errata].
% The list of Subject Index is available at
% http://solution.dynacom.jp/cgi-bin/ptp/submission/subject_index.cgi

\begin{document}
\maketitle


\section{Introduction}

We presently print all articles in \itPTP\ (PTP) directly from 
\LaTeXe manuscripts using ptptex.cls.
In this text, we explain how to use ptptex.cls.
In \PTPTeX, all the usual \LaTeX\ commands can be used, 
and there are some additional options, macros and environments that are 
specially prepared. This manual explains the commands specific 
to \PTPTeX\ from \S2 on.

\subsection{Submission}

We started ``PTP Online Submission and Review System'' on September 1, 2008. 
All processes, from submission to review, is completely handled on the Web. 
This system makes it easier to submit papers and faster to review them.   
Authors can check the progress of review and also see the record of 
submissions.

For ``PTP Online Submission and Review System'', please access the
following URL:

{\vskip \asp
\begin{verbatim}
             http://www2.yukawa.kyoto-u.ac.jp/~ptpwww/index.html
\end{verbatim}\vskip\bsp}

\vspace*{3mm}

\subsection{How to obtain the \protect\PTPTeX\ style file}

The \PTPTeX\ class file can be obtained from the
following URL:

{\vskip \asp \baselineskip 1.5em
\begin{verbatim}
       http://www2.yukawa.kyoto-u.ac.jp/~ptpwww/ptpcls.html
\end{verbatim}\vskip\bsp \baselineskip 1.5em}\noindent
Please check this site occasionally to obtain the most recent version,
as it is revised from time to time.

\newpage

The following files are obtained in the above site:
\begin{center}
\let\tabularsize\normalsize
\begin{tabular}{lp{.7\textwidth}} 
1. manptp.tex & Source file for this manual.\\
2. template.tex & Template for making a \PTPTeX\ compuscript.\\
3. ptptex.cls & Main class file of \PTPTeX.\\
4. ptp-prep.clo & Class option file for preprint style output.\\
5. wrapft.sty & Macro for wrapfigure and wraptable environments. \\
6. wrapfig.sty & (Macro called by wrapft.sty)\\
7. overcite.sty & (Macro called by ptptex.cls)\\
\end{tabular}\end{center}
The macros 6 and 7 are  style files attached to the standard
\LaTeX\ systems. We include them here because using 
different versions together may cause unexpected output.

The \PTPTeX\ class file takes as inputs the style files
amsmath.sty and \linebreak 
amssymb.sty, which are included in
the AMS-\LaTeX\ package.{\footnote{The amsmath.sty style
file, in turn, reads in several other AMS-\LaTeX\ files.}}
With these files input, AMS-\LaTeX\ commands, such as
\boxmac{boldsymbol} (and including commands that produce
special mathematical symbols) can be used. In the case that
these style files are not found, an error message appears and
compilation is suspended. Also, in the case that the \LaTeX\ 2.09
version of amsmath.sty is input, an error results.{\footnote{The error
message displayed in this case is {\tt ``Package amsfonts Warning: 
Obsolete command $\backslash$newsymbol; 
$\backslash$DeclareMathSymbol should be used instead $\cdots$''.}}
 For the correct versions of these files, the \LaTeXe\ version of 
the AMS-\LaTeX\ package is necessary.


\section{Style and preamble}

When creating a compuscript using ptptex.cls, 
please make use of the \linebreak
{\bfseries  template.tex} file, which is prepared for users' 
convenience as a separate file. With this, 
it is easy to make your compuscript in ptptex.cls format. 
The default style of ptptex.cls is that for Regular 
Articles in {\slshape Progress of Theoretical Physics}. 

You can start your \LaTeX\ source file with the line 
{\vskip \asp \baselineskip 1.1em
\begin{verbatim}
        \documentclass{ptptex}
\end{verbatim}
or
\begin{verbatim}
        \documentclass[seceq]{ptptex}
\end{verbatim}\vskip\bsp}
\noindent
Generally, a \LaTeX\ command appearing in square brackets
{\ttfamily  [ ]} is an ``optional argument" and can be omitted. 
The optional argument {\ttfamily  [seceq]} in the second of the above 
command lines is a declaration 
to create equation numbers that include 
section numbers, like (2$\cdot$15).  If this optional command is
omitted, then the equation numbers will appear as a single sequence 
from the beginning to the end of the paper, 
like (1),\ (2),\ $\cdots$.

In a usual \LaTeX\ {\ttfamily article.cls}, this first line is something like 
{\vskip \asp
\begin{verbatim}
        \documentclass[12pt]{article}
\end{verbatim}\vskip\bsp}
\noindent
If such a command line is replaced by one of those given above, then 
the output automatically becomes of 
the Article form for {\sl Progress of Theoretical Physics}. 
Since ptptex.cls is designed to be as compatible with the usual 
\LaTeX\ \verb+article.cls+ as possible, a source file 
using \LaTeX\ \verb+article.cls+ can (probably) also be compiled with
ptptex.cls.

In case you wish for the output to be in ``preprint" form 
for private distribution, the file should begin with the line
{\vskip \asp
\begin{verbatim}
        \documentclass[seceq,preprint]{ptptex}
\end{verbatim}\vskip\bsp}
\noindent

If you are writing a Letter article, Supplement article, Addenda or 
Errata instead of a regular PTP article, please start with the 
appropriate one of the following:
{\vskip \asp \baselineskip 1.1em
\begin{verbatim}
        \documentclass[letter]{ptptex}
        \documentclass[seceq,supplement]{ptptex}
        \documentclass[seceq,addenda]{ptptex}
        \documentclass[seceq,errata]{ptptex}
\end{verbatim}\vskip\bsp}
\noindent

The part of the source file from the first \verb+\documentclass+ line 
to the declaration line
{\vskip \asp
\begin{verbatim}
        \begin{document}
\end{verbatim}\vskip\bsp}
\noindent
is called the {\itshape  preamble}. In the preamble there usually appear 
definitions of personal macros and style specifications. 
However, because style specifications are quite unnecessary 
(and even harmful) in \PTPTeX, please do not include them. 
In \PTPTeX, you should enter the appropriate information in the 
following items (within the brackets) in the preamble.
You do not have to enter in ``\verb+\recdate{ }+}''.
{\vskip \asp \baselineskip 1.1em
\begin{verbatim}
     \markboth{ }{ } : Running head  [in the left { } appears the 
                         author's name (or authors'names), and in 
                         the right { } appears the paper's title]
      \title{ }      : Title of paper
      \author{ }     : Author's name (or authors' names)
      \inst{ }       : Institution (address)
      \recdate{ }    : Date received
      \abst{ }       : Abstract
\end{verbatim}\vskip\bsp}
\noindent
An example of the above is given in {\ttfamily  template.tex}. 
(Please beware of the fact that the commands appearing from \verb+\inst+ to 
\verb+\abst+ are macros particular to ptptex.cls. 
For this reason, if you compile a file containing these commands
using \LaTeX\ \verb+article.cls+, you will get 
the error message\  `{\ttfamily  !\,Undefined control sequence}'.)


In the preamble of {\ttfamily  template.tex}, the following 
commands also appear, but each is ``commented out" by the symbol 
{\ttfamily  \%} appearing in front:
\begin{center}\renewcommand\tabularsize{\normalsize}
\begin{tabular}{lcl}
   \verb+\notypesetlogo+         &:& Prevents appearance of
 ``{\sffamily  typeset using}\\
&&  {\sffamily \PTPTeX.cls}" in output file.\\
   \verb+\publishedin{ }+        
&:& Vol/Year/Page of the paper about which \\
&& Addenda or Errata is written.\\
\end{tabular}
\renewcommand\tabularsize{\normalsize}
\begin{tabular}{lcl}
   \verb+\preprintnumber[+{\itshape  width}\verb+]{ }+ &:& To include
 preprint numbers when manuscript \\ 
&&is printed. Here [{\itshape  width}] stipulates the width of \\ 
&&the preprint number column. \\
\end{tabular}
\end{center}
Use these commands (by deleting the preceding ``\,{\ttfamily \%}\,") 
when necessary. Their usage is made clear in 
the {\ttfamily  template.tex}.

In general, any packages to be used in compiling the manuscript are called
in the preamble using the \verb+\usepackage+ command. However, such
files as amsbsy.sty (which is necessary for a number of commands,
including \verb+\boldsymbol+) are automatically input when 
{\ttfamily ptptex.cls} is used, and therefore there is no need for them to
be called explicitly.

\section{Equations}

With the \LaTeXe\ version of \PTPTeX, the amsmath.sty style file is input
automatically, and therefore all of the basic mathematical commands contained
in AMS-\LaTeX\ can be used. Here we briefly explain two particularly
useful such commands.

\subsection{Mathematical italic bold}

The \verb+\boldsymbol+ command is used to make
math-bold (mathematical italic bold) fonts. Its use is demonstrated below:
{\vskip \asp
\begin{verbatim}
                      \boldsymbol{\alpha kx}
\end{verbatim}
\vskip\bsp}\noindent

\subsection{Subequations}

To number equations in a form like
(3$\cdot$2a), (3$\cdot$2b), $\cdots$ in an array of equations,
you can use the \boxenv{subequations} environment.  Its usage is 
clear from the following simple example:
%\vskip .5em
\begin{center}
\begin{minipage}[t]{6.5cm}
\centerline{\bfseries  Input}
\vskip .4em
\baselineskip 1.1em 
\begin{verbatim}
\begin{subequations}
  \label{eq:1}
  An example of subequations:
  \begin{equation}
    \alpha + 2\beta + \gamma = 2
    \label{eq:1a}
  \end{equation}
  Here is a sentence,
  which can be of any length.
  \begin{eqnarray}
    \gamma &=& \nu (2-\eta) \\
    \delta &=& \mu (1+\rho)
  \end{eqnarray}
\end{subequations}
\end{verbatim}
\end{minipage}
\begin{minipage}[t]{.8cm}
~\vspace{5.3\baselineskip}
\begin{tabular}{c}
$\Longrightarrow$ \\
gives
\end{tabular}
\end{minipage}
\hspace{.3cm}
\begin{minipage}[t]{.4\textwidth}
\centerline{\bfseries  Output}
\vskip 22pt
\fbox{
\begin{minipage}[t]{.95\textwidth}
\begin{subequations}
  \label{eq:1}
  An example of subequations:
  \begin{equation}
    \alpha + 2\beta + \gamma = 2
    \label{eq:1a}
  \end{equation}
  Here is a sentence, which can be of any length.
  \begin{eqnarray}
    \gamma &=& \nu (2-\eta) \hspace{4em} \\
    \delta &=& \mu (1+\rho)
  \end{eqnarray}
\end{subequations}
%\vskip 0em%\baselineskip
\end{minipage}}
\end{minipage}
\end{center}
\vskip 1em
\noindent
Here note that the command \verb+\label{eq:1}+ just after 
\verb+\begin{subequations}+ defines the label for the entire array,
so that \verb+(\ref{eq:1})+ gives 
``(\ref{eq:1})", while \linebreak 
\verb+\label{eq:1a}+ refers to the first 
equation in the array, (\ref{eq:1a}). 



\section{References}

References are cited using the \boxmac{cite} command.  All references 
to be cited at  the same point in the paper
should be listed in a single series and separated by commas inside the 
curly brackets, as \verb+\cite{rf:1,rf:3,rf:4,rf:5}+. 
(There should be no space after any commas here. Such a space will
be interpreted  by \LaTeX\ as being part of the reference label.)
When three or more consecutive reference numbers appear, as in this 
example, the numbers are automatically printed in a compressed 
format, like that at the end of this sentence.~\cite{rf:1,rf:3,rf:4,rf:5} 
In the case that all the references to be cited have consecutively 
numbered reference numbers, the following type of command can be used:
\verb+\cite{rf:3}\tocite{rf:5}+. In this way, only the first and
last reference numbers need to be included.
The output obtained from this command is that appearing at the end of 
this sentence.\cite{rf:3}\tocite{rf:5} 
The \ \boxmac{citen} command can be used to obtain citation numbers:
e.g., \verb+Ref.~\citen{rf:3}+ gives the output ``Ref.~\citen{rf:3}". 

\def\spacesymb{\raisebox{-1pt}[0pt][0pt]{$\sqcup$}}
When you use the \ttmac{cite} command at the end of a sentence, 
it should \linebreak 
always appear {\itshape  after a period, comma, colon or 
semicolon}, as follows: \verb+...some+ \linebreak
 \verb+text.\cite{rf:5}+ \ 
Then, between such a command and the next sentence (if the paragraph 
is continued), the command 
 \spacesymb{\ttfamily  \BS}\spacesymb\ (where \spacesymb\ represents 
a space) should be included to give an 
appropriate space between sentences. For instance, typing 
{\vskip \asp \baselineskip 1.1em
\begin{verbatim}
   This is $\cdots$ something.\cite{rf:5} \ Therefore we can $\cdots$ 
\end{verbatim}\vskip\bsp}\noindent
the output becomes the following:
This is $\cdots$ something.\cite{rf:5} \  Therefore we can $\cdots$ 

The references are included at the end of the file by using the 
\boxenv{thebibliography} environment and \boxmac{bibitem} command,
as usual. For example, the references at the end of this manual are 
input as follows:
{\vskip \asp \baselineskip 1.1em
\begin{verbatim}
\begin{thebibliography}{99}
 \bibitem{rf:1} 
    Leslie Lamport, \textit{LaTeX: A Document Preparation System} 
    (Addison-Wesley, New York, 1986).
 \bibitem{rf:2} 
    S.~Weinberg, Phys.\ Rev.\ Lett.\ \textbf{19} (1967), 1264.
 \bibitem{rf:3} 
    M.~Kobayashi and T.~Maskawa, \PTP{49,1973,652}.
 \bibitem{rf:4} 
    D.~Gross and F.~Wilczek, \PRL{30,1973,1343}.\\
    H.~D.~Politzer, \PRL{30,1973,1346}.
 \bibitem{rf:5}
    Y.~Nambu, Phys.\ Rev.\ \textbf{117} (1960), 648; \PRL{4,1960,380}.\\ 
    G.~'t~Hooft, \NPB{33,1971,173}; \NPB{35,1971,167}.
 \bibitem{rf:6}
    E.~Witten, \JL{Adv.\ Theor.\ Math.\ Phys.,2,1998,253}, 
    hep-th/9802150; hep-th/0112258; arXiv:0710.0631.
 \bibitem{rf:7}
    M.~Harada, Y.~Kikukawa, T.~Kugo and H.~Nakano, \PTP{92,1994,1161} 
    [Errata; \textbf{95} (1996), 835].
\end{thebibliography}
\end{verbatim}\vskip\bsp}
\noindent
Please input your bibitems following these examples 
in the PTP format. In particular, the volume numbers should be in 
boldface, ({\it{year}}) should be followed by a comma, and 
each item should end with a period.  When 
two or more references by different authors are cited in a single item, 
as in {\ttfamily  rf:4}, each reference should end with a period, and the
next reference should be preceded by a line return, facilitated by the 
command {\ttfamily  \BS\BS}.  When two or more references by the same 
author are cited in a single item, as in {\ttfamily  rf:5}, each
should be separated by a semi-colon without a line return.
For the proper format to be used in citing references from ``e-print
arXiv,'' see {\ttfamily  rf:6} as an example.  In the case that a 
preprint reference of this type corresponds to a paper already 
published in a journal, whose reference appears before it, the two 
should be separated by a comma.  In the case that such a preprint has 
not yet been published, if its reference appears after a reference for
a paper published by the same author, the two references should be 
separated by a semi-colon.  To make it easier to input bibitems 
following this PTP format, we prepared the macros 
\boxmac{JL}\ , \boxmac{andvol}\ , \boxmac{PRL}\ , 
$\cdots$, which are used in {\ttfamily  rf:3} -- {\ttfamily  rf:7}.
When referring to an errata, please follow the example of {\ttfamily rf:7}.

Personal, custom-made macros cannot be used in the bibliography section.
And also please do not use Bib\TeX.
This is because in PTP Online (the online version of Progress of 
Theoretical Physics), we provide a hyperlink
function that allows one to link from PTP papers to the online
versions of the references listed in their bibliographies.
Custom-made macros in the bibliography section and Bib\TeX\ may cause errors
to arise in this process and make such linking impossible.
If you wish to use macros in writing your bibliography, please use only
the following macros, which are designed specifically for PTP:
\begin{center}\renewcommand\tabularsize{\normalsize}
\begin{tabular}{r@{}l@{ : }p{4cm}l@{ : }p{4cm}}
   $\circ\ $ &\multicolumn{4}{@{}l}{for general use}\\
     & \verb+\JL+ & general journals         
& \verb+\andvol+ & Vol.~(Year), Page\\
$\circ\ $ &\multicolumn{4}{@{}l}{for individual journal}\\
     & \verb+\AJ+  & Astrophys.~J.  & \verb+\NC+  & Nuovo Cim. \\
     & \verb+\ANN+ & Ann.~of Phys.
     & \verb+\NPA+, \verb+\NPB+  & Nucl.~Phys.~[A, B]  \\
     & \verb+\CMP+ & Commun.~Math.~Phys.
     & \verb+\PLA+, \verb+\PLB+ & Phys.~Lett.~[A, B]  \\
     & \verb+\IJMP+& Int.~J.~Mod.~Phys.     
     & \verb+\PRA+ -- \verb+\PRE+ & Phys.~Rev.~[A--E] \\
     & \verb+\JHEP+& J.~High Energy Phys.
     & \verb+\PRL+ & Phys.~Rev.~Lett. \\
     & \verb+\JMP+ & J.~Math.~Phys. & \verb+\PRP+ & Phys.~Rep.\\
     & \verb+\JP+  & J.~of Phys.  & \verb+\PTP+ & Prog.~Theor.~Phys.  \\    
     & \verb+\JPSJ+& J.~Phys.~Soc.~Jpn.
     & \verb+\PTPS+&Prog.~Theor.~Phys.~Suppl. \\
\end{tabular}
\end{center}
The above macros are defined such that the output of each line on the 
left below is that given
to its right (with those macros not appearing below defined similarly):
\begin{center}\renewcommand\tabularsize{\normalsize}
\begin{tabular}{lcl}
\verb+\PRD{45,1990,34}+ & $\Rightarrow$ &
  \verb+Phys.\ Rev.\ D \textbf{45} (1990), 34+ \\
\verb+\JL{Nature,418,2002,123}+ & $\Rightarrow$ &
  \verb+Nature \textbf{418} (2002), 123+ \\
\verb+\andvol{123,1995,1020}+ & $\Rightarrow$ &
  \verb+\textbf{123} (1995), 1020+\\[1mm]
\end{tabular}\end{center}
Note that here again, no space should appear after commas separating 
volume, year and page numbers in the argument.


\section{Figures}

\subsection{PostScript (ps/eps) figure files}

Figure files in the form of epsf (encapsulated PostScript files)
are best suited for \PTPTeX\ manuscripts.
To insert an epsf figure into the text, the file 
{\ttfamily graphicx.sty}\footnote{{\ttfamily graphicx.sty} is included 
in standard \LaTeX\ distributions} must be input. This can be 
accomplished by including the following line in the preamble of 
the manuscript source file:
{\vskip \asp
\begin{verbatim}
        \usepackage{graphicx}
\end{verbatim}\vskip\bsp}\noindent 
Then, at the point in the manuscript where you wish for the figure to begin,
command lines like the following should be input:
{\vskip \asp
\begin{verbatim}
   \begin{figure}
       \centerline{\includegraphics[width=WIDTH cm,height=HEIGHT cm]
                                   {FILENAME.eps}}
   \caption{Explanation of the figure.}
   \label{fig:1}
   \end{figure}
\end{verbatim}\vskip.4em}
\noindent
Here, {\ttfamily  width=WIDTH cm} specifies the horizontal size 
and {\ttfamily  height=HEIGHT cm} the vertical size.
If one of these is omitted, the size is automatically set
by making the ratio of the height and width of the figure the same as 
that of the original epsf figure. If both of them are omitted, the
size defined in the epsf file is used. However, it is best not to omit
both, because in this case an unexpected result can occur for some 
epsf files.

If you wish to incorporate figures for which there only exist hard copies,
the original figures should be mailed to the PTP editorial office separately.
(They can be sent with other such correspondence, for example orders
for reprints.)
In this case, space for the figures in the \LaTeX\ source file should 
be created using the
\boxmac{figurebox} command. The following demonstrates how this is done:
{\vskip\asp
\begin{verbatim}
        \begin{figure}
        \figurebox{WIDTH}{HEIGHT}
        \caption{This is the caption.}
        \label{fig:1}
        \end{figure}
\end{verbatim}\vskip\bsp}
\noindent
To delete the frame demarking this {\ttfamily  figurebox}, 
the command \boxmac{nofigureboxrule} should be added to the preamble. In 
{\ttfamily  template.tex}, this command can be enabled by deleting the 
{\ttfamily  \%} appearing before it.

\subsection{Wrapfigure environment}

\begin{wrapfigure}{r}{6.6cm}
  \figurebox{60mm}{3cm}
\caption{A figure created using the {\ttfamily wrapfigure} environment.}
\label{fig:2}
\end{wrapfigure}
Narrow figures should be displayed with half text width. (The size
 used here is 6.6cm, which is actually 4mm smaller than the true
half text size, as it is necessary to include a space.)
The \boxenv{wrapfigure} environment can be used for this purpose. 
We give an example of the \verb+wrapfigure+ in Fig.~\ref{fig:2}. 
It should be noted that the \verb+wrapfigure+ environment is not a 
floating environment; that is, the position of the figure in the 
manuscript corresponds to the place in the source
file at which the \verb+\begin{wrapfigure}+ command appears, whether 
there is sufficient space for the figure or not.
Please take care, therefore, when this environment is used 
near the beginning or end of a section, subsection or page. Also,
it is best to incorporate figures using 
\verb+wrapfigure+ only when creating the final form of the source
file, after no further changes in the text are to be made. 

The use of the wrapfigure environment is demonstrated by the following 
commands, which create Fig.~\ref{fig:2} and appear at the beginning of 
the previous paragraph in the source file of this manuscript:
\begin{center}
\begin{verbatim}
    \begin{wrapfigure}{r}{6.6cm}   % r: RIGHT, 6.6cm: WIDTH  
      \figurebox{60mm}{3cm}
      \caption{A figure created using the {\ttfamily wrapfigure} 
       environment.}      
      \label{fig:2}
    \end{wrapfigure}
    Narrow figures should be displayed with half text width.... 
\end{verbatim}
\end{center}
Also, note that \verb+\centerline{\includegraphics{FILENAME.eps}}+ can 
be used here in place of \verb+\figurebox+.

To use the wrapfigure environment, {\ttfamily  wrapft.sty} must be 
input. This can be done by including the following command in the preamble:
{\vskip \asp
\begin{verbatim}
        \usepackage{wrapft}
\end{verbatim}\vskip\bsp}\noindent
The basic format of the \verb+wrapfigure+ environment is
{\vskip \asp
\begin{verbatim}
      \begin{wrapfigure}[number]{position}{width}
          <figure> etc.                          
          \caption{ <caption> }                  
      \end{wrapfigure}                           
\end{verbatim}\vskip\bsp}
\noindent
Here the optional argument {\ttfamily  [number]} specifies the number of
text lines corresponding to the height of the figure. Since this is 
almost always calculated automatically with no problem, it is usually 
best to omit it. It should be explicitly included only when there is a 
special need to enforce a certain value for this number.
(For instance, when a figure is placed at the bottom of a page, the
text lines at the beginning of the next page can sometimes become
narrow. This can be avoided by explicitly specifying the 
{\ttfamily  [number]}.) \ The argument \verb+{width}+ 
specifies the width of the space allocated for the
figure (and the figure caption). PTP allows only half
the text width for this size (7cm), for aesthetic reasons. 
For this reason, the argument
input here must be \verb+{6.6cm}+ or \verb+{\halftext}+, 
which allots the proper space between the figure and the text. 
(The quantity \boxmac{halftext} is defined to be 0.471
times {\ttfamily  \BS textwidth}. This is 6.6cm for PTP text
style.) The argument \verb+{position}+ is \verb+{r}+, 
which positions the figure on the right, or \verb+{l}+, 
which positions the figure on the left. 
(In PTP, half-size figures are placed on the right for odd pages 
and on the left for even pages.)

If you are inserting a full-size figure and wish to specify its 
position exactly, instead of using the floating positioning of the 
usual \verb+figure+ environment,
you can employ the wrapfigure environment
and set the position as \verb+{c}+
(center). In this case, the command line beginning the
figure would be \verb+\begin{wrapfigure}{c}{width}+.
With this command line, the figure is treated just as if 
the \verb+figure+ environment were used,
except that it  is positioned at the point in the manuscript 
corresponding to the position of this command line
in the source file.
The item \verb+{width}+ specifies the
caption width in this case. In the place where 
``{\ttfamily  <figure> etc.}" appears here,
an epsf figure can be input (as in the example used to create 
Fig.~1) or a picture environment can be
used, among other options.

To position a figure at the beginning of a paragraph, as
in the case of Fig.~\ref{fig:2}, place 
the commands for the \verb+wrapfigure+ environment before the 
beginning of the paragraph. If you wish to position a figure within 
a paragraph, this can be done manually. First, comment out the entire
\verb+wrapfigure+ environment and compile the source file.
Then, preview the manuscript and note the word that appears
at the end of the line below which you wish to place the 
figure. Insert the entire \verb+wrapfigure+ environment 
directly after this word. 

If you would like to avoid all such complications, please simply 
include all the figures with the ordinary \verb+figure+ environment. 
The Editorial Office will make the appropriate changes.

\subsection{Putting figures side by side}

Using either the {\ttfamily  figure} or {\ttfamily  wrapfigure} environment,
it is easy to position two or more figures in immediate succession vertically. 
To do this, simply repeat the {\ttfamily  <figure>} part of the environment,
making reference to the each of the figure files to be displayed. 
To include a separate caption for each such figure, the 
{\ttfamily  \BS caption} part must also
be repeated accordingly.

It is not so simple to place two figures side by side. In fact 
there are several ways to do this. Below we demonstrate one, in which the 
{\ttfamily  \BS parbox} command is used:
\begin{verbatim}
        \begin{figure}[htb]
            \parbox{\halftext}{%   %\def\halftext{.471\textwidth}
                \figurebox{6cm}{2cm}
                \caption{The first figure on the left.}}
            \hfill
            \parbox{\halftext}{
                \figurebox{6cm}{2cm}
                \caption{The second figure on the right.}}
        \end{figure}
\end{verbatim}
This gives the following output:
\begin{figure}[htb]
 \parbox{\halftext}{\figurebox{6cm}{2cm}
                \caption{The first figure on the left.}}
 \hfill
 \parbox{\halftext}{\figurebox{6cm}{2cm}
                \caption{The second figure on the right.}}
\end{figure}


\section{Tables}

\begin{wraptable}{r}{\halftext}
\caption{An example of a small table created with 
        {\ttfamily  \BS begin\{wraptable\}\{r\}\{\BS halftext\}}.}
\label{table:1}
\begin{center}
\begin{tabular}{ccc} \hline \hline
temperature & energy  & specific heat \\ \hline
0.1     & 0.24  & 2.46\\
0.2     & 0.80  & 4.62\\
0.3     & 1.11  & 3.27\\ \hline
\end{tabular}
\end{center}
\end{wraptable}
To make tables in \PTPTeX, the standard \LaTeX\ \verb+table+
environment can be used.
Here we give an example of a small table  
using the \boxenv{wraptable} environment in Table~\ref{table:1}. 
This environment is also supported by the {\ttfamily  wrapft.sty} file,
discussed above, and the format for this environment is quite similar 
to that for the \verb+wrapfigure+ environment. For tables in PTP the 
conventions are, as in this example, to put the caption above the 
table and to use a double line only at the top with the repeated 
commands {\ttfamily  \BS hline\ \BS hline}. Please follow these 
conventions. Table~\ref{table:1} was created with the
following: 
{\vskip.4em\baselineskip 1.1em
\begin{verbatim}
     \begin{wraptable}{r}{\halftext}
       \caption{An example of small table created with 
           {\ttfamily \BS begin\{wraptable\}\{r\}\{\BS halftext\}}.}
       \label{table:1}
       \begin{center}
         \begin{tabular}{ccc} \hline \hline
          temperature & energy  & specific heat \\ \hline
              0.1     &  0.24   &  2.46  \\
              0.2     &  0.80   &  4.62  \\
              0.3     &  1.11   &  3.27  \\ \hline
         \end{tabular}
       \end{center}
     \end{wraptable}
     To make tables in \PTPTeX, the standard \LaTeX\ .......
\end{verbatim}\vskip.8em}


The original \verb+tabular+ environment is rewritten in \PTPTeX\ 
so as to make the font used in the table footnote size. 
If, however, you wish to use the usual text font in a table, please add 
the line %\verb+\let\tabularsize\normalsize+ 
{\vskip \asp
\begin{verbatim}
        \let\tabularsize\normalsize
\end{verbatim}\vskip.15em}
\noindent 
after the  \verb+\begin{wraptable}+ command to
obtain normal size
output. However,  care must be taken to make sure that the size change 
only acts {\itshape  locally}.

\section{Comments}


If you discover any bugs in {\ttfamily  ptptex.cls} when using 
\PTPTeX, we would appreciate it if you would inform us. Please send 
any such correspondence to the following E-mail address:
{\vskip \asp
\begin{center}
        {\ttfamily  ptp@yukawa.kyoto-u.ac.jp}
\end{center}\vskip\bsp}\noindent
It would help us if you could make the title of the E-mail something like
{\ttfamily  ptptex.cls bug}. 
 

\section*{Acknowledgements}
Acknowledgements in \PTPTeX\ can be written using the \LaTeX\ standard command
\verb+\section*{Acknowledgements}+.

Class files and manuals for JJAP and JPSJ were of great help to us in
creating the \PTPTeX\ class file and writing this manual. 
We acknowledge the staff of JJAP and JPSJ for this help.
At the end of our class file {\ttfamily  ptptex.cls}, 
we have included the free-ware
package files {\ttfamily  subeqn.sty} (created by Stephen Gildea) and
{\ttfamily  overcite.sty} (created by Donald Arseneau). Also,
the file {\ttfamily  wrapft.sty} is a modified version of the
{\ttfamily  wrapfig.sty} created by Donald Arseneau.
We express our sincere thanks to these authors.  

\appendix
\section{How to Make an Appendix}
Appendices can be made using the standard \LaTeX\ commands as follows:
{\vskip.4em\baselineskip 1.1em
\begin{verbatim}
    \appendix
    \section{How to Make an Appendix}
      ...
    \section{Second Appendix}
      ...
\end{verbatim}\vskip.4em}
\noindent
If you make two or more appendices, the appendix numbers automatically
become {\bfseries  A}, {\bfseries  B}, etc.  In an appendix, equations become
numbered as
\begin{equation}
S_q^z=\frac{1}{L}\sum_{j=1}^LS_j^ze^{iqj}.
\end{equation} 

\section{Notes on Preparing Figures}
When you make graphics files for figures to be used in \PTPTeX, 
please do so according to the following directions.
\begin{itemize}
\item Graphics files should be prepared in EPS (encapsulated
PostScript) format.
Files should be made using graphics software supporting the EPS format.
The types of software we recommend include Adobe Illustrator, xfig, 
tgif, Mathematica and Maple.
We do not accept files made with a ``printer driver."
\item Fonts used in figures should be common PostScript fonts.
Times-Roman, Helvetica, Courier and Symbol fonts can be used.
\item Lines (solid, dotted, etc.) in figures must be thick and clear.
Because the printer we use for publishing has very fine resolution, 
lines in original figures that are very pale or fine are reproduced 
faithfully (i.e.~very pale or fine), and for this reason, they may be
undetectable.
Specifically, be sure that line widths are 1pt or thicker.
\item When colored figures are used in a manuscript, they are printed
in monochrome (i.e.~black and white only). Meanwhile, 
colored figures in a pdf format of the paper in PTP Online are viewed
in full color. 
Note that papers can be published in color, but in this case there 
is an additional fee.
\item When preparing graphics files by scanning original figures, the
scan setting should be ``monochrome (2bit),'' with a resolution of 
1200dpi if the original size is being used.
If a scanned figure is being enlarged or reduced, the resolution used 
should be [magnification ratio] $\times$ 1200dpi.
If a figure is to be printed as half the size (in a single dimension) 
of the original, the scanning resolution $1/2 \times 1200 = 600$dpi 
can be used.
\end{itemize}

\section{ }
If you wish to make an appendix without a title, like this appendix, 
simply begin it with the command \verb+\section{}+, leaving the 
argument empty or blank. Even in this case, the 
appendix section counter is active, and equation numbers will reflect 
this, as seen in the following:
\begin{equation}
A=B.
\end{equation}
Making an appendix without a title is not recommended, except when 
there is only a single appendix.

\begin{thebibliography}{99}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some macros are available for the bibliography:
%   o for general use
%      \JL : general journals          \andvol : Vol (Year) Page
%   o for individual journal 
%    \AJ   : Astrophys. J.           \NC         : Nuovo Cim.
%    \ANN  : Ann. of Phys.           \NPA, \NPB  : Nucl. Phys. [A,B]
%    \CMP  : Commun. Math. Phys.     \PLA, \PLB  : Phys. Lett. [A,B]
%    \IJMP : Int. J. Mod. Phys.      \PRA - \PRE : Phys. Rev. [A-E]     
%    \JHEP : J. High Energy Phys.    \PRL        : Phys. Rev. Lett.
%    \JMP  : J. Math. Phys.          \PRP        : Phys. Rep.
%    \JP   : J. of Phys.             \PTP        : Prog. Theor. Phys.     
%    \JPSJ : J. Phys. Soc. Jpn.      \PTPS       : Prog. Theor. Phys. Suppl
% Usage:
%   \PR{D45,1990,345}          ==> Phys.~Rev.\ \textbf{D45} (1990), 345
%   \JL{Nature,418,2002,123}   ==> Nature \textbf{418} (2002), 123
%   \andvol{B123,1995,1020}    ==> \textbf{B123} (1995), 1020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibitem{rf:1} 
   Leslie Lamport, \textit{LaTeX: A Document Preparation System} 
   (Addison-Wesley, New York, 1986).
\bibitem{rf:2} 
   S.~Weinberg, Phys.\ Rev.\ Lett.\ \textbf{19} (1967), 1264.
\bibitem{rf:3} 
   M.~Kobayashi and T.~Maskawa, 
   \PTP{49,1973,652}.
\bibitem{rf:4} 
   D.~Gross and F.~Wilczek, \PRL{30,1973,1343}.\\
   H.~D.~Politzer, \PRL{30,1973,1346}.
 \bibitem{rf:5}
   Y.~Nambu, Phys.\ Rev.\ \textbf{117} (1960), 648; \PRL{4,1960,380}.\\ 
   G.~'t~Hooft, \NPB{33,1971,173}; \NPB{35,1971,167}.
 \bibitem{rf:6}
   E.~Witten, \JL{Adv.\ Theor.\ Math.\ Phys.,2,1998,253}, hep-th/9802150;
   hep-th/0112258; arXiv:0710.0631.
\bibitem{rf:7}
    M.~Harada, Y.~Kikukawa, T.~Kugo and H.~Nakano, \PTP{92,1994,1161} 
    [Errata; \textbf{95} (1996), 835]. 
\end{thebibliography}
\end{document}






