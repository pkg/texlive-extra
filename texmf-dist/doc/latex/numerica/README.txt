numerica: a package to numerically evaluate mathematical 
          expressions in their LaTeX form.

Andrew Parsloe (ajparsloe@gmail.com)

This work may be distributed and/or modified under the conditions
of the LaTeX Project Public License, either version 1.3c of this 
license or any later version; see
http://www.latex-project.org/lppl.txt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This is version 2.0.0 of the numerica package. The packages 
l3kernel, l3packages, amsmath and mathtools are required. The 
commands \nmcIterate, \nmcSolve, \nmcRecur and \nmcTabulate
available in version 1 of numerica by means of package options 
are now available as separate packages. See numerica.pdf for
details on how to use the package.

Manifest
%%%%%%%%
README.txt             this document

numerica.sty           LaTeX .sty file
numerica.pdf           documentation for use of numerica
numerica.tex           documentation source file
 
