2015/05/17 v2.11  Harald Harders (harald.harders@gmx.de)
- Make sure that \LaTeX\ also compiles if recent changes in scrbook are
  present.
- Remove \ifpdfoutput before loading pdfcprot.sty

2015/05/15 v2.10  Harald Harders (harald.harders@gmx.de)
- Adapt class to support hyperref and imakeidx
- Fix PDF string issue if hyperref is loaded
- Adapt class to current scrbook version
- Fix bug in title page which made footnotes to disappear

2012/04/07 v2.02  Harald Harders (harald.harders@gmx.de)
- Adapted to Springer Vieweg Verlag

2011/12/29 v2.01  Harald Harders (harald.harders@gmx.de)
- Only define \subtitle if not present already
- Fix later redefinition of \d for usage in math mode
- Added macro \emphindex for important index entryies

2008/05/04 v2.00  Harald Harders (harald.harders@gmx.de)
- Adapted to Vieweg+Teubner Verlag

2008/04/27 v1.40  Harald Harders (harald.harders@gmx.de)
- Adapted \signature for multiline texts

2008/04/15 v1.39  Harald Harders (harald.harders@gmx.de)
- Indentation after the preface title fixed

2006/04/20 v1.38  Harald Harders (harald.harders@gmx.de)
- Bug in BibTeX styles (format.crossref.editor) fixed

2005/10/31 v1.37  Harald Harders (h.harders@tu-bs.de)
- Typeset abbreviated forenames with small space between them.
- Add new BiBTeX style with abbreviated forenames and long keywords.

2005/10/28 v1.36  Harald Harders (h.harders@tu-bs.de)
- Fix bug using crossref with books in biliography.
- Fix sorting problem in the biliography.
- Introduce \btxlastname for formatting lastnames.

2005/04/16 v1.35  Harald Harders (h.harders@tu-bs.de)
- Allow to number \paragraph and \subparagraph.

2005/01/07 v1.34  Harald Harders (h.harders@tu-bs.de)
- Remove wasysym integrals before amsmath is loaded.

2004/11/19 v1.33  Harald Harders (h.harders@tu-bs.de)
- Avoid to use the calc package since it causes problems with many
  other packages.

2004/08/19 v1.32  Harald Harders (h.harders@tu-bs.de)
- Move \ifhhcls@times outside definition of \appendixmore.
- Remove \rmfamily from pagehead since \normalfont does that already.

2004/05/09 v1.31  Harald Harders (h.harders@tu-bs.de)
- Minor buxfix for titlepage.
- Do not compile cdcover if cd-cover.sty is not available.
- Fix documentation of \pdfoutput=0.

2004/04/09 v1.30  Harald Harders (h.harders@tu-bs.de)
- Set PDF information only if PDF is produced.
- Write info file containing the same information as the PDF-info entries.
- Ignore class option `epsfigures'.
- Enable usage of \includegraphics with DVI output.
- Print error message when using ordinary latex (Use pdflatex with
  \pdfoutput=0 for producing DVI).
- Redefine quotation marks to allow math mode and kerning.
- Use bold instead of bold extended with Times.
- Error rather than a warning for using not existing bold math.
- answer environment: Remove \begingroup and \endgroup because they
  removed the correct linespacing.
- Make font in exercises adjustable.
- Improve spacing after title of theorem-like environment, subexercises,
  and subanswers.
- Subanswers can have another linespacing than answers.
- Start prefaces on odd and even pages.
- Change from LPPL 1.2 to LPPL 1.3.

2004/03/25 v1.21  Harald Harders (h.harders@tu-bs.de)
- Better top skip for important and longimportant when starting with
  a theorem-like environment. Unfortunately, this does not work with
  equations.
- Use correct framed.sty style if available, otherwise use workaround.
  (At the moment, only an inoffical framed.sty version is available that
  does not have the bug.)

2004/03/07 v1.20  Harald Harders (h.harders@tu-bs.de)
- Enable use of the MathTime and MathTimePlus fonts if available (package
  option "mathtime").
- Bugfix: the mathcomp symbols (e.g., \tccelsius, \tcohm, \tcperthousand)
  now use the correct fonts.
- With Times, many textcomp symbols are not available. Define substitutions
  (sometimes with minor quality, then with a warning). This is done by the
  package ptmxcomp.sty which someday may be available seperately.
- Bugfix: Avoid page break after title in theorem-like environments, 
  exercises, and answers.
- Warnings when using \oldstylenums.
- The titlepage now only types out how many figures, tables, and exercises
  are used, if they really are used.
- Bugfix: The adjustment of many margins inside the important(*) and
  longimportant environments is fixed again.
- Bugfix: Workaround for bug in framed.sty which caused a wrong margin
  if only a theorem was inside longimportant.
- New environment longimportant* as important*.

2003/12/17 v1.14  Harald Harders (h.harders@tu-bs.de)
- Avoid usage of \bgteubnerfileversion and \bgteubnerfiledate.
- New command \GetFileInfo (from url.sty).
- New abbreviated Bib styles (bgteuabbr.bst and bgteuabbr2.bst).

2003/12/16 v1.13  Harald Harders (h.harders@tu-bs.de)
- Second Bib style with different name format (bgteupln2.bst).
- Use variable margins for theorem-like environments.
- Make delimiter for theorems configurable.
- Avoid negative margins of theorems grey boxes.
- Reduce margins of advanced and exercises in grey boxes.

2003/11/13 v1.12  Harald Harders (h.harders@tu-bs.de)
- Problem with float.sty solved.
- Lists for new floats get the same layout as other lists.
- Redeclare captions of user-defined floats when using float.sty.

2003/10/30 v1.10  Harald Harders (h.harders@tu-bs.de)
- Add proof environment.
- Smaller indentation for quote, quotation, example, etc.
- Spacing around important(*) and theorem-like environments improved.
- Limits for integrals below and above the sign in displayed equations.
- Command \qed for proofs introduced.
- Font size in theorem-like environments adjustable (global switch,
  default is normalsize, now).
- Line break in ISBN prohibited.
- Use the same format for ISSN and ISBN.
- Class option `layoutraster' removed.

2003/10/15 v1.07  Harald Harders (h.harders@tu-bs.de)
- Copyright changed to LPPL.
- Extended German LIESMICH file and new English README file.
- Distribution prepared for upload to CTAN (thus changed language in
  ChangeLog to English).
- hhsubfigure package included.

2003/10/12 v1.06  Harald Harders (h.harders@tu-bs.de)
- Paket hfoldsty statt hfo laden.
- Paket ginpenc statt hhinputenc laden.
- Befehl \tensor in \tens umbenannt.

2003/09/25 v1.05  Harald Harders (h.harders@tu-bs.de)
- Auch �ltere subfloat-Versionen erlaubt, die Unterschiede sind nur 
  marginal.

2003/09/23 v1.04  Harald Harders (h.harders@tu-bs.de)
- CD-H�lle in die Hauptdistribution aufgenommen.
- R�cksetzen der Schreibmaschinenschrift auf Original cmtt nicht mehr 
  n�tig.
	
2003/09/18 v1.03  Harald Harders (h.harders@tu-bs.de)
- ifthen-Paket explizit laden.
- Times hat keine kursiven Kapit�lchen, daher slantsc.sty dann nicht
  laden.

2003/09/17 v1.02  Harald Harders (h.harders@tu-bs.de)
- In allen englischen Dialekten \frenchspacing verwenden.
- \normalfont in Bildunterschriften und Fu�noten aufrufen.
	
2003/09/16 v1.01  Harald Harders (h.harders@tu-bs.de)
- Befehl \acro f�r European Computer Modern serifenlos sowie kursive 
  Schreibmaschinenschrift mit \textsmaller statt \textsc.
- R�mische Seitenzahlen (\frontmatter) f�r European Computer Modern in
  Kapit�lchen statt Versalien.

2003/09/16 v1.00  Harald Harders (h.harders@tu-bs.de)
- Erste offizielle Version, an den Teubner Verlag geliefert.
