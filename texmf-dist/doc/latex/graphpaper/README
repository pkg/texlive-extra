*********************************************************************
*                         GraphPaper class                          *
*********************************************************************

      This is the README file of the "GraphPaper" LaTeX class.



** License **********************************************************

      Copyright (c) 2020-2022 by Claudio Beccari and Francesco Biccari

      This work (specified below) may be distributed and/or
      modified under the conditions of the 
      LaTeX Project Public License,
      either version 1.3c of this license or any later version.
      The latest version of this license is in
        http://www.latex-project.org/lppl.txt
      and version 1.3c or later is part of all distributions of
      LaTeX version 2005/12/01 or later.

      This work has the LPPL maintenance status 'maintained'.
      The Current Maintainer of this work is Francesco Biccari.



** Class information ************************************************

      Graphpaper is a new LaTeX document class which allows to print
	  several types of graph papers: bilinear (millimeter paper),
	  semilogarithmic, bilogarithmic, polar, log-polar, Smith charts.

      The documentation is provided in the file graphpaper.pdf.
      
      The version number of this class is reported at the top of the
      class file graphpaper.cls and in the pdf manual graphpaper.pdf.



** Installation instruction *****************************************

      GraphPaper can be installed in 3 different ways:
	  
	  1. The simplest way is by the package manager
      of your TeX distribution (TeX Live, MiKTeX or MacTeX).
      
      2. Instead, if you want to install GraphPaper manually, you have to
      download the GraphPaper.zip archive from CTAN.
	  
      The archive contains: 

        1. graphpaper.dtx (main file)
        2. README (this file)

	  You can simply copy graphpaper.dtx into your local work directory
	  and compile it with command "pdflatex.exe graphpaper.dtx",
	  the documentation pdf file and the class file graphpaper.cls
	  will be generated.
	  
      3. The last possibility is to copy the GraphPaper files
	  (cls and pdf generated as discussed in the previous point and
	  README file) into your texmf tree (or your localtexmf tree) at
	  these locations:

      <texmf>/tex/latex/graphpaper/graphpaper.cls
      <texmf>/doc/latex/graphpaper/graphpaper.pdf
      <texmf>/doc/latex/graphpaper/README

      Then, you need to update the file database.
