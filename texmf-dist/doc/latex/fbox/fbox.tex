%% $Id: fbox.tex 449 2022-02-20 20:50:12Z herbert $
%
\listfiles\setcounter{errorcontextlines}{100}
\documentclass[paper=a4,fontsize=11pt,DIV=13,parskip=half-,
               captions=tableabove,twoside=on]{scrartcl}
\usepackage{fontspec}
\setmainfont{AccanthisADFStdNo3}[
  UprightFont   =*-Regular,
  BoldFont      =*-Bold,
  ItalicFont    =*-Italic,
  BoldItalicFont=*-BoldItalic,
  RawFeature    = -rlig,
]
\setsansfont{GilliusADF}[
  UprightFont   =*-Regular,
  BoldFont      =*-Bold,
  ItalicFont    =*-Italic,
  BoldItalicFont=*-BoldItalic,
  RawFeature    = -rlig,
]
\setmonofont{Anonymous Pro}[Scale=MatchLowercase,FakeStretch=0.9]

\usepackage[english]{babel}
\usepackage{scrlayer-scrpage}
\usepackage{showexpl}
\usepackage{listings}
\lstset{basicstyle=\ttfamily\small,language=[LaTeX]TeX,rframe=}

\def\Lfile#1{\texttt{#1}\index{#1 file@\texttt{#1} file}}
\def\Lext#1{\texttt{.#1}\index{#1 file extension@\texttt{.#1} file extension}}
\def\Lcs#1{\texttt{\textbackslash#1}\index{#1@\texttt{\textbackslash#1}}}
\def\Lenv#1{\texttt{#1}\index{#1 environment@\texttt{#1} environment}}
\def\Lpack#1{\texttt{#1}\index{#1 package@\texttt{#1} package}}
\def\Lprog#1{\texttt{#1}\index{#1 program@\texttt{#1} program}}
\def\Loption#1{\texttt{#1}\index{#1@\texttt{#1} package option}}
\let\Ldim\Lcs

\def\demoText{DuckDuckGo! Time to spread the word!\par
Equipped with the talking points above, you’re ready to help anyone search and browse protected.}

%\usepackage[bibstyle=dtk]{biblatex}
%\addbibresource{\jobname.bib}

\usepackage{url}
\usepackage[colorlinks,linktocpage]{hyperref}

\makeatletter% from: doc.sty
\newcommand*\GetFileInfo[1]{%
  \def\filename{#1}%
  \def\@tempb##1 ##2 ##3\relax##4\relax{%
    \def\filedate{##1}%
    \def\fileversion{##2}%
    \def\fileinfo{##3}}%
  \edef\@tempa{\csname ver@#1\endcsname}%
  \expandafter\@tempb\@tempa\relax? ? \relax\relax}
\makeatother

%\GetFileInfo{xltabular.sty}
\usepackage{xspace}
\usepackage{fbox}

\def\setVersion#1{\setVVersion#1!!}
\def\setVVersion#1=#2!!{\def\fboxVersion{#2}} 

\setVersion{version = 0.06}%

\title{Package \texttt{fbox} \\--\\ \normalsize \fboxVersion\ (\today)}
\author{Herbert Voß\thanks{\url{hvoss@tug.org}\newline Thanks to Rolf Niepraschk}}
\begin{document}
\maketitle
%\tableofcontents

This package redefines the \LaTeX-macro \Lcs{fbox} to allow an optional
argument:

\begin{verbatim}
\fbox*[<optargs>]{<contents>}
\fparbox*[<optargs>]{<contents>}
\end{verbatim}

The star version does not put any horizontal space before or behind the argument
if no optional argument \texttt{l} or \texttt{r} is given. The non star version always inserts
\Ldim{fboxsep}.

\Lcs{fparbox} uses the current \Ldim{linewidth} minus two times the box sep and boxrule width.
It does \emph{not} a test for an existing paragraph indenting. It has do be done by the
user, e.g.:

\begin{verbatim}
\noindent
\fparbox ...
\end{verbatim}

The optional argument can be 
\begin{description}
\item[boxrule] The rule width is predefined to the current value of \Ldim{fboxrule} (0.4pt)
\item[boxsep] The box separation is predefined to the current value of \Ldim{fboxsep} (3pt)
\item[lcolor] Color for the left line (black)
\item[tcolor] Color for the top line (black)
\item[rcolor] Color for the right line (black)
\item[bcolor] Color for the bottom line (black)
\item[<letters>] Any combination of the letters
l, r, b, and t, or altenatively L, R, B, and T for  l)eft, r)right, b)ottom, and t)op
of the frame parts. A missing or an empty argument is the same as the default
\Lcs{fbox} command  from standard \LaTeX.
\end{description}


\begin{LTXexample}
\fbox{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[rbt]{foo gar baz}
\fbox[rbt,boxrule=4pt,rcolor=red,
  tcolor=green]{foo gar baz}
\fbox[brt]{foo gar baz}
\fbox[bRT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox*[rbt]{foo gar baz}
\fbox[rbt,boxrule=4pt,rcolor=red,
  tcolor=green]{foo gar baz}
\fbox*[brt]{foo gar baz}
\fbox*[bRT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[lT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox*[lT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[bT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox*[bT]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[Br]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox*[Br]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox[bT,boxrule=5pt]{foo gar baz}
\end{LTXexample}

\begin{LTXexample}
\fbox*[bT,boxrule=5pt]{foo gar baz}
\end{LTXexample}



\begin{LTXexample}
\fbox[Br,boxrule=5pt,bcolor=red]{foo gar baz}
\end{LTXexample}


\begin{LTXexample}
\fbox*[boxrule=5pt,Br]{foo gar baz}
\end{LTXexample}


\begin{LTXexample}
\fbox*[boxsep=5mm,bT,tcolor=green,
  boxrule=2pt]{foo gar baz}
\end{LTXexample}



\begin{LTXexample}
\fparbox{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox[]{\demoText}
\end{LTXexample}


\begin{LTXexample}
\fparbox[rbt,bcolor=red,rcolor=blue,
   tcolor=red]{\demoText}
\fparbox[brt]{\demoText}
\fparbox[bRT]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[rbt]{\demoText}
\fparbox*[brt]{\demoText}
\fparbox*[bRT,rcolor=blue,
   tcolor=green]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox[lT]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[lT]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox[bT]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[bT]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox[Br]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[Br]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox[bT,boxrule=5pt]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[bT,boxrule=5pt,boxsep=6pt]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[bT,boxrule=3pt,boxsep=10pt]{\demoText}
\end{LTXexample}


\begin{LTXexample}
\fparbox[Br,boxrule=5pt,boxsep=10pt]{\demoText}
\end{LTXexample}

\begin{LTXexample}
\fparbox*[boxrule=5pt,Br,
   bcolor=red!40,rcolor=blue!40]{\demoText}
\end{LTXexample}







\end{document}