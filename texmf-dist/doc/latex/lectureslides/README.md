# The `lectureslides` Package
#### Version 1.0

Taihao Zhang (t@taihao.de)
1 March 2022

This package allows for easily combining and indexing individual PDFs into one large PDF.

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ 
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

____

Changes:

* 2022-03-01 / 1.0 
  * Improve page range option
  * Improve documentation
  * Implement `ngerman` for footnote text
* 2021-03-15 / 0.1
    Initial release
    
____

This work consists of the files
* README.md (this file)
* lectureslides.sty
* lectureslides-doc.tex
* lectureslides-doc.pdf (derived from lectureslides-doc.tex)
