# vhistory

This folder contains the files of the LaTeX packages "vhistory" and "sets".
User documentation can be found in the subdirectory doc.  An example for
vhistory can be found there, too.

All feedback is welcome!  Please use the address mentioned in the
documentation.

## Requirements
Requirements are listed in the user documentation.

## License
The material in this directory and all of its subdirectories is subject
to the LaTeX Project Public License.  See
http://www.ctan.org/tex-archive/help/Catalogue/licenses.lppl.html for
the details of that license.
