# Release 1.8.0
- Included Spanish translations supplied by Hector, Fernando and Roberto
- Included Portugese translations supplied by João Ricardo Lourenço

# 2013-08-05: Release 1.7.0.
New option omittable for people who don't want to have a table with all revisions.

# 2013-04-21: Bugfix release 1.6.1
No new features, but:
- Fixed bug that occurs when the options hideauthorcolumn and tablegrid are used together
- Corrected lot of typos in the English documentation
- Described the previously undocumented option hideauthorcolumn

# 2013-01-22:
- Added Croation translation
- New option tablegrid
- Introduced possibility to change column width
Thanks to the users for their input which led to these changes.

# 2010-07-17
- Added French translation.
- Listing of sets improved.
- Dutch translation added and English labels improved.
My thanks go to the friendly people who sent their patches and comments to me.

# 2009-03-01: Bugfix release
Bug solved that occured when the first two letters of an element of a set were identical.

# 2006-01-09
Implementation of \listset changed. It now works within an \edef, but the element \empty is no longer allowed in a set.

# 2005-08-13
Documentation: Reference to LPPL added.