#citeall --- A package to cite all entries of a bbl created with biblatex
Version 1.4 Ulrike Fischer 2017

## License
LaTeX Project Public License

## Contents

- Readme (this file)
- citeall.sty (the sty)
- citeall.tex, citeall.pdf (the docu)
- examples-citeall.bib (and example bib-file used by the documentation)

## Installation

Put the sty where it can be found.

If you want to compile the documentation:
put the bib-file where it can be found. 
