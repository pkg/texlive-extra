% This file is public domain.
%
% This example document demonstrates the testidx package
% with XeLaTeX and xindy using digraph glyphs.
% You can build this document using:
%
% xelatex sample-idx-xe-digraphs
% xindy -M sample-idx-xe-digraphs -M texindy -C utf8 -L english -t sample-idx-xe-digraphs.ilg sample-idx-xe-digraphs.idx
% xelatex sample-idx-xe-digraphs
%
% You may want to experiment with different languages.
%
% If you are using arara, the directives are:
%
% arara: xelatex
% arara: xindy: {language: english, codepage: utf8,
% arara: --> modules: [sample-idx-xe-digraphs, texindy]}
% arara: xelatex
\documentclass{article}

\usepackage{filecontents}
\usepackage{fontspec}

% Need a font that supports the digraph glyphs
% For example:
\setmainfont{Linux Libertine O}

\usepackage{makeidx}
\usepackage[diglyphs]{testidx}

% Create the .xdy file for this example:
\begin{filecontents*}{\jobname.xdy}
; list of allowed attributes

(define-attributes ((
  "tstidxencapi"
  "tstidxencapii"
  "tstidxencapiii"
)))

; define format to use for locations

(markup-locref :open "\tstidxencapi{"
 :close "}"
 :attr "tstidxencapi")

(markup-locref :open "\tstidxencapii{"
 :close "}"
 :attr "tstidxencapii")

(markup-locref :open "\tstidxencapiii{"
 :close "}"
 :attr "tstidxencapiii")

(markup-locref-list :sep ",")
(markup-range :sep "--")

; define a numbers group

(define-letter-group "Numbers"
   :prefixes ("0" "1" "2" "3" "4" "5" "6" "7" "8" "9")
   :before "A")

; define a "Maths" group for keys starting with ">"

(define-letter-group "Maths"
   :prefixes (">" )
   :before "Numbers")

; define a "Markers" group for keys starting with "<"

(define-letter-group "Markers"
   :prefixes ("<" )
   :before "Maths")

\end{filecontents*}

\makeindex

\begin{document}
\testidx

\printindex
\end{document}

