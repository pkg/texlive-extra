\documentclass{scrartcl}

\usepackage{url,ifxetex}

\ifxetex
\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Hoefler Text}
\fi

\def\UrlFont{\ttfamily \small}

\usepackage[mergecollisions]{versonotes}

% Adjust the layout of the notes to make them more compact.
\let\origversolayout\versolayout
\renewcommand\versolayout{\origversolayout \footnotesize \baselineskip=10pt }
% And a macro to wrap the versonote.
\def\N#1#2{#1\versonote{\emph{#1}: #2}}

\parindent=0pt
\parskip\bigskipamount

\begin{document}
\textbf{\Large Paradise Lost}

\emph{John Milton}

This excerpt comprises the first couple of stanzas of \emph{Paradise
  Lost}, with notes.  The text and notes are quoted here (with
permission) to illustrate one use of the versonotes package
(\url{http://www.ctan.org/pkg/versonotes}), and its response to
collisions of notes.  Also, because a day with \emph{Paradise Lost} in
it is a day improved.

The text is from
\url{https://www.dartmouth.edu/~milton/reading_room/pl/book_1/}.
The general editor of this edition is Thomas H. Luxon, and the notes are
Copyright Trustees of Dartmouth College, 1997-2014; see the colophon
at \url{https://www.dartmouth.edu/~milton/reading_room/contents/about_mrr.shtml}.

\pagebreak

\textbf{\Large Book 1}

\emph{The Argument}

\emph{\emph{This first Book proposes, first in brief, the whole
    Subject}, Mans
disobedience, and the loss thereupon of Paradise wherein he was
plac't: \emph{Then touches} the prime cause of his fall, the Serpent, or
rather Satan in the Serpent; who revolting from God, and drawing to
his side many Legions of Angels, was by the command of God driven out
of Heaven with all his Crew into the great Deep. \emph{Which action past
over, the Poem hasts into \N{the midst of things}{Milton
  announces that he intends to follow classical precedents by
  beginning his epic \emph{in medeas res}, in the middle of things,
  and only later coming back, by reported action, to the action ``past
  over'' here. The story of the rebel angels being ``driven out of
  Heaven\dots into the great Deep,'' for example, comes in book 6.}},
presenting Satan with his Angels now fallen into Hell, \emph{describ'd here}, not in the Center
\emph{(for Heaven and Earth may be suppos'd as yet not made, certainly not
yet accurst)} but in a place of utter darkness, fitliest call'd \emph{Chaos:}
Here \emph{Satan} with his Angels lying on the burning Lake, thunder-struck
and astonisht, after a certain space recovers, as from confusion,
calls up him who next in Order and Dignity lay by him; they confer of
thir miserable fall. \emph{Satan} awakens all his Legions, who lay till then
in the same manner confounded; They rise, thir Numbers, array of
Battel, thir chief Leaders nam'd, according to the Idols known
afterwards in \emph{Canaan} and the Countries adjoyning. To these \emph{Satan}
directs his Speech, comforts them with hope yet of regaining Heaven,
but tells them lastly of a new World and new kind of Creature to be
created, according to an ancient Prophesie or report in Heaven; \emph{for
that Angels were long before this visible Creation, was the opinion of
many ancient Fathers}. To find out the truth of this Prophesie, and
what to determin thereon he refers to a full Councel. What his
Associates thence attempt. \N{\emph{Pandemonium}}{Literally, ``all the
  demons.'' Milton coins the name for the assembly hall of devils
  whose erection is recounted at the end of book 1.} the Palace of
Satan rises, suddenly built out of the Deep: The infernal Peers there
sit in Councel.}


Of Mans First Disobedience, and the Fruit\\
Of that Forbidden Tree, whose mortal tast\\
Brought \N{Death into the World, and all our woe}{This locution echoes fairly closely Virgil's narrative voice in \emph{Aeneid} book 4, announcing that death and woe followed the ersatz nuptials of Aeneas and Dido:\begin{quotation}
To the same cave come Dido and the Trojan chief. Primal earth and
nuptial Juno give the sign; fires flashed in heaven, the witness to
their bridal, and on the mountain-top screamed the Nymphs. That day
was the first day of death, that the first cause of
woe. (Trans. H. Rushton Fairclough in \emph{Virgil} vol. 1 [Cambridge, MA:
  Harvard University Press, 1935] 407)
  \end{quotation}
See also the Perseus Project edition of this passage.},\\
With loss of \emph{Eden}, till \N{one greater Man}{The messiah.}\\
Restore us, and regain the blissful Seat, \\
Sing \N{Heav'nly Muse}{Is the ``Heavenly Muse'' invoked here the same as the ``Urania,'' traditionally the muse of astronomy, invoked at book 7.1? More likely, contemporary readers would have first thought of the ``Holy Spirit,'' as the inspiration of Moses.}, that on the secret top\\
Of \N{\emph{Oreb}}{Moses, ``That Shepherd,'' received the Law on Mt.\ Horeb (Deuteronomy 4: 10) or its spur, Mt.\ Sinai (Exodus 19: 20).}, or of \emph{Sinai}, didst inspire\\
That Shepherd, who first taught the \N{chosen Seed}{The people of Israel. See Exodus 19-20.},\\
\N{In the Beginning}{The opening words of both Genesis (Geneva) and the Gospel of John (Geneva).} how the Heav'ns and Earth\\
Rose \N{out of Chaos}{One of Milton's several heterodox positions. Orthodoxy held that God created everything ex nihilo, out of nothing (the ``void'' of Genesis 1:2; See Calvin's Commentary on Genesis). Milton borrows the concept of chaos, or unformed matter, from Hesiod and Platonic philosophy (especially the Timaeus 53b). Milton was also a monist, holding that all things were created out of God; see book 5.468–490.}: Or if \N{\emph{Sion}}{To the haunts of the classical muses near the Castalian spring on Mt.\ Parnassus, Milton prefers to claim Mt.\ Sion and its brooks Kidron and Siloa, a kind of biblically authorized Parnassus.} Hill \\
Delight thee more, and \N{\emph{Siloa's Brook}}{``The Pool of Siloam (Hebrew:
% בריכת השילוח‎,
Breikhat Hashiloah) is a rock-cut pool on the southern slope of the City of David, the original site of Jerusalem, located outside the walls of the Old City to the southeast. The pool was fed by the waters of the Gihon Spring, carried there by two aqueducts'' (Wikipedia, ``Pool of Siloam.'')} that flow'd\\
Fast by the Oracle of God; I thence\\
Invoke thy aid to my \N{adventrous Song}{Note the similarities between Milton's opening and the opening lines of Virgil's Aeneid and of Homer's Odyssey. Milton wants not only to compare his project to the ancient epics, but also himself to those poets and his main character, Adam, to their celebrated heroes. All of these comparisons raise interesting and complicated questions of authority, heroism, and nationalism in art.},\\
That with no middle flight intends to soar\\
Above th' \N{Aonian Mount}{Mt.\ Helicon, in Aonia, sacred to the classical muses.}, while it pursues \\
Things unattempted yet \N{in Prose or Rhime}{The line ironically (maybe even sarcastically?) recalls the stanza 2 of canto 1 of Ariosto's Orlando Furioso.}.\\
And chiefly Thou O Spirit, that dost prefer\\
Before all Temples th' upright heart and pure,\\
Instruct me, for Thou know'st; Thou from the first\\
Wast present, and with mighty wings outspread \\
\N{Dove-like}{The Holy Spirit appears as a dove in John 1: 32. See also Paradise Regain'd 1.30-1.} satst \N{brooding on the vast Abyss}{Milton's ``brooding'' is a better translation of the Hebrew than the familiar ``moved upon the face of the waters'' of the Authorized version of Genesis 1:2.}\\
And mad'st it \N{pregnant}{Milton invites us to imagine the Holy Spirit copulating with the unformed matter of Chaos (``the vast Abyss''). In Milton's monism, distinctions between spirit and matter are not absolute.}: What in me is dark\\
Illumin, what is low raise and support;\\
That to the highth of this great Argument\\
I may assert Eternal Providence, \\
And justifie the wayes of God to men.

\N{Say first}{Compare this with Homer's invocation to the muse in the Iliad 1.8.}, for Heav'n hides nothing from thy view\\
Nor the deep Tract of Hell, say first what cause\\
Mov'd our Grand Parents in that happy State,\\
Favour'd of Heav'n so highly, to fall off \\
From thir Creator, and transgress his Will\\
For \N{one restraint}{That is, the single injunction against eating from the tree of the knowledge of good and evil (Genesis 2: 17).}, \N{Lords of the World}{According to Genesis 1:28, human beings were created to ``have dominion'' over the rest of creation.} besides?\\
Who first seduc'd them to that foul revolt?\\
Th' infernal Serpent; he it was, whose guile\\
Stird up with Envy and Revenge, deceiv'd \\
The Mother of Mankind, what time his Pride\\
Had cast him out from Heav'n, with all his Host\\
Of Rebel Angels, by whose aid aspiring\\
To set himself in Glory above his Peers,\\
He trusted to have equal'd the most High, \\
If he oppos'd; and with ambitious aim\\
Against the Throne and Monarchy of God\\
Rais'd impious War in Heav'n and Battel proud\\
With vain attempt. Him the Almighty Power\\
\N{Hurld headlong flaming}{This description recalls Pieter Bruegel's Fall of the Rebel Angels (about 1562). See also William Blake's 1808 watercolor illustration of the rebel angels' fall (told by Raphael at 6.864-66).} from th' Ethereal Skie \\
With hideous ruine and combustion down\\
To bottomless perdition, there to dwell\\
In \N{Adamantine}{Unbreakable, rocklike.} Chains and penal Fire,\\
Who durst defie th' Omnipotent to Arms.\\
\N{Nine times the Space}{In Hesiod's Theogony 664-735, the Titans take a similar fall at the hands of Zeus. Interestingly, though Milton alludes to the fall of the Titans here, he likens their nine-day fall, not to the fall of the rebel angels, but to the time they spent lying vanquished on the fiery gulf after their fall. Raphael, in book 6, line 871, however, tells Adam that the rebel angels fell for ``Nine dayes''.} that measures Day and Night \\
To mortal men, he with his horrid crew\\
Lay vanquisht, rowling in the fiery Gulfe\\
Confounded though immortal: But his doom\\
Reserv'd him to more wrath; for now the thought\\
Both of lost happiness and lasting pain \\
Torments him; round he throws his baleful eyes\\
That witness'd huge affliction and dismay\\
Mixt with obdurate pride and stedfast hate:\\
At once as far as Angels \N{kenn}{Range, which in the case of angels must be presumed to be nearly limitless.} he views\\
The dismal Situation waste and wilde, \\
A Dungeon horrible, on all sides round\\
As one great Furnace flam'd, yet from those flames\\
No light, but rather darkness visible\\
Serv'd onely to discover sights of woe,\\
Regions of sorrow, doleful shades, where peace \\
And rest can never dwell, \N{hope never comes}{A deliberate echo of Dante's Inferno 3.9: ``All hope abandon ye who enter here.''}\\
That comes to all; but torture without end\\
Still urges, and a fiery Deluge, fed\\
With ever-burning Sulphur unconsum'd:\\
Such place Eternal Justice had prepar'd \\
For those rebellious, here \N{thir}{Their. Milton's preferred spelling was ``thir,'' and Flannagan reports that ``their'' was changed to ``thir'' in later stages of the 1674 edition. The same is true of line 499 and other lines.} Prison ordain'd\\
In utter darkness, and thir portion set\\
As far remov'd from God and light of Heav'n\\
As \N{from the Center thrice to th' utmost Pole}{Milton asks us to refer to the Ptolemaic model of the universe with the earth at the center of nine concentric spheres. On Milton's cosmology, Ptolemaic or Copernican, see also book 8. 119-68.}.\\
O how unlike the place from whence they fell! \\
There the companions of his fall, o'rewhelm'd\\
With Floods and Whirlwinds of tempestuous fire,\\
He soon discerns, and weltring by his side\\
One next himself in power, and next in crime,\\
Long after known in Palestine, and nam'd \\
\N{Beelzebub}{``God of the flies'' or ``Chief of the devils.'' See Matthew 10: 25, Mark 3: 22, and Luke 11: 15. See also Christopher Marlowe's Doctor Faustus and Burton's Anatomy of Melancholy 1.2.1-2; see also the Wikipedia entry.}. To whom th' Arch-Enemy,\\
And thence in Heav'n \N{call'd Satan}{Originally Lucifer, ``bringer of light,'' his name in heaven is changed to Satan, ``enemy.''}, with bold words\\
Breaking the horrid silence thus began.

\end{document}
