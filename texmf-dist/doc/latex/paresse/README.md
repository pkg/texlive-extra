<!-- Time-stamp: <2021-05-16 13:41:20 Yvon Henel (TdS)> -->
<!-- Création : 2020-10-06T14:12:21+0200 -->

# paresse

Author: Yvon Henel, aka Le TeXnicien de surface
([contact](le.texnicien.de.surface@yvon-henel.fr))

## LICENCE

This material is subject to the LaTeX Project Public License. 
See http://www.ctan.org/license/lppl1.3c 
for the details of that license.

© 2021 Yvon Henel (Le TeXnicien de surface)

## Description of the Package

This package provides a means of typing “easily” greek letters
with the least possible effort ;) It takes care only of the
letters which have a macro name as `\alpha` or `\Omega`.


## History
* Version 5.0.2 — 2021-05-16
    * correction of a typo in documentation.

* Version 5.0.1 — 2020-10-10
    * bug correction: the keys ttau and ttheta had been left undefined; 
    * tiny changes in documentation.

* Version 5.0 — 2020-10-06
    * doesn't use package <kbd>skeyval</kbd> anymore;
    * use <kbd>l3keys</kbd> for managing keys;
    * two *sub-packages* <kbd>paresse-old</kbd> and <kbd>paresse-utf8</kbd>; 
    * partial usage of <kbd>expl3</kbd>.

* Version 4.1 — 2013-02-16


