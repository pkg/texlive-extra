<!-- Time-stamp: <2021-05-16 13:41:32 Yvon Henel (TdS)> -->
<!-- Création : 2020-10-06T14:12:21+0200 -->

# paresse

Auteur: Yvon Henel, alias _Le TeXnicien de surface_
([contact](le.texnicien.de.surface@yvon-henel.fr))

## LICENCE

Le contenu de cette archive est placé sous la « LaTeX Project Public License ».
Voir http://www.ctan.org/license/lppl1.3c 
pour la licence.

© 2021 Yvon Henel (Le TeXnicien de surface)

## Description de l’extension

Cette extension est faite pour taper « facilement » les lettres
grecques avec un minimum d'effort ;) Elle ne concerne que les
lettres grecques qui ont un nom de macro comme `\alpha` ou `\Omega`.

## Historique

* Version 5.0.2 — 2021-05-16
    * correction d’une erreur dans la documentation.

* Version 5.0.1 — 2020-10-10
    * correction de bogue: les clés ttau et ttheta n’étaient plus
      définies ; 
    * changements mineurs dans la documentation.

* Version 5.0 — 2020-10-06 
    * abandon de l'extension <kbd>skeyval</kbd> ;
    * gestion des clés avec <kbd>l3keys</kbd> ;
    * deux *sous-extensions* <kbd>paresse-old</kbd> and <kbd>paresse-utf8</kbd>; 
    * passage partiel à <kbd>expl3</kbd>.

* Version 4.1 — 2013-02-16
