# Manifest for paresse

This file is a listing of all files considered to be part of this package.
It is automatically generated with `texlua build.lua manifest`.


## Repository manifest

The following groups list the files included in the development repository of the package.
Files listed with a ‘†’ marker are included in the TDS but not CTAN files, and files listed
with ‘‡’ are included in both.

### Source files

These are source files for a number of purposes, including the `unpack` process which
generates the installation files of the package. Additional files included here will also
be installed for processing such as testing.

* paresse.dtx ‡
* paresse.ins ‡

### Typeset documentation source files

These files are typeset using LaTeX to produce the PDF documentation for the package.

* paresse.dtx ‡

### Documentation files

These files form part of the documentation but are not typeset. Generally they will be
additional input files for the typeset documentation files listed above.

* LISEZMOI.md ‡
* README.md ‡

### Text files

Plain text files included as documentation or metadata.

* LISEZMOI.md ‡
* MANIFEST.md ‡
* README.md ‡

### Derived files

The files created by ‘unpacking’ the package sources. This typically includes
`.sty` and `.cls` files created from DocStrip `.dtx` files.

* paresse-old.sty †
* paresse-utf8.sty †
* paresse.sty †

### Typeset documents

The output files (PDF, essentially) from typesetting the various source, demo,
etc., package files.

* paresse.pdf ‡


## TDS manifest

The following groups list the files included in the TeX Directory Structure used to install
the package into a TeX distribution.

### Source files (TDS)

All files included in the `paresse/source` directory.

* paresse.dtx 
* paresse.ins 

### TeX files (TDS)

All files included in the `paresse/tex` directory.

* paresse-old.sty 
* paresse-utf8.sty 
* paresse.sty 

### Doc files (TDS)

All files included in the `paresse/doc` directory.

* LISEZMOI.md 
* MANIFEST.md 
* README.md 
* paresse-eng.pdf 
* paresse-fra.pdf 
* paresse.pdf 


## CTAN manifest

The following group lists the files included in the CTAN package.

### CTAN files

* LISEZMOI.md 
* MANIFEST.md 
* README.md 
* paresse-eng.pdf 
* paresse-fra.pdf 
* paresse.dtx 
* paresse.ins 
* paresse.pdf 
