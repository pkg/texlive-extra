membranecomputing - a LaTeX package for the Membrane Computing
community. It comprises the definition of P systems, rules and
some concepts related to languages and computational
complexity usually needed for Membrane Computing research.

Written by David Orellana Martín <dorellana@us.es>

No dependencies other than the standards ifthen and xstring packages.
See membranecomputing.pdf for full documentation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright and license
% =====================
%
% Copyright (C) 2020-2022 David Orellana Martín
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%