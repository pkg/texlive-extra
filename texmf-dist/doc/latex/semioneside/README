
This is version 0.41 of the semioneside package, providing
support for one sided layout with figures etc. on left-hand
pages. Please refer to semioneside.pdf to learn more about
this package.

To install the package and to build documentation
follow this steps:

1. Run 'tex semioneside.ins' to extract the style file
   semioneside.sty, a MetaPost source file figure.mp and an
   example LaTeX file example.tex.
2. Run 'mpost -tex=latex figure' to build the graphic.
3. Run 'latex semioneside.dtx' (or pdflatex).
4. Run 'makeindex -s gind.ist -o semioneside.ind semioneside.idx'.
   to build the documentation index.
5. Run 'makeindex -s gglo.ist -o semioneside.gls semioneside.glo'
6. Again run latex or pdflatex on semioneside.dtx to build the
   final documentation.

Now move semioneside.sty, semioneside.dvi|pdf and example.tex
somewhere into your tex path, e.g., texmf/tex/latex/semioneside/
and texmf/doc/latex/semioneside/ and don't forget to refresh the
file name data base.

Stephan Hennig
stephanhennig@arcor.de
