ecobiblatex - A set of biblatex Global Ecology and Biogeography Journal BibLaTex styles
==========================================================================================

The `ecobiblatex` package is a set of styles for creating 
bibliographies using `biblatex` in the style of the Global Ecology and Biogeography journal.  The bundle comprises the style based on the
 conventions of the John Wiley & Sons Ltd
and Global Ecology and Biogeography Conventions. It therefore covers the journal
styles, included in-text citations and complete formatting of Bibliography and Reference List.
