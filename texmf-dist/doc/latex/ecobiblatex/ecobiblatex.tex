%% ---------------------------------------------------------------
%% ecobiblatex --- Global Ecology and Biogeography Journal
%%                 BibLaTex styles
%% Maintained by Norbert Balak
%% E-mail: norbert.balak@outlook.com
%% Released under the LaTeX Project Public License v1.3c or later
%% See http://www.latex-project.org/lppl.txt
%% ---------------------------------------------------------------
%%  

\documentclass[a4paper]{ltxdoc}
\usepackage{csquotes,lmodern}
\usepackage[T1]{fontenc}
\usepackage[final]{microtype}
\usepackage{hyperref}

\hypersetup{hidelinks}

\author{Norbert Balak\thanks{E-mail: 
  \href{mailto:norbert.balak@outlook.com}
  {\texttt{norbert.balak@outlook.com}}}}
\title{\pkg{ecobiblatex} -- A set of \pkg{biblatex} Global Ecology and Biogeography Journal BibLaTex styles%
  \footnote{This file describes v1.0, last revised 2015/12/28.}}
\date{Released 2018/12/28}

\providecommand*{\opt}[1]{\texttt{#1}}
\providecommand*{\pkg}[1]{\textsf{#1}}

\let\DescribeOption\DescribeEnv

\RecordChanges

\begin{document}

\maketitle

\begin{abstract}
  The \pkg{ecobiblatex} bundle is a set of styles for creating bibliographies
  using \pkg{biblatex} in the style of the Global Ecology and Biogeography journal. The
  package comprises styles based on the conventions of John Wiley \& Sons Ltd and \emph{Global Ecology and Biogeography Conventions} \copyright. It, therefore, covers the journal styles of, for example:
  \begin{itemize}
    \item \emph{Global Ecology and Biogeography (Standardised Harvard-style referencing)}
    \end{itemize}
  \end{abstract}

\section{Introduction}

The \pkg{biblatex} package introduces a completely new method for controlling
the creation of bibliographies using \BibTeX{}. This makes a great deal of
flexibility available when creating bibliographies, most of which is much more
difficult with traditional \BibTeX{} styles.

In order to use \pkg{biblatex}, an entirely new set of appropriate supporting
styles are needed. This package provides the styles needed to include references according to the Global Ecology and Biogeography Journal requirements which is a standardised Harvard-style referencing format,
following the rules of one of the most important journals in the field.

In order to benefit from the advantages of BibLaTex and this style package, it is highly recommended to use the Biber backend.\\
E.g.:
\begin{verbatim}  \usepackage{biblatex}[backend=biber, style=ecobiblatex]  \end{verbatim}

\section{The style}

The package currently contains four \pkg{biblatex} style files:
\begin{itemize}
   \item The {\pkg{ecobiblatex}} style,
     which covers the Global Ecology and Biogeography journal.
   \end{itemize}

The style can be used to follow the current layout rules of the Global Ecology and Biogeography journal
published by Wiley which is most often the preferred referencing style of many universities at all levels.

The styles use the standard \pkg{biblatex} database requirements. This means
that a database designed for traditional \pkg{biblatex} use may need some
editing for optimal output. The accompanying example database
\texttt{ecobiblatex.bib} shows examples of all of the supported entry types
with common fields filled in.

\section{Style options}

All of the styles here add a small number of package options to the standard
set provided by \pkg{biblatex}. This allows the styles to cover the variations
seen between different journals without needing a very large number of files:
the American Chemical Society in particular varies the exact details between
journals.

\DescribeOption{doi}
\DescribeOption{eprint}
\DescribeOption{isbn}
\DescribeOption{url}
The standard style options \opt{doi}, \opt{eprint} \opt{isbn} and
\opt{eprint}, as described in the \pkg{biblatex} manual. However, these
options are turned off as standard by the styles in the \pkg{ecobiblatex}
bundle. This reflects the fact that these entries may be present in reference
databases but are not generally included in published bibliographies. Note
that \textsc{doi} values are printed for journal articles with no pages
given, even if the \opt{doi} option is \opt{false}

\DescribeOption{subentry}
In common with the standard \pkg{biblatex} numeric styles, all of the styles
in the bundle support the boolean \texttt{subentry} option. With this set
\opt{true}, entries of type \texttt{set} are given individual labels within
the bibliography.

\DescribeOption{articletitle}
The use of article titles varies between individual journals. The
boolean option \opt{articletitle} is available and controls this behaviour.

\section{Use of the \pkg{ecobiblatex} package}
EcoBibLATEXtest file:\\ \newline
This is a book as \verb|\parencite{}| [2], and as \verb|\textcite{}| Elton [4]. This is a citation from a book chapter [3].
This is a paper as parencite [8], and as textcite Anderson et al. [1].
This is a citation command with two papers by the same author [11, 12]. This is a citation command with more than two papers by the same author, on the same year $[5-7]$.
This is a citation of a paper with only two authors in parencite [9], and another one in textcite Yang and Rannala [13].
This is a reference to R [10]. This is a reference to non-consecutive entries $[3$,$ 5$-$7$,$ 13]$.\\ \newline
\textbf{References}\newline
[1] M. J. Anderson et al. $"$Navigating the multiple meanings of beta diversity: a roadmap for the
practicing ecologist.$"$ In: \emph{Ecology Letters} 14.1 (2011), pp. 19$-$28.\\ \newline
[2] C. Darwin. \emph{On the origin of species by means of natural selection, or the preservation of favoured
races in the struggle for life.} New York: D. Appleton, 1859.\\ \newline
[3] J. A. Dunne. "The network structure of food webs". In: \emph{Ecological Networks: Linking Structure to
Dynamics in Food Webs.} Ed. by M. Pascual and J. A. Dunne. Oxford: Oxford University Press, 2006,
pp. 27$-$86.\\ \newline
[4] S. A. Frank. "Coevolutionary genetics of plants and pathogens". In: \emph{Evolutionary Ecology} 7.1
(1993), pp. 45$-$75.\\ \newline

\noindent\textbf{Example (Author-year style)}\\
\begin{verbatim}
\usepackage[backend=biber,style=ecobiblatex]{biblatex}
\addbibresource{SampleLibrary.bib} 
\renewcommand*{\nameyeardelim}{\addcomma\space}
...
\printbibliography[title=References]
\end{verbatim}
\section{New styles}

The current set of styles here is intended to form a strong base for ecologists, biologists, university and PhD students and biochemists.
However, there will be the need for other styles to be created. The package
author welcomes suggestions for other styles for inclusion. It would also be
good to keep all ecology- and biology-related \pkg{biblatex} styles in one bundle. Others
working on ecology styles for \pkg{biblatex} are welcome to send them to the
bundle maintainer so they can be incorporated here.

\section{Errors and omissions}

Suggestions for improvement and bug reports can be logged by sending an e-mail to 
\href{mailto:norbert.balak@outlook.com}
  {\texttt{norbert.balak@outlook.com}}.\\
  
\subsubsection*{Version history}
\emph{{v1.0}{2015/12/28} {First stable release}
  of \texttt{ecobiblatex} package.}
  


\end{document}

%% 
%% Copyright (C) 2010-2014 by
%%   Norbert Balak <norbert.balak@outlook.com>
%% 
%% It may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License (LPPL), either version 1.3c of
%% this license or (at your option) any later version.  The latest
%% version of this license is in the file:
%% 
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work is "maintained" (as per LPPL maintenance status) by
%%   Joseph Wright.
%% 
%% This work consists of the files ecobiblatex.bib,
%%                                 ecobiblatex.tex,
%%                                 ecobiblatex.bbx,
%%                                 ecobiblatex.cbx,
%%           and the derived file ecobiblatex.pdf
%% 
%%
%% End of file `biblatex-chem.tex'.