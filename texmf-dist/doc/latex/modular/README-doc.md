The main documentation file is `modular.tex`.
Build it to produce the package documentaiton.

The documentation refers to an example project whose sources are in `example/`.
Build `example/octopus/article.tex` to see a short article about octopuses.
Build `example/article_failed.tex` for a failed attempt at re-using the octopus article's content in an article about sea animals.
Build `example/article.tex` for a successful attempt using `modular`.

