************************************************************
*                                                          *
*                 The exsol package                        *
*                                                          *
************************************************************
                                                Walter Daems 
                               walter.daems(at)uantwerpen.be

The package \exsol{} provides macros to allow
embedding exercises and solutions in the LaTeX source of an
instructional text (e.g., a book or a course text) while generating
the following separate documents:
  - your original text that only contains the exercises, and
  - a solution book that only contains the solutions to the
    exercises (a package option exists to also copy the exercises
    themselves to the solution book).

The former is generated when running LaTeX on your document. This
run writes the solutions to a secondary file that can be included
into a simple document harness, such that when running LaTeX on
the latter, you can generate a nice solution book.

The code of the exsol package was taken almost literally from
fancyvrb [http://www.ctan.org/pkg/fancyvrb]. 
Therefore, all credits go to the authors/maintainers of fancyvrb.

If you think	
  - there's an error in this package,
  - there's a feature missing in this package,
please, don't hesitate to contact the author through e-mail
(walter.daems@uantwerpen.be).

Do you like this class file? You're welcome to send beer, wine, or
just kind words.

License
=======

 see file LICENSE

History
=======

 see documentation (exsol.pdf)