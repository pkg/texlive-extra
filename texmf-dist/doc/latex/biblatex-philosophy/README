-----------------------------------------------------------------------
The 'biblatex-philosophy' styles for biblatex
Author: Ivan Valbusa
        ivan dot valbusa at gmail dot com

This work has the LPPL maintenance status "author-maintained".
This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.
-----------------------------------------------------------------------

DESCRIPTION:

This package provides three bibliography and citation styles for use with
Philipp Lehmanıs biblatex package.
The style philosophy-verbose is intended for citations given in footnotes;
the styles philosophy-classic and philosophy-modern are based on the model
of BibLaTeX authoryear-comp style.
New options and features allow you to manage the information about the
translation of foreign texts or their reprints.

This work consists of the following files:

  README (this file)
  biblatex-philosophy.dtx
  biblatex-philosophy.pdf
  examples.zip

and of the derived:

    philosophy-standard.bbx
    philosophy-classic.bbx
    philosophy-modern.bbx
    philosophy-verbose.bbx

    philosophy-classic.cbx
    philosophy-modern.cbx
    philosophy-verbose.cbx

    english-philosophy.lbx
    italian-philosophy.lbx
    spanish-philosophy.lbx

    biblatex-philosophy.bib

INSTALLATION:

To use the styles place:
1) the .bbx files in: <HOME>/texmf/tex/latex/biblatex/bbx/
2) the .cbx files in: <HOME>/texmf/tex/latex/biblatex/cbx/
3) the .lbx files in: <HOME>/texmf/tex/latex/biblatex/lbx/
Alternatively, place the .bbx, .cbx and .lbx files in your working directory
or in:
<TEXMFLOCAL>/tex/latex/biblatex-contrib/biblatex-philosophy.
In this case remember to refresh the file name database.

USAGE:
\usepackage[italian=guillemets]{csquotes}
\usepackage[style=philosophy-modern]{biblatex}

DOCUMENTATION:

To reproduce the complete documentation, follow these steps:

$ latex biblatex-philosophy.dtx
$ biber biblatex-philosophy
makeindex -s gind.ist -o biblatex-philosophy.ind biblatex-philosophy.idx
makeindex -s gglo.ist -o biblatex-philosophy.gls biblatex-philosophy.glo
$ pdflatex biblatex-philosophy.dtx

from a shell (or in the preferred method for you system),
where `$' stands for the shell's prompt.

2012/04/21
Ivan Valbusa
