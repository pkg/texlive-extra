# The pfdicons package

Copyright 2021 Aaron Drews.

Contact: <adrews@ucsd.edu>

Current version: 1.0a, 2021/07/26


## Abstract

The `pfdicons` package provides TikZ shapes to represent commonly encountered unit operations for depiction in process flow diagrams (PFDs) and, to a lesser extent, process and instrumentation diagrams (PIDs). The package was designed with undergraduate chemical engineering students and faculty in mind, and the number of units provided should cover--in Turton's estimate--about 90 percent of all fluid processing operations.


## Licensing 

The `pfdicons` package is covered by the LaTeX Project Public License (LPPL), version 1.3c or later. The latest version can be found at <http://www.latex-project.org/lppl.txt>.


## Documentation

The documentation of the package can be found in `pfdicons-doc.pdf`, provided together with its source code `pfdicons-doc.tex`. A log of changes to the package is provided in `pfdicons-changelog.pdf`, which is provided together with its source code `pfdicons-changelog.tex`.