Biochemistry-colors is copyright 2017 by Dr Engelbert Buxbaum
<engelbert_buxbaum@web.de>

This file may be distributed and/or modified

1. under the LaTeX Project Public License and/or
2. under the GNU Public License.

Biochemistry-colors.sty defines the standard colors of biochemistry
colors for use with D.P. Carlisle's color-package and U. Kern's
xcolor (xcolor is loaded by Biochemistry-colors). They may be used
by authors (with \usepackage) or by package writers (with
\RequirePackage, e.g. to create Beamer color themes).

Colors include:
  - Shapely-colors for amino acids and nucleotides
  - CPK-Colors (Corey, Pauling and Koltun) of elements 		
  - Jmol-colors of elements, important isotopes and structures	
  - Glycopedia colors for sugars		

Biochemistry-colors.pdf provides a list of these colors with
components in rgb (hex, word, decimal) and hsl-format. The source
for this file is the spreadsheat Biochemistry-colors.xls

These files are provided in the hope that they may be useful, but
without any warranties.
