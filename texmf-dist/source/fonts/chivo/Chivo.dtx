% \iffalse meta-comment
%
% File: Chivo.dtx
% Copyright (C) 2016-2022 Arash Esbati <arash'at'gnu.org>
%
% This work may be distributed and/or modified under the conditions
% of the LaTeX Project Public License (LPPL), either version 1.3c of
% this license or (at your option) any later version.  The latest
% version of this license is in the file:
%
%    http://www.latex-project.org/lppl.txt
%
% \fi
%
% \iffalse
%<package>\NeedsTeXFormat{LaTeX2e}[2021/06/01]
%<package>\ProvidesPackage{Chivo}
%<package>  [2022/11/15 v2.2 Support for Chivo fonts (AE)]
%
%<*driver>
\documentclass[a4paper]{ltxdoc}
\usepackage[familydefault]{Chivo}
\usepackage[utf8]{inputenc}
\usepackage[OT1,LY1,T1]{fontenc}
\usepackage[scaled=1.09]{zlmtt}
\usepackage{fonttable,lipsum,array,booktabs}
\usepackage{xcolor}
\usepackage{caption}
\captionsetup[table]{%
  format        = plain         ,
  labelsep      = period        ,
  justification = centerlast    ,
  width         = 0.8\linewidth ,
  position      = top           ,
  skip          = 5pt           ,
  font          = small         ,
  labelfont     = bf
}
\usepackage[activate]{microtype}
\linespread{1.04}
\usepackage{hypdoc}
\newcommand*\pkg[1]{\textsf{#1}}
\DisableCrossrefs
\RecordChanges
\setlength\hfuzz{15pt}
\hbadness=7000
\frenchspacing
\raggedbottom
\makeatletter
\renewcommand\section{\@startsection{section}{1}{\z@}%
  {-3.5ex \@plus -1ex \@minus -.2ex}%
  {2.3ex \@plus.2ex}%
  {\normalfont\sffamily\Large\bfseries}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\sffamily\large\bfseries}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
  {3.25ex \@plus1ex \@minus.2ex}%
  {-1em}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
  {3.25ex \@plus1ex \@minus .2ex}%
  {-1em}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\@makefntext[1]{%
  \@setpar{%
    \@@par \@tempdima=\hsize
    \advance\@tempdima by -1.5em\relax
    \parshape \@ne 1.5em \@tempdima
  }%
  \par \parindent=\z@ \noindent %
  \hb@xt@ \z@{\hss \hb@xt@ 1.5em{\@thefnmark.\hss}}%
  #1%
}
\def\theCodelineNo{%
  \reset@font\scriptsize
  \color{gray}\arabic{CodelineNo}%
}
\makeatother
\begin{document}
  \DocInput{\jobname.dtx}
\end{document}
%</driver>
% \fi
%
% \CheckSum{274}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
%
% \GetFileInfo{\jobname.sty}
%
% \MakeShortVerb{\"}
%
% \changes{v2.2}{2022/11/15}{Rename the macros \cs{textin},
% \cs{textsu}, \cs{textnu} and \cs{textde}}
% \changes{v2.2}{2022/11/15}{Update the fonts to v.2.0, adjust the
% package}
% \changes{v2.1}{2019/07/20}{Fix a bug in handling of alias keys}
% \changes{v2.0}{2019/07/07}{Add the `scaled' key as requested by FMi}
% \changes{v2.0}{2019/07/07}{Use NFSS scheme for fonts mapping}
% \changes{v2.0}{2019/07/07}{Rewrite major part of the code supporting
% 14 fonts.  Harmonize the usage of package options over different TeX
% engines}
% \changes{v2.0}{2019/07/07}{Update OTF fonts to version 1.007 now
% consisting of 14 fonts, 843 glyphs each}
% \changes{v1.0}{2016/05/01}{Initial version}
%
% \title{Using \textsf{Chivo} fonts with LaTeX\thanks{This file has
% version number \fileversion, last revised \filedate.}}
% \author{Arash Esbati}
% \date{\filedate}
% \maketitle
%
% \begin{abstract}
%   \noindent
%   This package provides the necessary files to use the
%   \textsf{Chivo} fonts with LaTeX.  \textsf{Chivo} is a set of
%   eighteen fonts provided by Héctor Gatti \& Omnibus-Type Team under
%   the Open Font License (OFL).
% \end{abstract}
%
% \tableofcontents \clearpage
%
% \section{Introduction}
% \label{sec:introduction}
%
% \textsf{Chivo}\footnote{Copyright \textcopyright\ 2019, Omnibus-Type
% (\url{https://www.omnibus-type.com}).} is a set of eighteen fonts
% created by Héctor Gatti \& Omnibus-Type Team and provided by
% Omnibus-Type under the SIL Open Font License, Version 1.1.  From the
% readme:
% \begin{quote}
%   ``Chivo (`goat' in Spanish) is the first Omnibus-Type
%   neo-grotesque typeface family.  It has 9 weight variants, plus
%   matching italics.  Its solidness and balanced strokes give Chivo
%   both elegance and practicality.  Chivo Regular works perfectly in
%   long-reading texts, while Chivo Black is ideal for headlines,
%   banners and highlights.  Developed by Héctor Gatti, this is an
%   indispensable ally for any designer.''
% \end{quote}
%
% The fonts were obtained from Omnibus-Type GitHub
% repository\footnote{\url{https://github.com/Omnibus-Type/Chivo}}
% (v2.002, 18~fonts, 877~glyphs each).
%
% \section{Installation}
% \label{sec:installation}
%
% I suggest you use MikTeX or TeXlive and wait until the fonts are
% distributed for your TeX installation via their package manager.
%
% \section{Usage}
% \label{sec:usage}
%
% The fonts can be used with traditional (pdf)TeX engine and newer
% XeTeX and LuaTeX engines which can access OTF fonts directly.  They
% are described below.  In general, this bundle tries not to be smart
% and loads only \pkg{kvoptions.sty} and \pkg{fontaxes.sty} or
% \pkg{fontspec.sty}, depending on the used engine.  As the result,
% some work must be done by the user.
%
% \subsection{Engine independent options}
% \label{sec:engine-indep-opti}
%
% A package \pkg{Chivo.sty} is provided which can be loaded in the
% preamble as usual:
% \begin{quote}
%   "\usepackage{Chivo}"
% \end{quote}
% Loading the package changes the document's sans serif font to
% \textsf{Chivo}.  The package offers some options.
%
% \DescribeMacro{thin\,\textbar\,extralight\,\textbar\,light}
% \DescribeMacro{regular\,\textbar\,medium}
% The default font series is set with one of "thin", "extralight",
% "light", "regular" or "medium"~-- "regular" is the default.  It can
% be changed like this:
% \begin{quote}
%   "\usepackage[medium]{Chivo}"
% \end{quote}
%
% \DescribeMacro{semibold\,\textbar\,bold}
% \DescribeMacro{bold\,\textbar\,extrabold\textbar\,black}
% The default bold series is set with one of "semibold", "bold",
% "extrabold" or "black"~-- "bold" is the default.
%
% \clearpage
%
% \DescribeMacro{lining\,\textbar\,oldstyle}
% \DescribeMacro{proportional\,\textbar\,tabular}
% The alignment and style for the figures of the document are set by
% the options "lining" or "oldstyle" and "proportional" or "tabular".
% "lining" and "proportional" are the defaults.  Short aliases "lf",
% "osf", "prop" and "tab" are also available.  Being boolean options,
% following example results in proportional oldstyle figures:
% \begin{quote}
%   "\usepackage[lining=false,tabular=false]{Chivo}"
% \end{quote}
%
% \DescribeMacro{familydefault}
% The main font of a document can be switched to \textsf{Chivo} with
% package option "familydefault" or "familydefault=true", e.g.:
% \begin{quote}
%   "\usepackage[familydefault]{Chivo}"
% \end{quote}
%
% \DescribeMacro{opentype\,\textbar\,type1}
% If the automatic engine detection fails, it can be specified by the
% user with one of "opentype" or "type1".  Note that these option do
% not take a boolean value "true" or "false".
%
% \DescribeMacro{scale\,\textbar\,scaled}
% The size of the \textsf{Chivo} fonts can be adjusted with the
% "scale" or "scaled" option, e.g.:
% \begin{quote}
%   "\usepackage[scale=0.97]{Chivo}"
% \end{quote}
%
% \subsection{(pdf)LaTeX specific notes}
% \label{sec:under-pdflatex}
%
% OT1, LY1, T1 and TS1 encodings are supported.  \pkg{Chivo.sty}
% does not load the respective packages, it is up to user to load
% them, e.g.:
% \begin{quote}
%   "\usepackage{Chivo}"       \\
%   "\usepackage[T1]{fontenc}"
% \end{quote}
%
% Chivo fonts provide a rich set of figures (see
% table~\ref{tab:figures}).  With traditional engines,
% \pkg{fontaxes.sty} is loaded and the command \cmd{\fontversion} is
% available to switch between "lining", "oldstyle", "tabular" and
% "proportional" styles and alignments.  For other versions the macros
% below are provided.  Note that the names of these macros used to be
% \cs{textsu}, \cs{textin}\footnote{This macro is defined by the
% \pkg{hyperref} package.}, \cs{textnu} and \cs{textde}\footnote{These
% macros have a great potential to clash with \cs{babeltags}
% definitions.}.  The is a backward incompatible change in v2.2 of
% this package:
% \begin{itemize}
% \item "\textsuperior" renders its argument in superior figures. \\
%   "\textsuperior{123}" produces \textsuperior{123}.  The
%   corresponding declaration is "\sufigures".
% \item "\textinferior" renders its argument in inferior figures. \\
%   "\textinferior{123}" produces \textinferior{123}.  The
%   corresponding declaration is "\infigures".
% \item "\textnumerator" renders its argument in numerator figures. \\
%   "\textnumerator{123}" produces \textnumerator{123}.  The
%   corresponding declaration is "\nufigures".
% \item "\textdenominator" renders its argument in denominator
%   figures. \\
%   "\textdenominator{123}" produces \textdenominator{123}.  The
%   corresponding declaration is "\defigures".
% \end{itemize}
%
% \subsection{XeLaTeX and LuaLaTeX specific notes}
% \label{sec:under-xelat-lual}
%
% These engines can access OTF fonts directly.  The standard interface
% for this purpose is
% \pkg{fontspec.sty}\footnote{\url{https://ctan.org/pkg/fontspec}}.
% \pkg{Chivo.sty} detects if XeLaTeX or LuaLaTeX are used and passes
% options given to the package to "\setsansfont" via
% \cmd{\defaultfeature}.  Other \pkg{fontspec.sty} options can be set
% with \cmd{\defaultfeature+}:
% \begin{quote}
%   "\usepackage{fontspec}"          \\
%   "\usepackage{Chivo}"             \\
%   "\defaultfontfeatures+[Chivo]{"  \\
%   "  Scale = MatchLowercase ,"     \\
%   "  Color = blue"                 \\
%   "}"
% \end{quote}
% ^^A
% Table \ref{tab:font-features} contains a list of available font
% features.
% \begin{table}[tbp]
%   \centering
%   \caption{Chivo font features}
%   \label{tab:font-features}
%   \begin{tabular}{@{}>{\ttfamily}ll@{}}
%     \toprule
%     aalt & Access All Alternates \\
%     case & Case-Sensitive Forms \\
%     ccmp & Glyph Composition/Decomposition \\
%     cpsp & Capital Spacing \\
%     dnom & Denominators \\
%     frac & Fractions \\
%     kern & Kerning \\
%     liga & Standard Ligatures \\
%     lnum & Lining Figures \\
%     mark & Mark Positioning \\
%     mkmk & Mark to Mark Positioning \\
%     numr & Numerators \\
%     onum & Oldstyle Figures \\
%     ordn & Ordinals \\
%     pnum & Proportional Figures \\
%     sinf & Scientific Inferiors \\
%     subs & Subscript \\
%     sups & Superscript \\
%     tnum & Tabular Figures \\
%     zero & Slashed Zero \\
%     \bottomrule
%   \end{tabular}
% \end{table}
%
% LaTeX font selection macros like \cmd{\fontseries} work with these
% engines as well.
%
% \clearpage
%
% \section{OT1 encoding font table}
% \label{sec:ot1-font-table}
%
% \xfonttable{OT1}{Chivo-TLF}{m}{n}
%
% \clearpage
%
% \section{LY1 encoding font table}
% \label{sec:ly1-font-table}
%
% \xfonttable{LY1}{Chivo-TLF}{m}{n}
%
% \section{T1 encoding font table}
% \label{sec:t1-font-table}
%
% \xfonttable{T1}{Chivo-TLF}{m}{n}
%
% \section{TS1 encoding font table}
% \label{sec:ts1-font-table}
%
% \xfonttable{TS1}{Chivo-TLF}{m}{n}
%
% \clearpage
%
% \section{Text samples}
% \label{sec:text-samples}
%
% \subsection*{Regular}
% \lipsum[1]\lipsum*[2] \quad 1234567890
%
% \subsection*{Italic}
% {\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Medium}
% {\fontseries{medium}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Medium Italic}
% {\fontseries{medium}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Semi bold}
% {\fontseries{sb}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Semi bold Italic}
% {\fontseries{sb}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Bold}
% {\bfseries\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Bold italic}
% {\bfseries\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Extra Bold}
% {\fontseries{eb}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Extra Bold italic}
% {\fontseries{eb}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Black}
% {\fontseries{ub}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Black italic}
% {\fontseries{ub}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Thin}
% {\fontseries{el}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Thin italic}
% {\fontseries{el}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad  1234567890}
%
% \subsection*{Light}
% {\fontseries{l}\selectfont\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Light italic}
% {\fontseries{l}\selectfont\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Figures}
%
% \begin{table}[!h]
%   \centering
%   \caption{Figure styles and alignments}
%   \label{tab:figures}
%   \begin{tabular}{@{}ll@{}}
%     \toprule
%     Proportional lining   & \_\figureversion{prop,lf}0123456789\_  \\
%     Tabular lining        & \_\figureversion{tab,lf}0123456789\_   \\
%     Proportional oldstyle & \_\figureversion{prop,osf}0123456789\_ \\
%     Tabular oldstyle      & \_\figureversion{tab,osf}0123456789\_  \\
%     Superiors             & \_\sufigures 0123456789\_ \\
%     Inferiors             & \_\infigures 0123456789\_ \\
%     Numerators            & \_\nufigures 0123456789\_ \\
%     Denominators          & \_\defigures 0123456789\_ \\
%     \bottomrule
%   \end{tabular}
% \end{table}
%
%
% \StopEventually{^^A
%   \PrintChanges
% }
%
% \clearpage
%
% \section{Implementation}
% \label{sec:implementation}
%
% Font support files are generated by "autoinst", invoked with:
% \begin{verbatim}
% autoinst                \
%   --encoding=OT1,T1,LY1 \
%   --ts1                 \
%   --sanserif            \
%   --nosmallcaps         \
%   --noswash             \
%   --notitling           \
%   --inferiors=subs      \
%   --fractions           \
%   --noornaments         \
%   --ligatures           \
%   --verbose             \
%   Chivo-*.otf
% \end{verbatim}
% \vspace{-\baselineskip}
% We don't use the \pkg{Chivo.sty} generated by "autoinst" and use our
% version instead.
%
% \subsection{Chivo.sty}
% \label{sec:chivo.sty}
%
%    \begin{macrocode}
%<*package>
%    \end{macrocode}
%
% Require the package \pkg{iftex.sty}:
%    \begin{macrocode}
\RequirePackage{iftex}
%    \end{macrocode}
%
% First, we need a switch to know which engine is used:
%    \begin{macrocode}
\newif\ifChivo@otf
\iftutex                      % we are in XeTeX or LuaTeX
  \Chivo@otftrue
\else
  \Chivo@otffalse
\fi
%    \end{macrocode}
%
% Load the packages we rely on:
% \pkg{fontspec.sty}\footnote{\url{https://www.ctan.org/pkg/fontspec}}
% for LuaTeX and XeTeX, or
% \pkg{fontaxes.sty}\footnote{\url{https://www.ctan.org/pkg/fontaxes}}
% for (pdf)LaTeX:
%    \begin{macrocode}
\ifChivo@otf
  \RequirePackage{fontspec}
\else
  \RequirePackage{fontaxes}
\fi
%    \end{macrocode}
%
% We use
% \pkg{kvoptions}\footnote{\url{https://www.ctan.org/pkg/kvoptions}}
% for key handling:
%    \begin{macrocode}
\RequirePackage{kvoptions}
%    \end{macrocode}
% Setup the keyval options:
%    \begin{macrocode}
\SetupKeyvalOptions{%
  family = Chivo  ,
  prefix = Chivo@
}
%    \end{macrocode}
%
% The basic strategy for font and figure version related keys is to
% declare them as boolean options.
%    \begin{macrocode}
\DeclareBoolOption{thin}
\DeclareBoolOption{extralight}
\DeclareBoolOption{light}
\DeclareBoolOption{regular}
\DeclareBoolOption{medium}
\DeclareBoolOption{semibold}
\DeclareBoolOption{bold}
\DeclareBoolOption{extrabold}
\DeclareBoolOption{black}
\DeclareBoolOption{lining}
\DeclareComplementaryOption{oldstyle}{lining}
\DeclareBoolOption{tabular}
\DeclareComplementaryOption{proportional}{tabular}
%    \end{macrocode}
%
% Alias keys are defined with \cmd{\define@key}.  Given values to
% these keys are passed to the original keys and they are set via
% \cmd{\kvsetkeys}.
%    \begin{macrocode}
\define@key{Chivo}{lf}[true]{%
  \kvsetkeys{Chivo}{lining=#1}%
}
\define@key{Chivo}{osf}[true]{%
  \kvsetkeys{Chivo}{oldstyle=#1}%
}
\define@key{Chivo}{tab}[true]{%
  \kvsetkeys{Chivo}{tabular=#1}%
}
\define@key{Chivo}{prop}[true]{%
  \kvsetkeys{Chivo}{proportional=#1}%
}
%    \end{macrocode}
% "scale" and "scaled" keys are special: With (pdf)LaTeX, "scale" is a
% string option, "scaled" is an alias and sets \cmd{\Chivo@scale}
% which is defined by the "scale" key.  With XeLaTeX and LuaLaTeX,
% they set the macro \cmd{\Chivo@otf@scale} which is later used in
% \cmd{\defaultfontfeatures}.
%    \begin{macrocode}
\ifChivo@otf
  \newcommand*\Chivo@otf@scale{}
  \define@key{Chivo}{scale}[1.0]{%
    \def\Chivo@otf@scale{#1}%
  }
  \define@key{Chivo}{scaled}[1.0]{%
    \def\Chivo@otf@scale{#1}%
  }
\else
  \DeclareStringOption[1.0]{scale}
  \define@key{Chivo}{scaled}[1.0]{%
    \renewcommand*\Chivo@scale{#1}%
  }
\fi
%    \end{macrocode}
%
% "familydefault" switches the default font to \textsf{Chivo}:
%    \begin{macrocode}
\DeclareBoolOption{familydefault}
%    \end{macrocode}
%
% The next 2 are for users where the automatic engine detection might
% fail:
%    \begin{macrocode}
\DeclareVoidOption{opentype}{\Chivo@otftrue}
\DeclareVoidOption{type1}{\Chivo@otffalse}
%    \end{macrocode}
%
% Execute the default options and process the rest.
%    \begin{macrocode}
\kvsetkeys{Chivo}{regular,bold,lining,proportional}
\ProcessKeyvalOptions{Chivo}
%    \end{macrocode}
%
% We need some macros to store the figure alignment and style and font
% defintions:
%    \begin{macrocode}
\newcommand*\Chivo@figurestyle{}
\newcommand*\Chivo@figurealign{}
\newcommand*\Chivo@otf@regular{}
\newcommand*\Chivo@otf@italic{}
\newcommand*\Chivo@otf@bold{}
\newcommand*\Chivo@otf@bolditalic{}
%    \end{macrocode}
%
% Definition for "thin", "extralight", "light", "regular" and "medium"
% keys for all engines:
%    \begin{macrocode}
\ifChivo@regular
  \ifChivo@otf
    \def\Chivo@otf@regular{Regular}
    \def\Chivo@otf@italic{Italic}
  \else
    \DeclareFontSeriesDefault[sf]{md}{m}
  \fi
\fi
\ifChivo@medium
  \ifChivo@otf
    \def\Chivo@otf@regular{Medium}
    \def\Chivo@otf@italic{MediumItalic}
  \else
    \DeclareFontSeriesDefault[sf]{md}{medium}
  \fi
\fi
\ifChivo@light
  \ifChivo@otf
    \def\Chivo@otf@regular{Light}
    \def\Chivo@otf@italic{LightItalic}
  \else
    \DeclareFontSeriesDefault[sf]{md}{l}
  \fi
\fi
\ifChivo@extralight
  \ifChivo@otf
    \def\Chivo@otf@regular{ExtraLight}
    \def\Chivo@otf@italic{ExtraLightItalic}
  \else
    \DeclareFontSeriesDefault[sf]{md}{el}
  \fi
\fi
\ifChivo@thin
  \ifChivo@otf
    \def\Chivo@otf@regular{Thin}
    \def\Chivo@otf@italic{ThinItalic}
  \else
    \DeclareFontSeriesDefault[sf]{md}{ul}
  \fi
\fi
%    \end{macrocode}
%
% Definition for "semibold", "bold", "extrabold" and "black" keys for
% all engines:
%    \begin{macrocode}
\ifChivo@bold
  \ifChivo@otf
    \def\Chivo@otf@bold{Bold}
    \def\Chivo@otf@bolditalic{BoldItalic}
  \else
    \DeclareFontSeriesDefault[sf]{bf}{b}
  \fi
\fi
\ifChivo@semibold
  \ifChivo@otf
    \def\Chivo@otf@bold{SemiBold}
    \def\Chivo@otf@bolditalic{SemiBoldItalic}
  \else
    \DeclareFontSeriesDefault[sf]{bf}{sb}
  \fi
\fi
\ifChivo@extrabold
  \ifChivo@otf
    \def\Chivo@otf@bold{ExtraBold}
    \def\Chivo@otf@bolditalic{ExtraBoldItalic}
  \else
    \DeclareFontSeriesDefault[sf]{bf}{eb}
  \fi
\fi
\ifChivo@black
  \ifChivo@otf
    \def\Chivo@otf@bold{Black}
    \def\Chivo@otf@bolditalic{BlackItalic}
  \else
    \DeclareFontSeriesDefault[sf]{bf}{ub}
  \fi
\fi
%    \end{macrocode}
%
% Definition for figure styles for all engines:
%    \begin{macrocode}
\ifChivo@lining
  \ifChivo@otf
    \def\Chivo@figurestyle{Lining}
  \else
    \def\Chivo@figurestyle{LF}
  \fi
\else
  \ifChivo@otf
    \def\Chivo@figurestyle{OldStyle}
  \else
    \def\Chivo@figurestyle{OsF}
  \fi
\fi
%    \end{macrocode}
%
% Definition for figure alignments for all engines:
%    \begin{macrocode}
\ifChivo@tabular
  \ifChivo@otf
    \def\Chivo@figurealign{Monospaced}
  \else
    \def\Chivo@figurealign{T}
  \fi
\else
  \ifChivo@otf
    \def\Chivo@figurealign{Proportional}
  \else
    \def\Chivo@figurealign{}
  \fi
\fi
%    \end{macrocode}
%
% Standard setup for \pkg{fontaxes.sty}.  This is only relevant for
% (pdf)LaTeX:
%    \begin{macrocode}
\ifChivo@otf \else
  \fa@naming@exception{figures}{{superior}{proportional}}{Sup}
  \fa@naming@exception{figures}{{superior}{tabular}}{Sup}
  \def\sufigures{\@nomath\sufigures
    \fontfigurestyle{superior}\selectfont}
  \DeclareTextFontCommand{\textsuperior}{\sufigures}

  \fa@naming@exception{figures}{{inferior}{proportional}}{Inf}
  \fa@naming@exception{figures}{{inferior}{tabular}}{Inf}
  \def\infigures{\@nomath\infigures
    \fontfigurestyle{inferior}\selectfont}
  \DeclareTextFontCommand{\textinferior}{\infigures}

  \fa@naming@exception{figures}{{numerators}{proportional}}{Numr}
  \fa@naming@exception{figures}{{numerators}{tabular}}{Numr}
  \def\nufigures{\@nomath\nufigures
    \fontfigurestyle{numerators}\selectfont}
  \DeclareTextFontCommand{\textnumerator}{\nufigures}

  \fa@naming@exception{figures}{{denominators}{proportional}}{Dnom}
  \fa@naming@exception{figures}{{denominators}{tabular}}{Dnom}
  \def\defigures{\@nomath\defigures
    \fontfigurestyle{denominators}\selectfont}
  \DeclareTextFontCommand{\textdenominator}{\defigures}
\fi
%    \end{macrocode}
%
% For XeLaTeX/LuaLaTeX, we define the font features depending on the
% given package options:
%    \begin{macrocode}
\ifChivo@otf
  \defaultfontfeatures[Chivo]{%
    Extension      = .otf                         ,
    Ligatures      = {TeX,Common}                 ,
    Scale          = \Chivo@otf@scale             ,
    UprightFont    = *-\Chivo@otf@regular         ,
    ItalicFont     = *-\Chivo@otf@italic          ,
    BoldFont       = *-\Chivo@otf@bold            ,
    BoldItalicFont = *-\Chivo@otf@bolditalic      ,
    FontFace       = {ul}{n}{*-Thin}              ,
    FontFace       = {ul}{it}{*-ThinItalic}       ,
    FontFace       = {el}{n}{*-ExtraLight}        ,
    FontFace       = {el}{it}{*-ExtraLightItalic} ,
    FontFace       = {l}{n}{*-Light}              ,
    FontFace       = {l}{it}{*-LightItalic}       ,
    FontFace       = {m}{n}{*-Regular}            ,
    FontFace       = {m}{it}{*-Italic}            ,
    FontFace       = {medium}{n}{*-Medium}        ,
    FontFace       = {medium}{it}{*-MediumItalic} ,
    FontFace       = {sb}{n}{*-SemiBold}          ,
    FontFace       = {sb}{it}{*-SemiBoldItalic}   ,
    FontFace       = {b}{n}{*-Bold}               ,
    FontFace       = {b}{it}{*-BoldItalic}        ,
    FontFace       = {eb}{n}{*-ExtraBold}         ,
    FontFace       = {eb}{it}{*-ExtraBoldItalic}  ,
    FontFace       = {ub}{n}{*-Black}             ,
    FontFace       = {ub}{it}{*-BlackItalic}      ,
    Numbers        = {\Chivo@figurestyle,\Chivo@figurealign}
  }
\fi
%    \end{macrocode}
%
% Switch the sans serif font to \textsf{Chivo}.  With \pkg{fontspec},
% we delay the \cmd{\setsansfont} until "AtBeginDocument" so that user
% can provide additional features with \cmd{\defaultfontfeatures+}.
%    \begin{macrocode}
\ifChivo@otf
  \AtBeginDocument{%
    \setsansfont{Chivo}%
  }
\else
  \renewcommand*\sfdefault{%
    Chivo-\Chivo@figurealign\Chivo@figurestyle
  }
\fi
%    \end{macrocode}
%
% Switch the default font if the respective key is given:
%    \begin{macrocode}
\ifChivo@familydefault
  \ifChivo@otf
    \AtBeginDocument{%
      \setmainfont{Chivo}%
    }
  \else
    \renewcommand*\familydefault{\sfdefault}
  \fi
\fi
%    \end{macrocode}
%
% Also set the TS1 subset supported by the fonts.  LaTeX kernel sets
% this as well, but we put it here for the sake of completeness:
%    \begin{macrocode}
\ifChivo@otf \else
  \DeclareEncodingSubset{TS1}{Chivo-*}{4}
\fi
%    \end{macrocode}
%
%    \begin{macrocode}
%</package>
%    \end{macrocode}
%
% \Finale
%
\endinput
%
% Local Variables:
% mode: doctex
% TeX-master: t
% ispell-check-comments: exclusive
% LaTeX-shortvrb-chars: (?\")
% End:
