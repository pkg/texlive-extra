% \iffalse meta-comment
%
% File: Rosario.dtx
% Copyright (C) 2016-2019 Arash Esbati <arash'@'gnu.org>
%
% This work may be distributed and/or modified under the conditions
% of the LaTeX Project Public License (LPPL), either version 1.3c of
% this license or (at your option) any later version.  The latest
% version of this license is in the file:
%
%    http://www.latex-project.org/lppl.txt
%
% \fi
%
% \iffalse
%<package>\NeedsTeXFormat{LaTeX2e}[1999/12/01]
%<package>\ProvidesPackage{Rosario}
%<package>  [2019/07/19 v2.1 Support for Rosario fonts (AE)]
%
%<*driver>
\documentclass[a4paper]{ltxdoc}
\usepackage[osf]{mathpazo}
\usepackage[osf,scale=0.97]{Rosario}
\usepackage[utf8]{inputenc}
\usepackage[OT1,LY1,T1]{fontenc}
\usepackage{textcomp}
\usepackage{fonttable,lipsum,array,booktabs}
\usepackage{caption}
\captionsetup[table]{%
  format        = plain         ,
  labelsep      = period        ,
  justification = centerlast    ,
  width         = 0.8\linewidth ,
  position      = top           ,
  skip          = 5pt           ,
  font          = small         ,
  labelfont     = bf
}
\usepackage[activate]{microtype}
\linespread{1.02}
\usepackage{hypdoc}
\newcommand*\pkg[1]{\textsf{#1}}
\DisableCrossrefs
\RecordChanges
\setlength\hfuzz{15pt}
\hbadness=7000
\frenchspacing
\raggedbottom
\makeatletter
\renewcommand\section{\@startsection{section}{1}{\z@}%
  {-3.5ex \@plus -1ex \@minus -.2ex}%
  {2.3ex \@plus.2ex}%
  {\normalfont\sffamily\Large\bfseries}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\sffamily\large\bfseries}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
  {3.25ex \@plus1ex \@minus.2ex}%
  {-1em}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
  {3.25ex \@plus1ex \@minus .2ex}%
  {-1em}%
  {\normalfont\sffamily\normalsize\bfseries}}
\renewcommand\@makefntext[1]{%
  \@setpar{%
    \@@par \@tempdima=\hsize
    \advance\@tempdima by -1.5em\relax
    \parshape \@ne 1.5em \@tempdima
  }%
  \par \parindent=\z@ \noindent %
  \hb@xt@ \z@{\hss \hb@xt@ 1.5em{\@thefnmark.\hss}}%
  #1%
}
\makeatother
\begin{document}
  \DocInput{\jobname.dtx}
\end{document}
%</driver>
% \fi
%
% \CheckSum{247}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
%
% \GetFileInfo{\jobname.sty}
%
% \MakeShortVerb{\"}
%
% \changes{v2.1}{2019/07/20}{Fix a bug in handling of alias keys}
% \changes{v2.0}{2019/07/07}{Remove `Rosario.fontspec' from the bundle
% since the functionality is now provided by the package itself}
% \changes{v2.0}{2019/07/07}{Add the `scaled' key as requested by FMi}
% \changes{v2.0}{2019/07/07}{Use NFSS scheme for fonts mapping}
% \changes{v2.0}{2019/07/07}{Rewrite major part of the code supporting
% 8 fonts.  Harmonize the usage of package options over different TeX
% engines}
% \changes{v2.0}{2019/07/07}{Update OTF fonts to version 1.004 now
% consisting of 8 fonts, 844 glyphs each}
% \changes{v1.0}{2016/05/01}{Initial version}
%
% \title{Using \textsf{Rosario} fonts with LaTeX\thanks{This file has
% version number \fileversion, last revised \filedate.}}
% \author{Arash Esbati}
% \date{\filedate}
% \maketitle
%
% \begin{abstract}
%   \noindent
%   This package provides the necessary files to use the
%   \textsf{Rosario} fonts with LaTeX.  \textsf{Rosario} is a set of
%   eight fonts provided by Héctor Gatti, Adobe Fonts~\&
%   Omnibus-Type Team under the Open Font License (OFL).
% \end{abstract}
%
% \tableofcontents \clearpage
%
% \section{Introduction}
% \label{sec:introduction}
%
% \textsf{Rosario}\footnote{Copyright \textcopyright\ 2012--2019,
% Omnibus-Type (\url{https://www.omnibus-type.com})} is a set of eight
% fonts created by Héctor Gatti, Adobe Typekit \& Omnibus-Type Team
% and provided by Omnibus-Type\footnote{Many thanks to Omnibus-Type
% team for their explicit permission to upload the files to CTAN.}
% under the SIL Open Font License, Version 1.1.  From the web page:
% \begin{quote}\sffamily
%   ``\textsf{Rosario} is a singular typeface with 4 weight variants,
%   plus matching italics.  Its grace lies in the classic proportions.
%   Its weak endings make it subtle, yet dynamic.  Named after a
%   crowded city, it is suitable for diverse publishing contexts, such
%   as popular magazines, poetry books, illustrated editions and
%   academic journals.  It was designed by Héctor Gatti initially for
%   private use, later on developed by the Omnibus-Type Team, and
%   optimized by Adobe Fonts.''
% \end{quote}
%
% The fonts were obtained from Omni-Type web
% page\footnote{\url{https://www.omnibus-type.com/fonts/rosario/}}.
% (v1.004, 8~fonts, 844~glyphs each variant).
%
% \section{Installation}
% \label{sec:installation}
%
% I suggest you use MikTeX or TeXlive and wait until the fonts are
% distributed for your TeX installation via their package manager.
%
% \section{Usage}
% \label{sec:usage}
%
% The fonts can be used with traditional (pdf)TeX engine and newer
% XeTeX and LuaTeX engines which can access \textsc{otf} fonts
% directly.  They are described below.  In general, this bundle tries
% not to be smart and loads only \pkg{kvoptions}, \pkg{ifxetex},
% \pkg{ifluatex}, and depending on engine \pkg{fontaxes} and
% \pkg{mweights} or \pkg{fontspec}.  As the result, some work must be
% done by the user.
%
% \subsection{Engine independent options}
% \label{sec:engine-indep-opti}
%
% A package \pkg{Rosario.sty} is provided which can be loaded in the
% preamble as usual:
% \begin{quote}
%   "\usepackage{Rosario}"
% \end{quote}
% Loading the package changes the document's sans-serif font to
% \textsf{Rosario}.  The package offers some options.
%
% \DescribeMacro{light\,\textbar\,regular}
% The default sans-serif font series is set with one of "light" or
% "regular" -- "regular" is the default.  It can be changed like this:
% \begin{quote}
%   "\usepackage[light]{Rosario}"
% \end{quote}
%
% \DescribeMacro{bold\,\textbar\,semibold}
% The default bold series is set with one of "bold" or "semibold"~--
% "bold" is the default.
%
% \DescribeMacro{lining\,\textbar\,oldstyle}
% \DescribeMacro{proportional\,\textbar\,tabular}
% The alignment and style for the figures of the document are set by
% the options "lining" or "oldstyle" and "proportional" or "tabular".
% "lining" and "proportional" are the defaults.  Short aliases "lf",
% "osf", "prop" and "tab" are also available.  Being boolean options,
% following example results in proportional oldstyle figures:
% \begin{quote}
%   "\usepackage[lining=false,tabular=false]{Rosario}"
% \end{quote}
%
% \DescribeMacro{familydefault}
% The main font of a document can be switched to \textsf{Rosario} with
% package option "familydefault" or "familydefault=true", e.g.:
% \begin{quote}
%   "\usepackage[familydefault]{Rosario}"
% \end{quote}
%
% \DescribeMacro{opentype\,\textbar\,type1}
% If the automatic engine detection fails, it can be specified by the
% user with one of "opentype" or "type1".  Note that these option do
% not take a boolean value "true" or "false".
%
% \DescribeMacro{scale\,\textbar\,scaled}
% The size of the \textsf{Rosario} fonts can be adjusted with the
% "scale" or "scaled" option, e.g.:
% \begin{quote}
%   "\usepackage[scale=0.97]{Rosario}"
% \end{quote}
%
% \subsection{(pdf)LaTeX specific notes}
% \label{sec:under-pdflatex}
%
% \textsc{ot1}, \textsc{ly1}, \textsc{t1} and \textsc{ts1} encodings
% are supported.  \pkg{Rosario.sty} does not load the respective
% packages, it is up to user to load them, e.g.:
% \begin{quote}
%   "\usepackage[T1]{fontenc}" \\
%   "\usepackage{Rosario}"     \\
%   "\usepackage{textcomp}"
% \end{quote}
%
% \textsf{Rosario} fonts provide a rich set of figures (see
% table~\ref{tab:figures}).  With traditional engines,
% \pkg{fontaxes.sty} is loaded and the command \cmd{\fontversion} is
% available to switch between "lining", "oldstyle", "tabular" and
% "proportional" styles and alignments.  For other versions the
% following macros are provided:
% \begin{itemize}
% \item "\textsu" renders its argument in superior figures.
%   "\textsu{123}" produces {\sffamily\textsu{123}}.  The
%   corresponding declaration is "\sufigures".
% \item "\textin" renders its argument in inferior figures.
%   "\textin{123}" produces {\sffamily\textin{123}}.  The
%   corresponding declaration is "\infigures".
% \item "\textnu" renders its argument in numerator figures.
%   "\textnu{123}" produces {\sffamily\textnu{123}}.  The
%   corresponding declaration is "\nufigures".
% \item "\textde" renders its argument in denominator figures.
%   "\textde{123}" produces {\sffamily\textde{123}}.  The
%   corresponding declaration is "\defigures".
% \end{itemize}
%
% \subsection{XeLaTeX/LuaLaTeX specific notes}
% \label{sec:under-xelat-lual}
%
% These engines can access \textsc{OTF} fonts directly.  The standard
% interface for this purpose is
% \pkg{fontspec.sty}\footnote{\url{https://ctan.org/pkg/fontspec}}.
% \pkg{Rosario.sty} detects if XeLaTeX or LuaLaTeX are used and passes
% options given to the package to "\setsansfont" via
% \cmd{\defaultfeature}.  Other \pkg{fontspec.sty} options can be set
% with \cmd{\defaultfeature+}:
% \begin{quote}
%   "\usepackage{fontspec}"            \\
%   "\usepackage{Rosario}"             \\
%   "\defaultfontfeatures+[Rosario]{"  \\
%   "  Scale = MatchLowercase ,"       \\
%   "  Color = blue"                   \\
%   "}"
% \end{quote}
% ^^A
% Table \ref{tab:font-features} contains a list of available font
% features.
% \begin{table}[tbp]
%   \centering
%   \caption{Rosario font features}
%   \label{tab:font-features}
%   \begin{tabular}{@{}>{\ttfamily}ll@{}}
%     \toprule
%     aalt & Access All Alternates \\
%     case & Case-Sensitive Forms \\
%     ccmp & Glyph Composition/Decomposition \\
%     cpsp & Capital Spacing \\
%     dnom & Denominators \\
%     frac & Fractions \\
%     kern & Kerning \\
%     liga & Standard Ligatures \\
%     lnum & Lining Figures \\
%     mark & Mark Positioning \\
%     mkmk & Mark to Mark Positioning \\
%     numr & Numerators \\
%     onum & Oldstyle Figures \\
%     ordn & Ordinals \\
%     pnum & Proportional Figures \\
%     sinf & Scientific Inferiors \\
%     subs & Subscript \\
%     sups & Superscript \\
%     tnum & Tabular Figures \\
%     zero & Slashed Zero \\
%     \bottomrule
%   \end{tabular}
% \end{table}
%
% LaTeX font selection macros like \cmd{\fontseries} work with these
% engines as well.
%
% Earlier versions of this package provided a file
% \pkg{Rosario.fontspec}.  The functionality in now incorporated in
% the package.  Hence, \pkg{Rosario.fontspec} is removed.
%
% \clearpage
%
% \section{OT1 encoding font table}
% \label{sec:ot1-font-table}
%
% \xfonttable{OT1}{Rosario-LF}{m}{n}
%
% \clearpage
%
% \section{LY1 encoding font table}
% \label{sec:ly1-font-table}
%
% \xfonttable{LY1}{Rosario-LF}{m}{n}
%
% \section{T1 encoding font table}
% \label{sec:t1-font-table}
%
% \xfonttable{T1}{Rosario-LF}{m}{n}
%
% \section{TS1 encoding font table}
% \label{sec:ts1-font-table}
%
% \xfonttable{TS1}{Rosario-LF}{m}{n}
%
% \clearpage
%
% \section{Text samples}
% \label{sec:text-samples}
%
% \subsection*{Regular}
% {\sffamily\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Italic}
% {\sffamily\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Bold}
% {\sffamily\bfseries\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Bold italic}
% {\sffamily\bfseries\itshape\lipsum[1]\lipsum*[2] \quad 1234567890}
%
% \subsection*{Semi Bold}
% {\sffamily\fontseries{sb}\selectfont\lipsum[1]\lipsum*[2] \quad
% 1234567890}
%
% \subsection*{Semi Bold italic}
% {\sffamily\fontseries{sb}\selectfont\itshape\lipsum[1]\lipsum*[2]
% \quad 1234567890}
%
% \subsection*{Light}
% {\sffamily\fontseries{l}\selectfont\lipsum[1]\lipsum*[2] \quad
% 1234567890}
%
% \subsection*{Light italic}
% {\sffamily\fontseries{l}\selectfont\itshape\lipsum[1]\lipsum*[2]
% \quad 1234567890}
%
% \subsection*{Figures}
%
% \begin{table}[!h]
%   \centering
%   \caption{Figure styles and alignments}
%   \label{tab:figures}
%   \begin{tabular}{@{}l>{\sffamily}l@{}}
%     \toprule
%     Proportional lining   & \_\figureversion{prop,lf}0123456789\_  \\
%     Tabular lining        & \_\figureversion{tab,lf}0123456789\_   \\
%     Proportional oldstyle & \_\figureversion{prop,osf}0123456789\_ \\
%     Tabular oldstyle      & \_\figureversion{tab,osf}0123456789\_  \\
%     Superiors             & \_\sufigures 0123456789\_ \\
%     Inferiors             & \_\infigures 0123456789\_ \\
%     Numerators            & \_\nufigures 0123456789\_ \\
%     Denominators          & \_\defigures 0123456789\_ \\
%     \bottomrule
%   \end{tabular}
% \end{table}
%
% \StopEventually{^^A
%   \PrintChanges
% }
% \clearpage
%
% \section{Implementation}
% \label{sec:implementation}
%
% Font support files are generated by "autoinst".  As a random note,
% "autoinst" is invoked with:
% \begin{verbatim}
% autoinst                \
%   --encoding=OT1,T1,LY1 \
%   --ts1                 \
%   --sanserif            \
%   --nosmallcaps         \
%   --noswash             \
%   --notitling           \
%   --inferiors=subs      \
%   --fractions           \
%   --noornaments         \
%   --ligatures           \
%   --verbose             \
%   Rosario-*.otf
% \end{verbatim}
% \vspace{-\baselineskip}
%
% We don't use the \pkg{Rosario.sty} generated by "autoinst" and use
% our version instead.
%
% \subsection{Rosario.sty}
% \label{sec:rosario.sty}
%
%    \begin{macrocode}
%<*package>
%    \end{macrocode}
%
% Require the packages \pkg{ifxetex.sty} and \pkg{ifluatex.sty}:
%    \begin{macrocode}
\RequirePackage{ifxetex}
\RequirePackage{ifluatex}
%    \end{macrocode}
%
% First, we need a switch to know which engine is used:
%    \begin{macrocode}
\newif\ifRosario@otf
\ifxetex                      % we are in XeTeX
  \Rosario@otftrue
\else
  \ifluatex                   % we are in LuaTeX
    \Rosario@otftrue
  \else
    \Rosario@otffalse
  \fi
\fi
%    \end{macrocode}
%
% Load the packages we rely on:
% \pkg{fontspec.sty}\footnote{\url{https://www.ctan.org/pkg/fontspec}}
% for LuaTeX and XeTeX, or
% \pkg{mweights.sty}\footnote{\url{https://www.ctan.org/pkg/mweights}}
% and
% \pkg{fontaxes.sty}\footnote{\url{https://www.ctan.org/pkg/fontaxes}}
% for (pdf)LaTeX:
%    \begin{macrocode}
\ifRosario@otf
  \RequirePackage{fontspec}
\else
  \RequirePackage{mweights}
  \RequirePackage{fontaxes}
\fi
%    \end{macrocode}
%
% We use
% \pkg{kvoptions}\footnote{\url{https://www.ctan.org/pkg/kvoptions}}
% for key handling:
%    \begin{macrocode}
\RequirePackage{kvoptions}
%    \end{macrocode}
% Setup the keyval options:
%    \begin{macrocode}
\SetupKeyvalOptions{%
  family = Rosario  ,
  prefix = Rosario@
}
%    \end{macrocode}
%
% The basic strategy for font and figure related keys is to declare
% them as complementary ones, i.e. "regular" vs. "light", "bold"
% vs. "semibold", "lining" vs. "oldstyle" and "proportional"
% vs. "tabular":
%    \begin{macrocode}
\DeclareBoolOption{regular}
\DeclareComplementaryOption{light}{regular}
\DeclareBoolOption{bold}
\DeclareComplementaryOption{semibold}{bold}
\DeclareBoolOption{lining}
\DeclareComplementaryOption{oldstyle}{lining}
\DeclareBoolOption{tabular}
\DeclareComplementaryOption{proportional}{tabular}
%    \end{macrocode}
%
% Alias keys are defined with \cmd{\define@key}.  Given values to
% these keys are passed to the original keys and they are set via
% \cmd{\kvsetkeys}.
%    \begin{macrocode}
\define@key{Rosario}{lf}[true]{%
  \kvsetkeys{Rosario}{lining=#1}%
}
\define@key{Rosario}{osf}[true]{%
  \kvsetkeys{Rosario}{oldstyle=#1}%
}
\define@key{Rosario}{tab}[true]{%
  \kvsetkeys{Rosario}{tabular=#1}%
}
\define@key{Rosario}{prop}[true]{%
  \kvsetkeys{Rosario}{proportional=#1}%
}
%    \end{macrocode}
%
% "scale" and "scaled" keys are special: With (pdf)LaTeX, "scale" is a
% string option, "scaled" is an alias and sets \cmd{\Rosario@scale}
% which is defined by the "scale" key.  With XeLaTeX and LuaLaTeX,
% they set the macro \cmd{\Rosario@otf@scale} which is later used in
% \cmd{\defaultfontfeatures}.
%    \begin{macrocode}
\ifRosario@otf
  \newcommand*\Rosario@otf@scale{}
  \define@key{Rosario}{scale}[1.0]{%
    \renewcommand*\Rosario@otf@scale{#1}%
  }
  \define@key{Rosario}{scaled}[1.0]{%
    \renewcommand*\Rosario@otf@scale{#1}%
  }
\else
  \DeclareStringOption[1.0]{scale}
  \define@key{Rosario}{scaled}[1.0]{%
    \renewcommand*\Rosario@scale{#1}%
  }
\fi
%    \end{macrocode}
%
% "familydefault" switches the default font to \textsf{Rosario}:
%    \begin{macrocode}
\DeclareBoolOption{familydefault}
%    \end{macrocode}
%
% The next 2 are for users where the automatic engine detection might
% fail:
%    \begin{macrocode}
\DeclareVoidOption{opentype}{\Rosario@otftrue}
\DeclareVoidOption{type1}{\Rosario@otffalse}
%    \end{macrocode}
%
% Execute the default options and process the rest.
%    \begin{macrocode}
\setkeys{Rosario}{regular,bold,lining,proportional}
\ProcessKeyvalOptions{Rosario}
%    \end{macrocode}
%
% We need some macros to store the figure alignment and style and font
% defintions:
%    \begin{macrocode}
\newcommand*\Rosario@figurestyle{}
\newcommand*\Rosario@figurealign{}
\newcommand*\Rosario@otf@regular{}
\newcommand*\Rosario@otf@italic{}
\newcommand*\Rosario@otf@bold{}
\newcommand*\Rosario@otf@bolditalic{}
%    \end{macrocode}
%
% Definition for "light" and "regular" keys for all engines:
%    \begin{macrocode}
\ifRosario@regular
  \ifRosario@otf
    \def\Rosario@otf@regular{Regular}
    \def\Rosario@otf@italic{Italic}
  \else
    \def\mdseries@sf{m}
  \fi
\else
  \ifRosario@otf
    \def\Rosario@otf@regular{Light}
    \def\Rosario@otf@italic{LightItalic}
  \else
    \def\mdseries@sf{l}
  \fi
\fi
%    \end{macrocode}
%
% Definition for "semibold" and "bold" keys for all engines:
%    \begin{macrocode}
\ifRosario@bold
  \ifRosario@otf
    \def\Rosario@otf@bold{Bold}
    \def\Rosario@otf@bolditalic{BoldItalic}
  \else
    \def\bfseries@sf{b}
  \fi
\else
  \ifRosario@otf
    \def\Rosario@otf@bold{SemiBold}
    \def\Rosario@otf@bolditalic{SemiBoldItalic}
  \else
    \def\bfseries@sf{sb}
  \fi
\fi
%    \end{macrocode}
%
% Definition for figure styles for all engines:
%    \begin{macrocode}
\ifRosario@lining
  \ifRosario@otf
    \def\Rosario@figurestyle{Lining}
  \else
    \def\Rosario@figurestyle{LF}
  \fi
\else
  \ifRosario@otf
    \def\Rosario@figurestyle{OldStyle}
  \else
    \def\Rosario@figurestyle{OsF}
  \fi
\fi
%    \end{macrocode}
%
% Definition for figure alignments for all engines:
%    \begin{macrocode}
\ifRosario@tabular
  \ifRosario@otf
    \def\Rosario@figurealign{Monospaced}
  \else
    \def\Rosario@figurealign{T}
  \fi
\else
  \ifRosario@otf
    \def\Rosario@figurealign{Proportional}
  \else
    \def\Rosario@figurealign{}
  \fi
\fi
%    \end{macrocode}
%
% Standard setup for \pkg{fontaxes.sty}.  This is only relevant for
% (pdf)LaTeX:
%    \begin{macrocode}
\ifRosario@otf \else
  \fa@naming@exception{figures}{{superior}{proportional}}{Sup}
  \fa@naming@exception{figures}{{superior}{tabular}}{Sup}
  \def\sufigures{\@nomath\sufigures
    \fontfigurestyle{superior}\selectfont}
  \DeclareTextFontCommand{\textsu}{\sufigures}
  \let\textsuperior\textsu

  \fa@naming@exception{figures}{{inferior}{proportional}}{Inf}
  \fa@naming@exception{figures}{{inferior}{tabular}}{Inf}
  \def\infigures{\@nomath\infigures
    \fontfigurestyle{inferior}\selectfont}
  \DeclareTextFontCommand{\textin}{\infigures}
  \let\textinferior\textin

  \fa@naming@exception{figures}{{numerators}{proportional}}{Numr}
  \fa@naming@exception{figures}{{numerators}{tabular}}{Numr}
  \def\nufigures{\@nomath\nufigures
    \fontfigurestyle{numerators}\selectfont}
  \DeclareTextFontCommand{\textnu}{\nufigures}
  \let\textnumerator\textnu

  \fa@naming@exception{figures}{{denominators}{proportional}}{Dnom}
  \fa@naming@exception{figures}{{denominators}{tabular}}{Dnom}
  \def\defigures{\@nomath\defigures
    \fontfigurestyle{denominators}\selectfont}
  \DeclareTextFontCommand{\textde}{\defigures}
  \let\textdenominator\textde
\fi
%    \end{macrocode}
%
% For XeLaTeX/LuaLaTeX, we define the font features depending on the
% given package options:
%    \begin{macrocode}
\ifRosario@otf
  \defaultfontfeatures[Rosario]{%
    Extension      = .otf                        ,
    Ligatures      = {TeX,Common}                ,
    Scale          = \Rosario@otf@scale          ,
    UprightFont    = *-\Rosario@otf@regular      ,
    ItalicFont     = *-\Rosario@otf@italic       ,
    BoldFont       = *-\Rosario@otf@bold         ,
    BoldItalicFont = *-\Rosario@otf@bolditalic   ,
    FontFace       = {l}{n}{*-Light}             ,
    FontFace       = {l}{it}{*-LightItalic}      ,
    FontFace       = {m}{n}{*-Regular}           ,
    FontFace       = {m}{it}{*-Italic}           ,
    FontFace       = {b}{n}{*-Bold}              ,
    FontFace       = {b}{it}{*-BoldItalic}       ,
    FontFace       = {sb}{n}{*-SemiBold}        ,
    FontFace       = {sb}{it}{*-SemiBoldItalic} ,
    Numbers        = {\Rosario@figurestyle,\Rosario@figurealign}
  }
\fi
%    \end{macrocode}
%
% Switch the sans serif font to \textsf{Rosario}.  With
% \pkg{fontspec}, we delay the \cmd{\setsansfont} until
% "AtBeginDocument" so that user can provide additional features with
% \cmd{\defaultfontfeatures+}.
%    \begin{macrocode}
\ifRosario@otf
  \AtBeginDocument{%
    \setsansfont{Rosario}%
  }
\else
  \renewcommand*\sfdefault{%
    Rosario-\Rosario@figurealign\Rosario@figurestyle
  }
\fi
%    \end{macrocode}
%
% Switch the default font if the respective key is given:
%    \begin{macrocode}
\ifRosario@familydefault
  \ifRosario@otf
    \AtBeginDocument{%
      \setmainfont{Rosario}%
    }
  \else
    \renewcommand*\familydefault{\sfdefault}
    \ifRosario@regular \else
      \edef\seriesdefault{\mdseries@sf}
    \fi
    \ifRosario@bold \else
      \edef\bfdefault{\bfseries@sf}
    \fi
  \fi
\fi
%    \end{macrocode}
%
%    \begin{macrocode}
%</package>
%    \end{macrocode}
%
% \Finale
%
\endinput
%
% Local Variables:
% mode: doctex
% TeX-master: t
% End:
