\relax

\documentclass[twocolumn]{article}
\usepackage{fontdoc}

\title{Euler-VM math italic encoding vector}
\author{Walter Schmidt}
\date{2002-11-12 \\
Version 2.9a}

\begin{document}
\maketitle

\section{Introduction}

This document describes the encoding of the Euler-VM `letters'
font.

\encoding

\needsfontinstversion{1.800}


\comment{\section{Default values}}

\setstr{codingscheme}{TEX MATH ITALIC}

\setint{italicslant}{0}

\ifisglyph{x}\then
   \setint{xheight}{\height{x}}
\else
   \setint{xheight}{500}
\fi


\comment{\section{Default font dimensions}}

\setint{fontdimen(1)}{\int{italicslant}} % italic slant
\setint{fontdimen(2)}{0}                 % interword space
\setint{fontdimen(3)}{0}                 % interword stretch
\setint{fontdimen(4)}{0}                 % interword shrink
\setint{fontdimen(5)}{\int{xheight}}     % x-height
\setint{fontdimen(6)}{1000}              % quad
\setint{fontdimen(7)}{0}                 % extra space after .


\comment{\section{The encoding}
   There are 128 glyphs in this encoding.}

\setslot{Gammaupright}
   \comment{The greek letter `$\Gamma$'.}
\endsetslot

\setslot{Deltaupright}
   \comment{The greek letter `$\Delta$'.}
\endsetslot

\setslot{Thetaupright}
   \comment{The greek letter `$\Theta$'.}
\endsetslot

\setslot{Lambdaupright}
   \comment{The greek letter `$\Lambda$'.}
\endsetslot

\setslot{Xiupright}
   \comment{The greek letter `$\Xi$'.}
\endsetslot

\setslot{Piupright}
   \comment{The greek letter `$\Pi$'.}
\endsetslot

\setslot{Sigmaupright}
   \comment{The greek letter `$\Sigma$'.}
\endsetslot

\setslot{Upsilonupright}
   \comment{The greek letter `$\Upsilon$' named `Upsilon1' by Adobe.}
\endsetslot

\setslot{Phiupright}
   \comment{The greek letter `$\Phi$'.}
\endsetslot

\setslot{Psiupright}
   \comment{The greek letter `$\Psi$'.}
\endsetslot

\setslot{Omegaupright}
   \comment{The greek letter `$\Omega$'.}
\endsetslot

\setslot{alphaupright}
   \comment{The greek letter `$\alpha$'.}
\endsetslot

\setslot{betaupright}
   \comment{The greek letter `$\beta$'.}
\endsetslot

\setslot{gammaupright}
   \comment{The greek letter `$\gamma$'.}
\endsetslot

\setslot{deltaupright}
   \comment{The greek letter `$\delta$'.}
\endsetslot

\setslot{epsilonupright}
   \comment{The greek letter `$\epsilon$'.}
\endsetslot

\setslot{zetaupright}
   \comment{The greek letter `$\zeta$'.}
\endsetslot

\setslot{etaupright}
   \comment{The greek letter `$\eta$'.}
\endsetslot

\setslot{thetaupright}
   \comment{The greek letter `$\theta$'.}
\endsetslot

\setslot{iotaupright}
   \comment{The greek letter `$\iota$'.}
\endsetslot

\setslot{kappaupright}
   \comment{The greek letter `$\kappa$'.}
\endsetslot

\setslot{lambdaupright}
   \comment{The greek letter `$\lambda$'.}
\endsetslot

\setslot{muupright}
   \comment{The greek letter `$\mu$'.}
\endsetslot

\setslot{nuupright}
   \comment{The greek letter `$\nu$'.}
\endsetslot

\setslot{xiupright}
   \comment{The greek letter `$\xi$'.}
\endsetslot

\setslot{piupright}
   \comment{The greek letter `$\pi$'.}
\endsetslot

\setslot{rhoupright}
   \comment{The greek letter `$\rho$'.}
\endsetslot

\setslot{sigmaupright}
   \comment{The greek letter `$\sigma$'.}
\endsetslot

\setslot{tauupright}
   \comment{The greek letter `$\tau$'.}
\endsetslot

\setslot{upsilonupright}
   \comment{The greek letter `$\upsilon$'.}
\endsetslot

\setslot{phiupright}
   \comment{The greek letter `$\phi$'.}
\endsetslot

\setslot{chiupright}
   \comment{The greek letter `$\chi$'.}
\endsetslot

\setslot{psiupright}
   \comment{The greek letter `$\psi$'.}
\endsetslot

\setslot{omegaupright}
   \comment{The greek letter `$\omega$'.}
\endsetslot

\setslot{epsilon1upright}
   \comment{The greek letter `$\varepsilon$'.}
\endsetslot

\setslot{theta1upright}
   \comment{The greek letter `$\vartheta$'.}
\endsetslot

\setslot{pi1upright}
   \comment{The greek letter `$\varpi$', rather unfortunately named
      `omega1' by Adobe.}
\endsetslot

\nextslot{39}
\setslot{phi1upright}
   \comment{The greek letter `$\varphi$'.}
\endsetslot

\setslot{harpoonleftup}
   \comment{The harpoon symbol `$\leftharpoonup$',
       named `arrowlefttophalf' in MathTime and Lucida New Math.}
\endsetslot

\setslot{harpoonleftdown}
   \comment{The harpoon symbol `$\leftharpoondown$',
       named `arrowleftbothalf' in MathTime and Lucida New Math.}
\endsetslot

\setslot{harpoonrightup}
   \comment{The harpoon symbol `$\rightharpoonup$',
       named `arrowrighttophalf' in MathTime and Lucida New Math.}
\endsetslot

\setslot{harpoonrightdown}
   \comment{The harpoon symbol `$\rightharpoondown$',
       named `arrowrightbothalf' in MathTime and Lucida New Math.}
\endsetslot

\setslot{hookrightchar}
   \comment{The building block for `$\hookrightarrow$',
       named `arrowhookleft' in MathTime and Lucida New Math.}
\endsetslot

\setslot{hookleftchar}
   \comment{The building block for `$\hookleftarrow$',
       named `arrowhookright' in MathTime and Lucida New Math.}
\endsetslot

\setslot{triangleright}
   \comment{The triangle symbol `$\triangleright$'.}
\endsetslot

\setslot{triangleleft}
   \comment{The triangle symbol `$\triangleleft$'.}
\endsetslot

\setslot{zeroupright}
   \comment{The oldstyle digit zero `0'.}
\endsetslot

\setslot{oneupright}
   \comment{The oldstyle digit one `1'.}
\endsetslot

\setslot{twoupright}
   \comment{The oldstyle digit two `2'.}
\endsetslot

\setslot{threeupright}
   \comment{The oldstyle digit three `3'.}
\endsetslot

\setslot{fourupright}
   \comment{The oldstyle digit four `4'.}
\endsetslot

\setslot{fiveupright}
   \comment{The oldstyle digit five `5'.}
\endsetslot

\setslot{sixupright}
   \comment{The oldstyle digit six `6'.}
\endsetslot

\setslot{sevenupright}
   \comment{The oldstyle digit seven `7'.}
\endsetslot

\setslot{eightupright}
   \comment{The oldstyle digit eight `8'.}
\endsetslot

\setslot{nineupright}
   \comment{The oldstyle digit nine `9'.}
\endsetslot

\setslot{period}
   \comment{The period `$.$'.}
\endsetslot

\setslot{comma}
   \comment{The comma `$,$'.}
\endsetslot

\setslot{less}
   \comment{The less-than sign `$<$'.}
\endsetslot

\setslot{slash}
   \comment{The forwards oblique `$/$'.}
\endsetslot

\setslot{greater}
   \comment{The greater-than sign `$>$'.}
\endsetslot

\setslot{star}
   \comment{The star symbol `$\star$'.}
\endsetslot

\setslot{partialdiffupright}
   \comment{The upright partial differentiation symbol `$\partial$'.}
\endsetslot

\setslot{A}
   \comment{The letter `$\mathnormal{A}$'.}
\endsetslot

\setslot{B}
   \comment{The letter `$\mathnormal{B}$'.}
\endsetslot

\setslot{C}
   \comment{The letter `$\mathnormal{C}$'.}
\endsetslot

\setslot{D}
   \comment{The letter `$\mathnormal{D}$'.}
\endsetslot

\setslot{E}
   \comment{The letter `$\mathnormal{E}$'.}
\endsetslot

\setslot{F}
   \comment{The letter `$\mathnormal{F}$'.}
\endsetslot

\setslot{G}
   \comment{The letter `$\mathnormal{G}$'.}
\endsetslot

\setslot{H}
   \comment{The letter `$\mathnormal{H}$'.}
\endsetslot

\setslot{I}
   \comment{The letter `$\mathnormal{I}$'.}
\endsetslot

\setslot{J}
   \comment{The letter `$\mathnormal{J}$'.}
\endsetslot

\setslot{K}
   \comment{The letter `$\mathnormal{K}$'.}
\endsetslot

\setslot{L}
   \comment{The letter `$\mathnormal{L}$'.}
\endsetslot

\setslot{M}
   \comment{The letter `$\mathnormal{M}$'.}
\endsetslot

\setslot{N}
   \comment{The letter `$\mathnormal{N}$'.}
\endsetslot

\setslot{O}
   \comment{The letter `$\mathnormal{O}$'.}
\endsetslot

\setslot{P}
   \comment{The letter `$\mathnormal{P}$'.}
\endsetslot

\setslot{Q}
   \comment{The letter `$\mathnormal{Q}$'.}
\endsetslot

\setslot{R}
   \comment{The letter `$\mathnormal{R}$'.}
\endsetslot

\setslot{S}
   \comment{The letter `$\mathnormal{S}$'.}
\endsetslot

\setslot{T}
   \comment{The letter `$\mathnormal{T}$'.}
\endsetslot

\setslot{U}
   \comment{The letter `$\mathnormal{U}$'.}
\endsetslot

\setslot{V}
   \comment{The letter `$\mathnormal{V}$'.}
\endsetslot

\setslot{W}
   \comment{The letter `$\mathnormal{W}$'.}
\endsetslot

\setslot{X}
   \comment{The letter `$\mathnormal{X}$'.}
\endsetslot

\setslot{Y}
   \comment{The letter `$\mathnormal{Y}$'.}
\endsetslot

\setslot{Z}
   \comment{The letter `$\mathnormal{Z}$'.}
\endsetslot

\setslot{flat}
   \comment{The flat musical symbol `$\flat$'.}
\endsetslot

\setslot{natural}
   \comment{The natural musical symbol `$\natural$'.}
\endsetslot

\setslot{sharp}
   \comment{The sharp musical symbol `$\sharp$'.}
\endsetslot

\setslot{slurbelow}
   \comment{The slur symbol `$\smile$'.}
\endsetslot

\setslot{slurabove}
   \comment{The slur symbol `$\frown$'.}
\endsetslot

\setslot{lscript}
   \comment{The script letter `$\ell$'.}
\endsetslot

\setslot{a}
   \comment{The letter `$\mathnormal{a}$'.}
\endsetslot

\setslot{b}
   \comment{The letter `$\mathnormal{b}$'.}
\endsetslot

\setslot{c}
   \comment{The letter `$\mathnormal{c}$'.}
\endsetslot

\setslot{d}
   \comment{The letter `$\mathnormal{d}$'.}
\endsetslot

\setslot{e}
   \comment{The letter `$\mathnormal{e}$'.}
\endsetslot

\setslot{f}
   \comment{The letter `$\mathnormal{f}$'.}
\endsetslot

\setslot{g}
   \comment{The letter `$\mathnormal{g}$'.}
\endsetslot

\setslot{h}
   \comment{The letter `$\mathnormal{h}$'.}
\endsetslot

\setslot{i}
   \comment{The letter `$\mathnormal{i}$'.}
\endsetslot

\setslot{j}
   \comment{The letter `$\mathnormal{j}$'.}
\endsetslot

\setslot{k}
   \comment{The letter `$\mathnormal{k}$'.}
\endsetslot

\setslot{l}
   \comment{The letter `$\mathnormal{l}$'.}
\endsetslot

\setslot{m}
   \comment{The letter `$\mathnormal{m}$'.}
\endsetslot

\setslot{n}
   \comment{The letter `$\mathnormal{n}$'.}
\endsetslot

\setslot{o}
   \comment{The letter `$\mathnormal{o}$'.}
\endsetslot

\setslot{p}
   \comment{The letter `$\mathnormal{p}$'.}
\endsetslot

\setslot{q}
   \comment{The letter `$\mathnormal{q}$'.}
\endsetslot

\setslot{r}
   \comment{The letter `$\mathnormal{r}$'.}
\endsetslot

\setslot{s}
   \comment{The letter `$\mathnormal{s}$'.}
\endsetslot

\setslot{t}
   \comment{The letter `$\mathnormal{t}$'.}
\endsetslot

\setslot{u}
   \comment{The letter `$\mathnormal{u}$'.}
\endsetslot

\setslot{v}
   \comment{The letter `$\mathnormal{v}$'.}
\endsetslot

\setslot{w}
   \comment{The letter `$\mathnormal{w}$'.}
\endsetslot

\setslot{x}
   \comment{The letter `$\mathnormal{x}$'.}
\endsetslot

\setslot{y}
   \comment{The letter `$\mathnormal{y}$'.}
\endsetslot

\setslot{z}
   \comment{The letter `$\mathnormal{z}$'.}
\endsetslot

\setslot{dotlessi}
   \comment{The dotless letter `$\imath$'.}
\endsetslot

\setslot{dotlessj}
   \comment{The dotless letter `$\jmath$'.}
\endsetslot

\setslot{weierstrass}
   \comment{The Weierstrass P symbol `$\wp$'.}
\endsetslot

\setslot{vector}
   \comment{The vector accent symbol `$\vec{x}$'.}
\endsetslot

\setslot{skewchar}\endsetslot

\setslot{hslash}
   \comment{The letter `$\hslash$'.}
\endsetslot

\setslot{minus}
   \comment{The `relbar'}
\endsetslot

\setslot{equal}
   \comment{The `Relbar'}
\endsetslot

\endencoding

\end{document}
