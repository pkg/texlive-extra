% \iffalse meta-comment
%% plari - Typesetting stage play scripts with LaTeX2e
%%
%% Copyright (c) 1998 Antti-Juhani Kaijanaho
%%
%%    This program is free software; you can redistribute it and/or modify
%%    it under the terms of the GNU General Public License as published by
%%    the Free Software Foundation; either version 2 of the License, or
%%    (at your option) any later version.
%%
%%    This program is distributed in the hope that it will be useful,
%%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%    GNU General Public License for more details.
%%
%%    You should have received a copy of the GNU General Public License
%%    along with this program; see the file COPYING.  If not, write to
%%    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
%%    Boston, MA 02111-1307, USA.
%%
%% Just to be sure everybody understands this:
%%  Including plari.cls in one's document with the LaTeX2e command
%%     \documentclass[...]{plari}
%%  is considered /use/ of plari.cls and is therefore not restricted
%%  by the GNU General Public License.  If you still are unsure, you
%%  can regard this notice as a special exeption to the GNU General
%%  Public License, to be applied to plari.cls as authored by Antti-Juhani
%%  Kaijanaho.
%%
% \fi
%% \CheckSum{87}
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%
\def\filedate{1998/11/02}
\def\fileversion{19981102-CTAN}
\def\fileinfo{development snapshot}
%\iffalse
%<*driver>
\documentclass{ltxdoc}
\begin{document}
\DocInput{plari.dtx}
\end{document}
%</driver>
%\fi
% 
%
%\title{The \texttt{plari} document class\\
%  Typesetting stageplay scripts with \LaTeXe\thanks
%  {This document describes \texttt{plari.dtx} version number
%    \fileversion\ dated \filedate.}}
%\author{Antti-Juhani Kaijanaho}
%\maketitle
%
%\begin{abstract}
%  A document class for typesetting stageplay scripts is described.
%\end{abstract}
%

%\section{Usage}

%The document class \texttt{plari} behaves like the standard document
%class \texttt{report} except that (a)~the sectioning commands
%|\section| \textit{et cetera} do not work and (b)~paragraphs are
%separated, not indented.

%In addition the following macros are defined: |\newact| starts a new
%Act, |\newscene| starts a new scene, \texttt{\bslash
%  setting\{\meta{describe setting}\}} starts a new scene and describes
%the new setting, \texttt{\bslash repl\{\meta{speaker}\}} begins a new
%line of dialogue, spoken by \meta{speaker}, and \texttt{\bslash
%  paren\{\meta{note}\}} typesets a parenthetical note.

%
%\StopEventually{\relax}
%
%\section{Implementation}
%
% First we warm up.
%    \begin{macrocode}
%<*class>
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{plari}[\filedate\space\fileversion\space\fileinfo]
\typeout{plari <\filedate:\fileversion> - typesetting stageplay scripts}
%    \end{macrocode}

% Currently we don't take any special arguments.  We just let
% \texttt{report} handle them.
%    \begin{macrocode}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions
%    \end{macrocode}

% We steal most of our code from \texttt{report.cls}.
%    \begin{macrocode}
\LoadClass{report}
%    \end{macrocode}

% Now we get to the point.  Acts are numbered individually, scenes are
% numbered within acts.  When referenced, act numbers are Roman, and
% scenes are of the form \meta{Act}:\meta{Scene}, where \meta{Scene} is
% the current scene in Arabic.
%    \begin{macrocode}
\newcounter{act}
\newcounter{scene}[act]
\renewcommand{\theact}{\Roman{act}}
\renewcommand{\thescene}{\Roman{act}:\arabic{scene}}
%    \end{macrocode}

% Acts start a new page.  Their numbers are typeset bold Huge.  Scenes
% do not start a page, and their numbers are typeset medium Large.
%    \begin{macrocode}
\newcommand{\newact}{%
  \refstepcounter{act}% 
  \newpage{\centering\bfseries\Huge--- \Roman{act} ---\par}%
  \vspace{1.5cm plus .5cm minus .5cm}%
  }
\newcommand{\newscene}{%
  \refstepcounter{scene}%
  \bigskip%
  {\centering\mdseries\Large\arabic{scene}\par}%
  \medskip%
  }
%    \end{macrocode}

% Most scenes come with a change of scenery.  The setting is typeset
% with normal size italic, but with reduced canavas.  Describing a
% setting changes scene.
%    \begin{macrocode}
\newcommand{\setting}[1]{%
  \newscene%
  \begingroup%
    \list{}{%
      \listparindent=\parindent%
      \parsep=\parskip%
      \topsep=\z@%
      \rightmargin=\leftmargin%
      }%
    \item%
    \let\item\@undefined%
    {\normalsize\itshape\mdseries#1\par}%
    \endlist%
  \endgroup%
  }
%    \end{macrocode}

% Every line in dialogue is preceded by a name.  The name is typeset
% centered, in normal size medium series small caps.  The next paragraph
% should not be indented.
%    \begin{macrocode}
\newcommand{\repl}[1]{%
  \medskip%
  {\centering\normalsize\mdseries\scshape#1\par}%
%  \smallskip%
  }
%    \end{macrocode}

% Sometimes parenthetical notes must be inserted within dialogue.  These
% notes are typeset in italic.
%    \begin{macrocode}
\newcommand{\paren}[1]{\textit{#1}}
%    \end{macrocode}

% We do not want indents. Instead, we'll have a skip between
% paragraphs.
%    \begin{macrocode}
\parindent=\z@
\parskip=\medskipamount\relax
%    \end{macrocode}

% We don't want the user to use \LaTeX\ standard sectioning.
%    \begin{macrocode}
\let\@startsection\@undefined
%    \end{macrocode} 

% We are done for good.
%    \begin{macrocode}
%</class>
%    \end{macrocode}

% \Finale
