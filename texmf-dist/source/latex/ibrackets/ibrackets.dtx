% \iffalse meta-comment
%
% Copyright (C) 2022 by Antoine Missier <antoine.missier@ac-toulouse.fr>
%
% This file may be distributed and/or modified under the conditions of
% the LaTeX Project Public License, either version 1.3 of this license
% or (at your option) any later version.  The latest version of this
% license is in:
%
%   http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of LaTeX version
% 2005/12/01 or later.
% \fi
%
% \iffalse
%<*driver>
\ProvidesFile{ibrackets.dtx}
%</driver>
%<*package> 
\NeedsTeXFormat{LaTeX2e}[2005/12/01]
\ProvidesPackage{ibrackets}
    [2022/12/26 v1.1 .dtx ibrackets file]
%</package>
%<*driver>
\documentclass{ltxdoc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage[charter]{mathdesign}
\usepackage{ibrackets}
\usepackage{sectsty}
\usepackage{xcolor}

\definecolor{darkbrown}{rgb}{0.5,0.1,0.1}
\allsectionsfont{\color{darkbrown}}
\renewcommand\pi{\piup}

\DisableCrossrefs
%\CodelineIndex
%\RecordChanges
\usepackage{hyperref}
\hypersetup{%
    colorlinks,
    linkcolor=blue,
    citecolor=red,
    pdftitle={ibrackets},
    pdfsubject={LaTeX package},
    pdfauthor={Antoine Missier}
}
\MakeShortVerb{"}
\begin{document}
\DocInput{ibrackets.dtx}
%\PrintChanges
%\PrintIndex
\end{document}
%</driver>
% \fi
%
% \CheckSum{49}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
% \changes{v1.0}{2022/12/19}{Initial version}
%
% \changes{v1.1}{2022/12/28}{
%    - A bug correction in the README.md file
%    - Tiny changes in documentation}
%
% \GetFileInfo{ibrackets.sty}
%
% \title{Intelligent brackets \\ The \textsf{ibrackets} package}
% \author{Antoine Missier \\ \texttt{antoine.missier@ac-toulouse.fr}}
% \date{\filedate, \fileversion}
% \maketitle
%
% \section{Introduction}
%
% Open intervals are usually represented with parenthesis $(0, +\infty)$
% but sometimes we find also
% square brackets $]0, +\infty[$, for example in French mathematics.
% When using these, spacing is often unsuitable, 
% e.g.\@ $x \in \mathclose{]} 0, +\infty\mathopen{[}$.
% This small package redefines brackets symbols $[$ and $]$ for mathematical mode
% to get correct spacing: $x \in ]0, +\infty[$.
%
% \medskip
% Originally implemented in the \textsf{mismath} package~\cite{MIS}
% and also in \textsf{frenchmath}~\cite{FR}
% since version 2.1, our previous redefinitions produce however incorrect spacing
% when the left bound of the interval begins with a sign - or +,
% which was then interpreted as a binary operation. Thus blank
% spaces surrounding the sign would have been too large.
% This problem was pointed out by Jean-François Burnol, and
% an easy solution, that has been documented, consisted to nest the operator
% or the left bound within a pair of braces, 
% e.g.\@ |$x \in ]{-}\infty, 0]$|, or use |\left| and |\right| 
% or even |\mathopen{]}|.
%
% \medskip
% Inspired by the \textsf{icomma} package~\cite{ICOMMA} of Walter Schmidt, we now provide 
% an improved bracket definition that works correctly without
% these pairs of curly brackets.
%
% \medskip
% Let's also mention other approaches e.g.\@ |\DeclarePairedDelimiters|, a macro from
% the \textsf{mathtools} package~\cite{TOOL}, or
% the \textsf{interval} package~\cite{INT} with his
% |\interval| macro. Nevertheless our solution is lighter.
%
% \section{Usage}
% You just have to type intervals in an easy way:
% |$x\in]0,\pi[\cup]2\pi, 3\pi[$|
% produce
% \[ x\in]0, \pi[\cup]2\pi, 3\pi[ \mbox{\quad with \textsf{ibrackets}}, \]
% instead of
% \[ x\in\mathclose{]} 0,\pi \mathopen{[}\cup\mathclose{]} 2\pi, 3\pi\mathopen{[}
% \mbox{\quad without \textsf{ibrackets}}. 
% \]
%
% Generally $[$ and $]$ symbols are not defined anymore as delimiters,
% but as ordinary characters.
% Thereby a line break could occur between the two square brackets, but
% it is always possible to transform them 
% into delimiters with |\left| and |\right|.
%
% \medskip
% The problem of a sign following the first bracket is solved with this package,
% so the example in the introduction is simply obtained with |$x \in ]-\infty, 0]$|
% which gives $x \in ]-\infty, 0]$.
%
% \medskip
% \emph{However, you don't have to leave a space between the first bracket and the sign}: e.g.\@
% |$x \in ] -\infty, 0]$| yields $x \in ] -\infty, 0]$ with bad spacing around the
% minus sign. 
% Contrariwise, when you want to write algebra on intervals then
% \emph{you must leave a blank space between the second bracket and the} +/- \emph{operations},
% e.g.\@ |$[a, b] + [c, d]$| yields $[a, b] + [c, d]$
% but |$[a, b]+ [c, d]$| yields $[a, b]+ [c, d]$.
% To summarize the new behavior of a bracket: it is an ordinary character, 
% but an open delimiter when it is immediately followed by a + or - character.
%
% \StopEventually{}
%
% \section{Implementation}
% At |\begin{document}|, we memorize the "\mathcode" of the original brackets,
% in the |\math...bracket| macros, and we make the brackets in math mode active:
%    \begin{macrocode}
\AtBeginDocument{%
  \mathchardef\mathopenbracket\mathcode`[%
  \mathcode`[="8000
  \mathchardef\mathclosebracket\mathcode`]%
  \mathcode`]="8000
}

%    \end{macrocode}
% The active brackets check the next input character. If this is a - or a +, 
% the active brackets return |\mathopen| with the saved |\math...bracket| 
% so that no space will be added after the bracket; 
% otherwise, |\mathord\math...bracket| is returned :
%    \begin{macrocode}
{\catcode`[=\active
  \gdef[{\futurelet\@next\sm@rtopenbracket}}
\def\sm@rtopenbracket{%
  \ifx\@next- \mathopen \else
  \ifx\@next+ \mathopen \else
    \mathord\fi\fi \mathopenbracket}

{\catcode`]=\active
  \gdef]{\futurelet\@next\sm@rtclosebracket}}
\def\sm@rtclosebracket{%
  \ifx\@next- \mathopen \else
  \ifx\@next+ \mathopen \else
    \mathord\fi\fi \mathclosebracket}
%    \end{macrocode}
% We could have use the internal \TeX\ command |\@ifnextchar|
% to skip blank spaces after the bracket, and look if there is a + or - after,
% but then it  would become tricky when you really want
% to follow an interval with an operation plus or minus.
%
% \begin{thebibliography}{5}
% % \begin{raggedright}
% \bibitem{MIS} \emph{\textsf{mismath} -- Miscellaneus mathematical macros}. 
% Antoine Missier, CTAN, v2.0 2022/11/11.
% \bibitem{FR} \emph{L'extension \textsf{frenchmath}}. Antoine Missier, CTAN, v2.2 2022/12/15.
% \bibitem{ICOMMA} \emph{The \textsf{icomma} package for \LaTeXe}. 
% Walter Schmidt, CTAN, v2.0 2002/03/10.
% \bibitem{TOOL} \emph{The \textsf{mathtool} package}. Morten Høgholm, Lars Madsen, CTAN,
% v1.21 2018/01/08.
% \bibitem{INT} \emph{The \textsf{interval} package}. Lars Madsen, CTAN,
% v0.4 2019/03/06.
% \end{raggedright}
% \end{thebibliography}
% \Finale
\endinput
