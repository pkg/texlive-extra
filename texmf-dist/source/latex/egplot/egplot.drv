%%
%% This is file `egplot.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% egplot.dtx  (with options: `driver')
%% 
%% Copyright (C) 1998 by Axel.Probst@bam.de
%% 
%% This file is NOT the source for emp, because almost all comments
%% have been stripped from it.  It is NOT the preferred form of emp
%% for making modifications to it.
%% 
%% Therefore you can NOT redistribute and/or modify THIS file.  You can
%% however redistribute the complete source (emp.dtx and emp.ins)
%% and/or modify it under the terms of the GNU General Public License as
%% published by  the Free Software Foundation; either version 2, or (at
%% your option) any later version.
%% 
%% As a special exception, you can redistribute parts of this file for
%% the electronic distribution of scientific papers, provided that you
%% include a short note pointing to the complete source.
%% 
%% Emp is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \CheckSum{449}
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper]{article}
\usepackage{doc}
\usepackage{multicol}
\IfFileExists{mflogo.sty}%
  {\usepackage{mflogo}%
   \def\GP{\textsf{gnuplot}}%
   \def\EGP{\textsf{egplot}}%
   \def\EMP{\textlogo{EMP}}}%
  {\def\GP{\textsf{gnuplot}}%
   \def\EMP{\textsf{EMP}}%
   \def\EGP{\textsf{egplot}}}
\usepackage[gnuplot35]{egplot}
\setlength{\parindent}{0pt}
\def\manindex#1{\SortIndex{#1}{#1}}
\EnableCrossrefs
\RecordChanges
\CodelineIndex
\DoNotIndex{\def,\gdef,\long,\let,\begin,\end,\if,\ifx,\else,\fi}
\DoNotIndex{\immediate,\write,\newwrite,\openout,\closeout,\typeout}
\DoNotIndex{\font,\jobname,\documentclass,\char,\catcode,\ }
\DoNotIndex{\CodelineIndex,\DocInput,\DoNotIndex,\EnableCrossrefs}
\DoNotIndex{\filedate,\filename,\fileversion,\logo,\manfnt}
\DoNotIndex{\NeedsTeXFormat,\ProvidesPackage,\RecordChanges,\space}
\DoNotIndex{\begingroup,\csname,\edef,\endcsname,\expandafter}
\DoNotIndex{\usepackage,\@ifundefined,\ignorespaces,\item,\leavevmode}
\DoNotIndex{\newcounter,\newif,\par,\parindent}
\DoNotIndex{\relax,\setcounter,\stepcounter,\the,\advance}
\DoNotIndex{\CurrentOption,\DeclareOption,\documentstyle}
\DoNotIndex{\endgroup,\global,\hfuzz,\LaTeX,\LaTeXe}
\DoNotIndex{\macrocode,\@makeother,\OnlyDescription,\PassOptionsToPackage}
\DoNotIndex{\ProcessOptions,\RequirePackage,\string,\textsf,\unitlength}
\DoNotIndex{\@bsphack,\@esphack,\@nameuse,\@ne,\active,\do,\dospecials}
\DoNotIndex{\errhelp,\errmessage,\ifcase,\IfFileExists,\includegraphics}
\DoNotIndex{\manindex,\SortIndex,\newcommand,\newtoks,\or,\origmacrocode}
\DoNotIndex{\alpha,\displaystyle,\frac,\sin,\texttt}
\let\origmacrocode\macrocode
\def\macrocode{\hfuzz 5em\origmacrocode}
\begin{document}
  \DocInput{egplot.dtx}
\end{document}
\endinput
%%
%% End of file `egplot.drv'.
