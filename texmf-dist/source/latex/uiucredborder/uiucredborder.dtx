% \iffalse meta-comment
%
% Copyright (C) 2000 Scott Pakin <pakin@uiuc.edu>
% -------------------------------------------------------
%
% This package may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.2
% of this license or (at your option) any later version.
% The latest version of this license is in
%    http://www.latex-project.org/lppl.txt
% and version 1.2 or later is part of all distributions of LaTeX
% version 1999/12/01 or later.
%
% \fi
% \iffalse
%% File: uiucredborder.dtx Copyright (C) 2000 Scott Pakin <pakin@uiuc.edu>
%
%<class>\NeedsTeXFormat{LaTeX2e}
%<class>\ProvidesClass{uiucredborder}
%<class>	  [2000/11/02 v1.00 UIUC red-bordered form class (SDP)]
%<class>\setlength{\paperwidth}{8.5in}
%<class>\setlength{\paperheight}{11in}
%<class>\RequirePackage{geometry}
%
%<*driver>
\documentclass{ltxdoc}
\EnableCrossrefs
\CodelineIndex
\begin{document}
\title{The \textsf{uiucredborder} class}
\author{Scott Pakin\\pakin@uiuc.edu}
\date{November 3, 2000}
\maketitle
\DocInput{uiucredborder.dtx}
\end{document}
%</driver>
% \fi
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \changes{v1.00}{2000/11/02}{Initial version}
%
% \CheckSum{145}
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%%
%
% \DoNotIndex{\begin,\DeclareOption,\def,\end,\hfil,\hspace}
% \DoNotIndex{\input,\MakeUppercase,\newcommand,\newlength,\par,\parbox}
% \DoNotIndex{\ProcessOptions,\relax,\renewcommand,\vspace}
%
% \begin{abstract}
%   Every UIUC graduate student dreads having to fill out the red-bordered
%   form that gets attached to his thesis because of all the trial-and-error
%   necessary to get the text to line up properly.  The
%   \texttt{uiucredborder} class makes it easy to produce proper formatting
%   on the first try.
% \end{abstract}
%
% \section{Usage}
%
% \subsection{Commands}
%
% There are four parameters that must be set in your document's preamble:
%
% \begin{description}
%    \item[\cmd{\title}]        \DescribeMacro{\title}
%      The title of your thesis
%
%    \item[\cmd{\author}]       \DescribeMacro{\author}
%      Your full name, exactly as it appears in the Graduate College's
%      records
%
%    \item[\cmd{\degreemonth}]  \DescribeMacro{\degreemonth}
%      The month (spelled out) in which your degree will be conferred
%
%    \item[\cmd{\degreeyear}]   \DescribeMacro{\degreeyear}
%      The four-digit year in which your degree will be conferred
% \end{description}
%
% \noindent
% \DescribeMacro{\msthesis}
% \DescribeMacro{\phdthesis}
% In addition, you must invoke either \cmd{\msthesis} or
% \cmd{\phdthesis}, depending on whether the red-bordered form is for
% a Master's thesis or a doctoral dissertation.
%
% Note that all of the above except for \cmd{\degreemonth} behave
% identically in |uiucthesis.sty|.  (The thesis title page does not
% include the month in which the degree is awarded.)  Thus, you can
% move all of the above definitions into a separate file which you can
% then \cmd{\input} into both your thesis and your red-bordered form.
% That way, if you change your thesis title or other information, the
% red-bordered form will always be up-to-date.
%
% \DescribeMacro{\maketitle}
% The body of your document should contain only a |\maketitle|, which will
% use the above parameters to fill in the red-bordered form.
%
% \subsection{Class options}
%
% The |uiucredborder| class accepts the |10pt|, |11pt|, and |12pt| options
% for setting the font size, just like in the standard \LaTeX\ classes.  (The
% default is |10pt|.)  In addition, the |linecentered| option centers each of
% your name, thesis title, and degree relative within the space provided on
% the corresponding line on the form.  The default is to center each of those
% relative to the thesis title.  |linecentered| looks a lot uglier and is
% recommended only if you desperately need an extra~3$\:$cm of space for your
% name.
%
% \subsection{Other customizations}
%
% \DescribeMacro{\formattext}
% By default, |uiucredborder| formats each piece of text in bold and
% uppercase.  To alter that behavior, use |\renewcommand| to
% change the definition of |\formattext| to anything that accepts one
% argument and formats it appropriately.
%
% \section{Example}
%
% The following is typical usage of |uiucredborder|:
%
% \begin{verbatim}
%\documentclass[11pt]{uiucredborder}
%
%\title{A \LaTeX\ Class for Filling in Red-Bordered Forms}
%\author{Scott Pakin}
%\degreeyear{2001}
%\degreemonth{May}
%\phdthesis
%
%\begin{document}
%\maketitle
%\end{document}
% \end{verbatim}
%
% \StopEventually{\PrintIndex}
%
% \section{Implementation}
%
% This section contains the complete source code for \texttt{uiucredborder}.
% Most users will not get much out of it, but it should be of use to
% those who need more precise documentation and those who want to extend
% the \texttt{uiucredborder} class.
%
%    \begin{macrocode}
%<*class>
%    \end{macrocode}
%
% \subsection{User parameters}
%
% \begin{macro}{\phdthesis}
% \begin{macro}{\msthesis}
% \begin{macro}{\degreeyear}
% The following were ripped out of \texttt{uiucthesis.sty}.
%    \begin{macrocode}
\def\phdthesis{\def\@degree{Doctor of Philosophy}}
\def\msthesis{\def\@degree{Master of Science}}
\def\degreeyear#1{\def\@degreeyear{#1}}
%    \end{macrocode}
% \end{macro}
% \end{macro}
% \end{macro}
%
% \begin{macro}{\degreemonth}
% We also need to know the graduation month, which \texttt{uiucthesis.sty}
% doesn't care about.
%    \begin{macrocode}
\def\degreemonth#1{\def\@degreemonth{#1}}
%    \end{macrocode}
% \end{macro}
%
% \subsection{Lengths}
%
% There are four fields the student must fill in on the red-bordered form:
%
% \begin{itemize}
%   \item the date (month + year) in which the degree will be conferred,
%   \item the student's full name, exactly as it appears in UIUC's records,
%   \item the thesis title, and
%   \item the degree to be conferred (``M.S.'' or ``Ph.D.,'' spelled out).
% \end{itemize}
%
% \begin{macro}{\date@indent}
% \begin{macro}{\author@indent}
% \begin{macro}{\title@indent}
% \begin{macro}{\degree@indent}
% Field indents are measured from the left edge of the paper and specify
% the horizontal offset that each field starts at.
%    \begin{macrocode}
\newlength{\date@indent}
\newlength{\author@indent}
\newlength{\title@indent}
\newlength{\degree@indent}
%    \end{macrocode}
% \end{macro}
% \end{macro}
% \end{macro}
% \end{macro}
%
% \begin{macro}{\date@width}
% \begin{macro}{\author@width}
% \begin{macro}{\title@width}
% \begin{macro}{\degree@width}
% Field widths are the width of each field.
%    \begin{macrocode}
\newlength{\date@width}
\newlength{\author@width}
\newlength{\title@width}
\newlength{\degree@width}
%    \end{macrocode}
% \end{macro}
% \end{macro}
% \end{macro}
% \end{macro}
%
% \begin{macro}{\center@lines}
% Specify that each line should be centered individually.  This looks a bit
% crooked, but may be needed if you have a very, very long name or use an
% especially wide font.
%    \begin{macrocode}
\def\center@lines{%
  \setlength{\date@indent}{127mm}
  \setlength{\author@indent}{39mm}
  \setlength{\title@indent}{60mm}
  \setlength{\degree@indent}{73mm}
%
  \setlength{\date@width}{63mm}
  \setlength{\author@width}{150mm}
  \setlength{\title@width}{130mm}
  \setlength{\degree@width}{118mm}
}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\center@globally}
% Specify that the student's name, thesis title, and degree should all be
% centered relative to each other.  One might think that the degree field,
% which is the narrowest and most indented, should determine the indentation
% for the other two fields.  However, we can actually use the wider and
% less-indented title field for this purpose, because we know \textit{a
% priori} that the text in the degree field will be short relative to the
% length of line on the form.
%    \begin{macrocode}
\def\center@globally{%
  \setlength{\date@indent}{127mm}
  \setlength{\author@indent}{60mm}
  \setlength{\title@indent}{60mm}
  \setlength{\degree@indent}{60mm}
%
  \setlength{\date@width}{63mm}
  \setlength{\author@width}{130mm}
  \setlength{\title@width}{130mm}
  \setlength{\degree@width}{130mm}
}
%    \end{macrocode}
% \end{macro}
%
% \subsection{Filling in the form}
%
% \begin{macro}{\formattext}
% The |\formattext| macro is applied to each chunk of text we output.
% The default is to make the text bold and uppercase, but the user can
% change that behavior with |\renewcommand|.
%    \begin{macrocode}
\newcommand{\formattext}[1]{\textbf{\MakeUppercase{#1}}}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\maketitle}
% We define the |\maketitle| macro to fill in the form with the contents
% of |\@degreemonth|, |\@degreeyear|, |\@author|, |\@title|, and |\@degree|.
%    \begin{macrocode}
\newcommand{\maketitle}{%
  \thispagestyle{empty}%
  \setlength{\baselineskip}{11mm}\setlength{\parindent}{0pt}%
  \vspace*{65mm}\hspace*{\date@indent}%
  \parbox[t]{\date@width}{%
    \hfil\formattext{\@degreemonth{} \@degreeyear}\hfil}\par
  \vspace*{22mm}\hspace*{\author@indent}%
  \parbox[t]{\author@width}{%
    \hfil\formattext{\@author}\hfil}\par
  \hspace*{\title@indent}%
  \begin{minipage}[t][3\baselineskip]{\title@width}
    \begin{center}
      \setlength{\baselineskip}{11mm}\setlength{\parindent}{0pt}%
      \formattext{\@title}
    \end{center}
  \end{minipage}\par
  \vspace*{-1mm}\hspace*{\degree@indent}%
  \parbox[t]{\degree@width}{\hfil\formattext{\@degree}\hfil}
}
%    \end{macrocode}
% \end{macro}
%
% \subsection{Option processing}
%
% |\center@globally| is the default.  The |linecentered| option invokes
% |\center@lines|.
%    \begin{macrocode}
\center@globally
\DeclareOption{linecentered}{\center@lines}
%    \end{macrocode}
%
% Let the user set the font size, just like in the standard \LaTeX\ classes.
% Use 10pt by default.
%    \begin{macrocode}
\newcommand{\@ptsize}{0}
\DeclareOption{10pt}{\renewcommand\@ptsize{0}}
\DeclareOption{11pt}{\renewcommand\@ptsize{1}}
\DeclareOption{12pt}{\renewcommand\@ptsize{2}}
%    \end{macrocode}
%
% Process our options.
%    \begin{macrocode}
\ProcessOptions\relax
\input{size1\@ptsize.clo}
%    \end{macrocode}
%
% \subsection{Margins}
%
% Set the margins to zero, so we can work with absolute lengths.
%    \begin{macrocode}
\geometry{noheadfoot,letterpaper,marginparsep=0pt}
\geometry{lmargin=0mm, rmargin=0mm}
\geometry{tmargin=0mm, bmargin=0mm}
%    \end{macrocode}
%
%    \begin{macrocode}
%</class>
%    \end{macrocode}
%
% \Finale
\endinput
