% \iffalse meta-comment
%
% Copyright (C) 2019-2020 by Antoine Missier <antoine.missier@ac-toulouse.fr>
%
% This file may be distributed and/or modified under the conditions of
% the LaTeX Project Public License, either version 1.3 of this license
% or (at your option) any later version.  The latest version of this
% license is in:
%
%   http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of LaTeX version
% 2005/12/01 or later.
% \fi
%
% \iffalse
%<*driver>
\ProvidesFile{arraycols.dtx}
%</driver>
%<*package> 
\NeedsTeXFormat{LaTeX2e}[2005/12/01]
\ProvidesPackage{arraycols}   
    [2020/11/23 v1.1 .dtx arraycols file]
%</package>
%<*driver>
\documentclass{ltxdoc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french,english]{babel}
\usepackage{lmodern}
\usepackage{arraycols}
\DisableCrossrefs         
%\CodelineIndex
%\RecordChanges
\usepackage{hyperref}
\hypersetup{%
    colorlinks,
    linkcolor=blue, 
    citecolor=blue,
    pdftitle={arraycols}, 
    pdfsubject={LaTeX package}, 
    pdfauthor={Antoine Missier}
}
\MakeShortVerb{*}
\begin{document}
  \DocInput{arraycols.dtx}
  %\PrintChanges
  %\PrintIndex
\end{document}
%</driver>
% \fi
%
% \CheckSum{46}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
% \changes{v0.1}{27/12/2011}{First personal version}
% \changes{v1.0}{2019/05/04}{Initial version, creating dtx and ins files}
% \changes{v1.0}{2019/06/17}{English translation by François Bastouil}
% \changes{v1.1}{2020/11/23}{Incompatibility with tablestyles mentioned, 
% minor changes in documentation}
%
% \GetFileInfo{arraycols.sty}
%
% \title{The \textsf{arraycols} package\thanks{This document
% corresponds to \textsf{arraycols}~\fileversion, dated \filedate. 
% Thanks to François Bastouil for help in English translation.}}
% \author{Antoine Missier \\ \texttt{antoine.missier@ac-toulouse.fr}}
% \date{November 23, 2020}
% \maketitle
%
% \section{Introduction}
%
% This package provides new predefined column types to typeset tables in addition to 
% the \textsf{array} package by Frank Mittelbach and David Carlisle~\cite{ARRAY} 
% (loaded by \textsf{arraycols}) and also a command to draw wide horizontal rules.
% Here is a summary of the column types and macro, defined by \textsf{arraycols},
% which we detail in the next section.
% 
% \begin{center}\small \setlength{\extrarowheight}{1pt}
% \begin{tabular}{|cIm{7.9cm}|}
% \whline
% \multicolumn{2}{|c|}{\thead{Column definitions}} \\
% \Xhline{0.7pt}
% \texttt{L} & Left adjusted column (LR mode in \texttt{array} environments 
% or math mode in \texttt{tabular} environment) \\
% \hline
% \texttt{C} & Centered adjusted column (idem) \\
% \hline
% \texttt{R} & Right adjusted column (idem) \\
% \hline
% \texttt{t}\marg{width} & Text column (LR mode) of fixed \meta{width} like \texttt{p},
%  but horizontally and vertically centered  \\
% \hline
% \texttt{x} & Centered column in math mode with adjusted height to
% avoid touching the horizontal rules \\
% \hline
% \texttt{y} & Left aligned column in math mode with adjusted height \\
% \hline
% \texttt{z}\marg{width} & Centered column in math mode with adjusted height, like \texttt{x},
% and fixed \meta{width}  \\
% \hline
% \texttt{T} & Centered text column with adjusted width 
% for \texttt{tabularx} environments (calculated like \texttt{X} column)  \\
% \hline
% \texttt{Z} &  Centered column for \texttt{tabularx}, like \texttt{T},
% but in math mode with adjusted height, like \texttt{x} and	\texttt{z} \\
% \hline
% \texttt{I} & Thick vertical rule (1\,pt) \\
% \hline
% \texttt{V}\marg{thickness} & Vertical rule with variable \meta{thickness} \\
% \hline \hline
% \multicolumn{2}{|c|}{\thead{Horizontal rules}} \\
% \Xhline{0.7pt}
% |\whline| & Wide horizontal rule (1\,pt) \\
% \whline
% \end{tabular}
% \end{center}
%
% Note that if a column type has already been defined previously,
% it will be overwritten with a warning message.
%
% Besides \textsf{array}, \textsf{arraycols} loads the \textsf{cellspace} package~\cite{CELLSP}, 
% necessary for the \texttt{x}, \texttt{y}, \texttt{z} and \texttt{Z} types of columns and
% \textsf{tabularx}~\cite{TABX}, necessary for \texttt{T} and \texttt{Z}, 
% as well as \textsf{makecell}~\cite{MKCELL} for various alignments of multilined table cells.
% The \textsf{tablestyles} package~\cite{TBLSTY} defines also L, C, R, Z column types 
% but in a different way, nevertheless this package is incompatible with \textsf{makecell}
% and therefore with \textsf{arraycols} too.
%
% With a very short code, this package does not claim to develop new macros. 
% Its main action is to combine and set features coming from other packages.
%
% \section{Usage}
%
%\medskip
% \DescribeEnv{L}  \DescribeEnv{C}  \DescribeEnv{R}
% Referring to an example from the \textsf{array} package documentation, 
% \textsf{arraycols} provides the |L|, |C|, |R| columns types
% which reverse the mathematical mode.
% Then we can use them to get centered, left-aligned or right-aligned 
% LR-mode in an \texttt{array} environment or math-mode in a \texttt{tabular} environment.
% For instance, the declaration *\begin{tabular}{|l|C|r|}* sets centered mathematical mode
% in the second column and declaration *\begin{array}{|L|c|c|}* 
% sets text mode, left aligned in the first column\footnote{The declarations 
% \texttt{L, C, R} do not work in a \texttt{tabularx} environment.
% Note that the \textsf{tabulary} package by David Carlisle~\cite{TABY}
% already defines the \texttt{L, C, R, J} column types for particular alignments 
% in tables of same type as \texttt{tabularx}, 
% but there is no incompatibility because these column definitions
% only apply in \texttt{tabulary} environments.
% }.
% 
% \medskip
% \DescribeEnv{t\marg{width}}
% The new column type definition \texttt{t}\{\meta{width}\} (text in LR-mode)
% produces horizontal and vertical centering in the column unlike the classics
% \texttt{p}\marg{width} (in standard \LaTeX) and \texttt{m}\marg{width}
% (from the \textsf{array} package) which produce left aligned text (visible when the column
% is wider than the text inside).
%
% \medskip
% \DescribeEnv{x} \DescribeEnv{y}
% To ensure sufficient height for rows, for instance in displaymath mode
% formulas, we provide the columntypes \texttt{x} (centered)
% and \texttt{y} (left aligned), based on the \textsf{cellspace} package 
% by Josselin Noirel~\cite{CELLSP}.
% They allow automatic adjustment of row heights to avoid touching the 
% horizontal rules when content is too high.
% Although \textsf{cellspace} is defined \emph{a priori} for \texttt{tabular} environments,
% the new \texttt{x} and \texttt{y} column types, defined by \textsf{arraycols}, 
% produce a column in mathematical mode with good adjustment, 
% either in a \texttt{tabular} or in an \texttt{array} environment.
%
% Look at the following examples produced with
% *\begin{array}{|c|}* and with *\begin{array}{|x|}*.
% \[
% \begin{array}{|c|}
% \hline
% \text{bad}\\
% \hline
% \displaystyle\lim_{\substack{x \to 1\\x>1}} \ln\left(\dfrac{x^2}{x-1}\right) \\
% \hline
% \dfrac{a}{b} \\
% \hline
% \displaystyle\int_{1}^{X} \frac{1}{t}\,\mathrm{d} t  \\
% \hline
% \end{array}
% \qquad 
% \begin{array}{|x|}
% \hline
% \text{good}\\
% \hline
% \displaystyle\lim_{\substack{x \to 1\\x>1}} \ln\left(\dfrac{x^2}{x-1}\right) \\
% \hline
% \dfrac{a}{b} \\
% \hline
% \displaystyle\int_{1}^{X} \frac{1}{t}\,\mathrm{d} t  \\
% \hline
% \end{array}
% \]
%
% The \textsf{cellspace} package is loaded with the \texttt{math} option\footnote{The 
% \texttt{math} option loads the \textsf{amsmath} package.
% As mentionned in the \textsf{cellspace} package documentation:
% \og the \textsf{amsmath} package can be loaded beforehand with other
% packages (such as \texttt{empheq} or \texttt{mathtools}), 
% were an incompatibility to arise from one’s loading it later\fg.}
% for a good management of row heights in matrix tables.
% 
% \medskip
% Notice that another package, \textsf{booktabs}~\cite{BOOK}, also provides 
% an excellent adjustment of row heights, but unfortunately, it doesn't handle height 
% of vertical separators *|*.
% To get the same vertical adjustment as \textsf{booktabs}, 
% we set the \textsf{cellspace} parameters as follows:\\
% |\setlength{\cellspacetoplimit}{3pt}|, \\
% |\setlength{\cellspacebottomlimit}{2pt}|.
%
% We should also mention the \textsf{tabls} package by Donald Arneseau~\cite{TABLS}
% that makes a good adjustment of row heights as well, but it is incompatible 
% with the \textsf{array} and \textsf{numprint} packages.
%
% \medskip
% At last, it is also possible to make manual adjustments with the |\vstrut| command 
% from the \textsf{spacingtricks} package~\cite{SPA}, or |\gape| and |\Gape|
% from the \textsf{makecell} package~\cite{MKCELL}, 
% or |\bigstrut| from the \textsf{bigstrut} package~\cite{STRUT}.
%
% \medskip
% \DescribeEnv{z\marg{width}}
% The column type \texttt{z}\marg{width} enables to set the column width, 
% as \texttt{t}\marg{width}, 
% but also activates the math mode and adjusts the row height,
% as \texttt{x}.
%
% \medskip
%  \DescribeEnv{T}  \DescribeEnv{Z}
% The \textsf{tabularx} package by David Carlisle~\cite{TABX} provides
% the \texttt{X} column definition whose width is calculated according 
% to the required width for the whole table, and with left alignment
% as for \texttt{p}\marg{width}.
% *\begin{tabularx}{8cm}{|c|X|X|}* adjusts the width of the \texttt{X} columns 
% for a total witdh of the table equals to 8\,cm. As a complement, we propose 
% the \texttt{T} declaration, doing the same thing but with horizontal centering
% and \texttt{Z} which furthermore activates the mathematical mode and adjusts
% line heights (as \texttt{x} or \texttt{z}). Here is an example with
% *\begin{tabularx}{\linewidth}{|T|y|x|Z|T|}*.
% \begin{center}
% \begin{tabularx}{\linewidth}{|T|y|x|Z|T|}
% \hline
% A good job &
% \displaystyle\lim_{\substack{x \to 1\\x>1}} \ln\left(\dfrac{x^2}{x-1}\right) &
% \dfrac{a}{b} & 
% \dfrac{a}{b} + \displaystyle \int_{1}^{X} \frac{1}{t}\,\mathrm{d}t & 
% \makecell{a multiline \\ piece of text}\\
% \hline
% \end{tabularx} 
% \end{center}
% To keep the perfect aligment of fraction bars in mathematical formulas, 
% cells are not vertically centered, however, to get a proper vertical positioning 
% in the last cell, we have used the powerfull |\makecell| command of
% the \textsf{makecell} package by Olga Lapko~\cite{MKCELL}:
% |\makecell{a multiline \\ piece of text}|.
%
% \medskip
%  \DescribeEnv{I} \DescribeEnv{V\marg{thickness}}
% The column definition \texttt{I} is suggested in The \LaTeX\ Companion~\cite{COMP} 
% and enables to draw a thick vertical line (1\,pt thick) instead of the one obtained
% with standard declaration *|*. To choose thickness, we propose further column 
% definition \texttt{V}\marg{thickness}\footnote{The definition of \texttt{V}
% would have been simplified by using an optional argument for \texttt{I}
% but this way out is not working.}.
%
% \medskip
% \DescribeMacro{\whline}
% Likewise, the |\whline| command, proposed in The \LaTeX\ Companion, enables to draw a 
% thick horizontal line (1\,pt thick) instead of the one obtained with |\hline|
% and the \textsf{makecell} package provides further command |\Xhline|\marg{thickness}
% enabling to choose the thickness of the horizontal rule.
%
% \medskip
% The introduction table has been typeset with a column declaration \texttt{I} 
% as separator between the two columns of text, and with |\whline| for the horizontal rules
% at the begin and at the end of the table,
% and |\Xhline{0.8pt}| for the one following the legend rows.
% Formatting header lines has been done with |\thead| command from the \textsf{makecell}
% package. For this command \textsf{arraycols} sets by default:\\
% |\renewcommand\theadfont{\footnotesize\sffamily}| \\
% (originally |\footnotesize| only, without |\sffamily|).
% At last, according to a recommendation of the \textsf{array} package~\cite{ARRAY},
% 1\,pt has been added to the normal height of every row of this table, with the command
% |\setlength{\extrarowheight}{1pt}|\footnote{As mentioned 
% in the \textsf{array} package documentation: 
% \og This is important for tables with horizontal lines because those 
% lines normally touch the capital letters\fg.}.
%
% \StopEventually{}
%
% \section{Implementation}
%    \begin{macrocode}
\RequirePackage{array}
\RequirePackage[math]{cellspace}
\RequirePackage{tabularx} % must be loaded after cellspace
\RequirePackage{makecell}

\newcolumntype{C}{>{$}c<{$}}
\newcolumntype{L}{>{$}l<{$}}
\newcolumntype{R}{>{$}r<{$}}
\newcolumntype{t}[1]{>{\centering\arraybackslash}m{#1}}
%    \end{macrocode}
% The \textsf{cellspace} package provides the \texttt{S} modifier enabling, 
% when placed before a column declaration, to adjust the height of the content of the cells 
% to avoid to touch horizontal rules. Spacing between the content and the rules
% is controlled by the parameters
% |\cellspacetoplimit| and |\cellspacebottomlimit|.
% 
%    \begin{macrocode}
\newcolumntype{x}{>{$}Sc<{$}}
\newcolumntype{y}{>{$}Sl<{$}}
\setlength{\cellspacetoplimit}{3pt}
\setlength{\cellspacebottomlimit}{2pt}
\newcolumntype{z}[1]{>{$}S{>{\centering\arraybackslash}p{#1}}<{$}}
%    \end{macrocode}
% For the \texttt{z} definition of column, we use \texttt{p} and not \texttt{m}
% (which automatically centers) in order to keep a correct alignment for mathematical 
% expressions in the cells of a same row.
% \medskip
%    \begin{macrocode}
\newcolumntype{T}{>{\centering\arraybackslash}X}
\newcolumntype{Z}{>{$}ST<{$}}
%    \end{macrocode}
% The \texttt{T} columns are not automatically centered. It would be possible to do it 
% with the command |\renewcommand{\tabularxcolumn}[1]{m{#1}}|
% (with \texttt{m} instead of default value \texttt{p}), 
% but unfortunately this has a global effect for all the declarations of columns based 
% on \texttt{X}, so \texttt{T} but also \texttt{Z}, and this would lead to disturb
% alignment of mathematical expressions in the cells of a same row.
%    \begin{macrocode}
\newcolumntype{I}{!{\vrule width 1pt}}
\newcolumntype{V}[1]{!{\vrule width #1}}
\newlength\savedwidth
\newcommand{\whline}{%
  \noalign{\global\savedwidth\arrayrulewidth\global\arrayrulewidth 1pt}
  \hline 
  \noalign{\global\arrayrulewidth\savedwidth}
}
\renewcommand\theadfont{\footnotesize\sffamily}
%    \end{macrocode}
%
% \begin{thebibliography}{11}
% \bibitem{ARRAY} \emph{A new implementation of LATEX’s \textsf{tabular} and \textsf{array} 
% environment}, Frank Mittelbach, David Carlisle, CTAN, v2.4k revised 2018/12/30.
% \bibitem{CELLSP} \emph{The \textsf{cellspace} package}, Josselin Noirel, CTAN, 
% v1.8.1 2019/03/11. 
% \bibitem{TABX} \emph{The \textsf{tabularx} package}, David Carlisle, CTAN, v2.11.b 2016/02/03.
% \bibitem{MKCELL} \emph{The \textsf{makecell} package}, Olga Lapko, CTAN, v0.1e 2009/08/03.
% \bibitem{TABY} \emph{The \textsf{tabulary} package}, David Carlisle, CTAN, v1.10 2014/06/11.
% \bibitem{TBLSTY} \emph{The \textsf{tablestyles} package}, Matthias Pospiech, CTAN, 
% v0.1 2014/06/27.
% \bibitem{BOOK} \emph{Publication quality tables in \LaTeX}, package \textsf{booktabs} by
% Simon Fear, CTAN, v1.618033 2016/04/29.
% \bibitem{TABLS} \emph{The \textsf{tabls} package}, Donald Arseneau, CTAN, v3.5 2010/02/26.
% \bibitem{STRUT} \emph{The \textsf{multirow}, \textsf{bigstrut} and \textsf{bigdelim} packages},
% Piet van Oostrum, Øystein Bache, Jerry Leichter, CTAN, v2.4 2019/01/01.
% \bibitem{SPA} \emph{The \textsf{spacingtricks} package}, Antoine Missier, 
% CTAN, v1.0 2019/06/26.
% \bibitem{COMP} \emph{The \LaTeX\ Companion}. Frank Mittelbach, Michel Goossens, 
% Johannes Braams, David Carlisle, Chris Rowley, 2nd edition, Pearson Education, 2004.
% \end{thebibliography}

% \Finale
\endinput
