%%
%% ------------------------------------------
%% Copyright (C) 2003-2005 by Hendri Adriaens
%% ------------------------------------------
%%
%% This file may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License, either version 1.2 of this license
%% or (at your option) any later version.  The latest version of this
%% license is in:
%%
%%    http://www.latex-project.org/lppl.txt
%%
%% and version 1.2 or later is part of all distributions of LaTeX version
%% 1999/12/01 or later.
%%
%% Extra definitions for the HA-prosper documentation.
\usepackage{epic}
\usepackage{pstricks}
\usepackage{multicol}
\usepackage{array}
\usepackage{url}
\usepackage{pst-3d}
\usepackage{pst-node}
\SpecialCoor
\makeatletter
\renewenvironment{thebibliography}[1]
  {\renewcommand{\refname}{Links}\subsection{\refname}\label{sec:links}%
  This section contains addresses of websites which have been
  referred to in this documentation.\par
  \list{\@biblabel{\@arabic\c@enumiv}}%
    {\settowidth\labelwidth{\@biblabel{#1}}%
    \leftmargin\labelwidth
    \@openbib@code
    \usecounter{enumiv}%
    \let\p@enumiv\@empty
    \renewcommand\theenumiv{\@arabic\c@enumiv}}%
  \sloppy
  \clubpenalty4000
  \@clubpenalty \clubpenalty
  \widowpenalty4000%
  \sfcode`\.\@m}
  {\def\@noitemerr
  {\@latex@warning{Empty `thebibliography' environment}}%
  \endlist
}
\def\DescribeSwitch{\leavevmode\@bsphack\begingroup\MakePrivateLetters
  \Describe@Switch}
\def\Describe@Switch#1{\endgroup
              \marginpar{\raggedleft\PrintDescribeSwitch{#1}}%
              \SpecialSwitchIndex{#1}\@esphack\ignorespaces}
\def\PrintDescribeSwitch#1{\strut \MacroFont #1\ }
\def\SpecialSwitchIndex#1{\@bsphack
    \index{#1\actualchar{\protect\ttfamily#1}
           (switch)\encapchar usage}%
    \index{switches:\levelchar#1\actualchar{\protect\ttfamily#1}\encapchar
           usage}\@esphack}
\def\SpecialEnvIndex#1{\@bsphack
    \index{#1\actualchar{\protect\ttfamily#1}
           \encapchar usage}%
    \index{environments:\levelchar#1\actualchar{\protect\ttfamily#1}\encapchar
           usage}\@esphack}
\def\SpecialMainEnvIndex#1{\@bsphack\special@index{%
                                      #1\actualchar
                                      {\string\ttfamily\space#1}
                                      \encapchar main}%
    \special@index{environments:\levelchar#1\actualchar{%
                   \string\ttfamily\space#1}\encapchar
           main}\@esphack}
\makeatother
\def\HAPcmd#1{%
  \par\addvspace{1.2ex plus 0.8ex minus 0.2ex}%
  \vskip -\parskip\noindent
  \begin{tabular}{|l|}\hline\rule{0pt}{1em}%
  \ignorespaces#1\\\hline\end{tabular}%
  \par\nopagebreak\addvspace{1.2ex plus 0.8ex minus 0.2ex}%
  \vskip -\parskip
}
\def\pf#1{\textsf{#1}}
\newcolumntype{d}{l|l}
\newcolumntype{e}{l|l||l|l}
\newcolumntype{f}{c|c||c|c}
\newcolumntype{g}{|p{12em}|*{12}{p{1em}|}}
\def\bs{\symbol{'134}}
\def\ci#1{\texttt{\bs#1}}
\setlength{\parindent}{0cm}
\bibliographystyle{plain}
\endinput
