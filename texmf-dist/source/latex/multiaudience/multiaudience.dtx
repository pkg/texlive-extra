% \iffalse
% $Id: multiaudience.dtx,v 1.9 2021/10/02 22:21:51 boris Exp $
%
%% Copyright 2015-2021, Boris Veytsman <borisv@lk.net
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any 
%% later version.
%% The latest version of the license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of
%% LaTeX version 2003/06/01 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainer of this work is Boris Veytsman
%%
%    \begin{macrocode}
%<style>\NeedsTeXFormat{LaTeX2e}
%<*gobble>
\ProvidesFile{multiaudience.dtx}
%</gobble>
%<style>\ProvidesPackage{multiaudience}
%<*style>
[2021/10/02 v1.04 Generating versions for different audiences from the
same source]
%    \end{macrocode}
%</style>
%<*gobble>
% \fi
% \CheckSum{85}
%
%
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~} 
%
%\iffalse
%    \begin{macrocode}
\documentclass{ltxdoc}
\usepackage{hypdoc}
\PageIndex
\CodelineIndex
\RecordChanges
\EnableCrossrefs
\begin{document}
  \DocInput{multiaudience.dtx}
\end{document}
%    \end{macrocode}
%</gobble> 
% \fi
% \MakeShortVerb{|}
% \GetFileInfo{multiaudience.dtx}
% 
% 
% \title{Generating multiple versions of a document for different
% audiences from the same source\thanks{\copyright 2015 Boris Veytsman}
% \thanks{This package was commissined by Neadwerx, \url{http://www.neadwerx.com/}}}
% \author{Boris Veytsman \thanks{borisv@lk.net}}
% \date{\filedate, \fileversion}
% \maketitle
%
% \begin{abstract}
%   The \textsl{multiaudience} package allows to generate several
%   versions of the same document for different audiences
% \end{abstract}
%
% \tableofcontents
%
% \clearpage
%
%\section{Introduction}
%\label{sec:intro}
% 
% Suppose you want to present the same paper, say, software design
% document, to software developers and executives.  You expect that
% your intended readers have different interests and expectations:
% some paragraphs will be interesting only to the developers, while
% some---only to the executives.  On the other hand, some paragraphs
% will be interesting to both.  Basically you want two different
% versions of the document, one for each audience.  Another example
% may be the situation where you need a public and a confidential
% versions of the same document (or even classified and unclassified
% ones).
%
% Of course standard version controls tools like \textsl{subversion},
% \textsl{git} or \textsl{CVS} allow you to create branches of your
% document, but in many cases this is not a good solution.  If the
% versions have many paragraphs in common, you will find yourself
% constantly merging and branching your revisions.  
%
% The package \textsl{multiaudience} offers an alternative solution to
% the problem.  You create a \LaTeX\ document, clearly marking some
% parts for different audiences.  When you process the document, you
% indicate, for which audience the current output is intended.  The
% corresponding parts will be chosen.  
%
%
%\section{User's guide}
%\label{sec:ug}
%
%
%\subsection{Invocation}
%\label{sec:ug_invocation}
%
% \DescribeMacro{\CurrentAudience}%
% \DescribeMacro{\DefCurrentAudience}%
% To use the package you need first to define the macro
% \cs{CurrentAudience}.  There are two ways to do it: a \LaTeX-ish
% macro \cs{DefCurrentAudience}\marg{audience}
% \begin{verbatim}
% \DefCurrentAudience{execs}
% \end{verbatim}
%  or \TeX-ish \cs{def} primitive:
% \begin{verbatim}
% \def\CurrentAudience{execs}
% \end{verbatim}
%  You may set this parameter outside the document itself like this:
% \begin{verbatim}
% pdflatex "\def\CurrentAudience{execs}\input{file}"
% \end{verbatim}
%  This trick allows one to generate all versions of output from the
%  command line:
% \begin{verbatim}
% pdflatex -jobname file-execs "\def\CurrentAudience{execs}\input{file}"
% pdflatex -jobname file-devs "\def\CurrentAudience{devs}\input{file}"
% ...
% \end{verbatim}
%
% Then put in the preamble of your document \emph{after} defining the
% current audience 
% \begin{verbatim}
% \usepackage{multiaudience}
% \end{verbatim}
%  
% \DescribeMacro{\SetNewAudience}%
%  After calling the package you need
% to list all possible audience using the command
% \cs{SetNewAudience}\marg{audience}, for example,
% \begin{verbatim}
% \SetNewAudience{execs}
% \SetNewAudience{devs}
% \SetNewAudience{admins}
% \end{verbatim}
% 
% Now you can mark the portions of your document, for example,
% \begin{verbatim}
% \begin{shownto}{execs, admins}
%   Text for execs and admins
% \end{shownto}
% \begin{shownto}{-, execs}
%   Text for everybody but execs
% \end{shownto}
% \end{verbatim}
% 
%
%\subsection{Visibility scopes}
%\label{sec:scopes}
%
% The package contains a number of commands and environments, that
% mark certain portions of document for different audiences.  They all
% have as an argument an \marg{audiences} list.  It is a
% comma-separated list of audiences (defined by the command
% \cs{SetNewAudience}, see Section~\ref{sec:ug_invocation}),
% optionally starting with the keyword ``-''.  This argument defines
% the visibility scope of the command or environment.  
%
% The rules are:
% \begin{enumerate}
% \item If the list \marg{audiences} does \emph{not} start with the
% keyword ``-'', it defines the audiences, which will see the
% corresponding content.  All other audiences will not see it.  For
% example, the command
% \begin{verbatim}
% \showto{execs, devs}{TEXT}
% \end{verbatim}
%  makes TEXT visible only to execs and devs, but not to anybody
%  else among listed audiences.
% \item If the list \marg{audience} starts with the keyword ``-'', the
% logic is inverted: the audiences from the list will \emph{not} see
% the contents.  For example, the command
% \begin{verbatim}
% \showto{-, execs, devs}{TEXT}
% \end{verbatim}
% makes the text not visible to execs and devs.  
% \end{enumerate}
% 
% The scopes can be nested.  For example, in the following situation
% TEXT~1 will be visible only to devs and execs, while TEXT~2 only to
% devs.   
% \begin{verbatim}
% \begin{shownto}{devs,execs}
%   TEXT 1
%   \begin{shownto}{devs}
%     TEXT 2
%   \end{shownto}
% \end{shownto}
% \end{verbatim}
% There is, however, an important limitation: by nesting scopes you
% can hide a part of the document, but you cannot uncover it if it is
% hidden for the current audience by the enclosing scope.  In other
% hands, the following makes TEXT~2 invisible to everybody:
% \begin{verbatim}
% \begin{shownto}{execs}
%   TEXT 1
%   \begin{shownto}{devs}
%     TEXT 2
%   \end{shownto}
% \end{shownto}
% \end{verbatim}
% 
%
%\subsection{Commands and environments}
%\label{sec:ug_commands}
%
% \DescribeMacro{\showto}%
% The command \cs{showto}\marg{audience}\marg{contents} makes the
% \marg{contents} visible (or invisible) to the current audience
% according to the first argument, as describes in
% Section~\ref{sec:ug_invocation}.  
%
% \DescribeMacro{\Footnote}%
% The macro \cs{Footnote}\marg{audience}\marg{footnote} creates a
% footnote with the visibility determined by the \marg{audience}
% argument.  
%
% \DescribeEnv{shownto}%
% The environment
% \cs{begin\{shownto\}}\marg{audience}\ldots\cs{end\{shownto\}}
% makes the contents visible or invisible according to its argument.  
%
%
% \DescribeEnv{Section}%
% \DescribeEnv{Subsection}%
% \DescribeEnv{Subsubsection}%
% \DescribeEnv{Paragraph}%
% \DescribeEnv{Subparagraph}%
%  Environments |Section|, |Subsection|, |Subsubsection|, |Paragraph|,
%  |Subparagraph| define section-like units with limited visibility.
%  They have the structure like
%  \cs{begin\{Section\}}\marg{audience}\oarg{short title}\marg{long
%  title}\ldots\cs{end\{Section\}}, foe example
% \begin{verbatim}
% \begin{Section}{execs}[Executive comments]{Executive comments for
% the document}
%   Text
% \end{Section}
% \end{verbatim}
%  
%
%\subsection{Verbatim text}
%\label{sec:ug_verbatim}
%
% Due to the way verbatim is implemented in \LaTeX, it is currently
% impossile to directly put \cs{verb} and \texttt{verbatim} inside
% visibility scopes.  There are several workarounds shown in the
% example document:
% \begin{enumerate}
% \item You can put parts of the text with verbatim constructions in
% separate files and \cs{input} them inside visibility scopes.
% \item You can use \cs{SaveVerbatim} and \cs{UseVerbatim} from
% \textsl{fancyvrb} package.
% \item The command \cs{path} from \textsl{hyperref} package can be
% used for short verbatim-like fragments.
% \end{enumerate}
% 
%
%\subsection{Extensions}
%\label{sec:ug_extension}
%
% \DescribeMacro{\DefMultiaudienceCommand}%
% The macro \cs{DefMultiaudienceCommand}\marg{new command}\marg{old
%   command} can add visibility argument to \marg{old command}. It is
% assumed that \marg{old command} has only one argument.  For example,
% \cs{Footnote} command in Section~\ref{sec:ug_commands} was defined
% as
% \begin{verbatim}
% \DefMultiaudienceCommand{\Footnote}{\footnote}
% \end{verbatim}
% 
% \DescribeMacro{\NewMultiaudienceSectionEnv}%
% Similarly a section-like command \marg{command} can be converted
% into an environment with explicit visibility scope with the command
% \cs{NewMultiaudienceSectionEnv}\penalty0\marg{environment}\marg{command}.  For
% example, \texttt{Section} environment is defined as
% \begin{verbatim}
% \NewMultiaudienceSectionEnv{Section}{\section}
% \end{verbatim}
%
% \subsection{Combined audiences}
% \label{ug:combinations}
%
% Sometimes one wants to create a document for several audiences.  You
% can do this using comma-separated lists in \cs{CurrentAudience}, for
% example,
% \begin{verbatim}
% \DefCurrentAudience{execs,admins}
% \end{verbatim}
% 
%
% \emph{Important:} there should be no spaces in the definition.  The
% definition |\DefCurrentAudience{execs, admins}| will \emph{not} work!
%
% The resulting audience is the union of the audiences in the
% definition. Thus a fragment is included if either:
% \begin{enumerate}
% \item The list of audiences for the fragment does not start with
%   |-|, and at least one of the current audiences is mentioned in the
%   list.
% \item The list of audiences for the fragment starts with |-|, and
% none of the current audiences is mentioned in the list.
% \end{enumerate}
% For example, suppose the current audience is |execs,admins|.  Then
% the following is true:
% \begin{enumerate}
% \item |\showto{devs}{...}|---excluded.
% \item |\showto{devs,admins}{...}|---included.
% \item |\showto{-, devs}{...}|---included.
% \item |\showto{-, devs, admins}{...}|---excluded.
% \end{enumerate}
% 
%
%\StopEventually{\clearpage}
%
% \clearpage
% 
% \section{Implementation}
% \label{sec:implementation}
% 
%    \begin{macrocode}
%<*style>
%    \end{macrocode}
%
%
%\subsection{Switches and defaults}
%\label{sec:switches}
%
% \begin{macro}{\CurrentAudience}
%   The default for \cs{CurrentAudience}
%    \begin{macrocode}
\providecommand*\CurrentAudience{default}
%    \end{macrocode}
%   
% \end{macro}
%
% \begin{macro}{\DefCurrentAudience}
% \changes{v1.03}{2015/08/02}{Added macro}
%   A little sugar around \cs{def}:
%    \begin{macrocode}
\def\DefCurrentAudience#1{\def\CurrentAudience{#1}}
%    \end{macrocode}
%   
% \end{macro}
%
% \begin{macro}{\if@MULTAU@shown}
% \changes{v1.01}{2015/07/11}{Renamed}
%   The main switch
%    \begin{macrocode}
\newif\if@MULTAU@shown
\@MULTAU@showntrue
%    \end{macrocode}
%   
% \end{macro}
%
% \begin{macro}{\if@MULTAU@include}
%   Whether the current command is ``include'' or ``exclude'' type
%   (i.e. whether it does \emph{not} start with ``-'')
%    \begin{macrocode}
\newif\if@MULTAU@include
\@MULTAU@includetrue
%    \end{macrocode}
%   
% \end{macro}
%
%
%\subsection{Key-value interface}
%\label{sec:keyval}
%
% We use |xkeyval|
%    \begin{macrocode}
\RequirePackage{xkeyval}
%    \end{macrocode}
% 
% \begin{macro}{\KV@MULTAU@-}
%   Normally we evaluate visibility according to the following algorithm:
%   \begin{enumerate}
%   \item Set visibility to FALSE.
%   \item If found current audience in the list, set visibility to
%   TRUE. 
%   \end{enumerate}
%   The presense of ``-'' keys inverts the algorithm:
%   \begin{enumerate}
%   \item Set visibility to TRUE.
%   \item If found current audience in the list, set visibility to
%   FALSE. 
%   \end{enumerate}
%    \begin{macrocode}
\define@key{MULTAU}{-}[]{\@MULTAU@showntrue\@MULTAU@includefalse}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\SetNewAudience}
% \changes{v1.04}{2021/10/02}{Added facility for a list of current
% audiences} 
%   Definition of a new audience.  We create a new key that switches
%   visibility on or off.
%    \begin{macrocode}
\def\SetNewAudience#1{%
  \define@key{MULTAU}{#1}[]{%
    \def\@MULTAU@currkey{#1}%
    \@for\@tempa:=\CurrentAudience \do{%
      \ifx\@MULTAU@currkey\@tempa\relax
        \if@MULTAU@include
           \@MULTAU@showntrue
        \else
           \@MULTAU@shownfalse
        \fi%
    \fi%
}}}
%    \end{macrocode}
%   
% \end{macro}
%
%
%\subsection{Main command}
%\label{sec:main_command}
%
% \begin{macro}{\showto}
% \changes{v1.01}{2015/07/11}{Renamed}
%   The macro \cs{showto} is the heart of the package.  Note that we
%   cannot put the inside into a group since we want it to be
%   transparent for things like counters.
%    \begin{macrocode}
\long\def\showto#1#2{\@MULTAU@shownfalse\@MULTAU@includetrue
  \setkeys{MULTAU}{#1}%
  \if@MULTAU@shown#2\fi
  \@MULTAU@showntrue\@MULTAU@includetrue}
%    \end{macrocode}
%   
% \end{macro}
%
%
%
%\subsection{Main environment}
%\label{sec:main_env}
%
% We need the macros from the |environ| package
%    \begin{macrocode}
\RequirePackage{environ}
%    \end{macrocode}
% 
%
% \begin{macro}{\shownto}
% \changes{v1.01}{2015/07/11}{Renamed}
%   And the environment
%    \begin{macrocode}
\NewEnviron{shownto}[1]{%
  \@MULTAU@shownfalse\@MULTAU@includetrue
  \setkeys{MULTAU}{#1}%
  \if@MULTAU@shown\BODY\fi}{\@MULTAU@showntrue\@MULTAU@includetrue} 
%    \end{macrocode}
%   
% \end{macro}
%
% 
%
%\subsection{Extensions}
%\label{sec:extensions}
%
% \begin{macro}{\DefVisibilityCommand}
%   Create a new visibility command
% \changes{v1.01}{2015/07/11}{Renamed}
%    \begin{macrocode}
\def\DefMultiaudienceCommand#1#2{\long\def#1##1##2{\showto{##1}{#2{##2}}}}
%    \end{macrocode}
%   
% \end{macro}
%
% \begin{macro}{\Footnote}
%   Special footnote command:
%    \begin{macrocode}
\DefMultiaudienceCommand{\Footnote}{\footnote}
%    \end{macrocode}
%   
% \end{macro}
%
% \begin{macro}{\NewMultiaudienceSectionEnv2}
% \changes{v1.01}{2015/07/11}{Renamed}
%   Create a new visibility section environment
%    \begin{macrocode}
\def\NewMultiaudienceSectionEnv#1#2{%
  \NewEnviron{#1}[1]{%
    \@MULTAU@shownfalse\@MULTAU@includetrue
    \setkeys{MULTAU}{##1}%
    \if@MULTAU@shown\expandafter#2\BODY\fi}%
  {\@MULTAU@showntrue\@MULTAU@includetrue}}
%    \end{macrocode}   
% \end{macro}
%
%
% \begin{macro}{Section}
% \begin{macro}{Subsection}
% \begin{macro}{Subsubsection}
% \begin{macro}{Paragraph}
% \begin{macro}{Subparagraph}
%   A bunch of section-like commands
%    \begin{macrocode}
\NewMultiaudienceSectionEnv{Section}{\section}
\NewMultiaudienceSectionEnv{Subsection}{\subsection}
\NewMultiaudienceSectionEnv{Subsubsection}{\subsubsection}
\NewMultiaudienceSectionEnv{Paragraph}{\paragraph}
\NewMultiaudienceSectionEnv{Subparagraph}{\subparagraph}
%    \end{macrocode}
%   
% \end{macro}
% \end{macro}
% \end{macro}
% \end{macro}
% \end{macro}
%
%    \begin{macrocode}
%</style>
%    \end{macrocode}
%\Finale
%\clearpage
%
%\PrintChanges
%\clearpage
%\PrintIndex
%
\endinput
