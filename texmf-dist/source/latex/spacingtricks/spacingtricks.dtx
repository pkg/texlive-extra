% \iffalse meta-comment
%
% Copyright (C) 2019-2021 by Antoine Missier <antoine.missier@ac-toulouse.fr>
%
% This file may be distributed and/or modified under the conditions of
% the LaTeX Project Public License, either version 1.3 of this license
% or (at your option) any later version.  The latest version of this
% license is in:
%
%   http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of LaTeX version
% 2005/12/01 or later.
% \fi
%
% \iffalse
%<*driver>
\ProvidesFile{spacingtricks.dtx}
%</driver>
%<*package> 
\NeedsTeXFormat{LaTeX2e}[2005/12/01]
\ProvidesPackage{spacingtricks}
    [2021/09/20 v1.4 .dtx spacingtricks file]
%</package>
%<*driver>
\documentclass{ltxdoc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french,english]{babel}
\usepackage{lmodern}
\usepackage{spacingtricks}
\usepackage{pifont}
\usepackage{amsmath}
\usepackage{pstricks-add}
\DisableCrossrefs
%\CodelineIndex
%\RecordChanges
\usepackage{hyperref}
\hypersetup{%
    colorlinks,
    linkcolor=blue,
    citecolor=blue,
    pdftitle={spacingtricks},
    pdfsubject={LaTeX package},
    pdfauthor={Antoine Missier}
}
\begin{document}
\DeleteShortVerb{\|}
\MakeShortVerb{\"}
\DocInput{spacingtricks.dtx}
%\PrintChanges
%\PrintIndex
\end{document}
%</driver>
% \fi
%
% \CheckSum{165}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
%
% \changes{v0.1}{2011/12/27}{Initial version}
% \changes{v1.0}{2019/05/04}{dtx and ins files, added the footnote command}
% \changes{v1.0}{2019/06/16}{English translation of the documentation}
% \changes{v1.1}{2019/09/06}{No automatic line breaking at the end of the centered command,
% new length compactitemlist}
% \changes{v1.2}{2019/09/09}{A bug correction in the last update}
% \changes{v1.3}{2020/11/02}{Loading the setspace package, 
% small changes in compactlist environment, improvement in dualboxes macro}
% \changes{v1.4}{2021/09/21}{A mistake in the documentation is now corrected}
%
% \GetFileInfo{spacingtricks.sty}
%
% \title{The \texttt{spacingtricks} package\thanks{This document
% corresponds to \textsf{spacingtricks}~\fileversion, dated \filedate.}}
% \author{Antoine Missier \\ \texttt{antoine.missier@ac-toulouse.fr}}
% \date{September 20, 2021}
% \maketitle
%
% \section{Introduction}
%
% This package offers some macros to deal with spacing issues. Thus:
% \begin{compactlist}
% \item "\centered" yields good horizontal centering without vertical spacing;
% \item "\footnote" has been redefined to avoid unsuitable spacing;
% \item "\vstrut" produces a strut with variable height or depth;
% \item "\indent" has been redefined to indent a line at the beginning 
% of a particular paragraph even if "\parindent" has been set to 0;
% \item the "indentblock" environment produces indentation of all its content;
% \item the "compactlist" environment yields a compact list, without vertical spacing
% between the items, like here;
% several aliases are provided to type some list symbols shorter:
% "\bul", "\dash", "\ddash", "\aster", "\hand", "\checksymb", "\arrowsymb";
% \item the macros "\ie" and "\eg" attends to typeset common abbreviations
% \ie and \eg with correct spacing;
% \item the "\dualboxes" command attends to place two boxes 
% (figures, tables, text) side by side by adjusting the vertical positioning.
% \end{compactlist}
% 
% \medskip
% Two other common packages are loaded by \textsf{spacingtricks}:
% \textsf{setspace} (natively in \LaTeXe), 
% for setting line spacing in a piece of text 
% (with the \texttt{spacing} environment), and
% \textsf{xspace}~\cite{XSP}, which adds an inter-word space unless the macro 
% is followed by a punctuation character.
%
% Otherwise, we provide the package \textsf{arraycols}~\cite{ARCOL},
% which allows a good management of spacing in \texttt{tabular} 
% and \texttt{array} environments,
% and \textsf{mismath}~\cite{MSMATH} of which several macros tends to improve spacing 
% in mathematical formulas.
%
% \section{Usage}
%
% \DescribeMacro{\centered}
% The "\centered"\marg{text} command yields a centered line without vertical spacing.
% It acts like "\centerline" except in lists or tables where its behavior is much better
% (see the following examples).
% Moreover, the line break before (but not after) the macro is automatic.
%
% Here is a comparative example of the centering commands inside a list:
% \begin{enumerate}
% \item Here a centered line with "\centered": 
% \centered{Lorem ipsum dolor sit amet, consectetuer adipiscing elit.}
% \item Here another centered line with "\\ \centerline":\\
% \centerline{Lorem ipsum dolor sit amet, consectetuer adipiscing elit.}
% \item Here another centered line with "\par\centerline":
% \par\centerline{Lorem ipsum dolor sit amet, consectetuer adipiscing elit.}
% \item Here a centered line with the \texttt{center} environment:
% \begin{center} Lorem ipsum dolor sit amet, consectetuer adipiscing elit. \end{center}
% \end{enumerate}
%
% In tables, "\centered" allows to center a particular cell independently of 
% the (general) column alignment
% \footnote{In tables, we can also use the powerful \texttt{\bslash makecell} command
% of the \textsf{makecell} package~\cite{MKCELL}, on the other hand
% the \texttt{\bslash centerline} command doesn't work for a single line in a cell.
% Let us also mention the \texttt{\bslash centeredline} command, from the package
% \textsf{centeredline}~\cite{CENTER}, which allows to use \texttt{\bslash verb}
% commands inside the text to center; but it doesn't work in tables either.}.
%
% \begin{center}
% \begin{tabular}{|l|r|}
% \hline
% left aligned column & right aligned column \\
% \hline
% another cell  & \centered{centered cell} \\
% \hline
% \centered{centered line} & the last cell right aligned \\
% \hline
% \end{tabular}
% \end{center}
%
% \DescribeMacro{\footnote}
% The "\footnote" command doesn't have a good management of spacing issues,
% in particular when the \textsf{hyperref} package has been loaded.
% In English tradition, there is no space before numbers (or symbols) of note calls,
% and likewise at the beginning of footnotes,
% text begins immediately after the note number
% \footnote{The typesetting of footnotes and note calls depends on national typographic
% rules which are, in principle, managed by \textsf{babel}.
% For instance, by activating the \texttt{french} option of \textsf{babel},
% a thin space is added before the note calls, and the new \texttt{\bslash footnote}
% macro does not alter this behavior.}.
% To avoid undesirable spaces, we don't have to put some space before 
% or after writing "\footnote{", for instance:
% \begin{center}
% "this is a note\footnote{good spacing} which works fine,"
% \end{center}
% but sometimes, it is convenient to place the "\footnote" command
% on a new line. To achieve this, "\footnote" has been redefined to completely eliminate
% unwanted spaces
% \footnote{Probably it would have been enough to recommend the use of the \% symbol
% at the end of line; its effect is to cancel the space produced by a line break,
% but we do not always think of using it.}.
%
% \pagebreak
% \medskip
% \noindent\begin{minipage}{5.6cm}
% \begin{verbatim}
%This a note 
%\footnote{ 
%Bad spacing example.}
%with the old command.
% \end{verbatim}
% \vspace{-4ex} This a note
% \footnt{
% Bad spacing example.}
% with the old command.
% \end{minipage}
% \hfill 
% \begin{minipage}{5.6cm}
% \begin{verbatim}
%This a note
%\footnote{ 
%Good spacing example.}
%with the new command.
% \end{verbatim}
% \vspace{-4ex} This a note 
% \footnote{ Good spacing example.}
% with the new command.
% \end{minipage}
%
% \medskip
% \DescribeMacro{\footnotespace} \DescribeMacro{\footenoteindent}
% Like the old one, the new "\footnote" command can take an optional argument
% to force the number of the note.
% Likewise, we have always the customization macros 
% "\footnotesize", "\footnotesep", "\footnoterule",
% but two new macros have been added to manage spacing:
% "\footnotespace" produces the space before the note call symbol
% and "\footnoteindent" produces the space at the beginning of the footnote text.
% For instance, with "\renewcommand{\footnotespace}{\,}" 
% and "\renewcommand{\footnoteindent}{\enskip}"
% \footnote{\texttt{\bslash enskip} is equivalent to \texttt{\bslash hspace\{0.5em\}}.}
% we get:
% \renewcommand{\footnotespace}{\,}
% \renewcommand{\footnoteindent}{\enskip}
%
% \begin{center}
% \begin{minipage}{7cm}
% This is a note \footnote{en dash spacing at the beginning of the note.}  
% with particular space settings.
% \end{minipage}
% \end{center}
% \renewcommand{\footnotespace}{}
% \renewcommand{\footnoteindent}{}
% 
% \DescribeMacro{\vstrut} 
% "\vstrut"\oarg{depth}\marg{height} produces a strut with variable height or depth,
% in order to increase the line's height (above the base line) or depth 
% (below the base line, optional);
% this command can be used in a text line, a table, a list, a formula, etc.
% If the values of \meta{height} and \meta{depth} are inferior to the height and depth
% of the current line, the command has no effect.
% Here are some examples.
%
% \noindent
% \begin{minipage}[t]{8cm}
% \begin{verbatim}
%\[\frac{\sqrt{0.5p}}{10} = 
%\frac{\sqrt{\vstrut{1.7ex} 0.5p}}{10}\]
% \end{verbatim}
% \end{minipage}
% \hfill 
% \begin{minipage}[t]{6cm}
% \[\frac{\sqrt{0.5p}}{10} = \frac{\sqrt{\vstrut{1.7ex} 0.5p}}{10}\]
% \end{minipage}
% \\
% "\fbox{\vstrut{2ex}$\sigma(X)=1$}" gives
% \fbox{\vstrut{2ex}$\sigma(X)=1$}
% better than \fbox{$\sigma(X)=1$}
% 
% \medskip
% \noindent
% The height adjustment is done by trial and error.
% We could also have used a vertical phantom box;
% for example in the previous square root, we get a good result with "\vphantom{\bar{t}}",
% but it's not obvious to know what to put in the phantom box,
% moreover, "\vstrut" allows a finer tuning.
%
% In a table,
% "\renewcommand{\arraystretch}"\marg{stretch}
% allows to increase the height of the rows but this command
% has a global effect, whereas "\vstrut" allows to adjust properly the height of each row, 
% as in the following table:
% \[  
% \begin{array}{|c|}
% \hline
% \text{bad}\\
% \hline
% \displaystyle \lim_{\substack{x \to 1\\x>1}} \ln\left(\dfrac{x^2}{x-1}\right) \\
% \hline
% \dfrac{a}{b} \\
% \hline
% \displaystyle \int_{1}^{X} \frac{1}{t}\, \mathrm{d}t  \\
% \hline
% \end{array}
% \qquad
% \begin{array}{|c|c|}
% \hline
% \multicolumn{2}{|c|}{\text{good}}\\
% \hline
% \displaystyle\lim_{\substack{x \to 1\\x>1}} \ln\left(\dfrac{x^2}{x-1}\right)\vstrut{3.8ex}
% & \mbox{obtained with } "\vstrut{3.8ex}" \\
% \hline
% \dfrac{a}{b} \vstrut[2ex]{3ex} & "\vstrut[2ex]{3ex}" \\
% \hline
% \displaystyle\int_{1}^{X} \frac{1}{t} \, \mathrm{d} t \vstrut[2.5ex]{4.2ex}
% & "\vstrut[2.5ex]{4.2ex}" \\
% \hline
% \end{array}
%\]
% However, for tables, we have the \textsf{arraycols} package~\cite{ARCOL},
% based on \textsf{cellspace}~\cite{CELLSP}, which allows to adjust
% row heights automatically. Nevertheless, "\vstrut" can be useful for fine adjustments. 
%
% In a text line, "\vstrut" can be used in place of "\vspace".
%
% \medskip
% \DescribeMacro{\indent} \DescribeMacro{\parindentlength} 
% The command "\setlength{\parindent}{0cm}" allow to eliminate any indentation of lines
% at the beginning of every paragraph. 
% But in this case, the "\indent" command does not work anymore
% if we want exceptional indentation of a particular paragraph.
% So, the initial length of "\parindent" has been saved in "\parindentlength"
% and the command "\indent" has been redefined to still allow indentation
% of length "\parindentlength".
%
% \medskip
% \DescribeEnv{indentblock}
% The \texttt{indentblock} environment allows indentation of a whole block of lines.
% It has an optional argument which is the length of indentation
% (set by default to "\parindentlength").
% The following lyrics have been indented (and typeset in italic shape) with
% "\begin{indentblock}\itshape" and stanzas 2 and 4 have been affected by
% an additional indentation with "\begin{indentblock}[3em]".
%
% \begin{indentblock}\itshape
% Overhead the albatross hangs motionless upon the air \\
% And deep beneath the rolling waves in labyrinths of coral caves \\
% The echo of a distant time comes willowing across the sand \\
% And everything is green and submarine 
% \begin{indentblock}[3em]
%    And no one showed us to the land \\
%    And no one knows the where's or why's \\
%    But something stirs and something tries \\
%    Starts to climb towards the light
% \end{indentblock}
% \noindent
% Strangers passing in the street \\
% By chance two separate glances meet \\
% And I am you and what I see is me \\
% And do I take you by the hand \\
% And lead you through the land \\
% And help me understand the best I can?
% \begin{indentblock}[3em]
%    And no one calls us to move on \\
%    And no one forces down our eyes \\
%    No one speaks and no one tries \\
%    No one flies around the sun
% \end{indentblock}
% \end{indentblock}
% 
% \DescribeEnv{compactlist}
% As its name tells it, the "compactlist" environment allows to create
% a \og compact\fg\ list, \ie without vertical space neither above nor between items.
% As for lists in \LaTeX, items are generated by the "\item" command.
% The environment has an optional argument: "\begin{compactlist}"\oarg{symbol}.
% 
% \DescribeMacro{\bul}\DescribeMacro{\dash}\DescribeMacro{\ddash}\DescribeMacro{\aster}
% \DescribeMacro{\hand}
% Default item symbol is "\textbullet" but it can be changed.
% We provide aliases for several symbols commonly used in lists:
% "\bul" \bul\ (alias for "\textbullet"), "\dash" \dash\ ("\textendash"), 
% "\ddash" \ddash\ ("\textemdash"), "\aster" \aster\ ("\textasteriskcentered"),
% as well as "\hand" \hand ("\ding{43}"), "\checksymb" \checksymb ("\ding{51}")
% and "\arrowsymb" \arrowsymb ("\ding{226}")
% which need to load the \textsf{pifont} package in the preamble.
% \DescribeMacro{\checksymb}\DescribeMacro{\arrowsymb}
% The following example is obtained with "\begin{compactlist}[\checksymb]":
% \begin{compactlist}[\checksymb]
% \item First item.
% \item Second item.
% \item Third item.
% \end{compactlist}
% \hand These aliases can be used directly in text mode, of course.
% For "\hand", "\checksymb" and "\arrowsymb", the symbol is followed by a space
% if there is no punctuation character just after it (thanks to the 
% macro \texttt{xspace} from the \textsf{xspace} package~\cite{XSP}).
%
% \medskip
% \DescribeMacro{\compactlistindent}
% This length (fixed at 0.5\,em by default) can be modified with "\setlength" to increase 
% or decrease the indentation of the \texttt{compactlist} environment.
% Notice that there are several other ways to construct a compact list in particular
% with the \texttt{noitemsep} key of the \textsf{enumitem} package~\cite{ENUM}.
%
% \medskip
% \DescribeMacro{\ie} \DescribeMacro{\eg}
% In English, at the end of a sentence, the point is followed by an em space
% which is larger than an inter-word space.
% We provide the "\ie" (\textit{id est}) and "\eg" (\textit{exempli gratia}) macros,
% suggested in The \LaTeX\ Companion \cite{COMP},
% to get correct spacing after these abbreviations \eg here.
% In American typography, a comma is often placed after these abbreviations,
% what we can get with "\ie,"
% on the other hand, some authors prefer to typeset \textit{\ie} in italic shape,
% which is always possible with "\textit{\ie}". 
%
% \medskip
% \DescribeMacro{\dualboxes}
% Several packages intend to set the text around a figure or a table,
% but in general we have to give the width of the box containing the figure or the table.
% Let us mention however the \textsf{picins} package~\cite{PICINS},
% cited in The \LaTeX\ Companion~\cite{COMP}, which do not ask for the box width
% and it can also be used with lists.
% Nevertheless vertical positioning can be tricky.
% For this purpose, we have written the "\dualboxes"\oarg{pos}\marg{left}\marg{right} macro,
% which places two boxes, \meta{left} and \meta{right}, side by side.
% These boxes can contain figures, tables, text, \texttt{minipage} environments
% (for several paragraphs and lists), etc.
% The optional \meta{pos} parameter sets the vertical level on which the boxes are aligned:
% a number between 0 (bottom) and 1 (top, default value).
% Here is a first example with "\dualboxes[0.65]".
%
% \medskip
% \psset{xunit=0.5cm,yunit=1.0cm,algebraic=true}
% \dualboxes[0.65]{\footnotesize
%    $\begin{array}{|*{7}{c|}} \hline
%        x    & -2    & -1    & 0 & 1    & 2    & 3    \\ \hline
%        f(x) & -0.96 & -0.71 & 0 & 0.59 & 0.38 & 0.18 \\  \hline
%    \end{array}$
%    }{
%    \begin{pspicture}(-4.5,-1.5)(4.5,1)
%        \psaxes[labelFontSize=\scriptstyle,ticksize=-2pt,linewidth=0.3pt]
%            {->}(0,0)(-4.5,-1.5)(4.5,1)
%        \psplot[linecolor=red]{-4.5}{4.5}{x/(EXP(x)-x)}
%    \end{pspicture}
% } 
%
% \noindent
% The horizontal space is equally shared between left margin, inter-box space and right margin.
% 
% \medskip
% In the following example the right box has been shifted back 
% to make an overlapping with the left one, what \textsf{picins} cannot do. 
% 
%\dualboxes{\footnotesize
%    $\begin{array}{|*{7}{c|}} \hline
%        x    & -2    & -1    & 0 & 1    & 2    & 3    \\ \hline
%        f(x) & -0.96 & -0.71 & 0 & 0.59 & 0.38 & 0.18 \\  \hline
%    \end{array}$
%    }{\mbox{} \hspace{-3cm}
%    \begin{pspicture}(-4.5,-1.5)(4.5,1)
%        \psaxes[labelFontSize=\scriptstyle,ticksize=-2pt,linewidth=0.3pt]
%            {->}(0,0)(-4.5,-1.5)(4.5,1)
%        \psplot[linecolor=red]{-4.5}{4.5}{x/(EXP(x)-x)}
%    \end{pspicture}
%}
% \begin{verbatim}
% \dualboxes{\footnotesize
%    $\begin{array}{|*{7}{c|}} \hline
%        x    & -2    & -1    & 0 & 1    & 2    & 3    \\ \hline
%        f(x) & -0.96 & -0.71 & 0 & 0.59 & 0.38 & 0.18 \\ \hline
%    \end{array}$
% }{\mbox{} \hspace{-3cm}
%    \begin{pspicture}(-4.5,-1.5)(4.5,1) % needs the pstricks-add package
%        \psaxes[labelFontSize=\scriptstyle,ticksize=-2pt,linewidth=0.3pt]
%            {->}(0,0)(-4.5,-1.5)(4.5,1)
%        \psplot[linecolor=red]{-4.5}{4.5}{x/(EXP(x)-x)}
%    \end{pspicture}
% }
% \end{verbatim}
%
% \DescribeMacro{\dualboxes*}
% This command has a starred version which eliminates spaces at the beginning and at the
% end of the line, the only remaining space is between the boxes:
% "\dualboxes*"\oarg{pos}\marg{left}\marg{right}.
% \dualboxes*{\parbox{7cm}{
% Here the left part consist of a paragraph box obtained with
% \texttt{\bslash parbox\{7cm\}\{...\}}.
% If we want several paragraphs, a list or a mathematical displayed formula,
% in one of the boxes,
% then we have to put them in a \texttt{minipage} environment.
% }}{\begin{pspicture}(-4.5,-1.5)(4.5,1)
%        \psaxes[labelFontSize=\scriptstyle,ticksize=-2pt,linewidth=0.3pt]
%            {->}(0,0)(-4.5,-1.5)(4.5,1)
%        \psplot[linecolor=red]{-4.5}{4.5}{x/(EXP(x)-x)}
%    \end{pspicture}
% }
%
% \noindent
% Let us indicate that it's unfortunately not possible to use \texttt{verbatim} environments
% (nor the in-line \texttt{\bslash verb} command)
% within "\dualboxes" arguments,
% just as it's not possible for footnotes or margin notes.  
%
% \StopEventually{}
%
% \section{Implementation}
%    \begin{macrocode}
\RequirePackage{ifthen}
\RequirePackage{calc}
\RequirePackage{setspace}
\RequirePackage{xspace}

\newcommand*{\centered}[1]{{\setlength{\parskip}{0pt}\par\noindent\hfill
            #1\hfill\mbox{}}}
%    \end{macrocode}
% The double braces are necessary here to ensure that the "\parskip" modification applies
% locally within the command and not globally to the rest of the document. 
% \smallskip
%    \begin{macrocode}
\newcommand{\footnotespace}{}
\newcommand{\footnoteindent}{}
\let\footnt\footnote
\renewcommand{\footnote}[2][]{\unskip\footnotespace%
    \ifthenelse{\equal{#1}{}}{
        \unskip\footnt{\footnoteindent\ignorespaces #2}
    }{
        \unskip\footnt[#1]{\footnoteindent\ignorespaces #2}
    }\unskip
}
%    \end{macrocode}
% "\unskip" eliminates undesirable spaces before and "\ignorespaces" after.
% \smallskip
%    \begin{macrocode}
\newlength{\strutheight}
\newcommand*{\vstrut}[2][0pt]{%
    \setlength{\strutheight}{#2}%
    \addtolength{\strutheight}{#1}%
    \unskip
    \ensuremath{\rule[-#1]{0pt}{\strutheight}}%
    \ignorespaces%
}

\newlength{\parindentlength}
\setlength{\parindentlength}{\parindent}
\renewcommand{\indent}{\hspace{\parindentlength}}

\newenvironment*{indentblock}[1][\parindentlength]{
    \begin{list}{}{% 
            \setlength{\leftmargin}{#1}
            \setlength{\itemsep}{0pt}
            \setlength{\topsep}{1ex}
            \setlength{\partopsep}{0pt}
        }
        \item[]
        }{\end{list}}

\newlength{\compactlistindent}
\setlength{\compactlistindent}{0.5em}
\newenvironment*{compactlist}[1][\textbullet]{
    \par % sometimes necessary
    \begin{list}{#1\unskip}{% \unskip suppresses the space created by \xspace
            \setlength{\itemsep}{0pt}
            \setlength{\parsep}{0pt}
            \setlength{\topsep}{0ex}
            \setlength{\partopsep}{0pt}
            \setlength{\labelwidth}{1em}
            \setlength{\leftmargin}{\labelwidth}
            \addtolength{\leftmargin}{\labelsep}
            \addtolength{\leftmargin}{\compactlistindent}
        }
        }{\end{list}}

\providecommand{\bul}{\textbullet}
\providecommand{\dash}{\textendash}
\providecommand{\ddash}{\textemdash}
\providecommand{\aster}{\textasteriskcentered}
%    \end{macrocode}
% The command "\asterisk" already exists in the \textsf{mathabx} package.\\
% The following macros need the \textsf{pifont} package.
% \smallskip
%    \begin{macrocode}
\providecommand{\hand}{\ding{43}\xspace}
\providecommand{\checksymb}{\ding{51}\xspace}
\providecommand{\arrowsymb}{\ding{226}\xspace}

\providecommand{\ie}{i.e.\@\xspace}
\providecommand{\eg}{e.g.\@\xspace}

\newcommand{\@@dualboxes}[3][1]{
    \par\noindent
    \raisebox{\depth-#1\totalheight}{#2} \hfill
    \raisebox{\depth-#1\totalheight}{#3} \smallskip
}
\newcommand{\@dualboxes}[3][1]{
    \par\noindent \hfill
    \raisebox{\depth-#1\totalheight}{#2} \hfill
    \raisebox{\depth-#1\totalheight}{#3} \hfill\mbox{}\smallskip
}
\newcommand{\dualboxes}{\@ifstar{\@@dualboxes}{\@dualboxes}}
%    \end{macrocode}
%
% \medskip
% \begin{thebibliography}{9}
% \bibitem{XSP} \emph{The \textsf{xspace} package}, David Carlisle, Morten Høgholm, 
% CTAN v1.13 2014/10/28.
% \bibitem{MKCELL} \emph{The \textsf{makecell} package}, Olga Lapko, CTAN, v0.1e 2009/08/03.
% \bibitem{CENTER} \emph{\textsf{centeredline} -- A macro for centering lines}, Jean-François
% Burnol, CTAN, v1.1 2019/05/03.
% \bibitem{CELLSP} \emph{The \textsf{cellspace} package}, Josselin Noirel, CTAN, v1.8 2019/03/11.
% \bibitem{ENUM} \emph{Customizing lists with the \textsf{enumitem} package}, Javier Bezos, 
% CTAN, v3.9 2019/06/20.
% \bibitem{ARCOL} \emph{The \textsf{arraycols} package}, Antoine Missier, CTAN, v1.2 2021/09/20.
% \bibitem{MSMATH} \emph{\textsf{mismath} -- Miscellaneous mathematical macros}, Antoine Missier,
% CTAN, v1.8 2020/11/15.
% \bibitem{PICINS} \emph{Bilder in \LaTeX -Dokumenten -- PicIns-Benutzerhandbuch},
% Joachim Bleser, Edmund Lang, CTAN, v3.0 sept. 1992.
% \bibitem{COMP} \emph{The \LaTeX\ Companion}. Frank Mittelbach, Michel Goossens, 
% Johannes Braams, David Carlisle, Chris Rowley, 2nd edition, Pearson Education, 2004.
% \end{thebibliography}

% \Finale
\endinput
