%%
%% This is file `morefloats.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% morefloats.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% Project: morefloats
%% Version: 2015/07/22 v1.0h
%% 
%% Copyright (C) 2010 - 2015 by
%%     H.-Martin M"unch <Martin dot Muench at Uni-Bonn dot de>
%% Portions of code copyrighted by other people as marked.
%% 
%% The usual disclaimer applies:
%% If it doesn't work right that's your problem.
%% (Nevertheless, send an e-mail to the maintainer
%%  when you find an error in this package.)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. This version of this license is in
%%    http://www.latex-project.org/lppl/lppl-1-3c.txt
%% and the latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status "maintained".
%% 
%% The Current Maintainer of this work is H.-Martin Muench.
%% 
%% LaTeX 2015 provides the extrafloats command.
%% Don Hosek, Quixote, 1990/07/27 (Thanks!)
%% invented the main code for handling more floats
%% before extrafloats was available.
%% Maintenance has been taken over in September 2010
%% by H.-Martin Muench.
%% David Carlisle pointed the maintainer to the new
%% extrafloats command (Thanks!).
%% 
%% This work consists of the main source file morefloats.dtx,
%% the README, and the derived files
%%    morefloats.sty, morefloats.pdf,
%%    morefloats.ins, morefloats.drv,
%%    morefloats-example.tex, morefloats-example.pdf.
%% 
%% In memoriam
%%  Claudia Simone Barth + 1996/01/30
%%  Tommy Muench + 2014/01/02
%%  Hans-Klaus Muench + 2014/08/24
%% 
\NeedsTeXFormat{LaTeX2e}[2015/01/01]
\ProvidesFile{morefloats.drv}%
  [2015/07/22 v1.0h Raise limit of unprocessed floats (HMM)]
\documentclass{ltxdoc}[2015/03/26]%   v2.0w
\usepackage[T1]{fontenc}[2005/09/27]% v1.99g
\usepackage{pdflscape}[2008/08/11]%   v0.10
\usepackage{holtxdoc}[2012/03/21]%    v0.24
%% morefloats should work with earlier versions of LaTeX2e and
%% may work with earlier versions of the class and those packages,
%% but this was not tested.
%% Please consider updating your LaTeX, class, and packages
%% to the most recent version (if they are not already the most
%% recent version).
\hypersetup{%
 pdfsubject={LaTeX2e package for increasing the limit of unprocessed floats (HMM)},%
 pdfkeywords={LaTeX, morefloats, floats, H.-Martin Muench},%
 pdfencoding=auto,%
 pdflang={en},%
 breaklinks=true,%
 linktoc=all,%
 pdfstartview=FitH,%
 pdfpagelayout=OneColumn,%
 bookmarksnumbered=true,%
 bookmarksopen=true,%
 bookmarksopenlevel=2,%
 pdfmenubar=true,%
 pdftoolbar=true,%
 pdfwindowui=true,%
 pdfnewwindow=true%
}
\CodelineIndex
\hyphenation{docu-ment}
\gdef\unit#1{\mathord{\thinspace\mathrm{#1}}}%
\begin{document}
  \DocInput{morefloats.dtx}%
\end{document}
\endinput
%%
%% End of file `morefloats.drv'.
