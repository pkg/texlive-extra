%%
%% This is file `interfaces.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% interfaces.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% interfaces : 2011/02/19 v3.1 - interfaces : interfaces with keys for other packages (FC)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% 
%% This work consists of the main source file interfaces.dtx
%% and the derived files
%%     interfaces.sty, interfaces.pdf, interfaces.ins,
%%     interfaces-base.sty,
%%     interfaces-LaTeX.sty,
%%     interfaces-tikz.sty,
%%     interfaces-titlesec.sty,
%%     interfaces-hyperref.sty,
%%     interfaces-bookmark.sty,
%%     interfaces-hypbmsec.sty,
%%     interfaces-fancyhdr.sty,
%%     interfaces-marks.sty,
%%     interfaces-tocloft.sty,
%%     interfaces-makecell.sty,
%%     interfaces-enumitem.sty,
%%     interfaces-truncate.sty,
%%     interfaces-appendix.sty,
%%     interfaces-embedfile.sty,
%%     interfaces-environ.sty,
%%     interfaces-umrand.sty,
%%     interfaces-scrlfile.sty,
%%     interfaces-pgfkeys.sty,
%%     interfaces-etoolbox.sty
%% 
%% interfaces: interfaces with keys for other packages (FC)
%% Copyright (C) 2010 by Florent Chervet <florent.chervet@free.fr>
%% 
\let\CTANLinks=y
\let\interfacesquick=n
\edef\thisfile{\jobname}
\def\thisinfo{interfaces with keys for other packages}
\def\thisdate{2011/02/19}
\def\thisversion{3.1}
\def\thisVersion{\thisversion\,\textendash\,release}
\def\CTANbaseurl{http://www.ctan.org/tex-archive/}
\def\LocalOrCTAN#1#2{\ifx y\CTANLinks#2\else#1\fi}
\def\LOCALDOC{file:C:/texmf/doc/latex}
\let\loadclass\LoadClass
\def\LoadClass#1{\loadclass[abstracton]{scrartcl}\let\scrmaketitle\maketitle\AtEndOfClass{\let\maketitle\scrmaketitle}}
{\makeatletter{\endlinechar`\^^J\obeyspaces
 \gdef\ErrorUpdate#1=#2,{\@ifpackagelater{#1}{#2}{}{\let\CheckDate\errmessage\toks@\expandafter{\the\toks@
        \thisfile-documentation: updates required !
              package #1 must be later than #2
              to compile this documentation.}}}}%
 \gdef\CheckDate#1{{\let\CheckDate\relax\toks@{}\@for\x:=\thisfile=\thisdate,#1\do{\expandafter\ErrorUpdate\x,}\CheckDate\expandafter{\the\toks@}}}}
\AtBeginDocument{\CheckDate{tabu=2011/02/19}}
\documentclass[a4paper,oneside]{ltxdoc}
\AtBeginDocument{\DeleteShortVerb{\|}}
\usepackage[latin1]{inputenc}
\usepackage[american]{babel}
\usepackage[T1]{fontenc}
\usepackage{txfonts,times,marvosym,wasysym,nicefrac,numprint,hologo}
\usepackage[scaled=.9]{helvet}
\ifx y\interfacesquick\else
\usepackage[expansion=all,protrusion=none,shrink=50,stretch=20]{microtype}\fi
\PassOptionsToPackage{svgnames}{xcolor}
\usepackage{etoolbox,atveryend,calc}
\usepackage{graphicx,xspace,xcolor,framed}
\usepackage{geometry,lastpage,tocloft,titlesec,hypbmsec,fancyhdr,moresize,relsize,needspace}
\usepackage{enumitem,multirow,makecell,tabularx,booktabs,colortbl,dcolumn,delarray,hhline}
\usepackage[verbose]{linegoal}[2010/12/07]
\usepackage[breakall]{truncate}
\usepackage{appendix}
\usepackage{interfaces}[2010/12/27]
\usepackage{holtxdoc,hyperref,bookmark}
\usepackage{embedfile}
\usepackage{fp}
\usepackage{zref,zref-user,zref-savepos}
\usepackage{enumitem-zref}[2010/12/17]
\usetikz{basic,decorations.pathmorphing}
\csname endofdump\endcsname
\RequirePackage[debugshow]{tabu}[2010/12/28]
\FPmessagestrue
%%\OnlyDescription
\CodelineNumbered
\usepackage{fancyvrb} \fvset{gobble=1,listparameters={\topsep=0pt}}
\lastlinefit999      \widowpenalty5000    \clubpenalty10000    \doublehyphendemerits100000
\geometry{top=0pt,includehead,headheight=.8cm,headsep=.3cm,bottom=1.2cm,footskip=.5cm,left=2.5cm,right=1cm}
\hypersetup{%
  pdftitle={The interfaces package},%
  pdfsubject={interfaces with keys for other packages},%
  pdfauthor={F. CHERVET},%
  pdfpagemode=UseNone,%
  colorlinks,linkcolor=reflink,filecolor=reffilecolor,%
  pdfstartview={FitH},%
  pdfkeywords={tex, e-tex, TeX, e-TeX, eTeX, latex, package, interfaces, marks,%
               pgfkeys, scrlfile, etoolbox, titlesec, fancyhdr, tocloft, environ,%
               embedfile, hyperref, bookmark, hypbmsec, truncate, tikz,%
               makecell, enumitem, appendix, umrand},%
  bookmarksopen=true,bookmarksopenlevel=1,%
  pdfnewwindow=true}
\embedfile{\thisfile.dtx}
\begin{document}
   \DocInput{\thisfile.dtx}
\end{document}
\endinput
%%
%% End of file `interfaces.drv'.
