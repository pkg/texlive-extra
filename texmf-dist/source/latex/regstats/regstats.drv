%%
%% This is file `regstats.drv',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% regstats.dtx  (with options: `driver')
%% 
%% This is a generated file.
%% 
%% Project: regstats
%% Version: 2012/01/07 v1.0h
%% 
%% Copyright (C) 2011 - 2012 by
%%     H.-Martin M"unch <Martin dot Muench at Uni-Bonn dot de>
%% 
%% The usual disclaimer applies:
%% If it doesn't work right that's your problem.
%% (Nevertheless, send an e-mail to the maintainer
%%  when you find an error in this package.)
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3c of this license or (at your option) any later
%% version. This version of this license is in
%%    http://www.latex-project.org/lppl/lppl-1-3c.txt
%% and the latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status "maintained".
%% 
%% The Current Maintainer of this work is H.-Martin Muench.
%% 
%% This work consists of the main source file regstats.dtx,
%% the README, and the derived files
%%    regstats.sty, regstats.pdf,
%%    regstats.ins, regstats.drv,
%%    regstats-example.tex, regstats-example.pdf,
%%    regstats-example.log.
%% 
\NeedsTeXFormat{LaTeX2e}[2011/06/27]
\ProvidesFile{regstats.drv}%
  [2012/01/07 v1.0h Counting used registers (HMM)]
\documentclass{ltxdoc}[2007/11/11]% v2.0u
\usepackage{pdflscape}[2008/08/11]% v0.10
\usepackage{holtxdoc}[2011/02/04]%  v0.21
%% regstats may work with earlier versions of LaTeX2e and those
%% class and packages, but this was not tested.
%% Please consider updating your LaTeX, class, and packages
%% to the most recent version (if they are not already the most
%% recent version).
\hypersetup{%
 pdfsubject={Information about used number of TeX registers (HMM)},%
 pdfkeywords={LaTeX, regstats, registers, read, write, language, box, dimen, counter, toks, skip, muskip, math family, insertion, H.-Martin Muench},%
 pdfencoding=auto,%
 pdflang={en},%
 breaklinks=true,%
 linktoc=all,%
 pdfstartview=FitH,%
 pdfpagelayout=OneColumn,%
 bookmarksnumbered=true,%
 bookmarksopen=true,%
 bookmarksopenlevel=3,%
 pdfmenubar=true,%
 pdftoolbar=true,%
 pdfwindowui=true,%
 pdfnewwindow=true%
}
\CodelineIndex
\hyphenation{printing docu-ment}
\gdef\unit#1{\mathord{\thinspace\mathrm{#1}}}%
\makeatletter
\@ifundefined{eTeX}{\gdef\eTeX{$\m@th \varepsilon $-\TeX }}{% else \relax
}
\makeatother
\begin{document}
  \DocInput{regstats.dtx}%
\end{document}
\endinput
%%
%% End of file `regstats.drv'.
