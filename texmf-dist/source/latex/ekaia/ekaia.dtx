% \iffalse meta-comment
%
% ekaia.dtx
% Copyright (C) 2014-2019, Edorta Ibarra a and the Ekaia Journal (UPV/EHU)
% -----------------------------------------------------------------------
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3c
% of this license or (at your option) any later version.
% The latest version of this license is in
% http://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008-05-04 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Edorta Ibarra.
%
% This work consists of the files ekaia.dtx, ekaia.ins
% and the derived files ekaia.sty, ekaia.pdf and
% ekaia_EUS.pdf.
%
%
% \fi
%
% \iffalse
%
%    \begin{macrocode}
%<package>\NeedsTeXFormat{LaTeX2e}[1999/12/01]
%<package>\ProvidesPackage{ekaia}
%<package>   [2019/01/02 v1.06 ekaia Package]
%    \end{macrocode}
%<*driver>
\documentclass{ltxdoc}
\usepackage{verbatim}
\EnableCrossrefs
\CodelineIndex
\RecordChanges
\begin{document}
  \DocInput{ekaia.dtx}
\end{document}
%</driver>
% \fi
% \CheckSum{0}
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%%
% \changes{v1.00}{14/07/20}{First version for development}
% \changes{v1.02}{14/12/26}{First public version}
% \changes{v1.04}{14/12/26}{Article title in English language included}
% \changes{v1.06}{19/01/02}{Template modifications to comply with UPV/EHU Press latest requirements}
%
% \GetFileInfo{ekaia.sty}
%
% \title{The \textsf{ekaia} package\thanks{This file (\textsf{ekaia.dtx})  
% has version 1.06 last revised 19-01-02.}}
% \author{Edorta Ibarra and the Ekaia Journal (UPV/EHU)\\\texttt{ekaia@ehu.eus}}
% \date{2019-01-02}
% \maketitle

% \begin{abstract}
% \noindent This package configures the \texttt{article} document class layout and 
% provides a set of commands and environments to generate an article  following the 
% style of the University of the Basque Country Science and Technology Journal 
% Ekaia.
% \end{abstract}
%
% \tableofcontents
%
% \section{Introduction}
%
% In the last centuries, advances in science and technology have produced a significant impact
% in the society. For that reason, it is necessary to build
% a divulgation bridge between the scientific community and the society, publishing the research
% results in a knowledgeable fashion.
%
% In the Basque Country, the divulgation of science and technology has encountered problems
% because technical Basque Language was not sufficiently developed. Hopefully, a great amount of
% work has been carried out in the last years. Nowadays, a variety of subjects can be studied 
% in Basque Language at the university, and research groups that use Basque Language for their work have 
% been created. This fact has increased the need of scientific divulgation in Basque Language,
% because the terminology created in the scientific community and the tradition built around it
% do not have the sufficient impact in the society. Scientific subjects are usually addressed in
% Basque media. However, at this moment this is not sufficient. 
%
% In 1989, the University of the Basque Country (UPV/EHU) founded the journal Ekaia to support 
% the divulgation of science and technology in Basque Language. Unfortunately, this first 
% attempt was brief.
% In 1995, a group of lecturers of the University of the Basque Country re-founded the journal,
% because of the need of a terminological unity and the continuous appearance of new discoveries 
% and concepts.
%
% The Ekaia journal is mainly written for readers that have a basic scientific formation. Its main 
% objectives are the following: to publish the last advances in science and technology, 
% to provide complementary bibliography for university students and non-university teachers,
% to support the standardization of the technical and scientific Basque Language and to help to
% solve the problems that arise when the Basque Language is used in these fields. 
% Ekaia is published twice a year, and it gathers articles of a variety of scientific fields.
%
% The \texttt{ekaia} package configures the \texttt{article} document class layout and 
% provides a set of commands and environments to prepare articles using \LaTeX{} 
% for the peer review process of the University of the Basque Country Science and Technology 
% Journal Ekaia.
%
% \section{Calling the package}
%
% The package \verb|ekaia| is called using the \verb|\usepackage|
% command:\\ \verb|\usepackage[<options>]{ekaia}|. 
%
% The following package options are provided in the current \verb|ekaia.sty| version:
% \begin{itemize}
% \item \texttt{review}: This option must be selected when generating the manuscript for peer-review.
% \item \texttt{final}: This option must be selected when generating the final manuscript.
% \end{itemize}
% The \texttt{ekaia} package requires the following additional packages:
% \texttt{babel}, \texttt{geometry}, \texttt{sectsty}, \texttt{fancyhdr},
% \texttt{indentfirst}, \texttt{basque-date} and \texttt{ccicons}.
%
%
% \section{Package usage}
% 
% \subsection{\LaTeX{} commands and environments provided by the package}
%
% The \verb|ekaia| package provides the following \LaTeX{} commands for Ekaia
% articles preparation:
%
% \begin{itemize}
%
% \item \verb|\izenburua{}|: This command is used to produce the title of the document Basque.
% \item \verb|\azpiizenburua{}|: This command is used to produce the title of the document in English.
% \item \verb|\datak{}{}|: Submition and acceptance dates are introduced in this command,
% respectively.
% \end{itemize}
%
% Additionally, the \verb|ekaia| package provides the following \LaTeX{} environments:
%
% \begin{itemize}
% 
% \item \texttt{autoreak}: This environment is used to introduce information about the
% authors (names, surnames, affiliations and contact information).
% \item \texttt{laburpena}: This environment is used to produce the article abstract 
% in Basque Language.
% \item \texttt{hitz-gakoak}: This environment sets the keywords in Basque Language.
% \item \texttt{abstract}: This environment is used to produce the article abstract
% in English.
% \item \texttt{keywords}: This environment sets the keywords in English.
% \end{itemize}
% 
% \subsection{Package usage example}
%
% The following \texttt{.tex} code example illustrates how to use the commands and 
% environments summarized in the previous section to prepare a document for 
% submission to the Ekaia journal.\\
%
% \noindent
% \verb|\documentclass[twoside,a4paper,11pt]{article}|\\
% \verb|\usepackage[review]{ekaia}|\\
% \verb|\begin{document}|\\
% \verb|\izenburua{Ekaia Aldizkariko egileentzako gidalerroak}|\\
% \verb|\azpiizenburua{Ekaia: Guidelines for authors}|\\
% \verb||\\
% \verb|\begin{autoreak}|\\
% \verb|\textit{Egile guztien izenak$^1$}|\\
% \verb|\linebreak|\\
% \verb|$^1$Egile guztien afilizazioa (instituzioa, hiria, herrialdea)|\\
% \verb|\linebreak|\\
% \verb|Egile nagusiaren helbide elektronikoa|\\
% \verb|\linebreak|\\
% \verb|Egile nagusiaren helbide osoa (Instituzioa, kalea, zenbakia,|\\ 
% \verb|posta kutxatila, hiria, herrialdea)|\\
% \verb|\linebreak|\\
% \verb|Egile nagusiaren ORCID zenbakia|\\
% \verb|\end{autoreak}|\\
% \verb| |\\
% \verb|\datak{XXXX-XX-XX}{XXXX-XX-XX}|\\
% \verb| |\\
% \verb|\begin{laburpena}|\\
% \verb|(250 hitz gehienez eta paragrafo batean)|\\
% \verb|\end{laburpena}|\\
% \verb| |\\
% \verb|\begin{hitz-gakoak}|\\
% \verb|Ekaia, \LaTeX{}, euskara.|\\
% \verb|\end{hitz-gakoak}|\\
% \verb| |\\
% \verb|\begin{abstract}|\\
% \verb| (Maximum 250 words and one paragraph)|\\ 
% \verb|\end{abstract}|\\
% \verb| |\\
% \verb|\begin{keywords}|\\
% \verb|Ekaia, \LaTeX{}, Basque language.|\\
% \verb|\end{keywords}|\\
% \verb| |\\
% \verb|\section{Sarrera}|\\
%
% 
% \subsection{Creating the bibliography} 
%
% The use of the \texttt{thebibliography} environment is highly recommended to
% produce the article bibliography according to the Ekaia journal bibliography
% style\footnote{http://www.ehu.es/ojs/index.php/ekaia/about/submissions\#authorGuidelines}.
% The following example helps to illustrate the procedure.\\
%
% \noindent
% \verb|\renewcommand\refname{\indent Bibliografia}|\\
% \verb|\begin{thebibliography}{99}|\\
% \verb| |\\
% \verb|\bibitem{ibarra}|\\
% \verb|IBARRA E. eta ETXEBARRIA J.R. 2014. "<\LaTeX{}: euskarazko dokumentu|\\
% \verb|zientifiko-teknikoen ediziorako baliabideak">. \textit{Ekaia},|\\
% \verb|27, 329-343.|\\
% \verb|\end{thebibliography}|\\
%
% \appendix
%
% \section{Appendices}
%
% \subsection{License}
%
% Copyright 2014-2019 Edorta Ibarra and the Ekaia Journal (UPV/EHU). 
%
% This program can be redistributed and/or modified under the terms of the 
% \LaTeX\ Project Public License Distributed from CTAN archives in directory
% macros/latex/basee/lppl.txe; either version 1.2 of the License, or any later 
% version.
%
% \subsection{Version history}
%
% \begin{itemize}
% \item \textbf{Version v1.00 (14/07/27).} Initial non-public 
% version for development.
% \item \textbf{Version v1.02 (14/12/26).} First public version. 
% \item \textbf{Version v1.04 (16/11/04).} \verb|azpiizenburua| command included to generate the title in English. Minor mistakes corrected.
% \item \textbf{Version v1.06 (19/01/02).} Template modifications in order to comply with UPV/EHU Press latest requirements.
% \end{itemize}
%
%
% \iffalse
%<basque>
%<basque> %%%Documentation of the ekaia package in Basque
%<basque> \documentclass{ltxdoc}
%<basque> \begin{document}
%<basque> 
%<basque> \title{\textsf{ekaia} paketea\thanks{Fitxategi honek (\textsf{ekaia.dtx})
%<basque> 1.06. bertsioa du. Azken aldiz errebisatua: 19-01-02.}}
%<basque> \author{Edorta Ibarra eta Ekaia Aldizkaria (UPV/EHU)\\\texttt{ekaia@ehu.eus}}
%<basque> \date{2019-01-02}
%<basque> \renewcommand{\contentsname}{Aurkibidea}
%<basque> \renewcommand{\refname}{Bibliografia}
%<basque> \renewcommand\thesection{\arabic{section}.}
%<basque> \renewcommand\thesubsection{\thesection \arabic{subsection}.}
%<basque> \maketitle
%<basque> 
%<basque> \begin{abstract}
%<basque>  Pakete honek \texttt{article} dokumentu-klasearen itxura aldatzen du
%<basque>  eta zenbait komando eta ingurune eskaintzen ditu Euskal Herriko Unibertsitateko
%<basque>  Ekaia Zientzia eta Teknologi aldizkarirako artikuluak sortzeko. 
%<basque> \end{abstract}
%<basque>
%<basque> \tableofcontents
%<basque>
%<basque> \section{Sarrera}
%<basque> Azken mendeotako aurrerapen zientifikoek eta teknologiaren hedapenak sekulako
%<basque> eragina izan dute gizartean, eta funtsezkoak bilakatu dira guztiontzako. 
%<basque> Horretarako ezinbestekoa izan da zientzia eta gizartearen artean dibulgazioaren 
%<basque> zubia eraikitzea, ikerketaren emaitzak modu ulergarrian plazaratzea, alegia. 
%<basque>
%<basque> Euskal herrian zientziaren eta teknikaren dibulgazioak arazo ugari izan ditu, 
%<basque> euskara bera ez baitzegoen maila teknikorako egokituta. Zorionez, lan handia 
%<basque> egin da azken urteotan, eta gaur egun unibertsitateko gai ugari euskaraz irakasteaz 
%<basque> gain, euskaraz diharduten ikerketa-taldeak ere eratu dira. Honek areagotu egin du 
%<basque> euskarazko dibulgazioaren premia, inguru zientifikoetan sorturiko lexikoak eta 
%<basque> indartutako tradizioak ez baitute, tamalez, kalean nahikoa oihartzunik izan. 
%<basque> He\-dabide desberdinetan gai zientifikoak maiz agertzen dira, baina oraindik hutsune 
%<basque> asko geratzen dira. 
%<basque>
%<basque> Euskal Herriko Unibertsitateak zientzia- eta teknika-dibulgazioa bultzatzeko Ekaia 
%<basque> aldizkaria sortu zuen 1989. urtean. Lehen saio honen iraupena laburra zen, tamalez, 
%<basque> eta Ekaiaren bidea eten zen, 2. alean, hain zuzen ere. 1995. urtean EHUko irakasle-talde
%<basque> batek, indarberriturik eta hornikuntza hobearekin, inoiz utziriko bideari berrekin zion,
%<basque> bateratasun terminologikoak eta kontzeptu zein aurkikuntza berrien etengabeko agerpenak 
%<basque> horrela eskatzen dutelakoan. 
%<basque>
%<basque> Ekaia aldizkaria oinarrizko formazio zientifikoa duten irakurleei bideratuta dago. 
%<basque> Haren helburuen artean ondokoak daude: zientzia eta teknikaren alorretan egiten diren
%<basque> aurrerapenak plazaratzea, unibertsitateko ikasleei zein irakaskuntza ertaineko irakasleei
%<basque> testuliburuen osagarriak izango diren materialak eskaintzea, esparru zientifiko-teknikoan
%<basque> euskararen estandarizazioa bultzatzea, eta esparru honetan hizkuntzaren erabilerak sortzen
%<basque> dituen arazoak konpontzen laguntzea. Xede honekin, urtean bitan Ekaia plazaratzen da, alor
%<basque> zientifiko desberdinetako artikuluak biltzen ditu. 
%<basque>
%<basque> \texttt{article} dokumentu-klasearen itxura moldatzen du \texttt{ekaia} paketeak,
%<basque> eta Ekaia aldizkarirako \LaTeX{} bidezko artikuluak prestatzeko erabilgarriak
%<basque> diren komando eta inguruneak sortzen ditu.
%<basque>
%<basque> \section{Paketea nola deitu}
%<basque>
%<basque> \verb|\usepackage| komandoa erabiliz deitzen da
%<basque> \verb|ekaia| paketea:\\ \verb|\usepackage{ekaia}|.
%<basque>
%<basque> \verb|ekaia| paketearen bertsio honek hurrengo aukerak ditu:
%<basque> \begin{itemize}
%<basque> \item \texttt{review}: Errebisiorako bertsioa sortzeko hautatu behar da aukera hori.
%<basque> \item \texttt{final}: Onartutako bertsioa sortzeko hautatu behar da aukera hori.
%<basque> \end{itemize}
%<basque> eskaintzen. Hurrengo pakete gehigarriak behar ditu
%<basque> \texttt{ekaia} paketeak: \texttt{babel}, \texttt{geometry}, \texttt{sectsty}, 
%<basque> \texttt{fancyhdr}, \texttt{indentfirst}, \texttt{basque-date} eta \texttt{ccicons}.
%<basque>
%<basque> \section{Paketearen erabilera}
%<basque> \subsection{Paketeak eskaintzen dituen \LaTeX{}en komando eta inguruneak}
%<basque>
%<basque> Hurrengo komandoak eskaintzen ditu \texttt{ekaia} paketeak Ekaia aldizkarirako
%<basque> artikuluak prestatzeko:
%<basque>
%<basque> \begin{itemize}
%<basque> \item \verb|\izenburua{}|: Artikuluaren izenburua euskaraz sortzeko erabiltzen da
%<basque> komando hori.
%<basque> \item \verb|\azpiizenburua{}|: Artikuluaren izenburua ingelesez sortzeko erabiltzen da
%<basque> komando hori.
%<basque> \item \verb|\datak{}{}|: bidalpen- eta onarpen-datak inprimatzen ditu
%<basque> komando ho\-rrek, hurrenez hurren. 
%<basque> \end{itemize}
%<basque>
%<basque> Horrez gain, hurrengo inguruneak eskaintzen ditu \texttt{ekaia} 
%<basque> paketeak:
%<basque> \begin{itemize} 
%<basque> \item \texttt{autoreak}: Autoreei buruzko informazioa (izen-abizenak, afiliazioa,
%<basque> eta kontakturako informazioa) sortzeko erabiltzen da ingurune hori.
%<basque> \item \texttt{laburpena}: Artikuluaren laburpena euskaraz sortzeko erabiltzen da 
%<basque> ingurune hori.
%<basque> \item \texttt{hitz-gakoak}: Artikuluaren hitz gakoak euskaraz sortzeko erabiltzen
%<basque>  da ingurune hori.
%<basque> \item \texttt{abstract}: Artikuluaren laburpena ingelesez sortzeko erabiltzen da 
%<basque> ingurune hori.
%<basque> \item \texttt{keywords}: Artikuluaren hitz gakoak ingelesez sortzeko erabiltzen
%<basque>  da ingurune hori.
%<basque> \end{itemize}
%<basque>
%<basque> \subsection{Paketearen erabileraren adibidea}
%<basque> 
%<basque>  LaTeX{} bidez Ekaia aldizkarirako
%<basque>  dokumentuak prestatzeko \verb|ekaia.sty| paketeak eskaintzen dituen komandoen 
%<basque>  eta inguruneen erabilera erakusten du hurrengo \texttt{.tex} kodearen adibideak .\\
%<basque> 
%<basque> \noindent
%<basque> \verb|\documentclass[twoside,a4paper,11pt]{article}|\\
%<basque> \verb|\usepackage[review]{ekaia}|\\
%<basque> \verb|\begin{document}|\\
%<basque> \verb|\izenburua{Ekaia Aldizkariko egileentzako gidalerroak}|\\
%<basque> \verb|\azpiizenburua{Ekaia: Guidelines for authors}|\\
%<basque> \verb||\\
%<basque> \verb|\begin{autoreak}|\\
%<basque> \verb|\textit{Egile guztien izenak$^1$}|\\
%<basque> \verb|\linebreak|\\
%<basque> \verb|$^1$Egile guztien afilizazioa (instituzioa, hiria, herrialdea)|\\
%<basque> \verb|\linebreak|\\
%<basque> \verb|Egile nagusiaren helbide elektronikoa|\\
%<basque> \verb|\linebreak|\\
%<basque> \verb|Egile nagusiaren helbide osoa (Instituzioa, kalea, zenbakia,|\\ 
%<basque> \verb|posta kutxatila, hiria, herrialdea)|\\
%<basque> \verb|\linebreak|\\
%<basque> \verb|Egile nagusiaren ORCID zenbakia|\\
%<basque> \verb|\end{autoreak}|\\
%<basque> \verb| |\\
%<basque> \verb|\datak{XXXX-XX-XX}{XXXX-XX-XX}|\\
%<basque> \verb| |\\
%<basque> \verb|\begin{laburpena}|\\
%<basque> \verb|(250 hitz gehienez eta paragrafo batean)|\\
%<basque> \verb|\end{laburpena}|\\
%<basque> \verb| |\\
%<basque> \verb|\begin{hitz-gakoak}|\\
%<basque> \verb|Ekaia, \LaTeX{}, euskara.|\\
%<basque> \verb|\end{hitz-gakoak}|\\
%<basque> \verb| |\\
%<basque> \verb|\begin{abstract}|\\
%<basque> \verb| (Maximum 250 words and one paragraph)|\\ 
%<basque> \verb|\end{abstract}|\\
%<basque> \verb| |\\
%<basque> \verb|\begin{keywords}|\\
%<basque> \verb|Ekaia, \LaTeX{}, Basque language.|\\
%<basque> \verb|\end{keywords}|\\
%<basque> \verb| |\\
%<basque> \verb|\section{Sarrera}|\\
%<basque>
%<basque> \subsection{Bibliografia sortzen}
%<basque>
%<basque> \texttt{thebibliography} ingurunea erabiltzea gomendatzen da
%<basque> artikuluaren bibliografia Ekaia aldizkariaren bibliografia-estiloarekin
%<basque> bat izan dadin\footnote{http://www.ehu.es/ojs/index.php/ekaia/about/submissions\#authorGuidelines}.
%<basque> Hurrengo adibidea baliagarria da prozedura hori ulertzeko.\\
%<basque>
%<basque> \noindent
%<basque> \verb|\renewcommand\refname{\indent Bibliografia}|\\
%<basque> \verb|\begin{thebibliography}{99}|\\
%<basque> \verb| |\\
%<basque> \verb|\bibitem{ibarra}|\\
%<basque> \verb|IBARRA E. eta ETXEBARRIA J.R. 2014. "<\LaTeX{}: euskarazko dokumentu|\\
%<basque> \verb|zientifiko-teknikoen ediziorako baliabideak">. \textit{Ekaia},|\\
%<basque> \verb|27, 329-343.|\\
%<basque> \verb|\end{thebibliography}|\\
%<basque>
%<basque> \appendix
%<basque>
%<basque> \renewcommand\thesection{A.}
%<basque> \renewcommand\thesubsection{\thesection \arabic{subsection}.} 
%<basque>
%<basque> \section{Eranskinak}
%<basque> 
%<basque> \subsection{Lizentzia}
%<basque> 
%<basque> Copyright 2014-2019 Edorta Ibarra eta Ekaia Aldizkaria (UPV/EHU). 
%<basque>
%<basque> CTAN fitxategietan banatutako \LaTeX\ proiektuko lizentzia
%<basque> publikoaren terminoetan birbanatu edota alda daiteke
%<basque> programa hau:
%<basque>
%<basque> macros/latex/basee/lppl.txe; bai lizentziaren 1.2. bertsioaren
%<basque> terminoetan, edota ondorengo edozein bertsioren terminoetan. 
%<basque> 
%<basque> \subsection{Bertsioen historia}
%<basque> 
%<basque> \begin{itemize}
%<basque> \item \textbf{v1.00. bertsioa (14/07/27).} Garapenerako bertsio
%<basque> ez publikoa.
%<basque> \item \textbf{v1.02. bertsioa (14/12/26).} Lehen bertsio
%<basque> publikoa. 
%<basque> \item \textbf{v1.04. bertsioa (16/11/04).} \verb|azpiizenburua| komandoa
%<basque> gehitu da izenburua ingelesez sortzeko. Errore txikiak zuzenduta.
%<basque> \item \textbf{v1.06. bertsioa (19/01/02).} Aldaketak txantiloian UPV/EHUko argitalpen
%<basque> zerbitzuaren beharrizanak jarraituz.
%<basque> \end{itemize}
%<basque> 
%<basque> \subsection{Inplementazioa}
%<basque> Ingelesezko dokumentazioan daude irakurgai paketearen 
%<basque> inplementazioari buruzko xehetasun teknikoak.
%<basque> \end{document}
%<basque> 
% \fi
%
% \iffalse
%<example> %%%Example of an Ekaia article created in LaTeX
%<example> \documentclass[twoside,a4paper,11pt]{article}
%<example> \usepackage[review]{ekaia}
%<example> %\usepackage[final]{ekaia} %Aldatu aukera 'final'era onartutako bertsioa bidaltzean
%<example> \begin{document}
%<example> 
%<example> \izenburua{Ekaia Aldizkariko egileentzako gidalerroak}
%<example> \azpiizenburua{Ekaia: Guidelines for authors}
%<example>
%<example> \begin{autoreak}
%<example> \textit{Egile guztien izenak$^1$}
%<example> \linebreak
%<example> $^1$Egile guztien afilizazioa (instituzioa, hiria, herrialdea)
%<example> \linebreak
%<example> Egile nagusiaren helbide elektronikoa
%<example> \linebreak
%<example> Egile nagusiaren helbide osoa (Instituzioa, kalea, zenbakia, posta kutxatila, 
%<example> hiria, herrialdea)
%<example> \linebreak
%<example> Egile nagusiaren ORCID zenbakia
%<example> \end{autoreak}
%<example>
%<example> \datak{XXXX-XX-XX}{XXXX-XX-XX}
%<example>
%<example> \begin{laburpena}
%<example> (250 hitz gehienez eta paragrafo batean)
%<example> \end{laburpena}
%<example>
%<example> \begin{hitz-gakoak}
%<example> Ekaia, \LaTeX{}, euskara.
%<example> \end{hitz-gakoak}
%<example>
%<example> \begin{abstract}
%<example> (Maximum 250 words and one paragraph) 
%<example> \end{abstract}
%<example>
%<example> \begin{keywords}
%<example> Ekaia, \LaTeX{}, Basque language.
%<example> \end{keywords}
%<example>
%<example> \section{Egileentzako gidalerro orokorrak}
%<example>
%<example> Jarraian aurkezten dira Ekaia Aldizkarian publikatzeko autoreek kontuan izan behar 
%<example> dituzten gidalerro orokorrak.
%<example>
%<example> \begin{enumerate}
%<example> \item Ekaia aldizkarian Osasun Zientziak, Natur Zientziak, Zientzia Zehatzak eta 
%<example> Teknologiari buruzko idazlan originalak argitaratuko dira, goi-mailako dibulgazioaren 
%<example> eta ikerketaren esparruan. 
%<example> \item Ale bakoitzean 20 artikulu argitaratuko dira gehienez eta urtean ale arrunt bat 
%<example> eta beste ale berezi bat (gehienez 10 artikulurekin) argitaratuko dira. Urte berean
%<example> argitaratuko den alerako lanak urte horretako maiatzaren 31 baino lehen bidalitakoak 
%<example> izango dira; data horretatik kanpo bidalitako lanak hurrengo urteko aleko  
%<example> edizio-prozesuan sartuko dira.
%<example> \item Artikulu-egileek, dokumentuaren bertsio elektronikoa ``doc'' edo ``pdf''
%<example> artxibo"-formatoan (gure kasuan, ``doc'' formatuarekin lan egitea erosoagoa zaigu) 
%<example> bidali dezakete OJS on-line sistemara (http://www.ehu.es/ekaia).
%<example> \item Hizkuntza matematikorako (formulak,\ldots) \LaTeX{} formatoa ere onartuko da, 
%<example> dena den, aldi berean euskara zuzentzailearentzako ``pdf'' formatoan ere bidali 
%<example> behar da.
%<example> \item Testua euskara batuaz idatzita egongo da, hizkera argia eta zehatza erabiliz, 
%<example> eta literatura zientifikoaren usadio eta konbentzioak betez. Nomenklatura, laburdurak, 
%<example> ikurrak etab., nazioarteko kodeen arabera idatziko dira. XUXEN zuzentzaile elektronikoa
%<example> pasatuta bidaltzea ere eskatzen da.
%<example> Artikuluaren hasieran, izenburua, egileen izen-abizenak osorik (ezin da izenaren 
%<example> laburdura hizkia jarri), afiliazioak eta harremanetarako pertsonaren helbide 
%<example> elektronikoa jarriko dira.
%<example> Artikuluaren testu orokorraren aurretik laburpena (abstract-a) eta hitz gakoak 
%<example> (key-words-ak) jarri behar dira euskaraz eta ingelesez.
%<example> Artikuluaren atalak era librean aukeratu daitezke. Dena den, ezinbestean lehenengo 
%<example> atala eta azkenengoa sarrera eta bibliografia izan beharko dira, hurrenez hurren.
%<example> Testuaren formatoaren ezaugarriak ezagutzeko on-line sisteman jarri diren txantiloiak
%<example> jarraitu: ''doc” formatoarentzako eta \LaTeX formatoarentzako txantiloiak dituzue 
%<example> eskuragarri. 10.000 eta 15.000 karaktere (tarterik gabe) bitarteko lana izan behar da.
%<example> Testuan, irudi, argazki eta taulen kokapena adieraziko da. Egileak irudi, argazki eta 
%<example> taula bakoitza deskribatzen duen oina bidali beharko du eta irudia, argazkia edo taula
%<example> norberak egina ez bada nondik jaso den ere adierazi beharko da. Noski, berriz 
%<example> argitaratzeko eskubideak dituen pertsona edo erakundearen baimenarekin. Egile eskubideak
%<example> urratzen badira artikuluaren egilea izango da egindakoaren arduradun, Ekaiak ez du kasu
%<example> horietan ardurarik hartuko.
%<example> Aipamen bibliografikoak testuan azaltzen diren ordenean zenbatuko dira. Posible den 
%<example> kasuan erreferentzietan egileen izen eta abizenak osorik jartzea gomendatzen da. eta 
%<example> lanaren amaieran, ondorengo eran idatziko dira:
%<example> \begin{itemize}
%<example> \item[a)] Liburuetarako: [Zenbakia]. Egilea(k). Urtea. Liburuaren izenburua kurtsibaz.
%<example> Argitaletxea, Argitalpen-herria.
%<example> 
%<example> Adibidez, [3] SCHMIDT K. 1975. \textit{Respiration in air}. Cambridge University Press,
%<example> London.
%<example> \item[b)] Artikuluetarako: [Zenbakia]. Egilea(k). Urtea. "<Artikuluaren izenburua">.
%<example> Aldizkariaren izena kurtsibaz, bolumena beltzez, orriak.
%<example> 
%<example> Adibidez: [5] PUSKA M.J. eta NIEMINEN R.M. 1994. "<Theory of positrons in solids and 
%<example> no solid surfaces">. \textit{Reviews of Modern Physics}, 66, 841-896.
%<example> \item[c)] Sareko erreferentzien URLarekin batera web orrialdearen eguneratze-data 
%<example> adierazi beharko da.
%<example>  \end{itemize}
%<example> \item Bidalitako artikulua ez da beste inon argitaratua izan ezta argitaratua izan 
%<example> dadin beste aldizkari batera bidalia ere.
%<example> \item Egileek gaian adituak diren eta euskaraz dakiten bi ebaluatzaile zientifiko 
%<example> proposatu beharko dituzte.
%<example> \item Erredakzio-batzordeko zuzendariak arloko koordinatzaileari bidaliko dio artikulua 
%<example> eta arloko koordinatzaileak gaian aditua den ebaluatzaile bati. Ebaluatzailearen 
%<example> iruzkinak eta arloko koordinatzailearen erabakia (argitaratzeko prest, zuzenketa 
%<example> txikiak, zuzenketa nagusiak) egileari itzuliko zaizkio. Egileak zuzenketak egin eta 
%<example> gero, artikuluaren argitalpenaz azken erabakia erredakzio-batzordeak izango du.
%<example> \item Ebaluazio zientifikoa gainditu duten artikulu guztiak euskara zuzentzailearen 
%<example> eskuetatik pasatuko dira. Euskara zuzentzaileak ezinbestez zuzendu behar dena gorriz eta
%<example>  iradokizunak berdez adieraziko ditu testuan bertan. Egilearen lehenengo zuzenketaren 
%<example> ostean euskara zuzentzaileak erabakiko du bigarren zuzenketa bat beharrezkoa den edo ez.
%<example> \item Azkenik, Ekaia aldizkariak eta Kultura Zientifikoko Katedrak adostutako elkarlana
%<example> bultzatzeko, egileek artikuluaren 400-500 hitzetako laburpen dibulgatiboa egiteko
%<example> konpromezua hartuko dute, Zientzia Kaiera hedabide digitalean argitaratzeko.
%<example> \end{enumerate}
%<example>
%<example> \section{\LaTeX{}eko \texttt{ekaia.sty} paketea erabiltzeari buruzko oharrak}
%<example>
%<example> Titulua, egileak, autoreak, etab. zehazteko, beharrezkoa da txantiloiak emandako 
%<example> formatua eta komando propioak jarraitzea. Behin derrigorrezko atal horiek bete ondoren,
%<example> \LaTeX{}en ohikoa den bezala idatzi daiteke dokumentua. Hau da, atalak, taulak,  
%<example> ekuazioak, irudiak etab. sortzeko, \LaTeX{}en ohikoak diren komando eta inguruneak 
%<example> erabil daitezke.
%<example>
%<example> Beharrezkoa balitz, posible da pakete gehigarriak erabiltzea \verb|\usepackage{}|
%<example> komandoaren bidez, beti ere txantiloiaren formatu originala errespetatzen badute.
%<example>
%<example> \texttt{ekaia.sty} paketearen dokumentazioan (euskaraz eta ingelesez) aurki daitezke
%<example> pakete horri buruzko informazio gehigarria eta xehetasun teknikoak. Nahi izanez gero,
%<example> \LaTeX{} bidezko euskarazko dokumentu zientifiko-teknikoei buruzko informazio gehigarria 
%<example> lor daiteke \cite{ibarra} erreferentzian. 
%<example>
%<example> \renewcommand\refname{\indent Bibliografia}
%<example> \begin{thebibliography}{99}
%<example>
%<example> \bibitem{ibarra} 
%<example> IBARRA E. eta ETXEBARRIA J.R. 2014. "<\LaTeX{}: euskarazko dokumentu zientifiko-teknikoen
%<example>  ediziorako baliabideak">. \textit{Ekaia}, 27, 329-343.
%<example> \end{thebibliography}
%<example> \end{document}
%<example> 
%<example> %%OHARRA. Ondorengo inplementazio-kodea txantiloitik borratu daiteke, ez baita beharrezkoa.
%<example> %%Adibide-fitxategia .dtx fitxategi batetatik automatikoki sortu delako agertzen da
%<example> %%hurrengo inplementazio-kodea.
% \fi
%
% \subsection{Implementation}
%
% At the beginning of the code, document language selection is performed
% using the \verb|babel| package. Ekaia articles are written in Basque.
% However, as the abstract must be also written in English, both language
% options are loaded. 
%    \begin{macrocode}
\RequirePackage[english,basque]{babel}
\selectlanguage{basque}
%    \end{macrocode}
% Spaces according to Basque language rules are set.
%    \begin{macrocode}
\frenchspacing
%    \end{macrocode}
% Basque date options are loaded
%    \begin{macrocode}
\RequirePackage{basque-date}
%    \end{macrocode}
% The line spacing is set to one and a half of the standard line spacing.
%    \begin{macrocode}
\linespread{1.3}
%    \end{macrocode}
% Document margin layout is selected using the \verb|geometry| package.
%    \begin{macrocode}
\RequirePackage[top=2.6cm,bottom=2.8cm,left=3cm,right=3cm]{geometry}
%    \end{macrocode}
% Additionally, section font size selection is achieved using the \verb|sectsty|
% package.
%    \begin{macrocode}
\RequirePackage{sectsty}
\sectionfont{\fontsize{12}{15}\selectfont}
\subsectionfont{\fontsize{12}{15}\selectfont}
\subsubsectionfont{\fontsize{12}{15}\selectfont}
%    \end{macrocode}
% Creative commons icons are loaded to generate the final manuscript version
%    \begin{macrocode}
\RequirePackage{ccicons}
%    \end{macrocode}
% Package options for \texttt{review} and \texttt{final} manuscript versions are provided
%    \begin{macrocode}
\newcommand{\ekaiafoot}{dummy}
\DeclareOption{review}{\renewcommand{\ekaiafoot}{
\tiny Errebisiorako bertsioa\\
Ekaia aldizkaria (UPV/EHU), \eusdata}
}
\DeclareOption{final}{\renewcommand{\ekaiafoot}{
\tiny\copyright UPV/EHU Press\\
                              ISSN: 0214-9001\\
                              e-ISSN: 2444-3255\\
                              \ccLogo\ccAttribution\ccNonCommercial\ccShareAlike\\
                              Attribution-NonCommercial-ShareAlike\\
                              4.0 international (CC BY-NC-SA 4.0)}
}
\ExecuteOptions{review}
\ProcessOptions\relax
%    \end{macrocode}
% Document footer definition is performed using the \verb|fancyhdr| package.
%    \begin{macrocode}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\fancyhead[L]{ }
\fancyhead[C]{ }
\fancyhead[R]{ }
\fancyfoot[L]{ }
\fancyfoot[C]{\thepage}
\fancyfoot[R]{\ekaiafoot}

\makeatletter
\let\ps@plain\ps@fancy
\makeatother

\makeatletter
\def\ps@headings{%
    \let\@oddfoot\@empty
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\markboth
    \def\sectionmark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
          \thesection\quad
        \fi
        ##1}}}}
\makeatother

%    \end{macrocode}
% Indent after the \verb|\section| command is forced using the \verb|indentfirst| package.
%    \begin{macrocode}
\RequirePackage{indentfirst}

%    \end{macrocode}
% Figure and table captions are re-defined according to the Basque Language rules.
%    \begin{macrocode}
\makeatletter
\def\fnum@figure{\textbf{\fontsize{10}{15}\selectfont\thefigure .~irudia}}
\makeatother

\makeatletter
\def\fnum@table{\textbf{\fontsize{10}{15}\selectfont \thetable .~taula}}
\makeatother

\makeatletter
\long\def\@makecaption#1#2{%
\vskip\abovecaptionskip
\sbox\@tempboxa{#1\textbf{.} \fontsize{10}{15}\selectfont #2}%
\ifdim\wd\@tempboxa >\hsize
#1\textbf{.} \fontsize{10}{15}\selectfont #2\par
\else
\global \@minipagefalse
\hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
\fi
\vskip\belowcaptionskip}
\makeatother

%    \end{macrocode}
% Commands \verb|\thesection|, \verb|\thesubsection|, etc. are redefined in order to
% comply with the Basque Language rules. Additionally, section indent is set.
%    \begin{macrocode}
\renewcommand\thesection {\indent \arabic{section}.}
\renewcommand\thesubsection {\thesection \arabic{subsection}.}
\renewcommand\thesubsubsection {\thesubsection \arabic{subsubsection}.}
\renewcommand\theparagraph {\thesubsubsection \arabic{paragraph}.}
\renewcommand\thesubparagraph {\theparagraph \arabic{subparagraph}.}

%    \end{macrocode}
% \DescribeMacro{\izenburua} A new command is defined in order to create the 
% document title in Basque.
%    \begin{macrocode}
\newcommand{\izenburua}[1]{
\begin{flushleft} 
\fontsize{16}{15}\textbf{#1}\linebreak\fontsize{11}{15}
\end{flushleft}
}
%    \end{macrocode}
% \DescribeMacro{\azpiizenburua} A new command is defined in order to create the 
% document title in English.
%    \begin{macrocode}

\newcommand{\azpiizenburua}[1]{
\begin{flushleft} 
\fontsize{12}{11}\textit{(#1)}\linebreak\fontsize{12}{11}
\end{flushleft}
}



%    \end{macrocode}
% \DescribeMacro{\datak} A new command is defined in order to create the 
% document submission and acceptance dates.
%    \begin{macrocode}
\newcommand{\datak}[2]{
\begin{flushleft} 
Jasoa:~{#1}
\linebreak
Onartua:~{#2}
\linebreak
\end{flushleft}
}

%    \end{macrocode}
% \DescribeMacro{autoreak} A new environment is created in order to print 
% the document authors, affiliations and contact information.
%    \begin{macrocode}
\newenvironment{autoreak}
{
\flushright
}

%    \end{macrocode}
% \DescribeMacro{laburpena} A new environment is created to write the
% abstract in Basque.
%    \begin{macrocode}
\newenvironment{laburpena}
{
\selectlanguage{basque}
\setlength{\parindent}{0pt}
\textbf{Laburpena:}~\itshape
}{\setlength{\parindent}{0.8cm}\\ }

%    \end{macrocode}
% \DescribeMacro{abstract} The \verb|abstract| environment is redefined to
% write the abstract in English according to the Ekaia Journal style.
%    \begin{macrocode}
\renewenvironment{abstract}
{
\selectlanguage{english}
\setlength{\parindent}{0pt}
\textbf{Abstract:}~\bfseries
}{\setlength{\parindent}{0.8cm}\selectlanguage{basque}\\ }

%    \end{macrocode}
% \DescribeMacro{hitz-gakoak} A new environment for keywords in Basque is created.
%    \begin{macrocode}
\newenvironment{hitz-gakoak}{
\setlength{\parindent}{0pt}
\textbf{Hitz gakoak:}~
}{\setlength{\parindent}{0.8cm}\\ }

%    \end{macrocode}
% \DescribeMacro{keywords} A new environment for keywords in English is created.
% Language selection command is used to switch between languages.
%    \begin{macrocode}
\newenvironment{keywords}{
\setlength{\parindent}{0pt}
\textbf{Keywords:}~
}{\setlength{\parindent}{0.8cm}\\ }

%    \end{macrocode}
%
% \Finale
%
\endinput