bibtex:
bib
bst

bibtex/bib:
amsrefs

bibtex/bib/amsrefs:
amsj.bib

bibtex/bst:
amsrefs

bibtex/bst/amsrefs:
amsra.bst
amsrn.bst
amsrs.bst
amsru.bst
amsry.bst

doc:
latex

doc/latex:
amsrefs

doc/latex/amsrefs:
amsrdoc.pdf
amsrefs.pdf
amsxport.pdf
changes.pdf
cite-xa.tex
cite-xb.tex
cite-xh.tex
cite-xs.tex
gktest.ltb
ifoption.pdf
jr.bib
mathscinet.pdf
pcatcode.pdf
rkeyval.pdf
textcmds.pdf

source:
latex

source/latex:
amsrefs

source/latex/amsrefs:
README
amsrdoc.tex
amsrefs.dtx
amsrefs.faq
amsrefs.ins
amsxport.dtx
amsxport.ins
changes.tex
ifoption.dtx
ifoption.ins
install.txt
manifest.txt
mathscinet.dtx
mathscinet.ins
pcatcode.dtx
pcatcode.ins
rkeyval.dtx
rkeyval.ins
textcmds.dtx
textcmds.ins

tex:
latex

tex/latex:
amsrefs

tex/latex/amsrefs:
amsbst.sty
amsrefs.sty
ifoption.sty
mathscinet.sty
pcatcode.sty
rkeyval.sty
textcmds.sty
