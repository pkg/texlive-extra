% \def\filename{changes.tex}
% \def\fileversion{1.0}
% \def\filedate{2004/06/30}
%
% American Mathematical Society
% Technical Support
% Publications Technical Group
% 201 Charles Street
% Providence, RI 02904
% USA
% tel: (401) 455-4080
%      (800) 321-4267 (USA and Canada only)
% fax: (401) 331-3842
% email: tech-support@ams.org
%
% Copyright 2004, 2007, 2010 American Mathematical Society.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3c
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
% 
% This work has the LPPL maintenance status `maintained'.
% 
% The Current Maintainer of this work is the American Mathematical
% Society.
%
% ====================================================================

\documentclass[11pt]{amsdtx}

%\usepackage[T1]{fontenc}

\usepackage[latin1]{inputenc}

\usepackage{fullpage}

\usepackage{amsrefs}

\MakeShortVerb{\|}

\makeatletter

\def\bysame{%
    \leavevmode\hbox to3em{\hrulefill}\thinspace
    \kern\z@
}

\DeclareRobustCommand{\fld}{\category@index{field}}
\DeclareRobustCommand{\btype}{\category@index{entry type}}

\long\def\@ifempty#1{\@xifempty#1@@..\@nil}

\long\def\@xifempty#1#2@#3#4#5\@nil{%
    \ifx#3#4\@xp\@firstoftwo\else\@xp\@secondoftwo\fi
}

\long\def\@ifnotempty#1{\@ifempty{#1}{}}

\newcounter{changecounter}

\newcommand\change[1][]{%
    \refstepcounter{changecounter}%
    \medskip
    \par
    \noindent
    \textbf{Change~\thechangecounter\@ifnotempty{#1}{ [#1]}.}\enskip
    \ignorespaces
}

%%  \newcommand\bugref[1]{%
%%      [#1]%
%%  }

\newcounter{tableline}[table]

\newcommand{\tbl}{%
    \refstepcounter{tableline}
    \thetableline.%
}

\makeatother

\title{User-Visible Changes to the \pkg{amsrefs} Package, version~2.0}

\author{David M. Jones}

\date{June 30, 2004}

\begin{document}

\maketitle

This document describes some of the user-visible changes made to the
\pkg{amsrefs} package between version~1.23, the last public release,
and version~2.0.  This list is not exhaustive.  Instead, the focus is
on correct (or at least reasonable) user documents that would produce
different output when processed under the two versions.  In general,
the changes discussed here fall between the following poles:
\begin{enumerate}

\item
Version~1.23 produces results that are subtly wrong.  These range from
things that would go unnoticed by any but the most attentive reader to
things that, even if noticed, would likely be shrugged off as
unimportant.

\item
Version~1.23 produces results that are obviously wrong, i.e., they
would be obvious under even the most cursory examination of the
output.  In most cases, this means the resulting document would be
unacceptable.

\end{enumerate}

We don't include the even more severe category of examples where
version~1.23 generates a fatal error since no document would be
produced in that case.  We also don't include enhancements or new
features that depend upon new syntax, since such constructs would not
be expected to be found in version~1.23 documents.

Reference numbers in square brackets refer to bugs reported by users.

\section{The \opt{initials} option}

See table~\ref{tbl:initials_bugs} for illustrations of these changes.

\begin{table}\small
\caption{Bugs in the \opt{initials} option}
\label{tbl:initials_bugs}
\begin{tabular*}{\textwidth}{@{}ll@{\extracolsep{\fill}}l@{\extracolsep{\fill}}l@{}}
& \textbf{Input} & \textbf{v1.23 output} & \textbf{v2.0 output} \\

\tbl &
|Doe, J.|
& J. . Doe
& J. Doe
\\

\tbl &
|Doe, John R.|
& J. R. . Doe
& J. R. Doe
\\

\tbl &
|Doe, Yu.|
& Y. . Doe
& Yu. Doe
\\

\tbl &
|Doe, J.~R.|
& J.~ R. . Doe
& J. R. Doe
\\

\tbl &
|Doe, John~Henry|
& J. Doe
& J. H. Doe
\\

\tbl &
|Bing, R H| & R H. Bing & R H Bing\\

\tbl &
|Katzenbach, N. {deB}elleville|
& @B@@@@@@@@ @@ N. Katzenbach
& N. deB. Katzenbach
\\

\tbl &
|Katzenbach, N. deB.|
& @ @ @@ N. Katzenbach
& N. deB. Katzenbach
\\

\tbl &
\texttt{Gr�mov, M�rtin}
& M�. Gr�mov
& M. Gr�mov
\\

\tbl &
|Doe, {Yu}ri|
& Y. Doe
& Yu. Doe

\end{tabular*}
\end{table}

\change[B-2005]\label{chng:final_dots}
When a given name ends in a period, an extra space and period would be
appended to the name.  Lines 1 and~2 in table~\ref{tbl:initials_bugs}
illustrate the most common cases.  Line~3 illustrates a related case
that is both a bug fix and an enhancement (cf.\ lines 7 and~8):
initials consisting of multiple characters were not recognized (cf.\
change~\ref{chng:multi_char_inits}).

\change[B-2005a]
Ties (|~|) between given names resulted in incorrect output, either
extra space (line~4) or a dropped initial (line~5).  Since \bibtex/
automatically generates ties, this problem occurs frequently when
using \bst{amsxport}.

\change
A name consisting of a single letter would receive an erroneous period
(line~6).

\change\label{chng:initial_lowercase}
Names beginning with lowercase letters were handled badly (lines 7
and~8) (cf.\ change~\ref{chng:multi_char_inits}).

\change
Extended character sets were not handled well.  For example, when
using T1 fonts, lowercase 8-bit characters were not processed
correctly (line~9).

\change\label{chng:multi_char_inits}
There was no way to indicate a multi-character initial
(line~10). (Cf.\ changes \ref{chng:final_dots}
and~\ref{chng:initial_lowercase}).

\section{The \fld{xref} field}

\change[B-2009]
Repeatable fields (such as \fld{editor}) were not inherited via the
\fld{xref} field. Consider the following example:
\begin{verbatim}
    \bib*{icalp77}{proceedings}{
        editor={Arto Salomaa},
        editor={Magnus Steinby},
        title={Automata, Languages and Programming, Fourth Colloquium}
    }

    \bib{AhoS1977}{article}{
        title={How Hard is Compiler Code Generation?},
        author={Alfred V. Aho},
        author={Ravi Sethi},
        xref={icalp77},
        pages={1\ndash 15}
    }
\end{verbatim}
Version~1.23 would produce
\begin{quote}
Alfred V. Aho and Ravi Sethi, \emph{How Hard is Compiler Code
  Generation?}, Automata, Languages and Programming, Fourth
  Colloquium, pp.~1--15.
\end{quote}
(without the editors) instead of
\begin{quote}
Alfred V. Aho and Ravi Sethi, \emph{How Hard is Compiler Code
  Generation?}, Automata, Languages and Programming, Fourth Colloquium
  (Arto Salomaa and Magnus Steinby, eds.), pp.~1--15.
\end{quote}
Other additive keys like \fld{author}, \fld{isbn} and \fld{review}
would similarly be dropped.

\section{The \opt{alphabetic} option}

%%  \change[B-2013]\label{chng:b2013}
%%  A compound citation such as
%%  \begin{verbatim}
%%      \citelist{ \cite{SS75}*{Theorem 1} \cite{TM86}*{Prop. 2} }
%%  \end{verbatim}
%%  used to produce
%%  \begin{quote}
%%      [SS75, Theorem 1, TM86, Prop. 2]
%%  \end{quote}
%%  whereas version~2.0 correctly separates the citations with a
%%  semi-colon instead of a comma:
%%  \begin{quote}
%%      [SS75, Theorem 1; TM86, Prop. 2]
%%  \end{quote}

\change[B-2009]
In version~1.23, generation of alphabetic labels was done by \bibtex/
rather than by \fn{amsrefs.sty}.  This means that if you did not use
\bibtex/ to generate your \cs{bib} entries from a \fn{.bib} file,
\pkg{amsrefs} would use the author's citation key as the alphabetic
label\footnote{It was also possible for the user to supply a different
alphabetic label using the \fld{label} field, but this feature was
undocumented.}.  This means that an entry such as
\begin{verbatim}
    \bib{D1999}{article}{
        author={Doe, John~R.},
        year={1999}
    }
\end{verbatim}
would appear as
\begin{quote}
[D1999] John R. Doe (1999).
\end{quote}
while if the same entry was generated from a \fn{.bib} file using
\bst{amsxport}, the entry would appear as
\begin{quote}
[Doe99] John R. Doe (1999).
\end{quote}
This approach had a number of problems, such as lack of uniformity, so
in version~2.0, it is \pkg{amsrefs.sty} that generates the alphabetic
label, not \bibtex/, and the entry above would appear as [Doe99] even
if \bibtex/ is not used.

\section{Changes to bibliography style}

\change
A book with both an author and an editor, for example,
\begin{verbatim}
    \bib{X}{book}{
        author={Doe, John},
        editor={Crow, Jack},
        title={Book with one author and one editor}
    }
\end{verbatim}
would be formatted as
\begin{quote}
    John DoeJack Crow (ed.), \emph{Book with one author and one editor}.
\end{quote}
rather than
\begin{quote}
    John Doe, \emph{Book with one author and one editor}.  Edited by
    Jack Crow.
\end{quote}

\change
Editors did not participate in the automatic \cs{bysame} processing,
so if you had two books by the same editor, e.g.
\begin{verbatim}
    \bib{A}{book}{
        editor={Crow, Jack},
        title={First book}
    }

    \bib{B}{book}{
        editor={Crow, Jack},
        title={Second book}
    }
\end{verbatim}
the result would be
\begin{quote}
[1] Jack Crow (ed.), \emph{First book}.

[2] Jack Crow (ed.), \emph{Second book}.
\end{quote}
instead of
\begin{quote}
[1] Jack Crow (ed.), \emph{First book}.

[2] \bysame (ed.), \emph{Second book}.
\end{quote}

Consider also what happens if we change the |editor| in the first
entry to |author|:
\begin{quote}
[1] Jack Crow, \emph{First book}.

[2] Jack Crow (ed.), \emph{Second book}.
\end{quote}
versus
\begin{quote}
[1] Jack Crow, \emph{First book}.

[2] \bysame (ed.), \emph{Second book}.
\end{quote}

\change
Some changes have been made to punctuation and order of fields to
bring the \pkg{amsrefs} format into line with the AMS house style.

\section{\cs{cite}-like commands}

\change
The \cs{ycites}, \cs{ocites}, \cs{fullcite} and \cs{fullocite}
commands for author-year citations were not implemented in
version~1.23.  Instead, they produced a warning on the terminal and
then behaved identically to the \cs{cites}, \cs{cite} or \cs{ocite}
commands.  They will be implemented in version~2.0 and thus produce
different output.  Table~\ref{tbl:cites} illustrates the changes.

\begin{table}[hb]\small
\caption{The \cs{cite}-like commands}\label{tbl:cites}
\begin{center}
\begin{minipage}{.9\textwidth}\small
Consider the following entries:
\begin{quote}
    Crow, Jack, John Doe, and John Bull. 1999.\,\dots

    Doe, John. 2000.\,\dots
\end{quote}
If the citation keys for the items are  ``A'' and~``B'', respectively,
the various \cs{cite} commands would produce the following:

\begin{tabular*}{\textwidth}{@{}ll@{\extracolsep{\fill}}l@{\extracolsep{\fill}}l@{}}
& \textbf{Input} & \textbf{v1.23 output} & \textbf{v2.0 output} \\

\tbl &
|\cite{A}|
& (Crow et al., 1999)
& (Crow et al., 1999)
\\

\tbl &
|\fullcite{A}|
& (Crow et al., 1999)
& (Crow, Doe, and Bull, 1999)
\\

\tbl &
|\ocite{A}|
& Crow et al. (1999)
& Crow et al. (1999)
\\

\tbl &
|\fullocite{A}|
& Crow et al. (1999)
& Crow, Doe, and Bull (1999)
\\

\tbl &
|\cites{A,B}|
& (Crow et al., 1999; Doe, 2000)
& (Crow et al., 1999; Doe, 2000)
\\

\tbl &
|\ycites{A,B}|
& (Crow et al., 1999; Doe, 2000)
& (1999, 2000)
\\

\tbl &
|\ocites{A,B}|
& (Crow et al., 1999; Doe, 2000)
& Crow et al. (1999) and Doe (2000)
\\

\end{tabular*}
\end{minipage}
\end{center}
\end{table}

\change
Citation-sorting did not always work correctly when there were
optional arguments involved.  So,
\begin{verbatim}
    \citelist{\cite{2} \cite{1} \cite{3}}
\end{verbatim}
would correctly produce
\begin{quote}
    [1--3]
\end{quote}
but
\begin{verbatim}
    \citelist{\cite{2} \cite{1} \cite{3}*{Theorem 1}}
\end{verbatim}
would produce
\begin{quote}
    [1, 3, Theorem 1; 2]
\end{quote}
instead of
\begin{quote}
    [1; 2; 3, Theorem 1]
\end{quote}
Note also that version~2.0 correctly switches to using semi-colons
while version~1.23 erroneously used a comma between the first and
second items.% (cf.\ change~\ref{chng:b2013}).

\section{The \pkg{inicap} package and \cs{EnglishInitialCaps}}

\change

As of version~2.0, \pkg{amsrefs} no longer attempts to adjust the
capitalization of book titles.  Version~1.23 shipped with a package
called \pkg{inicap} that provided a function for converting English
titles from normal upper/lower case to \qq{initial caps} form.  For
example, if an author typed
\begin{verbatim}
    booktitle={Group rings, crossed products and Galois theory}
\end{verbatim}
\pkg{amsrefs} would automatically adjust it to
\begin{quote}
    Group Rings, Crossed Products and Galois Theory
\end{quote}
However, \pkg{inicap} is incomplete and produces incorrect results in
cases like the following:
\begin{quote}
\verb+Term rewriting with sharing and memo{\"\i}zation+\hfill\break
\i Term Rewriting with Sharing and memo\"zation

\medskip

\verb+The \ldq low $M^*$-estimate\rdq\ for covering numbers+\hfill\break
The \ldq low $M^*$-estimate\rdq\ for Covering Numbers
\hfill\break(where ``low'' and ``estimate'' should be capitalized)

\end{quote}
and an example like the following, which uses the \verb+\qq+ command
that we recommend, would result in a fatal error:
\begin{quote}
\verb+The \qq{low $M^*$-estimate} for covering numbers+
\end{quote}

In addition, this feature was of limited usefulness since it was only
applied to book titles.  Authors should already be accustomed to
taking full responsibility for the capitalization of book titles since
\bibtex/ does not alter the capitalization of book titles.

The low utility of the feature does not seem to warrant the effort
that would be required to make the \pkg{inicap} package robust enough
for use in a production environment, so we have decided instead to
deprecate it and remove this functionality from the \pkg{amsrefs}
package.

\end{document}
