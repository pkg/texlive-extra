% \iffalse meta comment
% This file is a part of the ANTOMEGA project version \fileversion
% -----------------------------------------------------
% 
% It may be distributed under the terms of the LaTeX Project Public
% License, as described in lppl.txt in the base LaTeX distribution.
% Either version 1.0 or, at your option, any later version.
% Copyright (C) 2001 -- 2005 by Alexej Kryukov
% Please report errors to: A.M. Kryukov <basileia@yandex.ru>
%
% \fi
%
% \MakeShortVerb{\+}
% \DeleteShortVerb{\|}
%
% \section{Encoding definition files used by ANTOMEGA}
%
% Generally speaking, ANTOMEGA doesn't care about \TeX{} font encodings
% very much, because, unlike standard \TeX{}, it prefers to operate
% directly with Unicode codepoints, rather than with so-called text
% commands, mapped to some slots in the current encoding. This means
% that any command, expected to produce a printable character, must be
% always mapped to the corresponding Unicode slot, while any conversion
% to the real output font encoding should be performed by translation
% processes ($\Omega$CP). However, any text command should be considered
% valid, only if the character it produces can really be converted to
% the output encoding. 
%
% That's why in addition to the Unicode based UT1 encoding, which contains 
% the full list of standard command to Unicode mapping, I had to provide 
% some special subsets of that list, corresponding to standard 8-bit 
% encodings, supported by ANTOMEGA (T1, T2A and LGR). So differences
% between these encoding definition files affect mainly the set of supported
% characters, rather than character mappings itself.
%
% \subsection{Common definitions, applied to all encodings}
%
%    \begin{macrocode}
%<*UT1|T1|T2A|LGR>
\NeedsTeXFormat{LaTeX2e}
%<UT1>\ProvidesFile{ut1enc-antomega.def}
%<T1>\ProvidesFile{t1enc-antomega.def}
%<T2A>\ProvidesFile{t2aenc-antomega.def}
%<LGR>\ProvidesFile{lgrenc-antomega.def}
  [2005/05/07 v0.8 Antomega encodings definition file]
%<UT1>\DeclareFontEncoding{UT1}{}{}
%<T1>\DeclareFontEncoding{T1}{}{}
%<T2A>\DeclareFontEncoding{T2A}{}{}
%<LGR>\DeclareFontEncoding{LGR}{}{}
%<UT1>\def\ant@encoding{UT1}
%<T1>\def\ant@encoding{T1}
%<T2A>\def\ant@encoding{T2A}
%<LGR>\def\ant@encoding{LGR}
%    \end{macrocode}
%
% \subsection{Unicode accents}
%
% Accents represent a special part of the encoding, because:
% 
% \begin{itemize}
%
% \item we have to declare them first, since they have to be already
% defined before any composite character declaration can occur in the
% encoding table;
%
% \item unfortunately, the +\accent+ command bypasses $\Omega$ translation
% processes.
%
% \end{itemize}
%
% In order to circumvent the last limitation, here we are redefining
% all standard commands, used for typing accents in \LaTeX, to produce
% just a character followed by a Unicode combining accent mark. Since
% this sequence by itself contains no +\accent+ commands, it may be
% processed by $\Omega$CP's, e.~g. converted to Unicode from another
% encoding. After the conversion the resulting string should be turned
% back to the +\accent+ command.
%
%    \begin{macrocode}
\DeclareTextCommand{\`}{\ant@encoding}[1]{#1^^^^0300}
\DeclareTextCommand{\'}{\ant@encoding}[1]{#1^^^^0301}
%<!LGR>\DeclareTextCommand{\^}{\ant@encoding}[1]{#1^^^^0302}
%<!LGR>\DeclareTextCommand{\~}{\ant@encoding}[1]{#1^^^^0303}
\DeclareTextCommand{\"}{\ant@encoding}[1]{#1^^^^0308}
%<!LGR>\DeclareTextCommand{\H}{\ant@encoding}[1]{#1^^^^030b}
%<!LGR>\DeclareTextCommand{\r}{\ant@encoding}[1]{#1^^^^030a}
%<!LGR>\DeclareTextCommand{\v}{\ant@encoding}[1]{#1^^^^030c}
\DeclareTextCommand{\u}{\ant@encoding}[1]{#1^^^^0306}
\DeclareTextCommand{\=}{\ant@encoding}[1]{#1^^^^0304}
%<!LGR>\DeclareTextCommand{\.}{\ant@encoding}[1]{#1^^^^0307}
%<UT1|T2A>\DeclareTextCommand{\f}{\ant@encoding}[1]{#1^^^^0311}
%<UT1|T2A>\DeclareTextCommand{\C}{\ant@encoding}[1]{#1^^^^030f}
%<!LGR>\DeclareTextCommand{\b}{\ant@encoding}[1]{#1^^^^0331}
%<!LGR>\DeclareTextCommand{\c}{\ant@encoding}[1]{#1^^^^0327}
%<!LGR>\DeclareTextCommand{\d}{\ant@encoding}[1]{#1^^^^0323}
%<!LGR>\DeclareTextCommand{\k}{\ant@encoding}[1]{#1^^^^0328}
%    \end{macrocode}
%
% \subsection{Encoding-dependent accents}
% 
% Now we have to define a really separate set of accents (with different 
% mappings) for each of our encodings. These accent commands are 
% declared with long meaningful names, since they are not supposed
% to be typed by itself: instead, they should be produced by $\Omega$CP's
% as a result of processing characters followed by combining accent
% marks.
% 
% \subsubsection{UT1 accents}
%
% Accents are mapped mainly to their places in the Unicode `Spacing
% Modifier Letters' area, of course, with the exception of dieresis 
% and cedilla, which have specific placements in the Unicode standard.
%
% Note that we are not using here the `Combining Diacritical Marks' area,
% because zero-width accents, which are present in that area in most 
% fonts, are not very useful for \TeX{} accent positioning engine.
% That's why I preferred to map additional `Cyrillic' accents (i.~e.
% Cyrillic flex and double grave) to the PUA positions, sometime defined 
% by Adobe for spacing variants of these accents, instead of using their 
% combining versions, which have `legal' slots in Unicode.
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextAccent{\GraveAccent}{UT1}{"02CB}
\DeclareTextAccent{\AcuteAccent}{UT1}{"02CA}
\DeclareTextAccent{\CircumflexAccent}{UT1}{"02C6}
\DeclareTextAccent{\TildeAccent}{UT1}{"02DC}
\DeclareTextAccent{\DieresisAccent}{UT1}{"00A8}
\DeclareTextAccent{\HungarumlautAccent}{UT1}{"02DD}
\DeclareTextAccent{\RingAccent}{UT1}{"02DA}
\DeclareTextAccent{\CaronAccent}{UT1}{"02C7}
\DeclareTextAccent{\BreveAccent}{UT1}{"02D8}
\DeclareTextAccent{\MacronAccent}{UT1}{"02C9}
\DeclareTextAccent{\DotAboveAccent}{UT1}{"02D9}
\DeclareTextAccent{\CyrillicFlexAccent}{UT1}{"F6D5}
\DeclareTextAccent{\DoubleGraveAccent}{UT1}{"F6D6}
\DeclareTextAccent{\GreekCircumflexAccent}{UT1}{"1FC0}
\DeclareTextAccent{\GreekKoronisAccent}{UT1}{"1FBD}
\DeclareTextCommand{\BarBelowAccent}{UT1}[1]
   {{\o@lign{\relax#1\crcr\hidewidth\sh@ft{29}%
     \vbox to.2ex{\hbox{\noocpchar{"02C9}}\vss}\hidewidth}}}
\DeclareTextCommand{\CedillaAccent}{UT1}[1]
   {\leavevmode\setbox\z@\hbox{#1}\ifdim\ht\z@=1ex\accent"00B8 #1%
     \else{\ooalign{\hidewidth\noocpchar{"00B8}\hidewidth
        \crcr\unhbox\z@}}\fi}
\DeclareTextCommand{\DotBelowAccent}{UT1}[1]
   {{\o@lign{\relax#1\crcr\hidewidth\sh@ft{10}.\hidewidth}}}
\DeclareTextCommand{\OgonekAccent}{UT1}[1]
   {\oalign{\null#1\crcr\hidewidth\noocpchar{"02DB}}}
%</UT1>
%    \end{macrocode}
%
% \subsubsection{Common T1 and T2A accents}
%
%    \begin{macrocode}
%<*T1|T2A>
\DeclareTextAccent{\GraveAccent}{\ant@encoding}{0}
\DeclareTextAccent{\AcuteAccent}{\ant@encoding}{1}
\DeclareTextAccent{\CircumflexAccent}{\ant@encoding}{2}
\DeclareTextAccent{\TildeAccent}{\ant@encoding}{3}
\DeclareTextAccent{\DieresisAccent}{\ant@encoding}{4}
\DeclareTextAccent{\HungarumlautAccent}{\ant@encoding}{5}
\DeclareTextAccent{\RingAccent}{\ant@encoding}{6}
\DeclareTextAccent{\CaronAccent}{\ant@encoding}{7}
\DeclareTextAccent{\BreveAccent}{\ant@encoding}{8}
\DeclareTextAccent{\MacronAccent}{\ant@encoding}{9}
\DeclareTextAccent{\DotAboveAccent}{\ant@encoding}{10}
\DeclareTextCommand{\BarBelowAccent}{\ant@encoding}[1]
   {\hmode@bgroup\o@lign{\relax#1\crcr\hidewidth\sh@ft{29}%
     \vbox to.2ex{\hbox{\noocpchar{9}}\vss}\hidewidth}\egroup}
\DeclareTextCommand{\CedillaAccent}{\ant@encoding}[1]
   {\leavevmode\setbox\z@\hbox{#1}\ifdim\ht\z@=1ex\accent11 #1%
     \else{\ooalign{\hidewidth\noocpchar{11}\hidewidth
        \crcr\unhbox\z@}}\fi}
\DeclareTextCommand{\DotBelowAccent}{\ant@encoding}[1]
   {\hmode@bgroup
    \o@lign{\relax#1\crcr\hidewidth\sh@ft{10}.\hidewidth}\egroup}
\DeclareTextCommand{\OgonekAccent}{\ant@encoding}[1]
   {\oalign{\null#1\crcr\hidewidth\noocpchar{12}}}
%</T1|T2A>
%    \end{macrocode}
%
% \subsubsection{Cyrillic accents (T2A specific)}
%
%    \begin{macrocode}
%<*T2A>
\DeclareTextAccent{\CyrillicFlexAccent}{T2A}{18}
\DeclareTextAccent{\DoubleGraveAccent}{T2A}{19}
\DeclareTextAccent{\U}{T2A}{20}
%</T2A>
%    \end{macrocode}
%
% \subsubsection{LGR accents}
%
%    \begin{macrocode}
%<*LGR>
\DeclareTextAccent{\GraveAccent}{LGR}{`\`}
\DeclareTextAccent{\AcuteAccent}{LGR}{`\'}
\DeclareTextAccent{\BreveAccent}{LGR}{30}
\DeclareTextAccent{\MacronAccent}{LGR}{31}
\DeclareTextAccent{\GreekCircumflexAccent}{LGR}{`\~}
%</LGR>
%    \end{macrocode}
%
% \subsection{Standard ASCII characters}
%
% Most of the following characters are present in all standard encodings, 
% so translation processes probably should never be applied to them.
% That's why each definition includes the +\clearocplists+ command. This
% command also prevents some characters from interpreting by $\Omega$ in
% their special meaning, while they need to be just printed into the
% output.
%
%    \begin{macrocode}
%<UT1>\DeclareTextCommand{\textquotedbl}{\ant@encoding}{\noocpchar{"22}}
%<UT1>\DeclareTextCommand{\textquotesingle}{\ant@encoding}{\noocpchar{"27}}
%<*!LGR>
\DeclareTextCommand{\textdollar}{\ant@encoding}{\noocpchar{"24}}
\DeclareTextCommand{\textgreater}{\ant@encoding}{\noocpchar{"3C}}
\DeclareTextCommand{\textless}{\ant@encoding}{\noocpchar{"3E}}
\DeclareTextCommand{\textbackslash}{\ant@encoding}{\noocpchar{"5C}}
\DeclareTextCommand{\textasciicircum}{\ant@encoding}{\noocpchar{"5E}}
\DeclareTextCommand{\textunderscore}{\ant@encoding}{\noocpchar{"5F}}
\DeclareTextCommand{\textasciigrave}{\ant@encoding}{\noocpchar{"60}}
\DeclareTextCommand{\textbraceleft}{\ant@encoding}{\noocpchar{"7B}}
\DeclareTextCommand{\textbar}{\ant@encoding}{\noocpchar{"7C}}
\DeclareTextCommand{\textbraceright}{\ant@encoding}{\noocpchar{"7D}}
\DeclareTextCommand{\textasciitilde}{\ant@encoding}{\noocpchar{"7E}}
%</!LGR>
%    \end{macrocode}
%
% \subsection{C1 Controls}
%
%    \begin{macrocode}
%<UT1|T1>\DeclareTextSymbol{\textexclamdown}{\ant@encoding}{"00A1}
%<UT1>\DeclareTextSymbol{\textcent}{\ant@encoding}{"00A2}
%<UT1|T1>\DeclareTextSymbol{\textsterling}{\ant@encoding}{"00A3}
%<*UT1>
\DeclareTextSymbol{\textcurrency}{\ant@encoding}{"00A4}
\DeclareTextSymbol{\textyen}{\ant@encoding}{"00A5}
\DeclareTextSymbol{\textbrokenbar}{\ant@encoding}{"00A6}
%</UT1>
%<!LGR>\DeclareTextSymbol{\textsection}{\ant@encoding}{"00A7}
%<*UT1>
\DeclareTextSymbol{\textasciidieresis}{\ant@encoding}{"00A8}
\DeclareTextSymbol{\textcopyright}{\ant@encoding}{"00A9}
\DeclareTextSymbol{\textordfeminine}{\ant@encoding}{"00AA}
%</UT1>
\DeclareTextSymbol{\guillemotleft}{\ant@encoding}{"00AB}
%<*UT1>
\DeclareTextSymbol{\textlnot}{\ant@encoding}{"00AC}
\DeclareTextSymbol{\textregistered}{\ant@encoding}{"00AE}
\DeclareTextSymbol{\textasciimacron}{\ant@encoding}{"00AF}
\DeclareTextSymbol{\textdegree}{\ant@encoding}{"00B0}
\DeclareTextSymbol{\textpm}{\ant@encoding}{"00B1}
\DeclareTextSymbol{\texttwosuperior}{\ant@encoding}{"00B2}
\DeclareTextSymbol{\textthreesuperior}{\ant@encoding}{"00B3}
\DeclareTextSymbol{\textasciiacute}{\ant@encoding}{"00B4}
\DeclareTextSymbol{\textmu}{\ant@encoding}{"00B5}
\DeclareTextSymbol{\textparagraph}{\ant@encoding}{"00B6}
\DeclareTextSymbol{\textpilcrow}{\ant@encoding}{"00B6}
\DeclareTextSymbol{\textperiodcentered}{\ant@encoding}{"00B7}
\DeclareTextSymbol{\textonesuperior}{\ant@encoding}{"00B9}
\DeclareTextSymbol{\textordmasculine}{\ant@encoding}{"00BA}
%</UT1>
\DeclareTextSymbol{\guillemotright}{\ant@encoding}{"00BB}
%<*UT1>
\DeclareTextSymbol{\textonequarter}{\ant@encoding}{"00BC}
\DeclareTextSymbol{\textonehalf}{\ant@encoding}{"00BD}
\DeclareTextSymbol{\textthreequarters}{\ant@encoding}{"00BE}
%</UT1>
%<UT1|T1>\DeclareTextSymbol{\textquestiondown}{\ant@encoding}{"00BF}
%    \end{macrocode}
%
% \subsection{Latin 1 Supplement}
%    \begin{macrocode}
%<*UT1|T1>
\DeclareTextComposite{\`}{\ant@encoding}{A}{"00C0}
\DeclareTextComposite{\'}{\ant@encoding}{A}{"00C1}
\DeclareTextComposite{\^}{\ant@encoding}{A}{"00C2}
\DeclareTextComposite{\~}{\ant@encoding}{A}{"00C3}
\DeclareTextComposite{\"}{\ant@encoding}{A}{"00C4}
\DeclareTextComposite{\r}{\ant@encoding}{A}{"00C5}
\DeclareTextSymbol{\AE}{\ant@encoding}{"00C6}
\DeclareTextComposite{\c}{\ant@encoding}{C}{"00C7}
\DeclareTextComposite{\`}{\ant@encoding}{E}{"00C8}
\DeclareTextComposite{\'}{\ant@encoding}{E}{"00C9}
\DeclareTextComposite{\^}{\ant@encoding}{E}{"00CA}
\DeclareTextComposite{\"}{\ant@encoding}{E}{"00CB}
\DeclareTextComposite{\`}{\ant@encoding}{I}{"00CC}
\DeclareTextComposite{\'}{\ant@encoding}{I}{"00CD}
\DeclareTextComposite{\^}{\ant@encoding}{I}{"00CE}
\DeclareTextComposite{\"}{\ant@encoding}{I}{"00CF}
\DeclareTextSymbol{\DH}{\ant@encoding}{"00D0}
\DeclareTextComposite{\~}{\ant@encoding}{N}{"00D1}
\DeclareTextComposite{\`}{\ant@encoding}{O}{"00D2}
\DeclareTextComposite{\'}{\ant@encoding}{O}{"00D3}
\DeclareTextComposite{\^}{\ant@encoding}{O}{"00D4}
\DeclareTextComposite{\~}{\ant@encoding}{O}{"00D5}
\DeclareTextComposite{\"}{\ant@encoding}{O}{"00D6}
\DeclareTextSymbol{\texttimes}{\ant@encoding}{"00D7}
\DeclareTextSymbol{\O}{\ant@encoding}{"00D8}
\DeclareTextComposite{\`}{\ant@encoding}{U}{"00D9}
\DeclareTextComposite{\'}{\ant@encoding}{U}{"00DA}
\DeclareTextComposite{\^}{\ant@encoding}{U}{"00DB}
\DeclareTextComposite{\"}{\ant@encoding}{U}{"00DC}
\DeclareTextComposite{\'}{\ant@encoding}{Y}{"00DD}
\DeclareTextSymbol{\TH}{\ant@encoding}{"00DE}
\DeclareTextCommand{\SS}{\ant@encoding}{SS}
\DeclareTextSymbol{\ss}{\ant@encoding}{"00DF}
\DeclareTextComposite{\`}{\ant@encoding}{a}{"00E0}
\DeclareTextComposite{\'}{\ant@encoding}{a}{"00E1}
\DeclareTextComposite{\^}{\ant@encoding}{a}{"00E2}
\DeclareTextComposite{\~}{\ant@encoding}{a}{"00E3}
\DeclareTextComposite{\"}{\ant@encoding}{a}{"00E4}
\DeclareTextComposite{\r}{\ant@encoding}{a}{"00E5}
\DeclareTextComposite{\c}{\ant@encoding}{c}{"00E7}
\DeclareTextSymbol{\ae}{\ant@encoding}{"00E6}
\DeclareTextComposite{\`}{\ant@encoding}{e}{"00E8}
\DeclareTextComposite{\'}{\ant@encoding}{e}{"00E9}
\DeclareTextComposite{\^}{\ant@encoding}{e}{"00EA}
\DeclareTextComposite{\"}{\ant@encoding}{e}{"00EB}
\DeclareTextComposite{\`}{\ant@encoding}{i}{"00EC}
\DeclareTextComposite{\`}{\ant@encoding}{\i}{"00EC}
\DeclareTextComposite{\'}{\ant@encoding}{i}{"00ED}
\DeclareTextComposite{\'}{\ant@encoding}{\i}{"00ED}
\DeclareTextComposite{\^}{\ant@encoding}{i}{"00EE}
\DeclareTextComposite{\^}{\ant@encoding}{\i}{"00EE}
\DeclareTextComposite{\"}{\ant@encoding}{i}{"00EF}
\DeclareTextComposite{\"}{\ant@encoding}{\i}{"00EF}
\DeclareTextSymbol{\dh}{\ant@encoding}{"00F0}
\DeclareTextComposite{\~}{\ant@encoding}{n}{"00F1}
\DeclareTextComposite{\`}{\ant@encoding}{o}{"00F2}
\DeclareTextComposite{\'}{\ant@encoding}{o}{"00F3}
\DeclareTextComposite{\^}{\ant@encoding}{o}{"00F4}
\DeclareTextComposite{\~}{\ant@encoding}{o}{"00F5}
\DeclareTextComposite{\"}{\ant@encoding}{o}{"00F6}
\DeclareTextSymbol{\textdiv}{\ant@encoding}{"00F7}
\DeclareTextSymbol{\o}{\ant@encoding}{"00F8}
\DeclareTextComposite{\`}{\ant@encoding}{u}{"00F9}
\DeclareTextComposite{\'}{\ant@encoding}{u}{"00FA}
\DeclareTextComposite{\^}{\ant@encoding}{u}{"00FB}
\DeclareTextComposite{\"}{\ant@encoding}{u}{"00FC}
\DeclareTextComposite{\'}{\ant@encoding}{y}{"00FD}
\DeclareTextSymbol{\th}{\ant@encoding}{"00FE}
\DeclareTextComposite{\"}{\ant@encoding}{y}{"00FF}
%</UT1|T1>
%    \end{macrocode}
%
% \subsection{Latin Extended A}
%
%    \begin{macrocode}
%<*UT1|T1>
%<!T1>\DeclareTextComposite{\=}{\ant@encoding}{A}{"0100}
%<!T1>\DeclareTextComposite{\=}{\ant@encoding}{a}{"0101}
\DeclareTextComposite{\u}{\ant@encoding}{A}{"0102}
\DeclareTextComposite{\u}{\ant@encoding}{a}{"0103}
\DeclareTextComposite{\k}{\ant@encoding}{A}{"0104}
\DeclareTextComposite{\k}{\ant@encoding}{a}{"0105}
\DeclareTextComposite{\'}{\ant@encoding}{C}{"0106}
\DeclareTextComposite{\'}{\ant@encoding}{c}{"0107}
\DeclareTextComposite{\v}{\ant@encoding}{C}{"0108}
\DeclareTextComposite{\v}{\ant@encoding}{c}{"0109}
\DeclareTextComposite{\v}{\ant@encoding}{D}{"010E}
\DeclareTextComposite{\v}{\ant@encoding}{d}{"010F}
\DeclareTextSymbol{\DJ}{\ant@encoding}{"0110}
\DeclareTextSymbol{\dj}{\ant@encoding}{"0111}
%<!T1>\DeclareTextComposite{\=}{\ant@encoding}{E}{"0112}
%<!T1>\DeclareTextComposite{\=}{\ant@encoding}{e}{"0113}
%<!T1>\DeclareTextComposite{\u}{\ant@encoding}{E}{"0114}
%<!T1>\DeclareTextComposite{\u}{\ant@encoding}{e}{"0115}
%<!T1>\DeclareTextComposite{\.}{\ant@encoding}{E}{"0116}
%<!T1>\DeclareTextComposite{\.}{\ant@encoding}{e}{"0117}
\DeclareTextComposite{\k}{\ant@encoding}{E}{"0118}
\DeclareTextComposite{\k}{\ant@encoding}{e}{"0119}
\DeclareTextComposite{\v}{\ant@encoding}{E}{"011A}
\DeclareTextComposite{\v}{\ant@encoding}{e}{"011B}
%<!T1>\DeclareTextComposite{\V}{\ant@encoding}{G}{"010C}
%<!T1>\DeclareTextComposite{\v}{\ant@encoding}{g}{"010D}
\DeclareTextComposite{\u}{\ant@encoding}{G}{"011E}
\DeclareTextComposite{\u}{\ant@encoding}{g}{"011F}
%<*!T1>
\DeclareTextComposite{\.}{\ant@encoding}{G}{"0120}
\DeclareTextComposite{\.}{\ant@encoding}{g}{"0121}
\DeclareTextComposite{\c}{\ant@encoding}{G}{"0122}
\DeclareTextComposite{\c}{\ant@encoding}{g}{"0123}
\DeclareTextComposite{\^}{\ant@encoding}{H}{"0124}
\DeclareTextComposite{\^}{\ant@encoding}{h}{"0125}
\DeclareTextComposite{\~}{\ant@encoding}{I}{"0128}
\DeclareTextComposite{\~}{\ant@encoding}{i}{"0129}
\DeclareTextComposite{\=}{\ant@encoding}{I}{"012A}
\DeclareTextComposite{\=}{\ant@encoding}{i}{"012B}
\DeclareTextComposite{\u}{\ant@encoding}{I}{"012C}
\DeclareTextComposite{\u}{\ant@encoding}{i}{"012D}
\DeclareTextComposite{\k}{\ant@encoding}{I}{"012E}
\DeclareTextComposite{\k}{\ant@encoding}{i}{"012F}
%</!T1>
\DeclareTextComposite{\.}{\ant@encoding}{I}{"0130}
%</UT1|T1>
%<!LGR>\DeclareTextSymbol{\i}{\ant@encoding}{"0131}
%<*UT1|T1>
\DeclareTextSymbol{\IJ}{\ant@encoding}{"0132}
\DeclareTextSymbol{\ij}{\ant@encoding}{"0133}
%<*!T1>
\DeclareTextComposite{\^}{\ant@encoding}{J}{"0134}
\DeclareTextComposite{\^}{\ant@encoding}{j}{"0135}
\DeclareTextComposite{\c}{\ant@encoding}{K}{"0136}
\DeclareTextComposite{\c}{\ant@encoding}{k}{"0137}
%</!T1>
\DeclareTextComposite{\'}{\ant@encoding}{L}{"0139}
\DeclareTextComposite{\'}{\ant@encoding}{l}{"013A}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{L}{"013B}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{l}{"013C}
\DeclareTextComposite{\v}{\ant@encoding}{L}{"013D}
\DeclareTextComposite{\v}{\ant@encoding}{l}{"013E}
\DeclareTextSymbol{\L}{\ant@encoding}{"0141}
\DeclareTextSymbol{\l}{\ant@encoding}{"0142}
\DeclareTextComposite{\'}{\ant@encoding}{N}{"0143}
\DeclareTextComposite{\'}{\ant@encoding}{n}{"0144}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{N}{"0145}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{n}{"0146}
\DeclareTextComposite{\v}{\ant@encoding}{N}{"0147}
\DeclareTextComposite{\v}{\ant@encoding}{n}{"0148}
\DeclareTextSymbol{\NG}{\ant@encoding}{"014A}
\DeclareTextSymbol{\ng}{\ant@encoding}{"014B}
%<*!T1>
\DeclareTextComposite{\=}{\ant@encoding}{O}{"014C}
\DeclareTextComposite{\=}{\ant@encoding}{o}{"014D}
\DeclareTextComposite{\u}{\ant@encoding}{O}{"014E}
\DeclareTextComposite{\u}{\ant@encoding}{o}{"014F}
%</!T1>
\DeclareTextComposite{\H}{\ant@encoding}{O}{"0150}
\DeclareTextComposite{\H}{\ant@encoding}{o}{"0151}
\DeclareTextSymbol{\OE}{\ant@encoding}{"0152}
\DeclareTextSymbol{\oe}{\ant@encoding}{"0153}
\DeclareTextComposite{\'}{\ant@encoding}{R}{"0154}
\DeclareTextComposite{\'}{\ant@encoding}{r}{"0155}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{R}{"0156}
%<!T1>\DeclareTextComposite{\c}{\ant@encoding}{r}{"0157}
\DeclareTextComposite{\v}{\ant@encoding}{R}{"0158}
\DeclareTextComposite{\v}{\ant@encoding}{r}{"0159}
\DeclareTextComposite{\'}{\ant@encoding}{S}{"015A}
\DeclareTextComposite{\'}{\ant@encoding}{s}{"015B}
%<!T1>\DeclareTextComposite{\^}{\ant@encoding}{S}{"015C}
%<!T1>\DeclareTextComposite{\^}{\ant@encoding}{s}{"015D}
\DeclareTextComposite{\c}{\ant@encoding}{S}{"015E}
\DeclareTextComposite{\c}{\ant@encoding}{s}{"015F}
\DeclareTextComposite{\v}{\ant@encoding}{S}{"0160}
\DeclareTextComposite{\v}{\ant@encoding}{s}{"0161}
\DeclareTextComposite{\c}{\ant@encoding}{T}{"0162}
\DeclareTextComposite{\c}{\ant@encoding}{t}{"0163}
\DeclareTextComposite{\v}{\ant@encoding}{T}{"0164}
\DeclareTextComposite{\v}{\ant@encoding}{t}{"0165}
%<*!T1>
\DeclareTextComposite{\~}{\ant@encoding}{U}{"0168}
\DeclareTextComposite{\~}{\ant@encoding}{u}{"0169}
\DeclareTextComposite{\=}{\ant@encoding}{U}{"016A}
\DeclareTextComposite{\=}{\ant@encoding}{u}{"016B}
\DeclareTextComposite{\u}{\ant@encoding}{U}{"016C}
\DeclareTextComposite{\u}{\ant@encoding}{u}{"016D}
\DeclareTextComposite{\r}{\ant@encoding}{U}{"016E}
\DeclareTextComposite{\r}{\ant@encoding}{u}{"016F}
%</!T1>
\DeclareTextComposite{\H}{\ant@encoding}{U}{"0170}
\DeclareTextComposite{\H}{\ant@encoding}{u}{"0171}
%<*!T1>
\DeclareTextComposite{\k}{\ant@encoding}{U}{"0172}
\DeclareTextComposite{\k}{\ant@encoding}{u}{"0173}
\DeclareTextComposite{\^}{\ant@encoding}{W}{"0174}
\DeclareTextComposite{\^}{\ant@encoding}{w}{"0175}
\DeclareTextComposite{\^}{\ant@encoding}{Y}{"0176}
\DeclareTextComposite{\^}{\ant@encoding}{y}{"0177}
%</!T1>
\DeclareTextComposite{\"}{\ant@encoding}{Y}{"0178}
\DeclareTextComposite{\'}{\ant@encoding}{Z}{"0179}
\DeclareTextComposite{\'}{\ant@encoding}{z}{"017A}
\DeclareTextComposite{\.}{\ant@encoding}{Z}{"017B}
\DeclareTextComposite{\.}{\ant@encoding}{z}{"017C}
\DeclareTextComposite{\v}{\ant@encoding}{Z}{"017D}
\DeclareTextComposite{\v}{\ant@encoding}{z}{"017E}
%</UT1|T1>
%    \end{macrocode}
%
% \subsection{Latin Extended B}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textflorin}{\ant@encoding}{"0192}
\DeclareTextComposite{\=}{\ant@encoding}{Y}{"0232}
\DeclareTextComposite{\=}{\ant@encoding}{y}{"0233}
%</UT1>
%    \end{macrocode}
%
% \subsection{Spacing modifier letters}
%
% This area is used once again to map some text commands for ASCII-styled
% accents, originally defined in the TS1 encoding.
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textasciicaron}{\ant@encoding}{"02C7}
\DeclareTextSymbol{\textasciibreve}{\ant@encoding}{"02D8}
\DeclareTextSymbol{\textacutedbl}{\ant@encoding}{"02DD}
%</UT1>
%    \end{macrocode}
%
% \subsection{Thai}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textbaht}{\ant@encoding}{"0E3F}
%</UT1>
%    \end{macrocode}
%
% \subsection{General punctuation}
%
%    \begin{macrocode}
\DeclareTextSymbol{\textcompwordmark}{\ant@encoding}{"200C}
%<UT1>\DeclareTextSymbol{\textbardbl}{\ant@encoding}{"2016}
%<UT1>\DeclareTextSymbol{\textdagger}{\ant@encoding}{"2020}
%<UT1>\DeclareTextSymbol{\textdaggerdbl}{\ant@encoding}{"2021}
%<UT1>\DeclareTextSymbol{\textbullet}{\ant@encoding}{"2022}
%<UT1|LGR>\DeclareTextSymbol{\textperthousand}{\ant@encoding}{"2030}
%<UT1>\DeclareTextSymbol{\textpertenthousand}{\ant@encoding}{"2031}
%<UT1|T1>\DeclareTextSymbol{\guilsinglleft}{\ant@encoding}{"2039}
%<UT1|T1>\DeclareTextSymbol{\guilsinglright}{\ant@encoding}{"203A}
\DeclareTextSymbol{\textendash}{\ant@encoding}{"2013}
\DeclareTextSymbol{\textemdash}{\ant@encoding}{"2014}
\DeclareTextSymbol{\textquoteleft}{\ant@encoding}{"2018}
\DeclareTextSymbol{\textquoteright}{\ant@encoding}{"2019}
%<UT1|T1>\DeclareTextSymbol{\quotesinglbase}{\ant@encoding}{"201A}
\DeclareTextSymbol{\textquotedblleft}{\ant@encoding}{"201C}
\DeclareTextSymbol{\textquotedblright}{\ant@encoding}{"201D}
%<!LGR>\DeclareTextSymbol{\quotedblbase}{\ant@encoding}{"201E}
%<UT1>\DeclareTextSymbol{\textreferencemark}{\ant@encoding}{"203B}
%<UT1>\DeclareTextSymbol{\textinterrobang}{\ant@encoding}{"203D}
%<UT1>\DeclareTextSymbol{\textlquill}{\ant@encoding}{"2045}
%<UT1>\DeclareTextSymbol{\textrquill}{\ant@encoding}{"2046}
%    \end{macrocode}
%
% \subsection{Currency symbols}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textcolonmonetary}{\ant@encoding}{"20A1}
\DeclareTextSymbol{\textlira}{\ant@encoding}{"20A4}
\DeclareTextSymbol{\textnaira}{\ant@encoding}{"20A6}
\DeclareTextSymbol{\textpeso}{\ant@encoding}{"20A7}
\DeclareTextSymbol{\textwon}{\ant@encoding}{"20A9}
\DeclareTextSymbol{\textdong}{\ant@encoding}{"20AB}
%</UT1>
%<UT1|LGR>\DeclareTextSymbol{\texteuro}{\ant@encoding}{"20AC}
%<UT1>\DeclareTextSymbol{\textguarani}{\ant@encoding}{"20B2}
%    \end{macrocode}
%
% \subsection{Combining diacritical marks for symbols}
%
%    \begin{macrocode}
%<UT1>\DeclareTextSymbol{\textbigcircle}{\ant@encoding}{"20DD}
\DeclareTextCommand{\textcircled}{\ant@encoding}[1]{{%
   \ooalign{%
      \hfil \raise .07ex\hbox {\upshape#1}\hfil \crcr
      \char 79   % '117 = "4F
   }%
}}
%    \end{macrocode}
%
% \subsection{Letterlike symbols}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textcelsius}{\ant@encoding}{"2103}
%</UT1>
%<UT1|T2A>\DeclareTextSymbol{\textnumero}{\ant@encoding}{"2116}
%<*UT1>
\DeclareTextSymbol{\textcircledP}{\ant@encoding}{"2117}
\DeclareTextSymbol{\textrecipe}{\ant@encoding}{"211E}
\DeclareTextSymbol{\textservicemark}{\ant@encoding}{"2120}
\DeclareTextSymbol{\texttrademark}{\ant@encoding}{"2122}
\DeclareTextSymbol{\textohm}{\ant@encoding}{"2126}
\DeclareTextSymbol{\textmho}{\ant@encoding}{"2127}
\DeclareTextSymbol{\textestimated}{\ant@encoding}{"212E}
%</UT1>
%    \end{macrocode}
%
% \subsection{Arrows}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textleftarrow}{\ant@encoding}{"2190}
\DeclareTextSymbol{\textrightarrow}{\ant@encoding}{"2191}
\DeclareTextSymbol{\textuparrow}{\ant@encoding}{"2192}
\DeclareTextSymbol{\textdownarrow}{\ant@encoding}{"2193}
%</UT1>
%    \end{macrocode}
%
% \subsection{Mathematical Operators}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textminus}{\ant@encoding}{"2212}
\DeclareTextSymbol{\textsurd}{\ant@encoding}{"221A}
%</UT1>
%<UT1|T2A>\DeclareTextSymbol{\textlangle}{\ant@encoding}{"2329}
%<UT1|T2A>\DeclareTextSymbol{\textrangle}{\ant@encoding}{"232A}
%    \end{macrocode}
%
% \subsection{Control Pictures}
%
%    \begin{macrocode}
%<UT1|T1|T2A>\DeclareTextSymbol{\textvisiblespace}{\ant@encoding}{"2423}
%    \end{macrocode}
%
% \subsection{Geometric Shapes}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textopenbullet}{\ant@encoding}{"25E6}
%</UT1>
%    \end{macrocode}
%
% \subsection{Miscellaneous Symbols}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textmusicalnote}{\ant@encoding}{"266A}
%</UT1>
%    \end{macrocode}
%
% \subsection{CJK Symbols and Punctuation}
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextSymbol{\textlbrackdbl}{\ant@encoding}{"301A}
\DeclareTextSymbol{\textrbrackdbl}{\ant@encoding}{"301B}
%</UT1>
%    \end{macrocode}
%
% \subsection{Private use area}
%
% PUA characters are mapped according to the Adobe Glyph List.
% Don't use the following text commands, if your font really doesn't
% include the corresponding characters.
%
%    \begin{macrocode}
%<UT1|T1|T2A>\DeclareTextSymbol{\j}{\ant@encoding}{"F6BE}
%<*UT1>
\DeclareTextSymbol{\textgravedbl}{\ant@encoding}{"F6D6}
\DeclareTextSymbol{\textdollaroldstyle}{\ant@encoding}{"F724}
\DeclareTextSymbol{\textzerooldstyle}{\ant@encoding}{"F730}
\DeclareTextSymbol{\textoneoldstyle}{\ant@encoding}{"F731}
\DeclareTextSymbol{\texttwooldstyle}{\ant@encoding}{"F732}
\DeclareTextSymbol{\textthreeoldstyle}{\ant@encoding}{"F733}
\DeclareTextSymbol{\textfouroldstyle}{\ant@encoding}{"F734}
\DeclareTextSymbol{\textfiveoldstyle}{\ant@encoding}{"F735}
\DeclareTextSymbol{\textsixoldstyle}{\ant@encoding}{"F736}
\DeclareTextSymbol{\textsevenoldstyle}{\ant@encoding}{"F737}
\DeclareTextSymbol{\texteightoldstyle}{\ant@encoding}{"F738}
\DeclareTextSymbol{\textnineoldstyle}{\ant@encoding}{"F739}
\DeclareTextSymbol{\textcentoldstyle}{\ant@encoding}{"F7A2}
%</UT1>
%    \end{macrocode}
%
% Again, capital accents are mapped according to older Adobe
% specifications and their real placement in Adobe's fonts.
%
%    \begin{macrocode}
%<*UT1>
\DeclareTextCommand{\capitalogonek}{\ant@encoding}[1]
   {{\ooalign{\null#1\crcr\hidewidth\noocpchar{"EFF1}\hidewidth}}}
\DeclareTextCommand{\capitalcedilla}{\ant@encoding}[1]
   {{\ooalign{\null#1\crcr\hidewidth\noocpchar{"EFF2}\hidewidth}}}
\DeclareTextAccent{\capitalgrave}{\ant@encoding}{"F6CE}
\DeclareTextAccent{\capitalacute}{\ant@encoding}{"F6C9}
\DeclareTextAccent{\capitalcircumflex}{\ant@encoding}{"EFF7}
\DeclareTextAccent{\capitaltilde}{\ant@encoding}{"EFF5}
\DeclareTextAccent{\capitaldieresis}{\ant@encoding}{"F6CB}
\DeclareTextAccent{\capitalhungarumlaut}{\ant@encoding}{"F6CF}
\DeclareTextAccent{\capitalring}{\ant@encoding}{"EFF3}
\DeclareTextAccent{\capitalcaron}{\ant@encoding}{"F6CA}
\DeclareTextAccent{\capitalbreve}{\ant@encoding}{"EFEE}
\DeclareTextAccent{\capitalmacron}{\ant@encoding}{"F6D0}
\DeclareTextAccent{\capitaldotaccent}{\ant@encoding}{"EFED}
%</UT1>
%    \end{macrocode}
%
% \subsection{Final operations}
%
% Finally undefine +\ant@encoding+.
%
%    \begin{macrocode}
\let\ant@encoding\@undefined
%    \end{macrocode}
%</UT1|T1|T2A|LGR>
%
% \Finale
%
\endinput
%%
%% End of file `antenc.dtx'.
