%%
\ProvidesFile{OT1fbb-LF.fd}
    [2020/07/01 (msharpe)  Font definitions for OT1/fbb-LF.]

\expandafter\ifx\csname fbb@@swashQ\endcsname\relax%
	\global\let\fbb@@swashQ\@empty
\fi

\expandafter\ifx\csname fbb@scale\endcsname\relax
    \let\fbb@@scale\@empty
\else
    \edef\fbb@@scale{s*[\csname fbb@scale\endcsname]}%
\fi

\expandafter\ifx\csname fbb@altP\endcsname\relax
    \let\fbb@@altP\@empty
\else
    \let\fbb@@altP\fbb@altP
\fi

\DeclareFontFamily{OT1}{fbb-LF}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-lf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-lf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-lf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-lf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{scit}{
      <-> \fbb@@scale fbb-Italic-lf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{sc}{
      <-> \fbb@@scale fbb\fbb@@alt-Regular-lf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{n}{
      <-> \fbb@@scale fbb\fbb@@alt-Regular-lf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{it}{
      <-> \fbb@@scale fbb-Italic-lf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{sl}{
      <-> ssub * fbb-LF/m/it
}{}

\DeclareFontShape{OT1}{fbb-LF}{m}{scsl}{
      <-> ssub * fbb-LF/m/scit
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{scit}{
      <-> \fbb@@scale fbb-BoldItalic-lf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{sc}{
      <-> \fbb@@scale fbb\fbb@@alt-Bold-lf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{it}{
      <-> \fbb@@scale fbb-BoldItalic-lf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{n}{
      <-> \fbb@@scale fbb\fbb@@alt-Bold-lf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{sl}{
      <-> ssub * fbb-LF/b/it
}{}

\DeclareFontShape{OT1}{fbb-LF}{b}{scsl}{
      <-> ssub * fbb-LF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{n}{
      <-> ssub * fbb-LF/b/n
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{sl}{
      <-> ssub * fbb-LF/b/sl
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{it}{
      <-> ssub * fbb-LF/b/it
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{scit}{
      <-> ssub * fbb-LF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{scsl}{
      <-> ssub * fbb-LF/b/scsl
}{}

\DeclareFontShape{OT1}{fbb-LF}{bx}{sc}{
      <-> ssub * fbb-LF/b/sc
}{}

\endinput
