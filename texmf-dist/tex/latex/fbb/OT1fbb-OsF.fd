%%
\ProvidesFile{OT1fbb-OsF.fd}
    [2020/07/01 (msharpe)  Font definitions for OT1/fbb-OsF.]

\expandafter\ifx\csname fbb@@swashQ\endcsname\relax%
	\global\let\fbb@@swashQ\@empty
\fi

\expandafter\ifx\csname fbb@scale\endcsname\relax
    \let\fbb@@scale\@empty
\else
    \edef\fbb@@scale{s*[\csname fbb@scale\endcsname]}%
\fi
\expandafter\ifx\csname fbb@altP\endcsname\relax
    \let\fbb@@altP\@empty
\else
    \let\fbb@@altP\fbb@altP
\fi

\DeclareFontFamily{OT1}{fbb-OsF}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-osf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-osf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{nw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-osf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{sw}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-osf-swash-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{sc}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-osf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{scit}{
      <-> \fbb@@scale fbb-BoldItalic-osf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{it}{
      <-> \fbb@@scale fbb-BoldItalic-osf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{n}{
      <-> \fbb@@scale fbb\fbb@@altP-Bold-osf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{sl}{
      <-> ssub * fbb-OsF/b/it
}{}

\DeclareFontShape{OT1}{fbb-OsF}{b}{scsl}{
      <-> ssub * fbb-OsF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{it}{
      <-> \fbb@@scale fbb-Italic-osf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{n}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-osf\fbb@@swashQ-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{sc}{
      <-> \fbb@@scale fbb\fbb@@altP-Regular-osf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{scit}{
      <-> \fbb@@scale fbb-Italic-osf-sc-ot1
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{sl}{
      <-> ssub * fbb-OsF/m/it
}{}

\DeclareFontShape{OT1}{fbb-OsF}{m}{scsl}{
      <-> ssub * fbb-OsF/m/scit
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{scsl}{
      <-> ssub * fbb-OsF/b/scsl
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{sc}{
      <-> ssub * fbb-OsF/b/sc
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{scit}{
      <-> ssub * fbb-OsF/b/scit
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{it}{
      <-> ssub * fbb-OsF/b/it
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{n}{
      <-> ssub * fbb-OsF/b/n
}{}

\DeclareFontShape{OT1}{fbb-OsF}{bx}{sl}{
      <-> ssub * fbb-OsF/b/sl
}{}

\endinput
