%%
%% This is file `notesslides.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% notesslides.dtx  (with options: `cls')
%% 
\ProvidesExplClass{notesslides}{2022/09/14}{3.2.0}{notesslides Class}
\RequirePackage{l3keys2e}

\keys_define:nn{notesslides / cls}{
  class   .str_set_x:N = \c__notesslides_class_str,
  notes   .bool_set:N  = \c__notesslides_notes_bool ,
  slides  .code:n      = { \bool_set_false:N \c__notesslides_notes_bool },
  docopt  .str_set_x:N = \c__notesslides_docopt_str,
  unknown .code:n      = {
    \PassOptionsToPackage{\CurrentOption}{document-structure}
    \PassOptionsToClass{\CurrentOption}{beamer}
    \PassOptionsToPackage{\CurrentOption}{notesslides}
    \PassOptionsToPackage{\CurrentOption}{stex}
  }
}
\ProcessKeysOptions{ notesslides / cls }

\str_if_empty:NF \c__notesslides_class_str {
  \PassOptionsToPackage{class=\c__notesslides_class_str}{document-structure}
}

\exp_args:No \str_if_eq:nnT\c__notesslides_class_str{book}{
  \PassOptionsToPackage{defaulttopsect=part}{notesslides}
}
\exp_args:No \str_if_eq:nnT\c__notesslides_class_str{report}{
  \PassOptionsToPackage{defaulttopsect=part}{notesslides}
}

\RequirePackage{stex}
\stex_html_backend:T {
  \bool_set_true:N\c__notesslides_notes_bool
}

\bool_if:NTF \c__notesslides_notes_bool {
  \PassOptionsToPackage{notes=true}{notesslides}
  \message{notesslides.cls:~Formatting~course~materials~in~notes~mode}
}{
  \PassOptionsToPackage{notes=false}{notesslides}
  \message{notesslides.cls:~Formatting~course~materials~in~slides~mode}
}
\bool_if:NTF \c__notesslides_notes_bool {
  \str_if_empty:NT \c__notesslides_class_str {
    \str_set:Nn \c__notesslides_class_str {article}
  }
  \exp_after:wN\LoadClass\exp_after:wN[\c__notesslides_docopt_str]
    {\c__notesslides_class_str}
}{
  \LoadClass[10pt,notheorems,xcolor={dvipsnames,svgnames}]{beamer}
  \newcounter{Item}
  \newcounter{paragraph}
  \newcounter{subparagraph}
  \newcounter{Hfootnote}
}
\RequirePackage{document-structure}
\RequirePackage{notesslides}
\endinput
%%
%% End of file `notesslides.cls'.
