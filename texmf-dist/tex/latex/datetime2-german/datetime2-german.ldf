%%
%% This is file `datetime2-german.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-german.dtx  (with options: `datetime2-german.ldf,package')
%% 
%%  datetime2-german.dtx
%%  Copyright 2015 Nicola Talbot
%%  Copyright 2017-19 Sebastian Friedl
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3c
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3c or later is part of all distributions of LaTeX
%%  version 2008/05/04 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%%  Current maintainer of the work is Sebastian Friedl
%% 
%%  This work consists of the files        and the derived files:
%%   - datetime2-german.ins,                - datetime2-german-base.ldf,
%%   - datetime2-german-base.dtx,           - datetime2-german-base-ascii.ldf,
%%   - datetime2-german-base-ascii.dtx,     - datetime2-german-base-utf8.ldf,
%%   - datetime2-german-base-utf8.dtx,      - datetime2-german.ldf,
%%   - datetime2-german.ldf,                - datetime2-de-DE.ldf,
%%   - datetime2-de-DE.ldf,                 - datetime2-de-AT.ldf and
%%   - datetime2-de-AT.ldf,                 - datetime2-de-CH.ldf
%%   - datetime2-de-CH.ldf and
%%   - datetime2-german-doc.dtx
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\ProvidesDateTimeModule{german}[2019/12/13 v3.0]
\RequireDateTimeModule{german-base}
\newcommand*{\DTMgermandowdaysep}{,\space}
\newcommand*{\DTMgermandaymonthsep}{.\DTMtexorpdfstring{\protect~}{\space}}
\newcommand*{\DTMgermanmonthyearsep}{\space}
\newcommand*{\DTMgermandatetimesep}{,\space}
\newcommand*{\DTMgermantimezonesep}{\space}
\newcommand*{\DTMgermandatesep}{.}
\newcommand*{\DTMgermantimesep}{:}
\DTMdefkey{german}{dowdaysep}{\renewcommand*{\DTMgermandowdaysep}{#1}}
\DTMdefkey{german}{daymonthsep}{\renewcommand*{\DTMgermandaymonthsep}{#1}}
\DTMdefkey{german}{monthyearsep}{\renewcommand*{\DTMgermanmonthyearsep}{#1}}
\DTMdefkey{german}{datetimesep}{\renewcommand*{\DTMgermandatetimesep}{#1}}
\DTMdefkey{german}{timezonesep}{\renewcommand*{\DTMgermantimezonesep}{#1}}
\DTMdefkey{german}{datesep}{\renewcommand*{\DTMgermandatesep}{#1}}
\DTMdefkey{german}{timesep}{\renewcommand*{\DTMgermantimesep}{#1}}
\DTMdefboolkey{german}{abbr}[true]{}
\DTMsetbool{german}{abbr}{false}
\DTMdefboolkey{german}{mapzone}[true]{}
\DTMsetbool{german}{mapzone}{true}
\DTMdefboolkey{german}{showdayofmonth}[true]{}
\DTMsetbool{german}{showdayofmonth}{true}
\DTMdefboolkey{german}{showyear}[true]{}
\DTMsetbool{german}{showyear}{true}
\DTMnewstyle
 {german}% label
 {% date style
   \renewcommand*\DTMdisplaydate[4]{%
     \ifDTMshowdow
       \ifnum##4>-1
         \DTMifbool{german}{abbr}%
         {\DTMgermanshortweekdayname{##4}}%
         {\DTMgermanweekdayname{##4}}%
         \DTMgermandowdaysep
       \fi
     \fi
     %
     \DTMifbool{german}{showdayofmonth}%
     {\DTMgermanordinal{##3}\DTMgermandaymonthsep}%
     {}%
     %
     \DTMifbool{german}{abbr}%
     {\DTMgermanshortmonthname{##2}}%
     {\DTMgermanmonthname{##2}}%
     %
     \DTMifbool{german}{showyear}%
     {%
       \DTMgermanmonthyearsep%
       \number##1 % space intended
     }%
     {}%
   }%
   \renewcommand*\DTMDisplaydate[4]{%
     \ifDTMshowdow
       \ifnum##4>-1
         \DTMifbool{german}{abbr}%
         {\DTMgermanshortweekdayname{##4}}%
         {\DTMgermanweekdayname{##4}}%
         \DTMgermandowdaysep
       \fi
     \fi
     %
     \DTMifbool{german}{showdayofmonth}%
     {\DTMgermanordinal{##3}\DTMgermandaymonthsep}%
     {}%
     %
     \DTMifbool{german}{abbr}%
     {\DTMgermanshortmonthname{##2}}%
     {\DTMgermanmonthname{##2}}%
     %
     \DTMifbool{german}{showyear}%
     {%
       \DTMgermanmonthyearsep%
       \number##1 % space intended
     }%
     {}%
    }%
   }%
 {% time style (use default)
   \renewcommand*\DTMdisplaytime[3]{%
    \DTMtwodigits{##1}%
    \DTMgermantimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMgermantimesep\DTMtwodigits{##3}\fi
   }%
 }%
 {% zone style
   \DTMresetzones
   \DTMgermanzonemaps
   \renewcommand*{\DTMdisplayzone}[2]{%
     \DTMifbool{german}{mapzone}%
     {\DTMusezonemapordefault{##1}{##2}}%
     {%
       \ifnum##1<0\else+\fi\DTMtwodigits{##1}%
       \ifDTMshowzoneminutes\DTMgermantimesep\DTMtwodigits{##2}\fi
     }%
   }%
 }%
 {% full style
   \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate
     \DTMdisplaydate{##1}{##2}{##3}{##4}%
     \DTMgermandatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
     \DTMgermantimezonesep
     \DTMdisplayzone{##8}{##9}%
    \fi
   }%
   \renewcommand*{\DTMDisplay}[9]{%
    \ifDTMshowdate
     \DTMDisplaydate{##1}{##2}{##3}{##4}%
     \DTMgermandatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
     \DTMgermantimezonesep
     \DTMdisplayzone{##8}{##9}%
    \fi
   }%
 }%
\DTMnewstyle
 {german-numeric}% label
 {% date style
    \renewcommand*\DTMdisplaydate[4]{%
      \ifDTMshowdow
        \ifnum##4>-1
          \DTMifbool{german}{abbr}%
          {\DTMgermanshortweekdayname{##4}}%
          {\DTMgermanweekdayname{##4}}%
          \DTMgermandowdaysep
        \fi
      \fi
      %
      \DTMifbool{german}{showdayofmonth}%
      {%
        \DTMtwodigits{##3}%
        \DTMgermandatesep
      }%
      {}%
      \DTMtwodigits{##2}%
      \DTMgermandatesep%
      \DTMifbool{german}{showyear}%
      {%
        \DTMifbool{german}{abbr}%
        {\DTMtwodigits{##1}}%
        {\number##1 }% space intended
      }%
      {}%
    }%
    \renewcommand*{\DTMDisplaydate}[4]{\DTMdisplaydate{##1}{##2}{##3}{##4}}%
 }%
 {% time style
    \renewcommand*\DTMdisplaytime[3]{%
      \DTMtwodigits{##1}%
      \DTMgermantimesep\DTMtwodigits{##2}%
      \ifDTMshowseconds\DTMgermantimesep\DTMtwodigits{##3}\fi
    }%
 }%
 {% zone style
   \DTMresetzones
   \DTMgermanzonemaps
   \renewcommand*{\DTMdisplayzone}[2]{%
     \DTMifbool{german}{mapzone}%
     {\DTMusezonemapordefault{##1}{##2}}%
     {%
       \ifnum##1<0\else+\fi\DTMtwodigits{##1}%
       \ifDTMshowzoneminutes\DTMgermantimesep\DTMtwodigits{##2}\fi
     }%
   }%
 }%
 {% full style
   \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate
     \DTMdisplaydate{##1}{##2}{##3}{##4}%
     \DTMgermandatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
     \DTMgermantimezonesep
     \DTMdisplayzone{##8}{##9}%
    \fi
   }%
   \renewcommand*{\DTMDisplay}{\DTMdisplay}%
 }
\DTMifcaseregional
{}% do nothing
{\DTMsetstyle{german}}
{\DTMsetstyle{german-numeric}}
\ifcsundef{date\CurrentTrackedDialect}
{%
  \ifundef\dategerman
  {% do nothing
  }%
  {%
    \def\dategerman{%
      \DTMifcaseregional
      {}% do nothing
      {\DTMsetstyle{german}}%
      {\DTMsetstyle{german-numeric}}%
    }%
  }%
}%
{%
  \csdef{date\CurrentTrackedDialect}{%
    \DTMifcaseregional
    {}% do nothing
    {\DTMsetstyle{german}}%
    {\DTMsetstyle{german-numeric}}%
  }%
}%
\endinput
%%
%% End of file `datetime2-german.ldf'.
