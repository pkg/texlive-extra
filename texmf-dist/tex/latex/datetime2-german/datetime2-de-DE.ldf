%%
%% This is file `datetime2-de-DE.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-de-DE.dtx  (with options: `datetime2-de-DE.ldf,package')
%% 
%%  datetime2-german.dtx
%%  Copyright 2015 Nicola Talbot
%%  Copyright 2017-19 Sebastian Friedl
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3c
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3c or later is part of all distributions of LaTeX
%%  version 2008/05/04 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%%  Current maintainer of the work is Sebastian Friedl
%% 
%%  This work consists of the files        and the derived files:
%%   - datetime2-german.ins,                - datetime2-german-base.ldf,
%%   - datetime2-german-base.dtx,           - datetime2-german-base-ascii.ldf,
%%   - datetime2-german-base-ascii.dtx,     - datetime2-german-base-utf8.ldf,
%%   - datetime2-german-base-utf8.dtx,      - datetime2-german.ldf,
%%   - datetime2-german.ldf,                - datetime2-de-DE.ldf,
%%   - datetime2-de-DE.ldf,                 - datetime2-de-AT.ldf and
%%   - datetime2-de-AT.ldf,                 - datetime2-de-CH.ldf
%%   - datetime2-de-CH.ldf and
%%   - datetime2-german-doc.dtx
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\ProvidesDateTimeModule{de-DE}[2019/12/13 v3.0]
\RequireDateTimeModule{german-base}
\newcommand*{\DTMdeDEdowdaysep}{,\space}
\newcommand*{\DTMdeDEdaymonthsep}{.\DTMtexorpdfstring{\protect~}{\space}}
\newcommand*{\DTMdeDEmonthyearsep}{\space}
\newcommand*{\DTMdeDEdatetimesep}{,\space}
\newcommand*{\DTMdeDEtimezonesep}{\space}
\newcommand*{\DTMdeDEdatesep}{.}
\newcommand*{\DTMdeDEtimesep}{:}
\DTMdefkey{de-DE}{dowdaysep}{\renewcommand*{\DTMdeDEdowdaysep}{#1}}
\DTMdefkey{de-DE}{daymonthsep}{\renewcommand*{\DTMdeDEdaymonthsep}{#1}}
\DTMdefkey{de-DE}{monthyearsep}{\renewcommand*{\DTMdeDEmonthyearsep}{#1}}
\DTMdefkey{de-DE}{datetimesep}{\renewcommand*{\DTMdeDEdatetimesep}{#1}}
\DTMdefkey{de-DE}{timezonesep}{\renewcommand*{\DTMdeDEtimezonesep}{#1}}
\DTMdefkey{de-DE}{datesep}{\renewcommand*{\DTMdeDEdatesep}{#1}}
\DTMdefkey{de-DE}{timesep}{\renewcommand*{\DTMdeDEtimesep}{#1}}
\DTMdefboolkey{de-DE}{abbr}[true]{}
\DTMsetbool{de-DE}{abbr}{false}
\DTMdefboolkey{de-DE}{mapzone}[true]{}
\DTMsetbool{de-DE}{mapzone}{true}
\DTMdefboolkey{de-DE}{showdayofmonth}[true]{}
\DTMsetbool{de-DE}{showdayofmonth}{true}
\DTMdefboolkey{de-DE}{showyear}[true]{}
\DTMsetbool{de-DE}{showyear}{true}
\DTMnewstyle
{de-DE}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow
      \ifnum##4>-1
        \DTMifbool{de-DE}{abbr}%
        {\DTMgermanshortweekdayname{##4}}%
        {\DTMgermanweekdayname{##4}}%
        \DTMdeDEdowdaysep
      \fi
    \fi
    %
    \DTMifbool{de-DE}{showdayofmonth}%
    {\DTMgermanordinal{##3}\DTMdeDEdaymonthsep}%
    {}%
    %
    \DTMifbool{de-DE}{abbr}%
    {\DTMgermanshortmonthname{##2}}%
    {\DTMgermanmonthname{##2}}%
    %
    \DTMifbool{de-DE}{showyear}%
    {%
      \DTMdeDEmonthyearsep%
      \number##1 % space intended
    }%
    {}%
  }%
  \renewcommand*\DTMDisplaydate[4]{%
    \ifDTMshowdow
      \ifnum##4>-1
        \DTMifbool{de-DE}{abbr}%
        {\DTMgermanshortweekdayname{##4}}%
        {\DTMgermanweekdayname{##4}}%
        \DTMdeDEdowdaysep
      \fi
    \fi
    %
    \DTMifbool{de-DE}{showdayofmonth}%
    {\DTMgermanordinal{##3}\DTMdeDEdaymonthsep}%
    {}%
    %
    \DTMifbool{de-DE}{abbr}%
    {\DTMgermanshortmonthname{##2}}%
    {\DTMgermanmonthname{##2}}%
    %
    \DTMifbool{de-DE}{showyear}%
    {%
      \DTMdeDEmonthyearsep%
      \number##1 % space intended
    }%
    {}%
  }
}%
{% time style (use default)
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMtwodigits{##1}%
    \DTMdeDEtimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMdeDEtimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones
  \DTMgermanzonemaps
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{de-DE}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0\else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMdeDEtimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMdeDEdatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
      \DTMdeDEtimezonesep
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}[9]{%
    \ifDTMshowdate
      \DTMDisplaydate{##1}{##2}{##3}{##4}%
      \DTMdeDEdatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
      \DTMdeDEtimezonesep
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
}%
\DTMnewstyle
{de-DE-numeric}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow
      \ifnum##4>-1
        \DTMifbool{de-DE}{abbr}%
        {\DTMgermanshortweekdayname{##4}}%
        {\DTMgermanweekdayname{##4}}%
        \DTMdeDEdowdaysep
      \fi
    \fi
    %
    \DTMifbool{de-DE}{showdayofmonth}%
    {%
      \DTMtwodigits{##3}%
      \DTMdeDEdatesep
    }%
    {}%
    \DTMtwodigits{##2}%
    \DTMdeDEdatesep%
    \DTMifbool{de-DE}{showyear}%
    {%
      \DTMifbool{de-DE}{abbr}%
      {\DTMtwodigits{##1}}%
      {\number##1 }% space intended
    }%
    {}%
    }%
  \renewcommand*{\DTMDisplaydate}[4]{\DTMdisplaydate{##1}{##2}{##3}{##4}}%
}%
{% time style
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMtwodigits{##1}%
    \DTMdeDEtimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMdeDEtimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones
  \DTMgermanzonemaps
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{de-DE}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0\else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMgermantimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMdeDEdatetimesep
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone
      \DTMdeDEtimezonesep
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}{\DTMdisplay}%
}
\DTMifcaseregional
  {}% do nothing
  {\DTMsetstyle{de-DE}}%
  {\DTMsetstyle{de-DE-numeric}}%
\ifcsundef{date\CurrentTrackedDialect}
{%
  \ifundef\dategerman
  {}% do nothing
  {%
    \def\dategerman{%
      \DTMifcaseregional
      {}% do nothing
      {\DTMsetstyle{de-DE}}%
      {\DTMsetstyle{de-DE-numeric}}%
    }%
  }%
}%
{%
  \csdef{date\CurrentTrackedDialect}{%
    \DTMifcaseregional
    {}% do nothing
    {\DTMsetstyle{de-DE}}%
    {\DTMsetstyle{de-DE-numeric}}
  }%
}%
\endinput
%%
%% End of file `datetime2-de-DE.ldf'.
