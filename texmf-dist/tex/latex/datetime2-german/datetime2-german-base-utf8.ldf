%%
%% This is file `datetime2-german-base-utf8.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-german-base-utf8.dtx  (with options: `datetime2-german-base-utf8.ldf,package')
%% 
%%  datetime2-german.dtx
%%  Copyright 2015 Nicola Talbot
%%  Copyright 2017-19 Sebastian Friedl
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3c
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3c or later is part of all distributions of LaTeX
%%  version 2008/05/04 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%%  Current maintainer of the work is Sebastian Friedl
%% 
%%  This work consists of the files        and the derived files:
%%   - datetime2-german.ins,                - datetime2-german-base.ldf,
%%   - datetime2-german-base.dtx,           - datetime2-german-base-ascii.ldf,
%%   - datetime2-german-base-ascii.dtx,     - datetime2-german-base-utf8.ldf,
%%   - datetime2-german-base-utf8.dtx,      - datetime2-german.ldf,
%%   - datetime2-german.ldf,                - datetime2-de-DE.ldf,
%%   - datetime2-de-DE.ldf,                 - datetime2-de-AT.ldf and
%%   - datetime2-de-AT.ldf,                 - datetime2-de-CH.ldf
%%   - datetime2-de-CH.ldf and
%%   - datetime2-german-doc.dtx
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\ProvidesDateTimeModule{german-base-utf8}[2019/12/13 v3.0]
\newcommand*{\DTMgermanmonthname}[1]{%
  \ifcase#1
  \or
  Januar%
  \or
  Februar%
  \or
  März%
  \or
  April%
  \or
  Mai%
  \or
  Juni%
  \or
  Juli%
  \or
  August%
  \or
  September%
  \or
  Oktober%
  \or
  November%
  \or
  Dezember%
  \fi
}
\newcommand*{\DTMdeATmonthname}[1]{%
  \ifcase#1
  \or
  Jänner%
  \or
  Februar%
  \or
  März%
  \or
  April%
  \or
  Mai%
  \or
  Juni%
  \or
  Juli%
  \or
  August%
  \or
  September%
  \or
  Oktober%
  \or
  November%
  \or
  Dezember%
  \fi
}
\newcommand*{\DTMgermanshortmonthname}[1]{%
  \ifcase#1
  \or
  Jan.%
  \or
  Feb.%
  \or
  März%
  \or
  Apr.%
  \or
  Mai%
  \or
  Juni%
  \or
  Juli%
  \or
  Aug.%
  \or
  Sept.%
  \or
  Okt.%
  \or
  Nov.%
  \or
  Dez.%
  \fi
}
\newcommand*{\DTMdeATshortmonthname}[1]{%
  \ifcase#1
  \or
  Jän.%
  \or
  Feb.%
  \or
  März%
  \or
  Apr.%
  \or
  Mai%
  \or
  Juni%
  \or
  Juli%
  \or
  Aug.%
  \or
  Sept.%
  \or
  Okt.%
  \or
  Nov.%
  \or
  Dez.%
  \fi
}
\newcommand*{\DTMdeCHshortmonthname}[1]{%
  \ifcase#1
  \or
  Jan.%
  \or
  Febr.%
  \or
  März%
  \or
  April%
  \or
  Mai%
  \or
  Juni%
  \or
  Juli%
  \or
  Aug.%
  \or
  Sept.%
  \or
  Okt.%
  \or
  Nov.%
  \or
  Dez.%
  \fi
}
\endinput
%%
%% End of file `datetime2-german-base-utf8.ldf'.
