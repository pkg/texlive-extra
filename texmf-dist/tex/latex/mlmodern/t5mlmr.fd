%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{t5mlmr.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{T5}{mlmr}{}
\DeclareFontShape{T5}{mlmr}{m}{n}%
     {<-5.5>    t5-mlmr5     <5.5-6.5> t5-mlmr6
      <6.5-7.5> t5-mlmr7     <7.5-8.5> t5-mlmr8
      <8.5-9.5> t5-mlmr9     <9.5-11>  t5-mlmr10
      <11-15>   t5-mlmr12
      <15-> t5-mlmr17
      }{}
\DeclareFontShape{T5}{mlmr}{m}{sl}%
     {<-8.5>    t5-mlmro8    <8.5-9.5> t5-mlmro9
      <9.5-11>  t5-mlmro10   <11-15>   t5-mlmro12
      <15-> t5-mlmro17
      }{}
\DeclareFontShape{T5}{mlmr}{m}{it}%
     {<-7.5>    t5-mlmri7
      <7.5-8.5> t5-mlmri8    <8.5-9.5> t5-mlmri9
      <9.5-11>  t5-mlmri10   <11->   t5-mlmri12
      }{}
\DeclareFontShape{T5}{mlmr}{m}{sc}%
     {<-> t5-mlmcsc10}{}
\DeclareFontShape{T5}{mlmr}{m}{ui}%
     {<-> t5-mlmu10}{}
%
% Is this the right 'shape'?:
\DeclareFontShape{T5}{mlmr}{m}{scsl}%
     {<-> t5-mlmcsco10}{}
%%%%%%% bold series
\DeclareFontShape{T5}{mlmr}{b}{n}
     {<-> t5-mlmb10}{}
\DeclareFontShape{T5}{mlmr}{b}{sl}
     {<-> t5-mlmbo10}{}
%%%%%%% bold extended series
\DeclareFontShape{T5}{mlmr}{bx}{n}
     {<-5.5>   t5-mlmbx5      <5.5-6.5> t5-mlmbx6
      <6.5-7.5> t5-mlmbx7      <7.5-8.5> t5-mlmbx8
      <8.5-9.5> t5-mlmbx9      <9.5-11>  t5-mlmbx10
      <11->   t5-mlmbx12
      }{}
\DeclareFontShape{T5}{mlmr}{bx}{it}
     {<-> t5-mlmbxi10}{}
\DeclareFontShape{T5}{mlmr}{bx}{sl}
     {<-> t5-mlmbxo10}{}
%%%%%%% Font/shape undefined, therefore substituted
\DeclareFontShape{T5}{mlmr}{b}{it}
     {<->sub * mlmr/b/sl}{}
\endinput
%%
%% End of file `t5mlmr.fd'.
