%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{ts1mlmss.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{TS1}{mlmss}{}
\DeclareFontShape{TS1}{mlmss}{m}{n}
     {<-8.5> ts1-mlmss8
      <8.5-9.5> ts1-mlmss9      <9.5-11>  ts1-mlmss10
      <11-15.5> ts1-mlmss12     <15.5-> ts1-mlmss17
      }{}
\DeclareFontShape{TS1}{mlmss}{m}{it}
      {<->ssub*mlmss/m/sl}{}
\DeclareFontShape{TS1}{mlmss}{m}{sl}
     {<-8.5> ts1-mlmsso8
      <8.5-9.5> ts1-mlmsso9      <9.5-11>  ts1-mlmsso10
      <11-15.5> ts1-mlmsso12     <15.5-> ts1-mlmsso17
      }{}
%%%%%%% Font/shape undefined, therefore substituted
\DeclareFontShape{TS1}{mlmss}{m}{sc}
     {<->sub*mlmr/m/sc}{}
\DeclareFontShape{TS1}{mlmss}{b}{n}
     {<->ssub * mlmss/bx/n}{}
\DeclareFontShape{TS1}{mlmss}{b}{sl}
     {<->ssub * mlmss/bx/sl}{}
\DeclareFontShape{TS1}{mlmss}{b}{it}
     {<->ssub * mlmss/bx/it}{}
%%%%%%%% semibold condensed series
\DeclareFontShape{TS1}{mlmss}{sbc}{n}
     {<-> ts1-mlmssdc10}{}
\DeclareFontShape{TS1}{mlmss}{sbc}{sl}
     {<-> ts1-mlmssdo10}{}
\DeclareFontShape{TS1}{mlmss}{sbc}{it}
     {<->ssub*mlmss/sbc/sl}{}
%%%%%%%%% bold extended series
\DeclareFontShape{TS1}{mlmss}{bx}{n}
     {<-> ts1-mlmssbx10}{}
\DeclareFontShape{TS1}{mlmss}{bx}{sl}
     {<-> ts1-mlmssbo10}{}
\DeclareFontShape{TS1}{mlmss}{bx}{it}
     {<->ssub*mlmss/bx/sl}{}
\endinput
%%
%% End of file `ts1mlmss.fd'.
