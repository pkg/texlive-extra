%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{ot1mlmss.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{OT1}{mlmss}{}
\DeclareFontShape{OT1}{mlmss}{m}{n}
     {<-8.5> rm-mlmss8
      <8.5-9.5> rm-mlmss9      <9.5-11>  rm-mlmss10
      <11-15.5> rm-mlmss12     <15.5-> rm-mlmss17
      }{}
\DeclareFontShape{OT1}{mlmss}{m}{it}
      {<->ssub*mlmss/m/sl}{}
\DeclareFontShape{OT1}{mlmss}{m}{sl}
     {<-8.5> rm-mlmsso8
      <8.5-9.5> rm-mlmsso9      <9.5-11>  rm-mlmsso10
      <11-15.5> rm-mlmsso12     <15.5-> rm-mlmsso17
      }{}
%%%%%%% Font/shape undefined, therefore substituted
\DeclareFontShape{OT1}{mlmss}{m}{sc}
     {<->sub*mlmr/m/sc}{}
\DeclareFontShape{OT1}{mlmss}{b}{n}
     {<->ssub * mlmss/bx/n}{}
\DeclareFontShape{OT1}{mlmss}{b}{sl}
     {<->ssub * mlmss/bx/sl}{}
\DeclareFontShape{OT1}{mlmss}{b}{it}
     {<->ssub * mlmss/bx/it}{}
%%%%%%%% semibold condensed series
\DeclareFontShape{OT1}{mlmss}{sbc}{n}
     {<-> rm-mlmssdc10}{}
\DeclareFontShape{OT1}{mlmss}{sbc}{sl}
     {<-> rm-mlmssdo10}{}
\DeclareFontShape{OT1}{mlmss}{sbc}{it}
     {<->ssub*mlmss/sbc/sl}{}
%%%%%%%%% bold extended series
\DeclareFontShape{OT1}{mlmss}{bx}{n}
     {<-> rm-mlmssbx10}{}
\DeclareFontShape{OT1}{mlmss}{bx}{sl}
     {<-> rm-mlmssbo10}{}
\DeclareFontShape{OT1}{mlmss}{bx}{it}
     {<->ssub*mlmss/bx/sl}{}
\endinput
%%
%% End of file `ot1mlmss.fd'.
