%This package provides support for the mlmodern fonts. See mlmodern.pdf
%for more information.
%This work may be distributed and/or modified under the conditions
%of the LaTeX Project Public License, either version 1.3c of this
%license or (at your option) any later version.
%Copyright 2003--2009 by B. Jackowski and J.M. Nowacki.
%Copyright 2021 by Daniel Benjamin Miller.
%This work has the LPPL maintenance status "maintained".
%The Current Maintainer of this work is Daniel Benjamin Miller.

\ProvidesFile{t1mlmtt.fd}[2021/01/11 v1.0 Font defs for mlmodern]
\DeclareFontFamily{T1}{mlmtt}{\hyphenchar \font\m@ne}

\ifx\mlmtt@use@light@as@normal\@empty
% macro defined, so we use the light variant as medium (m), and
% medium as semi-bold (sb):
\DeclareFontShape{T1}{mlmtt}{sb}{n}
     {<-8.5>   ec-mlmtt8     <8.5-9.5> ec-mlmtt9
      <9.5-11> ec-mlmtt10    <11->     ec-mlmtt12
      }{}
\DeclareFontShape{T1}{mlmtt}{sb}{it}
     {<-> ec-mlmtti10}{}
\DeclareFontShape{T1}{mlmtt}{sb}{sl}
     {<-> ec-mlmtto10}{}
\DeclareFontShape{T1}{mlmtt}{sb}{sc}
     {<-> ec-mlmtcsc10}{}
\DeclareFontShape{T1}{mlmtt}{sb}{scsl}
     {<-> ec-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{T1}{mlmtt}{m}{n}
     {<-> ec-mlmtl10}{}
\DeclareFontShape{T1}{mlmtt}{m}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{T1}{mlmtt}{m}{sl}
     {<-> ec-mlmtlo10}{}
\DeclareFontShape{T1}{mlmtt}{c}{n}
     {<-> ec-mlmtlc10}{}
\DeclareFontShape{T1}{mlmtt}{c}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{T1}{mlmtt}{c}{sl}
     {<-> ec-mlmtlco10}{}
\else
% usual setup of variants:
\DeclareFontShape{T1}{mlmtt}{m}{n}
     {<-8.5>   ec-mlmtt8     <8.5-9.5> ec-mlmtt9
      <9.5-11> ec-mlmtt10    <11->     ec-mlmtt12
      }{}
\DeclareFontShape{T1}{mlmtt}{m}{it}
     {<-> ec-mlmtti10}{}
\DeclareFontShape{T1}{mlmtt}{m}{sl}
     {<-> ec-mlmtto10}{}
\DeclareFontShape{T1}{mlmtt}{m}{sc}
     {<-> ec-mlmtcsc10}{}
\DeclareFontShape{T1}{mlmtt}{m}{scsl}
     {<-> ec-mlmtcso10}{}
%%%%%%%% light (l), light condensed (lc), and dark (b) variants:
\DeclareFontShape{T1}{mlmtt}{l}{n}
     {<-> ec-mlmtl10}{}
\DeclareFontShape{T1}{mlmtt}{l}{it}
     {<->sub*mlmtt/l/sl}{}
\DeclareFontShape{T1}{mlmtt}{l}{sl}
     {<-> ec-mlmtlo10}{}
\DeclareFontShape{T1}{mlmtt}{lc}{n}
     {<-> ec-mlmtlc10}{}
\DeclareFontShape{T1}{mlmtt}{lc}{it}
     {<->sub*mlmtt/lc/sl}{}
\DeclareFontShape{T1}{mlmtt}{lc}{sl}
     {<-> ec-mlmtlco10}{}
\fi
% bold is always bold (b):
\DeclareFontShape{T1}{mlmtt}{b}{n}
     {<-> ec-mlmtk10}{}
\DeclareFontShape{T1}{mlmtt}{b}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{T1}{mlmtt}{b}{sl}
{<-> ec-mlmtko10}{}
\DeclareFontShape{T1}{mlmtt}{bx}{it}
     {<->sub*mlmtt/b/sl}{}
\DeclareFontShape{T1}{mlmtt}{bx}{n}
     {<->ssub*mlmtt/b/n}{}
\DeclareFontShape{T1}{mlmtt}{bx}{sl}
     {<->ssub*mlmtt/b/sl}{}
\endinput
%%
%% End of file `t1mlmtt.fd'.
