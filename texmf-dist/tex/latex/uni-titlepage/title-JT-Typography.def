%%
%% This is file `title-JT-Typography.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% uni-titlepage.dtx  (with options: `driver,JT-Typography')
%% Copyright (c) 2009-2022 by Markus Kohm <komascript(at)gmx.info>
%% 
%% This file was generated from file(s) of uni-titlepage distribution.
%% 
%% ----------------------------------------------------------------------
%% 
%% This work may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License, version 1.3c of the license.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% This work has the LPPL maintenance status "maintained".
%% 
%% The Current Maintainer and author of this work is Markus Kohm.
%% 
%% This file may only be distributed together with
%% the file `uni-titlepage.dtx'.
%% You may however distribute the file `uni-titlepage.dtx' without this file.
%% 
\ProvidesFile{%
  title%
  -JT-Typography%
 .def%
}[%
2022/09/06 v1.1a
KOMA presents the title page project
]
\begingroup
  \def\PackageNotLoadedError#1{%
    \GenericError{%
      (#1)\@spaces\@spaces\@spaces\@spaces
    }{%
      File `#1' Error: package `uni-titlepage' hasn't been loaded%
    }{%
      This definition file was made to be loaded by package
      `uni-titlepage'.\MessageBreak
      You may not use it without package `uni-titlepage'!\MessageBreak
      See the documentation of package `uni-titlepage' for explanation.%
    }%
  }%
  \ifx\csname uni-titlepage.sty@vers\endcsname\relax
    \PackageNotLoadedError{%
      title-JT-Typography.def%
    }%
  \fi
\endgroup
\DefineFamilyMember[%
  JT-Typography%
]{title}
\renewcommand*{\inittitle}{%
  \ifx\@titlepagefont\@empty
    \renewcommand*{\@titlepagefont}{\normalfont\sffamily\bfseries}%
  \fi
}
\renewcommand*{\makemaintitle}{%
  \begin{%
    titlepage}
    \setlength{\parskip}{\z@}%
    \setlength{\parindent}{\z@}%
    \setlength{\parfillskip}{\z@\@plus 1fil}%
    \linespread{1}\selectfont
    \@titlepagefont
  \raggedright
  {\Large\MakeUppercase{\@author}\unskip\strut\\[\baselineskip]}
  {\titlefont{\Huge\MakeUppercase{\@title}\unskip\strut\\}}
  {\@subtitlefont{\normalsize\MakeUppercase{\@subtitle}\unskip\strut\\}}
  \vfill
  {\normalsize\MakeUppercase{\@place}\ifx\@place\@empty\else\enskip\fi
    {\LARGE\MakeUppercase{\@date}}%
    \ifx\@publisher\@empty\else\unskip\strut\\[1.5\baselineskip]
      \MakeUppercase{\@publisher}%
    \fi}
  \end{%
    titlepage}
}
\renewcommand*{\makemaintitleback}{%
  \begin{titlepage}
    \setlength{\parskip}{\z@}%
    \setlength{\parindent}{\z@}%
    \setlength{\parfillskip}{\z@\@plus 1fil}%
    \begin{minipage}[t]{\textwidth}
      \@titlepagefont{\@uppertitleback}%
    \end{minipage}\par
    \vfill
    \begin{minipage}[b]{\textwidth}
      \@titlepagefont{\@lowertitleback}%
    \end{minipage}
  \end{titlepage}
}
\providecommand*{\@extratitle}{}
\providecommand{\extratitle}[1]{\gdef\@extratitle{#1}}
\DefineFamilyKey[%
  JT-Typography%
]{title}{extratitle}{\extratitle{#1}\FamilyKeyStateProcessed}
\renewcommand*{\makepretitle}{%
  \ifx\@extratitle\@empty\else
    \begin{titlepage}
      \noindent\@extratitle
    \end{titlepage}
  \fi
}%
\renewcommand*{\makepretitleback}{%
  \ifx\@extratitle\@empty\else\begin{titlepage}\null\end{titlepage}\fi
}
\renewcommand*{\makeposttitle}{%
  \ifx\@dedication\@empty\else
    \begin{titlepage}
      \null\vfill
      {\centering\csname @dedicationfont\endcsname{\@dedication \par}}%
      \vskip \z@ \@plus 3fill
    \end{titlepage}
  \fi
}
\renewcommand*{\makeposttitleback}{%
  \ifx\@dedication\@empty\else\begin{titlepage}\null\end{titlepage}\fi
}
\endinput
%%
%% End of file `title-JT-Typography.def'.
