%% Generated by autoinst on 2019/12/06
%%
\ProvidesFile{LY1IBMPlexMono-Sup.fd}
    [2019/12/06 (autoinst)  Font definitions for LY1/IBMPlexMono-Sup.]

\ifcsname s@fct@alias\endcsname\else
\gdef\s@fct@alias{\sub@sfcnt\@font@aliasinfo}
\gdef\@font@aliasinfo#1{%
    \@font@info{Font\space shape\space `\curr@fontshape'\space will
        \space be\space aliased\MessageBreak to\space `\mandatory@arg'}%
}
\fi

\expandafter\ifx\csname IBMPlexMono@scale\endcsname\relax
    \let\IBMPlexMono@@scale\@empty
\else
    \edef\IBMPlexMono@@scale{s*[\csname IBMPlexMono@scale\endcsname]}%
\fi

\DeclareFontFamily{LY1}{IBMPlexMono-Sup}{\hyphenchar\font=-1}


%   ----  regular  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{regular}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{regular}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Italic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{regular}{sl}{
      <-> ssub * IBMPlexMono-Sup/regular/it
}{}


%   ----  thin  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{thin}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Thin-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{thin}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-ThinItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{thin}{sl}{
      <-> ssub * IBMPlexMono-Sup/thin/it
}{}


%   ----  semibold  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{semibold}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-SemiBold-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{semibold}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-SemiBoldItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{semibold}{sl}{
      <-> ssub * IBMPlexMono-Sup/semibold/it
}{}


%   ----  bold  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bold}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Bold-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bold}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-BoldItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bold}{sl}{
      <-> ssub * IBMPlexMono-Sup/bold/it
}{}


%   ----  light  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{light}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Light-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{light}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-LightItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{light}{sl}{
      <-> ssub * IBMPlexMono-Sup/light/it
}{}


%   ----  text  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{text}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Text-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{text}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-TextItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{text}{sl}{
      <-> ssub * IBMPlexMono-Sup/text/it
}{}


%   ----  medium  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{medium}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-Medium-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{medium}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-MediumItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{medium}{sl}{
      <-> ssub * IBMPlexMono-Sup/medium/it
}{}


%   ----  extralight  ----

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{extralight}{n}{
      <-> \IBMPlexMono@@scale IBMPlexMono-ExtraLight-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{extralight}{it}{
      <-> \IBMPlexMono@@scale IBMPlexMono-ExtraLightItalic-sup-ly1
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{extralight}{sl}{
      <-> ssub * IBMPlexMono-Sup/extralight/it
}{}

%
%  Extra 'alias' rules to map the standard NFSS codes to our fancy names
%

%   ul --> thin

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{ul}{sl}{
      <-> alias * IBMPlexMono-Sup/thin/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{ul}{n}{
      <-> alias * IBMPlexMono-Sup/thin/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{ul}{it}{
      <-> alias * IBMPlexMono-Sup/thin/it
}{}


%   el --> extralight

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{el}{sl}{
      <-> alias * IBMPlexMono-Sup/extralight/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{el}{n}{
      <-> alias * IBMPlexMono-Sup/extralight/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{el}{it}{
      <-> alias * IBMPlexMono-Sup/extralight/it
}{}


%   l --> light

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{l}{sl}{
      <-> alias * IBMPlexMono-Sup/light/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{l}{n}{
      <-> alias * IBMPlexMono-Sup/light/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{l}{it}{
      <-> alias * IBMPlexMono-Sup/light/it
}{}


%   m --> regular

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{m}{sl}{
      <-> alias * IBMPlexMono-Sup/regular/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{m}{n}{
      <-> alias * IBMPlexMono-Sup/regular/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{m}{it}{
      <-> alias * IBMPlexMono-Sup/regular/it
}{}


%   sb --> semibold

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{sb}{sl}{
      <-> alias * IBMPlexMono-Sup/semibold/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{sb}{n}{
      <-> alias * IBMPlexMono-Sup/semibold/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{sb}{it}{
      <-> alias * IBMPlexMono-Sup/semibold/it
}{}


%   b --> bold

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{b}{sl}{
      <-> alias * IBMPlexMono-Sup/bold/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{b}{n}{
      <-> alias * IBMPlexMono-Sup/bold/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{b}{it}{
      <-> alias * IBMPlexMono-Sup/bold/it
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bx}{n}{
      <-> ssub * IBMPlexMono-Sup/b/n
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bx}{sl}{
      <-> ssub * IBMPlexMono-Sup/b/sl
}{}

\DeclareFontShape{LY1}{IBMPlexMono-Sup}{bx}{it}{
      <-> ssub * IBMPlexMono-Sup/b/it
}{}

\endinput
