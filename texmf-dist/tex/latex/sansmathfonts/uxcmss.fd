%%
%% This is file `uxcmss.fd',
%% based on the file `ot1xcmss.fd' (and thus on ot1cmss.fd).
%% 
%% It is part of the sansmathfonts package.
%% 
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% 
\ProvidesFile{uxcmss.fd}
        [2021/06/22]
\DeclareFontFamily{U}{xcmss}{\hyphenchar\font45 }
\DeclareFontShape{U}{xcmss}{m}{n}
     {%
      <-9>cmss8%
      <9-10>cmss9%
      <10-12>cmss10%
      <12-17>cmss12%
      <17->cmss17%
      }{}
\DeclareFontShape{U}{xcmss}{m}{it}
      {<->ssub*xcmss/m/sl}{}
\DeclareFontShape{U}{xcmss}{m}{sl}
    {%
      <-9>cmssi8%
      <9-10>cmssi9%
      <10-12>cmssi10%
      <12-17>cmssi12%
      <17->cmssi17%
      }{}
% Bold sans serif and bold slanted (Sauter versions)
\DeclareFontShape{U}{xcmss}{bx}{n}
     {%
      <-9>cmssbx8%
      <9-10>cmssbx9%
      <10-12>cmssbx10%
      <12-17>cmssbx12%
      <17->cmssbx17%
      }{}
\DeclareFontShape{U}{xcmss}{bx}{it}
      {<->ssub*xcmss/bx/sl}{}
\DeclareFontShape{U}{xcmss}{bx}{sl}
    {%
      <-9>cmssxi8%
      <9-10>cmssxi9%
      <10-12>cmssxi10%
      <12-17>cmssxi12%
      <17->cmssxi17%
      }{}
% New: small caps
\DeclareFontShape{U}{xcmss}{m}{sc}
       {%
      <-9>cmsscsc8%
      <9-10>cmsscsc9%
      <10->cmsscsc10%
      }{}
\DeclareFontShape{U}{xcmss}{m}{scit}
      {<->ssub*xcmss/m/scsl}{}
\DeclareFontShape{U}{xcmss}{m}{scsl}
       {%
      <-9>cmsscsci8%
      <9-10>cmsscsci9%
      <10->cmsscsci10%
      }{}
\DeclareFontShape{U}{xcmss}{bx}{sc}
       {%
      <->cmssbxcsc10%
      }{}
\DeclareFontShape{U}{xcmss}{bx}{scit}
      {<->ssub*xcmss/bx/scsl}{}
\DeclareFontShape{U}{xcmss}{bx}{scsl}
       {%
      <->cmssxicsc10%
      }{}
\DeclareFontShape{U}{xcmss}{b}{n}{<->sub*xcmss/bx/n}{}
\DeclareFontShape{U}{xcmss}{b}{it}{<->sub*xcmss/bx/it}{}
\DeclareFontShape{U}{xcmss}{b}{sl}{<->sub*xcmss/bx/sl}{}
\DeclareFontShape{U}{xcmss}{b}{sc}{<->sub*xcmss/bx/sc}{}
\DeclareFontShape{U}{xcmss}{b}{scsl}{<->sub*xcmss/bx/scsl}{}
\DeclareFontShape{U}{xcmss}{b}{scit}{<->sub*xcmss/bx/scit}{}
% Odd shapes
\DeclareFontShape{U}{xcmss}{sbc}{n}
     {%
      <->cmssdc10%
       }{}
% The following shapes are necessary for a correct
% \pounds symbol
\DeclareFontShape{U}{xcmss}{m}{ui}
       {%
      <->cmssu10%
      }{}
\DeclareFontShape{U}{xcmss}{bx}{ui}
       {<->sub*cmsmf/m/ui}{}
\endinput
%%
%% End of file `uxcmss.fd'.
