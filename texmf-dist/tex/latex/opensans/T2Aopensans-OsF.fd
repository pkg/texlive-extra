%% T2Aopensans-OsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T2Aopensans-OsF.fd}[2019/06/24 Font definitions for T2A/opensans-OsF.]

\expandafter\ifx\csname opensans@scale\endcsname\relax
    \let\opensans@@scale\@empty
\else
    \edef\opensans@@scale{s*[\csname opensans@scale\endcsname]}
\fi

\DeclareFontFamily{T2A}{opensans-OsF}{}

\DeclareFontShape{T2A}{opensans-OsF}{l}{it}{<-> \opensans@@scale OpenSans-LightItalic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{eb}{n}{<-> \opensans@@scale OpenSans-ExtraBold-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{eb}{it}{<-> \opensans@@scale OpenSans-ExtraBoldItalic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{bc}{n}{<-> \opensans@@scale OpenSansCondensed-Bold-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{l}{n}{<-> \opensans@@scale OpenSans-Light-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{b}{n}{<-> \opensans@@scale OpenSans-Bold-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{sb}{it}{<-> \opensans@@scale OpenSans-SemiBoldItalic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{lc}{n}{<-> \opensans@@scale OpenSansCondensed-Light-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{lc}{it}{<-> \opensans@@scale OpenSansCondensed-LightItalic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{m}{it}{<-> \opensans@@scale OpenSans-Italic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{b}{it}{<-> \opensans@@scale OpenSans-BoldItalic-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{m}{n}{<-> \opensans@@scale OpenSans-Regular-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{sb}{n}{<-> \opensans@@scale OpenSans-SemiBold-T2A-OsF}{}
\DeclareFontShape{T2A}{opensans-OsF}{l}{sl}{<-> ssub * opensans-OsF/l/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{eb}{sl}{<-> ssub * opensans-OsF/eb/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{bx}{n}{<-> ssub * opensans-OsF/b/n}{}
\DeclareFontShape{T2A}{opensans-OsF}{sb}{sl}{<-> ssub * opensans-OsF/sb/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{lc}{sl}{<-> ssub * opensans-OsF/lc/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{m}{sl}{<-> ssub * opensans-OsF/m/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{b}{sl}{<-> ssub * opensans-OsF/b/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{bx}{it}{<-> ssub * opensans-OsF/b/it}{}
\DeclareFontShape{T2A}{opensans-OsF}{bx}{sl}{<-> ssub * opensans-OsF/b/it}{}

\endinput