%% X2opensans-TLF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{X2opensans-TLF.fd}[2019/06/24 Font definitions for X2/opensans-TLF.]

\expandafter\ifx\csname opensans@scale\endcsname\relax
    \let\opensans@@scale\@empty
\else
    \edef\opensans@@scale{s*[\csname opensans@scale\endcsname]}
\fi

\DeclareFontFamily{X2}{opensans-TLF}{}

\DeclareFontShape{X2}{opensans-TLF}{l}{it}{<-> \opensans@@scale OpenSans-LightItalic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{eb}{n}{<-> \opensans@@scale OpenSans-ExtraBold-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{eb}{it}{<-> \opensans@@scale OpenSans-ExtraBoldItalic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{bc}{n}{<-> \opensans@@scale OpenSansCondensed-Bold-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{l}{n}{<-> \opensans@@scale OpenSans-Light-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{b}{n}{<-> \opensans@@scale OpenSans-Bold-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{sb}{it}{<-> \opensans@@scale OpenSans-SemiBoldItalic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{lc}{n}{<-> \opensans@@scale OpenSansCondensed-Light-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{lc}{it}{<-> \opensans@@scale OpenSansCondensed-LightItalic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{m}{it}{<-> \opensans@@scale OpenSans-Italic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{b}{it}{<-> \opensans@@scale OpenSans-BoldItalic-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{m}{n}{<-> \opensans@@scale OpenSans-Regular-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{sb}{n}{<-> \opensans@@scale OpenSans-SemiBold-X2-TLF}{}
\DeclareFontShape{X2}{opensans-TLF}{l}{sl}{<-> ssub * opensans-TLF/l/it}{}
\DeclareFontShape{X2}{opensans-TLF}{eb}{sl}{<-> ssub * opensans-TLF/eb/it}{}
\DeclareFontShape{X2}{opensans-TLF}{bx}{n}{<-> ssub * opensans-TLF/b/n}{}
\DeclareFontShape{X2}{opensans-TLF}{sb}{sl}{<-> ssub * opensans-TLF/sb/it}{}
\DeclareFontShape{X2}{opensans-TLF}{lc}{sl}{<-> ssub * opensans-TLF/lc/it}{}
\DeclareFontShape{X2}{opensans-TLF}{m}{sl}{<-> ssub * opensans-TLF/m/it}{}
\DeclareFontShape{X2}{opensans-TLF}{b}{sl}{<-> ssub * opensans-TLF/b/it}{}
\DeclareFontShape{X2}{opensans-TLF}{bx}{it}{<-> ssub * opensans-TLF/b/it}{}
\DeclareFontShape{X2}{opensans-TLF}{bx}{sl}{<-> ssub * opensans-TLF/b/it}{}

\endinput