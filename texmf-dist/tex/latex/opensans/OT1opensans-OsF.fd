%% OT1opensans-OsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{OT1opensans-OsF.fd}[2019/06/24 Font definitions for OT1/opensans-OsF.]

\expandafter\ifx\csname opensans@scale\endcsname\relax
    \let\opensans@@scale\@empty
\else
    \edef\opensans@@scale{s*[\csname opensans@scale\endcsname]}
\fi

\DeclareFontFamily{OT1}{opensans-OsF}{}

\DeclareFontShape{OT1}{opensans-OsF}{l}{it}{<-> \opensans@@scale OpenSans-LightItalic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{eb}{n}{<-> \opensans@@scale OpenSans-ExtraBold-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{eb}{it}{<-> \opensans@@scale OpenSans-ExtraBoldItalic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{bc}{n}{<-> \opensans@@scale OpenSansCondensed-Bold-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{l}{n}{<-> \opensans@@scale OpenSans-Light-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{b}{n}{<-> \opensans@@scale OpenSans-Bold-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{sb}{it}{<-> \opensans@@scale OpenSans-SemiBoldItalic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{lc}{n}{<-> \opensans@@scale OpenSansCondensed-Light-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{lc}{it}{<-> \opensans@@scale OpenSansCondensed-LightItalic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{m}{it}{<-> \opensans@@scale OpenSans-Italic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{b}{it}{<-> \opensans@@scale OpenSans-BoldItalic-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{m}{n}{<-> \opensans@@scale OpenSans-Regular-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{sb}{n}{<-> \opensans@@scale OpenSans-SemiBold-OT1-OsF}{}
\DeclareFontShape{OT1}{opensans-OsF}{l}{sl}{<-> ssub * opensans-OsF/l/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{eb}{sl}{<-> ssub * opensans-OsF/eb/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{bx}{n}{<-> ssub * opensans-OsF/b/n}{}
\DeclareFontShape{OT1}{opensans-OsF}{sb}{sl}{<-> ssub * opensans-OsF/sb/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{lc}{sl}{<-> ssub * opensans-OsF/lc/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{m}{sl}{<-> ssub * opensans-OsF/m/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{b}{sl}{<-> ssub * opensans-OsF/b/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{bx}{it}{<-> ssub * opensans-OsF/b/it}{}
\DeclareFontShape{OT1}{opensans-OsF}{bx}{sl}{<-> ssub * opensans-OsF/b/it}{}

\endinput