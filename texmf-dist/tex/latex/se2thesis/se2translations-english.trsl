%%
%% This is file `se2translations-english.trsl',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% se2thesis.dtx  (with options: `translations,english')
%% Copyright (C) 2022--2023 by Stephan Lukasczyk <tex@lukasczyk.me>
%% 
%% It may be distributed and/or modified under the conditions of
%% the LaTeX Project Public License (LPPL), either version 1.3c of
%% this license or (at your option) any later version.  The latest
%% version of this license is in the file:
%% 
%%    https://www.latex-project.org/lppl.txt
%% 
%% This work is "maintained" (as per LPPL maintenance status by
%%   Stephan Lukasczyk.
%% 
%% This work consists of the files se2thesis.dtx,
%%                                 se2thesis.ins,
%%                                 se2colors.dtx,
%%                                 se2fonts.dtx,
%%                                 se2thesis-master-thesis-example.tex
%%           and the derived files se2thesis.pdf,
%%                                 se2thesis.cls,
%%                                 se2translations-english.trsl,
%%                                 se2translations-german.trsl,
%%                                 se2colors.sty,
%%                                 se2fonts.sty,
%%                                 se2thesis-master-thesis-example.bib, and
%%                                 se2thesis-master-thesis-example.pdf
\ProvideDictionaryFor{English}{se2translations}[2023/01/10]
\ProvideDictTranslation{abstract}{abstract}
\ProvideDictTranslation{Abstract}{Abstract}
\ProvideDictTranslation{acknowledgement}{acknowledgement}
\ProvideDictTranslation{Acknowledgement}{Acknowledgement}
\ProvideDictTranslation{acknowledgements}{acknowledgements}
\ProvideDictTranslation{Acknowledgements}{Acknowledgements}
\ProvideDictTranslation{advisor}{advisor}
\ProvideDictTranslation{Advisor}{Advisor}
\ProvideDictTranslation{advisors}{advisors}
\ProvideDictTranslation{Advisors}{Advisors}
\ProvideDictTranslation{co-advisor}{co-advisor}
\ProvideDictTranslation{Co-advisor}{Co-advisor}
\ProvideDictTranslation{co-advisors}{co-advisors}
\ProvideDictTranslation{Co-advisors}{Co-advisors}
\ProvideDictTranslation{matrnumber}{matriculation number}
\ProvideDictTranslation{Matrnumber}{Matriculation number}
\ProvideDictTranslation{supervisor}{supervisor}
\ProvideDictTranslation{Supervisor}{Supervisor}
\ProvideDictTranslation{co-supervisor}{co-supervisor}
\ProvideDictTranslation{Co-supervisor}{Co-supervisor}
\ProvideDictTranslation{external}{external examiner}
\ProvideDictTranslation{External}{External Examiner}
\ProvideDictTranslation{degreeprogramme}{programme}
\ProvideDictTranslation{Degreeprogramme}{Programme}
\ProvideDictTranslation{Bachelor-thesis}{Bachelor Thesis}
\ProvideDictTranslation{Bachelor-thesis-proposal}{Bachelor Thesis Proposal}
\ProvideDictTranslation{Master-thesis}{Master Thesis}
\ProvideDictTranslation{Master-thesis-proposal}{Master Thesis Proposal}
\ProvideDictTranslation{PhD-thesis}{PhD Thesis}
\ProvideDictTranslation{PhD-thesis-proposal}{PhD Thesis Proposal}
\ProvideDictTranslation{date}{date}
\ProvideDictTranslation{Date}{Date}
\ProvideDictTranslation{university-of-passau}{University of Passau}
\ProvideDictTranslation{up}{University of Passau}
\ProvideDictTranslation{fim}{Faculty of Computer Science and Mathematics}
