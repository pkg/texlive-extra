%%
%% This is file `lebhart/lebhart.cls',
%% generated with the docstrip utility.
%%
%% Copyright (C) 2021-2023 by Jinwen XU
%% 
%% This is part of the colorist class series.
%% 
%% This work may be distributed and/or modified under the conditions of the
%% LaTeX Project Public License, either version 1.3c of this license or (at
%% your option) any later version. The latest version of this license is in
%% 
%%     http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3c or later is part of all distributions of LaTeX version
%% 2005/12/01 or later.
%% 
\NeedsTeXFormat{LaTeX2e}[2022-06-01]
\ProvidesExplClass
  {lebhart}
  {2023/01/05} {}
  {A colorful article style}

\tl_const:Nn \l__colorclass_base_class_tl { article }


\bool_new:N \l__colorclass_load_custom_font_file_bool
\bool_set_false:N \l__colorclass_load_custom_font_file_bool

\bool_new:N \l__colorclass_load_custom_font_file_latin_bool
\bool_set_false:N \l__colorclass_load_custom_font_file_latin_bool

\bool_new:N \l__colorclass_load_custom_font_file_cjk_bool
\bool_set_false:N \l__colorclass_load_custom_font_file_cjk_bool

\bool_new:N \l__colorclass_load_custom_font_file_math_bool
\bool_set_false:N \l__colorclass_load_custom_font_file_math_bool

\keys_define:nn { colorclass }
  {
    , draft                   .bool_set:N         = \l__colorclass_fast_bool
    , draft                   .initial:n          = { false }
    , fast                    .bool_set:N         = \l__colorclass_fast_bool

    , print                   .bool_set:N         = \l__colorclass_print_mode_bool
    , print                   .initial:n          = { false }
    , print mode              .bool_set:N         = \l__colorclass_print_mode_bool
    , print~mode              .bool_set:N         = \l__colorclass_print_mode_bool
    , print-mode              .bool_set:N         = \l__colorclass_print_mode_bool
    , print version           .bool_set:N         = \l__colorclass_print_mode_bool
    , print~version           .bool_set:N         = \l__colorclass_print_mode_bool
    , print-version           .bool_set:N         = \l__colorclass_print_mode_bool


    , load custom font file   .code:n             = {
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_str { #1 }
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_latin_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_latin_str { colorist.font.latin }
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_cjk_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_cjk_str   { colorist.font.cjk }
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_math_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_math_str  { colorist.font.math }
                                                    }
    , load custom font file   .default:n          = { colorist.font }
    , load~custom~font~file   .meta:n             = { load custom font file = { #1 } }
    , load~custom~font~file   .default:n          = { colorist.font }
    , load-custom-font-file   .meta:n             = { load custom font file = { #1 } }
    , load-custom-font-file   .default:n          = { colorist.font }

    , load custom latin font file   .code:n       = {
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_latin_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_latin_str { #1 }
                                                    }
    , load custom latin font file   .default:n    = { colorist.font.latin }
    , load~custom~latin~font~file   .meta:n       = { load custom latin font file = { #1 } }
    , load~custom~latin~font~file   .default:n    = { colorist.font.latin }
    , load-custom-latin-font-file   .meta:n       = { load custom latin font file = { #1 } }
    , load-custom-latin-font-file   .default:n    = { colorist.font.latin }

    , load custom cjk font file     .code:n       = {
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_cjk_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_cjk_str { #1 }
                                                    }
    , load custom cjk font file     .default:n    = { colorist.font.cjk }
    , load~custom~cjk~font~file     .meta:n       = { load custom cjk font file = { #1 } }
    , load~custom~cjk~font~file     .default:n    = { colorist.font.cjk }
    , load-custom-cjk-font-file     .meta:n       = { load custom cjk font file = { #1 } }
    , load-custom-cjk-font-file     .default:n    = { colorist.font.cjk }

    , load custom math font file    .code:n       = {
                                                      \bool_set_true:N \l__colorclass_load_custom_font_file_math_bool
                                                      \str_set:Nn \l__colorclass_custom_font_file_math_str { #1 }
                                                    }
    , load custom math font file    .default:n    = { colorist.font.math }
    , load~custom~math~font~file    .meta:n       = { load custom math font file = { #1 } }
    , load~custom~math~font~file    .default:n    = { colorist.font.math }
    , load-custom-math-font-file    .meta:n       = { load custom math font file = { #1 } }
    , load-custom-math-font-file    .default:n    = { colorist.font.math }

    , a4paper                 .bool_set:N         = \l__colorclass_a_four_paper_bool
    , a4paper                 .initial:n          = { false }
    , b5paper                 .bool_set:N         = \l__colorclass_b_five_paper_bool
    , b5paper                 .initial:n          = { false }

    , 11pt                    .code:n             = { \PassOptionsToClass { \CurrentOption } { \l__colorclass_base_class_tl } }
    , 12pt                    .code:n             = { \PassOptionsToClass { \CurrentOption } { \l__colorclass_base_class_tl } }

    , unknown                 .code:n             = {
                                                      \PassOptionsToPackage { \CurrentOption } { colorist }
                                                    }
  }
\ProcessKeyOptions [ colorclass ]

\LoadClass{\l__colorclass_base_class_tl}

\NewDocumentCommand \IfPrintModeTF { m m }
  {
    \bool_if:NTF \l__colorclass_print_mode_bool { #1 } { #2 }
  }
\NewDocumentCommand \IfPrintModeT { m }
  {
    \bool_if:NT \l__colorclass_print_mode_bool { #1 }
  }
\NewDocumentCommand \IfPrintModeF { m }
  {
    \bool_if:NF \l__colorclass_print_mode_bool { #1 }
  }

%%================================
%%  Page layout
%%================================
\RequirePackage { silence }
\WarningFilter { geometry } { Over-specification }

\PassOptionsToPackage { heightrounded } { geometry }
\RequirePackage { geometry }

\geometry
  {
    papersize = { 8.5in, 11in },
    total = { 6.500in, 9.130in },
    centering,
    footnotesep = 2em plus 2pt minus 2pt,
    footskip = .5in,
  }

\bool_if:NT \l__colorclass_b_five_paper_bool
  {
    \geometry
      {
        b5paper,
        total = { 5.535in, 8.160in },
        centering,
        footnotesep = 2em plus 2pt minus 2pt,
        footskip = .5in,
      }
  }

\bool_if:NT \l__colorclass_a_four_paper_bool
  {
    \geometry
      {
        a4paper,
        total = { 6.500in, 9.685in },
        centering,
        footnotesep = 2em plus 2pt minus 2pt,
        footskip = .5in,
      }
  }

\bool_if:NT \l__colorclass_fast_bool
  {
    \PassOptionsToPackage { fast } { colorist }
    \RequirePackage { draftwatermark }
    \DraftwatermarkOptions { text = { \normalfont DRAFT }, color = paper!97!-paper }
  }

\RequirePackage { indentfirst }

\RequirePackage { colorist }


%%================================
%%  Fonts
%%================================
\WarningFilter { latexfont } { Font~shape }
\WarningFilter { latexfont } { Some~font  }

\hook_gput_code:nnn { begindocument/before } { colorclass }
  {
    \IfPackageLoadedTF { biblatex }
      {
        \PassOptionsToPackage { biblatex } { embrac }
      } {}
    \RequirePackage { embrac }
  }

\cs_new_protected:Nn \__colorclass_load_file_or_config:Nnn
  {
    \bool_if:NT #1
      {
        \exp_args:Nx \file_if_exist:nT { #2 }
          {
            \exp_args:Nx \file_input:n { #2 }
            \use_none:nn
          }
      }
    \use:n { #3 }
  }

\cs_new_protected:Nn \__colorclass_if_font_exist:nnn
  {
    \bool_if:NTF \l__colorclass_fast_bool
      { #3 }
      { \fontspec_font_if_exist:nTF { #1 } { #2 } { #3 } }
  }

\__colorclass_load_file_or_config:Nnn \l__colorclass_load_custom_font_file_bool { \l__colorclass_custom_font_file_str }
  {
    \RequirePackage { projlib-font }

    \bool_if:NF \g_projlib_font_already_set_bool
      {
        \bool_if:NT \l__colorclass_fast_bool
          {
            \RequirePackage { mathpazo }
          }

        \PassOptionsToPackage { no-math,quiet } { fontspec }
        \RequirePackage { fontspec }

        \__colorclass_load_file_or_config:Nnn \l__colorclass_load_custom_font_file_latin_bool { \l__colorclass_custom_font_file_latin_str }
          {
            \bool_if:NTF \l__projlib_font_useosf_bool
              {
                \setmainfont { TeXGyrePagellaX }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Italic,
                    BoldItalicFont = *-BoldItalic,
                    Numbers        = OldStyle ,
                  ]
                \setsansfont { SourceSansPro }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Semibold,
                    ItalicFont     = *-RegularIt,
                    BoldItalicFont = *-SemiboldIt,
                    WordSpace      = {1.25, 1, 1} ,
                    Numbers        = OldStyle ,
                  ]
              }
              {
                \setmainfont { TeXGyrePagellaX }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Italic,
                    BoldItalicFont = *-BoldItalic,
                  ]
                \setsansfont { SourceSansPro }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Semibold,
                    ItalicFont     = *-RegularIt,
                    BoldItalicFont = *-SemiboldIt,
                    WordSpace      = {1.25, 1, 1} ,
                  ]
              }
            \setmonofont { NewCMMono10 }
              [
                Scale          = 1.05 ,
                Extension      = .otf,
                UprightFont    = *-Regular,
                BoldFont       = *-Bold,
                ItalicFont     = *-Italic,
                BoldItalicFont = *-BoldOblique,
              ]

            \projlib_language_set_linespacing_latin:n { \setstretch { 1.07 } }

            \hook_gput_code:nnn { begindocument/before } { colorclass }
              {
                \RenewEmph{[}{]}
                \RenewEmph{(}{)}
              }
          }
      }

    \PassOptionsToPackage { fontset = none, scheme = plain } { ctex }
    \RequirePackage { ctex }

    \__colorclass_load_file_or_config:Nnn \l__colorclass_load_custom_font_file_cjk_bool { \l__colorclass_custom_font_file_cjk_str }
      {
        \__colorclass_if_font_exist:nnn { SourceHanSerifSC-Regular }
          {
            \setCJKmainfont { SourceHanSerifSC }
              [
                UprightFont    = *-Regular,
                BoldFont       = *-Bold,
                ItalicFont     = *-Regular,
                BoldItalicFont = *-Bold,
              ]
          }
          {
            \setCJKmainfont { FandolSong }
              [
                Extension      = .otf,
                UprightFont    = *-Regular,
                BoldFont       = *-Bold,
                ItalicFont     = FandolKai-Regular ,
                BoldItalicFont = FandolKai-Regular ,
                BoldItalicFeatures = { FakeBold = 4 } ,
              ]
          }

        \__colorclass_if_font_exist:nnn { SourceHanSansSC-Regular }
          {
            \setCJKsansfont { SourceHanSansSC }
              [
                UprightFont    = *-Regular,
                BoldFont       = *-Bold,
                ItalicFont     = *-Regular,
                BoldItalicFont = *-Bold,
              ]
          }
          {
            \setCJKsansfont { FandolHei }
              [
                Extension      = .otf,
                UprightFont    = *-Regular,
                BoldFont       = *-Bold,
                ItalicFont     = *-Regular,
                BoldItalicFont = *-Bold,
              ]
          }

        \__colorclass_if_font_exist:nnn { SourceHanMonoSC-Regular }
          {
            \setCJKmonofont { SourceHanMonoSC }
              [
                UprightFont    = *-Regular,
                BoldFont       = *-Medium,
                ItalicFont     = *-Regular,
                BoldItalicFont = *-Medium,
              ]
          }
          {
            \setCJKmonofont { FandolFang-Regular.otf }
              [
                BoldFont       = * ,
                BoldFeatures   = { FakeBold = 4 } ,
                ItalicFont     = * ,
                BoldItalicFont = * ,
                BoldItalicFeatures = { FakeBold = 4 } ,
              ]
          }

        \bool_if:NT \g__projlib_language_enabled_schinese_bool
          {
            \__colorclass_if_font_exist:nnn { SourceHanSerifSC-Regular }
              {
                \setCJKfamilyfont { SCmain } { SourceHanSerifSC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { SCmain } { FandolSong }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = FandolKai-Regular ,
                    BoldItalicFont = FandolKai-Regular ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanSansSC-Regular }
              {
                \setCJKfamilyfont { SCsans } { SourceHanSansSC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { SCsans } { FandolHei }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanMonoSC-Regular }
              {
                \setCJKfamilyfont { SCmono } { SourceHanMonoSC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Medium,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Medium,
                  ]
              }
              {
                \setCJKfamilyfont { SCmono } { FandolFang-Regular.otf }
                  [
                    BoldFont       = * ,
                    BoldFeatures   = { FakeBold = 4 } ,
                    ItalicFont     = * ,
                    BoldItalicFont = * ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
          }

        \bool_if:NT \g__projlib_language_enabled_tchinese_bool
          {
            \__colorclass_if_font_exist:nnn { SourceHanSerifTC-Regular }
              {
                \setCJKfamilyfont { TCmain } { SourceHanSerifTC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { TCmain } { FandolSong }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = FandolKai-Regular ,
                    BoldItalicFont = FandolKai-Regular ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanSansTC-Regular }
              {
                \setCJKfamilyfont { TCsans } { SourceHanSansTC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { TCsans } { FandolHei }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanMonoTC-Regular }
              {
                \setCJKfamilyfont { TCmono } { SourceHanMonoTC }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Medium,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Medium,
                  ]
              }
              {
                \setCJKfamilyfont { TCmono } { FandolFang-Regular.otf }
                  [
                    BoldFont       = * ,
                    BoldFeatures   = { FakeBold = 4 } ,
                    ItalicFont     = * ,
                    BoldItalicFont = * ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
          }

        \bool_if:NT \g__projlib_language_enabled_japanese_bool
          {
            \__colorclass_if_font_exist:nnn { SourceHanSerif-Regular }
              {
                \setCJKfamilyfont { JPmain } { SourceHanSerif }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { JPmain } { FandolSong }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = FandolKai-Regular ,
                    BoldItalicFont = FandolKai-Regular ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanSans-Regular }
              {
                \setCJKfamilyfont { JPsans } { SourceHanSans }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
              {
                \setCJKfamilyfont { JPsans } { FandolHei }
                  [
                    Extension      = .otf,
                    UprightFont    = *-Regular,
                    BoldFont       = *-Bold,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Bold,
                  ]
              }
            \__colorclass_if_font_exist:nnn { SourceHanMono-Regular }
              {
                \setCJKfamilyfont { JPmono } { SourceHanMono }
                  [
                    UprightFont    = *-Regular,
                    BoldFont       = *-Medium,
                    ItalicFont     = *-Regular,
                    BoldItalicFont = *-Medium,
                  ]
              }
              {
                \setCJKfamilyfont { JPmono } { FandolFang-Regular.otf }
                  [
                    BoldFont       = * ,
                    BoldFeatures   = { FakeBold = 4 } ,
                    ItalicFont     = * ,
                    BoldItalicFont = * ,
                    BoldItalicFeatures = { FakeBold = 4 } ,
                  ]
              }
          }

        \cs_new:Nn \colorclass_cjk_sffamily: {}
        \cs_new:Nn \colorclass_cjk_ttfamily: {}

        \hook_gput_code:nnn { cmd/sffamily/after } { colorclass } { \colorclass_cjk_sffamily: }
        \hook_gput_code:nnn { cmd/ttfamily/after } { colorclass } { \colorclass_cjk_ttfamily: }

        \AddLanguageSetting [schinese]
          {
            \cs_set:Nn \colorclass_cjk_sffamily: { \CJKfamily { SCsans } }
            \cs_set:Nn \colorclass_cjk_ttfamily: { \CJKfamily { SCmono } }
            \CJKfamily { SCmain }
          }
        \AddLanguageSetting [tchinese]
          {
            \cs_set:Nn \colorclass_cjk_sffamily: { \CJKfamily { TCsans } }
            \cs_set:Nn \colorclass_cjk_ttfamily: { \CJKfamily { TCmono } }
            \CJKfamily { TCmain }
          }
        \AddLanguageSetting [japanese]
          {
            \cs_set:Nn \colorclass_cjk_sffamily: { \CJKfamily { JPsans } }
            \cs_set:Nn \colorclass_cjk_ttfamily: { \CJKfamily { JPmono } }
            \CJKfamily { JPmain }
          }
      }

    \__colorclass_load_file_or_config:Nnn \l__colorclass_load_custom_font_file_math_bool { \l__colorclass_custom_font_file_math_str }
      {
        \bool_if:NF \g_projlib_font_already_set_bool
          {
            \RequirePackage { amssymb }
            \bool_if:NF \l__colorclass_fast_bool
              {
                \PassOptionsToPackage { warnings-off = { mathtools-colon, mathtools-overbracket } } { unicode-math }
                \RequirePackage { unicode-math }
                \unimathsetup { math-style = ISO, partial = upright, nabla = upright }
                \setmathfont { KpMath-Regular.otf }
                \setmathfont { KpMath-Regular.otf }
                  [
                    range = \amalg ,
                    Scale = 0.84625
                  ]
                \setmathfont { KpMath-Sans.otf }
                  [
                    range = { \sum }
                  ]
                \setmathfont { latinmodern-math.otf }
                  [
                    range = { frak, bffrak, \mitvarpi, \mupvarpi, \ast } ,
                    Scale = 1.10
                  ]
                \setmathfont { texgyrepagella-math.otf }
                  [
                    range = { `(, `) } ,
                    Scale = 1.10
                  ]
                \setmathfont { texgyrepagella-math.otf }
                  [
                    range = { \mathcomma, \mathsemicolon } ,
                  ]
                \setmathfont { texgyrepagella-math.otf }
                  [
                    range = { it / { Latin, latin }, bfit / { Latin, latin }, up / num, bfup / num }
                  ]
                \setmathfont { KpMath-Regular.otf } [ range = { cal, bfcal }, RawFeature=+ss01 ]
                % \setmathfont { KpMath-Regular.otf } [ range = {} ]

                \sys_if_engine_luatex:T
                  {
                    \mathitalicsmode=1
                  }

                \hook_gput_code:nnn { begindocument } { colorclass }
                  {
                    \cs_gset_eq:NN \overline \wideoverbar
                    \cs_gset_eq:NN \square \mdwhtsquare
                  }

                % https://tex.stackexchange.com/a/647789
                \hook_gput_code:nnn { begindocument } { colorclass }
                  {
                    \NewCommandCopy\unicodevdots\vdots
                    \RenewDocumentCommand{\vdots}{}{\mathrel{\loweredvdots}}
                  }
                \newcommand{\loweredvdots}{\mathpalette\loweredvdots@\relax}
                \newcommand{\loweredvdots@}[2]{%
                  \begingroup
                  \sbox\z@{$\m@th#1\unicodevdots$}%
                  \vrule width \z@ height 2.25\ht\z@ depth 0.012\ht\z@
                  \raisebox{0.25\height}{\usebox\z@}%
                  \endgroup
                }

                \RequirePackage { tikz-cd }
                \tikzcdset { arrow~style = tikz, diagrams = { >={Stealth[round,length=3.4pt,width=6.15pt,inset=2.25pt]} } }

                \box_new:N \l__colorclass_xarrows_above_box
                \box_new:N \l__colorclass_xarrows_below_box
                \dim_new:N \l__colorclass_xarrows_length_dim
                \cs_new_protected:Nn \colorclass_xarrows_generic:nnnn
                  % #3 = option of \tikz
                  % #4 = edge of \draw
                  {
                    \hbox_set:Nn \l__colorclass_xarrows_below_box { \ensuremath { \scriptstyle #1 } }
                    \hbox_set:Nn \l__colorclass_xarrows_above_box { \ensuremath { \scriptstyle #2 } }
                    \dim_set:Nn \l__colorclass_xarrows_length_dim
                      { \dim_eval:n { \dim_max:nn { \box_wd:N \l__colorclass_xarrows_below_box } { \box_wd:N \l__colorclass_xarrows_above_box } + 1em } }
                    \mathrel
                      {
                        \tikz [ #3, line~width = .6pt, baseline = -.5ex, every~node/.style = { inner~sep = 0pt }, >={Stealth[round,length=3.4pt,width=6.15pt,inset=2.25pt]} ]
                          \draw (0,0) #4
                            node [ below = 3pt ] { \box_use:N \l__colorclass_xarrows_below_box }
                            node [ above = 2pt ] { \box_use:N \l__colorclass_xarrows_above_box }
                            ( \l__colorclass_xarrows_length_dim ,0) ;
                      }
                  }

                \RenewDocumentCommand \xrightarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } { -> } { -- }
                  }
                \RenewDocumentCommand \xleftarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } { <- } { -- }
                  }
                \RenewDocumentCommand \xleftrightarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } { <-> } { -- }
                  }
                \RenewDocumentCommand \xhookrightarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/hookrightarrow ] }
                  }
                \RenewDocumentCommand \xhookleftarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/hookleftarrow ] }
                  }
                \RenewDocumentCommand \xmapsto { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/mapsto ] }
                  }
                \NewDocumentCommand \xlongequal { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/equal ] }
                  }
                \NewDocumentCommand \xtwoheadrightarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/twoheadrightarrow ] }
                  }
                \NewDocumentCommand \xtwoheadleftarrow { O{} m }
                  {
                    \colorclass_xarrows_generic:nnnn { #1 } { #2 } {} { edge [ commutative~diagrams/twoheadleftarrow ] }
                  }
                \hook_gput_code:nnn { begindocument/end } { colorclass }
                  {
                    \RenewDocumentCommand \twoheadrightarrow {}
                      {
                        \colorclass_xarrows_generic:nnnn {
                          \mathchoice { \,\, } { \, } { } { }
                        } {} {} { edge [ commutative~diagrams/twoheadrightarrow ] }
                      }
                    \RenewDocumentCommand \twoheadleftarrow {}
                      {
                        \colorclass_xarrows_generic:nnnn {
                          \mathchoice { \,\, } { \, } { } { }
                        } {} {} { edge [ commutative~diagrams/twoheadleftarrow ] }
                      }
                  }
              }
          }
      }
  }

\char_set_catcode_active:n { `\· }
\cs_new_protected:Npn · { \ensuremath\cdot }

%%================================
%%  Graphics
%%================================
\RequirePackage { graphicx }
\graphicspath { { images/ } }
\RequirePackage { wrapfig }
\RequirePackage { float }
\RequirePackage { caption }
\captionsetup { font = small }

%%================================
%%  Icing on the cake
%%================================
\bool_if:NT \l__colorclass_fast_bool { \endinput }

\sys_if_engine_luatex:TF
  {
    \RequirePackage { lua-widow-control }
    \lwcsetup { balanced }
  }
  {
    \PassOptionsToPackage { all } { nowidow }
    \RequirePackage { nowidow }
  }

\sys_if_engine_xetex:T
  {
    \RequirePackage { regexpatch }
    \skip_new:N \g_colorclass_parfillskip_skip
    \xpatchcmd{\@trivlist}{\@flushglue}{\g_colorclass_parfillskip_skip}{}{}
    \hook_gput_code:nnn { begindocument } { colorclass }
      {
        \skip_gset:Nn \g_colorclass_parfillskip_skip { 0pt plus \dim_eval:n { \linewidth - 3em } }
        \skip_gset_eq:NN \parfillskip \g_colorclass_parfillskip_skip
      }
  }

\endinput
%%
%% End of file `lebhart/lebhart.cls'.
