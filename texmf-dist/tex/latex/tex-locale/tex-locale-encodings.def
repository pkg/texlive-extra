%%
%% This is file `tex-locale-encodings.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% tex-locale.dtx  (with options: `tex-locale-encodings.def,package')
%% 
%%  tex-locale.dtx
%%  Copyright 2018 Nicola Talbot
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license or (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is Nicola Talbot.
%% 
%%  This work consists of the files tex-locale.dtx and tex-locale.ins and the derived files tex-locale.sty, tex-locale.tex, tex-locale-scripts-enc.def, tex-locale-encodings.def, tex-locale-support.def.
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%% arara: xetex: {shell: on}
%% arara: xetex: {shell: on}
\def\@locale@newencmap#1#2{%
  \@tracklang@namedef{@locale@encmap@#1}{#2}}
\def\@locale@ifhasencmap#1#2#3{%
  \@tracklang@ifundef{@locale@encmap@#1}{#3}{#2}}
\def\@locale@getencmap#1{%
  \@tracklang@nameuse{@locale@encmap@#1}}
\@locale@newencmap{iso88591}{latin1}
\@locale@newencmap{iso88592}{latin2}
\@locale@newencmap{iso88593}{latin3}
\@locale@newencmap{iso88594}{latin4}
\@locale@newencmap{iso88595}{latin5}
\@locale@newencmap{iso88599}{latin9}
\@locale@newencmap{windows1250}{cp1250}
\@locale@newencmap{windows1252}{cp1252}
\@locale@newencmap{windows1257}{cp1257}
\@locale@newencmap{ibm850}{cp850}
\@locale@newencmap{ibm852}{cp852}
\@locale@newencmap{ibm437}{cp437}
\@locale@newencmap{ibm865}{cp865}
\@locale@newencmap{usascii}{ascii}
\@locale@newencmap{xmaccentraleurope}{macce}
\endinput
%%
%% End of file `tex-locale-encodings.def'.
