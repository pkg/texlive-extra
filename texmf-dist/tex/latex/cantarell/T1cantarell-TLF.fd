%% T1cantarell-TLF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T1cantarell-TLF.fd}[2019/06/24 Font definitions for T1/cantarell-TLF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{T1}{cantarell-TLF}{}

\DeclareFontShape{T1}{cantarell-TLF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-T1-TLF}{}
\DeclareFontShape{T1}{cantarell-TLF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-T1-TLF}{}
\DeclareFontShape{T1}{cantarell-TLF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-T1-TLF}{}
\DeclareFontShape{T1}{cantarell-TLF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-T1-TLF}{}
\DeclareFontShape{T1}{cantarell-TLF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-T1-TLF}{}
\DeclareFontShape{T1}{cantarell-TLF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-T1-TLF-Slanted}{}
\DeclareFontShape{T1}{cantarell-TLF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-T1-TLF-Slanted}{}
\DeclareFontShape{T1}{cantarell-TLF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-T1-TLF-Slanted}{}
\DeclareFontShape{T1}{cantarell-TLF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-T1-TLF-Slanted}{}
\DeclareFontShape{T1}{cantarell-TLF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-T1-TLF-Slanted}{}
\DeclareFontShape{T1}{cantarell-TLF}{bx}{n}{<-> ssub * cantarell-TLF/b/n}{}
\DeclareFontShape{T1}{cantarell-TLF}{l}{it}{<-> ssub * cantarell-TLF/l/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{b}{it}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{bx}{sl}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{bx}{it}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{el}{it}{<-> ssub * cantarell-TLF/el/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{m}{it}{<-> ssub * cantarell-TLF/m/sl}{}
\DeclareFontShape{T1}{cantarell-TLF}{eb}{it}{<-> ssub * cantarell-TLF/eb/sl}{}

\endinput