%% TS1cantarell-TOsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{TS1cantarell-TOsF.fd}[2019/06/24 Font definitions for TS1/cantarell-TOsF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{TS1}{cantarell-TOsF}{}

\DeclareFontShape{TS1}{cantarell-TOsF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-TS1-TOsF}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-TS1-TOsF}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-TS1-TOsF}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-TS1-TOsF}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-TS1-TOsF}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-TS1-TOsF-Slanted}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-TS1-TOsF-Slanted}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-TS1-TOsF-Slanted}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-TS1-TOsF-Slanted}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-TS1-TOsF-Slanted}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{bx}{n}{<-> ssub * cantarell-TOsF/b/n}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{l}{it}{<-> ssub * cantarell-TOsF/l/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{b}{it}{<-> ssub * cantarell-TOsF/b/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{bx}{sl}{<-> ssub * cantarell-TOsF/b/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{bx}{it}{<-> ssub * cantarell-TOsF/b/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{el}{it}{<-> ssub * cantarell-TOsF/el/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{m}{it}{<-> ssub * cantarell-TOsF/m/sl}{}
\DeclareFontShape{TS1}{cantarell-TOsF}{eb}{it}{<-> ssub * cantarell-TOsF/eb/sl}{}

\endinput