%% T2Bcantarell-LF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T2Bcantarell-LF.fd}[2019/06/24 Font definitions for T2B/cantarell-LF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{T2B}{cantarell-LF}{}

\DeclareFontShape{T2B}{cantarell-LF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-T2B-LF}{}
\DeclareFontShape{T2B}{cantarell-LF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-T2B-LF}{}
\DeclareFontShape{T2B}{cantarell-LF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-T2B-LF}{}
\DeclareFontShape{T2B}{cantarell-LF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-T2B-LF}{}
\DeclareFontShape{T2B}{cantarell-LF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-T2B-LF}{}
\DeclareFontShape{T2B}{cantarell-LF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-T2B-LF-Slanted}{}
\DeclareFontShape{T2B}{cantarell-LF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-T2B-LF-Slanted}{}
\DeclareFontShape{T2B}{cantarell-LF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-T2B-LF-Slanted}{}
\DeclareFontShape{T2B}{cantarell-LF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-T2B-LF-Slanted}{}
\DeclareFontShape{T2B}{cantarell-LF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-T2B-LF-Slanted}{}
\DeclareFontShape{T2B}{cantarell-LF}{bx}{n}{<-> ssub * cantarell-LF/b/n}{}
\DeclareFontShape{T2B}{cantarell-LF}{l}{it}{<-> ssub * cantarell-LF/l/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{b}{it}{<-> ssub * cantarell-LF/b/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{bx}{sl}{<-> ssub * cantarell-LF/b/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{bx}{it}{<-> ssub * cantarell-LF/b/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{el}{it}{<-> ssub * cantarell-LF/el/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{m}{it}{<-> ssub * cantarell-LF/m/sl}{}
\DeclareFontShape{T2B}{cantarell-LF}{eb}{it}{<-> ssub * cantarell-LF/eb/sl}{}

\endinput