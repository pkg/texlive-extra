%% T2Acantarell-TLF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T2Acantarell-TLF.fd}[2019/06/24 Font definitions for T2A/cantarell-TLF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{T2A}{cantarell-TLF}{}

\DeclareFontShape{T2A}{cantarell-TLF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-T2A-TLF}{}
\DeclareFontShape{T2A}{cantarell-TLF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-T2A-TLF}{}
\DeclareFontShape{T2A}{cantarell-TLF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-T2A-TLF}{}
\DeclareFontShape{T2A}{cantarell-TLF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-T2A-TLF}{}
\DeclareFontShape{T2A}{cantarell-TLF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-T2A-TLF}{}
\DeclareFontShape{T2A}{cantarell-TLF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-T2A-TLF-Slanted}{}
\DeclareFontShape{T2A}{cantarell-TLF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-T2A-TLF-Slanted}{}
\DeclareFontShape{T2A}{cantarell-TLF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-T2A-TLF-Slanted}{}
\DeclareFontShape{T2A}{cantarell-TLF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-T2A-TLF-Slanted}{}
\DeclareFontShape{T2A}{cantarell-TLF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-T2A-TLF-Slanted}{}
\DeclareFontShape{T2A}{cantarell-TLF}{bx}{n}{<-> ssub * cantarell-TLF/b/n}{}
\DeclareFontShape{T2A}{cantarell-TLF}{l}{it}{<-> ssub * cantarell-TLF/l/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{b}{it}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{bx}{sl}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{bx}{it}{<-> ssub * cantarell-TLF/b/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{el}{it}{<-> ssub * cantarell-TLF/el/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{m}{it}{<-> ssub * cantarell-TLF/m/sl}{}
\DeclareFontShape{T2A}{cantarell-TLF}{eb}{it}{<-> ssub * cantarell-TLF/eb/sl}{}

\endinput