%% T1cantarell-OsF.fd
%% Copyright 2019 Mohamed El Morabity
%
% This work may be distributed and/or modified under the conditions of the LaTeX
% Project Public License, either version 1.3 of this license or (at your option)
% any later version. The latest version of this license is in
% http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
% distributions of LaTeX version 2005/12/01 or later.
%
% This work has the LPPL maintenance status \`maintained'.
%
% The Current Maintainer of this work is Mohamed El Morabity
%
% This work consists of all files listed in manifest.txt.


\ProvidesFile{T1cantarell-OsF.fd}[2019/06/24 Font definitions for T1/cantarell-OsF.]

\expandafter\ifx\csname cantarell@scale\endcsname\relax
    \let\cantarell@@scale\@empty
\else
    \edef\cantarell@@scale{s*[\csname cantarell@scale\endcsname]}
\fi

\DeclareFontFamily{T1}{cantarell-OsF}{}

\DeclareFontShape{T1}{cantarell-OsF}{l}{n}{<-> \cantarell@@scale Cantarell-Light-T1-OsF}{}
\DeclareFontShape{T1}{cantarell-OsF}{b}{n}{<-> \cantarell@@scale Cantarell-Bold-T1-OsF}{}
\DeclareFontShape{T1}{cantarell-OsF}{el}{n}{<-> \cantarell@@scale Cantarell-Thin-T1-OsF}{}
\DeclareFontShape{T1}{cantarell-OsF}{m}{n}{<-> \cantarell@@scale Cantarell-Regular-T1-OsF}{}
\DeclareFontShape{T1}{cantarell-OsF}{eb}{n}{<-> \cantarell@@scale Cantarell-ExtraBold-T1-OsF}{}
\DeclareFontShape{T1}{cantarell-OsF}{l}{sl}{<-> \cantarell@@scale Cantarell-Light-T1-OsF-Slanted}{}
\DeclareFontShape{T1}{cantarell-OsF}{b}{sl}{<-> \cantarell@@scale Cantarell-Bold-T1-OsF-Slanted}{}
\DeclareFontShape{T1}{cantarell-OsF}{el}{sl}{<-> \cantarell@@scale Cantarell-Thin-T1-OsF-Slanted}{}
\DeclareFontShape{T1}{cantarell-OsF}{m}{sl}{<-> \cantarell@@scale Cantarell-Regular-T1-OsF-Slanted}{}
\DeclareFontShape{T1}{cantarell-OsF}{eb}{sl}{<-> \cantarell@@scale Cantarell-ExtraBold-T1-OsF-Slanted}{}
\DeclareFontShape{T1}{cantarell-OsF}{bx}{n}{<-> ssub * cantarell-OsF/b/n}{}
\DeclareFontShape{T1}{cantarell-OsF}{l}{it}{<-> ssub * cantarell-OsF/l/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{b}{it}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{bx}{sl}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{bx}{it}{<-> ssub * cantarell-OsF/b/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{el}{it}{<-> ssub * cantarell-OsF/el/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{m}{it}{<-> ssub * cantarell-OsF/m/sl}{}
\DeclareFontShape{T1}{cantarell-OsF}{eb}{it}{<-> ssub * cantarell-OsF/eb/sl}{}

\endinput