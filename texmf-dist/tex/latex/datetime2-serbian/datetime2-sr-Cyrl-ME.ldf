%%
%% This is file `datetime2-sr-Cyrl-ME.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-serbian.dtx  (with options: `datetime2-sr-Cyrl-ME.ldf,package')
%% 
%%  datetime2-serbian.dtx
%%  Copyright 2019 Andrej Radović
%%  Copyright 2019 Nicola Talbot
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is Andrej Radović.
%% 
%%  This work consists of the files datetime2-serbian.dtx and
%%  datetime2-serbian.ins and the derived files are as follows:
%%    datetime2-serbian-base.ldf
%%    datetime2-serbian-base-utf8.ldf
%%    datetime2-serbian-base-ascii.ldf
%%    datetime2-serbian.ldf
%%    datetime2-sr-Latn.ldf
%%    datetime2-sr-Latn-RS.ldf
%%    datetime2-sr-Latn-ME.ldf
%%    datetime2-sr-Latn-BA.ldf
%%    datetime2-serbianc.ldf
%%    datetime2-sr-Cyrl.ldf
%%    datetime2-sr-Cyrl-RS.ldf
%%    datetime2-sr-Cyrl-ME.ldf
%%    datetime2-sr-Cyrl-BA.ldf
%% 
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\ProvidesDateTimeModule{sr-Cyrl-ME}[2019/11/22 v2.1.0]
\RequireDateTimeModule{serbianc}
\newcommand*{\DTMsrCyrlMEdowdaysep}{,\space}
\newcommand*{\DTMsrCyrlMEdaymonthsep}{%
  \DTMtexorpdfstring{\protect~}{\space}%
}
\newcommand*{\DTMsrCyrlMEmonthyearsep}{\space}
\newcommand*{\DTMsrCyrlMEdatetimesep}{\space}
\newcommand*{\DTMsrCyrlMEtimezonesep}{\space}
\newcommand*{\DTMsrCyrlMEdatesep}{.}
\newcommand*{\DTMsrCyrlMEtimesep}{.}
\DTMdefkey{sr-Cyrl-ME}{dowdaysep}%
    {\renewcommand*{\DTMsr-Cyrl-MEdowdaysep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{daymonthsep}%
    {\renewcommand*{\DTMsr-Cyrl-MEdaymonthsep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{monthyearsep}%
    {\renewcommand*{\DTMsr-Cyrl-MEmonthyearsep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{datetimesep}%
    {\renewcommand*{\DTMsr-Cyrl-MEdatetimesep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{timezonesep}%
    {\renewcommand*{\DTMsr-Cyrl-MEtimezonesep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{datesep}%
    {\renewcommand*{\DTMsr-Cyrl-MEdatesep}{#1}}
\DTMdefkey{sr-Cyrl-ME}{timesep}%
    {\renewcommand*{\DTMsr-Cyrl-MEtimesep}{#1}}
    %\changes{2.0.1}{2019-11-11}{Adopted semantic versioning.}
\newcommand*{\DTMsrCyrlMEweekdayname}%
{\DTMserbiancyrijweekdayname}

\newcommand*{\DTMsrCyrlMEWeekdayname}%
    {\DTMserbiancyrijWeekdayname}
    %\changes{2.0.1}{2019-11-11}{Adopted semantic versioning.}
\DTMdefchoicekey{sr-Cyrl-ME}%
    {pronunciation}[\@dtm@val\@dtm@nr]{ekavian,ijekavian}{%
  \ifcase\@dtm@nr\relax
    \renewcommand*{\DTMsrCyrlMEweekdayname}%
        {\DTMserbiancyrekweekdayname}%
    \renewcommand*{\DTMsrCyrlMEWeekdayname}%
        {\DTMserbiancyrekWeekdayname}%
  \or%
    \renewcommand*{\DTMsrCyrlMEweekdayname}%
        {\DTMserbiancyrijweekdayname}%
    \renewcommand*{\DTMsrCyrlMEWeekdayname}
        {\DTMserbiancyrijWeekdayname}%
  \fi
}
\DTMdefboolkey{sr-Cyrl-ME}{monthi}[true]{}
\DTMsetbool{sr-Cyrl-ME}{monthi}{false}
\DTMdefboolkey{sr-Cyrl-ME}{leadingzero}[true]{}
\DTMsetbool{sr-Cyrl-ME}{leadingzero}{false}
   \newcommand*{\DTMsrCyrlMEdayordinal}[1]{%
       \DTMifbool{sr-Cyrl-ME}{leadingzero}%
       {\DTMtwodigits{#1}}%
       {\number#1}\DTMsrCyrlMEdatesep}%
\newcommand*{\DTMsrCyrlMEnoimonthname}{\DTMserbiancyrnoimonthname}
\newcommand*{\DTMsrCyrlMEnoiMonthname}{\DTMserbiancyrnoiMonthname}
\newcommand*{\DTMsrCyrlMEimonthname}{\DTMserbiancyrimonthname}
\newcommand*{\DTMsrCyrlMEiMonthname}{\DTMserbiancyriMonthname}
\DTMdefboolkey{sr-Cyrl-ME}{mapzone}[true]{}
\DTMsetbool{sr-Cyrl-ME}{mapzone}{true}
\DTMdefboolkey{sr-Cyrl-ME}{showdayofmonth}[true]{}
\DTMsetbool{sr-Cyrl-ME}{showdayofmonth}{true}
\DTMdefboolkey{sr-Cyrl-ME}{showyear}[true]{}
\DTMsetbool{sr-Cyrl-ME}{showyear}{true}
\DTMnewstyle%
{sr-Cyrl-ME}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlMEweekdayname{##4}%
        \DTMsrCyrlMEdowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl-ME}{showdayofmonth}
      {\DTMsrCyrlMEdayordinal{##3}\DTMsrCyrlMEdaymonthsep}%
      {}%
    \DTMifbool{sr-Cyrl-ME}{monthi}%
      {\DTMsrCyrlMEimonthname{##2}}%
      {\DTMsrCyrlMEnoimonthname{##2}}%
    \DTMifbool{sr-Cyrl-ME}{showyear}%
    {%
      \DTMsrCyrlMEmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
  \renewcommand*\DTMDisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlMEWeekdayname{##4}%
        \DTMsrCyrlMEdowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl-ME}{showdayofmonth}
    {%
      \DTMsrCyrlMEdayordinal{##3}\DTMsrCyrlMEdaymonthsep%
      \DTMifbool{sr-Cyrl-ME}{monthi}%
        {\DTMsrCyrlMEimonthname{##2}}%
        {\DTMsrCyrlMEnoimonthname{##2}}%
    }%
    {%
      \DTMifbool{sr-Cyrl-ME}{monthi}%
        {\DTMsrCyrlMEiMonthname{##2}}%
        {\DTMsrCyrlMEnoiMonthname{##2}}%
    }%
    \DTMifbool{sr-Cyrl-ME}{showyear}%
    {%
      \DTMsrCyrlMEmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
}%
{% time style
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMifbool{sr-Cyrl-ME}{leadingzero}{\DTMtwodigits{##1}}{\number##1}%
    \DTMsrCyrlMEtimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMsrCyrlMEtimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones%
  \DTMsrCyrlMEzonemaps%
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{sr-Cyrl-ME}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0
      \else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMsrCyrlMEtimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate%
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrlMEdatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrlMEtimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}[9]{%
    \ifDTMshowdate%
      \DTMDisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrlMEdatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrlMEtimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
}%
   \newcommand*{\DTMsrCyrlMEmonthordinal}[1]{%
       \DTMifbool{sr-Cyrl-ME}{leadingzero}{\DTMtwodigits{#1}}{\number#1}.}%
\DTMdefchoicekey{sr-Cyrl-ME}{monthord}%
[\@dtm@val\@dtm@nr]{arabic,roman,romanlsc}{%
 \ifcase\@dtm@nr\relax
   \renewcommand*{\DTMsrCyrlMEmonthordinal}[1]{%
       \DTMifbool{sr-Cyrl-ME}{leadingzero}%
         {\DTMtwodigits{##1}}{\number##1}\DTMsrCyrlMEdatesep}%
 \or%
   \renewcommand*{\DTMsrCyrlMEmonthordinal}[1]{%
    \DTMtexorpdfstring{\protect\DTMserbianordinalROMAN{##1}}%
    {serbianordinalROMAN{##1}}}%
 \or%
   \renewcommand*{\DTMsrCyrlMEmonthordinal}[1]{%
    \DTMtexorpdfstring{\textsc{\protect\DTMserbianordinalroman{##1}}}%
    {serbianordinalROMAN{##1}}}%
 \fi
}
\DTMnewstyle%
{sr-Cyrl-ME-numeric}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlMEweekdayname{##4}%
        \DTMsrCyrlMEdowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl-ME}{showdayofmonth}%
    {\DTMsrCyrlMEdayordinal{##3}\DTMsrCyrlMEdaymonthsep}%
    {}%
    \DTMsrCyrlMEmonthordinal{##2}%
    \DTMifbool{sr-Cyrl-ME}{showyear}%
    {%
      \DTMsrCyrlMEmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
  \renewcommand*\DTMDisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlMEWeekdayname{##4}%
        \DTMsrCyrlMEdowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl-ME}{showdayofmonth}%
    {\DTMsrCyrlMEdayordinal{##3}\DTMsrCyrlMEdaymonthsep}%
    {}%
    \DTMsrCyrlMEmonthordinal{##2}%
    \DTMifbool{sr-Cyrl-ME}{showyear}%
    {%
      \DTMsrCyrlMEmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
}%
{% time style
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMifbool{sr-Cyrl-ME}{leadingzero}{\DTMtwodigits{##1}}{\number##1}%
    \DTMsrCyrlMEtimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMsrCyrlMEtimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones%
  \DTMsrCyrlMEzonemaps%
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{sr-Cyrl-ME}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0
      \else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMsrCyrlMEtimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate%
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrlMEdatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrlMEtimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}{\DTMdisplay}%
}
\newcommand*{\DTMsrCyrlMEzonemaps}{%
  \DTMdefzonemap{01}{00}{CET}%
  \DTMdefzonemap{02}{00}{CEST}%
}
\DTMifcaseregional%
{}% do nothing
{\DTMsetstyle{sr-Cyrl-ME}}%
{\DTMsetstyle{sr-Cyrl-ME-numeric}}%
\ifcsundef{date\CurrentTrackedDialect}
{%
  \ifundef\dateserbianc%
  {% do nothing
  }%
  {%
    \def\dateserbianc{%
      \DTMifcaseregional%
      {}% do nothing
      {\DTMsetstyle{sr-Cyrl-ME}}%
      {\DTMsetstyle{sr-Cyrl-ME-numeric}}%
    }%
  }%
}%
{%
  \csdef{date\CurrentTrackedDialect}{%
    \DTMifcaseregional%
    {}% do nothing
      {\DTMsetstyle{sr-Cyrl-ME}}%
      {\DTMsetstyle{sr-Cyrl-ME-numeric}}%
  }%
}%
\endinput
%%
%% End of file `datetime2-sr-Cyrl-ME.ldf'.
