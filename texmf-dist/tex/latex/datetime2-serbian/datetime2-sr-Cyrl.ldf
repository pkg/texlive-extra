%%
%% This is file `datetime2-sr-Cyrl.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-serbian.dtx  (with options: `datetime2-sr-Cyrl.ldf,package')
%% 
%%  datetime2-serbian.dtx
%%  Copyright 2019 Andrej Radović
%%  Copyright 2019 Nicola Talbot
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `maintained'.
%% 
%%  The Current Maintainer of this work is Andrej Radović.
%% 
%%  This work consists of the files datetime2-serbian.dtx and
%%  datetime2-serbian.ins and the derived files are as follows:
%%    datetime2-serbian-base.ldf
%%    datetime2-serbian-base-utf8.ldf
%%    datetime2-serbian-base-ascii.ldf
%%    datetime2-serbian.ldf
%%    datetime2-sr-Latn.ldf
%%    datetime2-sr-Latn-RS.ldf
%%    datetime2-sr-Latn-ME.ldf
%%    datetime2-sr-Latn-BA.ldf
%%    datetime2-serbianc.ldf
%%    datetime2-sr-Cyrl.ldf
%%    datetime2-sr-Cyrl-RS.ldf
%%    datetime2-sr-Cyrl-ME.ldf
%%    datetime2-sr-Cyrl-BA.ldf
%% 
%% 
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\ProvidesDateTimeModule{sr-Cyrl}[2019/11/22 v2.1.0]
\RequireDateTimeModule{serbianc}
\newcommand*{\DTMsrCyrldowdaysep}{,\space}
\newcommand*{\DTMsrCyrldaymonthsep}{%
  \DTMtexorpdfstring{\protect~}{\space}%
}
\newcommand*{\DTMsrCyrlmonthyearsep}{\space}
\newcommand*{\DTMsrCyrldatetimesep}{\space}
\newcommand*{\DTMsrCyrltimezonesep}{\space}
\newcommand*{\DTMsrCyrldatesep}{.}
\newcommand*{\DTMsrCyrltimesep}{.}
\DTMdefkey{sr-Cyrl}{dowdaysep}%
    {\renewcommand*{\DTMsr-Cyrldowdaysep}{#1}}
\DTMdefkey{sr-Cyrl}{daymonthsep}%
    {\renewcommand*{\DTMsr-Cyrldaymonthsep}{#1}}
\DTMdefkey{sr-Cyrl}{monthyearsep}%
    {\renewcommand*{\DTMsr-Cyrlmonthyearsep}{#1}}
\DTMdefkey{sr-Cyrl}{datetimesep}%
    {\renewcommand*{\DTMsr-Cyrldatetimesep}{#1}}
\DTMdefkey{sr-Cyrl}{timezonesep}%
    {\renewcommand*{\DTMsr-Cyrltimezonesep}{#1}}
\DTMdefkey{sr-Cyrl}{datesep}%
    {\renewcommand*{\DTMsr-Cyrldatesep}{#1}}
\DTMdefkey{sr-Cyrl}{timesep}%
    {\renewcommand*{\DTMsr-Cyrltimesep}{#1}}
    %\changes{2.0.1}{2019-11-11}{Adopted semantic versioning.}
\newcommand*{\DTMsrCyrlweekdayname}%
{\DTMserbiancyrekweekdayname}

\newcommand*{\DTMsrCyrlWeekdayname}%
    {\DTMserbiancyrekWeekdayname}
    %\changes{2.0.1}{2019-11-11}{Adopted semantic versioning.}
\DTMdefchoicekey{sr-Cyrl}%
    {pronunciation}[\@dtm@val\@dtm@nr]{ekavian,ijekavian}{%
  \ifcase\@dtm@nr\relax
    \renewcommand*{\DTMsrCyrlweekdayname}%
        {\DTMserbiancyrekweekdayname}%
    \renewcommand*{\DTMsrCyrlWeekdayname}%
        {\DTMserbiancyrekWeekdayname}%
  \or%
    \renewcommand*{\DTMsrCyrlweekdayname}%
        {\DTMserbiancyrijweekdayname}%
    \renewcommand*{\DTMsrCyrlWeekdayname}
        {\DTMserbiancyrijWeekdayname}%
  \fi
}
\DTMdefboolkey{sr-Cyrl}{monthi}[true]{}
\DTMsetbool{sr-Cyrl}{monthi}{false}
\DTMdefboolkey{sr-Cyrl}{leadingzero}[true]{}
\DTMsetbool{sr-Cyrl}{leadingzero}{false}
   \newcommand*{\DTMsrCyrldayordinal}[1]{%
       \DTMifbool{sr-Cyrl}{leadingzero}%
       {\DTMtwodigits{#1}}%
       {\number#1}\DTMsrCyrldatesep}%
\newcommand*{\DTMsrCyrlnoimonthname}{\DTMserbiancyrnoimonthname}
\newcommand*{\DTMsrCyrlnoiMonthname}{\DTMserbiancyrnoiMonthname}
\newcommand*{\DTMsrCyrlimonthname}{\DTMserbiancyrimonthname}
\newcommand*{\DTMsrCyrliMonthname}{\DTMserbiancyriMonthname}
\DTMdefboolkey{sr-Cyrl}{mapzone}[true]{}
\DTMsetbool{sr-Cyrl}{mapzone}{true}
\DTMdefboolkey{sr-Cyrl}{showdayofmonth}[true]{}
\DTMsetbool{sr-Cyrl}{showdayofmonth}{true}
\DTMdefboolkey{sr-Cyrl}{showyear}[true]{}
\DTMsetbool{sr-Cyrl}{showyear}{true}
\DTMnewstyle%
{sr-Cyrl}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlweekdayname{##4}%
        \DTMsrCyrldowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl}{showdayofmonth}
      {\DTMsrCyrldayordinal{##3}\DTMsrCyrldaymonthsep}%
      {}%
    \DTMifbool{sr-Cyrl}{monthi}%
      {\DTMsrCyrlimonthname{##2}}%
      {\DTMsrCyrlnoimonthname{##2}}%
    \DTMifbool{sr-Cyrl}{showyear}%
    {%
      \DTMsrCyrlmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
  \renewcommand*\DTMDisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlWeekdayname{##4}%
        \DTMsrCyrldowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl}{showdayofmonth}
    {%
      \DTMsrCyrldayordinal{##3}\DTMsrCyrldaymonthsep%
      \DTMifbool{sr-Cyrl}{monthi}%
        {\DTMsrCyrlimonthname{##2}}%
        {\DTMsrCyrlnoimonthname{##2}}%
    }%
    {%
      \DTMifbool{sr-Cyrl}{monthi}%
        {\DTMsrCyrliMonthname{##2}}%
        {\DTMsrCyrlnoiMonthname{##2}}%
    }%
    \DTMifbool{sr-Cyrl}{showyear}%
    {%
      \DTMsrCyrlmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
}%
{% time style
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMifbool{sr-Cyrl}{leadingzero}{\DTMtwodigits{##1}}{\number##1}%
    \DTMsrCyrltimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMsrCyrltimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones%
  \DTMsrCyrlzonemaps%
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{sr-Cyrl}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0
      \else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMsrCyrltimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate%
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrldatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrltimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}[9]{%
    \ifDTMshowdate%
      \DTMDisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrldatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrltimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
}%
   \newcommand*{\DTMsrCyrlmonthordinal}[1]{%
       \DTMifbool{sr-Cyrl}{leadingzero}{\DTMtwodigits{#1}}{\number#1}.}%
\DTMdefchoicekey{sr-Cyrl}{monthord}%
[\@dtm@val\@dtm@nr]{arabic,roman,romanlsc}{%
 \ifcase\@dtm@nr\relax
   \renewcommand*{\DTMsrCyrlmonthordinal}[1]{%
       \DTMifbool{sr-Cyrl}{leadingzero}%
         {\DTMtwodigits{##1}}{\number##1}\DTMsrCyrldatesep}%
 \or%
   \renewcommand*{\DTMsrCyrlmonthordinal}[1]{%
    \DTMtexorpdfstring{\protect\DTMserbianordinalROMAN{##1}}%
    {serbianordinalROMAN{##1}}}%
 \or%
   \renewcommand*{\DTMsrCyrlmonthordinal}[1]{%
    \DTMtexorpdfstring{\textsc{\protect\DTMserbianordinalroman{##1}}}%
    {serbianordinalROMAN{##1}}}%
 \fi
}
\DTMnewstyle%
{sr-Cyrl-numeric}% label
{% date style
  \renewcommand*\DTMdisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlweekdayname{##4}%
        \DTMsrCyrldowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl}{showdayofmonth}%
    {\DTMsrCyrldayordinal{##3}\DTMsrCyrldaymonthsep}%
    {}%
    \DTMsrCyrlmonthordinal{##2}%
    \DTMifbool{sr-Cyrl}{showyear}%
    {%
      \DTMsrCyrlmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
  \renewcommand*\DTMDisplaydate[4]{%
    \ifDTMshowdow%
      \ifnum##4>-1
        \DTMsrCyrlWeekdayname{##4}%
        \DTMsrCyrldowdaysep%
      \fi
    \fi
    \DTMifbool{sr-Cyrl}{showdayofmonth}%
    {\DTMsrCyrldayordinal{##3}\DTMsrCyrldaymonthsep}%
    {}%
    \DTMsrCyrlmonthordinal{##2}%
    \DTMifbool{sr-Cyrl}{showyear}%
    {%
      \DTMsrCyrlmonthyearsep%
      ##1\DTMfinaldot{}%
    }%
    {}%
  }%
}%
{% time style
  \renewcommand*\DTMdisplaytime[3]{%
    \DTMifbool{sr-Cyrl}{leadingzero}{\DTMtwodigits{##1}}{\number##1}%
    \DTMsrCyrltimesep\DTMtwodigits{##2}%
    \ifDTMshowseconds\DTMsrCyrltimesep\DTMtwodigits{##3}\fi
  }%
}%
{% zone style
  \DTMresetzones%
  \DTMsrCyrlzonemaps%
  \renewcommand*{\DTMdisplayzone}[2]{%
    \DTMifbool{sr-Cyrl}{mapzone}%
    {\DTMusezonemapordefault{##1}{##2}}%
    {%
      \ifnum##1<0
      \else+\fi\DTMtwodigits{##1}%
      \ifDTMshowzoneminutes\DTMsrCyrltimesep\DTMtwodigits{##2}\fi
    }%
  }%
}%
{% full style
  \renewcommand*{\DTMdisplay}[9]{%
    \ifDTMshowdate%
      \DTMdisplaydate{##1}{##2}{##3}{##4}%
      \DTMsrCyrldatetimesep%
    \fi
    \DTMdisplaytime{##5}{##6}{##7}%
    \ifDTMshowzone%
      \DTMsrCyrltimezonesep%
      \DTMdisplayzone{##8}{##9}%
    \fi
  }%
  \renewcommand*{\DTMDisplay}{\DTMdisplay}%
}
\newcommand*{\DTMsrCyrlzonemaps}{%
  \DTMdefzonemap{01}{00}{CET}%
  \DTMdefzonemap{02}{00}{CEST}%
}
\DTMifcaseregional%
{}% do nothing
{\DTMsetstyle{sr-Cyrl}}%
{\DTMsetstyle{sr-Cyrl-numeric}}%
\ifcsundef{date\CurrentTrackedDialect}
{%
  \ifundef\dateserbianc%
  {% do nothing
  }%
  {%
    \def\dateserbianc{%
      \DTMifcaseregional%
      {}% do nothing
      {\DTMsetstyle{sr-Cyrl}}%
      {\DTMsetstyle{sr-Cyrl-numeric}}%
    }%
  }%
}%
{%
  \csdef{date\CurrentTrackedDialect}{%
    \DTMifcaseregional%
    {}% do nothing
      {\DTMsetstyle{sr-Cyrl}}%
      {\DTMsetstyle{sr-Cyrl-numeric}}%
  }%
}%
\endinput
%%
%% End of file `datetime2-sr-Cyrl.ldf'.
