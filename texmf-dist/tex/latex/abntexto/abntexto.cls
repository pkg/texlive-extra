%! Public Domain Software
%! 2022-10-12
%! 1.2.1-alpha
%! Elayson
%! abntexto.classe at gmail.com

%! ANNOUNCEMETS

%!> Changed the manual's typographic font to a serif because they are better for reading long texts — although the manual is purposely short.

%!> Removal of \srcname macro because it is useless.

%! SUMÁRIO

%! PRÉ-CONFIGURAÇÕES
%! FONTES
%! LAYOUT
%! SUMÁRIO
%! SECIONAMEMTO
%! CITAÇÕES
%! ALÍNEAS
%! TABELAS
%! ÁREAS DE LEGENDA
%! NOTAS DE RODAPÉ
%! UTILIDADES
%! IDENTIFICAÇÃO DO TRABALHO
%! A VÍRGULA COMO SEPARADOR DECIMAL
%! INICIALIZAÇÃO

% |sec RECOMENDAÇÕES
% !=======================================================

% Macros internas não podem ser modificadas ou usadas diretamente. Isso é porque essas macros podem ser redefinidos ou até renomeados em futuras versões desta classe. Por macros internas entende-se aquelas com prefixo do pacote em questão, neste caso, \cs{tnba@}. Se você precisa de algum comando interno, defina outro sem anexar o prefixo.

% \sec PRÉ-CONFIGURAÇÕES
% !=======================================================

% Identificação do formato \TeX\ e da classe. Apenas o pacote \pkg{keyval} é carregado no \pkg{abntexto}.

% |bcode !------------------------------------------------
\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{abntexto}[2022-10-12 1.2.1-alpha Preparation of works in ABNT standards]

\RequirePackage{keyval}
% |ecode !------------------------------------------------

% Definição de um sistema \emph{chave=valor} pessoal e de macros auxiliares. Perceba que \cs{\tnba@bodyhook} não pode ser igual a \cs\relax, mas \cs\empty\ ao invés.

% |bcode !------------------------------------------------
\def\tnba@keyprefix{tnba}

\def\tnba@key{\define@key{\tnba@keyprefix @\tnba@keyfamily}}
\def\tnba@setkeys{\setkeys{\tnba@keyprefix @\tnba@keyfamily}}
\def\tnba@setmacrokeys#1{\setkeys{\tnba@keyprefix @#1}}

\def\advancecount#1{\global\advance#1by1 }
\def\tnba@addto#1#2{\expandafter\def\expandafter#1\expandafter{#1#2}}
\def\tnba@tobody{\tnba@addto\tnba@bodyhook}

\let\tnba@bodyhook=\empty
\AddToHook{begindocument}{\def\tnba@body{}\tnba@bodyhook}
% |ecode !------------------------------------------------

% Comando de \emph{debug}.

% |bcode !------------------------------------------------
\def\tnba@error{\ClassError{abntexto}}
% |ecode !------------------------------------------------

% \sec FONTE TIPOGRÁFICA
% !=======================================================

% Apenas o tamanho 12\,pt está definido. Defina outros usando \cs\fontsizedef\cc{size}\cc{defs}.

% Diferentemente de outras âncoras, \cs{\tnba@setfontsizehook} deve ser executado antes de \cs\begin\ccval{document} para evitar o alerta sobre \cs\normalsize\ não estar definido.

% |bcode !------------------------------------------------
\def\fontsizedef#1#2{\@namedef{tnba@fontsize@#1}{#2}}
\def\sizeselectordef#1#2#3{\DeclareRobustCommand#1{\@setfontsize#1{#2}{#3}}}
\def\normalsizedef#1#2{\def\normalsize{\@setfontsize\normalsize{#1}{#2}}}
\def\setfontsize#1{\ifcsname tnba@fontsize@#1\endcsname
	\def\tnba@setfontsizehook{\csname tnba@fontsize@#1\endcsname} \else
	\let\normalsize=\relax
	\tnba@error{Tamanho de fonte *#1* desconhecido}{}\relax \fi
}

\AddToHook{begindocument/before}{\tnba@setfontsizehook}

\def\spacing#1{\def\currspacing{#1}%
	\ifx\tnba@body\undefined
		\tnba@tobody{\tnba@spacing{#1}} \else
		\tnba@spacing{#1}\fi \ignorespaces
}
\def\tnba@spacing#1{\dimen0=\f@size pt \dimen0=#1\dimen0 \baselineskip=\dimen0}

\def\singlesp{\baselineskip=\normalbaselineskip}
\def\onehalfsp{\spacing{1.5}}
\def\doublesp{\spacing{2}}
% |ecode !------------------------------------------------

% \sec \emph{LAYOUT}
% !=======================================================

% |bcode !------------------------------------------------
\def\tnba@keyfamily{paper}

\def\papersizedef#1(#2,#3){\def\tnba@keyfamily{paper}%
	\tnba@key{#1}[]{\def\tnba@W{#2}\def\tnba@H{#3}}%
}

\tnba@key{width}    {\def\tnba@W{#1}}
\tnba@key{height}   {\def\tnba@H{#1}}
\tnba@key{left}     {\def\tnba@L{#1}}
\tnba@key{right}    {\def\tnba@R{#1}}
\tnba@key{top}      {\def\tnba@T{#1}}
\tnba@key{bottom}   {\def\tnba@B{#1}}
\tnba@key{twoside}[true]{\def\tnba@twoside{#1}}

\def\paper#1{\tnba@setmacrokeys{paper}{#1}%
	\paperwidth=\tnba@W
	\paperheight=\tnba@H
	\oddsidemargin=\tnba@L
	\evensidemargin=\tnba@R
	\topmargin=\tnba@T
	\textwidth=\paperwidth 
	\textheight=\paperheight
	\headheight=12pt 
	\headsep=1cm 
	\advance\topmargin-\headheight \advance\topmargin-\headsep 
	\advance\textwidth-\tnba@L     \advance\textwidth-\tnba@R
	\advance\textheight-\tnba@T    \advance\textheight-\tnba@B 
	\setbox0=\hbox{\@setfontsize\@tempa{10pt}{12pt}\@tempa 1}%
	\advance\headsep-\ht0 
	\ifx\pdfpagewidth\undefined
		\ifx\pagewidth\undefined
			\tnba@error{O comando \@backslashchar paper só suporta saída em PDF.
			 			 Use o formato PDF ou carregue o pacote *geometry*}\expandafter\enddocument\relax \else
			\horigin=0pt 
			\vorigin=0pt 
			\pagewidth=\paperwidth 
			\pageheight=\paperheight
		\fi
	\else
		\ifx\pdfhorigin\undefined
			\advance\oddsidemargin-1in 
			\advance\topmargin-1in 
			\pdfpagewidth=\paperwidth
			\pdfpageheight=\paperheight
		\else
			\pdfhorigin=0pt 
			\pdfvorigin=0pt 
			\pdfpagewidth=\paperwidth
			\pdfpageheight=\paperheight
		\fi
	\fi
	\ifx\tnba@twoside\undefined \else
	\csname @twoside\tnba@twoside\endcsname \fi
}
% |ecode !------------------------------------------------

% O estilo de página \cs\pretextual\ omite a numeração de página e desliga o modo \cs{twoside} porque os elementos pré-textuais que compreendem as folhas da capa à do sumário, devem constar no anverso da página, ou seja, na “frente” da folha.

% A instrução \cs\textual\ põe a numeração de página no topo direito nas páginas ímpares e no topo esquerdo em paginas pares. Além disso, \cs{twoside} vai espelhar as margens horizontais.

% |bcode !------------------------------------------------
\def\pretextual{\paper{twoside=false}%
	\let\@oddhead=\empty
	\let\@evenhead=\empty
	\let\@oddfoot=\empty
	\let\@evenfoot=\empty
}
\def\textual{\paper{twoside=true}%
	\def\@oddhead{{\itshape\rightmark}\hfil{\small\thepage}}%
	\def\@evenhead{{\small\thepage}\hfil\itshape\leftmark}%
	\let\@oddfoot=\empty
	\let\@evenfoot=\empty
	\justifying
	\parindent=1.5cm 
}
% |ecode !------------------------------------------------

% \sec SUMÁRIO
% !=======================================================

% Aqui, \cs\extline\ foi baseado em \cs\@dottedtocline, do kernel do LaTeX. Ele não é uma instrução privada, ou seja, com prefixo interno, pois existe a chance de o leitor querer redefiní-lo (o mesmo vale para \cs\extlineshape).

% |bcode !------------------------------------------------
\def\thepage{\the\c@page}

\def\extline#1#2#3#4{%
	\begingroup
		\csname use#1\romannumeral#2font\endcsname \nobreak
		\extlineshape \relax
		\hskip-\leftskip {#3}\nobreak
		\leaders\hbox{$\m@th \mkern4.5mu\hbox{.}\mkern4.5mu$}\hfil\nobreak
		\hbox to1.55em{\hss #4\kern-1pt\kern1pt}\par
	\endgroup
}
\def\extlineshape{%
	\parindent=0pt 
	\leftskip=\extlabelwidth
	\rightskip=2.55em 
	\parfillskip=-\rightskip
	\interlinepenalty=10000 
	\leavevmode
}

\def\l@section#1#2{\smartaboveskip{\penalty351}{\vskip1em plus1pt}%
	\begingroup
		\usetocifont \nobreak
		\extlineshape \relax
		\hskip-\leftskip \maybeuctoc{#1}\nobreak\hfil\nobreak
		\hbox to1.55em{\hss #2\kern-1pt\kern1pt}\par
	\endgroup
}
\def\l@subsection    {\extline{toc}{2}}
\def\l@subsubsection {\extline{toc}{3}}
\def\l@paragraph     {\extline{toc}{4}}
\def\l@subparagraph  {\extline{toc}{5}}
% |ecode !------------------------------------------------

% Comandos de personalização do ToC (sumário).

% |bcode !------------------------------------------------
\def\tociuppercase{\def\maybeuctoc{\MakeUppercase}}
\def\tocilowercase{\let\maybeuctoc=\relax}

\def\tocifont#1{\def\usetocifont{#1}}
\def\tociifont#1{\def\usetociifont{#1}}
\def\tociiifont#1{\def\usetociiifont{#1}}
\def\tocivfont#1{\def\usetocivfont{#1}}
\def\tocvfont#1{\def\usetocvfont{#1}}
% |ecode !------------------------------------------------

% \sec SECIONAMENTO
% !=======================================================

% As linhas abaixo são basicamente uma cópia da funcionalidade do formato OpTeX. O par \cs\smartaboveskip, \cs\smartbelowskip, em especial, serve para eliminar \cs\vskip’s redundantes.

% |bcode !------------------------------------------------
\def\tnba@nbpar{\interlinepenalty=10000\par}
\def\noindentfirst{\global\everypar={\tnba@wipeepar \setbox7=\lastbox}}
\def\tnba@wipeepar{\global\everypar={}}

\newskip\savedtitleskip
\newskip\savedlastskip

\def\smartaboveskip#1#2{\savedlastskip=\lastskip
	\ifdim\lastskip>\z@ \vskip-\lastskip \fi
	\ifnum\lastpenalty=11333 \vskip-\savedtitleskip \else #1\fi
	\ifdim\savedlastskip>\z@ \setbox0=\vbox{#2\global\dimen0=\lastskip}%
	\else \dimen0=\maxdimen \fi
	\ifdim\savedlastskip>\dimen0 \vskip\savedlastskip \else #2\fi
}
\def\smartbelowskip#1{#1\global\savedtitleskip=\lastskip \penalty11333}
% |ecode !------------------------------------------------

% |bcode !------------------------------------------------
\newcount\sectioncount
\newcount\subsectioncount
\newcount\subsubsectioncount
\newcount\paragraphcount
\newcount\subparagraphcount

\def\thesection{\the\sectioncount}
\def\thesubsection{\thesection .\the\subsectioncount}
\def\thesubsubsection{\thesubsection .\the\subsubsectioncount}
\def\theparagraph{\thesubsubsection .\the\paragraphcount}
\def\thesubparagraph{\theparagraph .\the\subparagraphcount}

\def\sectionmark#1{}
\def\subsectionmark#1{}
\def\subsubsectionmark#1{}
\def\paragraphmark#1{}
\def\subparagraphmark#1{}
% |ecode !------------------------------------------------

% Nesta parte estão definidos os comandos secionais.

% |bcode !------------------------------------------------
\def\section#1{\par \smartaboveskip{\penalty351}{\vskip\sectionabove}\noindent
	\advancecount\sectioncount
	\def\@currentlabel{seção~\thesection}%+/
	\setbox0=\hbox{\usesectionfont \thesection\quad}%
	{\usesectionfont \hangindent=\wd0 \box0 \maybeucsec{#1}\tnba@nbpar}%
%	\noindentfirst
	\hreftocline{section}%
	\addcontentsline{toc}{section}{\protect\extlabelbox{\thesection\hss}#1}%
	\nobreak \smartbelowskip{\vskip\sectionbelow}%
}
\def\subsection#1{\par \smartaboveskip{\penalty351}{\vskip\subsectionabove}\noindent
	\advancecount\subsectioncount
	\setbox0=\hbox{\usesubsectionfont \thesubsection\quad}%
	{\usesubsectionfont \hangindent=\wd0 \box0 #1\tnba@nbpar}%
%	\noindentfirst
	\hreftocline{subsection}%
	\addcontentsline{toc}{subsection}{\protect\extlabelbox{\thesubsection\hss}#1}%
	\nobreak \smartbelowskip{\vskip\subsectionbelow}%
}
\def\subsubsection#1{\par \smartaboveskip{\penalty351}{\vskip\subsubsectionabove}\noindent
	\advancecount\subsubsectioncount
	\setbox0=\hbox{\usesubsubsectionfont \thesubsubsection\quad}%
	{\usesubsubsectionfont \hangindent=\wd0 \box0 #1\tnba@nbpar}%
%	\noindentfirst
	\hreftocline{subsubsection}%
	\addcontentsline{toc}{subsubsection}{\protect\extlabelbox{\thesubsubsection\hss}#1}%
	\nobreak \smartbelowskip{\vskip\subsubsectionbelow}%
}
\def\paragraph#1{\par \smartaboveskip{\penalty351}{\vskip\paragraphabove}\noindent
	\advancecount\paragraphcount
	\setbox0=\hbox{\useparagraphfont \theparagraph\quad}%
	{\useparagraphfont \hangindent=\wd0 \box0 #1\tnba@nbpar}%
%	\noindentfirst
	\hreftocline{paragraph}%
	\addcontentsline{toc}{paragraph}{\protect\extlabelbox{\theparagraph\hss}#1}%
	\nobreak \smartbelowskip{\vskip\paragraphbelow}%
}
\def\subparagraph#1{\par \smartaboveskip{\penalty351}{\vskip\subparagraphabove}\noindent
	\advancecount\subparagraphcount
	\setbox0=\hbox{\subsubparagraphfont \thesubparagraph\quad}%
	{\usesubparagraphfont \hangindent=\wd0 \box0 #1\tnba@nbpar}%
%	\noindentfirst
	\hreftocline{subparagraph}%
	\addcontentsline{toc}{subparagraph}{\protect\extlabelbox{\thesubparagraph\hss}#1}%
	\nobreak \smartbelowskip{\vskip\subparagraphbelow}%
}

\def\hreftocline#1{\Hy@MakeCurrentHrefAuto{#1}%
	\Hy@raisedlink{\hyper@anchorstart{\@currentHref}\hyper@anchorend}%
}
\tnba@tobody{\ifx\hypertarget\undefined \let\hreftocline=\@gobble \fi}
% |ecode !------------------------------------------------

% A macro \cs\heading, em particular, serve para produzir títulos não contados, como “Sumário”, “Lista de figuras”, “Apêndice” etc.

% |bcode !------------------------------------------------
\def\heading#1{\noindent
	{\usesectionfont \centering \maybeucsec{#1}\tnba@nbpar}%
	\nobreak \vskip\sectionbelow
}
% |ecode !------------------------------------------------

% Comandos de personalização de seções.

% |bcode !------------------------------------------------
\def\sectionuppercase{\def\maybeucsec{\MakeUppercase}}
\def\sectionlowercase{\let\maybeucsec=\relax}

\def\sectionfont#1{\def\usesectionfont{#1}}
\def\subsectionfont#1{\def\usesubsectionfont{#1}}
\def\subsubsectionfont#1{\def\usesubsubsectionfont{#1}}
\def\paragraphfont#1{\def\useparagraphfont{#1}}
\def\subparagraphfont#1{\def\usesubparagraphfont{#1}}

\def\sectionspaces#1#2{\def\sectionabove{#1}\def\sectionbelow{#2}}
\def\subsectionspaces#1#2{\def\subsectionabove{#1}\def\subsectionbelow{#2}}
\def\subsubsectionspaces#1#2{\def\subsubsectionabove{#1}\def\subsubsectionbelow{#2}}
\def\paragraphspaces#1#2{\def\paragraphabove{#1}\def\paragraphbelow{#2}}
\def\subparagraphspaces#1#2{\def\subparagraphabove{#1}\def\subparagraphbelow{#2}}

\def\toclabelwidth{3.4ex}
\def\toclabelbox#1{\hbox to\extlabelwidth{#1\hfil}}
% |ecode !------------------------------------------------

% Após a chamada da instrução \cs\appendix, \cs\section\ vai ser alterado para produzir títulos centralizados com o prefixo “Apêndice”. O mesmo vale para \annex.

% |bcode !------------------------------------------------
\newcount\appendixcount
\newcount\annexcount

\def\theappendix{\@Alph\appendixcount}
\def\theannex{\@Alph\annexcount}
\def\appendixlabelwidth{6.6em}
\def\annexlabelwidth{6.2em}

\def\l@appendix{\let\extlabelwidth=\appendixlabelwidth
	\def\extlabelbox##1{\hbox to\extlabelwidth{Apêndice~##1~---~\hfil}}%
 	\l@section
}
\def\l@annex{\let\extlabelwidth=\annexlabelwidth
	\def\extlabelbox##1{\hbox to\extlabelwidth{Anexo~##1~---~\hfil}}%
 	\l@section
}

\def\appendix{\def\section##1{\newpage\noindent \advance\appendixcount by1
	{\usesectionfont \centering \maybeucsec{Apêndice~\theappendix~---~##1}\tnba@nbpar}%
		\hreftocline{appendix}%
		\addcontentsline{toc}{appendix}{\protect\extlabelbox{\theappendix}##1}%
		\nobreak \vskip\sectionbelow
	}%
}
\def\annex{\def\section##1{\newpage\noindent \advance\annexcount by1
	{\usesectionfont \centering \maybeucsec{Anexo~\theannex~---~##1}\tnba@nbpar}%
		\hreftocline{annex}%
		\addcontentsline{toc}{annex}{\protect\extlabelbox{\theannex}##1}%
		\nobreak \vskip\sectionbelow
	}%
}
% |ecode !------------------------------------------------

% \sec CITAÇÕES
% !=======================================================

% |bcode !------------------------------------------------
\def\qt#1{``#1''}

\def\quote#1{\smartaboveskip{\penalty351}{\vskip\baselineskip}%
	\hbox to\hsize{\hfil\vtop{\advance\hsize-4cm \noindent\small #1\tnba@getprevdepth}}\tnba@useprevdepth
	\smartbelowskip{\vskip\baselineskip}%
}
% |ecode !------------------------------------------------

% \sec ALÍNEAS
% !=======================================================

% Estas linhas não foram baseadas no ambiente \cs{list} do LaTeX, portanto, não podem ser personalizadas pelo pacote \cs{enumitem}, por exemplo.

% |bcode !------------------------------------------------
\newcount\topicsdepth
\newcount\topicscount

\def\topicsitem{\par\leavevmode\noindent \advance\topicscount by1 
	\def\@currentlabel{alínea~\@alph\topicscount )}%
	\kern\dimexpr-\parindent-\topicslabelwidth\relax
	\hbox to\topicslabelwidth{\topicsmakelabel\hfil}%
	\ignorespaces
}
\def\topicslabeli{\@alph\topicscount )}
\def\topicslabelii{---}

\def\topics{\par \advance\topicsdepth by1 
	\ifnum\topicsdepth=1 \advance\leftskip\parindent \fi
	\advance\leftskip\topicslabelwidth
	\edef\topicsmakelabel{\expandafter\noexpand\csname topicslabel\romannumeral\topicsdepth\endcsname}%
	\let\item=\topicsitem
	\tnba@addto\@esphack\ignorespaces
}
\let\endtopics=\par
% |ecode !------------------------------------------------

% \sec TABELAS
% !=======================================================

% Na classe \cs{article} o ambiente \cs{table} inicia o modo flutuante. Mas nesta classe ele é sinônimo de \cs{tabular} porque faz mais sentido para min.

% |bcode !------------------------------------------------
\newenvironment{table}[1]{\begin{tabular}{#1}}{\end{tabular}}
% |ecode !------------------------------------------------

% \sec ÁREAS DE LEGENDA
% !=======================================================

% |bcode !------------------------------------------------
\def\tnba@lastplacenum{0}

\def\legendplacedef#1#2#3#4#5{\expandafter\newcount\csname c@#1\endcsname
	\@namedef{#1name}{#2}
	\@namedef{p@#1}##1{#4}
	\@namedef{ext@#1}{#5}
	\@namedef{l@#1}{\extline{#5}{1}}
	\expandafter\edef\csname the#1\endcsname{\expandafter\noexpand\the\csname c@#1\endcsname}
	\@namedef{#5labelwidth}{10.4ex}
	\@namedef{#5labelbox}##1{\hbox to\extlabelwidth{#2 ##1 ---\hfil}}
	\@namedef{make#5}{\heading{#3}%
		\expandafter\let\expandafter\extlabelwidth\csname #5labelwidth\endcsname
		\expandafter\let\expandafter\extlabelbox\csname #5labelbox\endcsname
		\@starttoc{#5}\twonewpage
	}
}
% |ecode !------------------------------------------------

% |bcode !------------------------------------------------
\def\tnba@makelegend{\hbox to\hsize{\hfil\tnba@makelegendA\hfil}}
\def\tnba@makelegendA{\advancecount{\csname c@\tnba@savedplace\endcsname}%
	\vtop{\setbox0=\hbox{\legendlabel \tnba@savedlegend}%
		\ifdim\wd0>\legendmaxwidth \hsize=\legendmaxwidth \else \hsize=\wd0 \fi
		\legendlabel
		\vtop{\setbox0=\hbox{\legendlabel}\advance\hsize-\wd0 \tnba@savedlegend}%
	}%
	\addcontentsline{\csname ext@\tnba@savedplace\endcsname}%
			 			{\tnba@savedplace}%
			 			{\protect\extlabelbox\thelegend \tnba@savedlegend}%
}

\def\tnba@makesrc{\hbox to\hsize{\hfil\tnba@makesrcA\hfil}\tnba@useprevdepth}
\def\tnba@makesrcA{\vtop{\hsize=\tnba@placewidth \srclabel 
	\vtop{\setbox0=\hbox{\srclabel}\advance\hsize-\wd0 \tnba@savedsrc \tnba@getprevdepth}}%
}
% |ecode !------------------------------------------------

% |bcode !------------------------------------------------
\def\legend#1#2{%
	\ifdim\lastskip<0pt \else
	\ifnum\lastpenalty=11333 \vskip-\placeabovespace \else
	\removelastskip \fi\fi
	\def\tnba@savedplace{#1}\def\tnba@savedlegend{#2}%
	\def\@currentlabel{\csname p@#1\expandafter\endcsname
		\the\expandafter\numexpr\csname c@#1\endcsname+1}%
}
\def\legendname{\csname \tnba@savedplace name\endcsname}
\def\thelegend{\the\csname c@\tnba@savedplace\endcsname}

\def\src#1{%
	\ifdim\lastskip<0pt \else
	\ifnum\lastpenalty=11333 \vskip-\placeabovespace \else
	\removelastskip \fi\fi
	\def\tnba@savedsrc{#1}%
}
% |ecode !------------------------------------------------

% |bcode !------------------------------------------------
\let\tnba@savedplace=\empty
\let\tnba@savedlegend=\empty
\let\tnba@savedsrc=\empty

\long\def\place#1{\setbox0=\hbox{\ignorespaces#1}\edef\tnba@placewidth{\the\wd0}%
	\vskip\placeabovespace
	\begingroup
		\parindent=0pt 
		\ifx\tnba@savedplace\empty\else\tnba@makelegend\fi
		\nointerlineskip\nobreak\vskip10pt 
		\hbox to\hsize{\hfil\box0\hfil}%
		\nointerlineskip\nobreak\vskip8pt 
		\tnba@makesrc
		\vskip\placebelowspace
	\endgroup
	\let\tnba@savedplace=\empty
	\let\tnba@savedlegend=\empty
	\let\tnba@savedsrc=\empty
	\let\@currentlabel=\empy
}
\def\placespaces#1#2{\def\placeabovespace{#1}\def\placebelowspace{#2}}
% |ecode !------------------------------------------------

% \sec NOTAS DE RODAPÉ
% !=======================================================

% As notas de rodapé também foram modificadas.

% |bcode !------------------------------------------------
\let\footnotesize=\relax
\def\@makefnmark{\hbox{\@textsuperscript{\selectfont\@thefnmark}}}
\def\@makefntext#1{\leavevmode \setbox0=\hbox{\@makefnmark\kern2pt}%
\hangindent=\wd0 \box0 \small #1}
\def\footnoterule{\kern18pt\hrule width5cm\kern2.6pt}
% |ecode !------------------------------------------------

% \sec UTILIDADES
% !=======================================================

% Algumas macros úteis foram definidos aqui como \cs\Enter\ que serve para produzir um espaço vertical equivalente àquele produzido com a tecla Enter em editores de texto como MS Word.

% |bcode !------------------------------------------------
\def\tnba@getprevdepth{\par\xdef\tnba@getprevdepthA{\the\prevdepth}}
\def\tnba@useprevdepth{\par\prevdepth=\tnba@getprevdepthA}

\long\def\sizedpar#1{\leavevmode\vtop{\ignorespaces#1\tnba@getprevdepth}\tnba@useprevdepth}

\def\justifying{\let\\=\tnba@origcr
	\leftskip=0pt \rightskip=0pt 
	\finalhyphendemerits=5000 
	\parfillskip=0pt plus1fil 
}
\let\tnba@origcr=\\

\def\Enter{\@ifnextchar[\tnba@EnterS{\tnba@EnterS[]}}
\def\tnba@EnterS[#1]{\vskip#1\baselineskip}

\def\twonewpage{\newpage\leavevmode\newpage}
% |ecode !------------------------------------------------

% \sec IDENTIFICAÇÃO DO TRABALHO
% !=======================================================

% |bcode !------------------------------------------------
\long\def\indexcard#1{\hbox{\vrule\vbox to7.5cm{\hsize=12.5cm \advance\hsize by-2cm \advance\hsize by-0.8pt 
	\hrule\indexcardA{#1}\vss\indexcardbar\hrule}\vrule}%
}
\long\def\indexcardA#1{\hbox spread2cm{\hss\vbox spread2cm{\vss\ttfamily#1\vss}\hss}%
}
\def\indexcardbar{\hrule\kern1cm}

\def\judgeline#1{\par{\centering \rule{.7\linewidth}{.6pt}\\ #1\Enter[1.5]}}
% |ecode !------------------------------------------------

% \sec A VÍRGULA COMO SEPARADOR DECIMAL
% !=======================================================

% No Brasil e em outro lugares a vírgula é um separador decimal, portanto \verb+$1,2$+ deve produzir um resultado diferente de \verb+$1, 2$+, por exemplo.

% As linhas abaixo são uma cópia da implementação do pacote \pkg{icomma}.

% |bcode !------------------------------------------------
{\catcode`,=\active
	\gdef,{\futurelet\@let@token\tnba@smartcomma}
}
\def\tnba@smartcomma{%
	\ifx\@let@token\@sptoken \else
	\ifx\@let@token\space    \else \mathord \fi\fi
	\tnba@mathcomma
}

\mathchardef\tnba@mathcomma\mathcode`\,
\mathcode`\,="8000 
% |ecode !------------------------------------------------

% |bcode !------------------------------------------------
\ifx\XeTeXversion\undefined
\AddToHook{begindocument/before}{
	\let\tnba@hyper@linkstart=\hyper@linkstart
	\protected\def\hyper@linkstart#1#2{\lowercase{\tnba@hyper@linkstart{#1}{#2}}}
}
\fi
% |ecode !------------------------------------------------

% \sec INICIALIZAÇÃO
% !=======================================================

% Aqui o \pkg{abntexto} define as configurações padrões. Note que \cs\pretextual\ deve ser carregado após \cs{\begin{document}} para evitar múltiplas chamadas deste caso seja redefinido pelo usuário ou escritor de pacotes.

% |bcode !------------------------------------------------
\fontsizedef{12pt}{
	\normalsizedef{12pt}{14.5pt}
	\sizeselectordef\small{10pt}{12pt}
}
\setfontsize{12pt}
\spacing{1.5}

\papersizedef{A4}(210mm,297mm)
\paper{
	A4,
	left=3cm,
	top=3cm,
	right=2cm,
	bottom=2cm
}

\frenchspacing
\parindent=0pt 
\parskip=0pt 

\tociuppercase
\sectionuppercase

\tocifont   {\bfseries}
\tociifont  {\itshape}
\tociiifont {\scshape}
\tocivfont  {}
\tocvfont   {}

\sectionfont       {\bfseries}
\subsectionfont    {\itshape}
\subsubsectionfont {\scshape}
\paragraphfont     {}
\subparagraphfont  {}

\sectionspaces       {\baselineskip}{\baselineskip}
\subsectionspaces    {\baselineskip}{\baselineskip}
\subsubsectionspaces {\baselineskip}{\baselineskip}
\paragraphspaces     {\baselineskip}{\baselineskip}
\subparagraphspaces  {\baselineskip}{\baselineskip}

\legendplacedef{figure}{Figura}{Lista de figuras}{fig.\,#1}{lof}
\legendplacedef{table}{Tabela}{Lista de tabelas}{tab.\,#1}{lot}

\def\legendlabel{\legendname ~\thelegend ~---~}
\def\srclabel{\raggedright\small Fonte:~}
\def\legendmaxwidth{0.7\linewidth}

\placespaces{\baselineskip}{\baselineskip}

\def\maketoc{\heading{Sumário}
	\let\extlabelwidth=\toclabelwidth
	\let\extlabelbox=\toclabelbox
	\@starttoc{toc}\newpage
}

\def\topicslabelwidth{4ex}

\arrayrulewidth=0.4pt 

\topskip=12pt 

\AddToHook{begindocument}{\pretextual}

\endinput
% |ecode !------------------------------------------------
