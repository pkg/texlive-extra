%% This is file `bfh-layout-tabular.cfg' version 2.1.4 (2022/11/15),
%% it is part of
%% BFH-CI -- Corporate Design for Bern University of Applied Sciences
%% ----------------------------------------------------------------------------
%%
%%  Copyright (C) 2021–2022 by
%%      Marei Peischl <marei@peitex.de>
%%      Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% ============================================================================
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2008/05/04 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainers of this work are
%%   Marei Peischl <bfh-ci@peitex.de>
%%   Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% The development respository can be found at
%% https://gitlab.ti.bfh.ch/bfh-latex/bfh-ci/
%% Please use the issue tracker for feedback!
%%
%% ============================================================================
%%
\ProvidesExplFile{bfh-layout-tabular.cfg}{2022/03/01}{2.1.2}{tabular and tabularray configuration for bfhlayout}

\colorlet{BFH-table}{BFH-LightGreen!10}
\colorlet{BFH-tablehead}{BFH-LightGreen!50}

\def\BFHarraystretch{1.4}
\def\BFHarrayrulewidth{.08em}

\newcommand*{\setupBfhTabular}{%
	\rowcolors*{1}{BFH-table}{BFH-table}
	\arrayrulecolor{white}%
	\let\arraystretch\BFHarraystretch%
	\setlength{\arrayrulewidth}{\BFHarrayrulewidth}%
}

\newenvironment{bfhTabular}[2][c]{%
	\setupBfhTabular
	\begin{tabular}[#1]{#2}%
		\rowcolor{BFH-tablehead}%
	}{\end{tabular}}

\cs_if_exist:NTF \AddToHook {\AddToHook {begindocument} [BFH-tblr]}
	{\AtBeginDocument}
{%
	\cs_if_exist:NT \NewTblrEnviron {%
		\NewTblrEnviron{bfhTblr}%
		\SetTblrInner[bfhTblr]{%
			rows = {bg=BFH-table},
			hlines = {white,\BFHarrayrulewidth},
			stretch=1.1,
			row{1}	= {bg=BFH-tablehead}
		}
	}
}

\endinput