%% This is file `bfhcolors.sty' version 2.1.4 (2022/11/15),
%% it is part of
%% BFH-CI -- Corporate Design for Bern University of Applied Sciences
%% ----------------------------------------------------------------------------
%%
%%  Copyright (C) 2021–2022 by
%%      Marei Peischl <marei@peitex.de>
%%      Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% ============================================================================
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2008/05/04 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainers of this work are
%%   Marei Peischl <bfh-ci@peitex.de>
%%   Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% The development respository can be found at
%% https://gitlab.ti.bfh.ch/bfh-latex/bfh-ci/
%% Please use the issue tracker for feedback!
%%
%% ============================================================================
%%
\RequirePackage{expl3}
\ProvidesExplPackage{bfhcolors}{2022/03/01}{2.1.2}{color setup for BFH-CI, CI of Bern Universitry of Applied Sciences}

\RequirePackage{l3keys2e}
\tl_new:N \g__ptxcd_colormode_tl
\tl_gset:Nn \g__ptxcd_colormode_tl {4CU}

\keys_define:nn {ptxcd/colors} {
	coated .bool_gset:N = \g__ptxcd_color_coated_bool,
	coated .default:n = true,
	coated .initial:n = false,
	colormode .choice:,
	colormode / 4CU .code:n = {
			\keys_set:nn {ptxcd/colors} {coated=false}
			\PassOptionsToPackage{cmyk}{xcolor}
			\tl_gset:Nn \g__ptxcd_colormode_tl {4CU}
		},
	colormode / 4CC .code:n = {
			\keys_set:nn {ptxcd/colors} {coated=true}
			\PassOptionsToPackage{cmyk}{xcolor}
			\tl_gset:Nn \g__ptxcd_colormode_tl {4CC}
		},
	colormode / RGB .code:n = {
			\PassOptionsToPackage{rgb}{xcolor}
			\tl_gset:Nn \g__ptxcd_colormode_tl {RGB}
		},
	colormode / SW .code:n = {
			\PassOptionsToPackage{gray}{xcolor}
			\tl_gset:Nn \g__ptxcd_colormode_tl  {SW}
		},
	colormode / 1C .meta:n = {colormode=SW},
	RGB .meta:n = {colormode=RGB},
	rgb .meta:n = {colormode=RGB},
	cmyk .meta:n = {colormode = 4CU},
	gray .meta:n = {colormode= SW},
}
\ProcessKeysOptions{ptxcd/colors}

\RequirePackage{xcolor}

\bool_if:NTF \g__ptxcd_color_coated_bool
%coated definitions
{
	% Primary colors
	\definecolorset[named]{RGB/cmyk}{BFH-}{}{%
		Gray,105,125,145/0.55,0,0,0.6;%
		Orange,250,195,0/0.00,0.25,1,0}
	% Secondary colors
	\definecolorset[named]{RGB/cmyk}{BFH-}{}{%
		DarkGreen,85,100,85/.65,.3,.65,.3;%
		MediumGreen,105,150,115/.5,0,.4,.15;%
		LightGreen,140,175,130/.35,0,.35,.10;%
		DarkBlue,80,110,150/.7,.4,0,.2;%
		MediumBlue,105,155,190/.6,.25,0,0;%
		LightBlue,135,185,200/.40,.05,.05,.05;%
		DarkPurple,100,80,120/.5,.6,0,.35;%
		MediumPurple,130,90,125/.25,.55,0,.3;%
		LightPurple,160,135,170/.3,.4,0,0;%
		DarkOcher,120,100,80/.2,.3,.4,.35;%
		MediumOcher,150,130,95/.1,.2,.4,.25;%
		LightOcher,185,145,100/.1,.25,.40,.05;%
		DarkRed,180,20,40/.15,1,.8,.1;%
		MediumRed,210,90,80/.05,.75,.6,.1;%
		LightRed,255,145,125/.05,.50,.45,.1}%
}
%uncoated definitions
{
	% If the coated option was not passed, define the BFH colors in RGB and CMYK uncoated as the default.
	% Primary colors
	\definecolorset[named]{RGB/cmyk}{BFH-}{}{%
		Gray,105,125,145/0.55,0,0,.5;%
		Orange,250,195,0/0,0.2,1,0}
	% Secondary colors
	\definecolorset[named]{RGB/cmyk}{BFH-}{}{%
		DarkGreen,85,100,85/.6,.2,.6,.35;%
		MediumGreen,105,150,115/.55,0,.55,.10;%
		LightGreen,140,175,130/.4,0,.5,.05;%
		DarkBlue,80,110,150/.70,.40,0,.15;%
		MediumBlue,105,155,190/.55,.1,0,.1;%
		LightBlue,135,185,200/.4,0,.10,.05;%
		DarkPurple,100,80,120/.6,.8,.05,.15;%
		MediumPurple,130,90,125/.35,.7,.1,.15;%
		LightPurple,160,135,170/.3,.45,.05,0;%
		DarkOcher,120,100,80/.35,.45,.65,.25;%
		MediumOcher,150,130,95/.25,.3,.6,.15;%
		LightOcher,185,145,100/.1,.25,.6,.1;%
		DarkRed,180,20,40/.05,1,.8,.05;%
		MediumRed,210,90,80/0,.75,.6,.1;%
		LightRed,255,145,125/.05,.5,.45,.05}%
}

\endinput
