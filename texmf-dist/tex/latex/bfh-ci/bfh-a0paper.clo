%% This is file `bfh-a0paper.clo' version 2.1.4 (2022/11/15),
%% it is part of
%% BFH-CI -- Corporate Design for Bern University of Applied Sciences
%% ----------------------------------------------------------------------------
%%
%%  Copyright (C) 2021–2022 by
%%      Marei Peischl <marei@peitex.de>
%%      Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% ============================================================================
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2008/05/04 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainers of this work are
%%   Marei Peischl <bfh-ci@peitex.de>
%%   Andreas Habegger <andreas.habegger@bfh.ch>
%%
%% The development respository can be found at
%% https://gitlab.ti.bfh.ch/bfh-latex/bfh-ci/
%% Please use the issue tracker for feedback!
%%
%% ============================================================================
%%
\ProvidesExplFile{bfh-a0paper.clo}{2022/03/01}{2.1.2}{bfh-ci~size~adjustments~for~a0paper}
\cs_set:Nn \ptxcd_setup_title_sizes: {
	\cs_set:Nn \ptxcd_title_fontsize: {{126bp}{165bp}}
	\cs_set:Nn \ptxcd_subtitle_fontsize: {{38bp}{55bp}}
	\cs_set:Nn \ptxcd_department_fontsize: {{45bp}{55bp}}
	%	\cs_set:Nn \ptxcd_titleinfo_fontsize: {{28pt}{35pt}}
}
\cs_set:Nn \ptxcd_setup_base_sizes: {
	\dim_gset:Nn \g_ptxcd_margin_dim {39mm}
	\dim_gset:Nn \g_ptxcd_rule_height_dim {8.5mm}
	\dim_gset:Nn \g_ptxcd_rule_arc_dim {2.1mm}
	\tl_if_empty:NT \g_ptxcd_fontsize_tl {\tl_gset:Nn \g_ptxcd_fontsize_tl {30pt}}
}
\endinput