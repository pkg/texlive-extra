%%
%% This is file `wgexport.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% util/core.dtx  (with options: `exportcls')
%% 
%% Copyright (C) 2019 Christian Holm.
%% 
%% This file is NOT the source for wargame, because almost all comments
%% have been stripped from it.  It is NOT the preferred form of universal
%% for making modifications to it.
%% 
%% Therefore you can NOT redistribute and/or modify THIS file.  You can
%% however redistribute the complete source (wargame.dtx and wargame.ins)
%% and/or modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 2, or (at
%% your option) any later version.
%% 
%% The wargame package is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public
%% License along with this package; if not, write to the
%%   Free Software Foundation, Inc.,
%%   675 Mass Ave, Cambridge,
%%   MA 02139, USA.
%% 
%% See the file wargame.dtx for further comments and documentation.
%% 
\ProvidesClass{wgexport}
\PassOptionsToClass{multi=tikzpicture,varwidth=false}{standalone}
\DeclareOption{noterrainpic}{%
  \PassOptionsToPackage{\CurrentOption}{wargame}}
\DeclareOption{terrainpic}{%
  \PassOptionsToPackage{\CurrentOption}{wargame}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{standalone}}
\ProcessOptions\relax
\LoadClass{standalone}
\RequirePackage{wargame}
\begingroup
\catcode`\^^I=12
\def\@tabchar{^^I}
\catcode`<=1 \catcode`>=2
\catcode`{=12 \catcode`}=12
\gdef\@lbchar<{>
\gdef\@rbchar<}>
\endgroup
\newenvironment{imagelist}[1][\jobname]{%
  \newwrite\mk@out%
  \def\mk@i{}%
  \def\mk@w{\immediate\write\mk@out}%
  \immediate\openout\mk@out=#1.json
  \mk@w{[}
}{
  \mk@w{\mk@i \@lbchar "name":"End of list", "category": "<<eol>>",
      "subcategory": "" \@rbchar }
  \mk@w{]}
  \immediate\closeout\mk@out
}
\def\info{%
  \@ifstar{\@@info{,}}{\@@info{\@rbchar,}}}
\def\@@info#1#2#3#4{%
  \chit@dbg{2}{Making image `#2' of type `#3'/`#4' on page \thepage}%
  \mk@w{ \@lbchar}%
  \mk@w{ \space "name": "#2",}%
  \mk@w{ \space "category": "#3",}%
  \mk@w{ \space "subcategory": "#4", }%
  \mk@w{ \space "number": \thepage #1}%
  \let\oldmk@i\mk@i%
  \ifx#1,\relax\edef\mk@i{\mk@i\space\space}\fi}
\def\end@info{%
  \let\mk@i\oldmk@i%
  \mk@w{ \space \@rbchar,}}
\newcommand\chitimages[2][]{%
  \begingroup%
  \let\chit@report\do@chit@report%
  \let\natoapp@report\do@natoapp@report%
  \chit@dbg{2}{chits to make images of `#2'}%
  \foreach[count=\ti from 0] \t/\x in #2{%
    \ifx\t\empty\else% Ignore empty rows
      \chit@dbg{5}{^^JSubcategory: `\x' (default `#1')}
      \ifx\t\x\def\x{#1}\fi% Take sub-category or default
      \foreach \u/\m in \t{%
        \ifx\u\empty\else% Ignore empty cells
          \chit@dbg{2}{Next chit `\u' with possible multiplicity `\m'}%
          \ifx\m\@empty\def\m{1}\fi% If not multiplicity defined
          \ifx\u\m\def\m{1}\fi% If the same as unit
          \chit@dbg{2}{Next chit `\u' multiplicity `\m'}%
          %% We only make one copy of the chit, since we can duplicate
          %% it in VASSAL
          \info*{\u}{counter}{\x}
          \begin{tikzpicture}
            \chit[\u=\ti]%
          \end{tikzpicture}
          \end@info%
          %% \foreach \n in {1,...,\m}{% Make a number of copies
          %%   \ifx\u\chit@blank%
          %%     \chit@dbg{3}{Ignoring blank chit:\u}%
          %%   \else%
          %%     \info{\u}{counter}{#2}
          %%     \begin{tikzpicture}
          %%       \chit[\u=\ti](\c,\r)%
          %%     \end{tikzpicture}
          %%   \fi%
          %% }%
        \fi%
      }%
      \chit@dbg{2}{End of inner loop}%
    \fi%
  }%
  \chit@dbg{2}{End of outer loop}%
  \endgroup%
}
\newcommand\doublechitimages[2][]{%
  \begingroup%
  \let\chit@report\do@chit@report%
  \let\natoapp@report\do@natoapp@report%
  \foreach[count=\ti from 0] \t/\x in #2{%
    \ifx\t\empty\else% Ignore empty rows
      \chit@dbg{5}{^^JSubcategory: `\x' (default `#1')}
      \ifx\t\x\def\x{#1}\fi% Take sub-category or default
      \foreach \u/\m in \t{%
        \ifx\u\empty\else% Ignore empty cells
          \chit@dbg{2}{Next chit `\u' with possible multiplicity `\m'}%
          \ifx\m\@empty\def\m{1}\fi% If not multiplicity defined
          \ifx\u\m\def\m{1}\fi% If the same as unit
          \chit@dbg{2}{Next chit `\u' multiplicity `\m'}%
          %% Flipped chit
          \edef\s{\u\space flipped}%
          %% We only make one copy of the chit, since we can duplicate
          %% it in VASSAL
          \info*{\u}{counter}{\x}%
          \begin{tikzpicture}%
            \chit[\u=\ti]%
          \end{tikzpicture}%
          \end@info%
          \info*{\s}{counter}{\x}%
          \begin{tikzpicture}%
            \chit[\s=\ti]%
          \end{tikzpicture}%
          \end@info%
          %% \foreach \n in {1,...,\m}{% Make a number of copies
          %%   \ifx\u\chit@blank%
          %%     \chit@dbg{3}{Ignoring blank chit:\u}%
          %%   \else%
          %%     \info{\u}{counter}{#2}
          %%     \begin{tikzpicture}
          %%       \chit[\u=\ti](\c,\r)%
          %%     \end{tikzpicture}
          %%   \fi%
          %% }%
        \fi%
      }%
    \fi%
  }%
  \endgroup%
}
\def\pt@to@cm#1{\pgfmathparse{#1 * 0.0351367}}
\def\ptpoint@to@cm#1#2{%
  \pt@to@cm{#1}\edef\x{\pgfmathresult}%
  \pt@to@cm{#2}\edef\y{\pgfmathresult}}
\def\mk@get@anchor#1#2{%
  \pgfpointanchor{#1}{#2}%
  \pgfgetlastxy\tmp@x\tmp@y%
  \pt@to@cm{\tmp@x}\edef\tmp@x{\pgfmathresult}
  \pt@to@cm{\tmp@y}\edef\tmp@y{\pgfmathresult}
}
\def\mk@get@global@anchor#1#2{%
  \pgfpointanchor{#1}{#2}%
  \pgfgetlastxy\tmp@x\tmp@y%
  \pgfpointtransformed{\pgfpoint{\tmp@x}{\tmp@y}}
  \pgf@xa=\pgf@x
  \pgf@ya=\pgf@y
  \pt@to@cm{\the\pgf@xa}\edef\tmp@x{\pgfmathresult}
  \pt@to@cm{\the\pgf@ya}\edef\tmp@y{\pgfmathresult}
}
\def\get@bb#1{%
  % \pgfpointanchor{#1}{south west}%
  % \pgfgetlastxy\tmp@llx\tmp@lly%
  % \pgfpointanchor{#1}{north east}%
  % \pgfgetlastxy\tmp@urx\tmp@ury%
  % \pt@to@cm{\tmp@llx}\edef\llx{\pgfmathresult}
  % \pt@to@cm{\tmp@lly}\edef\lly{\pgfmathresult}
  % \pt@to@cm{\tmp@urx}\edef\urx{\pgfmathresult}
  % \pt@to@cm{\tmp@ury}\edef\ury{\pgfmathresult}
  \mk@get@anchor{#1}{south west}
  \edef\llx{\tmp@x}
  \edef\lly{\tmp@y}
  \mk@get@anchor{#1}{north east}
  \edef\urx{\tmp@x}
  \edef\ury{\tmp@y}
}
\def\mk@transform{%
  \pgfgettransformentries{\mxx}{\mxy}{\myx}{\myy}{\ptdx}{\ptdy}
  \pt@to@cm{\ptdx}\edef\dx{\pgfmathresult}
  \pt@to@cm{\ptdy}\edef\dy{\pgfmathresult}
  \mk@w{ \mk@i "xx": \mxx,}
  \mk@w{ \mk@i "xy": \mxy,}
  \mk@w{ \mk@i "yx": \myx,}
  \mk@w{ \mk@i "yy": \myy,}
  \mk@w{ \mk@i "dx": \dx,}
  \mk@w{ \mk@i "dy": \dy,}
}
\def\mk@bb#1{%
  \get@bb{#1}
  \mk@w{ \mk@i "lower left":  [\llx,\lly],}
  \mk@w{ \mk@i "upper right": [\urx,\ury],}
  \begingroup
   % \pgftransforminvert
   % \pgfpointanchor{#1}{south west}%
   % \pgfgetlastxy\tmp@llx\tmp@lly%
   % \pgfpointtransformed{\pgfpoint{\tmp@llx}{\tmp@lly}}
   % \pgf@xa=\pgf@x
   % \pgf@ya=\pgf@y
   % %
   % \pgfpointanchor{#1}{north east}%
   % \pgfgetlastxy\tmp@urx\tmp@ury%
   % \pgfgetlastxy\tmp@llx\tmp@lly%
   % \pgfpointtransformed{\pgfpoint{\tmp@urx}{\tmp@ury}}
   % \pgf@xb=\pgf@x
   % \pgf@yb=\pgf@y
   % \pt@to@cm{\the\pgf@xa}\edef\llx{\pgfmathresult}
   % \pt@to@cm{\the\pgf@ya}\edef\lly{\pgfmathresult}
   % \pt@to@cm{\the\pgf@xb}\edef\urx{\pgfmathresult}
   % \pt@to@cm{\the\pgf@yb}\edef\ury{\pgfmathresult}x
   \mk@get@global@anchor{#1}{south west}
   \mk@w{ \mk@i "global lower left":  [\tmp@x,\tmp@y],}
   \mk@get@global@anchor{#1}{north east}
   \mk@w{ \mk@i "global upper right": [\tmp@x,\tmp@y]}
 \endgroup
}
\def\mk@pos#1(#2){%
  \hex@dbg{10}{^^JMarking `#2' with `#1' - start}
  \coordinate[transform shape] (tmp) at (#2) {};
  \mk@get@anchor{tmp}{center}
  \hex@dbg{3}{^^JMarking `#2' with `#1' - `\tmp@x',\tmp@y'}
  \tikzset{zone point={#1}{\tmp@x}{\tmp@y}}
}
\pgfdeclaredecoration{record path construction}{initial}{%
  \state{initial}[width=0pt,next state=more]{
    \begingroup
      \pgf@decorate@inputsegment@first
      \ptpoint@to@cm{\the\pgf@x}{\the\pgf@y}
      \xdef\wg@path{[\x,\y]}
    \endgroup
  }%
  \state{more}[width=\pgfdecoratedinputsegmentremainingdistance]{%
    \begingroup
      \pgf@decorate@inputsegment@last
      \ptpoint@to@cm{\the\pgf@x}{\the\pgf@y}
      \xdef\wg@path{\wg@path,[\x,\y]}
    \endgroup
  }
  \state{final}{%
    \begingroup
      \pgf@decorate@inputsegment@last
      \ptpoint@to@cm{\the\pgf@x}{\the\pgf@y}
      \xdef\wg@path{\wg@path,[\x,\y]}
    \endgroup
    \mk@w{ \mk@i "zone path \wg@record@path@name": \@lbchar}
    \mk@w{ \mk@i\space "path": [\wg@path] \@rbchar,}
  }
}%
\newenvironment{boardimage}[3][board]{%
  \def\bd@n{#2}
  \newcount\mk@point
  \mk@point=0
  \let\oomk@i\mk@i%
  \let\markpos\mk@pos%
  \info{dummy}{<<dummy>>}{}%
  %\tikz{}%
  \tikz{\scoped[every hex/.try,every hex node/.try]{%
      \node[inner sep=0,outer sep=0]{%
        \global\let\mk@label\hex@label}}}%
  \info*{#2}{#1}{#3}%
  \mk@w{ \mk@i "zones": \@lbchar}%
  \edef\mk@i{\mk@i\space}
  %% Everything is made into centimeters
  \mk@w{ \mk@i "units": "cm",}
  \@ifundefined{mk@label}{}{\mk@w{ \mk@i "labels": "\mk@label",}}
  %% Write out coordinate options as "coords" object
  \mk@w{ \mk@i"coords": \@lbchar}%
  \mk@w{ \mk@i "row": \@lbchar}%
  \mk@w{ \mk@i\space "offset": \hex@coords@row@off,}%
  \mk@w{ \mk@i\space "factor": \hex@coords@row@fac \@rbchar,}%
  \mk@w{ \mk@i "column": \@lbchar}%
  \mk@w{ \mk@i\space "offset": \hex@coords@col@off,}%
  \mk@w{ \mk@i\space "factor": \hex@coords@col@fac,}%
  \mk@w{ \mk@i\space "top short": "\hex@top@short@col",}%
  \mk@w{ \mk@i\space "bottom short": "\hex@bot@short@col" \@rbchar}%
  \mk@w{ \mk@i\@rbchar,}%
  %%
  \let\oldbo@rdframe\bo@rdframe%
  \def\bo@rdframe[##1](##2)(##3){%
    \oldbo@rdframe[##1](##2)(##3)%
    \mk@w{ \mk@i"board frame": \@lbchar}
    \mk@w{ \mk@i\space "lower left": [\llx,\lly],}
    \mk@w{ \mk@i\space "upper right": [\urx,\ury],}
    \mk@w{ \mk@i\space "margin": \margin,}
    \mk@w{ \mk@i\space "width": \w,}
    \mk@w{ \mk@i\space "height": \h \@rbchar,}}%
  \tikzset{
    zoned/.code={% Apply to whole picture
      \pgfkeys{%
        % This needs to be done in the picture!
        /tikz/execute at end picture={%
          \mk@w{ \mk@i "zoned": \@lbchar}
          \mk@transform%
          \mk@bb{current bounding box}
          \mk@w{ \mk@i \@rbchar,}
        }
      }
    },
    zone scope/.code={%
      \mk@w{ \mk@i"zone scope ##1": \@lbchar}
      \let\omk@i\mk@i
      \edef\mk@i{\mk@i\space}
      \mk@transform%
      %\bd@w{ \@rbchar,}
      \gdef\wg@export@box{##1}%
      \pgfkeys{%
        /tikz/local bounding box=wg export box,
        /tikz/execute at end scope={
          \mk@bb{wg export box}
          \let\mk@i\omk@i
          \mk@w{ \mk@i\@rbchar,}},
      } % pgfkeys
    }, % zone scope
    zone point/.code n args={3}{
      \pgf@xa=##2 cm
      \pgf@ya=##3 cm
      \pgfpointtransformed{\pgfpoint{\pgf@xa}{\pgf@ya}}
      % \pgfpointtransformed{\pgfpoint{0pt}{0pt}}
      \pgf@xa=\pgf@x
      \pgf@ya=\pgf@y
      \pt@to@cm{\the\pgf@xa}\edef\px{\pgfmathresult}
      \pt@to@cm{\the\pgf@ya}\edef\py{\pgfmathresult}
      \advance\mk@point1
      \global\mk@point=\mk@point
      \mk@w{ \mk@i "point\the\mk@point": \@lbchar "name": "##1", "type": "point", "coords":  [\px,\py]
        \@rbchar, }
      %\message{^^JZone point \the\mk@point\space ##1: ##2,##3 -> \px,\py}
    },
    zone oob point/.code n args={3}{
      \pgf@xa=##2 cm
      \pgf@ya=##3 cm
      \advance\pgf@xa.1cm
      \advance\pgf@ya.1cm
      \pgfpointtransformed{\pgfpoint{\pgf@xa}{\pgf@ya}}
      % \pgfpointtransformed{\pgfpoint{0pt}{0pt}}
      \pgf@xa=\pgf@x
      \pgf@ya=\pgf@y
      \pt@to@cm{\the\pgf@xa}\edef\px{\pgfmathresult}
      \pt@to@cm{\the\pgf@ya}\edef\py{\pgfmathresult}
      \advance\mk@point1
      \global\mk@point=\mk@point
      \mk@w{ \mk@i "point\the\mk@point": \@lbchar "name": "##1", "type": "point", "coords":  [\px,\py]
        \@rbchar, }
      %\message{^^JZone point \the\mk@point\space ##1: ##2,##3 -> \px,\py}
    },
    zone global point/.code n args={3}{
      \advance\mk@point1
      \global\mk@point=\mk@point
      \mk@w{ \mk@i "point\the\mk@point": \@lbchar "name": "##1", "type": "point", "coords":  [\px,\py]
        \@rbchar, }
    },
    /pgf/decoration/record path name/.store in=\wg@record@path@name,
    zone path/.style={%
      postaction={decorate,decoration={
          record path construction,
          record path name=##1}}
    } % zone path
  }% tikzset
}
{%
  \mk@w{ \mk@i "name": "\bd@n" }%
  \let\mk@i\oomk@i%
  \mk@w{ \mk@i\@rbchar}%
  \end@info%
}
\tikzset{
  zone turn/.store in=\zone@turn,
  zone mult/.store in=\zone@mult
}
\def\do@chit@report{%
  \mk@w{ \mk@i "chit": \@lbchar}
  \@ifundefined{id}{}         {\mk@w{ \mk@i\space "id":      "\id", }}%
  \@ifundefined{chit@symbol}{}     {\mk@w{ \mk@i\space "symbol":  "true", }}%
  \@ifundefined{chit@full}{}       {\mk@w{ \mk@i\space "full":    "\chit@full", }}
  \@ifundefined{chit@factors}{}    {\mk@w{ \mk@i\space "factors": "\chit@factors", }}%
  \@ifundefined{chit@left}{}       {\mk@w{ \mk@i\space "left":    "\chit@left", }}%
  \@ifundefined{chit@right}{}      {\mk@w{ \mk@i\space "right":   "\chit@right", }}%
  \@ifundefined{chit@upper@left}{} {\mk@w{ \mk@i\space "upper left":  "\chit@upper@left", }}%
  \@ifundefined{chit@lower@left}{} {\mk@w{ \mk@i\space "lower left":  "\chit@lower@left", }}%
  \@ifundefined{chit@upper@right}{}{\mk@w{ \mk@i\space "upper right": "\chit@upper@right", }}%
  \@ifundefined{chit@lower@right}{}{\mk@w{ \mk@i\space "lower right": "\chit@lower@right}", }%
  \mk@w{ \mk@i\space "end": 0}
  \@ifundefined{chit@symbol}{
    \mk@w{ \mk@i \@rbchar }
  }{
    \mk@w{ \mk@i \@rbchar, }% NATOAPP6c will follow
  }%
}
\def\do@natoapp@report{%
  \mk@w{ \mk@i "natoapp6c": \@lbchar}
  \@ifundefined{id}{}{\mk@w{ \mk@i\space "id": "\id", }}
  \@ifundefined{natoapp@fac}{}{\mk@w{ \mk@i\space "faction": "\natoapp@fac", }}
  \@ifundefined{natoapp@cmd}{}{\mk@w{ \mk@i\space "command": "\natoapp@cmd", }}
  \@ifundefined{natoapp@ech}{}{\mk@w{ \mk@i\space "echelon": "\natoapp@ech", }}
  \@ifundefined{natoapp@main}{}{\mk@w{ \mk@i\space "main": "\natoapp@main", }}
  \@ifundefined{natoapp@left}{}{\mk@w{ \mk@i\space "left": "\natoapp@left", }}
  \@ifundefined{natoapp@right}{}{\mk@w{ \mk@i\space "right": "\natoapp@right", }}
  \@ifundefined{natoapp@upper}{}{\mk@w{ \mk@i\space "upper": "\natoapp@upper", }}
  \@ifundefined{natoapp@lower}{}{\mk@w{ \mk@i\space "lower": "\natoapp@lower", }}
  \@ifundefined{natoapp@below}{}{\mk@w{ \mk@i\space "below": "\natoapp@below", }}
  \mk@w{ \mk@i\space "end": 0}
  \mk@w{ \mk@i \@rbchar}
}
%% Local Variables:
%%   mode: LaTeX
%% End:
%%
%% End of file `wgexport.cls'.
