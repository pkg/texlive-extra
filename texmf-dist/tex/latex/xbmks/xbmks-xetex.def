%%
%% This is file `xbmks-xetex.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% xbmks.dtx  (with options: `copyright,xetex')
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% xbmks.sty package,                                    %%
%% Copyright (C) 2016--2018                              %%
%%   dpstory@uakron.edu                                  %%
%%                                                       %%
%% This program can redistributed and/or modified under  %%
%% the terms of the LaTeX Project Public License         %%
%% Distributed from CTAN archives in directory           %%
%% macros/latex/base/lppl.txt; either version 1.2 of the %%
%% License, or (at your option) any later version.       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\ReadBookmarks{%
  \pdf@ifdraftmode{}{%
    \begingroup\def\\{\@backslashchar\@backslashchar}% dps
      \def\calc@bm@number##1{%
        \@tempcnta=\check@bm@number{##1}\relax
        \advance\@tempcnta by 1 %
        \expandafter\edef\csname B_##1\endcsname{\the\@tempcnta}%
      }%
      \def\do##1{%
        \ifnum\catcode`##1=\active
          \@makeother##1%
        \else
          \ifnum\catcode`##1=6 %
            \@makeother##1%
          \fi
        \fi
      }%
      \dospecials
      \Hy@safe@activestrue
      \escapechar=`\\%
      \ifx\WriteBookmarks\relax
        \global\let\WriteBookmarks\relax
      \fi
      \begingroup
        \def\WriteBookmarks{0}%
        \count\z@=0\relax
        \edef\xbmk@J{\jobname}%
        \@whilenum \count\z@<\xbmk@cnt\relax\do{%
          \advance\count\z@ by 1 %
          \edef\xbmk@thisdoc{xbmk@doc\the\count\z@}%
          \edef\xbmk@filename{\@nameuse{\xbmk@thisdoc}}%
          \@onelevel@sanitize\xbmk@filename
          \bgroup
          \def\@@BOOKMARK[##1][##2]##3##4##5{%
            \calc@bm@number{##5}%
          }%
          \let\nextAction\@gobbletwo
          \InputIfFileExists{\xbmk@filename.out}{}{\x@outWarningMsg}%
          \def\@@BOOKMARK[##1][##2]##3##4##5{%
            \def\Hy@temp{##4}%
            \Hy@pstringdef\Hy@pstringName{\HyperDestNameFilter{##3}}%
            \x@rollCFIntoActionBmrk{##3}%
            \@pdfm@mark{%
              outline \ifHy@DvipdfmxOutlineOpen
                [\ifnum##21>\z@\else-\fi] \fi
              ##1<<%
              /Title(\expandafter\strip@prefix\meaning\Hy@temp)%
              \xbmkcsarg\ifx{X_##3}\relax
                \ifx\xbmk@J\xbmk@filename
                  /A<<%
                    /S/GoTo%
                    /D(\Hy@pstringName)%
                  >>%
                \else
                  /A<<%
                    /S/GoToR/F(\xbmk@filename.pdf)%
                    /D(\Hy@pstringName)%
                  >>%
                \fi
              \else
                /A<<\@nameuse{X_##3}>>
              \fi
              \thisCol\thisF
              >>%
            }% \@pdfm@mark
            \let\thisC\@empty\let\thisF\@empty
          }% bookmark
          \let\nextAction\nextAction@i
          \InputIfFileExists{\xbmk@filename.out}{}{}%
          \egroup
        }% \@whilenum
      \endgroup
    \endgroup
  }%
  \ifx\WriteBookmarks\relax
  \else
    \if@filesw
      \newwrite\@outlinefile
      \Hy@OutlineRerunCheck
     \immediate\openout\@outlinefile=\jobname.out\relax
      \ifHy@typexml
        \immediate\write\@outlinefile{<relaxxml>\relax}%
      \fi
    \fi
  \fi
}
\endinput
%%
%% End of file `xbmks-xetex.def'.
