%%
%% This is file `xbmks-pdftex.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% xbmks.dtx  (with options: `copyright,pdftex')
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% xbmks.sty package,                                    %%
%% Copyright (C) 2016--2018                              %%
%%   dpstory@uakron.edu                                  %%
%%                                                       %%
%% This program can redistributed and/or modified under  %%
%% the terms of the LaTeX Project Public License         %%
%% Distributed from CTAN archives in directory           %%
%% macros/latex/base/lppl.txt; either version 1.2 of the %%
%% License, or (at your option) any later version.       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\ReadBookmarks{%
  \pdf@ifdraftmode{}{%
    \begingroup\def\\{\@backslashchar\@backslashchar}% dps
    \def\calc@bm@number##1{%
      \@tempcnta=\check@bm@number{##1}\relax
      \advance\@tempcnta by 1 %
      \expandafter\edef\csname B_##1\endcsname{\the\@tempcnta}%
    }%
    \def\Hy@OutlineName##1##2##3##4{\def\@rgi{##1}%
      \expandafter\pdfoutline\ifx\@rgi\@empty\else
      attr {##1} \fi
      user {##2} count##3{##4}%
    %  goto name{#2} count#3{#4}%
    }%
    \def\do##1{%
      \ifnum\catcode`##1=\active
        \@makeother##1%
      \else
        \ifnum\catcode`##1=6 %
          \@makeother##1%
        \fi
      \fi
    }%
    \dospecials
    \Hy@safe@activestrue
    \escapechar=`\\%
    \ifx\WriteBookmarks\relax
      \global\let\WriteBookmarks\relax
    \fi
    \begingroup
    \def\WriteBookmarks{0}%
    \count\z@=0\relax
    \edef\xbmk@J{\jobname}%
    \@whilenum \count\z@<\xbmk@cnt\relax\do{%
      \advance\count\z@ by 1 %
      \edef\xbmk@thisdoc{xbmk@doc\the\count\z@}%
      \edef\xbmk@filename{\@nameuse{\xbmk@thisdoc}}%
      \@onelevel@sanitize\xbmk@filename
      \bgroup
      \gdef\@@BOOKMARK[##1][##2]##3##4##5{%
        \calc@bm@number{##5}%
      }%
      \let\nextAction\@gobbletwo
      \InputIfFileExists{\xbmk@filename.out}{}{\x@outWarningMsg}%
      \gdef\@@BOOKMARK[##1][##2]##3##4##5{%
        \def\Hy@temp{##4}%
        \Hy@pstringdef\Hy@pstringName{\HyperDestNameFilter{##3}}%
        \xbmkcsarg\ifx{X_##3}\relax
          \ifx\xbmk@J\xbmk@filename
            \Hy@OutlineName{\xbmks@intC\xbmk@intF}{<</S/GoTo%
              /D(\Hy@pstringName)>>}{%
              ##2\check@bm@number{\Hy@pstringName}%
            }{%
              \expandafter\strip@prefix\meaning\Hy@temp
            }%
          \else
            \Hy@OutlineName{\xbmks@extC\xbmk@extF}{<</S/GoToR%
              /F(\xbmk@filename.pdf)/D(\Hy@pstringName)>>}{%
              ##2\check@bm@number{##3}%
            }{%
              \expandafter\strip@prefix\meaning\Hy@temp
            }%
          \fi
        \else
          \x@rollCFIntoActionBmrk{##3}%
          \Hy@OutlineName{\thisCol\thisF}
          {<<\@nameuse{X_##3}>>}{%
              ##2\check@bm@number{\Hy@pstringName}%
          }{%
            \expandafter\strip@prefix\meaning\Hy@temp
          }%
        \fi
      }% bookmark
      \let\nextAction\nextAction@i
      \InputIfFileExists{\xbmk@filename.out}{}{}%
      \egroup
    } % \@whilenum
    \endgroup
  }%
  \ifx\WriteBookmarks\relax
  \else
    \if@filesw
      \newwrite\@outlinefile
      \Hy@OutlineRerunCheck
      \immediate\openout\@outlinefile=\jobname.out\relax
      \ifHy@typexml
        \immediate\write\@outlinefile{<relaxxml>\relax}%
      \fi
    \fi
  \fi
}
\endinput
%%
%% End of file `xbmks-pdftex.def'.
