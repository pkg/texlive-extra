%% Generated by autoinst on 2020/02/15
%%
\ProvidesFile{T1ETbb-LF.fd}
    [2020/02/15 (autoinst)  Font definitions for T1/ETbb-LF.]

\expandafter\ifx\csname ETbb@@swashQ\endcsname\relax%
	\global\let\ETbb@@swashQ\@empty
\fi

\ifcsname s@fct@alias\endcsname\else
\gdef\s@fct@alias{\sub@sfcnt\@font@aliasinfo}
\gdef\@font@aliasinfo#1{%
    \@font@info{Font\space shape\space `\curr@fontshape'\space will
        \space be\space aliased\MessageBreak to\space `\mandatory@arg'}%
}
\fi

\edef\ETbb@@S{} % do not use fancy sharpS
\expandafter\ifx\csname @ETbb@sharpSfalse\endcsname\relax
\csname iftrue\endcsname
\else
	\if@ETbb@sharpS\edef\ETbb@@S{1}\fi % do use fancy sharpS
\fi

\expandafter\ifx\csname ETbb@scale\endcsname\relax
    \let\ETbb@@scale\@empty
\else
    \edef\ETbb@@scale{s*[\csname ETbb@scale\endcsname]}%
\fi

\DeclareFontFamily{T1}{ETbb-LF}{}


%   ----  bold  ----

\DeclareFontShape{T1}{ETbb-LF}{bold}{nw}{
      <-> \ETbb@@scale ETbb-Bold-lf-swash-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{sw}{
      <-> \ETbb@@scale ETbb-BoldItalic-lf-swash-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{it}{
      <-> \ETbb@@scale ETbb\ETbb@@S-BoldItalic-lf\ETbb@@swashQ-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{scit}{
      <-> \ETbb@@scale ETbb\ETbb@@S-BoldItalic-lf-sc-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{n}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Bold-lf\ETbb@@swashQ-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{sc}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Bold-lf-sc-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{scsl}{
      <-> ssub * ETbb-LF/bold/scit
}{}

\DeclareFontShape{T1}{ETbb-LF}{bold}{sl}{
      <-> ssub * ETbb-LF/bold/it
}{}


%   ----  regular  ----

\DeclareFontShape{T1}{ETbb-LF}{regular}{nw}{
      <-> \ETbb@@scale ETbb-Regular-lf-swash-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{sw}{
      <-> \ETbb@@scale ETbb-Italic-lf-swash-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{it}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Italic-lf\ETbb@@swashQ-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{sc}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Regular-lf-sc-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{scit}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Italic-lf-sc-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{n}{
      <-> \ETbb@@scale ETbb\ETbb@@S-Regular-lf\ETbb@@swashQ-t1
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{scsl}{
      <-> ssub * ETbb-LF/regular/scit
}{}

\DeclareFontShape{T1}{ETbb-LF}{regular}{sl}{
      <-> ssub * ETbb-LF/regular/it
}{}

%
%  Extra 'alias' rules to map the standard NFSS codes to our fancy names
%

%   m --> regular

\DeclareFontShape{T1}{ETbb-LF}{m}{scit}{
      <-> alias * ETbb-LF/regular/scit
}{}

\DeclareFontShape{T1}{ETbb-LF}{m}{n}{
      <-> alias * ETbb-LF/regular/n
}{}

\DeclareFontShape{T1}{ETbb-LF}{m}{sl}{
      <-> alias * ETbb-LF/regular/sl
}{}

\DeclareFontShape{T1}{ETbb-LF}{m}{sc}{
      <-> alias * ETbb-LF/regular/sc
}{}

\DeclareFontShape{T1}{ETbb-LF}{m}{it}{
      <-> alias * ETbb-LF/regular/it
}{}

\DeclareFontShape{T1}{ETbb-LF}{m}{scsl}{
      <-> alias * ETbb-LF/regular/scsl
}{}


%   b --> bold

\DeclareFontShape{T1}{ETbb-LF}{b}{sw}{
      <-> alias * ETbb-LF/bold/sw
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{nw}{
      <-> alias * ETbb-LF/bold/nw
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{it}{
      <-> alias * ETbb-LF/bold/it
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{scsl}{
      <-> alias * ETbb-LF/bold/scsl
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{sl}{
      <-> alias * ETbb-LF/bold/sl
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{scit}{
      <-> alias * ETbb-LF/bold/scit
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{n}{
      <-> alias * ETbb-LF/bold/n
}{}

\DeclareFontShape{T1}{ETbb-LF}{b}{sc}{
      <-> alias * ETbb-LF/bold/sc
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{it}{
      <-> ssub * ETbb-LF/b/it
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{scsl}{
      <-> ssub * ETbb-LF/b/scsl
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{scit}{
      <-> ssub * ETbb-LF/b/scit
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{n}{
      <-> ssub * ETbb-LF/b/n
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{sl}{
      <-> ssub * ETbb-LF/b/sl
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{sc}{
      <-> ssub * ETbb-LF/b/sc
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{nw}{
      <-> ssub * ETbb-LF/b/nw
}{}

\DeclareFontShape{T1}{ETbb-LF}{bx}{sw}{
      <-> ssub * ETbb-LF/b/sw
}{}

\endinput
