%%
%% This is file `datetime2-ukrainian-ascii.ldf',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% datetime2-ukrainian.dtx  (with options: `datetime2-ukrainian-ascii.ldf,package')
%% ----------------------------------------------------------------
%% 
%% This material is subject to the LaTeX Project Public License.
%% See http://www.ctan.org/license/lppl1.3 for the details of that
%% license.
%% 
%% ----------------------------------------------------------------
%% 

\ProvidesDateTimeModule{ukrainian-ascii}[2017/06/13 1.2]
\newcommand*{\DTMukrainianordinal}[1]{%
  \number#1
}
\newcommand*{\DTMukrainianyear}[1]{%
  \number#1
  \DTMtexorpdfstring{\protect~}{\space}\protect\cyrr.%
}
\newcommand*{\DTMukrainiannominativemonthname}[1]{%
  \ifcase#1
  \or
   \protect\cyrs\protect\cyrii\protect\cyrch\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrl\protect\cyryu\protect\cyrt\protect\cyri\protect\cyrishrt
  \or
   \protect\cyrb\protect\cyre\protect\cyrr\protect\cyre\protect\cyrz
    \protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrk\protect\cyrv\protect\cyrii\protect\cyrt\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrt\protect\cyrr\protect\cyra\protect\cyrv\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrch\protect\cyre\protect\cyrr\protect\cyrv\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrl\protect\cyri\protect\cyrp\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrs\protect\cyre\protect\cyrr\protect\cyrp\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrv\protect\cyre\protect\cyrr\protect\cyre\protect\cyrs
    \protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrzh\protect\cyro\protect\cyrv\protect\cyrt\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\cyrl\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp\protect\cyra\protect\cyrd
  \or
   \protect\cyrg\protect\cyrr\protect\cyru\protect\cyrd\protect\protect\cyre\protect\cyrn\protect\cyrsftsn
  \fi
}
\newcommand*{\DTMukrainiannominativeMonthname}[1]{%
  \ifcase#1
  \or
   \protect\CYRS\protect\cyrii\protect\cyrch\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRL\protect\cyryu\protect\cyrt\protect\cyri\protect\cyrishrt
  \or
   \protect\CYRB\protect\cyre\protect\cyrr\protect\cyre\protect\cyrz
    \protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRK\protect\cyrv\protect\cyrii\protect\cyrt\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRT\protect\cyrr\protect\cyra\protect\cyrv\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRCH\protect\cyre\protect\cyrr\protect\cyrv\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRL\protect\cyri\protect\cyrp\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRS\protect\cyre\protect\cyrr\protect\cyrp\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRV\protect\cyre\protect\cyrr\protect\cyre\protect\cyrs
    \protect\CYRE\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRZH\protect\cyro\protect\cyrv\protect\cyrt\protect\cyre\protect\cyrn\protect\cyrsftsn
  \or
   \protect\CYRL\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp\protect\cyra\protect\cyrd
  \or
   \protect\CYRG\protect\cyrr\protect\cyru\protect\cyrd\protect\protect\cyre\protect\cyrn\protect\cyrsftsn
  \fi
}
\newcommand*{\DTMukrainiangenitivemonthname}[1]{%
  \ifcase#1
  \or
   \protect\cyrs\protect\cyrii\protect\cyrch\protect\cyrn\protect\cyrya
  \or
   \protect\cyrl\protect\cyryu\protect\cyrt\protect\cyro\protect\cyrg
    \protect\cyro
  \or
   \protect\cyrb\protect\cyre\protect\cyrr\protect\cyre\protect\cyrz
    \protect\cyrn\protect\cyrya
  \or
   \protect\cyrk\protect\cyrv\protect\cyrii\protect\cyrt\protect\cyrn
    \protect\cyrya
  \or
   \protect\cyrt\protect\cyrr\protect\cyra\protect\cyrv\protect\cyrn
    \protect\cyrya
  \or
   \protect\cyrch\protect\cyre\protect\cyrr\protect\cyrv\protect\cyrn
    \protect\cyrya
  \or
   \protect\cyrl\protect\cyri\protect\cyrp\protect\cyrn\protect\cyrya
  \or
   \protect\cyrs\protect\cyre\protect\cyrr\protect\cyrp\protect\cyrn
    \protect\cyrya
  \or
   \protect\cyrv\protect\cyre\protect\cyrr\protect\cyre\protect\cyrs
    \protect\cyrn\protect\cyrya
  \or
   \protect\cyrzh\protect\cyro\protect\cyrv\protect\cyrt\protect\cyrn
    \protect\cyrya
  \or
   \protect\cyrl\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp\protect\cyra\protect\cyrd\protect\cyra
  \or
   \protect\cyrg\protect\cyrr\protect\cyru\protect\cyrd\protect\cyrn
    \protect\cyrya
  \fi
}
\newcommand*{\DTMukrainiangenitiveMonthname}[1]{%
  \ifcase#1
  \or
   \protect\CYRS\protect\cyrii\protect\cyrch\protect\cyrn\protect\cyrya
  \or
   \protect\CYRL\protect\cyryu\protect\cyrt\protect\cyro\protect\cyrg
    \protect\cyro
  \or
   \protect\CYRB\protect\cyre\protect\cyrr\protect\cyre\protect\cyrz
    \protect\cyrn\protect\cyrya
  \or
   \protect\CYRK\protect\cyrv\protect\cyrii\protect\cyrt\protect\cyrn
    \protect\cyrya
  \or
   \protect\CYRT\protect\cyrr\protect\cyra\protect\cyrv\protect\cyrn
    \protect\cyrya
  \or
   \protect\CYRCH\protect\cyre\protect\cyrr\protect\cyrv\protect\cyrn
    \protect\cyrya
  \or
   \protect\CYRL\protect\cyri\protect\cyrp\protect\cyrn\protect\cyrya
  \or
   \protect\CYRS\protect\cyre\protect\cyrr\protect\cyrp\protect\cyrn
    \protect\cyrya
  \or
   \protect\CYRV\protect\cyre\protect\cyrr\protect\cyre\protect\cyrs
    \protect\cyrn\protect\cyrya
  \or
   \protect\CYRZH\protect\cyro\protect\cyrv\protect\cyrt\protect\cyrn
    \protect\cyrya
  \or
   \protect\CYRL\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp\protect\cyra\protect\cyrd\protect\cyra
  \or
   \protect\CYRG\protect\cyrr\protect\cyru\protect\cyrd\protect\cyrn
    \protect\cyrya
  \fi
}
\newcommand*{\DTMukrainianmonthname}{\DTMukrainiangenitivemonthname}% default
\newcommand*{\DTMukrainianMonthname}{\DTMukrainiangenitiveMonthname}% default
\newcommand*{\DTMukrainianshortmonthname}[1]{%
  \ifcase#1
  \or
   \protect\cyrs\protect\cyrii\protect\cyrch.
  \or
   \protect\cyrl\protect\cyryu\protect\cyrt.
  \or
   \protect\cyrb\protect\cyre\protect\cyrr.
  \or
   \protect\cyrk\protect\cyrv\protect\cyrii\protect\cyrt.
  \or
   \protect\cyrt\protect\cyrr\protect\cyra\protect\cyrv.
  \or
   \protect\cyrch\protect\cyre\protect\cyrr\protect\cyrv.
  \or
   \protect\cyrl\protect\cyri\protect\cyrp.
  \or
   \protect\cyrs\protect\cyre\protect\cyrr\protect\cyrp.
  \or
   \protect\cyrv\protect\cyre\protect\cyrr.
  \or
   \protect\cyrzh\protect\cyro\protect\cyrv\protect\cyrt.
  \or
   \protect\cyrl\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp.
  \or
   \protect\cyrg\protect\cyrr\protect\cyru\protect\cyrd.
  \fi
}
\newcommand*{\DTMukrainianshortMonthname}[1]{%
  \ifcase#1
  \or
   \protect\CYRS\protect\cyrii\protect\cyrch.
  \or
   \protect\CYRL\protect\cyryu\protect\cyrt.
  \or
   \protect\CYRB\protect\cyre\protect\cyrr.
  \or
   \protect\CYRK\protect\cyrv\protect\cyrii\protect\cyrt.
  \or
   \protect\CYRT\protect\cyrr\protect\cyra\protect\cyrv.
  \or
   \protect\CYRCH\protect\cyre\protect\cyrr\protect\cyrv.
  \or
   \protect\CYRL\protect\cyri\protect\cyrp.
  \or
   \protect\CYRS\protect\cyre\protect\cyrr\protect\cyrp.
  \or
   \protect\CYRV\protect\cyre\protect\cyrr.
  \or
   \protect\CYRZH\protect\cyro\protect\cyrv\protect\cyrt.
  \or
   \protect\CYRL\protect\cyri\protect\cyrs\protect\cyrt\protect\cyro
    \protect\cyrp.
  \or
   \protect\CYRG\protect\cyrr\protect\cyru\protect\cyrd.
  \fi
}
\newcommand*{\DTMukrainianweekdayname}[1]{%
  \ifcase#1
  \protect\cyrp\protect\cyro\protect\cyrn\protect\cyre\protect\cyrd\protect\cyrii\protect\cyrl\protect\cyro\protect\cyrk%
  \or
  \protect\cyrv\protect\cyrii\protect\cyrv\protect\cyrt\protect\cyro\protect\cyrr\protect\cyro\protect\cyrk%
  \or
  \protect\cyrs\protect\cyre\protect\cyrr\protect\cyre\protect\cyrd\protect\cyra%
  \or
  \protect\cyrch\protect\cyre\protect\cyrt\protect\cyrv\protect\cyre\protect\cyrr%
  \or
  \protect\cyrp'\protect\cyrya\protect\cyrt\protect\cyrn\protect\cyri\protect\cyrc\protect\cyrya%
  \or
  \protect\cyrs\protect\cyru\protect\cyrb\protect\cyro\protect\cyrt\protect\cyra%
  \or
  \protect\cyrn\protect\cyre\protect\cyrd\protect\cyrii\protect\cyrl\protect\cyrya%
  \fi
}
\newcommand*{\DTMukrainianWeekdayname}[1]{%
  \ifcase#1
  \protect\CYRP\protect\cyro\protect\cyrn\protect\cyre\protect\cyrd\protect\cyrii\protect\cyrl\protect\cyro\protect\cyrk%
  \or
  \protect\CYRV\protect\cyrii\protect\cyrv\protect\cyrt\protect\cyro\protect\cyrr\protect\cyro\protect\cyrk%
  \or
  \protect\CYRS\protect\cyre\protect\cyrr\protect\cyre\protect\cyrd\protect\cyra%
  \or
  \protect\CYRCH\protect\cyre\protect\cyrt\protect\cyrv\protect\cyre\protect\cyrr%
  \or
  \protect\CYRP'\protect\cyrya\protect\cyrt\protect\cyrn\protect\cyri\protect\cyrc\protect\cyrya%
  \or
  \protect\CYRS\protect\cyru\protect\cyrb\protect\cyro\protect\cyrt\protect\cyra%
  \or
  \protect\CYRN\protect\cyre\protect\cyrd\protect\cyrii\protect\cyrl\protect\cyrya%
  \fi
}
\newcommand*{\DTMukrainianshortweekdayname}[1]{%
  \ifcase#1
  \protect\cyrp\protect\cyrn.%
  \or
  \protect\cyrv\protect\cyrt.%
  \or
  \protect\cyrs\protect\cyrr.%
  \or
  \protect\cyrch\protect\cyrt%
  \or
  \protect\cyrp\protect\cyrt.%
  \or
  \protect\cyrs\protect\cyrb.%
  \or
  \protect\cyrn\protect\cyrd.%
  \fi
}
\newcommand*{\DTMukrainianshortWeekdayname}[1]{%
  \ifcase#1
  \protect\CYRP\protect\cyrn.%
  \or
  \protect\CYRV\protect\cyrt.%
  \or
  \protect\CYRS\protect\cyrr.%
  \or
  \protect\CYRCH\protect\cyrt.%
  \or
  \protect\CYRP\protect\cyrt.%
  \or
  \protect\CYRS\protect\cyrb.%
  \or
  \protect\CYRN\protect\cyrd.%
  \fi
}
%% 
%% ----------------------------------------------------------------
%%  datetime2-ukrainian.dtx
%%  Copyright 2015 Nicola Talbot
%%            2017 Sergiy M. Ponomarenko
%% 
%%  This work may be distributed and/or modified under the
%%  conditions of the LaTeX Project Public License, either version 1.3
%%  of this license of (at your option) any later version.
%%  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%%  and version 1.3 or later is part of all distributions of LaTeX
%%  version 2005/12/01 or later.
%% 
%%  This work has the LPPL maintenance status `inactive'.
%% 
%%  This work consists of the files datetime2-ukrainian.dtx
%%                                  datetime2-ukrainian.ins
%%                                  datetime2-ukrainian-utf8.ldf,
%%                                  datetime2-ukrainian-ascii.ldf
%%                                  datetime2-ukrainian.ldf.
%% ----------------------------------------------------------------
%% 
%%
%% End of file `datetime2-ukrainian-ascii.ldf'.
