%% Generated by autoinst on 2022/11/16
%%
\ProvidesFile{OT1Chivo-Sup.fd}
    [2022/11/16 (autoinst)  Font definitions for OT1/Chivo-Sup.]

\ifcsname s@fct@alias\endcsname\else
\gdef\s@fct@alias{\sub@sfcnt\@font@aliasinfo}
\gdef\@font@aliasinfo#1{%
    \@font@info{Font\space shape\space `\curr@fontshape'\space will
        \space be\space aliased\MessageBreak to\space `\mandatory@arg'}%
}
\fi

\expandafter\ifx\csname Chivo@scale\endcsname\relax
    \let\Chivo@@scale\@empty
\else
    \edef\Chivo@@scale{s*[\csname Chivo@scale\endcsname]}%
\fi

\DeclareFontFamily{OT1}{Chivo-Sup}{}


%   ----  eb = extrabold  ----

\DeclareFontShape{OT1}{Chivo-Sup}{eb}{n}{
      <-> \Chivo@@scale Chivo-ExtraBold-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extrabold}{n}{
      <-> alias * Chivo-Sup/eb/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{eb}{it}{
      <-> \Chivo@@scale Chivo-ExtraBoldItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extrabold}{it}{
      <-> alias * Chivo-Sup/eb/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{eb}{sl}{
    <-> ssub * Chivo-Sup/eb/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extrabold}{sl}{
    <-> ssub * Chivo-Sup/extrabold/it
}{}


%   ----  l = light  ----

\DeclareFontShape{OT1}{Chivo-Sup}{l}{it}{
      <-> \Chivo@@scale Chivo-LightItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{light}{it}{
      <-> alias * Chivo-Sup/l/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{l}{n}{
      <-> \Chivo@@scale Chivo-Light-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{light}{n}{
      <-> alias * Chivo-Sup/l/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{l}{sl}{
    <-> ssub * Chivo-Sup/l/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{light}{sl}{
    <-> ssub * Chivo-Sup/light/it
}{}


%   ----  sb = semibold  ----

\DeclareFontShape{OT1}{Chivo-Sup}{sb}{it}{
      <-> \Chivo@@scale Chivo-SemiBoldItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{semibold}{it}{
      <-> alias * Chivo-Sup/sb/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{sb}{n}{
      <-> \Chivo@@scale Chivo-SemiBold-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{semibold}{n}{
      <-> alias * Chivo-Sup/sb/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{sb}{sl}{
    <-> ssub * Chivo-Sup/sb/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{semibold}{sl}{
    <-> ssub * Chivo-Sup/semibold/it
}{}


%   ----  ub = black  ----

\DeclareFontShape{OT1}{Chivo-Sup}{ub}{n}{
      <-> \Chivo@@scale Chivo-Black-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{black}{n}{
      <-> alias * Chivo-Sup/ub/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{ub}{it}{
      <-> \Chivo@@scale Chivo-BlackItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{black}{it}{
      <-> alias * Chivo-Sup/ub/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{ub}{sl}{
    <-> ssub * Chivo-Sup/ub/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{black}{sl}{
    <-> ssub * Chivo-Sup/black/it
}{}


%   ----  ul = thin  ----

\DeclareFontShape{OT1}{Chivo-Sup}{ul}{it}{
      <-> \Chivo@@scale Chivo-ThinItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{thin}{it}{
      <-> alias * Chivo-Sup/ul/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{ul}{n}{
      <-> \Chivo@@scale Chivo-Thin-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{thin}{n}{
      <-> alias * Chivo-Sup/ul/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{ul}{sl}{
    <-> ssub * Chivo-Sup/ul/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{thin}{sl}{
    <-> ssub * Chivo-Sup/thin/it
}{}


%   ----  m = regular  ----

\DeclareFontShape{OT1}{Chivo-Sup}{m}{n}{
      <-> \Chivo@@scale Chivo-Regular-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{regular}{n}{
      <-> alias * Chivo-Sup/m/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{m}{it}{
      <-> \Chivo@@scale Chivo-Italic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{regular}{it}{
      <-> alias * Chivo-Sup/m/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{m}{sl}{
    <-> ssub * Chivo-Sup/m/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{regular}{sl}{
    <-> ssub * Chivo-Sup/regular/it
}{}


%   ----  b = bold  ----

\DeclareFontShape{OT1}{Chivo-Sup}{b}{n}{
      <-> \Chivo@@scale Chivo-Bold-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{bold}{n}{
      <-> alias * Chivo-Sup/b/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{b}{it}{
      <-> \Chivo@@scale Chivo-BoldItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{bold}{it}{
      <-> alias * Chivo-Sup/b/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{b}{sl}{
    <-> ssub * Chivo-Sup/b/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{bold}{sl}{
    <-> ssub * Chivo-Sup/bold/it
}{}


%   ----  medium  ----

\DeclareFontShape{OT1}{Chivo-Sup}{medium}{n}{
      <-> \Chivo@@scale Chivo-Medium-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{medium}{it}{
      <-> \Chivo@@scale Chivo-MediumItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{medium}{sl}{
    <-> ssub * Chivo-Sup/medium/it
}{}


%   ----  el = extralight  ----

\DeclareFontShape{OT1}{Chivo-Sup}{el}{it}{
      <-> \Chivo@@scale Chivo-ExtraLightItalic-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extralight}{it}{
      <-> alias * Chivo-Sup/el/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{el}{n}{
      <-> \Chivo@@scale Chivo-ExtraLight-sup-ot1
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extralight}{n}{
      <-> alias * Chivo-Sup/el/n
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{el}{sl}{
    <-> ssub * Chivo-Sup/el/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{extralight}{sl}{
    <-> ssub * Chivo-Sup/extralight/it
}{}


%   ----  Extra 'ssub' rules to map 'bx' to 'b'  ----

\DeclareFontShape{OT1}{Chivo-Sup}{bx}{it}{
      <-> ssub * Chivo-Sup/b/it
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{bx}{sl}{
      <-> ssub * Chivo-Sup/b/sl
}{}

\DeclareFontShape{OT1}{Chivo-Sup}{bx}{n}{
      <-> ssub * Chivo-Sup/b/n
}{}

\endinput
