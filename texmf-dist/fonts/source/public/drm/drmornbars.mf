% +AMDG  This document was begun on 21 June 11EX, the feast
% of St. William the Abbot, and it is humbly dedicated to
% him and to the Immaculate Heart of Mary for their prayers,
% and to the Sacred Heart of Jesus for His mercy.

beginchar("a",ornshort#+2ss#,ornheight#+2ss#,0);

% draw the border of the ornament
pickup borderpen;
top lft z20 = (ss,h-ss); top rt z21 = (w-ss,h-ss);
bot rt z22 = (w-ss,ss); bot lft z23 = (ss,ss);
draw z20--z21--z22--z23--cycle;

% path for the ellipse in the center construct
z0 = (-centerwid/2,0);
z1 = (centerwid/2,0);
z2 = z0 shifted (0,centerht/2);
z3 = z0 shifted (0,-centerht/2);
z4 = z1 shifted (0,centerht/2);
z5 = z1 shifted (0,-centerht/2);
z6 = z0 shifted (-ccurvrad,0);
z7 = z1 shifted (ccurvrad,0);
path centell; centell = z2..z6..z3--z5..z7..z4--cycle;

% points for placement of center construct
z8 = (w/2,h/2);
z9 = (w/2,h/2+centerht);
z10 = (w/2,h/2-centerht);
z11 = (w/2,h/2+1.75centerht);
z12 = (w/2,h/2-1.75centerht);

% draw the main cardinal loops
z13 = (w/2,h-ss-borderwid-smallspace);
z14 = 0.75[z11,z13] shifted (-centerwid/2,0);
z15 = 0.75[z11,z13] shifted (centerwid/2,0);
z16 = 0.5[z11,z13] shifted (-centerwid/4,0);
z17 = 0.5[z11,z13] shifted (centerwid/4,0);
pickup thinpen;
path lftshortloop; lftshortloop = z11{up}..z16..z14..{up}z13;
path rtshortloop; rtshortloop = z11{up}..z17..z15..{up}z13;
draw lftshortloop; draw rtshortloop;
draw lftshortloop reflectedabout ((0,h/2),(w,h/2));
draw rtshortloop reflectedabout ((0,h/2),(w,h/2));
z18 = (ss+borderwid+smallspace,h/2);
z19 = z8 shifted (-centerwid/2,0);
z24 = 0.75[z19,z18] shifted (0,centerwid/1);
z25 = 0.75[z19,z18] shifted (0,-centerwid/1);
z26 = 0.5[z19,z18] shifted (0,centerwid/2);
z27 = 0.5[z19,z18] shifted (0,-centerwid/2);
path uplongloop; uplongloop = z19{left}..z26..z24..{left}z18;
path downlongloop; downlongloop = z19{left}..z27..z25..{left}z18;
draw uplongloop; draw downlongloop;
draw uplongloop reflectedabout ((w/2,0),(w/2,h));
draw downlongloop reflectedabout ((w/2,0),(w/2,h));
% draw the large side loops
z28 = z16 shifted (-smallspace-thinline-centerwid/2,0);
z29 = z26 shifted (smallspace+thinline,smallspace+thinline);
z30 = (x29,y13);
z31 = z8 shifted (-centerwid-smallspace-thinline,
	centerht+smallspace+thinline);
z32 = (x18+smallspace+thinline,3h/4);
z33 = (x18+smallspace+thinline,h/4);
z34 = (x30,ss+borderwid+smallspace);
path sideloops;
sideloops = z29{z8-z29}..z31..z28..{left}z30{left}..z32..z24..z25;
% continue on with the side loops;
z80 = (w/4,h/2);
z81 = (x32+bigspace,h-y32);
z82 = (x30,h-y30+bigspace);
z83 = (x31-bigspace,h-y31);
z84 = 0.25[z19,z18];
path downloops;
downloops := z84{down}..z83..z82..tension 1.5..z81{dir 45}..z80..z29;
% do the internal ornaments
z88 = z29 shifted (0,smallspace);
z85 = (z88--(w/4,h)) intersectionpoint sideloops;
z86 = (z88--(0,h)) intersectionpoint sideloops;
z87 = (z88--(3w/4,h)) intersectionpoint sideloops;
z89 = 0.5[z86,z85] shifted (0,-3bigspace);
z90 = 0.5[z85,z87] shifted (0,-3bigspace);
z91 = 0.5[z86,z85];
z92 = 0.5[z85,z87];
z93 = (x29,y29+2bigspace);
z94 = (x25,y93);
z95 = (x84,y93);
z96 = (x86,(y89+y94)/2);
z97 = (x89,y93+2bigspace);
pickup extthinpen;
path curveone; path curvetwo; path curvethree;
curveone = z91..z86..z89..{dir 30}z85{dir -45}..z90..z87..z92;
curvetwo = z89..z93..z94..z96..z97;
curvethree = z90..z93..z95;

% draw the inner curves
pickup extthinpen;
draw curveone;
draw curveone reflectedabout ((0,h/2),(w,h/2));
draw curveone reflectedabout ((w/2,h),(w/2,0));
draw curveone reflectedabout ((w/2,h),(w/2,0))
	reflectedabout ((0,h/2),(w,h/2));
draw curvetwo;
draw curvetwo reflectedabout ((0,h/2),(w,h/2));
draw curvetwo reflectedabout ((w/2,h),(w/2,0));
draw curvetwo reflectedabout ((w/2,h),(w/2,0))
	reflectedabout ((0,h/2),(w,h/2));
draw curvethree;
draw curvethree reflectedabout ((0,h/2),(w,h/2));
draw curvethree reflectedabout ((w/2,h),(w/2,0));
draw curvethree reflectedabout ((w/2,h),(w/2,0))
	reflectedabout ((0,h/2),(w,h/2));
% erase sideloop paths
pickup thinerasepen;
erase draw sideloops;
erase draw sideloops reflectedabout ((0,h/2),(w,h/2));
erase draw sideloops reflectedabout ((w/2,h),(w/2,0));
erase draw sideloops reflectedabout ((w/2,h),(w/2,0))
	reflectedabout ((0,h/2),(w,h/2));
pickup thinpen;
draw sideloops;
draw sideloops reflectedabout ((0,h/2),(w,h/2));
draw sideloops reflectedabout ((w/2,h),(w/2,0));
draw sideloops reflectedabout ((w/2,h),(w/2,0))
	reflectedabout ((0,h/2),(w,h/2));
% erase the lines that will be the cardinal loops
pickup thinerasepen;
erase draw uplongloop; erase draw downlongloop;
erase draw uplongloop reflectedabout ((w/2,0),(w/2,h));
erase draw downlongloop reflectedabout ((w/2,0),(w/2,h));
% now draw in the cardinal loops
pickup thinpen;
draw uplongloop; draw downlongloop;
draw uplongloop reflectedabout ((w/2,0),(w/2,h));
draw downlongloop reflectedabout ((w/2,0),(w/2,h));
draw lftshortloop; draw rtshortloop;
draw lftshortloop reflectedabout ((0,h/2),(w,h/2));
draw rtshortloop reflectedabout ((0,h/2),(w,h/2));
% draw the center construct
fill centell shifted z8;
fill centell xscaled 0.75 yscaled 0.75 shifted z9;
fill centell xscaled 0.75 yscaled 0.75 shifted z10;
fill centell xscaled 0.25 yscaled 0.5 shifted z11;
fill centell xscaled 0.25 yscaled 0.5 shifted z12;
% add in the binders
fill centell scaled 0.75 rotated (90) shifted z84;
fill centell scaled 0.75 rotated (90) shifted z84
	reflectedabout ((w/2,h),(w/2,0));
pickup extthinpen;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
penlabels(17,18,19,20,21,22,23,24,25,26,27,28,29,30);
penlabels(31,32,33,34,35,36,37,38,39,40,41,42,43,44);
endchar;
