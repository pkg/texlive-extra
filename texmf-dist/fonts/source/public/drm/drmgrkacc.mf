% +AMDG  This document was begun on 2 May 11EX, the feast of
% St. Athanasius, BC, and it is humbly dedicated to him and
% to the Immaculate Heart of Mary for their prayers and to
% the Sacred Heart of Jesus for His mercy.

picture ringacc;
picture hungum;
picture roundcircum;
picture invcircum;
picture macron;
picture overdot;
picture cedille;
picture revcid;
picture grave;
picture acute;
picture diaresis;
picture circumflex;
picture tilde;

beginchar(oct"021",2pwid#,cap#,0); "The grave accent, `";

z0 = (ss,h-o);
z1 = z0 shifted (accwid,accwid);
z2 = z0 shifted (accwid,-accwid);
z3 = (w-ss,h-o);
z4 = z3 shifted (-2accwid,2accwid);
z5 = z3 shifted (-2accwid,-2accwid);
z6 = (w/2,h-o);

path graveacc; graveacc = z0..z1--z4..z3..z5--z2..cycle;
fill graveacc rotatedaround (z6,-250) shifted (0,-h);

grave := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"255",2pwid#,cap#,0); "The Hungarian diaresis";

z0 = (w/2-accwid,h-o);
z2 = z0 shifted (0,-pwid);
z3 = z0 shifted (2accwid,0);
z4 = z2 shifted (2accwid,0);
z5 = 0.5[z0,z3] shifted (0,accwid);
z6 = 0.5[z2,z4] shifted (0,-accwid);
z7 = 0.5[z5,z6];

path lthung; lthung = z0..z5..z3--z4..z6..z2--cycle;

fill lthung rotatedaround (z7,-30);
fill lthung rotatedaround (z7,-30) shifted (w/3,0);

hungum := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"255",2pwid#,cap#,0); "The ring accent";

z0 = (w/2,h-dotwid-accwid);

pickup pencircle scaled thinl;
path ringaccent;
ringaccent = fullcircle scaled 3dotwid shifted z0;
draw ringaccent;

ringacc := currentpicture;

endchar;

beginchar(oct"020",2pwid#,cap#,0); "The acute accent, '";

z6 = (w/2,h-o);

fill graveacc rotatedaround (z6,70) shifted (0,-h);

acute := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"140",2pwid#,cap#,0); "High grave accent, `";
currentpicture := grave shifted (0,h-pwid/3);
picture highgrave; highgrave := currentpicture;
endchar;

beginchar(oct"047",2pwid#,cap#,0); "The acute accent, '";
z6 = (w/2,h-o);
fill graveacc rotatedaround (z6,70) shifted (0,-pwid/2);
picture highacute; highacute := currentpicture;
endchar;

beginchar(oct"376",2pwid#,cap#,0); "The acute accent, '";
currentpicture := highacute;
endchar;

beginchar(oct"377",2pwid#,cap#,0); "inverted acute accent";
currentpicture := highgrave reflectedabout ((0,h/2),(w,h/2)) 
	shifted (0,-dep);
endchar;

beginchar(oct"255",2pwid#,cap#,0); "The circumflex, ^";

z0 = (w/2,h-o);
z1 = (ss,h-pwid);
z2 = (w-ss,h-pwid);
z3 = z0 shifted (0,-3accwid);
z5 = z1 shifted (3accwid,0);
z7 = z2 shifted (-3accwid,0);

path circum;
circum = z0--z2--z7--z3--z5--z1--cycle;
fill circum;
circumflex := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"255",2pwid#,cap#,0); "The inverted circumflex";

fill circum reflectedabout ((0,h-pwid/2-o),(w,h-pwid/2-o));
invcircum := currentpicture;

endchar;

beginchar(oct"024",2pwid#,cap#,0); "The low rounded inverted circumflex";

z0 = (w/2,h-pwid);
z1 = (ss,h-o);
z2 = (w-ss,h-o);
z3 = z0 shifted (0,-2accwid);
z4 = z1 shifted (accwid,0);
z5 = z2 shifted (-accwid,0);
z6 = 0.5[z2,z5] shifted (0,accwid/2);
z7 = 0.5[z1,z4] shifted (0,accwid/2);

fill z3{right}..{up}z2..z6..z5{down}..{left}z0{left}..{up}z4..z7..z1{down}..{right}cycle shifted (0,-h);

roundcircum := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"036",2pwid#,cap#,0); "The inverted circumflex";

z0 = (w/2,h-pwid);
z1 = (ss,h-o);
z2 = (w-ss,h-o);
z3 = z0 shifted (0,-2accwid);
z4 = z1 shifted (accwid,0);
z5 = z2 shifted (-accwid,0);
z6 = 0.5[z2,z5] shifted (0,accwid/2);
z7 = 0.5[z1,z4] shifted (0,accwid/2);

fill
z3{right}..{up}z2..z6..z5{down}..{left}z0{left}..{up}z4..z7..z1{down}..{right}cycle;

roundcircum := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"001",2pwid#,cap#,0); "The low rounded circumflex";

z0 = (w/2,h-pwid);
z1 = (ss,h-o);
z2 = (w-ss,h-o);
z3 = z0 shifted (0,-2accwid);
z4 = z1 shifted (accwid,0);
z5 = z2 shifted (-accwid,0);
z6 = 0.5[z2,z5] shifted (0,accwid/2);
z7 = 0.5[z1,z4] shifted (0,accwid/2);

fill z3{right}..{up}z2..z6..z5{down}..{left}z0{left}..{up}z4..z7..z1{down}..{right}cycle
reflectedabout ((0,h/2),(w,h/2)) shifted (0,-dep);

roundcircum := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"037",2pwid#,cap#,0); "The macron";

z0 = (ss,h-pwid/2);
z1 = (w-ss,h-pwid/2);
z2 = z0 shifted (0,-accwid/2);
z3 = z1 shifted (0,-accwid/2);

fill z0---z1..z3---z2..cycle;

macron := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"012",pwid#,cap#,0); "The overdot";

fill fullcircle scaled 2dotwid shifted (w/2,cap-dotwid);

overdot := currentpicture;

endchar;

beginchar(oct"176",2pwid#,cap#,0); "The tilde, ~";

z0 = (ss,h-pwid-o);
z1 = (w/3,h-ss);
z2 = (2w/3,h-pwid);
z3 = (w-ss,h);
z4 = z1 shifted (0,-1.5thinl);
z5 = z2 shifted (0,1.5thinl);

fill z0..{right}z1{right}..{right}z5{right}..{dir 60}z3{dir
	-95}..{left}z2{left}..{left}z4{left}..z0..cycle;

tilde := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"042",2pwid#,cap#,0); "The diaresis";

z0 = (w/4,h-dotwid);
z1 = (3w/4,h-dotwid);

fill fullcircle scaled 2dotwid shifted z0;
fill fullcircle scaled 2dotwid shifted z1;

diaresis := currentpicture;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"010",pwid#,cap#,0); "The capital iota subscript";
fill capeye yscaled 0.5 xscaled 0.8;
picture capiotasub; capiotasub := currentpicture;
endchar;

beginchar(oct"022",ex#,cap#,0); "lightning-bolt like symbol";

pickup pencircle scaled 1.5thinl;
penpos0(thickl,0);
z0 = (w/4,h+o);
penpos1(thinl,-45);
z1l = (ss,h/2-thinstroke);
penpos2(thinl,-45);
z2r = (w-ss,h/2+thinstroke);
penpos3(thickl,0);
z3 = (3w/4,0-o);

penstroke z0e--z1e;
penstroke z1e--z2e;
penstroke z2e--z3e;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"023",ex#,cap#,0); "magnifying glass";

z0 = (w/2,0-o);
z1 = z0 shifted (1.5thinstroke,1.5thinstroke);
z2 = z0 shifted (-1.5thinstroke,1.5thinstroke);
path magcirc;
magcirc = fullcircle scaled (w-2ss-1.5thinl) shifted (w/2,3h/4);
z3 = ((z2--(w/2,3h/4)) intersectionpoint magcirc) shifted
	(-thinl/2,0);
z4 = ((z1--(w/2,3h/4)) intersectionpoint magcirc) shifted
	(thinl/2,0);
z5 = (z0--(w/2,3h/4)) intersectionpoint magcirc;

fill z0..z1---z4..z5..z3---z2..cycle;
pickup pencircle scaled 1.5thinl;
draw magcirc;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"025",ex#,cap#,0); "magnifying glass";

pickup pencircle scaled 1.5thinl;
draw magcirc;
z0 = (w/2-thickl/2,lserhl);
z1 = z0 shifted (thickl,0);
llserif(0,2,3,4,5)(1);
lrserif(1,6,7,8,9)(1);
z10 = (z0--(x0,3h/4)) intersectionpoint magcirc;
z11 = (z1--(x1,3h/4)) intersectionpoint magcirc;

fill z0..z2..z3--z4--z8--z7..z6..z1--z11--z10--cycle;

penlabels(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51);
endchar;

beginchar(oct"040",2pwid#,cap#,0); "The tilde and diaresis";
currentpicture := diaresis shifted (0,-0.5pwid) + 
	tilde shifted (0,0.75pwid);
picture tildeumlaut; tildeumlaut := currentpicture;
endchar;

beginchar(oct"043",2pwid#,cap#,0); "The acute and diaresis";
currentpicture := diaresis + acute shifted (0,h);
picture acuteumlaut; acuteumlaut := currentpicture;
endchar;

beginchar(oct"044",2pwid#,cap#,0); "The grave and diaresis";
currentpicture := diaresis + grave shifted (0,h);
picture graveumlaut; graveumlaut := currentpicture;
endchar;

beginchar(oct"100",2pwid#,cap#,0); "Rough breathing and tilde";
currentpicture := roughbreath + tilde shifted (-pwid/4,pwid);
picture roughtilde; roughtilde := currentpicture;
endchar;

beginchar(oct"103",2pwid#,cap#,0); "Rough breathing and grave";
currentpicture := roughbreath shifted (-pwid/4,0) + 
	grave shifted (pwid/4,h-pwid/2);
picture roughgrave; roughgrave := currentpicture;
endchar;

beginchar(oct"126",2pwid#,cap#,0); "Rough breathing and acute";
currentpicture := roughbreath shifted (-pwid/4,0) + 
	acute shifted (pwid/4,h-pwid/2);
picture roughacute; roughacute := currentpicture;
endchar;

beginchar(oct"134",2pwid#,cap#,0); "Smooth breathing and tilde";
currentpicture := smoothbreath + tilde shifted (-pwid/4,pwid);
picture smoothtilde; smoothtilde := currentpicture;
endchar;

beginchar(oct"136",2pwid#,cap#,0); "Smooth breathing and acute";
currentpicture := smoothbreath shifted (-pwid/4,0) + 
	acute shifted (pwid/4,h-pwid/2);
picture smoothacute; smoothacute := currentpicture;
endchar;

beginchar(oct"137",2pwid#,cap#,0); "Smooth breathing and grave";
currentpicture := smoothbreath shifted (-pwid/4,0) + 
	grave shifted (pwid/4,h-pwid/2);
picture smoothgrave; smoothgrave := currentpicture;
endchar;

beginchar(oct"174",2pwid#,0,dep#); "iota subscript";

z0 = (w/2-1.5thinstroke/2,-thinstroke);
z1 = z0 shifted (1.5thinstroke,0);
z2 = z0 shifted (0,-dep/2);
z3 = z2 shifted (1.5thinstroke,0);
z4 = ((x2+x3)/2,-dep);
z5 = z3 shifted (1.5thinstroke,0);
z6 = ((x3+x5)/2,y3-0.5thinstroke);

fill z0---z2..z4{right}..{up}z5..z6..z3---z1--cycle;

picture iotasub; iotasub := currentpicture;

penlabels(0,1,2,3,4,5,6);
endchar;

beginchar(oct"227",cap#,cap#,0); "smile";
fill theparen yscaled 0.70 rotatedaround ((w/2,h/2),90)
	shifted (-0.275w,h-ex/2);
endchar;
