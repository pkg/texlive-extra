% Schulausgangsschrift (SAS) wesa
% Walter Entenmann
% 16.10.2011
% 18.11.2014: einheitliche Sonderzeichen
%             Ziffern nur 3/4-hoch und korrigiert
%             Gross- und Kleinbuchstaben korrigiert
% 30.11.2014: modifiziert, Duktus
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% #
beginchar("#",35ut#,Ht#,0);
  pickup wesa_pen;
  p:=(0,0)--(0,Ht);
  draw p slanted 0.176 shifted (10ut,0);
  draw p slanted 0.176 shifted (20ut,0);
  q:=((0,0)--(26ut,0)) shifted (2ut,0);
  draw q shifted(0,Ht/3) slanted 0.176;
  draw q shifted(0,2Ht/3) slanted 0.176;
endchar;

% §
  beginchar(oct"237",43.333ut#,Ht#,dt#);
  pickup wesa_pen;
sep:=10ut; breit:=20ut;
p := ((8ut,20ut){curl 10}..(0ut,30ut){left}..(-breit/2,22ut){down}..
  (breit/2,-2.5ut){down}..{-1,2/3}(2ut,-11ut)) scaled ((Ht+dt)/60ut);
  draw p shifted (sep+breit*0.583,ht/2);
  draw p rotated 180 shifted (sep+breit*0.583,ht/2);
  endchar;
  
% *
beginchar("*",30ut#,Ht#,0);
  pickup wesa_pen;
  p:= (-ht/2,0)--(ht/2,0);
  draw p rotated 90 shifted (15ut,1.5ht);
  draw p rotated 30 shifted (15ut,1.5ht);
  draw p rotated -30 shifted (15ut,1.5ht);
endchar;

% Prozentzeichen (%)
beginchar("%",40ut#,Ht#,0);
  pickup wesa_pen;
  q := (-23.6ut,0)--(23.6ut,0);
  breit:=12.5ut;hoch:=17.5ut;
  p := (breit/2,0){up}..(0,hoch/2){left}..(-breit/2,0){down}..(0,-hoch/2){right}..{up}(breit/2,0);
  x0:=whatever;y0:=whatever;
  z0=(directionpoint (-1,2) of (subpath (0,1) of p)) shifted (7.5ut+breit/2,Ht-hoch/2);
  draw p shifted (7.5ut+breit/2,Ht-hoch/2);
  draw p shifted (7.5ut+25ut-breit/2,hoch/2);
  draw q rotated 58 shifted (w/2,h/2);
  draw z0{1,-2}..{dir 58}(7.5ut+25ut,Ht);
endchar;

% &
  beginchar("&",40ut#,Ht#,0);
  pickup wesa_pen;
  p := (22.5ut,12.5ut)..tension 1.2..(7.5ut,0){left}..(0,8ut){up}..
  %(8.167ut,21ut){3,2}..
  tension 1.2..(17ut,34.5ut){up}..(11.5ut,Ht){left}..(4ut,30ut){down}..{2,-3}(8.167ut,21ut)--(22.5ut,0);
  draw p shifted (10ut,0);
  endchar;

% $
beginchar("$",30ut#,Ht#,0);
  pickup wesa_pen;
  draw halfcircle xscaled 20ut yscaled 20ut shifted (15ut,30ut);
   draw (halfcircle rotated 180 xscaled 20ut yscaled 20ut) shifted (15ut,10ut);
   draw ((-10ut,10ut){down}..{down}(10ut,-10ut)) shifted (15ut,ht);
   draw ((0,ht+2.5ut)--(0,-ht-2.5ut)) shifted (15ut,ht);
endchar;

% "+"
beginchar("+",30ut#,22.5ut#,0);
  path p;
  pickup wesa_pen;
  z0=(15ut,hmath);
  z1=(0ut,10ut);
  z2=(0ut,-10ut);
  p := z1--z2;
  draw p   shifted z0 ;
  draw p   rotated 90 shifted z0 ;
  labels(range 0 thru 2); 
endchar;

% ASCII Bindestrich oct"055" (OT1: auch Trennstrich)
beginchar("-",27.5ut#,ht#,0);
  pickup wesa_pen;
  draw ((0,0)--(12.5ut,0)) shifted (7.5ut,hdash);
endchar;

% T1: Trennstrich
beginchar(oct"177",27.5ut#,ht#,0);
  pickup wesa_pen;
  draw ((0,0)--(12.5ut,0)) shifted (7.5ut,hdash);
endchar;

% "/" 
beginchar("/",25ut#,Ht#,0);
  pickup wesa_pen;
draw ((5ut,0)--(20ut,Ht));
endchar;

% ">" 
beginchar(">",30ut#,Ht#,0);
  path p;
  pickup wesa_pen;
p:=(0,0)--(20ut,0);
draw p rotated 30 shifted (6.33ut,hmath);
  draw p rotated -30 shifted (6.33ut,hmath);
endchar;

% "=" 
beginchar("=",30ut#,ht#,0);
    pickup wesa_pen;
  draw((0,0)--(20ut,0)) shifted (5ut,hmath-2.5ut);
  draw((0,0)--(20ut,0)) shifted (5ut,hmath+2.5ut);
endchar;

% "<" 
beginchar("<",30ut#,Ht#,0);
  path p;
  pickup wesa_pen;
p:=(0,0)--(-20ut,0);
draw p rotated 30 shifted (23.66ut,hmath);
  draw p rotated -30 shifted (23.66ut,hmath);
endchar;

% oct"025" Bis-Strich --, Gedankenstrich 
beginchar(oct"025",30ut#,ht#,0);
  pickup wesa_pen;
draw ((0,0)--(20ut,0)) shifted (5ut,hdash);
endchar;

% engl. Gedankenstrich ---
beginchar(oct"026",45ut#,ht#,0);
  pickup wesa_pen;
draw ((0,0)--(30ut,0)) shifted (7.5ut,hdash);
endchar;

% oct"023" franz. Anfz. <<
beginchar(oct"023",32.5ut#,ht#,0);
  path p, q;
  pickup wesa_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
  z4=(6,1);
  z5=(7.5ut,ht/2);
z6=(15ut,ht/2);
  p := (z1{z3}..z2) rotated 45;
    q := (z1{z4}..z2) rotated -45;
  draw p shifted z5  ;
  draw p shifted z6 ;
    draw q shifted z5 ;
  draw q shifted z6 ;
  labels(range 1 thru 6);
endchar;  

% oct"024" franz. Anfz. >>
beginchar(oct"024",32.5ut#,ht#,0);
  path p, q;
  pickup wesa_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
    z4=(6,1);
  z5=(17.5ut,ht/2);
  z6=(25ut,ht/2);

  p := (z1{z4}..z2) rotated 135;
    q := (z1{z3}..z2) rotated -135;
  draw p shifted z5 ;
  draw p shifted z6 ;
    draw q shifted z5 ;
  draw q shifted z6 ;
  labels(range 1 thru 6);
endchar;  

% oct"016" franz. Anfz. <
beginchar(oct"016",25ut#,ht#,0);
  path p, q;
  pickup wesa_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
  z4=(6,1);
  z5=(7.5ut,ht/2);
  p := (z1{z3}..z2) rotated 45;
    q := (z1{z4}..z2) rotated -45;
  draw p shifted z5  ;
    draw q shifted z5 ;
  labels(range 1 thru 5);
endchar;  

% oct"017" franz. Anfz. >
beginchar(oct"017",25ut#,ht#,0);
  path p, q;
  pickup wesa_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
    z4=(6,1);
  z5=(17.5ut,ht/2);
  p := (z1{z4}..z2) rotated 135;
    q := (z1{z3}..z2) rotated -135;
  draw p shifted z5 ;
    draw q shifted z5 ;
  labels(range 1 thru 5);
endchar;  

% "@":
beginchar("@",50ut#,Ht#,0);
  pickup wesa_pen;
sep:=10ut;gap:=8ut;
draw (superellipse((a,0),(0,b),(-a,0),(0,-b),sigma)) shifted (gap+a+sep,ht);
draw ((0,ht)--(0,5ut){down}..(2ut,0){right}..tension 1.5..(gap,ht/2){up}..
  (-a,3ht/2){left}..(-2*a-gap,ht/2){down}..
  (-a,-ht/2){right}..(7.5ut,-5ut)) shifted (2*a+gap+sep,ht/2);
endchar;

% Euro (\symbol{160})
beginchar(oct"240",47.5ut#,Ht#,0);
  pickup wesa_pen;
  sep:=7.5ut; gap:=7.5ut;
    draw (halfcircle rotated 90) xscaled 30ut yscaled Ht shifted (15ut+gap+sep,Ht/2);
  draw ((0,0){right}..(7.5ut,-3ut)) shifted (15ut+gap+sep,Ht);
  draw ((0,0){right}..(7.5ut,3ut)) shifted (15ut+gap+sep,0);
  draw (((-25ut,2.5ut)--(0,2.5ut))  shifted (15ut+gap+sep,ht)) slanted 0.25;
  draw (((-25ut,-2.5ut)--(0,-2.5ut)) shifted (15ut+gap+sep,ht)) slanted 0.25;
endchar;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Aufstrich am Wortanfang 
beginchar(oct"200",11.688ut#,ht#,0);
  pickup wesa_pen;
  draw ((0,0)--(((ht)-Delta)/m,ht-Delta));
  xpos:=((ht)-Delta)/m;
  zeigen;
endchar;

% Verbindungsstrich normal                   um rho verbreitert
beginchar(oct"005",15.688ut#,ht#,0);
  pickup wesa_pen;
  draw ((0,0){right}..tension2 and 3.5..{1,m}(2rho+((ht)-Delta)/m,ht-Delta));
  xpos:=2rho+((ht)-Delta)/m;
  zeigen;
endchar;

 % "e" mit Aufstrich am Wortanfang 
beginchar(oct"030",8.709ut#,ht#,0);
  pickup wesa_pen;
     xpos:=0;
  draw ((0,0){dir30}..tension1.5..{up}((he)/m+ebreite,ht-ef'*ebreite))
    shifted (xpos,0);
     xpos:=xpos+(he)/m;
     latin_e_form(xpos);
     zeigen;
endchar;

% Endstueck gerade
beginchar(oct"007",3.5ut#,ht#,0);
    pickup wesa_pen;
    draw ((0,0)--(3.5ut,m'*3.5ut));
  endchar;

% Endstueck  rund
  beginchar(oct"006",5ut#,ht#,0);
      pickup wesa_pen;
draw ((0,0){right}..(5ut,2ut));
  endchar;

% Verlaengerungsstriche mit e,
% 10ut
  beginchar(oct"000",20.709ut#,ht#,0);
     pickup wesa_pen;
     xpos:=0;
schweif((0),ht-ebreite*ef',10ut+rho+(he)/m+ebreite,0,90,1.2,xpos);
xpos:=xpos-ebreite;
     latin_e_form(xpos);
     zeigen;
endchar;

% 12.5ut
  beginchar(oct"001",23.209ut#,ht#,0);
     pickup wesa_pen;
     xpos:=0;
schweif((0),ht-ebreite*ef',12.5ut+rho+(he)/m+ebreite,0,90,1.2,xpos);
xpos:=xpos-ebreite;
     latin_e_form(xpos);
      zeigen;
endchar;

% 15ut
  beginchar(oct"002",25.709ut#,ht#,0);
     pickup wesa_pen;
     xpos:=0;
schweif((0),ht-ebreite*ef',15ut+rho+(he)/m+ebreite,0,90,1.2,xpos);
xpos:=xpos-ebreite;
     latin_e_form(xpos);
      zeigen;
endchar;

% 17.5ut
  beginchar(oct"003",28.209ut#,ht#,0);
     pickup wesa_pen;
     xpos:=0;
schweif((0),ht-ebreite*ef',17.5ut+rho+(he)/m+ebreite,0,90,1.2,xpos);
xpos:=xpos-ebreite;
      latin_e_form(xpos);
       zeigen;
endchar;

% 20ut
  beginchar(oct"004",30.709ut#,ht#,0);
     pickup wesa_pen;
     xpos:=0;
schweif((0),ht-ebreite*ef',20ut+rho+(he)/m+ebreite,0,90,1.2,xpos);
xpos:=xpos-ebreite;
      latin_e_form(xpos);
       zeigen;
     endchar;

     % Aufstriche zu spitzen Buchstaben:
% normal                                  um rho verbreitert, tension 1.2-->1.5
   beginchar(oct"035",14.5ut#,ht#,0);
  pickup wesa_pen;
     draw ((0,0){right}..tension 1.5..{up}(12.5ut+rho,ht));
endchar;

% nach Unterlaengen
   beginchar(oct"036",12.5ut#,ht#,0);
  pickup wesa_pen;
draw ((0,0){1,m'}..tension 1..{up}(12.5ut,ht));
endchar;

% Lange Aufstriche vor spitzen BSt.
% 18.5ut
  beginchar(oct"021",18.5ut#,ht#,0);
     pickup wesa_pen;
axe:=18.5ut; sigm:=0.71;
draw ((0,0){right}..{axe,ht}(sigm*axe,(1-sigm)*ht)..{up}(axe,ht));
endchar;

% 21ut
  beginchar(oct"031",21ut#,ht#,0);
     pickup wesa_pen;
 axe:=21ut; sigm:=0.72;
      draw ((0,0){right}..{axe,ht}(sigm*axe,(1-sigm)*ht)..{up}(axe,ht));
endchar;

% 23.5ut
  beginchar(oct"032",23.5ut#,ht#,0);
     pickup wesa_pen;
 axe:=23.5ut; sigm:=0.73;
       draw ((0,0){right}..{axe,ht}(sigm*axe,(1-sigm)*ht)..{up}(axe,ht));
endchar;

% 26ut
  beginchar(oct"033",26ut#,ht#,0);
     pickup wesa_pen;
 axe:=26ut; sigm:=0.74;
	draw ((0,0){right}..{axe,ht}(sigm*axe,(1-sigm)*ht)..{up}(axe,ht));
endchar;

% 28.5ut
  beginchar(oct"034",28.5ut#,ht#,0);
     pickup wesa_pen;
 axe:=28.5ut; sigm:=0.75;
     draw ((0,0){right}..{axe,ht}(sigm*axe,(1-sigm)*ht)..{up}(axe,ht));
      endchar;
      
% fuer oben verbundene Buchstaben
% von b,o,v,w
      beginchar(oct"027",14.222ut#,ht#,0);
	     pickup wesa_pen;
	     draw ((0,ht){1,-mfahne}..tension 1.2..{up}(bfahne,ht));
	     xpos:=bfahne;
	     zeigen;
  endchar;
  
% von r
    beginchar(oct"037",12ut#,ht#,0);
           pickup wesa_pen;
	   draw ((0,ht){down}..tension 1.2..{up}(rfahne,ht));
	   xpos:=rfahne;
	   zeigen;
	 endchar;

% Verbindungsstrich von A Ä F H zu spitzen BSt.
	 beginchar(oct"236",15ut#,ht#,0);
	   pickup wesa_pen;
	   draw ((0,ht-Delta-eta)--(8ut,ht-Delta-eta){right}...{up}(15ut,ht));
	   xpos:=10ut;
	   	   zeigen;
	 endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Ziffern und Satzzeichen
%
% Ziffern
beginchar("0",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((0,h){left}..(-w'/2,h/2){down}..(0,0){right}..(w'/2,h/2){up}..{left}(0,h)) shifted (w/2,0);
endchar;

beginchar("1",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((-w'/2,0.6*h)--(0,h)--(0,0))  shifted (w/2+s'/2,0);
endchar;

beginchar("2",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((0,h-r)..(r,h){right}..(2r,h-r){down}..tension2 and 3..{-5,-6}(0,0))  shifted (s',0);
draw ((0,0)--(w',0)) shifted (s',0);
endchar;

beginchar("3",Wz#,Hz#,0);
    pickup wesa_pen;
    draw ((0,10h/12){up}..(3w'/8,h){right}..(6w'/8,9h/12){down}..tension1.2..{left}(0,h/2+1.5ut))
 shifted (s',0);
    draw ((0,h/2+1.5ut){right}..tension1..(w',3h/12){down}..(w'/2,0){left}..(0,h/12)) shifted (s',0);
endchar;

beginchar("4",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((0.5w',h)--(0,h/3)--(w',h/3)) shifted (s',0);
draw ((0.75w',3h/4)--(0.75w',0)) shifted (s',0);
endchar;


beginchar("5",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((0.75w',h)--(0,h)--(0,5h/8-dd){1,1}..(w'/2,5h/8){right}..(w',2.5h/8){down}..(w'/2,0){left}..(0,h/8)) shifted (s',0);
endchar;


beginchar("6",Wz#,Hz#,0);
    pickup wesa_pen;
draw ((0.75w',h){left}..tension0.8..(0,3h/8){down}..(w'/2,0){right}..(w',3h/8){up}..(w'/2,5h/8){left}..(0,3h/8){down}) shifted (s',0);
endchar;


beginchar("7",Wz#,Hz#,0);
  pickup wesa_pen;
  xpos:=0;
draw ((0,h)--(w',h)--(0.375w',0)) shifted (s',0);
quer((h/2),xpos+13.75ut+s')
endchar;

beginchar("8",Wz#,Hz#,0);
  pickup wesa_pen;
draw ((0,0.5625h){1,0.15}..(3w'/8,h-3w'/8){up}..(0,h){left}..(-3w'/8,h-3w'/8){down}..(0,0.5625h){1,-0.15}..
  (w'/2,h/4){down}..(0,0){left}..(-w'/2,h/4){up}..{1,0.15}(0,0.5625h)) shifted (s'+w'/2,0);
endchar;



beginchar("9",Wz#,Hz#,0);
  pickup wesa_pen;
  hoch:=5h/16; breit:=w'/2;
draw ((0,hoch){left}..(-breit,0){down}..(0,-hoch){right}..(breit,0){up}..{left}(0,hoch)) shifted (w/2,0.6875h);
draw ((w',0.6875h){down}..(w'/2,0){left}..(w'/8,h/16)) shifted (s',0);
endchar;


% Satzzeichen
% ASCII-Zeichen
% !
beginchar("!",20ut#,Ht#,0);
  pickup wesa_pen;
  stamm(h,10ut,10ut);
  pickup wesa_pen_thick;
  drawdot (0,0) shifted (10ut,0);
endchar;

% ", oct"042", ASCII-Doppelapostroph, hier identisch mit oct"020", dt. Anfz. o.
beginchar(oct"042",25ut#,Ht#,0);
  pickup wesa_pen;
  komma(Ht,10ut);
  komma(Ht,15ut);
endchar;

% ' (Apostroph)
beginchar("'",15ut#,Ht#,0);
  pickup wesa_pen;
komma((Ht),7.5ut);
endchar;

% "("
beginchar("(",20ut#,Ht#,dt#);
  pickup wesa_pen;
draw ((10ut,Ht)..(2.5ut,ht/2){down}..(10ut,-dt)) shifted (5ut,0);
endchar;

% ")"
beginchar(")",20ut#,Ht#,dt#);
  pickup wesa_pen;
draw ((5ut,Ht)..(12.5ut,ht/2){down}..(5ut,-dt));
endchar;

% "," Komma
beginchar(",",20ut#,ht#,dt#);
  pickup wesa_pen;
komma(2.5ut,10ut);
endchar;

% "." Punkt
beginchar(".",20ut#,ht#,0);
  pickup wesa_pen_thick;
  drawdot (0,0) shifted (10ut,0);
endchar;

% ":" Doppelpunkt
beginchar(":",20ut#,ht#,0);
  pickup wesa_pen_thick;
  drawdot (0,0) shifted (10ut,0);
  drawdot (0,0) shifted (10ut,15ut);
endchar;

% ";" Strichpunkt
beginchar(";",20ut#,ht#,dt#);
  pickup wesa_pen;
komma(2.5ut,10ut);
  pickup wesa_pen_thick;
  drawdot (0,0) shifted (10ut,15ut);
endchar;

% ?
beginchar("?",35ut#,Ht#,0);
  radi:=7.5ut;
  pickup wesa_pen;
  z1=(-radi,h-radi);
  z2=(0,h);
  z3=(radi,h-radi);
  z5=(-radi,10ut+radi);
  z6=(0,10ut);
  z7=(radi,10ut+radi);
  p := z1.. z2{right}..z3{down}..{down}z5..z6{right}..z7;
  draw p shifted (17.5ut,0);
  pickup wesa_pen_thick;
  drawdot (0,0) shifted (17.5ut,0);
endchar;

% spezielle Sonderzeichen (ec-Kodierung(
% oct"020", wie ASCII " (oct"042"), dt. Anfuehrungszeichen oben
beginchar(oct"020",15ut#,Ht#,0);
  pickup wesa_pen;
  komma((Ht),5ut);
  komma((Ht),10ut);
endchar;

% oct"022", dt. Anfuehrungszeichen unten
beginchar(oct"022",25ut#,ht#,dt#);
  pickup wesa_pen;
  komma(2.5ut,10ut);
  komma(2.5ut,15ut);
endchar;

% oct"133" [
beginchar("[",20ut#,Ht#,dt#);
  pickup wesa_pen;
draw ((5ut,Ht)--(0,Ht)--(0,-dt)--(5ut,-dt)) shifted (10ut,0);
endchar;

% oct"135" ]
beginchar("]",20ut#,Ht#,dt#);
  pickup wesa_pen;
draw ((0,Ht)--(5ut,Ht)--(5ut,-dt)--(0,-dt)) shifted (5ut,0);
endchar;

%%%%%%%%%%%%%%%%%%%% End of File %%%%%%%%%%%%%%%%%
