% Vereinfachte Ausgangsschrift weva
% Ziffern, Satzzeichen, Sonderzeichen
% Walter Entenmann
% 11.09.2011
% 13.09.2012
% 14.11.2014: einheitliche Sonderzeichen

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% #
beginchar("#",35ut#,Ht#,0);
  pickup weva_pen;
  p:=(0,0)--(0,Ht);
  draw p slanted 0.176 shifted (10ut,0);
  draw p slanted 0.176 shifted (20ut,0);
  q:=((0,0)--(26ut,0)) shifted (2ut,0);
  draw q shifted(0,Ht/3) slanted 0.176;
  draw q shifted(0,2Ht/3) slanted 0.176;
endchar;

% §
  beginchar(oct"237",43.333ut#,Ht#,dt#);
  pickup weva_pen;
sep:=10ut; breit:=20ut;
p := ((8ut,20ut){curl 10}..(0ut,30ut){left}..(-breit/2,22ut){down}..
  (breit/2,-2.5ut){down}..{-1,2/3}(2ut,-11ut)) scaled ((Ht+dt)/60ut);
  draw p shifted (sep+breit*0.583,ht/2);
  draw p rotated 180 shifted (sep+breit*0.583,ht/2);
  endchar;
  
% *
beginchar("*",30ut#,Ht#,0);
  pickup weva_pen;
  p:= (-ht/2,0)--(ht/2,0);
  draw p rotated 90 shifted (15ut,1.5ht);
  draw p rotated 30 shifted (15ut,1.5ht);
  draw p rotated -30 shifted (15ut,1.5ht);
endchar;

% Prozentzeichen (%)
beginchar("%",40ut#,Ht#,0);
  pickup weva_pen;
  q := (-23.6ut,0)--(23.6ut,0);
  breit:=12.5ut;hoch:=17.5ut;
  p := (breit/2,0){up}..(0,hoch/2){left}..(-breit/2,0){down}..(0,-hoch/2){right}..{up}(breit/2,0);
  x0:=whatever;y0:=whatever;
  z0=(directionpoint (-1,2) of (subpath (0,1) of p)) shifted (7.5ut+breit/2,Ht-hoch/2);
  draw p shifted (7.5ut+breit/2,Ht-hoch/2);
  draw p shifted (7.5ut+25ut-breit/2,hoch/2);
  draw q rotated 58 shifted (w/2,h/2);
  draw z0{1,-2}..{dir 58}(7.5ut+25ut,Ht);
endchar;

% &
  beginchar("&",40ut#,Ht#,0);
  pickup weva_pen;
  p := (22.5ut,12.5ut)..tension 1.2..(7.5ut,0){left}..(0,8ut){up}..
  %(8.167ut,21ut){3,2}..
  tension 1.2..(17ut,34.5ut){up}..(11.5ut,Ht){left}..(4ut,30ut){down}..{2,-3}(8.167ut,21ut)--(22.5ut,0);
  draw p shifted (10ut,0);
  endchar;

% $
beginchar("$",30ut#,Ht#,0);
  pickup weva_pen;
  draw halfcircle xscaled 20ut yscaled 20ut shifted (15ut,30ut);
   draw (halfcircle rotated 180 xscaled 20ut yscaled 20ut) shifted (15ut,10ut);
   draw ((-10ut,10ut){down}..{down}(10ut,-10ut)) shifted (15ut,ht);
   draw ((0,ht+2.5ut)--(0,-ht-2.5ut)) shifted (15ut,ht);
endchar;

% "+"
beginchar("+",30ut#,22.5ut#,0);
  path p;
  pickup weva_pen;
  z0=(15ut,hmath);
  z1=(0ut,10ut);
  z2=(0ut,-10ut);
  p := z1--z2;
  draw p   shifted z0 ;
  draw p   rotated 90 shifted z0 ;
  labels(range 0 thru 2); 
endchar;

% ASCII Bindestrich oct"055" (OT1: auch Trennstrich)
beginchar("-",27.5ut#,ht#,0);
  pickup weva_pen;
  draw ((0,0)--(12.5ut,0)) shifted (7.5ut,hdash);
endchar;

% T1: Trennstrich
beginchar(oct"177",27.5ut#,ht#,0);
  pickup weva_pen;
  draw ((0,0)--(12.5ut,0)) shifted (7.5ut,hdash);
endchar;

% "/" 
beginchar("/",25ut#,Ht#,0);
  pickup weva_pen;
draw ((5ut,0)--(20ut,Ht));
endchar;

% ">" 
beginchar(">",30ut#,Ht#,0);
  path p;
  pickup weva_pen;
p:=(0,0)--(20ut,0);
draw p rotated 30 shifted (6.33ut,hmath);
  draw p rotated -30 shifted (6.33ut,hmath);
endchar;

% "=" 
beginchar("=",30ut#,ht#,0);
    pickup weva_pen;
  draw((0,0)--(20ut,0)) shifted (5ut,hmath-2.5ut);
  draw((0,0)--(20ut,0)) shifted (5ut,hmath+2.5ut);
endchar;

% "<" 
beginchar("<",30ut#,Ht#,0);
  path p;
  pickup weva_pen;
p:=(0,0)--(-20ut,0);
draw p rotated 30 shifted (23.66ut,hmath);
  draw p rotated -30 shifted (23.66ut,hmath);
endchar;

% oct"025" Bis-Strich --, Gedankenstrich 
beginchar(oct"025",30ut#,ht#,0);
  pickup weva_pen;
draw ((0,0)--(20ut,0)) shifted (5ut,hdash);
endchar;

% engl. Gedankenstrich ---
beginchar(oct"026",45ut#,ht#,0);
  pickup weva_pen;
draw ((0,0)--(30ut,0)) shifted (7.5ut,hdash);
endchar;

% oct"023" franz. Anfz. <<
beginchar(oct"023",32.5ut#,ht#,0);
  path p, q;
  pickup weva_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
  z4=(6,1);
  z5=(7.5ut,ht/2);
z6=(15ut,ht/2);
  p := (z1{z3}..z2) rotated 45;
    q := (z1{z4}..z2) rotated -45;
  draw p shifted z5  ;
  draw p shifted z6 ;
    draw q shifted z5 ;
  draw q shifted z6 ;
  labels(range 1 thru 6);
endchar;  

% oct"024" franz. Anfz. >>
beginchar(oct"024",32.5ut#,ht#,0);
  path p, q;
  pickup weva_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
    z4=(6,1);
  z5=(17.5ut,ht/2);
  z6=(25ut,ht/2);

  p := (z1{z4}..z2) rotated 135;
    q := (z1{z3}..z2) rotated -135;
  draw p shifted z5 ;
  draw p shifted z6 ;
    draw q shifted z5 ;
  draw q shifted z6 ;
  labels(range 1 thru 6);
endchar;  

% oct"016" franz. Anfz. <
beginchar(oct"016",25ut#,ht#,0);
  path p, q;
  pickup weva_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
  z4=(6,1);
  z5=(7.5ut,ht/2);
  p := (z1{z3}..z2) rotated 45;
    q := (z1{z4}..z2) rotated -45;
  draw p shifted z5  ;
    draw q shifted z5 ;
  labels(range 1 thru 5);
endchar;  

% oct"017" franz. Anfz. >
beginchar(oct"017",25ut#,ht#,0);
  path p, q;
  pickup weva_pen;
  z1=(0ut,0ut);
  z2=(1.1*ht/2,0);
  z3=(6,-1);
    z4=(6,1);
  z5=(17.5ut,ht/2);
  p := (z1{z4}..z2) rotated 135;
    q := (z1{z3}..z2) rotated -135;
  draw p shifted z5 ;
    draw q shifted z5 ;
  labels(range 1 thru 5);
endchar;  

% "@":
beginchar("@",50ut#,Ht#,0);
  pickup weva_pen;
sep:=10ut;gap:=8ut;
draw (superellipse((a,0),(0,b),(-a,0),(0,-b),sigma)) shifted (gap+a+sep,ht);
draw ((0,ht)--(0,5ut){down}..(2ut,0){right}..tension 1.5..(gap,ht/2){up}..
  (-a,3ht/2){left}..(-2*a-gap,ht/2){down}..
  (-a,-ht/2){right}..(7.5ut,-5ut)) shifted (2*a+gap+sep,ht/2);
endchar;

% Euro (\symbol{160})
beginchar(oct"240",47.5ut#,Ht#,0);
  pickup weva_pen;
  sep:=7.5ut; gap:=7.5ut;
    draw (halfcircle rotated 90) xscaled 30ut yscaled Ht shifted (15ut+gap+sep,Ht/2);
  draw ((0,0){right}..(7.5ut,-3ut)) shifted (15ut+gap+sep,Ht);
  draw ((0,0){right}..(7.5ut,3ut)) shifted (15ut+gap+sep,0);
  draw (((-25ut,2.5ut)--(0,2.5ut))  shifted (15ut+gap+sep,ht)) slanted 0.25;
  draw (((-25ut,-2.5ut)--(0,-2.5ut)) shifted (15ut+gap+sep,ht)) slanted 0.25;
endchar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Ziffern
beginchar("0",30ut#,Ht#,0);
  pickup weva_pen;
draw fullcircle xscaled 20ut yscaled Ht shifted (15ut,ht);
endchar;

beginchar("1",30ut#,Ht#,0);
  pickup weva_pen;
  xpos:=7.5ut;
  gerade(ht,Ht,10ut,xpos);
  stamm(Ht,0,xpos);
endchar;

beginchar("2",30ut#,Ht#,0);
  pickup weva_pen;
  draw halfcircle xscaled 17.5ut yscaled ht shifted (13.75ut,1.5*ht);
  draw ((22.5ut,1.5*ht){down}..tension2and 5..{-17.5ut,-27.5ut}(5ut,0));
  gerade(0,0,20ut,5ut);
endchar;

beginchar("3",30ut#,Ht#,0);
  pickup weva_pen;
  draw ((-8.5ut,Ht-5ut)..(0,Ht){right}..(8.5ut,30ut){down}..(-1.5ut,ht+1ut){left}) shifted (15ut,0);
  draw ((-1.5ut,ht+1ut){right}..(10ut,ht/2){down}..(0,0){left}..(-10ut,5ut)) shifted (15ut,0);
endchar;

beginchar("4",30ut#,Ht#,0);
  pickup weva_pen;
draw ((10ut,h)--(0,h/4)--(20ut,h/4)) shifted (5ut,0);
draw ((15ut,25ut)--(15ut,0)) shifted (5ut,0);
endchar;

beginchar("5",30ut#,Ht#,0);
  pickup weva_pen;
  draw ((20ut,Ht)--(0,Ht)--(0,25ut)) shifted (5ut,0);
  draw (halfcircle rotated -90 xscaled 25ut yscaled 27.5ut) shifted (12.5ut,13.75ut);
  draw ((7.5ut,27.5ut){left}..(0,25ut)) shifted (5ut,0);
  draw ((7.5ut,0){left}..(0,5ut)) shifted (5ut,0);
endchar;


beginchar("6",30ut#,Ht#,0);
  pickup weva_pen;
  draw fullcircle xscaled 20ut yscaled 27.5ut shifted (15ut,13.75ut);
  draw ((0,13.75ut){up}..(10ut,Ht){right}..(16ut,Ht-2.5ut)) shifted (5ut,0);
endchar;

beginchar("7",30ut#,Ht#,0);
  pickup weva_pen;
  draw ((0,Ht)--(20ut,Ht)--(10ut,0)) shifted (5ut,0);
    draw ((0,ht)--(10ut,ht)) shifted (15ut,0);
endchar;

beginchar("8",30ut#,Ht#,0);
  pickup weva_pen;
  p:=(0,0){-1,-macht}..(-10ut,-10ut){down}..(0,-ht){right}..(10ut,-10ut){up}..{-1,macht}(0,0);
  draw p shifted (15ut,ht);
  draw p rotated 180 shifted (15ut,ht);
endchar;


beginchar("9",30ut#,Ht#,0);
  pickup weva_pen;
  draw fullcircle xscaled 20ut yscaled 25ut shifted (15ut,27.5ut);
  draw ((20ut,Ht)--(20ut,27.5ut){down}..(10ut,0){left}..(0,5ut)) shifted (5ut,0);
endchar;

% ASCII-Zeichen
beginchar("!",20ut#,Ht#,0);
  pickup weva_pen;
  z1=(10ut,40ut);
  z2=(10ut,10ut);
  z3=(10ut,0);
  p := z1--z2;
  draw p;
  pickup weva_pen_thick;
  drawdot z3;
  labels(range 1 thru 3); 
endchar;

% ", Doppelapostroph, hier identisch mit oct"020", dt. Anfz. o.
beginchar(oct"042",25ut#,Ht#,0);
  pickup weva_pen;
  komma(12.5ut,Ht);
  komma(17.5ut,Ht);
endchar;

% [
beginchar("[",25ut#,Ht#,ht#);
  pickup weva_pen;
draw ((10ut,Ht)--(5ut,Ht)--(5ut,-ht)--(10ut,-ht)) shifted (5ut,0);
  endchar;

% ]
beginchar("]",25ut#,Ht#,ht#);
  pickup weva_pen;
draw ((0,Ht)--(5ut,Ht)--(5ut,-ht)--(0,-ht)) shifted (5ut,0);
  endchar;

beginchar("'",20ut#,Ht#,0);
  pickup weva_pen;
komma(12.5ut,Ht);
endchar;

beginchar("(",20ut#,60ut#,ht#);
  pickup weva_pen;
draw ((10ut,Ht)..(2.5,ht/2){down}..(10ut,-ht)) shifted (5ut,0);
endchar;

beginchar(")",20ut#,60ut#,ht#);
  pickup weva_pen;
draw ((0,Ht)..(7.5ut,ht/2){down}..(0,-ht)) shifted(5ut,0);
endchar;

beginchar(",",15ut#,ht#,dt#);
  pickup weva_pen;
komma(7.5ut,3.5ut); 
endchar;

beginchar(".",15ut#,ht#,0);
  pickup weva_pen_thick;
  z1=(7.5ut,0ut);
  drawdot z1  ;
  labels(1); 
endchar;

beginchar(":",20ut#,ht#,0);
  pickup weva_pen_thick;
  drawdot (0,0) shifted (10ut,0);
  drawdot (0,0) shifted (10ut,17.5ut);
endchar;

beginchar(";",20ut#,ht#,-dt#);
  pickup weva_pen;
komma(10ut,3.5ut);
  pickup weva_pen_thick;
  drawdot (0,0) shifted (10ut,17.5ut);
endchar;

beginchar("?",35ut#,Ht#,0);
  pickup weva_pen;
  z1=(-7.5ut,10ut);
  z2=(0,15ut);
  z3=(7.5ut,7.5ut);
  z4=(0,0);
  z5=(-7.5ut,-7.5ut);
  z6=(0,-15ut);
  z7=(7.5ut,-10ut);
  p := z1..z2{right}..z3{down}..z4..{down}z5..z6{right}..z7;
  draw p  shifted (w/2,h/2+5ut) ;
  pickup weva_pen_thick;
  drawdot (0,0) shifted (w/2,0);
  labels(range 1 thru 7); 
endchar;

%
% spezielle Sonderzeichen (T1-Kodierung)
% dt. Anfuehrungszeichen oben (\grqq), wie oct"042"
beginchar(oct"020",25ut#,Ht#,0);
  pickup weva_pen;
  komma(12.5ut,Ht);
  komma(17.5ut,Ht);
endchar;

% dt. Anfuehrungszeichen unten (\glqq)
beginchar(oct"022",25ut#,ht#,dt#);
  pickup weva_pen;
  komma(7.5ut,3.5ut);
  komma(12.5ut,3.5ut);
endchar;



%%%%%%%%%%%%%%%% File-Ende %%%%%%%%%%%%%%%%%
