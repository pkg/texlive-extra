#!/usr/bin/make -f
# 
# debian/rules file for texlive-extra

PACKAGES=texlive-bibtex-extra texlive-extra-utils texlive-font-utils texlive-formats-extra texlive-plain-generic texlive-latex-extra texlive-fonts-extra texlive-music texlive-games texlive-pstricks texlive-publishers texlive-humanities texlive-science texlive-fonts-extra-doc texlive-humanities-doc texlive-latex-extra-doc texlive-science-doc texlive-pstricks-doc texlive-publishers-doc
METAPACKAGES=texlive-fonts-extra-links

SHELL=/bin/bash

build: build-arch build-indep
build-arch:
build-indep:

skip_to_avoid_java-build-stamp:

	mkdir tex4ht-java
	mkdir -p texmf-dist/tex4ht/bin
	cd tex4ht ; javac *.java */*.java */*/*.java -d ../tex4ht-java
	cd tex4ht-java ; jar cvf ../texmf-dist/tex4ht/bin/tex4ht.jar .
	# build pax.jar
	#mkdir texmf-dist/source/latex/pax/lib
	#cd texmf-dist/source/latex/pax/lib ; ln -s /usr/share/java/commons-logging.jar .
	#cd texmf-dist/source/latex/pax/lib ; ln -s /usr/share/java/pdfbox.jar .
	cd texmf-dist/source/latex/pax ; ant
	# only needed for building
	rm -rf texmf-dist/source/latex/pax/lib
	# will not be installed, so dh_missing might complain, we do the links
	# via texlive-latex-extra.links
	rm -rf texmf-dist/scripts/pax/lib

	touch build-stamp

clean: 
	for i in $(PACKAGES) $(METAPACKAGES) ; do \
	  rm -f debian/$$i.{README.Debian,links,links.generated} ; \
	  rm -f debian/$$i.{postinst,postrm,preinst,prerm} ; \
	  rm -f debian/$$i.{maps,hyphens,formats,info} ; \
	  rm -f debian/$$i.{lintian-overrides} ; \
	done


	rm -rf tex4ht-java texmf-dist/tex4ht/bin/tex4ht.jar
	rm -f texmf-dist/tex/latex/pax.jar

	# texlive contains some .orig files we need to keep otherwise
	# the copy will fail due to the files occurring in the tlpdb
	dh_clean -X.orig -X.bak
	rm -f configure-stamp
	rm -f build-stamp
	rm -f install-stamp

install: install-stamp

install-stamp:
	# make sure we have texmf-dist/web2c otherwise the media detection
	# might break (like in texlive-lang)
	mkdir -p texmf-dist/web2c
	perl debian/tpm2deb-bin.pl --nosource $(PACKAGES) $(METAPACKAGES)
	for i in $(PACKAGES) $(METAPACKAGES) ; do \
	  bash debian/generate-license-file $$i ; \
	  bash debian/fix-privacy-breach $$i ; \
	  bash debian/useless-fix-perl-shebang-for-policy $$i ; \
	  find debian/$i/usr/share -name \*.jar -exec chmod ugo-x '{}' \; ; \
	  if [ -r debian/$$i.links.dist ] ; then cat debian/$$i.links.dist >> debian/$$i.links ; fi ; \
	done




	# rename wordcount to latex-wordcount
	mv debian/texlive-latex-extra/usr/bin/wordcount debian/texlive-latex-extra/usr/bin/latex-wordcount

	for i in $(PACKAGES) $(METAPACKAGES) ; do \
	  bash debian/create-font-links $$i >> debian/$$i.links ; \
	done
	touch install-stamp

binary-arch:

binary-indep: build install
	dh_testdir
	dh_testroot
	dh_install
	dh_installdirs
	#dh_installmenu
	dh_lintian
	dh_bugfiles -A
	dh_installdocs -A debian/README.source debian/README.Debian

	# this is texlive-extra
	dh_installtex -A --flavor=tree:texlive

	dh_installemacsen
	dh_installchangelogs
	dh_installman
	bash debian/fix-manpages.sh
	# info pages
	bash debian/convert-info-files-to-unix.sh
	dh_installinfo

	dh_installmime
	dh_lintian
	dh_link
	dh_strip_nondeterminism
	dh_compress -X.pdf
	dh_fixperms
	# remove .bat files, nobody needs them
	find debian/texlive-*/usr/share/texlive/texmf-dist/ -name \*.bat -a -type f -exec rm '{}' \;
	# remove x bit from files in the doc hierarchy
	find debian/texlive-*/usr/share/doc/texlive-doc -type f -exec chmod a-x '{}' \;
	# remove x bit from jar files
	find debian/texlive-*/usr/share/texlive -name \*.jar -exec chmod a-x '{}' \;
	# we also have to fix the permissions of the link targets from /usr/bin
	bash debian/fix-bin-symlink-targets-permissions.sh
	# remove texosquery and pax docs
	rm -rf debian/texlive-extra-utils/usr/share/doc/texlive-doc/support/texosquery/
	rm -rf debian/texlive-latex-extra-doc/usr/share/doc/texlive-doc/latex/pax/
	dh_installdeb
	dh_gencontrol
	dh_md5sums

	dh_builddeb -Ntexlive-latex-extra-doc -- -Zxz
	dh_builddeb -ptexlive-latex-extra-doc -- -Zxz -Sextreme -z9


binary: binary-indep binary-arch

.PHONY: build clean binary-indep binary-arch binary install

# eof
